.class public Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;
.super Ljava/lang/Object;
.source "CaptureTrimPreviewClient.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;
    }
.end annotation


# instance fields
.field private O0O:Z

.field private O88O:Landroidx/collection/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LruCache<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/scanner/ScannerUtils$CandidateLinesData;",
            ">;"
        }
    .end annotation
.end field

.field private O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

.field private OO:Landroid/view/View;

.field private OO〇00〇8oO:Z

.field private o0:Landroid/app/Dialog;

.field private o8o:Z

.field private o8oOOo:Z

.field private o8〇OO0〇0o:I

.field private oOO〇〇:I

.field private final oOo0:Landroid/app/Activity;

.field private oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;

.field private ooo0〇〇O:I

.field private o〇00O:Landroid/widget/CheckBox;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/view/MagnifierView;

.field private 〇08O〇00〇o:Landroid/view/View;

.field private 〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

.field private 〇8〇oO〇〇8o:I

.field private 〇OOo8〇0:Landroid/view/View;

.field private final 〇O〇〇O8:Lcom/intsig/Interpolator/EaseCubicInterpolator;

.field private volatile 〇o0O:Z

.field 〇〇08O:Lcom/intsig/utils/ClickLimit;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 6
    .line 7
    const/4 v0, -0x1

    .line 8
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇8〇oO〇〇8o:I

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->ooo0〇〇O:I

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇〇08O:Lcom/intsig/utils/ClickLimit;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O0O:Z

    .line 20
    .line 21
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oOOo:Z

    .line 22
    .line 23
    new-instance v1, Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 24
    .line 25
    const v2, 0x3dcccccd    # 0.1f

    .line 26
    .line 27
    .line 28
    const/high16 v3, 0x3f800000    # 1.0f

    .line 29
    .line 30
    const/high16 v4, 0x3e800000    # 0.25f

    .line 31
    .line 32
    invoke-direct {v1, v4, v2, v4, v3}, Lcom/intsig/Interpolator/EaseCubicInterpolator;-><init>(FFFF)V

    .line 33
    .line 34
    .line 35
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇O〇〇O8:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 36
    .line 37
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇o0O:Z

    .line 38
    .line 39
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOO〇〇:I

    .line 40
    .line 41
    const/4 v0, 0x1

    .line 42
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8o:Z

    .line 43
    .line 44
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 45
    .line 46
    iput-boolean p2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private O08000(Landroid/view/View;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    if-eqz v0, :cond_4

    .line 4
    .line 5
    if-eqz p1, :cond_4

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇08O〇00〇o:Landroid/view/View;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto/16 :goto_0

    .line 12
    .line 13
    :cond_0
    const/4 v0, 0x2

    .line 14
    new-array v1, v0, [I

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 17
    .line 18
    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 19
    .line 20
    .line 21
    new-array v2, v0, [I

    .line 22
    .line 23
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 24
    .line 25
    .line 26
    const/4 v3, 0x0

    .line 27
    aget v4, v2, v3

    .line 28
    .line 29
    aget v3, v1, v3

    .line 30
    .line 31
    sub-int/2addr v4, v3

    .line 32
    const/4 v3, 0x1

    .line 33
    aget v2, v2, v3

    .line 34
    .line 35
    aget v1, v1, v3

    .line 36
    .line 37
    sub-int/2addr v2, v1

    .line 38
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 39
    .line 40
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-gtz v1, :cond_1

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 47
    .line 48
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 53
    .line 54
    const/16 v5, 0xb4

    .line 55
    .line 56
    invoke-static {v3, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    iget-object v5, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇08O〇00〇o:Landroid/view/View;

    .line 61
    .line 62
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    .line 63
    .line 64
    .line 65
    move-result v5

    .line 66
    if-lez v5, :cond_2

    .line 67
    .line 68
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇08O〇00〇o:Landroid/view/View;

    .line 69
    .line 70
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    .line 71
    .line 72
    .line 73
    move-result v3

    .line 74
    sget-object v5, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 75
    .line 76
    const/16 v6, 0x1e

    .line 77
    .line 78
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 79
    .line 80
    .line 81
    move-result v5

    .line 82
    add-int/2addr v3, v5

    .line 83
    :cond_2
    iget-object v5, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 84
    .line 85
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 86
    .line 87
    .line 88
    move-result-object v5

    .line 89
    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 90
    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 92
    .line 93
    .line 94
    move-result v6

    .line 95
    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 96
    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 98
    .line 99
    .line 100
    move-result v6

    .line 101
    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 102
    .line 103
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 104
    .line 105
    iput v2, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 106
    .line 107
    iget-boolean v6, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 108
    .line 109
    if-eqz v6, :cond_3

    .line 110
    .line 111
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 112
    .line 113
    .line 114
    move-result v6

    .line 115
    add-int/2addr v6, v2

    .line 116
    add-int/2addr v6, v3

    .line 117
    if-le v6, v1, :cond_3

    .line 118
    .line 119
    sub-int/2addr v1, v2

    .line 120
    sub-int/2addr v1, v3

    .line 121
    if-lez v1, :cond_3

    .line 122
    .line 123
    const/high16 v3, 0x3f800000    # 1.0f

    .line 124
    .line 125
    int-to-float v6, v1

    .line 126
    mul-float v6, v6, v3

    .line 127
    .line 128
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 129
    .line 130
    .line 131
    move-result v3

    .line 132
    int-to-float v3, v3

    .line 133
    mul-float v6, v6, v3

    .line 134
    .line 135
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 136
    .line 137
    .line 138
    move-result v3

    .line 139
    int-to-float v3, v3

    .line 140
    div-float/2addr v6, v3

    .line 141
    float-to-int v3, v6

    .line 142
    iput v3, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 143
    .line 144
    iput v1, v5, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 145
    .line 146
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 147
    .line 148
    .line 149
    move-result p1

    .line 150
    iget v1, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 151
    .line 152
    sub-int/2addr p1, v1

    .line 153
    div-int/2addr p1, v0

    .line 154
    add-int/2addr v4, p1

    .line 155
    iput v4, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 156
    .line 157
    iput v2, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 158
    .line 159
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 160
    .line 161
    invoke-virtual {p1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 162
    .line 163
    .line 164
    return-void

    .line 165
    :cond_4
    :goto_0
    const-string p1, "CaptureTrimPreviewClient"

    .line 166
    .line 167
    const-string v0, "imageEditView == null || srcPreView == null || targetBitmap == null"

    .line 168
    .line 169
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .line 171
    .line 172
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/widget/RadioButton;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oo〇(Landroid/widget/RadioButton;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic O8ooOoo〇(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 2
    .line 3
    const v0, 0x7f0806df

    .line 4
    .line 5
    .line 6
    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O8〇o(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v0, "isChecked="

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const-string v0, "CaptureTrimPreviewClient"

    .line 19
    .line 20
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 24
    .line 25
    const v0, -0xe64364

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->setLinePaintColor(I)V

    .line 29
    .line 30
    .line 31
    if-eqz p2, :cond_2

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 34
    .line 35
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 36
    .line 37
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->Oo80:Ljava/util/concurrent/Future;

    .line 38
    .line 39
    if-eqz p1, :cond_1

    .line 40
    .line 41
    invoke-interface {p1}, Ljava/util/concurrent/Future;->isDone()Z

    .line 42
    .line 43
    .line 44
    move-result p2

    .line 45
    if-eqz p2, :cond_0

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇8oOO88(Ljava/util/concurrent/Future;)V

    .line 49
    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 53
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇8(Z)V

    .line 54
    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_2
    const/4 p1, 0x0

    .line 58
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇8(Z)V

    .line 59
    .line 60
    .line 61
    :goto_1
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)Landroid/app/Activity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static synthetic OOO〇O0(Landroid/graphics/drawable/Drawable;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇8O8〇(Z)V

    .line 3
    .line 4
    .line 5
    if-eqz p0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic Oo08(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇00(Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Oo8Oo00oo(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$3;

    .line 16
    .line 17
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$3;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/view/View;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇〇0o()V

    .line 30
    .line 31
    .line 32
    :goto_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇8O8〇008(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2

    .line 1
    const/4 p1, 0x4

    .line 2
    const/4 v0, 0x1

    .line 3
    if-ne p2, p1, :cond_0

    .line 4
    .line 5
    const-string p1, "CaptureTrimPreviewClient"

    .line 6
    .line 7
    const-string p2, "click system back"

    .line 8
    .line 9
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-wide/16 p1, 0x0

    .line 13
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0〇O0088o(J)V

    .line 15
    .line 16
    .line 17
    return v0

    .line 18
    :cond_0
    const/16 p1, 0x18

    .line 19
    .line 20
    if-ne p2, p1, :cond_1

    .line 21
    .line 22
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-ne p1, v0, :cond_1

    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/utils/CommonUtil;->〇O00()Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-eqz p1, :cond_1

    .line 33
    .line 34
    const-string p1, "scan_type"

    .line 35
    .line 36
    const-string p2, "cs_button"

    .line 37
    .line 38
    const-string p3, "CSBatchCropConfirm"

    .line 39
    .line 40
    const-string v1, "continue_by_button"

    .line 41
    .line 42
    invoke-static {p3, v1, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 46
    .line 47
    const p2, 0x7f0a132b

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 55
    .line 56
    .line 57
    return v0

    .line 58
    :cond_1
    const/4 p1, 0x0

    .line 59
    return p1
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O〇O〇oO()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 2
    .line 3
    const v1, 0x7f0806df

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 13
    .line 14
    const/16 v2, 0x12

    .line 15
    .line 16
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    const/4 v2, -0x1

    .line 21
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    .line 22
    .line 23
    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 24
    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    invoke-virtual {v0, v2, v2, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 28
    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 31
    .line 32
    const v2, 0x7f0a136d

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    check-cast v1, Landroid/widget/TextView;

    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 42
    .line 43
    const v3, 0x7f130a07

    .line 44
    .line 45
    .line 46
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    const/4 v3, 0x4

    .line 51
    invoke-static {v2, v0, v3}, Lcom/intsig/camscanner/util/StringUtil;->〇8o8o〇(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)Ljava/lang/CharSequence;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    .line 57
    .line 58
    :cond_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o800o8O(Landroid/view/View;)Landroid/view/animation/Animation;
    .locals 11
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v1, 0x0

    .line 10
    :goto_0
    if-eqz p1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOffset()F

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    int-to-float v1, v1

    .line 23
    const/high16 v2, 0x3f800000    # 1.0f

    .line 24
    .line 25
    mul-float v1, v1, v2

    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 28
    .line 29
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    int-to-float v3, v3

    .line 34
    const/high16 v4, 0x40000000    # 2.0f

    .line 35
    .line 36
    mul-float p1, p1, v4

    .line 37
    .line 38
    sub-float/2addr v3, p1

    .line 39
    div-float/2addr v1, v3

    .line 40
    int-to-float v0, v0

    .line 41
    mul-float v0, v0, v2

    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 44
    .line 45
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    int-to-float v2, v2

    .line 50
    sub-float/2addr v2, p1

    .line 51
    div-float/2addr v0, v2

    .line 52
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    .line 53
    .line 54
    .line 55
    move-result v6

    .line 56
    new-instance p1, Landroid/view/animation/ScaleAnimation;

    .line 57
    .line 58
    const/high16 v3, 0x3f800000    # 1.0f

    .line 59
    .line 60
    const/high16 v5, 0x3f800000    # 1.0f

    .line 61
    .line 62
    const/4 v7, 0x1

    .line 63
    const/high16 v8, 0x3f000000    # 0.5f

    .line 64
    .line 65
    const/4 v9, 0x1

    .line 66
    const/high16 v10, 0x3f000000    # 0.5f

    .line 67
    .line 68
    move-object v2, p1

    .line 69
    move v4, v6

    .line 70
    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 71
    .line 72
    .line 73
    return-object p1
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private o8oO〇(Landroid/view/View;I)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-ne v0, p2, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 11
    .line 12
    .line 13
    :cond_1
    :goto_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇oO08〇o0()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 17
    .line 18
    new-instance v1, LO8〇o〇88/oO80;

    .line 19
    .line 20
    invoke-direct {v1, p0}, LO8〇o〇88/oO80;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oO80(Landroid/graphics/drawable/Drawable;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OOO〇O0(Landroid/graphics/drawable/Drawable;Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oo88o8O()V
    .locals 4

    .line 1
    new-instance v0, Landroid/app/Dialog;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 4
    .line 5
    const v2, 0x7f1401d6

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 20
    .line 21
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    invoke-virtual {v1}, Landroid/view/Window;->getNavigationBarColor()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    invoke-virtual {v0, v1}, Landroid/view/Window;->setNavigationBarColor(I)V

    .line 32
    .line 33
    .line 34
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 35
    .line 36
    const/4 v1, 0x1

    .line 37
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 41
    .line 42
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const v2, 0x7f0d018b

    .line 47
    .line 48
    .line 49
    const/4 v3, 0x0

    .line 50
    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 55
    .line 56
    const v2, 0x7f0a132b

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 67
    .line 68
    const v2, 0x7f0a1745

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 79
    .line 80
    const v2, 0x7f0a093b

    .line 81
    .line 82
    .line 83
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    check-cast v0, Lcom/intsig/camscanner/view/ImageEditView;

    .line 88
    .line 89
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 90
    .line 91
    new-instance v2, LO8〇o〇88/Oo08;

    .line 92
    .line 93
    invoke-direct {v2}, LO8〇o〇88/Oo08;-><init>()V

    .line 94
    .line 95
    .line 96
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setRecycler(Lcom/intsig/camscanner/view/ImageViewTouchBase$Recycler;)V

    .line 97
    .line 98
    .line 99
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 100
    .line 101
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/view/ImageEditView;->setOnCornorChangeListener(Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;)V

    .line 102
    .line 103
    .line 104
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 105
    .line 106
    const v2, 0x7f0a0d65

    .line 107
    .line 108
    .line 109
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    check-cast v0, Lcom/intsig/camscanner/view/MagnifierView;

    .line 114
    .line 115
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇080OO8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 116
    .line 117
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 118
    .line 119
    invoke-virtual {v0, v1, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 120
    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇080OO8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 123
    .line 124
    invoke-virtual {v0, v1, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 125
    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 128
    .line 129
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 130
    .line 131
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 132
    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 135
    .line 136
    new-instance v1, LO8〇o〇88/o〇0;

    .line 137
    .line 138
    invoke-direct {v1, p0}, LO8〇o〇88/o〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 142
    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 145
    .line 146
    new-instance v1, LO8〇o〇88/〇〇888;

    .line 147
    .line 148
    invoke-direct {v1, p0}, LO8〇o〇88/〇〇888;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 152
    .line 153
    .line 154
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 155
    .line 156
    const v1, 0x7f0a19fe

    .line 157
    .line 158
    .line 159
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO:Landroid/view/View;

    .line 164
    .line 165
    const/high16 v1, -0x1000000

    .line 166
    .line 167
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 168
    .line 169
    .line 170
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 171
    .line 172
    const v1, 0x7f0a0b14

    .line 173
    .line 174
    .line 175
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇08O〇00〇o:Landroid/view/View;

    .line 180
    .line 181
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇OOo8〇0:Landroid/view/View;

    .line 182
    .line 183
    const v1, 0x7f0a035a

    .line 184
    .line 185
    .line 186
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 187
    .line 188
    .line 189
    move-result-object v0

    .line 190
    check-cast v0, Landroid/widget/CheckBox;

    .line 191
    .line 192
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 193
    .line 194
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 195
    .line 196
    .line 197
    move-result-object v0

    .line 198
    if-eqz v0, :cond_1

    .line 199
    .line 200
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 201
    .line 202
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$1;

    .line 207
    .line 208
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$1;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)V

    .line 209
    .line 210
    .line 211
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 212
    .line 213
    .line 214
    :cond_1
    return-void
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private synthetic oo〇(Landroid/widget/RadioButton;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇00〇8(Landroid/widget/RadioButton;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/widget/RadioButton;Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇〇0〇(Landroid/widget/RadioButton;Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private o〇0OOo〇0()V
    .locals 9

    .line 1
    const-string v0, "CSBatchCropAsk"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 7
    .line 8
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const v1, 0x7f0d01a6

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const v1, 0x7f0a14da

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Landroid/widget/TextView;

    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 30
    .line 31
    const/4 v3, 0x2

    .line 32
    new-array v3, v3, [Ljava/lang/Object;

    .line 33
    .line 34
    const v4, 0x7f131db3

    .line 35
    .line 36
    .line 37
    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    const/4 v5, 0x0

    .line 42
    aput-object v4, v3, v5

    .line 43
    .line 44
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 45
    .line 46
    const v6, 0x7f1303bc

    .line 47
    .line 48
    .line 49
    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v4

    .line 53
    const/4 v6, 0x1

    .line 54
    aput-object v4, v3, v6

    .line 55
    .line 56
    const v4, 0x7f130a08

    .line 57
    .line 58
    .line 59
    invoke-virtual {v2, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    .line 65
    .line 66
    const v1, 0x7f0a0ee4

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    check-cast v1, Landroid/widget/RadioButton;

    .line 74
    .line 75
    invoke-virtual {v1, v6}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 76
    .line 77
    .line 78
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 79
    .line 80
    const v3, 0x7f0806df

    .line 81
    .line 82
    .line 83
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    const/high16 v3, 0x3f800000    # 1.0f

    .line 88
    .line 89
    const v4, 0x7f060208

    .line 90
    .line 91
    .line 92
    if-eqz v2, :cond_0

    .line 93
    .line 94
    iget-object v6, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 95
    .line 96
    const/16 v7, 0x10

    .line 97
    .line 98
    invoke-static {v6, v7}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 99
    .line 100
    .line 101
    move-result v6

    .line 102
    invoke-static {v4, v3}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 103
    .line 104
    .line 105
    move-result v7

    .line 106
    sget-object v8, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    .line 107
    .line 108
    invoke-virtual {v2, v7, v8}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {v2, v5, v5, v6, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 112
    .line 113
    .line 114
    iget-object v6, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 115
    .line 116
    const v7, 0x7f130a1a

    .line 117
    .line 118
    .line 119
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v6

    .line 123
    const/4 v7, 0x4

    .line 124
    invoke-static {v6, v2, v7}, Lcom/intsig/camscanner/util/StringUtil;->〇8o8o〇(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)Ljava/lang/CharSequence;

    .line 125
    .line 126
    .line 127
    move-result-object v6

    .line 128
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    .line 130
    .line 131
    :cond_0
    const v1, 0x7f0a0ee3

    .line 132
    .line 133
    .line 134
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 135
    .line 136
    .line 137
    move-result-object v1

    .line 138
    check-cast v1, Landroid/widget/RadioButton;

    .line 139
    .line 140
    const v6, 0x7f130a19

    .line 141
    .line 142
    .line 143
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 144
    .line 145
    .line 146
    new-instance v6, Lcom/intsig/app/AlertDialog$Builder;

    .line 147
    .line 148
    iget-object v7, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 149
    .line 150
    invoke-direct {v6, v7}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v6, v5}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 154
    .line 155
    .line 156
    move-result-object v5

    .line 157
    const v6, 0x7f130a1f

    .line 158
    .line 159
    .line 160
    invoke-static {v4, v3}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 161
    .line 162
    .line 163
    move-result v3

    .line 164
    invoke-virtual {v5, v6, v3}, Lcom/intsig/app/AlertDialog$Builder;->〇08O8o〇0(II)Lcom/intsig/app/AlertDialog$Builder;

    .line 165
    .line 166
    .line 167
    move-result-object v3

    .line 168
    invoke-virtual {v3, v0}, Lcom/intsig/app/AlertDialog$Builder;->oO(Landroid/view/View;)Lcom/intsig/app/AlertDialog$Builder;

    .line 169
    .line 170
    .line 171
    move-result-object v0

    .line 172
    new-instance v3, LO8〇o〇88/〇080;

    .line 173
    .line 174
    invoke-direct {v3, p0, v1}, LO8〇o〇88/〇080;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/widget/RadioButton;)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v0, v3}, Lcom/intsig/app/AlertDialog$Builder;->O8ooOoo〇(Landroid/content/DialogInterface$OnKeyListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    new-instance v3, LO8〇o〇88/〇o00〇〇Oo;

    .line 182
    .line 183
    invoke-direct {v3, v2}, LO8〇o〇88/〇o00〇〇Oo;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v0, v3}, Lcom/intsig/app/AlertDialog$Builder;->O〇8O8〇008(Landroid/content/DialogInterface$OnDismissListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 187
    .line 188
    .line 189
    move-result-object v0

    .line 190
    new-instance v2, LO8〇o〇88/〇o〇;

    .line 191
    .line 192
    invoke-direct {v2, p0, v1}, LO8〇o〇88/〇o〇;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/widget/RadioButton;)V

    .line 193
    .line 194
    .line 195
    const v1, 0x7f131e36

    .line 196
    .line 197
    .line 198
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 199
    .line 200
    .line 201
    move-result-object v0

    .line 202
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 207
    .line 208
    .line 209
    return-void
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private o〇8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;->〇〇888()V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OoO8()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o〇8oOO88(Ljava/util/concurrent/Future;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future<",
            "*>;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 4
    .line 5
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$2;

    .line 6
    .line 7
    invoke-direct {v2, p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$2;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Ljava/util/concurrent/Future;)V

    .line 8
    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 11
    .line 12
    const v3, 0x7f130e22

    .line 13
    .line 14
    .line 15
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic o〇〇0〇(Landroid/widget/RadioButton;Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    const/16 v0, 0x18

    .line 2
    .line 3
    if-ne p3, v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p4}, Landroid/view/KeyEvent;->getAction()I

    .line 6
    .line 7
    .line 8
    move-result p3

    .line 9
    const/4 p4, 0x1

    .line 10
    if-ne p3, p4, :cond_0

    .line 11
    .line 12
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇00〇8(Landroid/widget/RadioButton;)V

    .line 16
    .line 17
    .line 18
    return p4

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static synthetic 〇00(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇0000OOO(FLandroid/animation/ValueAnimator;)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    check-cast p2, Ljava/lang/Float;

    .line 6
    .line 7
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    .line 8
    .line 9
    .line 10
    move-result p2

    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 12
    .line 13
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setOffset(F)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 17
    .line 18
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 19
    .line 20
    .line 21
    invoke-static {p2, p1}, Ljava/lang/Float;->compare(FF)I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    if-nez p1, :cond_0

    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇8〇0〇o〇O()V

    .line 28
    .line 29
    .line 30
    const/4 p1, 0x0

    .line 31
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O0O:Z

    .line 32
    .line 33
    :cond_0
    return-void
.end method

.method private 〇00〇8(Landroid/widget/RadioButton;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O00oo0(Z)V

    .line 12
    .line 13
    .line 14
    const-wide/16 v0, 0x0

    .line 15
    .line 16
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0〇O0088o(J)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    const-string p1, "ON"

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const-string p1, "OFF"

    .line 29
    .line 30
    :goto_0
    const-string v0, "CSBatchCropAsk"

    .line 31
    .line 32
    const-string v1, "confirm"

    .line 33
    .line 34
    const-string v2, "SCHEMA"

    .line 35
    .line 36
    invoke-static {v0, v1, v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;FLandroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0000OOO(FLandroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇0〇O0088o(J)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oOOo:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oOOo:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO:Landroid/view/View;

    .line 10
    .line 11
    const/16 v1, 0x8

    .line 12
    .line 13
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oO〇(Landroid/view/View;I)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇08O〇00〇o:Landroid/view/View;

    .line 23
    .line 24
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oO〇(Landroid/view/View;I)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;

    .line 28
    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 38
    .line 39
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇080:[I

    .line 40
    .line 41
    if-eqz v1, :cond_1

    .line 42
    .line 43
    invoke-static {v1, v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getScanBoundF([I[I)[I

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 48
    .line 49
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 50
    .line 51
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 52
    .line 53
    invoke-static {v1, v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isSameBorder([I[I)Z

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    if-nez v1, :cond_2

    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 60
    .line 61
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 62
    .line 63
    iput-object v0, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 64
    .line 65
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;

    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 68
    .line 69
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 70
    .line 71
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;->OO0o〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 72
    .line 73
    .line 74
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇〇8O0〇8(J)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇8(Z)V
    .locals 3

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 4
    .line 5
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 6
    .line 7
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇o〇:[I

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-static {p1}, Lcom/intsig/utils/PointUtil;->Oo08([I)[F

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 18
    .line 19
    iget v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->oO80:F

    .line 20
    .line 21
    const/4 v2, 0x1

    .line 22
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/camscanner/view/ImageEditView;->o8oO〇([FFZ)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 29
    .line 30
    iget v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->oO80:F

    .line 31
    .line 32
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 33
    .line 34
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 35
    .line 36
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->〇8(FLjava/lang/String;)V

    .line 37
    .line 38
    .line 39
    :goto_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8ooOoo〇(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)Landroid/widget/CheckBox;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8〇0〇o〇O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇08O〇00〇o:Landroid/view/View;

    .line 2
    .line 3
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v1, 0x4

    .line 11
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oO〇(Landroid/view/View;I)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getRotateBitmap()Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const/4 v3, 0x1

    .line 21
    invoke-virtual {v0, v1, v3}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 25
    .line 26
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇8(Z)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 34
    .line 35
    invoke-virtual {v0, v3, v2}, Lcom/intsig/camscanner/view/ImageEditView;->o〇O(ZZ)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 39
    .line 40
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImageEditView;->O〇8O8〇008(Z)V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 46
    .line 47
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 48
    .line 49
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/view/ImageEditView;->〇00(ZZ)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 53
    .line 54
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/view/ImageEditView;->setRegionVisibility(Z)V

    .line 55
    .line 56
    .line 57
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 58
    .line 59
    if-eqz v0, :cond_1

    .line 60
    .line 61
    invoke-static {}, Lcom/intsig/camscanner/scanner/CandidateLinesManager;->getInstance()Lcom/intsig/camscanner/scanner/CandidateLinesManager;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/CandidateLinesManager;->initResource4Lines()V

    .line 66
    .line 67
    .line 68
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8〇OO0〇0o:I

    .line 69
    .line 70
    add-int/2addr v0, v3

    .line 71
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8〇OO0〇0o:I

    .line 72
    .line 73
    const-string v0, "CSBatchCropConfirm"

    .line 74
    .line 75
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_1
    const-wide/16 v0, 0x64

    .line 80
    .line 81
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0〇O0088o(J)V

    .line 82
    .line 83
    .line 84
    :goto_1
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O08000(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O888o0o(Landroid/view/View;)Landroid/view/animation/Animation;
    .locals 12

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    new-array v0, v0, [I

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 7
    .line 8
    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 12
    .line 13
    .line 14
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .line 15
    .line 16
    const/4 v4, 0x0

    .line 17
    const/4 v5, 0x0

    .line 18
    const/4 v6, 0x0

    .line 19
    const/4 v3, 0x0

    .line 20
    aget v7, v0, v3

    .line 21
    .line 22
    int-to-float v7, v7

    .line 23
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 24
    .line 25
    .line 26
    move-result v8

    .line 27
    int-to-float v8, v8

    .line 28
    const/high16 v9, 0x40000000    # 2.0f

    .line 29
    .line 30
    div-float/2addr v8, v9

    .line 31
    add-float/2addr v7, v8

    .line 32
    aget v3, v1, v3

    .line 33
    .line 34
    int-to-float v3, v3

    .line 35
    iget-object v8, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 36
    .line 37
    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    .line 38
    .line 39
    .line 40
    move-result v8

    .line 41
    int-to-float v8, v8

    .line 42
    div-float/2addr v8, v9

    .line 43
    add-float/2addr v3, v8

    .line 44
    sub-float/2addr v7, v3

    .line 45
    const/4 v8, 0x0

    .line 46
    const/4 v10, 0x0

    .line 47
    const/4 v11, 0x0

    .line 48
    const/4 v3, 0x1

    .line 49
    aget v0, v0, v3

    .line 50
    .line 51
    int-to-float v0, v0

    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    int-to-float p1, p1

    .line 57
    div-float/2addr p1, v9

    .line 58
    add-float/2addr v0, p1

    .line 59
    aget p1, v1, v3

    .line 60
    .line 61
    int-to-float p1, p1

    .line 62
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 63
    .line 64
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    int-to-float v1, v1

    .line 69
    div-float/2addr v1, v9

    .line 70
    add-float/2addr p1, v1

    .line 71
    sub-float p1, v0, p1

    .line 72
    .line 73
    move-object v3, v2

    .line 74
    move v9, v10

    .line 75
    move v10, v11

    .line 76
    move v11, p1

    .line 77
    invoke-direct/range {v3 .. v11}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 78
    .line 79
    .line 80
    return-object v2
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇8(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇o()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v0, "CaptureTrimPreviewClient"

    .line 6
    .line 7
    const-string v1, "is playEnterAnimationForShowImage"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O0O:Z

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 19
    .line 20
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 21
    .line 22
    iget-boolean v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇800OO〇0O:Z

    .line 23
    .line 24
    if-eqz v2, :cond_1

    .line 25
    .line 26
    const/16 v2, 0x28

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/16 v2, 0x14

    .line 30
    .line 31
    :goto_0
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    int-to-float v1, v1

    .line 36
    const/4 v2, 0x2

    .line 37
    new-array v2, v2, [F

    .line 38
    .line 39
    const/4 v3, 0x0

    .line 40
    const/4 v4, 0x0

    .line 41
    aput v4, v2, v3

    .line 42
    .line 43
    aput v1, v2, v0

    .line 44
    .line 45
    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    new-instance v2, LO8〇o〇88/〇80〇808〇O;

    .line 50
    .line 51
    invoke-direct {v2, p0, v1}, LO8〇o〇88/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;F)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 55
    .line 56
    .line 57
    const-wide/16 v1, 0xc8

    .line 58
    .line 59
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O〇8O8〇008(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic 〇oOO8O8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0〇Oo()Z

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O88O:Landroidx/collection/LruCache;

    .line 14
    .line 15
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->findCandidateLines(Ljava/lang/String;Lcom/intsig/camscanner/view/ImageEditView;ZLandroidx/collection/LruCache;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private 〇oo〇([I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 2
    .line 3
    const-string v1, "CaptureTrimPreviewClient"

    .line 4
    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇080:[I

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOO〇〇:I

    .line 13
    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOO〇〇:I

    .line 21
    .line 22
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOO〇〇:I

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 25
    .line 26
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇080:[I

    .line 27
    .line 28
    invoke-static {v0, v2, p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->checkCropBounds(I[I[I)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    new-instance v2, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v3, "isCanTrim = "

    .line 38
    .line 39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v3, ", bounds = "

    .line 46
    .line 47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return v0

    .line 65
    :cond_2
    :goto_0
    const-string p1, "isCanTrim multiCapturePreviewData == null || engineContext == 0 || multiCapturePreviewData.srcImageBound == null"

    .line 66
    .line 67
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    const/4 p1, 0x0

    .line 71
    return p1
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8〇o(Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇〇0o()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setOffset(F)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 8
    .line 9
    iget-object v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 10
    .line 11
    new-instance v3, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->Oo08:Landroid/graphics/Bitmap;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O8()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-direct {v3, v0, v2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;I)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v3}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const/4 v2, 0x0

    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    const-string v0, "CaptureTrimPreviewClient"

    .line 30
    .line 31
    const-string v1, "thumb == null"

    .line 32
    .line 33
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    new-instance v4, Landroid/graphics/RectF;

    .line 38
    .line 39
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 40
    .line 41
    .line 42
    move-result v5

    .line 43
    int-to-float v5, v5

    .line 44
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 45
    .line 46
    .line 47
    move-result v6

    .line 48
    int-to-float v6, v6

    .line 49
    invoke-direct {v4, v1, v1, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 50
    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 53
    .line 54
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ImageEditView;->setRegionVisibility(Z)V

    .line 55
    .line 56
    .line 57
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 58
    .line 59
    const/4 v5, 0x0

    .line 60
    invoke-virtual {v1, v5}, Lcom/intsig/camscanner/view/ImageEditView;->setBitmapEnhanced(Landroid/graphics/Bitmap;)V

    .line 61
    .line 62
    .line 63
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 64
    .line 65
    invoke-virtual {v1}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 70
    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇080OO8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 73
    .line 74
    invoke-virtual {v1, v0, v4}, Lcom/intsig/camscanner/view/MagnifierView;->o〇0(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)V

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 78
    .line 79
    const/4 v1, 0x1

    .line 80
    invoke-virtual {v0, v3, v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 81
    .line 82
    .line 83
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 84
    .line 85
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oO〇(Landroid/view/View;I)V

    .line 86
    .line 87
    .line 88
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇o()V

    .line 89
    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇〇0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇oOO8O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇8O0〇8(J)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;->〇080()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;

    .line 13
    .line 14
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;->〇080()Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    new-instance v1, Landroid/view/animation/AnimationSet;

    .line 19
    .line 20
    const/4 v2, 0x1

    .line 21
    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 22
    .line 23
    .line 24
    const-wide/16 v3, 0x1f4

    .line 25
    .line 26
    invoke-virtual {v1, v3, v4}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o800o8O(Landroid/view/View;)Landroid/view/animation/Animation;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 34
    .line 35
    .line 36
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇O888o0o(Landroid/view/View;)Landroid/view/animation/Animation;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 41
    .line 42
    .line 43
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$4;

    .line 44
    .line 45
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$4;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1, p1, p2}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 52
    .line 53
    .line 54
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇O〇〇O8:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/Interpolator/EaseCubicInterpolator;->〇o00〇〇Oo()V

    .line 57
    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇O〇〇O8:Lcom/intsig/Interpolator/EaseCubicInterpolator;

    .line 60
    .line 61
    invoke-virtual {v1, p1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 65
    .line 66
    const/4 p2, 0x0

    .line 67
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/view/ImageEditView;->O〇8O8〇008(Z)V

    .line 68
    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 71
    .line 72
    invoke-virtual {p1, p2, v2}, Lcom/intsig/camscanner/view/ImageEditView;->〇00(ZZ)V

    .line 73
    .line 74
    .line 75
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 76
    .line 77
    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 78
    .line 79
    .line 80
    return-void

    .line 81
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OoO8()V

    .line 82
    .line 83
    .line 84
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public OoO8()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oOOo:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 5
    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :catch_0
    move-exception v0

    .line 22
    const-string v1, "CaptureTrimPreviewClient"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o0ooO()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOO〇〇:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient$CaptureTrimPreviewCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o88〇OO08〇(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImageEditView;->setRegionAvailability(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇oo〇([I)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 29
    .line 30
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/ImageEditView;->setRegionAvailability(Z)V

    .line 31
    .line 32
    .line 33
    const-string v0, "onCornorChanged: isRegionAvailabl = true,  isCanTrim = false"

    .line 34
    .line 35
    const-string v3, "CaptureTrimPreviewClient"

    .line 36
    .line 37
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    const/16 v0, -0x6b00

    .line 41
    .line 42
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 43
    .line 44
    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/view/ImageEditView;->setLinePaintColor(I)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 48
    .line 49
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 50
    .line 51
    .line 52
    if-eqz p1, :cond_1

    .line 53
    .line 54
    iget-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8o:Z

    .line 55
    .line 56
    if-eqz p1, :cond_1

    .line 57
    .line 58
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oOo0:Landroid/app/Activity;

    .line 59
    .line 60
    const v0, 0x7f130529

    .line 61
    .line 62
    .line 63
    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-virtual {p1, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 71
    .line 72
    .line 73
    iput-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8o:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .line 75
    goto :goto_0

    .line 76
    :catch_0
    move-exception p1

    .line 77
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_0
    const p1, -0xe64364

    .line 82
    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 85
    .line 86
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ImageEditView;->setLinePaintColor(I)V

    .line 87
    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 90
    .line 91
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 92
    .line 93
    .line 94
    :cond_1
    :goto_0
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇〇08O:Lcom/intsig/utils/ClickLimit;

    .line 6
    .line 7
    sget-wide v2, Lcom/intsig/utils/ClickLimit;->〇o〇:J

    .line 8
    .line 9
    invoke-virtual {v1, p1, v2, v3}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-nez p1, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    const p1, 0x7f0a1745

    .line 17
    .line 18
    .line 19
    const-string v1, "ON"

    .line 20
    .line 21
    const-string v2, "OFF"

    .line 22
    .line 23
    const-string v3, "SCHEMA"

    .line 24
    .line 25
    const-string v4, "CSBatchCropConfirm"

    .line 26
    .line 27
    const-string v5, "CaptureTrimPreviewClient"

    .line 28
    .line 29
    if-ne v0, p1, :cond_2

    .line 30
    .line 31
    const-string p1, "retake"

    .line 32
    .line 33
    invoke-static {v5, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇8()V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 40
    .line 41
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_1

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    move-object v1, v2

    .line 49
    :goto_0
    invoke-static {v4, p1, v3, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_2
    const p1, 0x7f0a132b

    .line 54
    .line 55
    .line 56
    if-ne v0, p1, :cond_5

    .line 57
    .line 58
    const-string p1, "continue"

    .line 59
    .line 60
    invoke-static {v5, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇00O:Landroid/widget/CheckBox;

    .line 64
    .line 65
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    if-eqz v0, :cond_3

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_3
    move-object v1, v2

    .line 73
    :goto_1
    invoke-static {v4, p1, v3, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    const/4 p1, 0x1

    .line 77
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8〇OO0〇0o:I

    .line 78
    .line 79
    if-lt v0, p1, :cond_4

    .line 80
    .line 81
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OoO888()Z

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    if-eqz p1, :cond_4

    .line 86
    .line 87
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o〇0OOo〇0()V

    .line 88
    .line 89
    .line 90
    goto :goto_2

    .line 91
    :cond_4
    const-wide/16 v0, 0x0

    .line 92
    .line 93
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0〇O0088o(J)V

    .line 94
    .line 95
    .line 96
    :cond_5
    :goto_2
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public o〇0o〇〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "CaptureTrimPreviewClient"

    .line 6
    .line 7
    const-string v1, "multiCapturePreviewData == null"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇o0O:Z

    .line 14
    .line 15
    if-nez v0, :cond_2

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 18
    .line 19
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 22
    .line 23
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    const/4 v0, 0x1

    .line 30
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇o0O:Z

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O88O:Landroidx/collection/LruCache;

    .line 33
    .line 34
    if-nez v0, :cond_1

    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->createCandidateLinesDataLruCache()Landroidx/collection/LruCache;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O88O:Landroidx/collection/LruCache;

    .line 41
    .line 42
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/scanner/CandidateLinesManager;->getInstance()Lcom/intsig/camscanner/scanner/CandidateLinesManager;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    new-instance v1, LO8〇o〇88/O8;

    .line 47
    .line 48
    invoke-direct {v1, p0}, LO8〇o〇88/O8;-><init>(Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/scanner/CandidateLinesManager;->findCandidateLines(Ljava/lang/Runnable;)V

    .line 52
    .line 53
    .line 54
    const/4 v0, 0x0

    .line 55
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇o0O:Z

    .line 56
    .line 57
    :cond_2
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇O8〇〇o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oOOo:Z

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O0O:Z

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇08O8o〇0(Landroid/view/View;Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;)V
    .locals 3

    .line 1
    iput-object p2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 4
    .line 5
    if-nez p2, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oo88o8O()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O〇O〇oO()V

    .line 11
    .line 12
    .line 13
    :cond_0
    const/4 p2, 0x0

    .line 14
    if-eqz p1, :cond_1

    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    :goto_0
    if-eqz p1, :cond_2

    .line 23
    .line 24
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    goto :goto_1

    .line 29
    :cond_2
    const/4 v1, 0x0

    .line 30
    :goto_1
    iget v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇8〇oO〇〇8o:I

    .line 31
    .line 32
    if-ne v2, v0, :cond_3

    .line 33
    .line 34
    iget v2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->ooo0〇〇O:I

    .line 35
    .line 36
    if-eq v2, v1, :cond_4

    .line 37
    .line 38
    :cond_3
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇8〇oO〇〇8o:I

    .line 39
    .line 40
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->ooo0〇〇O:I

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 43
    .line 44
    const/4 v1, 0x4

    .line 45
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oO〇(Landroid/view/View;I)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇08O〇00〇o:Landroid/view/View;

    .line 49
    .line 50
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oO〇(Landroid/view/View;I)V

    .line 51
    .line 52
    .line 53
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO:Landroid/view/View;

    .line 54
    .line 55
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 56
    .line 57
    if-eqz v1, :cond_5

    .line 58
    .line 59
    const/4 v1, 0x0

    .line 60
    goto :goto_2

    .line 61
    :cond_5
    const/16 v1, 0x8

    .line 62
    .line 63
    :goto_2
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8oO〇(Landroid/view/View;I)V

    .line 64
    .line 65
    .line 66
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->OO〇00〇8oO:Z

    .line 67
    .line 68
    if-eqz v0, :cond_6

    .line 69
    .line 70
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 71
    .line 72
    const/high16 v0, -0x1000000

    .line 73
    .line 74
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 75
    .line 76
    .line 77
    goto :goto_3

    .line 78
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 79
    .line 80
    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 81
    .line 82
    .line 83
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->oO()V

    .line 84
    .line 85
    .line 86
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 87
    .line 88
    invoke-virtual {p2}, Landroid/app/Dialog;->isShowing()Z

    .line 89
    .line 90
    .line 91
    move-result p2

    .line 92
    if-nez p2, :cond_7

    .line 93
    .line 94
    :try_start_0
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o0:Landroid/app/Dialog;

    .line 95
    .line 96
    invoke-virtual {p2}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .line 98
    .line 99
    goto :goto_4

    .line 100
    :catch_0
    move-exception p2

    .line 101
    const-string v0, "CaptureTrimPreviewClient"

    .line 102
    .line 103
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 104
    .line 105
    .line 106
    :cond_7
    :goto_4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->Oo8Oo00oo(Landroid/view/View;)V

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇08O〇00〇o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇080OO8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/MagnifierView;->〇080()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0O〇Oo()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇Oo(FF)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇080OO8〇0:Lcom/intsig/camscanner/view/MagnifierView;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 6
    .line 7
    if-eqz v1, :cond_3

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇0O:Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;

    .line 10
    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    goto :goto_2

    .line 14
    :cond_0
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;->〇80〇808〇O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 15
    .line 16
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O8()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 21
    .line 22
    invoke-virtual {v1}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/ImageEditView;->getCropRegion()Lcom/intsig/camscanner/view/HightlightRegion;

    .line 29
    .line 30
    .line 31
    move-result-object v5

    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/ImageEditView;->O8〇o()Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-eqz v1, :cond_2

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->O8o08O8O:Lcom/intsig/camscanner/view/ImageEditView;

    .line 41
    .line 42
    const/4 v2, 0x0

    .line 43
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->〇oo〇([I)Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    if-eqz v1, :cond_1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    const/4 v6, 0x0

    .line 55
    goto :goto_1

    .line 56
    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 57
    const/4 v6, 0x1

    .line 58
    :goto_1
    move v1, p1

    .line 59
    move v2, p2

    .line 60
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/view/MagnifierView;->update(FFILandroid/graphics/Matrix;Lcom/intsig/camscanner/view/HightlightRegion;Z)V

    .line 61
    .line 62
    .line 63
    return-void

    .line 64
    :cond_3
    :goto_2
    const-string p1, "CaptureTrimPreviewClient"

    .line 65
    .line 66
    const-string p2, "magnifierView == null || imageEditView == null || multiCapturePreviewData == null"

    .line 67
    .line 68
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇〇〇0〇〇0(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/client/CaptureTrimPreviewClient;->o8〇OO0〇0o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

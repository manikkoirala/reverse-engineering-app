.class public interface abstract Lcom/intsig/camscanner/multiimageedit/client/IAnimationClient$CaptureTrimPreviewCallback;
.super Ljava/lang/Object;
.source "IAnimationClient.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/multiimageedit/client/IAnimationClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CaptureTrimPreviewCallback"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract O8(Landroid/graphics/Bitmap;Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;)V
    .param p2    # Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract Oo08(Z)V
.end method

.method public abstract o〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation
.end method

.method public abstract 〇080()Landroid/view/View;
.end method

.method public abstract 〇o00〇〇Oo()V
.end method

.method public abstract 〇o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;)V
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiCapturePreviewData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

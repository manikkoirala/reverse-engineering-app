.class public Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;
.super Lcom/intsig/singleton/Singleton;
.source "MultiImageEditPageManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditModelRunnable;,
        Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;,
        Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;,
        Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskType;,
        Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;,
        Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;
    }
.end annotation


# instance fields
.field private O8:I

.field private volatile OO0o〇〇:Landroid/os/Handler;

.field private final OO0o〇〇〇〇0:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;",
            ">;"
        }
    .end annotation
.end field

.field private Oo08:I

.field private OoO8:Ljava/util/concurrent/ThreadPoolExecutor;

.field private Oooo8o0〇:Ljava/lang/Thread;

.field private final o800o8O:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private oO80:Z

.field private final oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

.field o〇0:Ljava/util/concurrent/atomic/AtomicInteger;

.field public o〇O8〇〇o:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇00:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;",
            ">;"
        }
    .end annotation
.end field

.field 〇080:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation
.end field

.field private 〇0〇O0088o:Ljava/util/concurrent/Semaphore;

.field private volatile 〇80〇808〇O:Z

.field private final 〇8o8o〇:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇O00:[B

.field private final 〇O888o0o:[B

.field private 〇O8o08O:Landroid/os/HandlerThread;

.field private volatile 〇O〇:Z

.field 〇o00〇〇Oo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation
.end field

.field public 〇oo〇:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o〇:I

.field private volatile 〇〇808〇:Z

.field 〇〇888:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final 〇〇8O0〇8:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/singleton/Singleton;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 10
    .line 11
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o00〇〇Oo:Ljava/util/List;

    .line 17
    .line 18
    const/4 v0, -0x1

    .line 19
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o〇:I

    .line 20
    .line 21
    const/16 v0, 0x800

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8:I

    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oo08:I

    .line 27
    .line 28
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 29
    .line 30
    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 31
    .line 32
    .line 33
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 34
    .line 35
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    .line 36
    .line 37
    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 38
    .line 39
    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇888:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 41
    .line 42
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇80〇808〇O:Z

    .line 43
    .line 44
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/model/〇080;

    .line 45
    .line 46
    invoke-direct {v1}, Lcom/intsig/camscanner/multiimageedit/model/〇080;-><init>()V

    .line 47
    .line 48
    .line 49
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇〇〇0:Ljava/util/Comparator;

    .line 50
    .line 51
    new-instance v2, Ljava/util/concurrent/PriorityBlockingQueue;

    .line 52
    .line 53
    const/16 v3, 0xa

    .line 54
    .line 55
    invoke-direct {v2, v3, v1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    .line 56
    .line 57
    .line 58
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇8o8o〇:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 59
    .line 60
    const/4 v1, 0x0

    .line 61
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O8o08O:Landroid/os/HandlerThread;

    .line 62
    .line 63
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇:Landroid/os/Handler;

    .line 64
    .line 65
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oooo8o0〇:Ljava/lang/Thread;

    .line 66
    .line 67
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇808〇:Z

    .line 68
    .line 69
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇:Z

    .line 70
    .line 71
    new-array v2, v0, [B

    .line 72
    .line 73
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O00:[B

    .line 74
    .line 75
    new-array v2, v0, [B

    .line 76
    .line 77
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇8O0〇8:[B

    .line 78
    .line 79
    new-instance v2, Ljava/util/concurrent/Semaphore;

    .line 80
    .line 81
    const/4 v3, 0x1

    .line 82
    invoke-direct {v2, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 83
    .line 84
    .line 85
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 86
    .line 87
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OoO8:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 88
    .line 89
    new-instance v1, Ljava/util/HashSet;

    .line 90
    .line 91
    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 92
    .line 93
    .line 94
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o800o8O:Ljava/util/HashSet;

    .line 95
    .line 96
    new-array v0, v0, [B

    .line 97
    .line 98
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O888o0o:[B

    .line 99
    .line 100
    sget-object v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇8o8o〇:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;

    .line 101
    .line 102
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;->〇080()Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 107
    .line 108
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 109
    .line 110
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 111
    .line 112
    .line 113
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇oo〇:Landroidx/lifecycle/MutableLiveData;

    .line 114
    .line 115
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 116
    .line 117
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 118
    .line 119
    .line 120
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇O8〇〇o:Landroidx/lifecycle/MutableLiveData;

    .line 121
    .line 122
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 123
    .line 124
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 125
    .line 126
    .line 127
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇00:Ljava/util/List;

    .line 128
    .line 129
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O0(Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/os/Handler;

    .line 2
    .line 3
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O000(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Ljava/lang/Integer;)Lkotlin/Unit;
    .locals 0

    .line 1
    sget-object p3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 2
    .line 3
    invoke-direct {p0, p1, p3, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8〇o(Ljava/util/List;Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O08000(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;I)V
    .locals 7

    .line 1
    new-instance v6, Lcom/intsig/camscanner/multiimageedit/model/〇o〇;

    .line 2
    .line 3
    move-object v0, v6

    .line 4
    move-object v1, p0

    .line 5
    move-object v2, p1

    .line 6
    move v3, p4

    .line 7
    move-object v4, p2

    .line 8
    move-object v5, p3

    .line 9
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/multiimageedit/model/〇o〇;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;)V

    .line 10
    .line 11
    .line 12
    const-string p2, "HandleAll"

    .line 13
    .line 14
    invoke-direct {p0, p1, p2, v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o8O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditModelRunnable;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private O0O8OO088(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string p2, "MultiImageEditPageManager"

    .line 10
    .line 11
    const-string v0, "whitePadHandle pad tempSmallOnlyTrimImagePath not exist"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 17
    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8〇o(Ljava/util/List;Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/capture/writeboard/PadImageHandler;->〇080:Lcom/intsig/camscanner/capture/writeboard/PadImageHandler;

    .line 23
    .line 24
    iget-object v1, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 25
    .line 26
    invoke-virtual {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    new-instance v3, LO〇O/〇o00〇〇Oo;

    .line 31
    .line 32
    invoke-direct {v3, p0, p2, p1, p3}, LO〇O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 33
    .line 34
    .line 35
    new-instance p2, LO〇O/〇o〇;

    .line 36
    .line 37
    invoke-direct {p2, p0, p1, p3}, LO〇O/〇o〇;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/intsig/camscanner/capture/writeboard/PadImageHandler;->O8(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O0o〇〇Oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskType;->〇o00〇〇Oo:I

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇OOo:Z

    .line 7
    .line 8
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->Oo08(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Z)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/model/OO0o〇〇〇〇0;

    .line 12
    .line 13
    invoke-direct {v0, p0, p2, p1}, Lcom/intsig/camscanner/multiimageedit/model/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)V

    .line 14
    .line 15
    .line 16
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->oO80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private O8〇o(Ljava/util/List;Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, LO〇O/〇080;

    .line 2
    .line 3
    invoke-direct {v0, p0, p2, p3, p1}, LO〇O/〇080;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Ljava/util/List;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O0(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private OOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;)V
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskType;->〇080:I

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇OOo:Z

    .line 7
    .line 8
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->Oo08(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Z)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/model/〇o00〇〇Oo;

    .line 12
    .line 13
    invoke-direct {v0, p0, p2, p1, p3}, Lcom/intsig/camscanner/multiimageedit/model/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;)V

    .line 14
    .line 15
    .line 16
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->oO80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private OOO〇O0(Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Z)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 16
    .param p3    # Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v12, p1

    .line 4
    .line 5
    iget-object v0, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇80〇808〇O()Lcom/intsig/okbinder/ServerInfo;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const-string v13, "MultiImageEditPageManager"

    .line 12
    .line 13
    if-eqz p4, :cond_1

    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO00OOO()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-lez v2, :cond_1

    .line 28
    .line 29
    const/4 v14, 0x1

    .line 30
    const/4 v15, 0x0

    .line 31
    :try_start_0
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 32
    .line 33
    invoke-virtual {v2, v14}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 34
    .line 35
    .line 36
    new-instance v2, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v3, "handleImageProgressClient before executeProgress, executeUuid="

    .line 42
    .line 43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-static {v13, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    move-object v2, v0

    .line 61
    check-cast v2, Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;

    .line 62
    .line 63
    const/4 v6, 0x0

    .line 64
    const/4 v7, 0x0

    .line 65
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 66
    .line 67
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    new-instance v8, Loo/〇〇808〇;

    .line 71
    .line 72
    invoke-direct {v8, v0}, Loo/〇〇808〇;-><init>(Lcom/intsig/camscanner/paper/PaperUtil;)V

    .line 73
    .line 74
    .line 75
    sget-object v0, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇080:Lcom/intsig/camscanner/demoire/DeMoireManager;

    .line 76
    .line 77
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    new-instance v9, Loo/OoO8;

    .line 81
    .line 82
    invoke-direct {v9, v0}, Loo/OoO8;-><init>(Lcom/intsig/camscanner/demoire/DeMoireManager;)V

    .line 83
    .line 84
    .line 85
    sget-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 86
    .line 87
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    .line 89
    .line 90
    new-instance v10, Loo/o800o8O;

    .line 91
    .line 92
    invoke-direct {v10, v0}, Loo/o800o8O;-><init>(Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;)V

    .line 93
    .line 94
    .line 95
    sget-object v0, Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;->〇080:Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;

    .line 96
    .line 97
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    new-instance v11, Loo/〇O888o0o;

    .line 101
    .line 102
    invoke-direct {v11, v0}, Loo/〇O888o0o;-><init>(Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;)V

    .line 103
    .line 104
    .line 105
    move-object/from16 v3, p1

    .line 106
    .line 107
    move-object/from16 v4, p2

    .line 108
    .line 109
    move-object/from16 v5, p3

    .line 110
    .line 111
    invoke-interface/range {v2 .. v11}, Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;->executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;Lcom/intsig/camscanner/imagescanner/EraseMaskAllServerCallback;Lcom/intsig/camscanner/imagescanner/DeMoireWithTrimmedBackBack;Lcom/intsig/camscanner/imagescanner/SuperFilterImageCallback;Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 112
    .line 113
    .line 114
    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 115
    :try_start_1
    const-string v0, "handleImageProgressClient after executeProgress"

    .line 116
    .line 117
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    .line 119
    .line 120
    iget-object v0, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 121
    .line 122
    invoke-virtual {v0, v15}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 123
    .line 124
    .line 125
    return-object v2

    .line 126
    :catchall_0
    move-exception v0

    .line 127
    goto :goto_0

    .line 128
    :catchall_1
    move-exception v0

    .line 129
    move-object/from16 v2, p2

    .line 130
    .line 131
    :goto_0
    :try_start_2
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    .line 133
    .line 134
    sget-object v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇8o8o〇:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;

    .line 135
    .line 136
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;->〇080()Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇O〇()Z

    .line 141
    .line 142
    .line 143
    iget-object v0, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 144
    .line 145
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇〇888()V

    .line 146
    .line 147
    .line 148
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->android_multi_process_no_retry:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 153
    .line 154
    if-ne v0, v14, :cond_0

    .line 155
    .line 156
    iget-object v0, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 157
    .line 158
    invoke-virtual {v0, v15}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 159
    .line 160
    .line 161
    return-object v2

    .line 162
    :cond_0
    iget-object v0, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 163
    .line 164
    invoke-virtual {v0, v15}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 165
    .line 166
    .line 167
    goto :goto_1

    .line 168
    :catchall_2
    move-exception v0

    .line 169
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 170
    .line 171
    invoke-virtual {v2, v15}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 172
    .line 173
    .line 174
    throw v0

    .line 175
    :cond_1
    move-object/from16 v2, p2

    .line 176
    .line 177
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 178
    .line 179
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 180
    .line 181
    .line 182
    const-string v3, "executeProgress: in main process, uuid="

    .line 183
    .line 184
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    .line 196
    .line 197
    const/4 v0, 0x0

    .line 198
    move-object/from16 v3, p3

    .line 199
    .line 200
    invoke-virtual {v2, v12, v3, v0, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;)V

    .line 201
    .line 202
    .line 203
    return-object v2
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o8oO〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic Oo8Oo00oo(Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Ljava/util/List;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇888:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 14
    .line 15
    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 16
    .line 17
    .line 18
    :cond_0
    if-eqz p2, :cond_1

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oo08:I

    .line 27
    .line 28
    invoke-interface {p2, p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;->〇080(II)V

    .line 29
    .line 30
    .line 31
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 32
    .line 33
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oo08:I

    .line 38
    .line 39
    if-ne p1, v0, :cond_3

    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇888:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 42
    .line 43
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-nez p1, :cond_2

    .line 48
    .line 49
    if-eqz p2, :cond_3

    .line 50
    .line 51
    invoke-interface {p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;->〇o〇(Ljava/util/List;)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    if-eqz p2, :cond_3

    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇888:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 58
    .line 59
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    iget p3, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oo08:I

    .line 64
    .line 65
    invoke-interface {p2, p1, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;->〇o00〇〇Oo(II)V

    .line 66
    .line 67
    .line 68
    :cond_3
    :goto_0
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private Ooo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;)V
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskType;->〇o〇:I

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇OOo:Z

    .line 7
    .line 8
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->Oo08(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Z)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/model/〇80〇808〇O;

    .line 12
    .line 13
    invoke-direct {v0, p0, p2, p1, p3}, Lcom/intsig/camscanner/multiimageedit/model/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;)V

    .line 14
    .line 15
    .line 16
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->oO80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oo8Oo00oo(Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic O〇O〇oO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V
    .locals 0

    .line 1
    new-instance p3, Lcom/intsig/camscanner/multiimageedit/model/O8;

    .line 2
    .line 3
    invoke-direct {p3, p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/model/O8;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "TurnLeft"

    .line 7
    .line 8
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o8O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditModelRunnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o0ooO()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagTest2()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->oO80()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 21
    :goto_1
    return v0
.end method

.method private synthetic o8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {v0, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;->〇080(I)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 13
    .line 14
    invoke-virtual {p2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 15
    .line 16
    .line 17
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O888o0o:[B

    .line 18
    .line 19
    monitor-enter p2

    .line 20
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o800o8O:Ljava/util/HashSet;

    .line 21
    .line 22
    invoke-static {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇o00〇〇Oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    monitor-exit p2

    .line 30
    return-void

    .line 31
    :catchall_0
    move-exception p1

    .line 32
    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    throw p1
.end method

.method private o8O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditModelRunnable;)V
    .locals 8

    .line 1
    const-string v0, "MultiImageEditPageManager"

    .line 2
    .line 3
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :catch_0
    move-exception v1

    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 12
    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    :goto_0
    if-nez v1, :cond_1

    .line 16
    .line 17
    new-instance p3, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string p2, " copyMultiImageEditModel == null"

    .line 26
    .line 27
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p2

    .line 34
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇00:Ljava/util/List;

    .line 38
    .line 39
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 40
    .line 41
    .line 42
    move-result-object p2

    .line 43
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 44
    .line 45
    .line 46
    move-result p3

    .line 47
    if-eqz p3, :cond_0

    .line 48
    .line 49
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object p3

    .line 53
    check-cast p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 54
    .line 55
    invoke-interface {p3, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 56
    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_0
    return-void

    .line 60
    :cond_1
    iget v2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 61
    .line 62
    sget v3, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->o〇0:I

    .line 63
    .line 64
    if-ne v2, v3, :cond_2

    .line 65
    .line 66
    const-string p1, "multiImageEditModel.imageStatus delete"

    .line 67
    .line 68
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    return-void

    .line 72
    :cond_2
    sget v2, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->Oo08:I

    .line 73
    .line 74
    iput v2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 75
    .line 76
    const/4 v2, 0x1

    .line 77
    iput-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇80〇808〇O:Z

    .line 78
    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 80
    .line 81
    .line 82
    move-result-wide v3

    .line 83
    new-instance v5, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v6, " handleImage start copyMultiImageEditModel="

    .line 92
    .line 93
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v5

    .line 103
    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    const/4 v5, 0x0

    .line 107
    if-eqz p3, :cond_3

    .line 108
    .line 109
    invoke-interface {p3, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditModelRunnable;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 110
    .line 111
    .line 112
    move-result-object p3

    .line 113
    if-eqz p3, :cond_3

    .line 114
    .line 115
    invoke-virtual {p3}, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk()Z

    .line 116
    .line 117
    .line 118
    move-result v2

    .line 119
    invoke-virtual {p3}, Lcom/intsig/camscanner/util/ImageProgressClient;->getServerHandleImageErrorCode()I

    .line 120
    .line 121
    .line 122
    move-result p3

    .line 123
    goto :goto_2

    .line 124
    :cond_3
    const/4 p3, 0x0

    .line 125
    :goto_2
    iput-boolean v5, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇80〇808〇O:Z

    .line 126
    .line 127
    new-instance v5, Ljava/lang/StringBuilder;

    .line 128
    .line 129
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .line 131
    .line 132
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    const-string v6, " handleImage end costTime="

    .line 136
    .line 137
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 141
    .line 142
    .line 143
    move-result-wide v6

    .line 144
    sub-long/2addr v6, v3

    .line 145
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v3

    .line 152
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 156
    .line 157
    .line 158
    move-result v3

    .line 159
    const-string v4, " copyMultiImageEditModel="

    .line 160
    .line 161
    if-eqz v3, :cond_7

    .line 162
    .line 163
    iget-boolean v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 164
    .line 165
    iput-boolean v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 166
    .line 167
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 168
    .line 169
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 170
    .line 171
    .line 172
    move-result v3

    .line 173
    if-nez v3, :cond_6

    .line 174
    .line 175
    if-nez v2, :cond_6

    .line 176
    .line 177
    const v2, 0x98e4a6

    .line 178
    .line 179
    .line 180
    if-ne p3, v2, :cond_5

    .line 181
    .line 182
    const-string p3, "error code = 10020006; we delete current"

    .line 183
    .line 184
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 188
    .line 189
    if-eqz p3, :cond_8

    .line 190
    .line 191
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 192
    .line 193
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 194
    .line 195
    .line 196
    move-result p3

    .line 197
    if-nez p3, :cond_8

    .line 198
    .line 199
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 200
    .line 201
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 202
    .line 203
    .line 204
    move-result-object p3

    .line 205
    :cond_4
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    .line 206
    .line 207
    .line 208
    move-result v2

    .line 209
    if-eqz v2, :cond_8

    .line 210
    .line 211
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 212
    .line 213
    .line 214
    move-result-object v2

    .line 215
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 216
    .line 217
    if-eqz v2, :cond_4

    .line 218
    .line 219
    iget-object v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 220
    .line 221
    if-eqz v3, :cond_4

    .line 222
    .line 223
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 224
    .line 225
    iget-object v5, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 226
    .line 227
    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 228
    .line 229
    .line 230
    move-result v3

    .line 231
    if-eqz v3, :cond_4

    .line 232
    .line 233
    new-instance p3, Ljava/lang/StringBuilder;

    .line 234
    .line 235
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    .line 237
    .line 238
    const-string v3, "really delete uuid:"

    .line 239
    .line 240
    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    .line 242
    .line 243
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 244
    .line 245
    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    .line 247
    .line 248
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 249
    .line 250
    .line 251
    move-result-object p3

    .line 252
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    .line 254
    .line 255
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇oo〇:Landroidx/lifecycle/MutableLiveData;

    .line 256
    .line 257
    invoke-virtual {p3, v2}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 258
    .line 259
    .line 260
    goto :goto_3

    .line 261
    :cond_5
    sget p3, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇〇888:I

    .line 262
    .line 263
    iput p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 264
    .line 265
    goto :goto_3

    .line 266
    :cond_6
    sget p3, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇080:I

    .line 267
    .line 268
    iput p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 269
    .line 270
    goto :goto_3

    .line 271
    :cond_7
    new-instance p3, Ljava/lang/StringBuilder;

    .line 272
    .line 273
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    .line 275
    .line 276
    const-string v2, "multiImageEditModel="

    .line 277
    .line 278
    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    .line 280
    .line 281
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    .line 283
    .line 284
    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    .line 286
    .line 287
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 288
    .line 289
    .line 290
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 291
    .line 292
    .line 293
    move-result-object p3

    .line 294
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    .line 296
    .line 297
    :cond_8
    :goto_3
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->ooO:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 298
    .line 299
    if-eqz p3, :cond_9

    .line 300
    .line 301
    invoke-interface {p3, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 302
    .line 303
    .line 304
    :cond_9
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇00:Ljava/util/List;

    .line 305
    .line 306
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 307
    .line 308
    .line 309
    move-result-object p3

    .line 310
    :goto_4
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    .line 311
    .line 312
    .line 313
    move-result v2

    .line 314
    if-eqz v2, :cond_a

    .line 315
    .line 316
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 317
    .line 318
    .line 319
    move-result-object v2

    .line 320
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 321
    .line 322
    invoke-interface {v2, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 323
    .line 324
    .line 325
    new-instance v2, Ljava/lang/StringBuilder;

    .line 326
    .line 327
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    .line 332
    .line 333
    const-string v3, " multiImageEditModel="

    .line 334
    .line 335
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    .line 337
    .line 338
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 339
    .line 340
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    .line 342
    .line 343
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    .line 345
    .line 346
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 347
    .line 348
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    .line 350
    .line 351
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 352
    .line 353
    .line 354
    move-result-object v2

    .line 355
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    .line 357
    .line 358
    goto :goto_4

    .line 359
    :cond_a
    return-void
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private synthetic o8oO〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 7

    .line 1
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    add-int/lit16 v0, v0, 0x168

    .line 4
    .line 5
    add-int/lit8 v0, v0, 0x5a

    .line 6
    .line 7
    rem-int/lit16 v0, v0, 0x168

    .line 8
    .line 9
    iput v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 10
    .line 11
    iput v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 12
    .line 13
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    iget-object v1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 22
    .line 23
    const/16 v2, 0x5a

    .line 24
    .line 25
    const/high16 v3, 0x3f800000    # 1.0f

    .line 26
    .line 27
    const/16 v4, 0x50

    .line 28
    .line 29
    const/4 v5, 0x0

    .line 30
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 31
    .line 32
    .line 33
    move-result v6

    .line 34
    move-object v0, p0

    .line 35
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 36
    .line 37
    .line 38
    :cond_0
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 39
    .line 40
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    if-eqz p1, :cond_1

    .line 45
    .line 46
    iget-object v1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 47
    .line 48
    const/16 v2, 0x5a

    .line 49
    .line 50
    const/high16 v3, 0x3f800000    # 1.0f

    .line 51
    .line 52
    const/16 v4, 0x50

    .line 53
    .line 54
    const/4 v5, 0x0

    .line 55
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 56
    .line 57
    .line 58
    move-result v6

    .line 59
    move-object v0, p0

    .line 60
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 61
    .line 62
    .line 63
    :cond_1
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 64
    .line 65
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 66
    .line 67
    .line 68
    move-result p1

    .line 69
    if-eqz p1, :cond_2

    .line 70
    .line 71
    iget-object v1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 72
    .line 73
    const/16 v2, 0x5a

    .line 74
    .line 75
    const/high16 v3, 0x3f800000    # 1.0f

    .line 76
    .line 77
    const/16 v4, 0x50

    .line 78
    .line 79
    const/4 v5, 0x0

    .line 80
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 81
    .line 82
    .line 83
    move-result v6

    .line 84
    move-object v0, p0

    .line 85
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 86
    .line 87
    .line 88
    :cond_2
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 89
    .line 90
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 91
    .line 92
    .line 93
    move-result p1

    .line 94
    if-eqz p1, :cond_3

    .line 95
    .line 96
    iget-object v1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 97
    .line 98
    const/16 v2, 0x5a

    .line 99
    .line 100
    const/high16 v3, 0x3f800000    # 1.0f

    .line 101
    .line 102
    const/16 v4, 0x50

    .line 103
    .line 104
    const/4 v5, 0x0

    .line 105
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 106
    .line 107
    .line 108
    move-result v6

    .line 109
    move-object v0, p0

    .line 110
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 111
    .line 112
    .line 113
    :cond_3
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    if-eqz p1, :cond_4

    .line 122
    .line 123
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    const/16 v2, 0x5a

    .line 128
    .line 129
    const/high16 v3, 0x3f800000    # 1.0f

    .line 130
    .line 131
    const/16 v4, 0x50

    .line 132
    .line 133
    const/4 v5, 0x0

    .line 134
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 135
    .line 136
    .line 137
    move-result v6

    .line 138
    move-object v0, p0

    .line 139
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 140
    .line 141
    .line 142
    :cond_4
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇888()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 147
    .line 148
    .line 149
    move-result p1

    .line 150
    if-eqz p1, :cond_5

    .line 151
    .line 152
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇888()Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v1

    .line 156
    const/16 v2, 0x10e

    .line 157
    .line 158
    const/high16 v3, 0x3f800000    # 1.0f

    .line 159
    .line 160
    const/16 v4, 0x50

    .line 161
    .line 162
    const/4 v5, 0x0

    .line 163
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 164
    .line 165
    .line 166
    move-result v6

    .line 167
    move-object v0, p0

    .line 168
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 169
    .line 170
    .line 171
    :cond_5
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇80〇808〇O()Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 176
    .line 177
    .line 178
    move-result p1

    .line 179
    if-eqz p1, :cond_6

    .line 180
    .line 181
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇80〇808〇O()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v1

    .line 185
    const/16 v2, 0x10e

    .line 186
    .line 187
    const/high16 v3, 0x3f800000    # 1.0f

    .line 188
    .line 189
    const/16 v4, 0x50

    .line 190
    .line 191
    const/4 v5, 0x0

    .line 192
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 193
    .line 194
    .line 195
    move-result v6

    .line 196
    move-object v0, p0

    .line 197
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 198
    .line 199
    .line 200
    :cond_6
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->ooOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 201
    .line 202
    .line 203
    const/4 p1, 0x0

    .line 204
    return-object p1
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private synthetic oO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;I)V
    .locals 7

    .line 1
    new-instance v6, Lcom/intsig/camscanner/multiimageedit/model/o〇0;

    .line 2
    .line 3
    move-object v0, v6

    .line 4
    move-object v1, p0

    .line 5
    move v2, p4

    .line 6
    move-object v3, p2

    .line 7
    move-object v4, p1

    .line 8
    move-object v5, p3

    .line 9
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/multiimageedit/model/o〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;)V

    .line 10
    .line 11
    .line 12
    const-string p2, "Enhance"

    .line 13
    .line 14
    invoke-direct {p0, p1, p2, v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o8O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditModelRunnable;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic oO00OOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)Lkotlin/Unit;
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {v0, p1}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 21
    .line 22
    .line 23
    :cond_0
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 24
    .line 25
    invoke-direct {p0, p2, p1, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8〇o(Ljava/util/List;Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 26
    .line 27
    .line 28
    const/4 p1, 0x0

    .line 29
    return-object p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇〇0〇〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private ooo〇8oO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskType;->〇o00〇〇Oo:I

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇OOo:Z

    .line 7
    .line 8
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->Oo08(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Z)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/model/oO80;

    .line 12
    .line 13
    invoke-direct {v0, p0, p2, p1}, Lcom/intsig/camscanner/multiimageedit/model/oO80;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)V

    .line 14
    .line 15
    .line 16
    invoke-static {p1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->oO80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oo〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o800o8O:Ljava/util/HashSet;

    .line 8
    .line 9
    invoke-static {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇o00〇〇Oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const-wide/16 v0, 0x64

    .line 20
    .line 21
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :catch_0
    move-exception v0

    .line 26
    const-string v1, "MultiImageEditPageManager"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O888o0o:[B

    .line 40
    .line 41
    monitor-enter v0

    .line 42
    :try_start_1
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o800o8O:Ljava/util/HashSet;

    .line 43
    .line 44
    invoke-static {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇o00〇〇Oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :try_start_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 55
    .line 56
    .line 57
    goto :goto_1

    .line 58
    :catch_1
    move-exception v0

    .line 59
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 64
    .line 65
    .line 66
    const-string v1, "MultiImageEditPageManager"

    .line 67
    .line 68
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    .line 70
    .line 71
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇00()Ljava/util/concurrent/ExecutorService;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/model/〇〇888;

    .line 76
    .line 77
    invoke-direct {v1, p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/model/〇〇888;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 78
    .line 79
    .line 80
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 81
    .line 82
    .line 83
    goto :goto_2

    .line 84
    :catchall_0
    move-exception p1

    .line 85
    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 86
    throw p1

    .line 87
    :cond_1
    :goto_2
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Ljava/lang/Integer;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O000(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Ljava/lang/Integer;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic o〇0OOo〇0(Landroid/os/Message;)Z
    .locals 6

    .line 1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    return v2

    .line 9
    :cond_0
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 10
    .line 11
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O888o0o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    iget p1, p1, Landroid/os/Message;->what:I

    .line 16
    .line 17
    const/4 v3, 0x1

    .line 18
    const-string v4, "MultiImageEditPageManager"

    .line 19
    .line 20
    packed-switch p1, :pswitch_data_0

    .line 21
    .line 22
    .line 23
    const-string p1, "default"

    .line 24
    .line 25
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return v2

    .line 29
    :pswitch_0
    iget-object p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 30
    .line 31
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    xor-int/2addr p1, v3

    .line 36
    if-nez p1, :cond_1

    .line 37
    .line 38
    const-string p1, "tempSmallOnlyTrimImagePath right"

    .line 39
    .line 40
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O0o〇〇Oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 44
    .line 45
    .line 46
    goto/16 :goto_2

    .line 47
    .line 48
    :cond_1
    const-string p1, "whole ImagePath right"

    .line 49
    .line 50
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iget p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 54
    .line 55
    add-int/lit16 p1, p1, 0x168

    .line 56
    .line 57
    add-int/lit8 p1, p1, 0x5a

    .line 58
    .line 59
    rem-int/lit16 p1, p1, 0x168

    .line 60
    .line 61
    iput p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 62
    .line 63
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇80o08O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 64
    .line 65
    .line 66
    goto/16 :goto_2

    .line 67
    .line 68
    :pswitch_1
    new-instance p1, LO〇O/〇〇888;

    .line 69
    .line 70
    invoke-direct {p1, p0, v0}, LO〇O/〇〇888;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 71
    .line 72
    .line 73
    iget-object v5, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 74
    .line 75
    invoke-static {v5}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    if-eqz v5, :cond_2

    .line 80
    .line 81
    const-string v5, "tempSmallOnlyTrimImagePath enhance"

    .line 82
    .line 83
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-direct {p0, v1, v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Ooo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;)V

    .line 87
    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_2
    const-string v5, "whole ImagePath enhance"

    .line 91
    .line 92
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    invoke-direct {p0, v1, v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;)V

    .line 96
    .line 97
    .line 98
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇8o8o〇:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 99
    .line 100
    invoke-virtual {p1, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 101
    .line 102
    .line 103
    move-result p1

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .line 108
    .line 109
    const-string v5, "removeSameObject="

    .line 110
    .line 111
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    goto :goto_2

    .line 125
    :pswitch_2
    iget-object p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 126
    .line 127
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 128
    .line 129
    .line 130
    move-result p1

    .line 131
    if-eqz p1, :cond_3

    .line 132
    .line 133
    iget-object p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 134
    .line 135
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 136
    .line 137
    .line 138
    move-result p1

    .line 139
    goto :goto_1

    .line 140
    :cond_3
    iget-object p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 141
    .line 142
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 143
    .line 144
    .line 145
    move-result p1

    .line 146
    :goto_1
    xor-int/2addr p1, v3

    .line 147
    if-nez p1, :cond_4

    .line 148
    .line 149
    const-string p1, "tempSmallOnlyTrimImagePath left"

    .line 150
    .line 151
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->ooo〇8oO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 155
    .line 156
    .line 157
    goto :goto_2

    .line 158
    :cond_4
    const-string p1, "whole ImagePath left"

    .line 159
    .line 160
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    iget p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 164
    .line 165
    add-int/lit16 p1, p1, 0x168

    .line 166
    .line 167
    add-int/lit16 p1, p1, 0x10e

    .line 168
    .line 169
    rem-int/lit16 p1, p1, 0x168

    .line 170
    .line 171
    iput p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 172
    .line 173
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇80o08O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 174
    .line 175
    .line 176
    goto :goto_2

    .line 177
    :pswitch_3
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇80o08O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 178
    .line 179
    .line 180
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇8o8o〇:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 181
    .line 182
    invoke-virtual {p1, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 183
    .line 184
    .line 185
    move-result p1

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    .line 187
    .line 188
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    .line 190
    .line 191
    const-string v5, "HANDLE_ALL before exist="

    .line 192
    .line 193
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object p1

    .line 203
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    .line 205
    .line 206
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇8o8o〇:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 207
    .line 208
    invoke-virtual {p1, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 209
    .line 210
    .line 211
    iput-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇808〇:Z

    .line 212
    .line 213
    return v3

    .line 214
    nop

    .line 215
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private static synthetic o〇8(Ljava/util/List;Z)V
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-eqz v2, :cond_1

    .line 15
    .line 16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 21
    .line 22
    if-nez v2, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 26
    .line 27
    iget-wide v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 28
    .line 29
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 38
    .line 39
    invoke-static {v1, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->O880oOO08(Landroid/content/Context;Ljava/util/List;)Ljava/util/Map;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v2, "discardAllData onlyTemp:"

    .line 49
    .line 50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v2, " imageMap:"

    .line 57
    .line 58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    const-string v2, "MultiImageEditPageManager"

    .line 69
    .line 70
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-eqz v1, :cond_5

    .line 82
    .line 83
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 88
    .line 89
    if-nez v1, :cond_2

    .line 90
    .line 91
    const-string v1, "discardAllData multiImageEditPage == null"

    .line 92
    .line 93
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    goto :goto_1

    .line 97
    :cond_2
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 98
    .line 99
    iget-wide v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 100
    .line 101
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    .line 107
    .line 108
    move-result-object v3

    .line 109
    check-cast v3, Ljava/lang/Integer;

    .line 110
    .line 111
    if-nez p1, :cond_4

    .line 112
    .line 113
    if-eqz v3, :cond_3

    .line 114
    .line 115
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 116
    .line 117
    .line 118
    move-result v3

    .line 119
    const/4 v4, 0x2

    .line 120
    if-eq v3, v4, :cond_3

    .line 121
    .line 122
    goto :goto_2

    .line 123
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇080()V

    .line 124
    .line 125
    .line 126
    goto :goto_1

    .line 127
    :cond_4
    :goto_2
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o00〇〇Oo()V

    .line 128
    .line 129
    .line 130
    goto :goto_1

    .line 131
    :cond_5
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic o〇8oOO88(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V
    .locals 0

    .line 1
    new-instance p3, Lcom/intsig/camscanner/multiimageedit/model/Oo08;

    .line 2
    .line 3
    invoke-direct {p3, p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/model/Oo08;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "TurnLeft"

    .line 7
    .line 8
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o8O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditModelRunnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static synthetic o〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)I
    .locals 5

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)J

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    cmp-long v4, v0, v2

    .line 10
    .line 11
    if-lez v4, :cond_0

    .line 12
    .line 13
    const/4 p0, -0x1

    .line 14
    return p0

    .line 15
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)J

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    invoke-static {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)J

    .line 20
    .line 21
    .line 22
    move-result-wide p0

    .line 23
    cmp-long v2, v0, p0

    .line 24
    .line 25
    if-gez v2, :cond_1

    .line 26
    .line 27
    const/4 p0, 0x1

    .line 28
    return p0

    .line 29
    :cond_1
    const/4 p0, 0x0

    .line 30
    return p0
    .line 31
    .line 32
    .line 33
.end method

.method private 〇00()Ljava/util/concurrent/ExecutorService;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OoO8:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x3

    .line 6
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 11
    .line 12
    const-wide/16 v1, 0x3c

    .line 13
    .line 14
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 15
    .line 16
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/ThreadPoolExecutor;->setKeepAliveTime(JLjava/util/concurrent/TimeUnit;)V

    .line 17
    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OoO8:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 24
    .line 25
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OoO8:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 26
    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇000O0(Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;",
            ">;",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;",
            ")V"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_6

    .line 2
    .line 3
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto/16 :goto_2

    .line 10
    .line 11
    :cond_0
    if-eqz p3, :cond_1

    .line 12
    .line 13
    invoke-interface {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;->onStart()V

    .line 14
    .line 15
    .line 16
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oo08:I

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇0:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇888:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 31
    .line 32
    .line 33
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_5

    .line 42
    .line 43
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 48
    .line 49
    iget v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 50
    .line 51
    new-instance v2, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    const-string v3, "whitePadHandle --- status="

    .line 57
    .line 58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    const-string v3, "MultiImageEditPageManager"

    .line 69
    .line 70
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    sget v2, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇080:I

    .line 74
    .line 75
    if-ne v1, v2, :cond_2

    .line 76
    .line 77
    invoke-direct {p0, p1, v0, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O0O8OO088(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_2
    sget v2, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->O8:I

    .line 82
    .line 83
    if-eq v1, v2, :cond_4

    .line 84
    .line 85
    sget v2, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->Oo08:I

    .line 86
    .line 87
    if-ne v1, v2, :cond_3

    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 91
    .line 92
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .line 94
    .line 95
    const-string v2, "whitePadHandle  pad unknown error status="

    .line 96
    .line 97
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 111
    .line 112
    invoke-direct {p0, p1, v0, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8〇o(Ljava/util/List;Ljava/lang/Boolean;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 113
    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_4
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    .line 117
    .line 118
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .line 120
    .line 121
    const-string v2, "whitePadHandle listener for id="

    .line 122
    .line 123
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    iget-object v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 127
    .line 128
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    new-instance v1, LO〇O/oO80;

    .line 139
    .line 140
    invoke-direct {v1, p0, v0, p1, p3}, LO〇O/oO80;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 141
    .line 142
    .line 143
    iput-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->ooO:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 144
    .line 145
    goto :goto_0

    .line 146
    :cond_5
    return-void

    .line 147
    :cond_6
    :goto_2
    if-eqz p3, :cond_7

    .line 148
    .line 149
    invoke-interface {p3, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;->〇o〇(Ljava/util/List;)V

    .line 150
    .line 151
    .line 152
    :cond_7
    return-void
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private 〇00O0O0(J)V
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception p1

    .line 6
    const-string p2, "MultiImageEditPageManager"

    .line 7
    .line 8
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
.end method

.method private 〇00〇8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O8o08O:Landroid/os/HandlerThread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇:Landroid/os/Handler;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O00:[B

    .line 11
    .line 12
    monitor-enter v0

    .line 13
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O8o08O:Landroid/os/HandlerThread;

    .line 14
    .line 15
    if-nez v1, :cond_1

    .line 16
    .line 17
    new-instance v1, Landroid/os/HandlerThread;

    .line 18
    .line 19
    const-string v2, "MultiImageEdit Thread"

    .line 20
    .line 21
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O8o08O:Landroid/os/HandlerThread;

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 27
    .line 28
    .line 29
    new-instance v1, Landroid/os/Handler;

    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O8o08O:Landroid/os/HandlerThread;

    .line 32
    .line 33
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    new-instance v3, LO〇O/Oo08;

    .line 38
    .line 39
    invoke-direct {v3, p0}, LO〇O/Oo08;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 43
    .line 44
    .line 45
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇:Landroid/os/Handler;

    .line 46
    .line 47
    :cond_1
    monitor-exit v0

    .line 48
    return-void

    .line 49
    :catchall_0
    move-exception v1

    .line 50
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    throw v1
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇08O8o〇0(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/util/ImageProgressClient;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->reset()Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setThreadContext(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setEncodeImageSMoz(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    iget-object v2, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 20
    .line 21
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSrcImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    iget-object v2, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 26
    .line 27
    invoke-static {v2}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRawImageSize([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->enableTrim(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-direct {p0, p5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇oOO8O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setPadPath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    iget v1, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 48
    .line 49
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageEnhanceMode(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    iget v1, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O0O:I

    .line 54
    .line 55
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setBrightness(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    iget v1, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇08O:I

    .line 60
    .line 61
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setContrast(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    iget v1, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8oOOo:I

    .line 66
    .line 67
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setDetail(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    const/4 v1, 0x1

    .line 72
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSuperFilterTrace(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    iget-object v1, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 77
    .line 78
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSaveImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    invoke-static {}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->getNeedTrimWhenNoBorder()Z

    .line 83
    .line 84
    .line 85
    move-result v1

    .line 86
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropWhenNoBorder(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    sget-object v1, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->INSTANCE:Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;

    .line 91
    .line 92
    invoke-virtual {v1}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->isCropDewrapOn()Z

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropDewrap(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 97
    .line 98
    .line 99
    new-instance p1, Ljava/lang/StringBuilder;

    .line 100
    .line 101
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .line 103
    .line 104
    const-string v1, "MULTI_PREVIEW_ENHANCE_"

    .line 105
    .line 106
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    iget-object v1, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 110
    .line 111
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    const/4 v1, 0x0

    .line 119
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 120
    .line 121
    .line 122
    move-result p2

    .line 123
    invoke-direct {p0, p1, v0, v1, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OOO〇O0(Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->getServerFilterRes()I

    .line 128
    .line 129
    .line 130
    move-result p2

    .line 131
    if-eqz p2, :cond_0

    .line 132
    .line 133
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->getEnhanceMode()I

    .line 134
    .line 135
    .line 136
    move-result p2

    .line 137
    iput p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 138
    .line 139
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->getEnhanceMode()I

    .line 140
    .line 141
    .line 142
    move-result p2

    .line 143
    iput p2, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 144
    .line 145
    if-eqz p4, :cond_0

    .line 146
    .line 147
    invoke-interface {p4}, Lcom/intsig/callback/Callback0;->call()V

    .line 148
    .line 149
    .line 150
    :cond_0
    invoke-virtual {p0, p5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->ooOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 151
    .line 152
    .line 153
    return-object p1
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O08000(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private synthetic 〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 7

    .line 1
    iget-object v0, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0OoOOo0:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->isCurrentWaiting()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o0ooO()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$1;

    .line 18
    .line 19
    invoke-direct {v0, p0, p5, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$1;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 20
    .line 21
    .line 22
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 23
    .line 24
    .line 25
    move-result-wide v1

    .line 26
    new-instance v3, Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 27
    .line 28
    invoke-direct {v3}, Lcom/intsig/camscanner/util/ImageProgressClient;-><init>()V

    .line 29
    .line 30
    .line 31
    iget-boolean v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇08O〇00〇o:Z

    .line 32
    .line 33
    const/4 v5, 0x0

    .line 34
    if-nez v4, :cond_1

    .line 35
    .line 36
    iget v4, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8:I

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_1
    const/4 v4, 0x0

    .line 40
    :goto_1
    invoke-virtual {v3}, Lcom/intsig/camscanner/util/ImageProgressClient;->reset()Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 41
    .line 42
    .line 43
    move-result-object v6

    .line 44
    invoke-virtual {v6, p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->setThreadContext(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    invoke-virtual {p2, v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->setEncodeImageSMoz(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    invoke-direct {p0, p5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇oOO8O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v5

    .line 56
    invoke-virtual {p2, v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->setPadPath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 57
    .line 58
    .line 59
    move-result-object p2

    .line 60
    iget-object v5, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 61
    .line 62
    invoke-virtual {p2, v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSrcImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 63
    .line 64
    .line 65
    move-result-object p2

    .line 66
    iget-object v5, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 67
    .line 68
    const/4 v6, 0x1

    .line 69
    invoke-static {v5, v6}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 70
    .line 71
    .line 72
    move-result-object v5

    .line 73
    invoke-virtual {p2, v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRawImageSize([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    iget-boolean v5, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 78
    .line 79
    invoke-virtual {p2, v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->enableTrim(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    iget-object v5, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 84
    .line 85
    invoke-virtual {p2, v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSaveOnlyTrimImage(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 86
    .line 87
    .line 88
    move-result-object p2

    .line 89
    iget-object v5, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 90
    .line 91
    invoke-virtual {p2, v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageBorder([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 92
    .line 93
    .line 94
    move-result-object p2

    .line 95
    iget-boolean v5, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇oO:Z

    .line 96
    .line 97
    invoke-virtual {p2, v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedDetectBorder(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setTrimImageMaxSide(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 102
    .line 103
    .line 104
    move-result-object p2

    .line 105
    iget v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 106
    .line 107
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRation(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 108
    .line 109
    .line 110
    move-result-object p2

    .line 111
    iget v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 112
    .line 113
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageEnhanceMode(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    iget v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O0O:I

    .line 118
    .line 119
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setBrightness(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 120
    .line 121
    .line 122
    move-result-object p2

    .line 123
    invoke-virtual {p5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇808〇()Z

    .line 124
    .line 125
    .line 126
    move-result v4

    .line 127
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedDeBlurImage(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 128
    .line 129
    .line 130
    move-result-object p2

    .line 131
    invoke-virtual {p5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->Oooo8o0〇()Z

    .line 132
    .line 133
    .line 134
    move-result v4

    .line 135
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropDewrap(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 136
    .line 137
    .line 138
    move-result-object p2

    .line 139
    invoke-static {}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->getNeedTrimWhenNoBorder()Z

    .line 140
    .line 141
    .line 142
    move-result v4

    .line 143
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropWhenNoBorder(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 144
    .line 145
    .line 146
    move-result-object p2

    .line 147
    iget v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇08O:I

    .line 148
    .line 149
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setContrast(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 150
    .line 151
    .line 152
    move-result-object p2

    .line 153
    iget v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8oOOo:I

    .line 154
    .line 155
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setDetail(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 156
    .line 157
    .line 158
    move-result-object p2

    .line 159
    invoke-virtual {p2, v6}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSuperFilterTrace(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 160
    .line 161
    .line 162
    move-result-object p2

    .line 163
    iget-object v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 164
    .line 165
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSaveImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 166
    .line 167
    .line 168
    move-result-object p2

    .line 169
    iget-boolean v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8o:Z

    .line 170
    .line 171
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setEnableAutoFindRotation(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 172
    .line 173
    .line 174
    move-result-object p2

    .line 175
    iget-object v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 176
    .line 177
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setTrimmedPaperPath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 178
    .line 179
    .line 180
    new-instance p2, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    const-string v4, "MULTI_PREVIEW_ALL_"

    .line 186
    .line 187
    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    iget-object v4, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 191
    .line 192
    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object p2

    .line 199
    invoke-static {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 200
    .line 201
    .line 202
    move-result p3

    .line 203
    invoke-direct {p0, p2, v3, v0, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OOO〇O0(Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 204
    .line 205
    .line 206
    move-result-object p2

    .line 207
    iget-boolean p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 208
    .line 209
    if-eqz p3, :cond_2

    .line 210
    .line 211
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 212
    .line 213
    if-nez p3, :cond_2

    .line 214
    .line 215
    iget-boolean p3, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 216
    .line 217
    if-eqz p3, :cond_2

    .line 218
    .line 219
    iget-object p3, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 220
    .line 221
    if-nez p3, :cond_2

    .line 222
    .line 223
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->getBorder()[I

    .line 224
    .line 225
    .line 226
    move-result-object p3

    .line 227
    if-eqz p3, :cond_2

    .line 228
    .line 229
    iput-object p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 230
    .line 231
    array-length v0, p3

    .line 232
    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([II)[I

    .line 233
    .line 234
    .line 235
    move-result-object p3

    .line 236
    iput-object p3, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 237
    .line 238
    :cond_2
    iget-boolean p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8o:Z

    .line 239
    .line 240
    const-string v0, "MultiImageEditPageManager"

    .line 241
    .line 242
    if-eqz p3, :cond_3

    .line 243
    .line 244
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->getRotation()I

    .line 245
    .line 246
    .line 247
    move-result p3

    .line 248
    iget v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 249
    .line 250
    if-eq p3, v3, :cond_3

    .line 251
    .line 252
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->getRotation()I

    .line 253
    .line 254
    .line 255
    move-result p3

    .line 256
    iget v3, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 257
    .line 258
    if-eq p3, v3, :cond_3

    .line 259
    .line 260
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->getRotation()I

    .line 261
    .line 262
    .line 263
    move-result p3

    .line 264
    new-instance v3, Ljava/lang/StringBuilder;

    .line 265
    .line 266
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 267
    .line 268
    .line 269
    const-string v4, "tempRotation = "

    .line 270
    .line 271
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    .line 273
    .line 274
    iget v4, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 275
    .line 276
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 277
    .line 278
    .line 279
    const-string v4, " -> "

    .line 280
    .line 281
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    .line 283
    .line 284
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 285
    .line 286
    .line 287
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 288
    .line 289
    .line 290
    move-result-object v3

    .line 291
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    .line 293
    .line 294
    iput p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 295
    .line 296
    iput p3, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 297
    .line 298
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->getServerFilterRes()I

    .line 299
    .line 300
    .line 301
    move-result p3

    .line 302
    if-eqz p3, :cond_4

    .line 303
    .line 304
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->getEnhanceMode()I

    .line 305
    .line 306
    .line 307
    move-result p3

    .line 308
    iput p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 309
    .line 310
    invoke-virtual {p2}, Lcom/intsig/camscanner/util/ImageProgressClient;->getEnhanceMode()I

    .line 311
    .line 312
    .line 313
    move-result p3

    .line 314
    iput p3, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 315
    .line 316
    if-eqz p4, :cond_4

    .line 317
    .line 318
    invoke-interface {p4}, Lcom/intsig/callback/Callback0;->call()V

    .line 319
    .line 320
    .line 321
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 322
    .line 323
    .line 324
    move-result-wide p3

    .line 325
    sub-long/2addr p3, v1

    .line 326
    iget-object v3, p5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 327
    .line 328
    invoke-static {v3}, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇〇808〇(Ljava/lang/String;)I

    .line 329
    .line 330
    .line 331
    move-result v3

    .line 332
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OoO8(I)V

    .line 333
    .line 334
    .line 335
    invoke-virtual {p5, v3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OoO8(I)V

    .line 336
    .line 337
    .line 338
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 339
    .line 340
    .line 341
    move-result-wide v3

    .line 342
    sub-long/2addr v3, v1

    .line 343
    sub-long/2addr v3, p3

    .line 344
    new-instance p1, Ljava/lang/StringBuilder;

    .line 345
    .line 346
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 347
    .line 348
    .line 349
    const-string v1, "image use time = "

    .line 350
    .line 351
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    .line 353
    .line 354
    invoke-virtual {p1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 355
    .line 356
    .line 357
    const-string p3, " imageQualityTime = "

    .line 358
    .line 359
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    .line 361
    .line 362
    invoke-virtual {p1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 363
    .line 364
    .line 365
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 366
    .line 367
    .line 368
    move-result-object p1

    .line 369
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    .line 371
    .line 372
    invoke-virtual {p0, p5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->ooOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 373
    .line 374
    .line 375
    return-object p2
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
.end method

.method private synthetic 〇80(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "whitePadHandle listener back for id="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, " status= "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const-string v1, "MultiImageEditPageManager"

    .line 31
    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    const/4 v0, 0x0

    .line 36
    iput-object v0, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->ooO:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 37
    .line 38
    invoke-direct {p0, p2, p1, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O0O8OO088(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇80〇808〇O(Ljava/util/List;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇8(Ljava/util/List;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇8〇0〇o〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 7

    .line 1
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    add-int/lit16 v0, v0, 0x168

    .line 4
    .line 5
    add-int/lit16 v0, v0, 0x10e

    .line 6
    .line 7
    rem-int/lit16 v0, v0, 0x168

    .line 8
    .line 9
    iput v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 10
    .line 11
    iput v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 12
    .line 13
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    iget-object v1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 22
    .line 23
    const/16 v2, 0x10e

    .line 24
    .line 25
    const/high16 v3, 0x3f800000    # 1.0f

    .line 26
    .line 27
    const/16 v4, 0x50

    .line 28
    .line 29
    const/4 v5, 0x0

    .line 30
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 31
    .line 32
    .line 33
    move-result v6

    .line 34
    move-object v0, p0

    .line 35
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 36
    .line 37
    .line 38
    :cond_0
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 39
    .line 40
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    if-eqz p1, :cond_1

    .line 45
    .line 46
    iget-object v1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 47
    .line 48
    const/16 v2, 0x10e

    .line 49
    .line 50
    const/high16 v3, 0x3f800000    # 1.0f

    .line 51
    .line 52
    const/16 v4, 0x50

    .line 53
    .line 54
    const/4 v5, 0x0

    .line 55
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 56
    .line 57
    .line 58
    move-result v6

    .line 59
    move-object v0, p0

    .line 60
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 61
    .line 62
    .line 63
    :cond_1
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 64
    .line 65
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 66
    .line 67
    .line 68
    move-result p1

    .line 69
    if-eqz p1, :cond_2

    .line 70
    .line 71
    iget-object v1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 72
    .line 73
    const/16 v2, 0x10e

    .line 74
    .line 75
    const/high16 v3, 0x3f800000    # 1.0f

    .line 76
    .line 77
    const/16 v4, 0x50

    .line 78
    .line 79
    const/4 v5, 0x0

    .line 80
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 81
    .line 82
    .line 83
    move-result v6

    .line 84
    move-object v0, p0

    .line 85
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 86
    .line 87
    .line 88
    :cond_2
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 93
    .line 94
    .line 95
    move-result p1

    .line 96
    if-eqz p1, :cond_3

    .line 97
    .line 98
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    const/16 v2, 0x5a

    .line 103
    .line 104
    const/high16 v3, 0x3f800000    # 1.0f

    .line 105
    .line 106
    const/16 v4, 0x50

    .line 107
    .line 108
    const/4 v5, 0x0

    .line 109
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 110
    .line 111
    .line 112
    move-result v6

    .line 113
    move-object v0, p0

    .line 114
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 115
    .line 116
    .line 117
    :cond_3
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 118
    .line 119
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 120
    .line 121
    .line 122
    move-result p1

    .line 123
    if-eqz p1, :cond_4

    .line 124
    .line 125
    iget-object v1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 126
    .line 127
    const/16 v2, 0x10e

    .line 128
    .line 129
    const/high16 v3, 0x3f800000    # 1.0f

    .line 130
    .line 131
    const/16 v4, 0x50

    .line 132
    .line 133
    const/4 v5, 0x0

    .line 134
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 135
    .line 136
    .line 137
    move-result v6

    .line 138
    move-object v0, p0

    .line 139
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 140
    .line 141
    .line 142
    :cond_4
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇888()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 147
    .line 148
    .line 149
    move-result p1

    .line 150
    if-eqz p1, :cond_5

    .line 151
    .line 152
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇888()Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v1

    .line 156
    const/16 v2, 0x10e

    .line 157
    .line 158
    const/high16 v3, 0x3f800000    # 1.0f

    .line 159
    .line 160
    const/16 v4, 0x50

    .line 161
    .line 162
    const/4 v5, 0x0

    .line 163
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 164
    .line 165
    .line 166
    move-result v6

    .line 167
    move-object v0, p0

    .line 168
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 169
    .line 170
    .line 171
    :cond_5
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇80〇808〇O()Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 176
    .line 177
    .line 178
    move-result p1

    .line 179
    if-eqz p1, :cond_6

    .line 180
    .line 181
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇80〇808〇O()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v1

    .line 185
    const/16 v2, 0x10e

    .line 186
    .line 187
    const/high16 v3, 0x3f800000    # 1.0f

    .line 188
    .line 189
    const/16 v4, 0x50

    .line 190
    .line 191
    const/4 v5, 0x0

    .line 192
    invoke-static {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 193
    .line 194
    .line 195
    move-result v6

    .line 196
    move-object v0, p0

    .line 197
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O(Ljava/lang/String;IFILjava/lang/String;Z)V

    .line 198
    .line 199
    .line 200
    :cond_6
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->ooOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 201
    .line 202
    .line 203
    const/4 p1, 0x0

    .line 204
    return-object p1
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private 〇O(Ljava/lang/String;IFILjava/lang/String;Z)V
    .locals 9

    .line 1
    const-string v0, "MultiImageEditPageManager"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇80〇808〇O()Lcom/intsig/okbinder/ServerInfo;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-eqz p6, :cond_1

    .line 10
    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p6

    .line 17
    if-eqz p6, :cond_1

    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO00OOO()I

    .line 20
    .line 21
    .line 22
    move-result p6

    .line 23
    if-lez p6, :cond_1

    .line 24
    .line 25
    const/4 p6, 0x1

    .line 26
    const/4 v2, 0x0

    .line 27
    :try_start_0
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 28
    .line 29
    invoke-virtual {v3, p6}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 30
    .line 31
    .line 32
    const-string v3, "scaleImage before executeProgress"

    .line 33
    .line 34
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    move-object v3, v1

    .line 42
    check-cast v3, Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;

    .line 43
    .line 44
    move-object v4, p1

    .line 45
    move v5, p2

    .line 46
    move v6, p3

    .line 47
    move v7, p4

    .line 48
    move-object v8, p5

    .line 49
    invoke-interface/range {v3 .. v8}, Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 50
    .line 51
    .line 52
    const-string v1, "scaleImage after executeProgress"

    .line 53
    .line 54
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    .line 56
    .line 57
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 58
    .line 59
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 60
    .line 61
    .line 62
    return-void

    .line 63
    :catchall_0
    move-exception v1

    .line 64
    :try_start_1
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    .line 66
    .line 67
    sget-object v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇8o8o〇:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;

    .line 68
    .line 69
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;->〇080()Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇O〇()Z

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 77
    .line 78
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇〇888()V

    .line 79
    .line 80
    .line 81
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->android_multi_process_no_retry:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 86
    .line 87
    if-ne v0, p6, :cond_0

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_0
    iget-object p6, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 91
    .line 92
    invoke-virtual {p6, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 93
    .line 94
    .line 95
    goto :goto_1

    .line 96
    :catchall_1
    move-exception p1

    .line 97
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 98
    .line 99
    invoke-virtual {p2, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 100
    .line 101
    .line 102
    throw p1

    .line 103
    :cond_1
    :goto_1
    invoke-static {p1, p2, p3, p4, p5}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static synthetic 〇O00(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O〇O〇oO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇O888o0o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;-><init>(LO〇O/〇80〇808〇O;)V

    .line 5
    .line 6
    .line 7
    iget-wide v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 8
    .line 9
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇〇888(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;J)V

    .line 10
    .line 11
    .line 12
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 13
    .line 14
    invoke-static {v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->o〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇8〇0〇o〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/callback/Callback0;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private 〇O〇80o08O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oooo8o0〇:Ljava/lang/Thread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇8O0〇8:[B

    .line 7
    .line 8
    monitor-enter v0

    .line 9
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oooo8o0〇:Ljava/lang/Thread;

    .line 10
    .line 11
    if-nez v1, :cond_1

    .line 12
    .line 13
    new-instance v1, Ljava/lang/Thread;

    .line 14
    .line 15
    new-instance v2, LO〇O/o〇0;

    .line 16
    .line 17
    invoke-direct {v2, p0}, LO〇O/o〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 21
    .line 22
    .line 23
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oooo8o0〇:Ljava/lang/Thread;

    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 26
    .line 27
    .line 28
    :cond_1
    monitor-exit v0

    .line 29
    return-void

    .line 30
    :catchall_0
    move-exception v1

    .line 31
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    throw v1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇0OOo〇0(Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇oOO8O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    :goto_0
    return-object p1
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oO00OOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic 〇〇0o()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇808〇:Z

    .line 6
    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    const-wide/16 v1, 0x1f4

    .line 10
    .line 11
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇00O0O0(J)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇:Z

    .line 16
    .line 17
    if-eqz v1, :cond_2

    .line 18
    .line 19
    goto :goto_3

    .line 20
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇8o8o〇:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/util/concurrent/PriorityBlockingQueue;->take()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇80〇808〇O()Lcom/intsig/okbinder/ServerInfo;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-static {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Z

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    if-eqz v3, :cond_3

    .line 39
    .line 40
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO00OOO()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-lez v3, :cond_3

    .line 45
    .line 46
    const/4 v3, 0x1

    .line 47
    goto :goto_1

    .line 48
    :cond_3
    const/4 v3, 0x0

    .line 49
    :goto_1
    if-eqz v3, :cond_4

    .line 50
    .line 51
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 52
    .line 53
    invoke-virtual {v4}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->OO0o〇〇()V

    .line 54
    .line 55
    .line 56
    :cond_4
    if-eqz v3, :cond_5

    .line 57
    .line 58
    if-eqz v2, :cond_5

    .line 59
    .line 60
    invoke-virtual {v2}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    if-eqz v2, :cond_5

    .line 65
    .line 66
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 67
    .line 68
    .line 69
    goto :goto_2

    .line 70
    :cond_5
    invoke-static {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;

    .line 71
    .line 72
    .line 73
    move-result-object v2

    .line 74
    if-eqz v2, :cond_6

    .line 75
    .line 76
    invoke-static {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;->O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTaskRunnable;->〇080(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    .line 82
    .line 83
    goto :goto_2

    .line 84
    :catch_0
    move-exception v1

    .line 85
    const-string v2, "MultiImageEditPageManager"

    .line 86
    .line 87
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 99
    .line 100
    .line 101
    :cond_6
    :goto_2
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇:Z

    .line 102
    .line 103
    if-eqz v1, :cond_0

    .line 104
    .line 105
    :goto_3
    if-eqz v0, :cond_7

    .line 106
    .line 107
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 108
    .line 109
    .line 110
    :cond_7
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇〇808〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇8oOO88(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇08O8o〇0(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$ImageTask;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/callback/Callback0;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private synthetic 〇〇〇0〇〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oO80:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oO80:Z

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇O8〇〇o:Landroidx/lifecycle/MutableLiveData;

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O8O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 1

    .line 1
    const/16 v0, 0x65

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO8oO0o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;JI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O8ooOoo〇()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO8oO0o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;JI)V
    .locals 2

    .line 1
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->O8:I

    .line 2
    .line 3
    iput v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 7
    .line 8
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇:Z

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇00〇8()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o()V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇:Landroid/os/Handler;

    .line 17
    .line 18
    const-string v1, "MultiImageEditPageManager"

    .line 19
    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    new-instance p2, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string p3, "pushImage workHandler == null imageUUID="

    .line 28
    .line 29
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 33
    .line 34
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇:Landroid/os/Handler;

    .line 46
    .line 47
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iput p4, v0, Landroid/os/Message;->what:I

    .line 52
    .line 53
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 54
    .line 55
    iget-object p4, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇:Landroid/os/Handler;

    .line 56
    .line 57
    invoke-virtual {p4, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 58
    .line 59
    .line 60
    new-instance p2, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    const-string p3, "pushImage imageUUID="

    .line 66
    .line 67
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 71
    .line 72
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
.end method

.method public OOO8o〇〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OoO8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 2

    .line 1
    const-string v0, "MultiImageEditPageManager"

    .line 2
    .line 3
    const-string v1, "addMultiImageEditPage "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 9
    .line 10
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Ooo8〇〇()V
    .locals 3

    .line 1
    const-string v0, "MultiImageEditPageManager"

    .line 2
    .line 3
    const-string v1, "stopHandleImage"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O〇:Z

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇808〇:Z

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oooo8o0〇:Ljava/lang/Thread;

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇8O0〇8:[B

    .line 20
    .line 21
    monitor-enter v0

    .line 22
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oooo8o0〇:Ljava/lang/Thread;

    .line 23
    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 27
    .line 28
    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oooo8o0〇:Ljava/lang/Thread;

    .line 30
    .line 31
    :cond_0
    monitor-exit v0

    .line 32
    goto :goto_0

    .line 33
    :catchall_0
    move-exception v1

    .line 34
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    throw v1

    .line 36
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O8o08O:Landroid/os/HandlerThread;

    .line 37
    .line 38
    if-eqz v0, :cond_4

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O00:[B

    .line 41
    .line 42
    monitor-enter v0

    .line 43
    :try_start_1
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O8o08O:Landroid/os/HandlerThread;

    .line 44
    .line 45
    if-eqz v2, :cond_2

    .line 46
    .line 47
    invoke-virtual {v2}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 48
    .line 49
    .line 50
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇O8o08O:Landroid/os/HandlerThread;

    .line 51
    .line 52
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇:Landroid/os/Handler;

    .line 53
    .line 54
    if-eqz v2, :cond_3

    .line 55
    .line 56
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO0o〇〇:Landroid/os/Handler;

    .line 57
    .line 58
    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    :cond_3
    monitor-exit v0

    .line 62
    goto :goto_1

    .line 63
    :catchall_1
    move-exception v1

    .line 64
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 65
    throw v1

    .line 66
    :cond_4
    :goto_1
    return-void
    .line 67
    .line 68
.end method

.method public Oo〇O(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;",
            ")",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_2

    .line 8
    .line 9
    new-instance v0, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_1

    .line 23
    .line 24
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 29
    .line 30
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 31
    .line 32
    if-eqz v2, :cond_0

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    if-nez v3, :cond_0

    .line 43
    .line 44
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-nez v1, :cond_3

    .line 53
    .line 54
    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇000O0(Ljava/util/List;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)V

    .line 55
    .line 56
    .line 57
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 58
    .line 59
    return-object p1

    .line 60
    :cond_2
    if-eqz p2, :cond_3

    .line 61
    .line 62
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;->〇o〇(Ljava/util/List;)V

    .line 63
    .line 64
    .line 65
    :cond_3
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 66
    .line 67
    return-object p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public O〇8O8〇008()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    add-int/lit8 v1, v1, -0x1

    .line 18
    .line 19
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o0O0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇00:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇00:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO00OOO()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-lez p1, :cond_1

    .line 11
    .line 12
    const/4 v0, 0x3

    .line 13
    if-le p1, v0, :cond_0

    .line 14
    .line 15
    new-instance p1, Ljava/util/concurrent/Semaphore;

    .line 16
    .line 17
    invoke-direct {p1, v0}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    new-instance v0, Ljava/util/concurrent/Semaphore;

    .line 24
    .line 25
    invoke-direct {v0, p1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 26
    .line 27
    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 29
    .line 30
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->OO0o〇〇()V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o88〇OO08〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ZJ)V
    .locals 0

    .line 1
    iput-boolean p2, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oO80:Z

    .line 2
    .line 3
    const/16 p2, 0x67

    .line 4
    .line 5
    invoke-virtual {p0, p1, p3, p4, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO8oO0o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;JI)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public oo88o8O(Z)V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 6
    .line 7
    .line 8
    new-instance v1, LO〇O/O8;

    .line 9
    .line 10
    invoke-direct {v1, v0, p1}, LO〇O/O8;-><init>(Ljava/util/List;Z)V

    .line 11
    .line 12
    .line 13
    invoke-static {v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o00〇〇Oo:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 19
    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 22
    .line 23
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 38
    .line 39
    if-eqz v0, :cond_0

    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o00〇〇Oo:Ljava/util/List;

    .line 42
    .line 43
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 48
    .line 49
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 50
    .line 51
    .line 52
    const/4 p1, -0x1

    .line 53
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o〇:I

    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public ooOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 5

    .line 1
    iget-boolean v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇08O〇00〇o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O000()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v1, ".jpg"

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 32
    .line 33
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-eqz v1, :cond_1

    .line 38
    .line 39
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 40
    .line 41
    .line 42
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 43
    .line 44
    invoke-static {v1, v0}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 45
    .line 46
    .line 47
    invoke-static {v0}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇o(Ljava/lang/String;)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    new-instance v2, Landroid/content/ContentValues;

    .line 52
    .line 53
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 54
    .line 55
    .line 56
    const-string v3, "_data"

    .line 57
    .line 58
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    const-string v0, "thumb_data"

    .line 62
    .line 63
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    const-string v0, "image_rotation"

    .line 67
    .line 68
    const/4 v1, 0x0

    .line 69
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 74
    .line 75
    .line 76
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 77
    .line 78
    add-int/lit16 v0, v0, 0x168

    .line 79
    .line 80
    iget v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O〇08oOOO0:I

    .line 81
    .line 82
    sub-int/2addr v0, v3

    .line 83
    rem-int/lit16 v0, v0, 0x168

    .line 84
    .line 85
    const-string v3, "ori_rotation"

    .line 86
    .line 87
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 92
    .line 93
    .line 94
    const-string v0, "status"

    .line 95
    .line 96
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 97
    .line 98
    .line 99
    move-result-object v3

    .line 100
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 101
    .line 102
    .line 103
    const-string v0, "cache_state"

    .line 104
    .line 105
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 110
    .line 111
    .line 112
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 113
    .line 114
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->oo〇(I)I

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    const-string v1, "enhance_mode"

    .line 123
    .line 124
    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 125
    .line 126
    .line 127
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 128
    .line 129
    const/4 v1, 0x1

    .line 130
    invoke-static {v0, v1}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 135
    .line 136
    if-nez v3, :cond_0

    .line 137
    .line 138
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->〇08O8o〇0([I)[I

    .line 139
    .line 140
    .line 141
    move-result-object v3

    .line 142
    :cond_0
    iget v4, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 143
    .line 144
    invoke-static {v0, v0, v3, v4}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    const-string v3, "image_border"

    .line 149
    .line 150
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇08O:I

    .line 154
    .line 155
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    const-string v3, "contrast_index"

    .line 160
    .line 161
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 162
    .line 163
    .line 164
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O0O:I

    .line 165
    .line 166
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 167
    .line 168
    .line 169
    move-result-object v0

    .line 170
    const-string v3, "bright_index"

    .line 171
    .line 172
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 173
    .line 174
    .line 175
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8oOOo:I

    .line 176
    .line 177
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    const-string v3, "detail_index"

    .line 182
    .line 183
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 184
    .line 185
    .line 186
    iput-boolean v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 187
    .line 188
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 189
    .line 190
    iget-wide v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 191
    .line 192
    invoke-static {v0, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 193
    .line 194
    .line 195
    move-result-object p1

    .line 196
    const-string v0, "2"

    .line 197
    .line 198
    const-string v1, "5"

    .line 199
    .line 200
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v0

    .line 204
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 205
    .line 206
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 207
    .line 208
    .line 209
    move-result-object v1

    .line 210
    const-string v3, "sync_jpage_state != ?  AND sync_jpage_state != ?"

    .line 211
    .line 212
    invoke-virtual {v1, p1, v2, v3, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 213
    .line 214
    .line 215
    move-result p1

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    .line 217
    .line 218
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .line 220
    .line 221
    const-string v1, "saveImageInfo isDirectlyUsingBigImage:true updateNumber:"

    .line 222
    .line 223
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object p1

    .line 233
    const-string v0, "MultiImageEditPageManager"

    .line 234
    .line 235
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    :cond_1
    return-void
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public o〇O8〇〇o()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇〇0〇()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇080:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o88〇OO08〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ZJ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇0000OOO()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0O〇Oo(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o0O0O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 1

    .line 1
    const/16 v0, 0x66

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO8oO0o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;JI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇oo〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇8o8o〇:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇80〇808〇O:Z

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇o8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 1

    .line 1
    const/16 v0, 0x68

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OO8oO0o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;JI)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;
.super Ljava/lang/Object;
.source "GlideImageFileDataExtKey.java"

# interfaces
.implements Lcom/bumptech/glide/load/Key;


# instance fields
.field private O8:Ljava/lang/String;

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

.field private 〇o〇:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, ""

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->O8:Ljava/lang/String;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 9
    .line 10
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o00〇〇Oo:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    if-eqz p1, :cond_3

    .line 7
    .line 8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    if-eq v2, v3, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;

    .line 20
    .line 21
    iget v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o〇:I

    .line 22
    .line 23
    iget v3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o〇:I

    .line 24
    .line 25
    if-ne v2, v3, :cond_2

    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->O8:Ljava/lang/String;

    .line 28
    .line 29
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->O8:Ljava/lang/String;

    .line 30
    .line 31
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-eqz v2, :cond_2

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o00〇〇Oo:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 38
    .line 39
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o00〇〇Oo:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 40
    .line 41
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-eqz p1, :cond_2

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_2
    const/4 v0, 0x0

    .line 49
    :goto_0
    return v0

    .line 50
    :cond_3
    :goto_1
    return v1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public hashCode()I
    .locals 3

    .line 1
    const/4 v0, 0x3

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o00〇〇Oo:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o〇:I

    .line 10
    .line 11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const/4 v2, 0x1

    .line 16
    aput-object v1, v0, v2

    .line 17
    .line 18
    const/4 v1, 0x2

    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->O8:Ljava/lang/String;

    .line 20
    .line 21
    aput-object v2, v0, v1

    .line 22
    .line 23
    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇o00〇〇Oo(Ljava/security/MessageDigest;)V
    .locals 2
    .param p1    # Ljava/security/MessageDigest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o00〇〇Oo:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->toString()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o〇:I

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->O8:Ljava/lang/String;

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sget-object v1, Lcom/bumptech/glide/load/Key;->〇080:Ljava/nio/charset/Charset;

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {p1, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇o〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;->〇o〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

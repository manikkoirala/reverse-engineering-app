.class public Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;
.super Lcom/intsig/adapter/RecyclingPagerAdapter;
.source "MultiImageEditAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$TraverseViewHolderCallback;,
        Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;,
        Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;,
        Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;
    }
.end annotation


# instance fields
.field private final O8:I

.field private O8ooOoo〇:Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

.field private OO0o〇〇:I

.field private OO0o〇〇〇〇0:I

.field private final Oo08:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public OoO8:Z

.field private Oooo8o0〇:I

.field private O〇8O8〇008:Lcom/intsig/camscanner/multiimageedit/animation/MultiPreviewHorizontalAnimation;

.field public o800o8O:Z

.field private oO80:I

.field private oo88o8O:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

.field private o〇O8〇〇o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/widget/ImageView;",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;",
            ">;"
        }
    .end annotation
.end field

.field private 〇00:Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;

.field public 〇0〇O0088o:Z

.field private 〇80〇808〇O:I

.field private 〇8o8o〇:Z

.field private final 〇O00:Z

.field private 〇O888o0o:Landroid/view/View;

.field private final 〇O8o08O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

.field private final 〇O〇:Z

.field private final 〇o00〇〇Oo:Landroid/app/Activity;

.field private 〇oo〇:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
            ">;"
        }
    .end annotation
.end field

.field public final 〇o〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇〇808〇:Z

.field private 〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

.field private 〇〇8O0〇8:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;ZILjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;ZI",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 1
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;ZIZ)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;ZIZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;ZIZ)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Lcom/intsig/adapter/RecyclingPagerAdapter;-><init>()V

    .line 3
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    const/4 v0, -0x1

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oO80:I

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇80〇808〇O:I

    .line 6
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇8o8o〇:Z

    .line 8
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    invoke-direct {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O8o08O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    const/4 v2, 0x1

    .line 9
    iput-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇8O0〇8:Z

    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0〇O0088o:Z

    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OoO8:Z

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o800o8O:Z

    .line 13
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oo88o8O:Ljava/util/HashSet;

    .line 14
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oo〇:Ljava/util/HashMap;

    .line 15
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O8〇〇o:Ljava/util/HashMap;

    .line 16
    new-instance v3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$3;

    invoke-direct {v3, p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$3;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;)V

    iput-object v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O8ooOoo〇:Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 17
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 18
    iput p4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O8:I

    .line 19
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 20
    iput-boolean p3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O00:Z

    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0000OOO()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 22
    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    :cond_0
    new-instance p2, Landroid/util/DisplayMetrics;

    invoke-direct {p2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 24
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 25
    iget p1, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇:I

    .line 26
    iget p1, p2, Landroid/util/DisplayMetrics;->heightPixels:I

    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oooo8o0〇:I

    .line 27
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x17

    if-le p1, p2, :cond_1

    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇808〇:Z

    .line 28
    iput-boolean p5, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O〇:Z

    .line 29
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->showOriginImgCompareFun()Z

    move-result p1

    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o800o8O:Z

    return-void
.end method

.method private O0o〇〇Oo(Lcom/intsig/camscanner/view/ZoomImageView;F)V
    .locals 7

    .line 1
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2
    .line 3
    invoke-static {p2, v0}, Ljava/lang/Float;->compare(FF)I

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    const/16 v0, 0x8

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    if-gtz p2, :cond_0

    .line 11
    .line 12
    const/4 p2, 0x0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/16 p2, 0x8

    .line 15
    .line 16
    :goto_0
    const/4 v2, 0x0

    .line 17
    :goto_1
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 18
    .line 19
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    if-ge v2, v3, :cond_a

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 26
    .line 27
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 32
    .line 33
    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    check-cast v3, Landroid/view/View;

    .line 38
    .line 39
    if-nez v3, :cond_1

    .line 40
    .line 41
    goto/16 :goto_7

    .line 42
    .line 43
    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    instance-of v4, v3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 48
    .line 49
    if-nez v4, :cond_2

    .line 50
    .line 51
    goto :goto_7

    .line 52
    :cond_2
    check-cast v3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 53
    .line 54
    iget-object v4, v3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 55
    .line 56
    if-ne v4, p1, :cond_9

    .line 57
    .line 58
    iget-object v4, v3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 59
    .line 60
    iget-boolean v5, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇8o8o〇:Z

    .line 61
    .line 62
    if-eqz v5, :cond_3

    .line 63
    .line 64
    const/16 v5, 0x8

    .line 65
    .line 66
    goto :goto_2

    .line 67
    :cond_3
    move v5, p2

    .line 68
    :goto_2
    invoke-direct {p0, v4, v5}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o88〇OO08〇(Landroid/view/View;I)V

    .line 69
    .line 70
    .line 71
    iget-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0〇O0088o:Z

    .line 72
    .line 73
    if-eqz v4, :cond_4

    .line 74
    .line 75
    iget-object v4, v3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 76
    .line 77
    invoke-direct {p0, v4, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o88〇OO08〇(Landroid/view/View;I)V

    .line 78
    .line 79
    .line 80
    :cond_4
    iget-object v4, v3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 81
    .line 82
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    move-result-object v4

    .line 86
    instance-of v5, v4, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;

    .line 87
    .line 88
    if-eqz v5, :cond_5

    .line 89
    .line 90
    check-cast v4, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;

    .line 91
    .line 92
    iget v4, v4, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;->〇080:I

    .line 93
    .line 94
    goto :goto_3

    .line 95
    :cond_5
    const/4 v4, -0x1

    .line 96
    :goto_3
    if-ltz v4, :cond_6

    .line 97
    .line 98
    iget-object v5, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 99
    .line 100
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    if-ge v4, v5, :cond_6

    .line 105
    .line 106
    iget-object v5, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 107
    .line 108
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object v4

    .line 112
    check-cast v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 113
    .line 114
    if-eqz v4, :cond_6

    .line 115
    .line 116
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 117
    .line 118
    goto :goto_4

    .line 119
    :cond_6
    const/4 v4, 0x0

    .line 120
    :goto_4
    const/4 v5, 0x1

    .line 121
    if-nez p2, :cond_7

    .line 122
    .line 123
    const/4 v6, 0x1

    .line 124
    goto :goto_5

    .line 125
    :cond_7
    const/4 v6, 0x0

    .line 126
    :goto_5
    invoke-direct {p0, v3, v6, v4}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->ooo〇8oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 127
    .line 128
    .line 129
    if-nez p2, :cond_8

    .line 130
    .line 131
    goto :goto_6

    .line 132
    :cond_8
    const/4 v5, 0x0

    .line 133
    :goto_6
    invoke-direct {p0, v3, v5, v4}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OOO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 134
    .line 135
    .line 136
    goto :goto_7

    .line 137
    :cond_9
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 138
    .line 139
    invoke-direct {p0, v3, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o88〇OO08〇(Landroid/view/View;I)V

    .line 140
    .line 141
    .line 142
    :goto_7
    add-int/lit8 v2, v2, 0x1

    .line 143
    .line 144
    goto :goto_1

    .line 145
    :cond_a
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O888o0o:Landroid/view/View;

    .line 146
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o88〇OO08〇(Landroid/view/View;I)V

    .line 148
    .line 149
    .line 150
    return-void
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;ILcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o8(ILcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic O8〇o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;->o8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OOO〇O0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private OOO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 5

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-nez p3, :cond_0

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v2, 0x0

    .line 8
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v4, "tryShowCompareView show:"

    .line 14
    .line 15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string v4, ", showCompare:"

    .line 22
    .line 23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    iget-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o800o8O:Z

    .line 27
    .line 28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v4, ", multiImageEditModel is null: "

    .line 32
    .line 33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    const-string v4, "MultiImageEditAdapter"

    .line 44
    .line 45
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    if-nez v2, :cond_2

    .line 49
    .line 50
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇()Z

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    if-nez v2, :cond_1

    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_1
    const/4 v2, 0x0

    .line 58
    goto :goto_2

    .line 59
    :cond_2
    :goto_1
    const/4 v2, 0x1

    .line 60
    :goto_2
    if-eqz p3, :cond_5

    .line 61
    .line 62
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->showOriginImgCompareFun()Z

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    if-eqz v2, :cond_4

    .line 67
    .line 68
    iget p3, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 69
    .line 70
    const/4 v2, -0x1

    .line 71
    if-ne p3, v2, :cond_3

    .line 72
    .line 73
    goto :goto_3

    .line 74
    :cond_3
    const/4 v0, 0x0

    .line 75
    :cond_4
    :goto_3
    move v2, v0

    .line 76
    :cond_5
    iget-boolean p3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o800o8O:Z

    .line 77
    .line 78
    if-eqz p3, :cond_7

    .line 79
    .line 80
    if-eqz p2, :cond_7

    .line 81
    .line 82
    if-eqz v2, :cond_6

    .line 83
    .line 84
    goto :goto_4

    .line 85
    :cond_6
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 86
    .line 87
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 88
    .line 89
    .line 90
    goto :goto_5

    .line 91
    :cond_7
    :goto_4
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 92
    .line 93
    const/4 p2, 0x4

    .line 94
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 95
    .line 96
    .line 97
    :goto_5
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private synthetic OOO〇O0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;->o88o0O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oo〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private Oo8Oo00oo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Z)V
    .locals 4

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->O〇O〇oO()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_5

    .line 8
    .line 9
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->o8oO〇()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_0
    iget-boolean v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OO8ooO8〇:Z

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    return-void

    .line 30
    :cond_2
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 31
    .line 32
    invoke-direct {p0, v0, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O〇8O8〇008(Landroid/view/View;Ljava/lang/String;)[I

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 37
    .line 38
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    instance-of v2, v1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;

    .line 43
    .line 44
    if-eqz v2, :cond_3

    .line 45
    .line 46
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_3
    const/4 v1, 0x0

    .line 50
    :goto_0
    if-nez v1, :cond_4

    .line 51
    .line 52
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;

    .line 53
    .line 54
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;)V

    .line 55
    .line 56
    .line 57
    :cond_4
    iput-object p2, v1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;->〇o00〇〇Oo:Ljava/lang/String;

    .line 58
    .line 59
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 60
    .line 61
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 62
    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 65
    .line 66
    invoke-static {v1}, Lcom/bumptech/glide/Glide;->〇0〇O0088o(Landroid/app/Activity;)Lcom/bumptech/glide/RequestManager;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lcom/bumptech/glide/RequestManager;->〇o00〇〇Oo()Lcom/bumptech/glide/RequestBuilder;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {v1, p2}, Lcom/bumptech/glide/RequestBuilder;->ooO〇00O(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    const/4 v2, 0x0

    .line 79
    aget v2, v0, v2

    .line 80
    .line 81
    const/4 v3, 0x1

    .line 82
    aget v0, v0, v3

    .line 83
    .line 84
    invoke-direct {p0, v2, v0, p2, p4}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇00(IILjava/lang/String;Z)Lcom/bumptech/glide/request/RequestOptions;

    .line 85
    .line 86
    .line 87
    move-result-object p4

    .line 88
    invoke-virtual {v1, p4}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 89
    .line 90
    .line 91
    move-result-object p4

    .line 92
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$2;

    .line 93
    .line 94
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$2;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {p4, v0}, Lcom/bumptech/glide/RequestBuilder;->O〇0(Lcom/bumptech/glide/request/target/Target;)Lcom/bumptech/glide/request/target/Target;

    .line 98
    .line 99
    .line 100
    :cond_5
    :goto_1
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/view/ZoomImageView;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oO00OOO(Lcom/intsig/camscanner/view/ZoomImageView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Ooo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇〇0〇〇0(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p3, :cond_0

    .line 6
    .line 7
    iget-object p3, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 8
    .line 9
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0OOo〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Z

    .line 10
    .line 11
    .line 12
    move-result p3

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p3, 0x0

    .line 15
    :goto_0
    const/4 v1, 0x2

    .line 16
    const/16 v2, 0x8

    .line 17
    .line 18
    if-ne p2, v1, :cond_1

    .line 19
    .line 20
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 21
    .line 22
    const-string p3, "#99000000"

    .line 23
    .line 24
    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    invoke-static {p3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 29
    .line 30
    .line 31
    move-result-object p3

    .line 32
    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 33
    .line 34
    .line 35
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 36
    .line 37
    sget-object p3, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    .line 38
    .line 39
    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 40
    .line 41
    .line 42
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 43
    .line 44
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 45
    .line 46
    .line 47
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 48
    .line 49
    invoke-virtual {p2}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;->release()V

    .line 50
    .line 51
    .line 52
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 53
    .line 54
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 55
    .line 56
    .line 57
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇80〇808〇O:Landroid/view/View;

    .line 58
    .line 59
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 60
    .line 61
    .line 62
    goto/16 :goto_3

    .line 63
    .line 64
    :cond_1
    const/4 v1, 0x1

    .line 65
    if-ne p2, v1, :cond_7

    .line 66
    .line 67
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 68
    .line 69
    .line 70
    move-result p2

    .line 71
    sub-int/2addr p2, v1

    .line 72
    if-ne p2, p4, :cond_2

    .line 73
    .line 74
    iget-boolean p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0〇O0088o:Z

    .line 75
    .line 76
    if-eqz p2, :cond_2

    .line 77
    .line 78
    return-void

    .line 79
    :cond_2
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 80
    .line 81
    const v1, -0x66a5a5a6

    .line 82
    .line 83
    .line 84
    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 89
    .line 90
    .line 91
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 92
    .line 93
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    .line 94
    .line 95
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 96
    .line 97
    .line 98
    if-eqz p3, :cond_6

    .line 99
    .line 100
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 101
    .line 102
    invoke-virtual {p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getDisplayBoundRect()Landroid/graphics/RectF;

    .line 103
    .line 104
    .line 105
    move-result-object p2

    .line 106
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 107
    .line 108
    invoke-virtual {p3, v0}, Lcom/intsig/camscanner/view/ZoomImageView;->setSupportScale(Z)V

    .line 109
    .line 110
    .line 111
    if-eqz p2, :cond_3

    .line 112
    .line 113
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    .line 114
    .line 115
    .line 116
    move-result p3

    .line 117
    const/4 v1, 0x0

    .line 118
    cmpl-float p3, p3, v1

    .line 119
    .line 120
    if-lez p3, :cond_3

    .line 121
    .line 122
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 123
    .line 124
    invoke-virtual {p3, p2}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;->setDisplayRect(Landroid/graphics/RectF;)V

    .line 125
    .line 126
    .line 127
    :cond_3
    iget p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 128
    .line 129
    if-ne p2, p4, :cond_5

    .line 130
    .line 131
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 132
    .line 133
    invoke-virtual {p2}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;->animalIsRun()Z

    .line 134
    .line 135
    .line 136
    move-result p2

    .line 137
    if-nez p2, :cond_4

    .line 138
    .line 139
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 140
    .line 141
    invoke-virtual {p2, p4}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;->showLoading(I)V

    .line 142
    .line 143
    .line 144
    :cond_4
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 145
    .line 146
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 147
    .line 148
    .line 149
    goto :goto_1

    .line 150
    :cond_5
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 151
    .line 152
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 153
    .line 154
    .line 155
    :goto_1
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 156
    .line 157
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 158
    .line 159
    .line 160
    goto :goto_2

    .line 161
    :cond_6
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 162
    .line 163
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 164
    .line 165
    .line 166
    :goto_2
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇80〇808〇O:Landroid/view/View;

    .line 167
    .line 168
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 169
    .line 170
    .line 171
    goto :goto_3

    .line 172
    :cond_7
    iget p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 173
    .line 174
    if-eq p2, p4, :cond_8

    .line 175
    .line 176
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 177
    .line 178
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 179
    .line 180
    .line 181
    :cond_8
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 182
    .line 183
    const/4 p3, 0x0

    .line 184
    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 185
    .line 186
    .line 187
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 188
    .line 189
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 190
    .line 191
    .line 192
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇80〇808〇O:Landroid/view/View;

    .line 193
    .line 194
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 195
    .line 196
    .line 197
    :goto_3
    return-void
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;)Lcom/intsig/camscanner/view/MyViewPager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇8O8〇008(Landroid/view/View;Ljava/lang/String;)[I
    .locals 12

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇808〇:Z

    .line 2
    .line 3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/high16 v0, 0x3fc00000    # 1.5f

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    .line 12
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-gtz p1, :cond_1

    .line 17
    .line 18
    iget p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇:I

    .line 19
    .line 20
    :cond_1
    const/16 v2, 0x7d0

    .line 21
    .line 22
    if-le p1, v2, :cond_2

    .line 23
    .line 24
    const/16 p1, 0x7d0

    .line 25
    .line 26
    :cond_2
    int-to-float p1, p1

    .line 27
    mul-float p1, p1, v0

    .line 28
    .line 29
    float-to-int p1, p1

    .line 30
    const/4 v2, 0x0

    .line 31
    invoke-static {p2, v2}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    const/4 v3, 0x1

    .line 36
    if-eqz p2, :cond_3

    .line 37
    .line 38
    int-to-float v4, p1

    .line 39
    mul-float v4, v4, v1

    .line 40
    .line 41
    aget v1, p2, v3

    .line 42
    .line 43
    int-to-float v1, v1

    .line 44
    mul-float v4, v4, v1

    .line 45
    .line 46
    aget p2, p2, v2

    .line 47
    .line 48
    int-to-float p2, p2

    .line 49
    div-float/2addr v4, p2

    .line 50
    float-to-int p2, v4

    .line 51
    goto :goto_1

    .line 52
    :cond_3
    const/4 p2, 0x0

    .line 53
    :goto_1
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oooo8o0〇:I

    .line 54
    .line 55
    if-lez v1, :cond_4

    .line 56
    .line 57
    int-to-double v4, v1

    .line 58
    const-wide v6, 0x3fe6666666666666L    # 0.7

    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    mul-double v4, v4, v6

    .line 64
    .line 65
    float-to-double v8, v0

    .line 66
    mul-double v4, v4, v8

    .line 67
    .line 68
    int-to-double v10, p2

    .line 69
    cmpg-double v0, v4, v10

    .line 70
    .line 71
    if-gez v0, :cond_4

    .line 72
    .line 73
    int-to-double v0, v1

    .line 74
    mul-double v0, v0, v6

    .line 75
    .line 76
    mul-double v0, v0, v8

    .line 77
    .line 78
    double-to-int p2, v0

    .line 79
    :cond_4
    const/4 v0, 0x2

    .line 80
    new-array v0, v0, [I

    .line 81
    .line 82
    aput p1, v0, v2

    .line 83
    .line 84
    aput p2, v0, v3

    .line 85
    .line 86
    return-object v0
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private o0O0(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 2

    .line 1
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 4
    .line 5
    sget v1, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇080:I

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, p1, v0, p3, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Ooo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/adapter/O8;

    .line 15
    .line 16
    invoke-direct {v0, p0, p2, p1, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/O8;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;ILcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 17
    .line 18
    .line 19
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 20
    .line 21
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 25
    .line 26
    const-wide/16 p2, 0x96

    .line 27
    .line 28
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 29
    .line 30
    .line 31
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic o0ooO(Ljava/lang/Boolean;)V
    .locals 1

    .line 1
    const-string p1, "MultiImageEditAdapter"

    .line 2
    .line 3
    const-string v0, "superAnimView notify"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o8(ILcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 2
    .line 3
    const-string v1, "MultiImageEditAdapter"

    .line 4
    .line 5
    if-eqz v0, :cond_5

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_2

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const/4 v2, 0x0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    invoke-direct {p0, p2, v2, p3, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Ooo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 24
    .line 25
    .line 26
    const-string p1, "instantiateSparseArray.get(position) == null"

    .line 27
    .line 28
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_1
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 33
    .line 34
    iget v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 35
    .line 36
    sget v1, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇080:I

    .line 37
    .line 38
    if-eq v0, v1, :cond_4

    .line 39
    .line 40
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 41
    .line 42
    iget v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 43
    .line 44
    sget v1, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->oO80:I

    .line 45
    .line 46
    if-ne v0, v1, :cond_2

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_2
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 50
    .line 51
    iget v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 52
    .line 53
    sget v1, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇〇888:I

    .line 54
    .line 55
    if-ne v0, v1, :cond_3

    .line 56
    .line 57
    const/4 v0, 0x2

    .line 58
    invoke-direct {p0, p2, v0, p3, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Ooo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 59
    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_3
    const/4 v0, 0x1

    .line 63
    invoke-direct {p0, p2, v0, p3, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Ooo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 64
    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_4
    :goto_0
    invoke-direct {p0, p2, v2, p3, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Ooo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 68
    .line 69
    .line 70
    :goto_1
    return-void

    .line 71
    :cond_5
    :goto_2
    const-string p1, "activity == null || activity.isFinishing()"

    .line 72
    .line 73
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/view/ZoomImageView;F)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O0o〇〇Oo(Lcom/intsig/camscanner/view/ZoomImageView;F)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o88〇OO08〇(Landroid/view/View;I)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-ne v0, p2, :cond_1

    .line 9
    .line 10
    return-void

    .line 11
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oO(Lcom/intsig/camscanner/view/ImageViewTouchBase;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOriDisplayRectF()Landroid/graphics/RectF;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-nez v0, :cond_1

    .line 9
    .line 10
    return-void

    .line 11
    :cond_1
    const/4 v0, 0x0

    .line 12
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setOriDisplayRectF(Landroid/graphics/RectF;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO00OOO(Lcom/intsig/camscanner/view/ZoomImageView;)V
    .locals 4

    .line 1
    if-eqz p1, :cond_5

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ZoomImageView;->getLastMarginLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_5

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOriDisplayRectF()Landroid/graphics/RectF;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/MyViewPager;->getLastViewPagerLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    if-nez v1, :cond_2

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/MyViewPager;->getLastPaddingLeft()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-nez v1, :cond_2

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/MyViewPager;->getLastPaddingTop()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-nez v1, :cond_2

    .line 44
    .line 45
    return-void

    .line 46
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getBitmapDisplayed()Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    if-nez v1, :cond_3

    .line 51
    .line 52
    const-string p1, "MultiImageEditAdapter"

    .line 53
    .line 54
    const-string v0, "imageView.getBitmapDisplayed() == null"

    .line 55
    .line 56
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    return-void

    .line 60
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getDisplayBoundRect()Landroid/graphics/RectF;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 65
    .line 66
    invoke-virtual {v2}, Lcom/intsig/camscanner/view/MyViewPager;->getLastViewPagerLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    if-eqz v2, :cond_4

    .line 71
    .line 72
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 73
    .line 74
    invoke-virtual {v2}, Lcom/intsig/camscanner/view/MyViewPager;->getLastViewPagerLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 79
    .line 80
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 81
    .line 82
    add-int/2addr v2, v3

    .line 83
    int-to-float v2, v2

    .line 84
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 85
    .line 86
    invoke-virtual {v3}, Lcom/intsig/camscanner/view/MyViewPager;->getLastViewPagerLayoutParams()Landroid/view/ViewGroup$MarginLayoutParams;

    .line 87
    .line 88
    .line 89
    move-result-object v3

    .line 90
    iget v3, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 91
    .line 92
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 93
    .line 94
    add-int/2addr v3, v0

    .line 95
    int-to-float v0, v3

    .line 96
    invoke-virtual {v1, v2, v0}, Landroid/graphics/RectF;->offset(FF)V

    .line 97
    .line 98
    .line 99
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 100
    .line 101
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/MyViewPager;->getLastPaddingLeft()I

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    int-to-float v0, v0

    .line 106
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 107
    .line 108
    invoke-virtual {v2}, Lcom/intsig/camscanner/view/MyViewPager;->getLastPaddingTop()I

    .line 109
    .line 110
    .line 111
    move-result v2

    .line 112
    int-to-float v2, v2

    .line 113
    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/view/ZoomImageView;->setOriDisplayRectF(Landroid/graphics/RectF;)V

    .line 117
    .line 118
    .line 119
    :cond_5
    :goto_0
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;ZILcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇8(ZILcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private oo88o8O(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 9

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    if-nez p3, :cond_1

    .line 7
    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O888o0o(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;)V

    .line 9
    .line 10
    .line 11
    return-void

    .line 12
    :cond_1
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0OOo〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const-string v1, "MultiImageEditAdapter"

    .line 17
    .line 18
    const/16 v2, 0x8

    .line 19
    .line 20
    if-eqz v0, :cond_2

    .line 21
    .line 22
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 23
    .line 24
    if-eq p2, v0, :cond_3

    .line 25
    .line 26
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v3, "hideAnimalView position: "

    .line 32
    .line 33
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    :cond_3
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 52
    .line 53
    const/4 v3, 0x0

    .line 54
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 58
    .line 59
    iget-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇8o8o〇:Z

    .line 60
    .line 61
    const/4 v5, 0x4

    .line 62
    if-nez v4, :cond_4

    .line 63
    .line 64
    iget v4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 65
    .line 66
    if-ne v4, p2, :cond_4

    .line 67
    .line 68
    const/4 v4, 0x0

    .line 69
    goto :goto_0

    .line 70
    :cond_4
    const/4 v4, 0x4

    .line 71
    :goto_0
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 72
    .line 73
    .line 74
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 75
    .line 76
    const/4 v4, 0x1

    .line 77
    if-ne v0, p2, :cond_5

    .line 78
    .line 79
    const/4 v0, 0x1

    .line 80
    goto :goto_1

    .line 81
    :cond_5
    const/4 v0, 0x0

    .line 82
    :goto_1
    invoke-direct {p0, p1, v0, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->ooo〇8oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 83
    .line 84
    .line 85
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 86
    .line 87
    if-ne v0, p2, :cond_6

    .line 88
    .line 89
    const/4 v3, 0x1

    .line 90
    :cond_6
    invoke-direct {p0, p1, v3, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OOO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 91
    .line 92
    .line 93
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->oO80:Landroid/view/View;

    .line 94
    .line 95
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 96
    .line 97
    .line 98
    iget p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO0880O:I

    .line 99
    .line 100
    if-ne p2, v4, :cond_7

    .line 101
    .line 102
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇888()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    if-eqz v0, :cond_7

    .line 111
    .line 112
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇888()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object p2

    .line 116
    move-object v6, p2

    .line 117
    const/4 v2, 0x1

    .line 118
    goto/16 :goto_4

    .line 119
    .line 120
    :cond_7
    if-ne p2, v4, :cond_8

    .line 121
    .line 122
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 123
    .line 124
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 125
    .line 126
    .line 127
    move-result p2

    .line 128
    if-eqz p2, :cond_8

    .line 129
    .line 130
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 131
    .line 132
    const/4 v2, 0x2

    .line 133
    :goto_2
    move-object v6, p2

    .line 134
    goto :goto_4

    .line 135
    :cond_8
    iget-boolean p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->Ooo08:Z

    .line 136
    .line 137
    const/4 v0, 0x3

    .line 138
    if-nez p2, :cond_9

    .line 139
    .line 140
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 141
    .line 142
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 143
    .line 144
    .line 145
    move-result p2

    .line 146
    if-eqz p2, :cond_9

    .line 147
    .line 148
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 149
    .line 150
    :goto_3
    move-object v6, p2

    .line 151
    const/4 v2, 0x3

    .line 152
    goto :goto_4

    .line 153
    :cond_9
    iget-boolean p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->Ooo08:Z

    .line 154
    .line 155
    if-eqz p2, :cond_a

    .line 156
    .line 157
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 158
    .line 159
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 160
    .line 161
    .line 162
    move-result p2

    .line 163
    if-eqz p2, :cond_a

    .line 164
    .line 165
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 166
    .line 167
    goto :goto_3

    .line 168
    :cond_a
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 169
    .line 170
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 171
    .line 172
    .line 173
    move-result p2

    .line 174
    if-eqz p2, :cond_b

    .line 175
    .line 176
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 177
    .line 178
    move-object v6, p2

    .line 179
    const/4 v2, 0x4

    .line 180
    goto :goto_4

    .line 181
    :cond_b
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 182
    .line 183
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 184
    .line 185
    .line 186
    move-result p2

    .line 187
    if-eqz p2, :cond_c

    .line 188
    .line 189
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 190
    .line 191
    const/4 v2, 0x5

    .line 192
    goto :goto_2

    .line 193
    :cond_c
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oO80()Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object p2

    .line 197
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 198
    .line 199
    .line 200
    move-result p2

    .line 201
    if-eqz p2, :cond_d

    .line 202
    .line 203
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oO80()Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object p2

    .line 207
    const/4 v2, 0x6

    .line 208
    goto :goto_2

    .line 209
    :cond_d
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇80〇808〇O()Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object p2

    .line 213
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 214
    .line 215
    .line 216
    move-result p2

    .line 217
    if-eqz p2, :cond_e

    .line 218
    .line 219
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇80〇808〇O()Ljava/lang/String;

    .line 220
    .line 221
    .line 222
    move-result-object p2

    .line 223
    const/4 v2, 0x7

    .line 224
    goto :goto_2

    .line 225
    :cond_e
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 226
    .line 227
    goto :goto_2

    .line 228
    :goto_4
    new-instance p2, Ljava/lang/StringBuilder;

    .line 229
    .line 230
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 231
    .line 232
    .line 233
    const-string v0, "bindThumbData testFlag="

    .line 234
    .line 235
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 239
    .line 240
    .line 241
    const-string v0, "; path="

    .line 242
    .line 243
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    .line 245
    .line 246
    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    .line 248
    .line 249
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 250
    .line 251
    .line 252
    move-result-object p2

    .line 253
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    .line 255
    .line 256
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 257
    .line 258
    invoke-static {v6, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 259
    .line 260
    .line 261
    move-result v8

    .line 262
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oo88o8O:Ljava/util/HashSet;

    .line 263
    .line 264
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 265
    .line 266
    invoke-virtual {p2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 267
    .line 268
    .line 269
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 270
    .line 271
    invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 272
    .line 273
    .line 274
    move-result-object p2

    .line 275
    if-eqz p2, :cond_10

    .line 276
    .line 277
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oo〇:Ljava/util/HashMap;

    .line 278
    .line 279
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 280
    .line 281
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 282
    .line 283
    .line 284
    move-result p2

    .line 285
    if-eqz p2, :cond_f

    .line 286
    .line 287
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 288
    .line 289
    invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 290
    .line 291
    .line 292
    move-result-object p2

    .line 293
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oo〇:Ljava/util/HashMap;

    .line 294
    .line 295
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 296
    .line 297
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    .line 299
    .line 300
    move-result-object v0

    .line 301
    check-cast v0, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 302
    .line 303
    invoke-virtual {p2, v0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 304
    .line 305
    .line 306
    :cond_f
    new-instance p2, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$1;

    .line 307
    .line 308
    move-object v3, p2

    .line 309
    move-object v4, p0

    .line 310
    move-object v5, p1

    .line 311
    move-object v7, p3

    .line 312
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$1;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Z)V

    .line 313
    .line 314
    .line 315
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oo〇:Ljava/util/HashMap;

    .line 316
    .line 317
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 318
    .line 319
    invoke-virtual {p3, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    .line 321
    .line 322
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 323
    .line 324
    invoke-virtual {p3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 325
    .line 326
    .line 327
    move-result-object p3

    .line 328
    invoke-virtual {p3, p2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 329
    .line 330
    .line 331
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 332
    .line 333
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 334
    .line 335
    .line 336
    goto :goto_5

    .line 337
    :cond_10
    invoke-direct {p0, p1, v6, p3, v8}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo8Oo00oo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Z)V

    .line 338
    .line 339
    .line 340
    :goto_5
    return-void
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private ooo〇8oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "tryShowLoadingView show:"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "MultiImageEditAdapter"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OoO8:Z

    .line 24
    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    if-eqz p2, :cond_1

    .line 28
    .line 29
    if-eqz p3, :cond_1

    .line 30
    .line 31
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O8o08O()Z

    .line 32
    .line 33
    .line 34
    move-result p2

    .line 35
    if-nez p2, :cond_0

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 39
    .line 40
    const/4 p2, 0x0

    .line 41
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_1
    :goto_0
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 46
    .line 47
    const/4 p2, 0x4

    .line 48
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    :goto_1
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic oo〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-eqz p2, :cond_2

    .line 5
    .line 6
    if-eqz p3, :cond_2

    .line 7
    .line 8
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    if-nez p2, :cond_0

    .line 13
    .line 14
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

    .line 15
    .line 16
    invoke-interface {p2, p1, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;->O0o8〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Z)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    .line 21
    .line 22
    .line 23
    move-result p2

    .line 24
    if-eq p2, v0, :cond_1

    .line 25
    .line 26
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    .line 27
    .line 28
    .line 29
    move-result p2

    .line 30
    const/4 p3, 0x3

    .line 31
    if-ne p2, p3, :cond_2

    .line 32
    .line 33
    :cond_1
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

    .line 34
    .line 35
    const/4 p3, 0x0

    .line 36
    invoke-interface {p2, p1, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;->O0o8〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Z)V

    .line 37
    .line 38
    .line 39
    :cond_2
    :goto_0
    return v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O8〇o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o〇0OOo〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Z
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget v2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 10
    .line 11
    const/16 v3, -0xd

    .line 12
    .line 13
    const/4 v4, 0x1

    .line 14
    if-ne v2, v3, :cond_1

    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/4 v2, 0x0

    .line 19
    :goto_0
    invoke-virtual {v1, v2}, Lcom/intsig/tsapp/sync/AppConfigJson;->useSuperFilterNewLoading(Z)Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-eqz v1, :cond_2

    .line 24
    .line 25
    iget p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 26
    .line 27
    const/16 v1, -0xc

    .line 28
    .line 29
    if-ne p1, v1, :cond_2

    .line 30
    .line 31
    const/4 v0, 0x1

    .line 32
    :cond_2
    return v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic o〇8(ZILcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V
    .locals 3

    .line 1
    if-ltz p4, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-ge p4, v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 12
    .line 13
    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 22
    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0OOo〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-eqz p1, :cond_1

    .line 30
    .line 31
    iget p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 32
    .line 33
    if-ne p4, p1, :cond_1

    .line 34
    .line 35
    invoke-direct {p0, p3, p4}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oo〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 v0, 0x0

    .line 40
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0000OOO()Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    const/4 v1, 0x4

    .line 45
    const/4 v2, 0x0

    .line 46
    if-eqz p1, :cond_2

    .line 47
    .line 48
    if-eq p2, p4, :cond_3

    .line 49
    .line 50
    :cond_2
    iget p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 51
    .line 52
    if-eq p1, p4, :cond_4

    .line 53
    .line 54
    :cond_3
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 55
    .line 56
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 57
    .line 58
    .line 59
    invoke-direct {p0, p3, v2, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->ooo〇8oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0, p3, v2, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OOO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 63
    .line 64
    .line 65
    iget-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0〇O0088o:Z

    .line 66
    .line 67
    if-eqz p1, :cond_6

    .line 68
    .line 69
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 70
    .line 71
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 72
    .line 73
    .line 74
    goto :goto_2

    .line 75
    :cond_4
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 76
    .line 77
    iget-boolean p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇8o8o〇:Z

    .line 78
    .line 79
    if-eqz p2, :cond_5

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_5
    const/4 v1, 0x0

    .line 83
    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 84
    .line 85
    .line 86
    const/4 p1, 0x1

    .line 87
    invoke-direct {p0, p3, p1, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->ooo〇8oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 88
    .line 89
    .line 90
    invoke-direct {p0, p3, p1, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OOO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 91
    .line 92
    .line 93
    iget-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0〇O0088o:Z

    .line 94
    .line 95
    if-eqz p1, :cond_6

    .line 96
    .line 97
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 98
    .line 99
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 100
    .line 101
    .line 102
    :cond_6
    :goto_2
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic o〇〇0〇(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-interface {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;->o08O()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇00(IILjava/lang/String;Z)Lcom/bumptech/glide/request/RequestOptions;
    .locals 2

    .line 1
    new-instance v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;

    .line 15
    .line 16
    invoke-direct {v1, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇0(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 20
    .line 21
    .line 22
    move-result-object p3

    .line 23
    check-cast p3, Lcom/bumptech/glide/request/RequestOptions;

    .line 24
    .line 25
    if-nez p4, :cond_0

    .line 26
    .line 27
    if-lez p1, :cond_0

    .line 28
    .line 29
    if-lez p2, :cond_0

    .line 30
    .line 31
    invoke-virtual {p3, p1, p2}, Lcom/bumptech/glide/request/BaseRequestOptions;->O000(II)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    move-object p3, p1

    .line 36
    check-cast p3, Lcom/bumptech/glide/request/RequestOptions;

    .line 37
    .line 38
    :cond_0
    return-object p3
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic 〇00〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;->o〇0〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/view/ImageViewTouchBase;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oO(Lcom/intsig/camscanner/view/ImageViewTouchBase;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oo〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇〇0〇(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0OOo〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O888o0o(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;)V
    .locals 3

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-direct {p0, p1, v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->ooo〇8oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0, p1, v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OOO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 19
    .line 20
    .line 21
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 22
    .line 23
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->oO80:Landroid/view/View;

    .line 27
    .line 28
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->oO80:Landroid/view/View;

    .line 32
    .line 33
    new-instance v0, LoOO8/oo88o8O;

    .line 34
    .line 35
    invoke-direct {v0, p0}, LoOO8/oo88o8O;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇00〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo8Oo00oo(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private 〇O〇80o08O(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$TraverseViewHolderCallback;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string p1, "MultiImageEditAdapter"

    .line 10
    .line 11
    const-string v0, "instantiateSparseArray.size() == 0"

    .line 12
    .line 13
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 19
    .line 20
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-ge v0, v1, :cond_3

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 27
    .line 28
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 33
    .line 34
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    check-cast v2, Landroid/view/View;

    .line 39
    .line 40
    if-nez v2, :cond_1

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    instance-of v3, v2, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 48
    .line 49
    if-eqz v3, :cond_2

    .line 50
    .line 51
    if-eqz p1, :cond_2

    .line 52
    .line 53
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 54
    .line 55
    invoke-interface {p1, v2, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$TraverseViewHolderCallback;->〇080(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V

    .line 56
    .line 57
    .line 58
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_3
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic 〇o(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o0O0(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇oo〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    if-ltz p2, :cond_c

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-lt p2, v0, :cond_1

    .line 13
    .line 14
    goto/16 :goto_1

    .line 15
    .line 16
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 23
    .line 24
    if-nez v0, :cond_2

    .line 25
    .line 26
    return-void

    .line 27
    :cond_2
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 28
    .line 29
    new-instance v2, LoOO8/〇0〇O0088o;

    .line 30
    .line 31
    invoke-direct {v2, p0, v0}, LoOO8/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    .line 36
    .line 37
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 38
    .line 39
    new-instance v2, LoOO8/OoO8;

    .line 40
    .line 41
    invoke-direct {v2, p0, v0}, LoOO8/OoO8;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 45
    .line 46
    .line 47
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 48
    .line 49
    new-instance v2, LoOO8/o800o8O;

    .line 50
    .line 51
    invoke-direct {v2, p0, v0}, LoOO8/o800o8O;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    .line 56
    .line 57
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇80〇808〇O:Landroid/view/View;

    .line 58
    .line 59
    new-instance v2, LoOO8/〇O888o0o;

    .line 60
    .line 61
    invoke-direct {v2, p0, v0}, LoOO8/〇O888o0o;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    .line 66
    .line 67
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;

    .line 68
    .line 69
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;)V

    .line 70
    .line 71
    .line 72
    iput p2, v1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ZoomImageViewTag;->〇080:I

    .line 73
    .line 74
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 75
    .line 76
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 77
    .line 78
    .line 79
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 80
    .line 81
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇808〇:Z

    .line 82
    .line 83
    const/4 v3, 0x0

    .line 84
    const/4 v4, 0x1

    .line 85
    if-eqz v2, :cond_3

    .line 86
    .line 87
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 88
    .line 89
    invoke-virtual {v2}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;->animalIsRun()Z

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    if-nez v2, :cond_3

    .line 94
    .line 95
    const/4 v2, 0x1

    .line 96
    goto :goto_0

    .line 97
    :cond_3
    const/4 v2, 0x0

    .line 98
    :goto_0
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ZoomImageView;->setSupportScale(Z)V

    .line 99
    .line 100
    .line 101
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 102
    .line 103
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O8ooOoo〇:Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 104
    .line 105
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ZoomImageView;->setZoomImageViewListener(Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;)V

    .line 106
    .line 107
    .line 108
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 109
    .line 110
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/view/ZoomImageView;->setDisableScrollWhenScale(Z)V

    .line 111
    .line 112
    .line 113
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 114
    .line 115
    const/high16 v2, 0x40000000    # 2.0f

    .line 116
    .line 117
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ZoomImageView;->setMaxZoomScale(F)V

    .line 118
    .line 119
    .line 120
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 121
    .line 122
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ZoomImageView;->setDoubleTapScale(F)V

    .line 123
    .line 124
    .line 125
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 126
    .line 127
    invoke-direct {p0, p1, p2, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oo88o8O(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 128
    .line 129
    .line 130
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 131
    .line 132
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    instance-of v2, v1, Ljava/lang/Runnable;

    .line 137
    .line 138
    if-eqz v2, :cond_4

    .line 139
    .line 140
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 141
    .line 142
    check-cast v1, Ljava/lang/Runnable;

    .line 143
    .line 144
    invoke-virtual {v2, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 145
    .line 146
    .line 147
    :cond_4
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 148
    .line 149
    if-nez v1, :cond_5

    .line 150
    .line 151
    return-void

    .line 152
    :cond_5
    iget v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 153
    .line 154
    sget v2, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇080:I

    .line 155
    .line 156
    if-ne v1, v2, :cond_6

    .line 157
    .line 158
    const/4 v3, 0x1

    .line 159
    :cond_6
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇80〇808〇O:I

    .line 160
    .line 161
    const-string v2, "MultiImageEditAdapter"

    .line 162
    .line 163
    const/4 v4, -0x1

    .line 164
    if-ltz v1, :cond_8

    .line 165
    .line 166
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 167
    .line 168
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇()Z

    .line 169
    .line 170
    .line 171
    move-result v1

    .line 172
    if-eqz v1, :cond_8

    .line 173
    .line 174
    if-eqz v3, :cond_8

    .line 175
    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    .line 177
    .line 178
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .line 180
    .line 181
    const-string v3, "horizontalAnimationPosition = "

    .line 182
    .line 183
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    iget v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇80〇808〇O:I

    .line 187
    .line 188
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    .line 197
    .line 198
    iput v4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇80〇808〇O:I

    .line 199
    .line 200
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O〇8O8〇008:Lcom/intsig/camscanner/multiimageedit/animation/MultiPreviewHorizontalAnimation;

    .line 201
    .line 202
    if-nez v1, :cond_7

    .line 203
    .line 204
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/animation/MultiPreviewHorizontalAnimation;

    .line 205
    .line 206
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 207
    .line 208
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/multiimageedit/animation/MultiPreviewHorizontalAnimation;-><init>(Landroid/app/Activity;)V

    .line 209
    .line 210
    .line 211
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O〇8O8〇008:Lcom/intsig/camscanner/multiimageedit/animation/MultiPreviewHorizontalAnimation;

    .line 212
    .line 213
    :cond_7
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o0O0(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 214
    .line 215
    .line 216
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O〇8O8〇008:Lcom/intsig/camscanner/multiimageedit/animation/MultiPreviewHorizontalAnimation;

    .line 217
    .line 218
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 219
    .line 220
    const-wide/16 v1, 0x64

    .line 221
    .line 222
    invoke-virtual {p2, v0, v1, v2}, Lcom/intsig/camscanner/multiimageedit/animation/MultiPreviewHorizontalAnimation;->〇〇888(Landroid/widget/ImageView;J)V

    .line 223
    .line 224
    .line 225
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 226
    .line 227
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O〇8O8〇008:Lcom/intsig/camscanner/multiimageedit/animation/MultiPreviewHorizontalAnimation;

    .line 228
    .line 229
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/view/ZoomImageView;->setImageAreaAnimationCallback(Lcom/intsig/camscanner/view/ZoomImageView$ImageAreaAnimationCallback;)V

    .line 230
    .line 231
    .line 232
    return-void

    .line 233
    :cond_8
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oO80:I

    .line 234
    .line 235
    if-ne p2, v1, :cond_b

    .line 236
    .line 237
    iput v4, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oO80:I

    .line 238
    .line 239
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 240
    .line 241
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 242
    .line 243
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 244
    .line 245
    .line 246
    move-result v1

    .line 247
    new-instance v4, Ljava/lang/StringBuilder;

    .line 248
    .line 249
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    .line 251
    .line 252
    const-string v5, "animationPosition = "

    .line 253
    .line 254
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    .line 256
    .line 257
    iget v5, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oO80:I

    .line 258
    .line 259
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 260
    .line 261
    .line 262
    const-string v5, "; isNotPaperPage="

    .line 263
    .line 264
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    .line 266
    .line 267
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    const-string v5, "; isPageFinished="

    .line 271
    .line 272
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    .line 274
    .line 275
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 276
    .line 277
    .line 278
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 279
    .line 280
    .line 281
    move-result-object v4

    .line 282
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    .line 284
    .line 285
    if-nez v1, :cond_9

    .line 286
    .line 287
    if-eqz v3, :cond_b

    .line 288
    .line 289
    :cond_9
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇8O0〇8:Z

    .line 290
    .line 291
    if-eqz v1, :cond_b

    .line 292
    .line 293
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇00:Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;

    .line 294
    .line 295
    if-nez v1, :cond_a

    .line 296
    .line 297
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;

    .line 298
    .line 299
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 300
    .line 301
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;-><init>(Landroid/app/Activity;)V

    .line 302
    .line 303
    .line 304
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇00:Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;

    .line 305
    .line 306
    :cond_a
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇00:Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;

    .line 307
    .line 308
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/adapter/〇o〇;

    .line 309
    .line 310
    invoke-direct {v2, p0, p1, p2, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/〇o〇;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 311
    .line 312
    .line 313
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;->Oooo8o0〇(Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation$AnimatorUpdateListener;)V

    .line 314
    .line 315
    .line 316
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇00:Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;

    .line 317
    .line 318
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 319
    .line 320
    const-wide/16 v1, 0x12c

    .line 321
    .line 322
    invoke-virtual {p2, v0, v1, v2}, Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;->OO0o〇〇〇〇0(Landroid/widget/ImageView;J)V

    .line 323
    .line 324
    .line 325
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 326
    .line 327
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇00:Lcom/intsig/camscanner/multiimageedit/animation/ImageScannerAnimation;

    .line 328
    .line 329
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/view/ZoomImageView;->setImageAreaAnimationCallback(Lcom/intsig/camscanner/view/ZoomImageView$ImageAreaAnimationCallback;)V

    .line 330
    .line 331
    .line 332
    return-void

    .line 333
    :cond_b
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o0O0(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 334
    .line 335
    .line 336
    :cond_c
    :goto_1
    return-void
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private 〇〇0o(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Z)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string p1, "MultiImageEditAdapter"

    .line 10
    .line 11
    const-string p2, "activity.isFinishing"

    .line 12
    .line 13
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    invoke-static {p2, v0}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    if-eqz p2, :cond_10

    .line 23
    .line 24
    aget v1, p2, v0

    .line 25
    .line 26
    if-eqz v1, :cond_10

    .line 27
    .line 28
    const/4 v1, 0x1

    .line 29
    aget p2, p2, v1

    .line 30
    .line 31
    if-nez p2, :cond_1

    .line 32
    .line 33
    goto/16 :goto_0

    .line 34
    .line 35
    :cond_1
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 36
    .line 37
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    .line 38
    .line 39
    .line 40
    move-result p2

    .line 41
    if-lez p2, :cond_10

    .line 42
    .line 43
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 44
    .line 45
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    if-gtz p2, :cond_2

    .line 50
    .line 51
    goto/16 :goto_0

    .line 52
    .line 53
    :cond_2
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 54
    .line 55
    invoke-virtual {p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getBitmapDisplayed()Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    if-eqz p2, :cond_10

    .line 60
    .line 61
    invoke-virtual {p2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    if-eqz p2, :cond_10

    .line 66
    .line 67
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 68
    .line 69
    invoke-virtual {p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getOriDisplayRectF()Landroid/graphics/RectF;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    if-eqz p2, :cond_3

    .line 74
    .line 75
    goto/16 :goto_0

    .line 76
    .line 77
    :cond_3
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 78
    .line 79
    invoke-virtual {p2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getDisplayBoundRect()Landroid/graphics/RectF;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 84
    .line 85
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 90
    .line 91
    iget v2, p2, Landroid/graphics/RectF;->left:F

    .line 92
    .line 93
    float-to-int v2, v2

    .line 94
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 95
    .line 96
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    .line 97
    .line 98
    .line 99
    move-result v3

    .line 100
    div-int/lit8 v3, v3, 0x2

    .line 101
    .line 102
    sub-int/2addr v2, v3

    .line 103
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 104
    .line 105
    iget v2, p2, Landroid/graphics/RectF;->top:F

    .line 106
    .line 107
    float-to-int v2, v2

    .line 108
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 109
    .line 110
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    .line 111
    .line 112
    .line 113
    move-result v3

    .line 114
    div-int/lit8 v3, v3, 0x2

    .line 115
    .line 116
    sub-int/2addr v2, v3

    .line 117
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 118
    .line 119
    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 120
    .line 121
    if-gez v3, :cond_4

    .line 122
    .line 123
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 124
    .line 125
    :cond_4
    if-gez v2, :cond_5

    .line 126
    .line 127
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 128
    .line 129
    :cond_5
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 130
    .line 131
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    .line 133
    .line 134
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OoO8:Z

    .line 135
    .line 136
    const/high16 v2, 0x41000000    # 8.0f

    .line 137
    .line 138
    if-eqz v1, :cond_8

    .line 139
    .line 140
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 141
    .line 142
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 147
    .line 148
    iget v3, p2, Landroid/graphics/RectF;->right:F

    .line 149
    .line 150
    float-to-int v3, v3

    .line 151
    iget-object v4, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 152
    .line 153
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    .line 154
    .line 155
    .line 156
    move-result v4

    .line 157
    sub-int/2addr v3, v4

    .line 158
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 159
    .line 160
    .line 161
    move-result v4

    .line 162
    sub-int/2addr v3, v4

    .line 163
    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 164
    .line 165
    iget v3, p2, Landroid/graphics/RectF;->top:F

    .line 166
    .line 167
    float-to-int v3, v3

    .line 168
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 169
    .line 170
    .line 171
    move-result v4

    .line 172
    add-int/2addr v3, v4

    .line 173
    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 174
    .line 175
    if-gez v3, :cond_6

    .line 176
    .line 177
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 178
    .line 179
    :cond_6
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 180
    .line 181
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    .line 182
    .line 183
    .line 184
    move-result v3

    .line 185
    if-lez v3, :cond_7

    .line 186
    .line 187
    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 188
    .line 189
    iget-object v5, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 190
    .line 191
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    .line 192
    .line 193
    .line 194
    move-result v5

    .line 195
    add-int/2addr v4, v5

    .line 196
    if-le v4, v3, :cond_7

    .line 197
    .line 198
    iget-object v4, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 199
    .line 200
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    .line 201
    .line 202
    .line 203
    move-result v4

    .line 204
    sub-int/2addr v3, v4

    .line 205
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 206
    .line 207
    .line 208
    move-result v4

    .line 209
    sub-int/2addr v3, v4

    .line 210
    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 211
    .line 212
    :cond_7
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 213
    .line 214
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 215
    .line 216
    .line 217
    :cond_8
    if-eqz p3, :cond_9

    .line 218
    .line 219
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 220
    .line 221
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 222
    .line 223
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 224
    .line 225
    .line 226
    move-result-object v1

    .line 227
    invoke-virtual {p3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    .line 229
    .line 230
    :cond_9
    iget-boolean p3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o800o8O:Z

    .line 231
    .line 232
    if-eqz p3, :cond_d

    .line 233
    .line 234
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 235
    .line 236
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 237
    .line 238
    .line 239
    move-result-object p3

    .line 240
    check-cast p3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 241
    .line 242
    iget v1, p2, Landroid/graphics/RectF;->right:F

    .line 243
    .line 244
    float-to-int v1, v1

    .line 245
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 246
    .line 247
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    .line 248
    .line 249
    .line 250
    move-result v3

    .line 251
    sub-int/2addr v1, v3

    .line 252
    const/high16 v3, 0x3f800000    # 1.0f

    .line 253
    .line 254
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 255
    .line 256
    .line 257
    move-result v4

    .line 258
    add-int/2addr v1, v4

    .line 259
    iput v1, p3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 260
    .line 261
    iget v1, p2, Landroid/graphics/RectF;->top:F

    .line 262
    .line 263
    float-to-int v1, v1

    .line 264
    iget-object v4, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 265
    .line 266
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    .line 267
    .line 268
    .line 269
    move-result v4

    .line 270
    sub-int/2addr v1, v4

    .line 271
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 272
    .line 273
    .line 274
    move-result v2

    .line 275
    sub-int/2addr v1, v2

    .line 276
    iput v1, p3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 277
    .line 278
    if-gez v1, :cond_a

    .line 279
    .line 280
    iput v0, p3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 281
    .line 282
    :cond_a
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 283
    .line 284
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 285
    .line 286
    .line 287
    move-result v1

    .line 288
    if-lez v1, :cond_b

    .line 289
    .line 290
    iget v2, p3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 291
    .line 292
    iget-object v4, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 293
    .line 294
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    .line 295
    .line 296
    .line 297
    move-result v4

    .line 298
    add-int/2addr v2, v4

    .line 299
    if-le v2, v1, :cond_b

    .line 300
    .line 301
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 302
    .line 303
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    .line 304
    .line 305
    .line 306
    move-result v2

    .line 307
    sub-int v2, v1, v2

    .line 308
    .line 309
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 310
    .line 311
    .line 312
    move-result v3

    .line 313
    add-int/2addr v2, v3

    .line 314
    iput v2, p3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 315
    .line 316
    :cond_b
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->showOriginImgCompareFun()Z

    .line 317
    .line 318
    .line 319
    move-result v2

    .line 320
    if-eqz v2, :cond_c

    .line 321
    .line 322
    if-lez v1, :cond_c

    .line 323
    .line 324
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 325
    .line 326
    const v3, 0x7f081096

    .line 327
    .line 328
    .line 329
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 330
    .line 331
    .line 332
    iput v0, p3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 333
    .line 334
    int-to-float v1, v1

    .line 335
    iget v2, p2, Landroid/graphics/RectF;->right:F

    .line 336
    .line 337
    sub-float/2addr v1, v2

    .line 338
    float-to-int v1, v1

    .line 339
    iput v1, p3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 340
    .line 341
    :cond_c
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 342
    .line 343
    invoke-virtual {v1, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 344
    .line 345
    .line 346
    :cond_d
    iget-boolean p3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0〇O0088o:Z

    .line 347
    .line 348
    if-eqz p3, :cond_10

    .line 349
    .line 350
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 351
    .line 352
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 353
    .line 354
    .line 355
    move-result-object p3

    .line 356
    check-cast p3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 357
    .line 358
    iget v1, p2, Landroid/graphics/RectF;->right:F

    .line 359
    .line 360
    float-to-int v1, v1

    .line 361
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 362
    .line 363
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    .line 364
    .line 365
    .line 366
    move-result v2

    .line 367
    div-int/lit8 v2, v2, 0x2

    .line 368
    .line 369
    sub-int/2addr v1, v2

    .line 370
    iput v1, p3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 371
    .line 372
    iget p2, p2, Landroid/graphics/RectF;->top:F

    .line 373
    .line 374
    float-to-int p2, p2

    .line 375
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 376
    .line 377
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    .line 378
    .line 379
    .line 380
    move-result v1

    .line 381
    div-int/lit8 v1, v1, 0x2

    .line 382
    .line 383
    sub-int/2addr p2, v1

    .line 384
    iput p2, p3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 385
    .line 386
    if-gez p2, :cond_e

    .line 387
    .line 388
    iput v0, p3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 389
    .line 390
    :cond_e
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 391
    .line 392
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    .line 393
    .line 394
    .line 395
    move-result p2

    .line 396
    if-lez p2, :cond_f

    .line 397
    .line 398
    iget v0, p3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 399
    .line 400
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 401
    .line 402
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 403
    .line 404
    .line 405
    move-result v1

    .line 406
    add-int/2addr v0, v1

    .line 407
    if-le v0, p2, :cond_f

    .line 408
    .line 409
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 410
    .line 411
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 412
    .line 413
    .line 414
    move-result v0

    .line 415
    sub-int/2addr p2, v0

    .line 416
    iput p2, p3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 417
    .line 418
    :cond_f
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 419
    .line 420
    invoke-virtual {p1, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 421
    .line 422
    .line 423
    :cond_10
    :goto_0
    return-void
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;)Ljava/util/HashSet;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oo88o8O:Ljava/util/HashSet;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o0ooO(Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇0o(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇〇〇0〇〇0(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 8
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-wide v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->o〇0:J

    .line 5
    .line 6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 7
    .line 8
    .line 9
    move-result-wide v2

    .line 10
    new-instance v4, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v5, "showProgressStatus, progressStatus="

    .line 16
    .line 17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v5, "; lastShowLoadingTime="

    .line 24
    .line 25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v5, "; currentTime="

    .line 32
    .line 33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    const-string v5, "MultiImageEditAdapter"

    .line 44
    .line 45
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    const-wide/16 v6, 0x0

    .line 49
    .line 50
    cmp-long v4, v0, v6

    .line 51
    .line 52
    if-gez v4, :cond_1

    .line 53
    .line 54
    if-eqz p1, :cond_1

    .line 55
    .line 56
    iput-wide v2, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->o〇0:J

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_1
    if-eqz v4, :cond_3

    .line 60
    .line 61
    if-nez p1, :cond_3

    .line 62
    .line 63
    if-lez v4, :cond_2

    .line 64
    .line 65
    sub-long/2addr v2, v0

    .line 66
    goto :goto_0

    .line 67
    :cond_2
    const-wide/16 v2, -0x1

    .line 68
    .line 69
    :goto_0
    iput-wide v6, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->o〇0:J

    .line 70
    .line 71
    new-instance p1, Ljava/lang/StringBuilder;

    .line 72
    .line 73
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .line 75
    .line 76
    const-string p2, "showProgressStatus; currentCost="

    .line 77
    .line 78
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-static {v5, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    cmp-long p1, v2, v6

    .line 92
    .line 93
    if-lez p1, :cond_3

    .line 94
    .line 95
    const/4 p1, 0x2

    .line 96
    new-array p1, p1, [Landroid/util/Pair;

    .line 97
    .line 98
    new-instance p2, Landroid/util/Pair;

    .line 99
    .line 100
    const-string v0, "from"

    .line 101
    .line 102
    const-string v1, "cs_batch_result_filter_switch_filter"

    .line 103
    .line 104
    invoke-direct {p2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 105
    .line 106
    .line 107
    const/4 v0, 0x0

    .line 108
    aput-object p2, p1, v0

    .line 109
    .line 110
    new-instance p2, Landroid/util/Pair;

    .line 111
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    const-string v1, ""

    .line 121
    .line 122
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    const-string v1, "time"

    .line 130
    .line 131
    invoke-direct {p2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 132
    .line 133
    .line 134
    const/4 v0, 0x1

    .line 135
    aput-object p2, p1, v0

    .line 136
    .line 137
    const-string p2, "CSWaiting"

    .line 138
    .line 139
    const-string v0, "show"

    .line 140
    .line 141
    invoke-static {p2, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 142
    .line 143
    .line 144
    :cond_3
    :goto_1
    return-void
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public O000(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇〇〇0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O08000(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇8o8o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8ooOoo〇()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_2

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 19
    .line 20
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 21
    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    iget v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 25
    .line 26
    const/16 v3, 0x8

    .line 27
    .line 28
    if-eq v2, v3, :cond_1

    .line 29
    .line 30
    const/16 v3, 0x9

    .line 31
    .line 32
    if-ne v2, v3, :cond_0

    .line 33
    .line 34
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_2
    return v1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public OO8oO0o〇(Z)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    add-int/lit8 v0, v0, -0x1

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/adapter/〇o00〇〇Oo;

    .line 8
    .line 9
    invoke-direct {v1, p0, p1, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;ZI)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O〇80o08O(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$TraverseViewHolderCallback;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O〇O〇oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Lcom/intsig/adapter/RecyclingPagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 5
    .line 6
    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 7
    .line 8
    .line 9
    instance-of p1, p3, Landroid/view/View;

    .line 10
    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    check-cast p3, Landroid/view/View;

    .line 14
    .line 15
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    instance-of p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 20
    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    check-cast p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 28
    .line 29
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O8〇〇o:Ljava/util/HashMap;

    .line 30
    .line 31
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 32
    .line 33
    invoke-virtual {p2, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oo〇:Ljava/util/HashMap;

    .line 37
    .line 38
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 39
    .line 40
    invoke-virtual {p2, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oo88o8O:Ljava/util/HashSet;

    .line 44
    .line 45
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 46
    .line 47
    invoke-virtual {p2, p3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 51
    .line 52
    invoke-virtual {p2}, Lcom/intsig/camscanner/view/ZoomImageView;->Ooo()V

    .line 53
    .line 54
    .line 55
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 56
    .line 57
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oO(Lcom/intsig/camscanner/view/ImageViewTouchBase;)V

    .line 58
    .line 59
    .line 60
    :cond_0
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 3
    .line 4
    .line 5
    invoke-super {p0, p1, p2}, Lcom/intsig/adapter/RecyclingPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    instance-of v0, p1, Landroid/view/View;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 14
    .line 15
    move-object v1, p1

    .line 16
    check-cast v1, Landroid/view/View;

    .line 17
    .line 18
    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-object p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/adapter/RecyclingPagerAdapter;->notifyDataSetChanged()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/adapter/〇080;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/adapter/〇080;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O〇80o08O(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$TraverseViewHolderCallback;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8oO〇(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0000OOO()Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-eqz p1, :cond_1

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O8o08O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 23
    .line 24
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o〇8oOO88(Lcom/intsig/camscanner/view/MyViewPager;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇8O0〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇O8〇〇o(I)Lcom/intsig/camscanner/view/ZoomImageView;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    const-string p1, "MultiImageEditAdapter"

    .line 11
    .line 12
    const-string v0, "instantiateSparseArray.size() == 0"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-object v1

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oo08:Landroid/util/SparseArray;

    .line 19
    .line 20
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    check-cast p1, Landroid/view/View;

    .line 25
    .line 26
    if-nez p1, :cond_1

    .line 27
    .line 28
    return-object v1

    .line 29
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    instance-of v0, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 34
    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    check-cast p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 38
    .line 39
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 40
    .line 41
    return-object p1

    .line 42
    :cond_2
    return-object v1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇0()V
    .locals 2

    .line 1
    new-instance v0, Landroid/util/DisplayMetrics;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 7
    .line 8
    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 17
    .line 18
    .line 19
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 20
    .line 21
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO0o〇〇:I

    .line 22
    .line 23
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 24
    .line 25
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->Oooo8o0〇:I

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇0000OOO()Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O00:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O8:I

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    if-gtz v0, :cond_1

    .line 11
    .line 12
    return v2

    .line 13
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iget v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O8:I

    .line 20
    .line 21
    if-ge v0, v3, :cond_2

    .line 22
    .line 23
    return v2

    .line 24
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 25
    .line 26
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    sub-int/2addr v3, v2

    .line 31
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 36
    .line 37
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O8o08O:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 38
    .line 39
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->equals(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_3

    .line 44
    .line 45
    return v2

    .line 46
    :cond_3
    return v1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇08O8o〇0(Lcom/intsig/camscanner/view/ZoomImageView;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Z
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p2, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O8〇〇o:Ljava/util/HashMap;

    .line 6
    .line 7
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 12
    .line 13
    invoke-static {v1, p2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    return v0

    .line 20
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O8〇〇o:Ljava/util/HashMap;

    .line 21
    .line 22
    invoke-virtual {p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->clone()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    check-cast p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 27
    .line 28
    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    const/4 p2, 0x2

    .line 32
    new-array v1, p2, [I

    .line 33
    .line 34
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 35
    .line 36
    .line 37
    aget v2, v1, v0

    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    div-int/2addr v3, p2

    .line 44
    add-int/2addr v2, v3

    .line 45
    const/4 v3, 0x1

    .line 46
    aget v1, v1, v3

    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    div-int/2addr v4, p2

    .line 53
    add-int/2addr v1, v4

    .line 54
    if-lez v2, :cond_2

    .line 55
    .line 56
    if-lez v1, :cond_2

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 59
    .line 60
    .line 61
    move-result p2

    .line 62
    const/high16 v1, 0x3f800000    # 1.0f

    .line 63
    .line 64
    invoke-static {p2, v1}, Ljava/lang/Float;->compare(FF)I

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    if-eqz p2, :cond_2

    .line 69
    .line 70
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O8ooOoo〇:Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;

    .line 71
    .line 72
    invoke-interface {p2, p1, v1}, Lcom/intsig/camscanner/view/ZoomImageView$ZoomImageViewListener;->O8(Lcom/intsig/camscanner/view/ZoomImageView;F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .line 74
    .line 75
    return v3

    .line 76
    :catch_0
    move-exception p1

    .line 77
    const-string p2, "MultiImageEditAdapter"

    .line 78
    .line 79
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    .line 81
    .line 82
    :cond_2
    return v0
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇8(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->oO80:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇80(Landroid/view/View;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇O888o0o:Landroid/view/View;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8〇0〇o〇O(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇80〇808〇O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    if-nez p2, :cond_3

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 4
    .line 5
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    const v0, 0x7f0d0459

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    new-instance p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    invoke-direct {p3, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;-><init>(LoOO8/〇oo〇;)V

    .line 21
    .line 22
    .line 23
    iput-object p2, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 24
    .line 25
    const v0, 0x7f0a093b

    .line 26
    .line 27
    .line 28
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    check-cast v0, Lcom/intsig/camscanner/view/ZoomImageView;

    .line 33
    .line 34
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 35
    .line 36
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o00〇〇Oo:Landroid/app/Activity;

    .line 37
    .line 38
    const/16 v3, 0x14

    .line 39
    .line 40
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    int-to-float v2, v2

    .line 45
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setOffset(F)V

    .line 46
    .line 47
    .line 48
    const v0, 0x7f0a0e61

    .line 49
    .line 50
    .line 51
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇o〇:Landroid/view/View;

    .line 56
    .line 57
    const v0, 0x7f0a0cbe

    .line 58
    .line 59
    .line 60
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇80〇808〇O:Landroid/view/View;

    .line 65
    .line 66
    const v0, 0x7f0a08a5

    .line 67
    .line 68
    .line 69
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->O8:Landroid/view/View;

    .line 74
    .line 75
    const v0, 0x7f0a0d33

    .line 76
    .line 77
    .line 78
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 83
    .line 84
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OoO8:Z

    .line 85
    .line 86
    const/16 v3, 0x8

    .line 87
    .line 88
    if-eqz v2, :cond_0

    .line 89
    .line 90
    const/4 v2, 0x0

    .line 91
    goto :goto_0

    .line 92
    :cond_0
    const/16 v2, 0x8

    .line 93
    .line 94
    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 95
    .line 96
    .line 97
    const v0, 0x7f0a0d2a

    .line 98
    .line 99
    .line 100
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 105
    .line 106
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o800o8O:Z

    .line 107
    .line 108
    if-eqz v2, :cond_1

    .line 109
    .line 110
    const/4 v2, 0x0

    .line 111
    goto :goto_1

    .line 112
    :cond_1
    const/16 v2, 0x8

    .line 113
    .line 114
    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 115
    .line 116
    .line 117
    const v0, 0x7f0a0a0b

    .line 118
    .line 119
    .line 120
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->〇〇888:Landroid/view/View;

    .line 125
    .line 126
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0〇O0088o:Z

    .line 127
    .line 128
    if-eqz v2, :cond_2

    .line 129
    .line 130
    goto :goto_2

    .line 131
    :cond_2
    const/16 v1, 0x8

    .line 132
    .line 133
    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 134
    .line 135
    .line 136
    const v0, 0x7f0a008f

    .line 137
    .line 138
    .line 139
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->oO80:Landroid/view/View;

    .line 144
    .line 145
    const v0, 0x7f0a1a36

    .line 146
    .line 147
    .line 148
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    check-cast v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 153
    .line 154
    iput-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 155
    .line 156
    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 157
    .line 158
    .line 159
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 160
    .line 161
    new-instance v1, LoOO8/〇〇8O0〇8;

    .line 162
    .line 163
    invoke-direct {v1, p0}, LoOO8/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;->setAnimalCallBack(Lcom/intsig/callback/Callback;)V

    .line 167
    .line 168
    .line 169
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;

    .line 170
    .line 171
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇〇888:Lcom/intsig/camscanner/view/MyViewPager;

    .line 172
    .line 173
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterAnimView;->setViewPager(Lcom/intsig/camscanner/view/MyViewPager;)V

    .line 174
    .line 175
    .line 176
    goto :goto_3

    .line 177
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 178
    .line 179
    .line 180
    move-result-object p3

    .line 181
    check-cast p3, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;

    .line 182
    .line 183
    :goto_3
    invoke-direct {p0, p3, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oo〇(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ViewHolder;I)V

    .line 184
    .line 185
    .line 186
    return-object p2
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public 〇oOO8O8()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_2

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 18
    .line 19
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 20
    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    iget v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 24
    .line 25
    const/16 v2, 0x8

    .line 26
    .line 27
    if-eq v1, v2, :cond_1

    .line 28
    .line 29
    const/16 v2, 0x9

    .line 30
    .line 31
    if-ne v1, v2, :cond_0

    .line 32
    .line 33
    :cond_1
    return v1

    .line 34
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceDefaultIndex()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    return v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.class public Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "MultiImageEditPreviewFragment.java"

# interfaces
.implements Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$BatchImageTaskForMultiEdit;,
        Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$UpdateAdjustProgressCallback;
    }
.end annotation


# static fields
.field private static final o〇oo:Ljava/lang/String; = "MultiImageEditPreviewFragment"


# instance fields
.field private O0O:Landroid/widget/LinearLayout;

.field private O0O0〇:Lcom/intsig/owlery/TheOwlery;

.field private final O0〇0:Landroid/view/View$OnClickListener;

.field private O88O:Landroid/view/View;

.field private O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

.field private O8o〇O0:Ljava/lang/String;

.field private O8〇8〇O80:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private O8〇o〇88:Z

.field private OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private OO〇00〇8oO:Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

.field private OO〇OOo:Z

.field private Oo0O0o8:Z

.field private Oo0〇Ooo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private Oo80:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private Ooo08:Z

.field private final Ooo8o:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

.field private O〇08oOOO0:Z

.field private O〇O:Z

.field private O〇o88o08〇:Landroid/view/View;

.field private final o0:J

.field private o00〇88〇08:Z

.field private o0OoOOo0:Ljava/lang/String;

.field private final o880:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

.field private o8O:Z

.field private o8o:Landroid/widget/CheckBox;

.field private o8oOOo:Landroid/widget/TextView;

.field private o8〇OO:Z

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

.field private oO00〇o:Z

.field private oOO0880O:Z

.field private oOO8:I

.field private oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

.field private oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

.field private oOoO8OO〇:Lcom/intsig/camscanner/mode_ocr/OCRClient;

.field private oOoo80oO:Z

.field private oOo〇08〇:Z

.field private oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

.field private oO〇8O8oOo:Z

.field private oO〇oo:Z

.field private oo8ooo8O:Landroid/widget/LinearLayout;

.field private ooO:Z

.field private ooo0〇〇O:Landroid/view/View;

.field private oooO888:Ljava/lang/Boolean;

.field private o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

.field private o〇0〇o:Lcom/intsig/camscanner/demoire/ImageQualityLoadingAnim;

.field private o〇O8OO:Landroid/widget/EditText;

.field private o〇oO:Lcom/google/android/material/tabs/TabLayout;

.field private o〇o〇Oo88:Z

.field private 〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

.field 〇088O:Z

.field private 〇08O:Landroid/view/View;

.field private final 〇08O〇00〇o:Lcom/intsig/utils/ClickLimit;

.field private 〇08〇o0O:Landroid/widget/TextView;

.field private 〇0O:Landroid/widget/TextView;

.field private 〇0O〇O00O:Ljava/lang/String;

.field private 〇0oO〇oo00:Z

.field private 〇0ooOOo:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

.field public final 〇0〇0:Landroid/view/View$OnClickListener;

.field private 〇800OO〇0O:I

.field private 〇80O8o8O〇:I

.field private 〇8O0880:Landroid/view/animation/Animation;

.field private 〇8〇80o:Landroid/widget/TextView;

.field private 〇8〇OOoooo:J

.field private 〇8〇o88:Z

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;

.field private 〇O0o〇〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private 〇O8oOo0:Z

.field private 〇O8〇8000:Landroid/view/animation/Animation;

.field private 〇OO8ooO8〇:I

.field private final 〇OOo8〇0:I

.field private 〇OO〇00〇0O:J

.field private final 〇Oo〇O:Z

.field private 〇O〇〇O8:Landroid/widget/TextView;

.field private 〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

.field private 〇o0O:Landroidx/recyclerview/widget/RecyclerView;

.field private 〇oo〇O〇80:Z

.field private final 〇o〇88〇8:Lcom/intsig/camscanner/demoire/DeMoireManager$OnDeMoireFinishListener;

.field private 〇〇08O:Lcom/intsig/view/ImageTextButton;

.field private final 〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

.field private 〇〇o0〇8:I

.field private 〇〇o〇:Landroidx/recyclerview/widget/RecyclerView;

.field private 〇〇〇0:Z

.field private 〇〇〇00:Z

.field private 〇〇〇0o〇〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const-wide/16 v0, 0x1388

    .line 5
    .line 6
    iput-wide v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0:J

    .line 7
    .line 8
    const/16 v0, 0x66

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OOo8〇0:I

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08O〇00〇o:Lcom/intsig/utils/ClickLimit;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 19
    .line 20
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Ooo08:Z

    .line 27
    .line 28
    const/4 v1, -0x1

    .line 29
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO8ooO8〇:I

    .line 30
    .line 31
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 32
    .line 33
    const-wide/16 v2, -0x1

    .line 34
    .line 35
    iput-wide v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO〇00〇0O:J

    .line 36
    .line 37
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇0o〇〇0:Z

    .line 38
    .line 39
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO〇8O8oOo:Z

    .line 40
    .line 41
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 42
    .line 43
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO00〇o:Z

    .line 44
    .line 45
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 46
    .line 47
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOoo80oO:Z

    .line 48
    .line 49
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0O0o8:Z

    .line 50
    .line 51
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇OOo:Z

    .line 52
    .line 53
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO8:I

    .line 54
    .line 55
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇oo〇O〇80:Z

    .line 56
    .line 57
    const/4 v2, 0x1

    .line 58
    iput-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8〇o〇88:Z

    .line 59
    .line 60
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇O:Z

    .line 61
    .line 62
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇o88:Z

    .line 63
    .line 64
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8O:Z

    .line 65
    .line 66
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇08〇:Z

    .line 67
    .line 68
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8oOo0:Z

    .line 69
    .line 70
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO〇oo:Z

    .line 71
    .line 72
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 73
    .line 74
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oooO888:Ljava/lang/Boolean;

    .line 75
    .line 76
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$7;

    .line 77
    .line 78
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$7;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 79
    .line 80
    .line 81
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o880:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 82
    .line 83
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00〇88〇08:Z

    .line 84
    .line 85
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o0〇8:I

    .line 86
    .line 87
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$9;

    .line 88
    .line 89
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$9;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 90
    .line 91
    .line 92
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0〇0:Landroid/view/View$OnClickListener;

    .line 93
    .line 94
    const-wide/16 v1, 0x0

    .line 95
    .line 96
    iput-wide v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇OOoooo:J

    .line 97
    .line 98
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇oOO8O8;

    .line 99
    .line 100
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/〇oOO8O8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 101
    .line 102
    .line 103
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o〇88〇8:Lcom/intsig/camscanner/demoire/DeMoireManager$OnDeMoireFinishListener;

    .line 104
    .line 105
    const/4 v1, 0x0

    .line 106
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇O8OO:Landroid/widget/EditText;

    .line 107
    .line 108
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$10;

    .line 109
    .line 110
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$10;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 111
    .line 112
    .line 113
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0〇0:Landroid/view/View$OnClickListener;

    .line 114
    .line 115
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$15;

    .line 116
    .line 117
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$15;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 118
    .line 119
    .line 120
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Ooo8o:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 121
    .line 122
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇0:Z

    .line 123
    .line 124
    new-instance v2, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 125
    .line 126
    invoke-direct {v2}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;-><init>()V

    .line 127
    .line 128
    .line 129
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 130
    .line 131
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0oO〇oo00:Z

    .line 132
    .line 133
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8〇8〇O80:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 134
    .line 135
    invoke-static {}, Lcom/intsig/camscanner/experiment/SuperFilterStyleExp;->Oo08()Z

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oo〇O:Z

    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O008o8oo(I)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "checkScrollRvPosition pos: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇〇888:Ljava/util/HashMap;

    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView;

    .line 26
    .line 27
    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 32
    .line 33
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    if-eqz v3, :cond_0

    .line 42
    .line 43
    if-eqz v2, :cond_0

    .line 44
    .line 45
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    check-cast p1, Ljava/lang/Integer;

    .line 54
    .line 55
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v3, "smoothScrollToPosition pos: "

    .line 65
    .line 66
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    const/4 v0, 0x0

    .line 80
    invoke-virtual {v2, p1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    .line 81
    .line 82
    .line 83
    :cond_0
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic O008oO0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O00OoO〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O00o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 33
    .line 34
    iget-object v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 35
    .line 36
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0OoOOo0:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 37
    .line 38
    invoke-virtual {v2}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->isCurrentNoNeed()Z

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    if-eqz v2, :cond_0

    .line 43
    .line 44
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 45
    .line 46
    sget-object v2, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;

    .line 47
    .line 48
    invoke-virtual {v2}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;->requireWaitingInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    iput-object v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0OoOOo0:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 53
    .line 54
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇8o00()Ljava/lang/Boolean;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    if-eqz v0, :cond_1

    .line 63
    .line 64
    return-void

    .line 65
    :cond_1
    if-nez p1, :cond_2

    .line 66
    .line 67
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 68
    .line 69
    const-string v2, "initMultiImageEditViewModel getModelMutableLiveData observe multiImageEditModel == null"

    .line 70
    .line 71
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 76
    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    const-string v3, "initMultiImageEditViewModel getModelMutableLiveData observe multiImageEditModel imageUUID="

    .line 83
    .line 84
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 88
    .line 89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    const-string v3, "; rawPath="

    .line 93
    .line 94
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 98
    .line 99
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 110
    .line 111
    if-eqz v0, :cond_9

    .line 112
    .line 113
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 114
    .line 115
    if-nez v0, :cond_3

    .line 116
    .line 117
    goto/16 :goto_1

    .line 118
    .line 119
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 124
    .line 125
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇0OOo〇0()I

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    if-eq v0, v2, :cond_4

    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 132
    .line 133
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 134
    .line 135
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 136
    .line 137
    .line 138
    move-result-object v2

    .line 139
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o8oO〇(Ljava/util/List;)V

    .line 140
    .line 141
    .line 142
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 143
    .line 144
    .line 145
    move-result v0

    .line 146
    if-eqz v0, :cond_5

    .line 147
    .line 148
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0oO〇oo00:Z

    .line 149
    .line 150
    if-eqz v0, :cond_5

    .line 151
    .line 152
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08O80O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 153
    .line 154
    .line 155
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 156
    .line 157
    .line 158
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 159
    .line 160
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0000OOO()Z

    .line 161
    .line 162
    .line 163
    move-result p1

    .line 164
    const/4 v0, 0x1

    .line 165
    if-eqz p1, :cond_6

    .line 166
    .line 167
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 168
    .line 169
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 170
    .line 171
    .line 172
    move-result p1

    .line 173
    if-le p1, v0, :cond_7

    .line 174
    .line 175
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 176
    .line 177
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 178
    .line 179
    .line 180
    move-result p1

    .line 181
    if-ge p1, v0, :cond_8

    .line 182
    .line 183
    :cond_7
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 184
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    .line 186
    .line 187
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .line 189
    .line 190
    const-string v1, "data is clear,finish count:"

    .line 191
    .line 192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 196
    .line 197
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 198
    .line 199
    .line 200
    move-result v1

    .line 201
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 205
    .line 206
    .line 207
    move-result-object v0

    .line 208
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 212
    .line 213
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 214
    .line 215
    .line 216
    return-void

    .line 217
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 218
    .line 219
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0o〇〇〇8o()I

    .line 220
    .line 221
    .line 222
    move-result v0

    .line 223
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O000(I)V

    .line 224
    .line 225
    .line 226
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 227
    .line 228
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OO8oO0o〇(Z)V

    .line 229
    .line 230
    .line 231
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0o〇〇〇8o()I

    .line 232
    .line 233
    .line 234
    move-result p1

    .line 235
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O〇〇00(I)V

    .line 236
    .line 237
    .line 238
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0o()V

    .line 239
    .line 240
    .line 241
    return-void

    .line 242
    :cond_9
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 243
    .line 244
    const-string v0, "imageViewPager == null || multiImageEditAdapter == null"

    .line 245
    .line 246
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    return-void
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private O00oo0()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showSuperFilterGuide"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 9
    .line 10
    const v1, 0x7f0a07d2

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    return v0

    .line 23
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇O80Oo()V

    .line 24
    .line 25
    .line 26
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇o0O0O8;

    .line 27
    .line 28
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/multiimageedit/〇o0O0O8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/view/ImageTextButton;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 32
    .line 33
    .line 34
    const/4 v0, 0x1

    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic O00〇8(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8oOo0:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private declared-synchronized O00〇o00()Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;

    .line 3
    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 7
    .line 8
    const-string v1, "getFragmentViewModel: First INIT"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 16
    .line 17
    invoke-direct {v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 18
    .line 19
    .line 20
    const-class v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;

    .line 27
    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;

    .line 29
    .line 30
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    .line 32
    monitor-exit p0

    .line 33
    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    .line 35
    monitor-exit p0

    .line 36
    throw v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O088O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/mode_ocr/OCRClient;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOoO8OO〇:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O08o()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-array v0, v1, [Landroid/view/View;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 12
    .line 13
    const v3, 0x7f0a01f7

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v2

    .line 21
    .line 22
    invoke-static {v2, v0}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    new-array v0, v1, [Landroid/view/View;

    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 33
    .line 34
    const v3, 0x7f0a01f6

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    aput-object v1, v0, v2

    .line 42
    .line 43
    invoke-static {v2, v0}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 44
    .line 45
    .line 46
    return-void

    .line 47
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8Oo8〇8()Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_2

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 54
    .line 55
    const v3, 0x7f0a0729

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 63
    .line 64
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo80:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 65
    .line 66
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 67
    .line 68
    .line 69
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o8〇o()Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-eqz v0, :cond_3

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 76
    .line 77
    const v3, 0x7f0a03ea

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇o88o08〇:Landroid/view/View;

    .line 85
    .line 86
    new-array v0, v1, [Landroid/view/View;

    .line 87
    .line 88
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 89
    .line 90
    const v3, 0x7f0a01f8

    .line 91
    .line 92
    .line 93
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    aput-object v1, v0, v2

    .line 98
    .line 99
    invoke-static {v2, v0}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 100
    .line 101
    .line 102
    return-void

    .line 103
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 104
    .line 105
    if-eqz v0, :cond_4

    .line 106
    .line 107
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 108
    .line 109
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->〇80〇808〇O(Landroid/view/View;)Z

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    if-eqz v0, :cond_4

    .line 114
    .line 115
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 116
    .line 117
    const-string v1, "checkAndChangeViewForSpecificType: actionClient return"

    .line 118
    .line 119
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    return-void

    .line 123
    :cond_4
    new-array v0, v1, [Landroid/view/View;

    .line 124
    .line 125
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 126
    .line 127
    const v3, 0x7f0a01f4

    .line 128
    .line 129
    .line 130
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    aput-object v1, v0, v2

    .line 135
    .line 136
    invoke-static {v2, v0}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 137
    .line 138
    .line 139
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇oo〇O〇80:Z

    .line 140
    .line 141
    if-eqz v0, :cond_5

    .line 142
    .line 143
    new-instance v0, Lcom/intsig/camscanner/capture/CapWaveControl;

    .line 144
    .line 145
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 146
    .line 147
    const v2, 0x3f0253c8

    .line 148
    .line 149
    .line 150
    const v3, 0x3e99999a    # 0.3f

    .line 151
    .line 152
    .line 153
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/capture/CapWaveControl;-><init>(Landroid/app/Activity;FF)V

    .line 154
    .line 155
    .line 156
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/CapWaveControl;->〇080()Z

    .line 157
    .line 158
    .line 159
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 160
    .line 161
    const-string v1, "showWaveAnimation"

    .line 162
    .line 163
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    :cond_5
    return-void
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic O08〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇8Oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O08〇oO8〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oooO888:Ljava/lang/Boolean;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O0O0〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O00o(Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O0Oo〇8()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O08o()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 5
    .line 6
    const v1, 0x7f0a1672    # 1.8355E38f

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Landroid/widget/TextView;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O:Landroid/widget/TextView;

    .line 16
    .line 17
    const/16 v0, 0x11

    .line 18
    .line 19
    new-array v0, v0, [I

    .line 20
    .line 21
    fill-array-data v0, :array_0

    .line 22
    .line 23
    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO0〇([I)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 28
    .line 29
    const v1, 0x7f0a0774

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo0〇〇O:Landroid/view/View;

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 39
    .line 40
    const v1, 0x7f0a0810

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇08O:Lcom/intsig/view/ImageTextButton;

    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 52
    .line 53
    const v1, 0x7f0a0c24

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    check-cast v0, Landroid/widget/LinearLayout;

    .line 61
    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O:Landroid/widget/LinearLayout;

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 65
    .line 66
    const v1, 0x7f0a14ee

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    check-cast v0, Landroid/widget/TextView;

    .line 74
    .line 75
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8oOOo:Landroid/widget/TextView;

    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 78
    .line 79
    const v1, 0x7f0a14ed

    .line 80
    .line 81
    .line 82
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    check-cast v0, Landroid/widget/TextView;

    .line 87
    .line 88
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O〇〇O8:Landroid/widget/TextView;

    .line 89
    .line 90
    if-eqz v0, :cond_0

    .line 91
    .line 92
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0〇0:Landroid/view/View$OnClickListener;

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    .line 96
    .line 97
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 98
    .line 99
    const v1, 0x7f0a0808

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 107
    .line 108
    const v2, 0x7f0a07f5

    .line 109
    .line 110
    .line 111
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    iget v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO8:I

    .line 116
    .line 117
    const/16 v3, 0x8

    .line 118
    .line 119
    const/4 v4, 0x0

    .line 120
    if-lez v2, :cond_2

    .line 121
    .line 122
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇O:Z

    .line 123
    .line 124
    if-eqz v2, :cond_2

    .line 125
    .line 126
    if-eqz v0, :cond_1

    .line 127
    .line 128
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 129
    .line 130
    .line 131
    :cond_1
    if-eqz v1, :cond_4

    .line 132
    .line 133
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 134
    .line 135
    .line 136
    goto :goto_0

    .line 137
    :cond_2
    if-eqz v0, :cond_3

    .line 138
    .line 139
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 140
    .line 141
    .line 142
    :cond_3
    if-eqz v1, :cond_4

    .line 143
    .line 144
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 145
    .line 146
    .line 147
    :cond_4
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0()V

    .line 148
    .line 149
    .line 150
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0o()V

    .line 151
    .line 152
    .line 153
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0o〇〇〇8o()I

    .line 154
    .line 155
    .line 156
    move-result v0

    .line 157
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O〇〇00(I)V

    .line 158
    .line 159
    .line 160
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 161
    .line 162
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0o〇〇〇8o()I

    .line 163
    .line 164
    .line 165
    move-result v1

    .line 166
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇OO8Oo0〇(I)V

    .line 167
    .line 168
    .line 169
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0o〇〇〇8o()I

    .line 170
    .line 171
    .line 172
    move-result v0

    .line 173
    invoke-direct {p0, v0, v4}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OOo0Oo8O(IZ)V

    .line 174
    .line 175
    .line 176
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8〇o〇88:Z

    .line 177
    .line 178
    if-eqz v0, :cond_5

    .line 179
    .line 180
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 181
    .line 182
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0o〇〇〇8o()I

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇8(I)V

    .line 187
    .line 188
    .line 189
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 190
    .line 191
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0o〇〇〇8o()I

    .line 192
    .line 193
    .line 194
    move-result v1

    .line 195
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O000(I)V

    .line 196
    .line 197
    .line 198
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8Oo8〇8()Z

    .line 199
    .line 200
    .line 201
    move-result v0

    .line 202
    if-eqz v0, :cond_6

    .line 203
    .line 204
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 205
    .line 206
    const v1, 0x7f0a0309

    .line 207
    .line 208
    .line 209
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 210
    .line 211
    .line 212
    move-result-object v0

    .line 213
    check-cast v0, Landroid/widget/CheckBox;

    .line 214
    .line 215
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 216
    .line 217
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 218
    .line 219
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 220
    .line 221
    .line 222
    move-result-object v0

    .line 223
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 224
    .line 225
    .line 226
    move-result v0

    .line 227
    const/4 v1, 0x1

    .line 228
    if-le v0, v1, :cond_7

    .line 229
    .line 230
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 231
    .line 232
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 233
    .line 234
    .line 235
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 236
    .line 237
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 238
    .line 239
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇8o8O〇O()Z

    .line 240
    .line 241
    .line 242
    move-result v1

    .line 243
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 244
    .line 245
    .line 246
    goto :goto_1

    .line 247
    :cond_6
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 248
    .line 249
    const v1, 0x7f0a0359

    .line 250
    .line 251
    .line 252
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    check-cast v0, Landroid/widget/CheckBox;

    .line 257
    .line 258
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 259
    .line 260
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 261
    .line 262
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇8;

    .line 263
    .line 264
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/〇8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 265
    .line 266
    .line 267
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 268
    .line 269
    .line 270
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 271
    .line 272
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 273
    .line 274
    .line 275
    move-result-object v0

    .line 276
    if-eqz v0, :cond_8

    .line 277
    .line 278
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 279
    .line 280
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 281
    .line 282
    .line 283
    move-result-object v0

    .line 284
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$2;

    .line 285
    .line 286
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 287
    .line 288
    .line 289
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 290
    .line 291
    .line 292
    :cond_8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 293
    .line 294
    const v1, 0x7f0a0586

    .line 295
    .line 296
    .line 297
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 298
    .line 299
    .line 300
    move-result-object v0

    .line 301
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 302
    .line 303
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 304
    .line 305
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 306
    .line 307
    const v1, 0x7f0a0b1d

    .line 308
    .line 309
    .line 310
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 311
    .line 312
    .line 313
    move-result-object v0

    .line 314
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O88O:Landroid/view/View;

    .line 315
    .line 316
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 317
    .line 318
    const v1, 0x7f0a0736

    .line 319
    .line 320
    .line 321
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 322
    .line 323
    .line 324
    move-result-object v0

    .line 325
    check-cast v0, Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 326
    .line 327
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 328
    .line 329
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o880:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 330
    .line 331
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setImageAdjustListener(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;)V

    .line 332
    .line 333
    .line 334
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o008o08O()V

    .line 335
    .line 336
    .line 337
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 338
    .line 339
    const v1, 0x7f0a11d8

    .line 340
    .line 341
    .line 342
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 343
    .line 344
    .line 345
    move-result-object v0

    .line 346
    check-cast v0, Lcom/google/android/material/tabs/TabLayout;

    .line 347
    .line 348
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 349
    .line 350
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 351
    .line 352
    const v1, 0x7f0a160b

    .line 353
    .line 354
    .line 355
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 356
    .line 357
    .line 358
    move-result-object v0

    .line 359
    check-cast v0, Landroid/widget/TextView;

    .line 360
    .line 361
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08〇o0O:Landroid/widget/TextView;

    .line 362
    .line 363
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 364
    .line 365
    const v1, 0x7f0a102f

    .line 366
    .line 367
    .line 368
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 369
    .line 370
    .line 371
    move-result-object v0

    .line 372
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 373
    .line 374
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView;

    .line 375
    .line 376
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OOO〇()V

    .line 377
    .line 378
    .line 379
    return-void

    .line 380
    nop

    .line 381
    :array_0
    .array-data 4
        0x7f0a09e8
        0x7f0a09a6
        0x7f0a0808
        0x7f0a07f5
        0x7f0a0751
        0x7f0a0754
        0x7f0a07ca
        0x7f0a1a47
        0x7f0a05c8
        0x7f0a0774
        0x7f0a0b1d
        0x7f0a0810
        0x7f0a07d2
        0x7f0a14ed
        0x7f0a0800
        0x7f0a07e7
        0x7f0a160b
    .end array-data
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static bridge synthetic O0o0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇800OO〇0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0o0〇8o()V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO00〇o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_4

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOoo80oO:Z

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇800OO〇0O:I

    .line 12
    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇0:Z

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    const/4 v3, 0x2

    .line 26
    if-ge v2, v3, :cond_1

    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 34
    .line 35
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 36
    .line 37
    const/4 v2, 0x1

    .line 38
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 43
    .line 44
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 45
    .line 46
    if-eqz v0, :cond_3

    .line 47
    .line 48
    if-nez v1, :cond_2

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_2
    iget-wide v4, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 52
    .line 53
    iget-wide v6, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 54
    .line 55
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8〇〇o8〇()Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇8o8o〇()Landroidx/lifecycle/MutableLiveData;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇00〇8;

    .line 64
    .line 65
    move-object v2, v1

    .line 66
    move-object v3, p0

    .line 67
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/multiimageedit/〇00〇8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;JJ)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0, p0, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 71
    .line 72
    .line 73
    return-void

    .line 74
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 75
    .line 76
    const-string v1, "imageModel1 or imageModel2 == null "

    .line 77
    .line 78
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    :cond_4
    :goto_1
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O0oO()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic O0oOo(JJLjava/lang/Long;)V
    .locals 4

    .line 1
    if-nez p5, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "observe imageId "

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    .line 27
    .line 28
    .line 29
    move-result-wide v1

    .line 30
    cmp-long v3, p1, v1

    .line 31
    .line 32
    if-eqz v3, :cond_1

    .line 33
    .line 34
    invoke-virtual {p5}, Ljava/lang/Long;->longValue()J

    .line 35
    .line 36
    .line 37
    move-result-wide p1

    .line 38
    cmp-long p5, p3, p1

    .line 39
    .line 40
    if-nez p5, :cond_2

    .line 41
    .line 42
    :cond_1
    iget p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇800OO〇0O:I

    .line 43
    .line 44
    add-int/lit8 p1, p1, 0x1

    .line 45
    .line 46
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇800OO〇0O:I

    .line 47
    .line 48
    new-instance p1, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    const-string p2, " mDealImgNum:"

    .line 54
    .line 55
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    iget p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇800OO〇0O:I

    .line 59
    .line 60
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    :cond_2
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OoOo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0〇0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o0〇0O〇o(Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O0〇0o8O(Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 6
    .line 7
    const-string v0, "enhanceThumbAdapter == null"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    if-nez p1, :cond_1

    .line 14
    .line 15
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 16
    .line 17
    const-string v0, "multiEditEnhanceThumb == null"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 24
    .line 25
    const-string v1, "initEnhanceThumbViewModel getModelMutableLiveData().observe"

    .line 26
    .line 27
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 31
    .line 32
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 33
    .line 34
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->〇0〇O0088o(Ljava/util/List;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic O0〇8〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0〇O80ooo()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_3

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_2

    .line 28
    .line 29
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 34
    .line 35
    if-eqz v2, :cond_1

    .line 36
    .line 37
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 38
    .line 39
    iget v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 40
    .line 41
    sget v3, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇〇888:I

    .line 42
    .line 43
    if-ne v2, v3, :cond_1

    .line 44
    .line 45
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 46
    .line 47
    const-string v1, "F-currentListHasError and get error "

    .line 48
    .line 49
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    const/4 v0, 0x1

    .line 53
    return v0

    .line 54
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 55
    .line 56
    const-string v2, "F-currentListHasError and no error "

    .line 57
    .line 58
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    return v1

    .line 62
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 63
    .line 64
    const-string v2, "F-currentListHasError and get null ptr"

    .line 65
    .line 66
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return v1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O0〇o8o〇〇()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "initActionClient: START"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$1;

    .line 9
    .line 10
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$1;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OoOO〇()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    if-eqz v2, :cond_0

    .line 18
    .line 19
    const-string v2, "initActionClient: isFromBookMode"

    .line 20
    .line 21
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;

    .line 25
    .line 26
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;-><init>(Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;)V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 30
    .line 31
    :cond_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O80()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const v1, 0x7f13156c

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/O0;

    .line 21
    .line 22
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/O0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 23
    .line 24
    .line 25
    const v2, 0x7f13057e

    .line 26
    .line 27
    .line 28
    const v3, 0x7f060207

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2, v3, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/ooOO;

    .line 36
    .line 37
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/ooOO;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 38
    .line 39
    .line 40
    const v2, 0x7f130026

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O8080〇O8o(Landroid/content/Intent;)V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "loadIntentData"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    return-void

    .line 18
    :cond_1
    const-string v1, "extra_multi_scan_process"

    .line 19
    .line 20
    const/4 v2, -0x1

    .line 21
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO8:I

    .line 26
    .line 27
    const-string v1, "extra_from_show_wave_animation"

    .line 28
    .line 29
    const/4 v3, 0x0

    .line 30
    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇oo〇O〇80:Z

    .line 35
    .line 36
    const-string v1, "extra_boolean_show_adapter_enhance_anim"

    .line 37
    .line 38
    const/4 v4, 0x1

    .line 39
    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8〇o〇88:Z

    .line 44
    .line 45
    const-string v1, "extra_show_all_capture_mode"

    .line 46
    .line 47
    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇O:Z

    .line 52
    .line 53
    const-string v1, "extra_from_single_capture"

    .line 54
    .line 55
    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇o88:Z

    .line 60
    .line 61
    const-string v1, "EXTRA_IS_FOR_PRINT"

    .line 62
    .line 63
    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇OO:Z

    .line 68
    .line 69
    const-string v1, "extra_need_new_pic_record"

    .line 70
    .line 71
    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Ooo08:Z

    .line 76
    .line 77
    const-string v1, "extra_from_paper_import"

    .line 78
    .line 79
    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 84
    .line 85
    const-string v1, "extra_reedit_page_id"

    .line 86
    .line 87
    const-wide/16 v5, -0x1

    .line 88
    .line 89
    invoke-virtual {v0, v1, v5, v6}, Landroid/os/BaseBundle;->getLong(Ljava/lang/String;J)J

    .line 90
    .line 91
    .line 92
    move-result-wide v5

    .line 93
    iput-wide v5, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO〇00〇0O:J

    .line 94
    .line 95
    const-string v1, "extra_max_page_num"

    .line 96
    .line 97
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;I)I

    .line 98
    .line 99
    .line 100
    move-result v1

    .line 101
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO8ooO8〇:I

    .line 102
    .line 103
    const-string v1, "extra_from_import_image"

    .line 104
    .line 105
    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇08oOOO0:Z

    .line 110
    .line 111
    const-string v1, "extra_parcel_doc_info"

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    instance-of v5, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 118
    .line 119
    if-eqz v5, :cond_2

    .line 120
    .line 121
    check-cast v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 122
    .line 123
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 124
    .line 125
    :cond_2
    const-string v1, "EXTRA_FROM_IMPORT_IMAGE_PATH_LIST"

    .line 126
    .line 127
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 132
    .line 133
    const-string v0, "extra_from_widget"

    .line 134
    .line 135
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇0o〇〇0:Z

    .line 140
    .line 141
    const-string v0, "extra_start_do_camera"

    .line 142
    .line 143
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 144
    .line 145
    .line 146
    move-result v0

    .line 147
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO〇8O8oOo:Z

    .line 148
    .line 149
    const-string v0, "EXTRA_FROM_PART"

    .line 150
    .line 151
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 156
    .line 157
    const-string v0, "extra_custom_from_part"

    .line 158
    .line 159
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0OoOOo0:Ljava/lang/String;

    .line 164
    .line 165
    const-string v0, "EXTRA_LOTTERY_VALUE"

    .line 166
    .line 167
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v0

    .line 171
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o〇O0:Ljava/lang/String;

    .line 172
    .line 173
    const-string v0, "extra_capture_function_entrance"

    .line 174
    .line 175
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_NEW_ESIGN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 180
    .line 181
    if-ne v0, v1, :cond_3

    .line 182
    .line 183
    const/4 v0, 0x1

    .line 184
    goto :goto_0

    .line 185
    :cond_3
    const/4 v0, 0x0

    .line 186
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8O:Z

    .line 187
    .line 188
    const-string v0, "EXTRA_FROM_PAGE_TAG"

    .line 189
    .line 190
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 191
    .line 192
    .line 193
    move-result-object p1

    .line 194
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 195
    .line 196
    .line 197
    move-result v0

    .line 198
    if-nez v0, :cond_a

    .line 199
    .line 200
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 201
    .line 202
    .line 203
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 204
    .line 205
    .line 206
    move-result v0

    .line 207
    sparse-switch v0, :sswitch_data_0

    .line 208
    .line 209
    .line 210
    goto :goto_1

    .line 211
    :sswitch_0
    const-string v0, "OcrCaptureSceneNew"

    .line 212
    .line 213
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 214
    .line 215
    .line 216
    move-result p1

    .line 217
    if-nez p1, :cond_4

    .line 218
    .line 219
    goto :goto_1

    .line 220
    :cond_4
    const/4 v2, 0x5

    .line 221
    goto :goto_1

    .line 222
    :sswitch_1
    const-string v0, "BankCardJournalCaptureScene"

    .line 223
    .line 224
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 225
    .line 226
    .line 227
    move-result p1

    .line 228
    if-nez p1, :cond_5

    .line 229
    .line 230
    goto :goto_1

    .line 231
    :cond_5
    const/4 v2, 0x4

    .line 232
    goto :goto_1

    .line 233
    :sswitch_2
    const-string v0, "CaptureSignatureCaptureScene"

    .line 234
    .line 235
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 236
    .line 237
    .line 238
    move-result p1

    .line 239
    if-nez p1, :cond_6

    .line 240
    .line 241
    goto :goto_1

    .line 242
    :cond_6
    const/4 v2, 0x3

    .line 243
    goto :goto_1

    .line 244
    :sswitch_3
    const-string v0, "DocToWordMultiCaptureScene"

    .line 245
    .line 246
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 247
    .line 248
    .line 249
    move-result p1

    .line 250
    if-nez p1, :cond_7

    .line 251
    .line 252
    goto :goto_1

    .line 253
    :cond_7
    const/4 v2, 0x2

    .line 254
    goto :goto_1

    .line 255
    :sswitch_4
    const-string v0, "DocToExcelCaptureScene"

    .line 256
    .line 257
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 258
    .line 259
    .line 260
    move-result p1

    .line 261
    if-nez p1, :cond_8

    .line 262
    .line 263
    goto :goto_1

    .line 264
    :cond_8
    const/4 v2, 0x1

    .line 265
    goto :goto_1

    .line 266
    :sswitch_5
    const-string v0, "WhitePadScene"

    .line 267
    .line 268
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 269
    .line 270
    .line 271
    move-result p1

    .line 272
    if-nez p1, :cond_9

    .line 273
    .line 274
    goto :goto_1

    .line 275
    :cond_9
    const/4 v2, 0x0

    .line 276
    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 277
    .line 278
    .line 279
    goto :goto_2

    .line 280
    :pswitch_0
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 281
    .line 282
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇OOo:Z

    .line 283
    .line 284
    goto :goto_2

    .line 285
    :pswitch_1
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0O0o8:Z

    .line 286
    .line 287
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇OOo:Z

    .line 288
    .line 289
    goto :goto_2

    .line 290
    :pswitch_2
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇OOo:Z

    .line 291
    .line 292
    goto :goto_2

    .line 293
    :pswitch_3
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO00〇o:Z

    .line 294
    .line 295
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇OOo:Z

    .line 296
    .line 297
    goto :goto_2

    .line 298
    :pswitch_4
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOoo80oO:Z

    .line 299
    .line 300
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇OOo:Z

    .line 301
    .line 302
    goto :goto_2

    .line 303
    :pswitch_5
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 304
    .line 305
    iput-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇OOo:Z

    .line 306
    .line 307
    :cond_a
    :goto_2
    return-void

    .line 308
    nop

    .line 309
    :sswitch_data_0
    .sparse-switch
        -0x5e39fd1e -> :sswitch_5
        -0x56cb6c36 -> :sswitch_4
        -0x54117d1e -> :sswitch_3
        -0x38d265c8 -> :sswitch_2
        0x1d262e91 -> :sswitch_1
        0x66cd69fc -> :sswitch_0
    .end sparse-switch

    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method static bridge synthetic O80OO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇08oOOO0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O80〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->〇080()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0oO〇oo00:Z

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O80〇〇o()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    :goto_0
    if-eqz v0, :cond_1

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 22
    .line 23
    iget-boolean v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->Ooo08:Z

    .line 24
    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    const/4 v0, 0x1

    .line 28
    goto :goto_1

    .line 29
    :cond_1
    const/4 v0, 0x0

    .line 30
    :goto_1
    return v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O88(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O880O〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OOO80〇〇88(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic O888Oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oooo088〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O888o8(Lcom/intsig/callback/Callback0;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->〇o00〇〇Oo()Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->〇o00〇〇Oo()Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const-wide/16 v0, 0x1

    .line 19
    .line 20
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 21
    .line 22
    if-eqz v2, :cond_1

    .line 23
    .line 24
    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_1

    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 31
    .line 32
    new-instance v3, Lcom/intsig/camscanner/multiimageedit/o〇8;

    .line 33
    .line 34
    invoke-direct {v3, p0, v0, v1, p1}, Lcom/intsig/camscanner/multiimageedit/o〇8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;JLcom/intsig/callback/Callback0;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic O88Oo8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇00o08(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O8O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇0Oo0880(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O8O〇8〇〇8〇(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 2
    .line 3
    const-wide/16 v1, -0x1

    .line 4
    .line 5
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const-string v2, "MultiImageEditPreviewFragment.showRenameDlg"

    .line 10
    .line 11
    invoke-virtual {v0, v2, p1, p2, v1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->OoO8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 12
    .line 13
    .line 14
    invoke-static {p2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    if-eqz p2, :cond_0

    .line 19
    .line 20
    sget-object p2, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->UNDEFINED:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 21
    .line 22
    invoke-virtual {p2}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    sget-object p2, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->CUSTOM:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 28
    .line 29
    invoke-virtual {p2}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 30
    .line 31
    .line 32
    move-result p2

    .line 33
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇〇〇O0(Ljava/lang/String;I)V

    .line 34
    .line 35
    .line 36
    const/4 p1, 0x0

    .line 37
    return-object p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private O8o()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v0, "CSTestPaper"

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const-string v0, "CSBatchResult"

    .line 11
    .line 12
    :goto_0
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O8o0〇(I)Landroid/content/Intent;
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-eqz v1, :cond_4

    .line 14
    .line 15
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    if-lez v3, :cond_4

    .line 20
    .line 21
    new-instance v3, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;

    .line 22
    .line 23
    invoke-direct {v3}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v4

    .line 34
    if-eqz v4, :cond_3

    .line 35
    .line 36
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    check-cast v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 41
    .line 42
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 43
    .line 44
    if-nez v4, :cond_0

    .line 45
    .line 46
    sget-object v4, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 47
    .line 48
    const-string v5, "getMultiCaptureResultIntent multiImageEditModel == null"

    .line 49
    .line 50
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    iget-object v5, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 55
    .line 56
    invoke-static {v5}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 57
    .line 58
    .line 59
    move-result v5

    .line 60
    if-nez v5, :cond_1

    .line 61
    .line 62
    sget-object v5, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 63
    .line 64
    new-instance v6, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v7, "getMultiCaptureResultIntent multiImageEditModel="

    .line 70
    .line 71
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v7

    .line 78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v6

    .line 85
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    :cond_1
    iget-object v5, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 89
    .line 90
    iget v6, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 91
    .line 92
    invoke-virtual {v3, v5, v6}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->Oooo8o0〇(Ljava/lang/String;I)V

    .line 93
    .line 94
    .line 95
    iget-object v6, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 96
    .line 97
    if-eqz v6, :cond_2

    .line 98
    .line 99
    invoke-virtual {v3, v5, v6}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->OO0o〇〇〇〇0(Ljava/lang/String;[I)V

    .line 100
    .line 101
    .line 102
    goto :goto_1

    .line 103
    :cond_2
    move-object v6, v2

    .line 104
    :goto_1
    iget-wide v7, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 105
    .line 106
    iget v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 107
    .line 108
    invoke-static {v7, v8, v5, v4, v6}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->O8(JLjava/lang/String;I[I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 109
    .line 110
    .line 111
    move-result-object v4

    .line 112
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_3
    invoke-virtual {v3, p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->〇O00(I)V

    .line 117
    .line 118
    .line 119
    goto :goto_2

    .line 120
    :cond_4
    move-object v3, v2

    .line 121
    :goto_2
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 122
    .line 123
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->clone()Ljava/lang/Object;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    check-cast p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    .line 129
    goto :goto_3

    .line 130
    :catch_0
    move-exception p1

    .line 131
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 132
    .line 133
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 134
    .line 135
    .line 136
    move-object p1, v2

    .line 137
    :goto_3
    if-nez p1, :cond_5

    .line 138
    .line 139
    return-object v2

    .line 140
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 141
    .line 142
    const/4 v2, 0x6

    .line 143
    invoke-static {v1, p1, v3, v2, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultActivity;->O0〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;ILjava/util/List;)Landroid/content/Intent;

    .line 144
    .line 145
    .line 146
    move-result-object p1

    .line 147
    return-object p1
.end method

.method private O8oO0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O88O:Landroid/view/View;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0o()Landroid/view/animation/Animation;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O88O:Landroid/view/View;

    .line 11
    .line 12
    const/16 v1, 0x8

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O8ooO8o()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08〇o0O:Landroid/widget/TextView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const/16 v1, 0x18

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    const/16 v2, 0x8

    .line 17
    .line 18
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 23
    .line 24
    const/4 v3, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    if-eqz v2, :cond_1

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-nez v2, :cond_1

    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 35
    .line 36
    const v5, 0x7f080e59

    .line 37
    .line 38
    .line 39
    invoke-static {v2, v5}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    add-int v5, v0, v1

    .line 44
    .line 45
    invoke-virtual {v2, v3, v1, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08〇o0O:Landroid/widget/TextView;

    .line 49
    .line 50
    invoke-virtual {v0, v4, v2, v4, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 51
    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08〇o0O:Landroid/widget/TextView;

    .line 54
    .line 55
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 56
    .line 57
    const v2, 0x7f06024e

    .line 58
    .line 59
    .line 60
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 69
    .line 70
    const v5, 0x7f080e58

    .line 71
    .line 72
    .line 73
    invoke-static {v2, v5}, Landroidx/appcompat/content/res/AppCompatResources;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    add-int v5, v0, v1

    .line 78
    .line 79
    invoke-virtual {v2, v3, v1, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08〇o0O:Landroid/widget/TextView;

    .line 83
    .line 84
    invoke-virtual {v0, v4, v2, v4, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 85
    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08〇o0O:Landroid/widget/TextView;

    .line 88
    .line 89
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 90
    .line 91
    const v2, 0x7f060268

    .line 92
    .line 93
    .line 94
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 99
    .line 100
    .line 101
    :goto_0
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O8o〇o()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "rename"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSBatchResult"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oo〇oO8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method private O8〇(Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8o80O(Z)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇Oo〇0O〇8(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O8〇8〇O80(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o8〇〇(Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic O8〇o0〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 4
    .line 5
    invoke-static {p0}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->oO80(Ljava/lang/String;)Ljava/io/File;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00〇88〇08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO00〇0o〇〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "enhanceThumbAdapter == null"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceMode(I)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 22
    .line 23
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/〇〇0o;

    .line 24
    .line 25
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/multiimageedit/〇〇0o;-><init>(I)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o08oOO(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel$MultiImageEditModelTraverse;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic OO0O(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o80oO(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OO0O8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sget-object v1, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->INSTANCE:Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;

    .line 14
    .line 15
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->recordOneTimeForImage(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 16
    .line 17
    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 21
    .line 22
    const-string v1, "turnRight multiImageEditPage == null"

    .line 23
    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 29
    .line 30
    iget-object v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 31
    .line 32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 33
    .line 34
    .line 35
    move-result-wide v3

    .line 36
    invoke-virtual {v1, v2, v3, v4}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->oo0O〇0〇〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 40
    .line 41
    .line 42
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0O0o8:Z

    .line 43
    .line 44
    if-eqz v1, :cond_1

    .line 45
    .line 46
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇oo〇;

    .line 47
    .line 48
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/multiimageedit/〇oo〇;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 49
    .line 50
    .line 51
    invoke-static {v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 52
    .line 53
    .line 54
    :cond_1
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OO0o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO0o88()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "initFragmentViewModel"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O00〇o00()Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "signature"

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 15
    .line 16
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    const/4 v1, 0x1

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;->Oooo8o0〇(Z)V

    .line 24
    .line 25
    .line 26
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 31
    .line 32
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/O000;

    .line 33
    .line 34
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/O000;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private varargs OO0〇([I)V
    .locals 4
    .param p1    # [I
        .annotation build Landroidx/annotation/IdRes;
        .end annotation
    .end param

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    if-ge v1, v0, :cond_1

    .line 4
    .line 5
    aget v2, p1, v1

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 8
    .line 9
    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    if-eqz v2, :cond_0

    .line 14
    .line 15
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    .line 17
    .line 18
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic OO0〇O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO80O0o8O(I)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00OOO8()V

    .line 5
    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x2

    .line 9
    if-ne p1, v0, :cond_1

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O80〇()V

    .line 12
    .line 13
    .line 14
    :cond_1
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO8〇O8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OOO0o〇(Lcom/intsig/camscanner/multiimageedit/model/TaskType;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$13;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$13;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/TaskType;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic OOO80〇〇88(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "handForPadFail try"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇8o00()Ljava/lang/Boolean;

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OOOo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo0〇〇O:Landroid/view/View;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0o()Landroid/view/animation/Animation;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo0〇〇O:Landroid/view/View;

    .line 11
    .line 12
    const/16 v1, 0x8

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O8ooOoo〇()V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private OOO〇()V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8Oo8〇8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 9
    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    return-void

    .line 13
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8ooO8o()V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 17
    .line 18
    if-nez v0, :cond_2

    .line 19
    .line 20
    const/high16 v0, 0x42700000    # 60.0f

    .line 21
    .line 22
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 23
    .line 24
    .line 25
    move-result v6

    .line 26
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 29
    .line 30
    const/4 v3, 0x1

    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 32
    .line 33
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O〇O〇oO()Ljava/util/List;

    .line 34
    .line 35
    .line 36
    move-result-object v7

    .line 37
    move-object v1, v0

    .line 38
    move v4, v6

    .line 39
    move v5, v6

    .line 40
    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;-><init>(Landroid/app/Activity;ZIIILjava/util/List;)V

    .line 41
    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 44
    .line 45
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 46
    .line 47
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇80;

    .line 48
    .line 49
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/〇80;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->OOO〇O0(Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter$OnItemClickListener;)V

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 56
    .line 57
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$4;

    .line 58
    .line 59
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$4;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->addOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;)V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 66
    .line 67
    invoke-virtual {v0}, Lcom/google/android/material/tabs/TabLayout;->removeAllTabs()V

    .line 68
    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 71
    .line 72
    const v1, 0x7f1311d3

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O〇〇〇(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->addTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 83
    .line 84
    const v1, 0x7f1319bc

    .line 85
    .line 86
    .line 87
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O〇〇〇(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->addTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 92
    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 95
    .line 96
    const v1, 0x7f1319be

    .line 97
    .line 98
    .line 99
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O〇〇〇(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->addTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 104
    .line 105
    .line 106
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 107
    .line 108
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O08OO8o8O(Lcom/google/android/material/tabs/TabLayout;)V

    .line 109
    .line 110
    .line 111
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 112
    .line 113
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 114
    .line 115
    const/4 v2, 0x0

    .line 116
    invoke-direct {v0, v1, v2, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 117
    .line 118
    .line 119
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView;

    .line 120
    .line 121
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 122
    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView;

    .line 125
    .line 126
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 129
    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView;

    .line 132
    .line 133
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$5;

    .line 134
    .line 135
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$5;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 139
    .line 140
    .line 141
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oooo088〇()V

    .line 142
    .line 143
    .line 144
    return-void
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic OOo00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroid/widget/CheckBox;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO〇000(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8〇(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic OO〇80oO〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OO〇〇o0oO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o〇8([Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic Oo088O〇8〇(III)V
    .locals 0

    .line 1
    if-lt p1, p2, :cond_0

    .line 2
    .line 3
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOOO0(I)V

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 8
    .line 9
    const-string p2, "check not over by vip enhance property = $this"

    .line 10
    .line 11
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇0〇8〇0O()V

    .line 15
    .line 16
    .line 17
    :goto_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic Oo0O〇8800(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇O〇0oo0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Oo0o()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O:Landroid/widget/TextView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0o〇〇〇8o()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x1

    .line 11
    add-int/2addr v0, v1

    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 19
    .line 20
    invoke-virtual {v3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0000OOO()Z

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    const/4 v4, 0x0

    .line 25
    const/4 v5, 0x2

    .line 26
    const-string v6, "%d/%d"

    .line 27
    .line 28
    if-eqz v3, :cond_2

    .line 29
    .line 30
    if-ne v0, v2, :cond_1

    .line 31
    .line 32
    add-int/lit8 v0, v2, -0x1

    .line 33
    .line 34
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O:Landroid/widget/TextView;

    .line 35
    .line 36
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 37
    .line 38
    .line 39
    move-result-object v7

    .line 40
    new-array v5, v5, [Ljava/lang/Object;

    .line 41
    .line 42
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    aput-object v0, v5, v4

    .line 47
    .line 48
    sub-int/2addr v2, v1

    .line 49
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    aput-object v0, v5, v1

    .line 54
    .line 55
    invoke-static {v7, v6, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    if-lt v0, v2, :cond_3

    .line 64
    .line 65
    move v0, v2

    .line 66
    :cond_3
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O:Landroid/widget/TextView;

    .line 67
    .line 68
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 69
    .line 70
    .line 71
    move-result-object v7

    .line 72
    new-array v5, v5, [Ljava/lang/Object;

    .line 73
    .line 74
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    aput-object v0, v5, v4

    .line 79
    .line 80
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    aput-object v0, v5, v1

    .line 85
    .line 86
    invoke-static {v7, v6, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    .line 92
    .line 93
    :goto_0
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic Oo0o0o8(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "vip enhance limit use origin img"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    const/4 p2, 0x7

    .line 11
    invoke-static {p1, p2}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇o〇(Landroid/content/Context;I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇〇8〇8(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇oo88〇O〇8()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Oo8O()Z
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08O:Landroid/view/View;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 7
    .line 8
    const v2, 0x7f0a1a6a

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 19
    .line 20
    const v2, 0x7f0a0d10

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08O:Landroid/view/View;

    .line 28
    .line 29
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;->〇080:Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 32
    .line 33
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08O:Landroid/view/View;

    .line 34
    .line 35
    new-instance v5, Lcom/intsig/camscanner/multiimageedit/O8O〇;

    .line 36
    .line 37
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/multiimageedit/O8O〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 38
    .line 39
    .line 40
    sget-object v6, Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;->BOTTOM:Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;

    .line 41
    .line 42
    const v0, 0x7f130a1d

    .line 43
    .line 44
    .line 45
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v7

    .line 49
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 50
    .line 51
    const v8, 0x7f0a07ca

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v8

    .line 58
    const/4 v0, 0x1

    .line 59
    new-array v9, v0, [Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 60
    .line 61
    iget-object v10, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O0o〇〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 62
    .line 63
    aput-object v10, v9, v1

    .line 64
    .line 65
    invoke-virtual/range {v2 .. v9}, Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;->Oo08(Landroid/app/Activity;Landroid/view/View;Lcom/intsig/callback/Callback0;Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;Ljava/lang/String;Landroid/view/View;[Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)Z

    .line 66
    .line 67
    .line 68
    return v0
.end method

.method private Oo8oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private Oo8〇〇ooo()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8〇8〇O80:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->Oo08(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    xor-int/lit8 v0, v0, 0x1

    .line 22
    .line 23
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic OoO888(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "F-showNoNetworkDialog click no network dialog retry"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o0o()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OoOO〇(Ljava/util/List;IZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;IZ)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 6
    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 9
    .line 10
    sget-object v3, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_OCR_MULTI_CAPTURE_EDIT:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    move v4, p2

    .line 14
    move v6, p3

    .line 15
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mode_ocr/BatchOCRDataResultActivity;->O0〇(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mode_ocr/PageFromType;ILjava/lang/String;Z)Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    const/16 p2, 0x6e

    .line 20
    .line 21
    invoke-virtual {p0, p1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OoO〇OOo8o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O88Oo8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic Ooo8o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o〇〇88〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OooO〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇80oo〇0〇o(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OooO〇080()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "go2MultiTrimPreview: START"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 11
    .line 12
    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 23
    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 27
    .line 28
    invoke-virtual {v2, p0, v3, v1}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->〇o00〇〇Oo(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)Z

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-eqz v1, :cond_0

    .line 33
    .line 34
    const-string v1, "go2MultiTrimPreview: SPECIAL END"

    .line 35
    .line 36
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 41
    .line 42
    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o0〇(I)Landroid/content/Intent;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const/16 v1, 0x69

    .line 51
    .line 52
    invoke-static {p0, v0, v1}, Lcom/intsig/utils/TransitionUtil;->O8(Landroidx/fragment/app/Fragment;Landroid/content/Intent;I)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic Oo〇0o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "CSTestLimitPop"

    .line 2
    .line 3
    const-string p2, "check_existed_test"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8oOo0:Z

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Oo〇88〇()V
    .locals 5

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0oO〇oo00:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    new-array v0, v0, [I

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    const/4 v4, 0x5

    .line 12
    aput v4, v0, v3

    .line 13
    .line 14
    invoke-virtual {v1, v2, v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->O8(Landroid/content/Context;[I)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->〇o00〇〇Oo()V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/ooo〇8oO;

    .line 25
    .line 26
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/ooo〇8oO;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->oO80(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private Oo〇〇〇〇(Z)Lorg/json/JSONObject;
    .locals 2

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "FROM"

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    :try_start_0
    const-string p1, "back_key"

    .line 11
    .line 12
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const-string p1, "toolbar"

    .line 17
    .line 18
    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19
    .line 20
    .line 21
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 22
    .line 23
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    if-nez p1, :cond_1

    .line 28
    .line 29
    const-string p1, "from_part"

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 32
    .line 33
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .line 35
    .line 36
    goto :goto_1

    .line 37
    :catch_0
    move-exception p1

    .line 38
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 39
    .line 40
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 41
    .line 42
    .line 43
    :cond_1
    :goto_1
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O〇00O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇00o08(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "clickDelete: START! PS: THIS IS REAL DELETE, NOT OUTER DELETE!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->〇o〇()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    new-instance p1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v1, "clickDelete: intercept by actionClient="

    .line 24
    .line 25
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 29
    .line 30
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0〇8o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 42
    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8o088〇0()Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_1

    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oooo088〇()V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->OoO8()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo8O8o80()Z

    .line 66
    .line 67
    .line 68
    move-result p1

    .line 69
    if-eqz p1, :cond_2

    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oooo088〇()V

    .line 72
    .line 73
    .line 74
    :cond_2
    :goto_0
    const-string p1, "CSBatchResultDelete"

    .line 75
    .line 76
    const-string v0, "delete"

    .line 77
    .line 78
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇0888o(I)Z
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    const/16 v0, 0x9

    .line 6
    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 13
    :goto_1
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇0O088()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showGiveUpImportImage"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 9
    .line 10
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    const v1, 0x7f131d10

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const v1, 0x7f130a8d

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇O888o0o;

    .line 32
    .line 33
    invoke-direct {v1}, Lcom/intsig/camscanner/multiimageedit/〇O888o0o;-><init>()V

    .line 34
    .line 35
    .line 36
    const v2, 0x7f13057e

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/oo88o8O;

    .line 44
    .line 45
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/oo88o8O;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 46
    .line 47
    .line 48
    const v2, 0x7f130128

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O〇0O〇Oo〇o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8080o8〇〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇0o8o8〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o0〇8:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇0o8〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O80()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇88()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    :goto_0
    if-eqz v0, :cond_3

    .line 20
    .line 21
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8o8o〇()Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-nez v1, :cond_1

    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_1
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 31
    .line 32
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 33
    .line 34
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-nez v1, :cond_2

    .line 39
    .line 40
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 41
    .line 42
    new-instance v2, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    const-string v3, "clickSmudgePaper trimmedPaperPath is not exist "

    .line 48
    .line 49
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 53
    .line 54
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 55
    .line 56
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    return-void

    .line 67
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 68
    .line 69
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/Doodle;->〇〇888(Landroid/content/Context;)Landroid/content/Intent;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 74
    .line 75
    iget-wide v2, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 76
    .line 77
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 78
    .line 79
    iget-object v4, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 80
    .line 81
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 82
    .line 83
    invoke-static {v1, v2, v3, v4, v0}, Lcom/intsig/camscanner/doodle/Doodle;->〇080(Landroid/content/Intent;JLjava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    const/16 v0, 0x6b

    .line 87
    .line 88
    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 89
    .line 90
    .line 91
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 92
    .line 93
    const-string v1, "clickSmudgePaper succes"

    .line 94
    .line 95
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    return-void

    .line 99
    :cond_3
    :goto_1
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 100
    .line 101
    new-instance v2, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    const-string v3, "clickSmudgePaper error - multiImageEditPage="

    .line 107
    .line 108
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O〇8O0O80〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇08〇0oo0()Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇8Oo()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇80O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic O〇8O〇(Ljava/lang/String;)V
    .locals 22

    .line 1
    move-object/from16 v8, p0

    .line 2
    .line 3
    move-object/from16 v7, p1

    .line 4
    .line 5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->〇8o8o〇()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 20
    .line 21
    const-string v1, "trySavePaperPagesIntoDb but find it was not paper, return!"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    :cond_1
    sget-object v9, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 27
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v1, "F-trySavePaperPagesIntoDb paperPropertyString="

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    sget-object v10, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 49
    .line 50
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 51
    .line 52
    iget-wide v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 53
    .line 54
    const-wide/16 v11, 0x0

    .line 55
    .line 56
    const/4 v13, 0x1

    .line 57
    const/4 v2, -0x1

    .line 58
    cmp-long v3, v0, v11

    .line 59
    .line 60
    if-lez v3, :cond_3

    .line 61
    .line 62
    invoke-static {v10, v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    if-eqz v3, :cond_2

    .line 67
    .line 68
    invoke-static {v10, v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    add-int/2addr v2, v13

    .line 73
    move-wide v14, v0

    .line 74
    move/from16 v16, v2

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_2
    const-wide/16 v0, -0x1

    .line 78
    .line 79
    :cond_3
    move-wide v14, v0

    .line 80
    const/16 v16, -0x1

    .line 81
    .line 82
    :goto_0
    const/4 v6, 0x3

    .line 83
    const/16 v17, 0x0

    .line 84
    .line 85
    cmp-long v0, v14, v11

    .line 86
    .line 87
    if-gez v0, :cond_b

    .line 88
    .line 89
    sget-object v0, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->Oo08:Lcom/intsig/camscanner/paper/PaperPropertyEntity$Companion;

    .line 90
    .line 91
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/paper/PaperPropertyEntity$Companion;->〇080(Ljava/lang/String;)Lcom/intsig/camscanner/paper/PaperPropertyEntity;

    .line 92
    .line 93
    .line 94
    move-result-object v18

    .line 95
    if-eqz v18, :cond_4

    .line 96
    .line 97
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->O8()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    goto :goto_1

    .line 102
    :cond_4
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 103
    .line 104
    const v1, 0x7f130cd8

    .line 105
    .line 106
    .line 107
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    :goto_1
    invoke-static {v10, v0, v13}, Lcom/intsig/camscanner/util/Util;->OOO(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v5

    .line 115
    new-instance v4, Lcom/intsig/camscanner/datastruct/DocProperty;

    .line 116
    .line 117
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 118
    .line 119
    iget-object v2, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 120
    .line 121
    const/4 v3, 0x0

    .line 122
    const/16 v19, 0x0

    .line 123
    .line 124
    const/16 v20, 0x3e8

    .line 125
    .line 126
    iget-boolean v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 127
    .line 128
    move-object v0, v4

    .line 129
    move/from16 v21, v1

    .line 130
    .line 131
    move-object v1, v5

    .line 132
    move-object v11, v4

    .line 133
    move/from16 v4, v19

    .line 134
    .line 135
    move-object v12, v5

    .line 136
    move/from16 v5, v20

    .line 137
    .line 138
    const/4 v13, 0x3

    .line 139
    move/from16 v6, v21

    .line 140
    .line 141
    move-object/from16 v7, p1

    .line 142
    .line 143
    invoke-direct/range {v0 .. v7}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;)V

    .line 144
    .line 145
    .line 146
    invoke-static {v10, v11}, Lcom/intsig/camscanner/util/Util;->O8O〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/DocProperty;)Landroid/net/Uri;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    if-nez v0, :cond_5

    .line 151
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    .line 153
    .line 154
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    .line 156
    .line 157
    const-string v1, "handleTopicPapers error title: "

    .line 158
    .line 159
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    const-string v1, " | mDocId: "

    .line 166
    .line 167
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 174
    .line 175
    .line 176
    move-result-object v0

    .line 177
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    const/4 v0, 0x0

    .line 181
    goto :goto_2

    .line 182
    :cond_5
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 183
    .line 184
    .line 185
    move-result-wide v0

    .line 186
    move-wide v14, v0

    .line 187
    const/4 v0, 0x1

    .line 188
    const/16 v16, 0x1

    .line 189
    .line 190
    :goto_2
    new-instance v1, Ljava/util/ArrayList;

    .line 191
    .line 192
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 193
    .line 194
    .line 195
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 196
    .line 197
    .line 198
    move-result-object v2

    .line 199
    invoke-virtual {v2}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagTest2()Z

    .line 200
    .line 201
    .line 202
    move-result v2

    .line 203
    if-eqz v2, :cond_7

    .line 204
    .line 205
    const v2, 0x7f130f87

    .line 206
    .line 207
    .line 208
    invoke-virtual {v8, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v2

    .line 212
    invoke-static {v2}, Lcom/intsig/camscanner/app/DBUtil;->〇0O〇Oo(Ljava/lang/String;)J

    .line 213
    .line 214
    .line 215
    move-result-wide v3

    .line 216
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 217
    .line 218
    .line 219
    move-result-object v3

    .line 220
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    .line 222
    .line 223
    new-array v3, v13, [Landroid/util/Pair;

    .line 224
    .line 225
    new-instance v4, Landroid/util/Pair;

    .line 226
    .line 227
    const-string v5, "name"

    .line 228
    .line 229
    invoke-direct {v4, v5, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 230
    .line 231
    .line 232
    aput-object v4, v3, v17

    .line 233
    .line 234
    new-instance v2, Landroid/util/Pair;

    .line 235
    .line 236
    const-string v4, "type"

    .line 237
    .line 238
    const-string v5, "scene_from"

    .line 239
    .line 240
    invoke-direct {v2, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 241
    .line 242
    .line 243
    const/4 v4, 0x1

    .line 244
    aput-object v2, v3, v4

    .line 245
    .line 246
    new-instance v2, Landroid/util/Pair;

    .line 247
    .line 248
    iget-boolean v4, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 249
    .line 250
    if-eqz v4, :cond_6

    .line 251
    .line 252
    const-string v4, "CSImport"

    .line 253
    .line 254
    goto :goto_3

    .line 255
    :cond_6
    const-string v4, "CSScan"

    .line 256
    .line 257
    :goto_3
    const-string v5, "from_part"

    .line 258
    .line 259
    invoke-direct {v2, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 260
    .line 261
    .line 262
    const/4 v4, 0x2

    .line 263
    aput-object v2, v3, v4

    .line 264
    .line 265
    const-string v2, "CSNewDoc"

    .line 266
    .line 267
    const-string v4, "select_identified_label"

    .line 268
    .line 269
    invoke-static {v2, v4, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 270
    .line 271
    .line 272
    :cond_7
    if-eqz v18, :cond_9

    .line 273
    .line 274
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->〇o〇()Ljava/util/List;

    .line 275
    .line 276
    .line 277
    move-result-object v2

    .line 278
    if-eqz v2, :cond_9

    .line 279
    .line 280
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->〇o〇()Ljava/util/List;

    .line 281
    .line 282
    .line 283
    move-result-object v2

    .line 284
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 285
    .line 286
    .line 287
    move-result-object v2

    .line 288
    :cond_8
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 289
    .line 290
    .line 291
    move-result v3

    .line 292
    if-eqz v3, :cond_9

    .line 293
    .line 294
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 295
    .line 296
    .line 297
    move-result-object v3

    .line 298
    check-cast v3, Ljava/lang/String;

    .line 299
    .line 300
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 301
    .line 302
    .line 303
    move-result v4

    .line 304
    if-nez v4, :cond_8

    .line 305
    .line 306
    invoke-static {v3}, Lcom/intsig/camscanner/app/DBUtil;->〇0O〇Oo(Ljava/lang/String;)J

    .line 307
    .line 308
    .line 309
    move-result-wide v3

    .line 310
    const-wide/16 v5, 0x0

    .line 311
    .line 312
    cmp-long v7, v3, v5

    .line 313
    .line 314
    if-ltz v7, :cond_8

    .line 315
    .line 316
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 317
    .line 318
    .line 319
    move-result-object v3

    .line 320
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 321
    .line 322
    .line 323
    goto :goto_4

    .line 324
    :cond_9
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 325
    .line 326
    .line 327
    move-result v2

    .line 328
    if-lez v2, :cond_a

    .line 329
    .line 330
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 331
    .line 332
    invoke-static {v2, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 333
    .line 334
    .line 335
    move-result-object v2

    .line 336
    invoke-static {v10, v1, v2}, Lcom/intsig/camscanner/app/DBUtil;->O0〇OO8(Landroid/content/Context;Ljava/util/List;Landroid/net/Uri;)V

    .line 337
    .line 338
    .line 339
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 340
    .line 341
    const-string v2, "adding tags, "

    .line 342
    .line 343
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    .line 345
    .line 346
    :cond_a
    move v9, v0

    .line 347
    goto :goto_5

    .line 348
    :cond_b
    const/4 v13, 0x3

    .line 349
    const/4 v9, 0x0

    .line 350
    :goto_5
    iget-wide v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO〇00〇0O:J

    .line 351
    .line 352
    const-wide/16 v2, 0x0

    .line 353
    .line 354
    cmp-long v4, v0, v2

    .line 355
    .line 356
    if-lez v4, :cond_c

    .line 357
    .line 358
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 359
    .line 360
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 361
    .line 362
    .line 363
    move-result-object v0

    .line 364
    if-eqz v0, :cond_c

    .line 365
    .line 366
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 367
    .line 368
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 369
    .line 370
    .line 371
    move-result-object v0

    .line 372
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 373
    .line 374
    .line 375
    move-result v0

    .line 376
    const/4 v1, 0x1

    .line 377
    if-ne v0, v1, :cond_c

    .line 378
    .line 379
    const/4 v11, 0x1

    .line 380
    goto :goto_6

    .line 381
    :cond_c
    const/4 v11, 0x0

    .line 382
    :goto_6
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 383
    .line 384
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 385
    .line 386
    .line 387
    move-result-object v0

    .line 388
    if-eqz v0, :cond_12

    .line 389
    .line 390
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 391
    .line 392
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 393
    .line 394
    .line 395
    move-result-object v0

    .line 396
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 397
    .line 398
    .line 399
    move-result v12

    .line 400
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0oO()Z

    .line 401
    .line 402
    .line 403
    move-result v18

    .line 404
    move/from16 v7, v16

    .line 405
    .line 406
    const/4 v6, 0x0

    .line 407
    :goto_7
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 408
    .line 409
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 410
    .line 411
    .line 412
    move-result-object v0

    .line 413
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 414
    .line 415
    .line 416
    move-result v0

    .line 417
    if-ge v6, v0, :cond_10

    .line 418
    .line 419
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 420
    .line 421
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 422
    .line 423
    .line 424
    move-result-object v0

    .line 425
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 426
    .line 427
    .line 428
    move-result-object v0

    .line 429
    move-object v4, v0

    .line 430
    check-cast v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 431
    .line 432
    if-nez v4, :cond_d

    .line 433
    .line 434
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 435
    .line 436
    new-instance v1, Ljava/lang/StringBuilder;

    .line 437
    .line 438
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 439
    .line 440
    .line 441
    const-string v2, "F-trySavePaperPagesIntoDb, ERROR!["

    .line 442
    .line 443
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    .line 445
    .line 446
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 447
    .line 448
    .line 449
    const-string v2, "] page is null"

    .line 450
    .line 451
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    .line 453
    .line 454
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 455
    .line 456
    .line 457
    move-result-object v1

    .line 458
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    .line 460
    .line 461
    move/from16 v17, v6

    .line 462
    .line 463
    goto/16 :goto_9

    .line 464
    .line 465
    :cond_d
    if-eqz v11, :cond_e

    .line 466
    .line 467
    iget-boolean v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇08〇:Z

    .line 468
    .line 469
    if-eqz v0, :cond_e

    .line 470
    .line 471
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 472
    .line 473
    iget-object v1, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 474
    .line 475
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o0O:Ljava/lang/String;

    .line 476
    .line 477
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/paper/PaperUtil;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 478
    .line 479
    .line 480
    move-result-object v0

    .line 481
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 482
    .line 483
    new-instance v2, Ljava/lang/StringBuilder;

    .line 484
    .line 485
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 486
    .line 487
    .line 488
    const-string v3, "F-trySavePaperPagesIntoDb, "

    .line 489
    .line 490
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 491
    .line 492
    .line 493
    iget-object v3, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 494
    .line 495
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 496
    .line 497
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    .line 499
    .line 500
    const-string v3, " -> realRawPath="

    .line 501
    .line 502
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    .line 504
    .line 505
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    .line 507
    .line 508
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 509
    .line 510
    .line 511
    move-result-object v2

    .line 512
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    .line 514
    .line 515
    iget-object v1, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 516
    .line 517
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 518
    .line 519
    invoke-static {v1, v0}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 520
    .line 521
    .line 522
    const/4 v1, 0x1

    .line 523
    move-object/from16 v0, p0

    .line 524
    .line 525
    move-object v2, v4

    .line 526
    move v3, v7

    .line 527
    move-object/from16 p1, v4

    .line 528
    .line 529
    move-wide v4, v14

    .line 530
    move/from16 v17, v6

    .line 531
    .line 532
    move v6, v12

    .line 533
    move/from16 v16, v7

    .line 534
    .line 535
    move/from16 v7, v17

    .line 536
    .line 537
    invoke-direct/range {v0 .. v7}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇0o8〇(ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;IJII)V

    .line 538
    .line 539
    .line 540
    goto :goto_8

    .line 541
    :cond_e
    move-object/from16 p1, v4

    .line 542
    .line 543
    move/from16 v17, v6

    .line 544
    .line 545
    move/from16 v16, v7

    .line 546
    .line 547
    move-object/from16 v0, p0

    .line 548
    .line 549
    move v1, v11

    .line 550
    move-object/from16 v2, p1

    .line 551
    .line 552
    move/from16 v3, v16

    .line 553
    .line 554
    move-wide v4, v14

    .line 555
    move v6, v12

    .line 556
    move/from16 v7, v17

    .line 557
    .line 558
    invoke-direct/range {v0 .. v7}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇0o8〇(ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;IJII)V

    .line 559
    .line 560
    .line 561
    :goto_8
    if-eqz v18, :cond_f

    .line 562
    .line 563
    move-object/from16 v0, p1

    .line 564
    .line 565
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 566
    .line 567
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 568
    .line 569
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 570
    .line 571
    .line 572
    move-result v1

    .line 573
    if-eqz v1, :cond_f

    .line 574
    .line 575
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 576
    .line 577
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 578
    .line 579
    invoke-static {v0}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 580
    .line 581
    .line 582
    move-result-object v0

    .line 583
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->〇0〇O0088o(Ljava/lang/String;)V

    .line 584
    .line 585
    .line 586
    :cond_f
    add-int/lit8 v7, v16, 0x1

    .line 587
    .line 588
    :goto_9
    add-int/lit8 v6, v17, 0x1

    .line 589
    .line 590
    goto/16 :goto_7

    .line 591
    .line 592
    :cond_10
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 593
    .line 594
    new-instance v1, Ljava/lang/StringBuilder;

    .line 595
    .line 596
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 597
    .line 598
    .line 599
    const-string v2, "F-trySavePaperPagesIntoDb successfully!, DOCID="

    .line 600
    .line 601
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    .line 603
    .line 604
    invoke-virtual {v1, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 605
    .line 606
    .line 607
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 608
    .line 609
    .line 610
    move-result-object v1

    .line 611
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    .line 613
    .line 614
    const-string v0, ""

    .line 615
    .line 616
    invoke-static {v10, v14, v15, v0}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o0O0O8(Landroid/content/Context;JLjava/lang/String;)V

    .line 617
    .line 618
    .line 619
    if-eqz v9, :cond_11

    .line 620
    .line 621
    const/4 v4, 0x1

    .line 622
    goto :goto_a

    .line 623
    :cond_11
    const/4 v4, 0x3

    .line 624
    :goto_a
    const/4 v5, 0x1

    .line 625
    const/4 v6, 0x1

    .line 626
    move-object v1, v10

    .line 627
    move-wide v2, v14

    .line 628
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 629
    .line 630
    .line 631
    :cond_12
    if-nez v11, :cond_13

    .line 632
    .line 633
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 634
    .line 635
    const/4 v1, 0x1

    .line 636
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8〇o(Z)V

    .line 637
    .line 638
    .line 639
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 640
    .line 641
    const-string v1, "F-trySavePaperPagesIntoDb, !isChangeNotInsert -> discardAllData in work thread"

    .line 642
    .line 643
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    .line 645
    .line 646
    :cond_13
    iget-object v0, v8, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 647
    .line 648
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/o〇0OOo〇0;

    .line 649
    .line 650
    invoke-direct {v1, v8, v14, v15}, Lcom/intsig/camscanner/multiimageedit/o〇0OOo〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;J)V

    .line 651
    .line 652
    .line 653
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 654
    .line 655
    .line 656
    return-void
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public static synthetic O〇8〇008(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;J)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOOo(J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇O800oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o88〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇O88(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 14
    .line 15
    new-instance v2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v3, "get liveDataForServerEnhanceError, current="

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const/4 v3, 0x0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    iget-object v4, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 29
    .line 30
    if-eqz v4, :cond_0

    .line 31
    .line 32
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    move-object v4, v3

    .line 36
    :goto_0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v4, ";model="

    .line 40
    .line 41
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    if-eqz p1, :cond_1

    .line 45
    .line 46
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 47
    .line 48
    :cond_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    const-string v3, "; waiting="

    .line 52
    .line 53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 57
    .line 58
    iget-boolean v3, v3, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇08O〇00〇o:Z

    .line 59
    .line 60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 71
    .line 72
    iget-boolean v2, v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇08O〇00〇o:Z

    .line 73
    .line 74
    if-eqz v2, :cond_2

    .line 75
    .line 76
    const/4 v2, 0x0

    .line 77
    iput-boolean v2, v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇08O〇00〇o:Z

    .line 78
    .line 79
    if-eqz p1, :cond_2

    .line 80
    .line 81
    if-eqz v0, :cond_2

    .line 82
    .line 83
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 84
    .line 85
    if-eqz v0, :cond_2

    .line 86
    .line 87
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 88
    .line 89
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 90
    .line 91
    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-eqz p1, :cond_2

    .line 96
    .line 97
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o08()V

    .line 98
    .line 99
    .line 100
    const/4 p1, 0x2

    .line 101
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇O0o〇(I)V

    .line 102
    .line 103
    .line 104
    :cond_2
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private O〇O〇88O8O()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "go2DirNavigation"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Landroid/content/Intent;

    .line 9
    .line 10
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const-class v2, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;

    .line 15
    .line 16
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 20
    .line 21
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 22
    .line 23
    const-string v2, "extra_folder_id"

    .line 24
    .line 25
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    .line 27
    .line 28
    const/4 v1, 0x1

    .line 29
    new-array v1, v1, [J

    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 32
    .line 33
    iget-wide v2, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 34
    .line 35
    const/4 v4, 0x0

    .line 36
    aput-wide v2, v1, v4

    .line 37
    .line 38
    const-string v2, "extra_multi_doc_id"

    .line 39
    .line 40
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 41
    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 44
    .line 45
    iget-boolean v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 46
    .line 47
    const-string v2, "extra_offline_folder"

    .line 48
    .line 49
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 50
    .line 51
    .line 52
    const-string v1, "action"

    .line 53
    .line 54
    const/4 v2, 0x2

    .line 55
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 56
    .line 57
    .line 58
    const-string v1, "EXTRA_FROM_PART"

    .line 59
    .line 60
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 61
    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    .line 64
    .line 65
    const/16 v1, 0x6a

    .line 66
    .line 67
    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O〇o0O0OO0()V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8Oo8〇8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    if-nez v0, :cond_5

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-gtz v0, :cond_1

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 22
    .line 23
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 28
    .line 29
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O〇O〇oO()Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 34
    .line 35
    const/16 v4, 0x4a

    .line 36
    .line 37
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    const v5, 0x7f070520

    .line 46
    .line 47
    .line 48
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 53
    .line 54
    .line 55
    move-result v5

    .line 56
    mul-int v5, v5, v3

    .line 57
    .line 58
    if-ge v5, v0, :cond_2

    .line 59
    .line 60
    const/high16 v3, 0x3f800000    # 1.0f

    .line 61
    .line 62
    int-to-float v0, v0

    .line 63
    mul-float v0, v0, v3

    .line 64
    .line 65
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 66
    .line 67
    .line 68
    move-result v2

    .line 69
    int-to-float v2, v2

    .line 70
    div-float/2addr v0, v2

    .line 71
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    :goto_0
    move v7, v0

    .line 76
    goto :goto_2

    .line 77
    :cond_2
    div-int v2, v0, v3

    .line 78
    .line 79
    const/4 v3, 0x5

    .line 80
    if-gt v2, v3, :cond_3

    .line 81
    .line 82
    const/high16 v2, 0x40900000    # 4.5f

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_3
    int-to-float v2, v2

    .line 86
    const/high16 v3, 0x3f000000    # 0.5f

    .line 87
    .line 88
    sub-float/2addr v2, v3

    .line 89
    :goto_1
    int-to-float v0, v0

    .line 90
    div-float/2addr v0, v2

    .line 91
    float-to-int v0, v0

    .line 92
    goto :goto_0

    .line 93
    :goto_2
    sub-int v0, v7, v4

    .line 94
    .line 95
    sub-int v8, v0, v4

    .line 96
    .line 97
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 98
    .line 99
    new-instance v2, Ljava/lang/StringBuilder;

    .line 100
    .line 101
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .line 103
    .line 104
    const-string v3, " oneItemWidth="

    .line 105
    .line 106
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v2

    .line 116
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 120
    .line 121
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    const v2, 0x7f0701be

    .line 126
    .line 127
    .line 128
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 129
    .line 130
    .line 131
    move-result v9

    .line 132
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 133
    .line 134
    iget-object v6, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 135
    .line 136
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 137
    .line 138
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O〇O〇oO()Ljava/util/List;

    .line 139
    .line 140
    .line 141
    move-result-object v10

    .line 142
    move-object v5, v0

    .line 143
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;-><init>(Landroid/app/Activity;IIILjava/util/List;)V

    .line 144
    .line 145
    .line 146
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 147
    .line 148
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 149
    .line 150
    if-eqz v2, :cond_4

    .line 151
    .line 152
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->〇0000OOO(Z)V

    .line 153
    .line 154
    .line 155
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 156
    .line 157
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 158
    .line 159
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 160
    .line 161
    .line 162
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 163
    .line 164
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/Ooo8〇〇;

    .line 165
    .line 166
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/Ooo8〇〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 167
    .line 168
    .line 169
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->OOO〇O0(Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter$OnItemClickListener;)V

    .line 170
    .line 171
    .line 172
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo0〇〇O:Landroid/view/View;

    .line 173
    .line 174
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 175
    .line 176
    .line 177
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇〇O()Landroid/view/animation/Animation;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$16;

    .line 182
    .line 183
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$16;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 187
    .line 188
    .line 189
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo0〇〇O:Landroid/view/View;

    .line 190
    .line 191
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 192
    .line 193
    .line 194
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oooo088〇()V

    .line 195
    .line 196
    .line 197
    return-void
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static bridge synthetic O〇o8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇oo8O80(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/TaskType;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OOO0o〇(Lcom/intsig/camscanner/multiimageedit/model/TaskType;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇〇O()Landroid/view/animation/Animation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8O0880:Landroid/view/animation/Animation;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const v0, 0x7f010042

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇〇8〇〇(I)Landroid/view/animation/Animation;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8O0880:Landroid/view/animation/Animation;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8O0880:Landroid/view/animation/Animation;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/callback/Callback0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O888o8(Lcom/intsig/callback/Callback0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇〇o8O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditPreviewViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private init()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO0o88()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO8o〇o〇8()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO800o()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇8oooO〇()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0Oo〇8()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo0〇080()V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇80O80O〇0()V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O()V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇8o00()Ljava/lang/Boolean;

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO0oO()V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic o00(Lcom/intsig/owlery/DialogOwl;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O〇0(Lcom/intsig/owlery/DialogOwl;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 12
    .line 13
    new-instance v2, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v3, "owl showDialog = "

    .line 19
    .line 20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Lcom/intsig/owlery/TheOwlery;->o〇0(Lcom/intsig/owlery/DialogOwl;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private o00OOO8()V
    .locals 5

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0oO〇oo00:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    new-array v0, v0, [I

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    const/4 v4, 0x3

    .line 12
    aput v4, v0, v3

    .line 13
    .line 14
    invoke-virtual {v1, v2, v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->O8(Landroid/content/Context;[I)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->〇o00〇〇Oo()V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/O0O8OO088;

    .line 25
    .line 26
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/O0O8OO088;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->oO80(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o00o0O〇〇o()V
    .locals 10

    .line 1
    const-class v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/singleton/Singleton;->〇080(Ljava/lang/Class;)Lcom/intsig/singleton/Singleton;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 10
    .line 11
    iget-boolean v2, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    if-eqz v2, :cond_0

    .line 15
    .line 16
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 17
    .line 18
    iget-wide v5, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 19
    .line 20
    const/4 v7, 0x2

    .line 21
    const/4 v8, 0x1

    .line 22
    const/4 v9, 0x0

    .line 23
    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 24
    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_0
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 28
    .line 29
    if-eqz v1, :cond_2

    .line 30
    .line 31
    array-length v1, v1

    .line 32
    if-lez v1, :cond_2

    .line 33
    .line 34
    new-instance v1, Ljava/util/ArrayList;

    .line 35
    .line 36
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 37
    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 40
    .line 41
    iget-object v2, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 42
    .line 43
    array-length v4, v2

    .line 44
    const/4 v5, 0x0

    .line 45
    :goto_0
    if-ge v5, v4, :cond_1

    .line 46
    .line 47
    aget-wide v6, v2, v5

    .line 48
    .line 49
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 50
    .line 51
    .line 52
    move-result-object v6

    .line 53
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    add-int/lit8 v5, v5, 0x1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 60
    .line 61
    const/4 v4, 0x2

    .line 62
    invoke-static {v2, v1, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO〇00〇8oO(Landroid/content/Context;Ljava/util/List;I)V

    .line 63
    .line 64
    .line 65
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 66
    .line 67
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 68
    .line 69
    iget-wide v4, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 70
    .line 71
    invoke-static {v1, v4, v5}, Lcom/intsig/camscanner/db/dao/DocumentDao;->Oo〇O(Landroid/content/Context;J)V

    .line 72
    .line 73
    .line 74
    :goto_1
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O(Z)V

    .line 75
    .line 76
    .line 77
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 78
    .line 79
    const-string v1, "extra_from_import_image"

    .line 80
    .line 81
    if-eqz v0, :cond_3

    .line 82
    .line 83
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 84
    .line 85
    if-eqz v0, :cond_3

    .line 86
    .line 87
    new-instance v0, Landroid/content/Intent;

    .line 88
    .line 89
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 90
    .line 91
    .line 92
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 93
    .line 94
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    .line 96
    .line 97
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 98
    .line 99
    invoke-virtual {v1, v3, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 100
    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8Oo8〇8()Z

    .line 104
    .line 105
    .line 106
    move-result v0

    .line 107
    if-eqz v0, :cond_4

    .line 108
    .line 109
    new-instance v0, Landroid/content/Intent;

    .line 110
    .line 111
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 112
    .line 113
    .line 114
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 115
    .line 116
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 117
    .line 118
    .line 119
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 120
    .line 121
    invoke-virtual {v1, v3, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 122
    .line 123
    .line 124
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 125
    .line 126
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 127
    .line 128
    .line 129
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o00oooo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08O:Landroid/view/View;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/16 v1, 0x8

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇O〇0O0〇()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O0o〇〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08O:Landroid/view/View;

    .line 20
    .line 21
    const v1, 0x7f0a1212

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    check-cast v0, Lcom/intsig/comm/widget/CustomTextView;

    .line 29
    .line 30
    if-eqz v0, :cond_3

    .line 31
    .line 32
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    if-nez v1, :cond_2

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O0o〇〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 46
    .line 47
    .line 48
    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O0o〇〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 50
    .line 51
    :cond_3
    :goto_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O00o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o08(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->oo88o8O(I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇〇8〇8(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o088O8800(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO〇oo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o088〇〇()Lorg/json/JSONObject;
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "SCHEME"

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 9
    .line 10
    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-eqz v2, :cond_0

    .line 15
    .line 16
    const-string v2, "on"

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const-string v2, "off"

    .line 20
    .line 21
    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .line 23
    .line 24
    goto :goto_1

    .line 25
    :catch_0
    move-exception v1

    .line 26
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 27
    .line 28
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    :goto_1
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o08O80O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 4
    .line 5
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇〇888:I

    .line 6
    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p1, 0x0

    .line 12
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 13
    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v2, "triggerWhenOnePageFinishWhileLoading, finish="

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0ooO()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v2, "; encodeOK="

    .line 34
    .line 35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    if-eqz p1, :cond_1

    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O80〇()V

    .line 51
    .line 52
    .line 53
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o08()V

    .line 54
    .line 55
    .line 56
    return-void

    .line 57
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0ooO()Z

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    if-eqz p1, :cond_2

    .line 64
    .line 65
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O80〇()V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇OO()V

    .line 69
    .line 70
    .line 71
    :cond_2
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private o08o(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O8ooOoo〇()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOOO0(I)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o80()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-nez v1, :cond_2

    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇oOO8O8()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 26
    .line 27
    invoke-static {v0, p1}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇o〇(Landroid/content/Context;I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    iget v0, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 32
    .line 33
    const/16 v1, 0x9

    .line 34
    .line 35
    if-ne v0, v1, :cond_1

    .line 36
    .line 37
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->REMOVE_HANDWRITING_FILTER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->REMOVE_WATERMARK_FILTER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 41
    .line 42
    :goto_0
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 43
    .line 44
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-direct {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 49
    .line 50
    .line 51
    const/4 v2, 0x1

    .line 52
    new-array v2, v2, [Ljava/lang/Object;

    .line 53
    .line 54
    const/4 v3, 0x0

    .line 55
    iget-object p1, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 56
    .line 57
    aput-object p1, v2, v3

    .line 58
    .line 59
    const p1, 0x7f131a26

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0, p1, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    invoke-virtual {v1, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇0〇O0088o;

    .line 71
    .line 72
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/multiimageedit/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/purchase/entity/Function;)V

    .line 73
    .line 74
    .line 75
    const v0, 0x7f131a3c

    .line 76
    .line 77
    .line 78
    const v2, 0x7f060207

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/OoO8;

    .line 86
    .line 87
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/OoO8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 88
    .line 89
    .line 90
    const v1, 0x7f131a25

    .line 91
    .line 92
    .line 93
    invoke-virtual {p1, v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 102
    .line 103
    .line 104
    return-void

    .line 105
    :cond_2
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/o800o8O;

    .line 110
    .line 111
    invoke-direct {v2, p0, v0, p1}, Lcom/intsig/camscanner/multiimageedit/o800o8O;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;II)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v1, v2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 115
    .line 116
    .line 117
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private o08〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    const v1, 0x7f130c8f

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const v1, 0x7f130cb5

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇o;

    .line 25
    .line 26
    invoke-direct {v1}, Lcom/intsig/camscanner/multiimageedit/〇o;-><init>()V

    .line 27
    .line 28
    .line 29
    const v2, 0x7f13057e

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/o0ooO;

    .line 37
    .line 38
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/o0ooO;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 39
    .line 40
    .line 41
    const v2, 0x7f131db2

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o08〇808()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "initToolbarRightForType - isFromTopicPaper"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇80o:Landroid/widget/TextView;

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const v2, 0x7f0d0107

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Landroid/widget/TextView;

    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇80o:Landroid/widget/TextView;

    .line 33
    .line 34
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇0〇08()V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇80o:Landroid/widget/TextView;

    .line 38
    .line 39
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇80o:Landroid/widget/TextView;

    .line 43
    .line 44
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/o8oO〇;

    .line 45
    .line 46
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/o8oO〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇80o:Landroid/widget/TextView;

    .line 53
    .line 54
    const-string v1, "#19BCAA"

    .line 55
    .line 56
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇80o:Landroid/widget/TextView;

    .line 66
    .line 67
    invoke-virtual {v0, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarMenu(Landroid/view/View;)V

    .line 68
    .line 69
    .line 70
    :cond_1
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic o0O()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0oO〇oo00:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o0O0O〇〇〇0(Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO888O0〇(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0O8o00()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo8ooo8O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 7
    .line 8
    .line 9
    sget-object v0, Lcom/intsig/nativelib/OcrLanguage$LangMode;->OCR:Lcom/intsig/nativelib/OcrLanguage$LangMode;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/ocrapi/OcrLanguagesCompat;->〇o00〇〇Oo(Lcom/intsig/nativelib/OcrLanguage$LangMode;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, ","

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    array-length v1, v0

    .line 22
    const/4 v2, 0x0

    .line 23
    const/4 v3, 0x0

    .line 24
    const/4 v4, 0x0

    .line 25
    :goto_0
    if-ge v3, v1, :cond_4

    .line 26
    .line 27
    aget-object v5, v0, v3

    .line 28
    .line 29
    const-string v6, "zh"

    .line 30
    .line 31
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    move-result v6

    .line 35
    if-eqz v6, :cond_1

    .line 36
    .line 37
    const-string v5, "zh-s"

    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_1
    const-string v6, "zht"

    .line 41
    .line 42
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    move-result v6

    .line 46
    if-eqz v6, :cond_2

    .line 47
    .line 48
    const-string v5, "zh-t"

    .line 49
    .line 50
    :cond_2
    :goto_1
    iget-object v6, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 51
    .line 52
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 53
    .line 54
    .line 55
    move-result-object v6

    .line 56
    const v7, 0x7f0d045d

    .line 57
    .line 58
    .line 59
    iget-object v8, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo8ooo8O:Landroid/widget/LinearLayout;

    .line 60
    .line 61
    invoke-virtual {v6, v7, v8, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 62
    .line 63
    .line 64
    move-result-object v6

    .line 65
    check-cast v6, Landroid/widget/TextView;

    .line 66
    .line 67
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 68
    .line 69
    .line 70
    move-result-object v7

    .line 71
    if-lez v4, :cond_3

    .line 72
    .line 73
    instance-of v8, v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 74
    .line 75
    if-eqz v8, :cond_3

    .line 76
    .line 77
    move-object v8, v7

    .line 78
    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 79
    .line 80
    iget-object v9, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 81
    .line 82
    const/4 v10, 0x6

    .line 83
    invoke-static {v9, v10}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 84
    .line 85
    .line 86
    move-result v9

    .line 87
    iput v9, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 88
    .line 89
    invoke-virtual {v6, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    .line 91
    .line 92
    :cond_3
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    .line 94
    .line 95
    const v5, 0x7f081128

    .line 96
    .line 97
    .line 98
    invoke-virtual {v6, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 99
    .line 100
    .line 101
    const v5, -0xded5c5

    .line 102
    .line 103
    .line 104
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 105
    .line 106
    .line 107
    iget-object v5, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo8ooo8O:Landroid/widget/LinearLayout;

    .line 108
    .line 109
    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 110
    .line 111
    .line 112
    add-int/lit8 v4, v4, 0x1

    .line 113
    .line 114
    add-int/lit8 v3, v3, 0x1

    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_4
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic o0OO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇o88:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o0Oo(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o0o8Oo(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o0Oo〇()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    iget-wide v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 4
    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    cmp-long v4, v0, v2

    .line 8
    .line 9
    if-gtz v4, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 12
    .line 13
    const-string v1, "selfSignature docId <= 0"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 32
    .line 33
    iget-wide v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 34
    .line 35
    const-string v3, "ENTRANCE_PDF_PREVIEW"

    .line 36
    .line 37
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/camscanner/newsign/signtype/SelectSignTypeHelper;->〇80〇808〇O(Landroidx/fragment/app/FragmentActivity;JLjava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 44
    .line 45
    iget-wide v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 46
    .line 47
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 52
    .line 53
    sget-object v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_CS_SCAN:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 60
    .line 61
    .line 62
    move-result-object v5

    .line 63
    const-string v6, ""

    .line 64
    .line 65
    const/4 v7, 0x0

    .line 66
    const/4 v8, 0x1

    .line 67
    const/4 v9, 0x0

    .line 68
    const/4 v10, 0x0

    .line 69
    invoke-static/range {v3 .. v10}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->OO0o〇〇(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/Integer;Ljava/lang/String;ZZZZ)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o0o〇〇〇8o()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇8o〇〇8080()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    add-int/lit8 v1, v1, -0x1

    .line 14
    .line 15
    if-le v0, v1, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    add-int/lit8 v0, v0, -0x1

    .line 24
    .line 25
    :cond_0
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o0〇OO008O()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v0, "CSTestPaper"

    .line 8
    .line 9
    const-string v1, "complete"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇0()V

    .line 15
    .line 16
    .line 17
    goto/16 :goto_4

    .line 18
    .line 19
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    .line 20
    .line 21
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 22
    .line 23
    .line 24
    :try_start_0
    invoke-static {}, Lcom/intsig/camscanner/capture/capturemode/CaptureModePreferenceHelper;->〇oo〇()Z

    .line 25
    .line 26
    .line 27
    move-result v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    const-string v2, "type"

    .line 29
    .line 30
    if-nez v1, :cond_2

    .line 31
    .line 32
    :try_start_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇oO08〇o0()Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-eqz v3, :cond_1

    .line 37
    .line 38
    const-string v3, "auto_crop_on"

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    const-string v3, "auto_crop_off"

    .line 42
    .line 43
    :goto_0
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 44
    .line 45
    .line 46
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇()Z

    .line 47
    .line 48
    .line 49
    move-result v3
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 50
    const-string v4, "from_part"

    .line 51
    .line 52
    if-eqz v3, :cond_3

    .line 53
    .line 54
    :try_start_2
    const-string v3, "cs_scan"

    .line 55
    .line 56
    invoke-virtual {v0, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 57
    .line 58
    .line 59
    const-string v3, "from"

    .line 60
    .line 61
    const-string v4, "bank_statement"

    .line 62
    .line 63
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 64
    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_3
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 68
    .line 69
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    if-nez v3, :cond_4

    .line 74
    .line 75
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 76
    .line 77
    invoke-virtual {v0, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 78
    .line 79
    .line 80
    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 81
    .line 82
    if-eqz v3, :cond_a

    .line 83
    .line 84
    invoke-virtual {v3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 85
    .line 86
    .line 87
    move-result v3

    .line 88
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 89
    .line 90
    invoke-virtual {v4}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0000OOO()Z

    .line 91
    .line 92
    .line 93
    move-result v4

    .line 94
    if-eqz v4, :cond_5

    .line 95
    .line 96
    add-int/lit8 v3, v3, -0x1

    .line 97
    .line 98
    :cond_5
    if-eqz v1, :cond_7

    .line 99
    .line 100
    const/4 v1, 0x1

    .line 101
    if-le v3, v1, :cond_6

    .line 102
    .line 103
    const-string v1, "batch"

    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_6
    const-string v1, "single"

    .line 107
    .line 108
    :goto_2
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 109
    .line 110
    .line 111
    :cond_7
    const-string v1, "num"

    .line 112
    .line 113
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 114
    .line 115
    .line 116
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 117
    .line 118
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇o〇:Ljava/util/List;

    .line 119
    .line 120
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 125
    .line 126
    .line 127
    move-result v2

    .line 128
    if-eqz v2, :cond_a

    .line 129
    .line 130
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    move-result-object v2

    .line 134
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 135
    .line 136
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 137
    .line 138
    if-eqz v2, :cond_8

    .line 139
    .line 140
    iget v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 141
    .line 142
    const/16 v3, -0xc

    .line 143
    .line 144
    if-ne v2, v3, :cond_8

    .line 145
    .line 146
    invoke-static {}, Lcom/intsig/camscanner/experiment/SuperFilterStyleExp;->〇o〇()Z

    .line 147
    .line 148
    .line 149
    move-result v1
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 150
    const-string v2, "scheme"

    .line 151
    .line 152
    if-eqz v1, :cond_9

    .line 153
    .line 154
    :try_start_3
    const-string v1, "magic_ultra"

    .line 155
    .line 156
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 157
    .line 158
    .line 159
    goto :goto_3

    .line 160
    :cond_9
    const-string v1, "super_filter"

    .line 161
    .line 162
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 163
    .line 164
    .line 165
    goto :goto_3

    .line 166
    :catch_0
    move-exception v1

    .line 167
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 168
    .line 169
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170
    .line 171
    .line 172
    :cond_a
    :goto_3
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->logForNewDocDone()V

    .line 173
    .line 174
    .line 175
    const-string v1, "CSBatchResult"

    .line 176
    .line 177
    const-string v2, "confirm"

    .line 178
    .line 179
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 180
    .line 181
    .line 182
    :goto_4
    sget-object v0, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->INSTANCE:Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;

    .line 183
    .line 184
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 185
    .line 186
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 187
    .line 188
    .line 189
    move-result-object v1

    .line 190
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->uploadRecordedImage(Ljava/util/List;)V

    .line 191
    .line 192
    .line 193
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇80oo8()V

    .line 194
    .line 195
    .line 196
    return-void
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private o0〇O〇0oo0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 8

    .line 1
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8oOOo:Landroid/widget/TextView;

    .line 4
    .line 5
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O〇〇O8:Landroid/widget/TextView;

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v1, :cond_4

    .line 12
    .line 13
    if-eqz v2, :cond_4

    .line 14
    .line 15
    if-eqz v3, :cond_4

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/16 v0, 0x8

    .line 21
    .line 22
    if-nez p1, :cond_1

    .line 23
    .line 24
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 25
    .line 26
    const-string v2, "tryShowImageQualityTips is not image item"

    .line 27
    .line 28
    invoke-static {p1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    iget-object v4, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 36
    .line 37
    invoke-virtual {v4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    iget-boolean v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇OOo:Z

    .line 42
    .line 43
    if-eqz v4, :cond_2

    .line 44
    .line 45
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 46
    .line 47
    const-string v2, "tryShowImageQualityTips skipImageQuality = true"

    .line 48
    .line 49
    invoke-static {p1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v5}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->OOO〇O0()V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 56
    .line 57
    .line 58
    return-void

    .line 59
    :cond_2
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 60
    .line 61
    iget p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 62
    .line 63
    invoke-static {p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isSuperFilterMode(I)Z

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    if-eqz p1, :cond_3

    .line 68
    .line 69
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 70
    .line 71
    const-string v2, "tryShowImageQualityTips enhanceModeIndex = super_filter"

    .line 72
    .line 73
    invoke-static {p1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 77
    .line 78
    .line 79
    return-void

    .line 80
    :cond_3
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 81
    .line 82
    const/4 v4, 0x0

    .line 83
    const/4 v6, 0x1

    .line 84
    const-string v7, "CSBatchEnhanceSnackBar"

    .line 85
    .line 86
    invoke-static/range {v0 .. v7}, Lcom/intsig/camscanner/demoire/ImageQualityUtil;->〇o〇(Landroid/content/Context;Landroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/intsig/camscanner/demoire/ImageQualityHelper;ZLjava/lang/String;)V

    .line 87
    .line 88
    .line 89
    :cond_4
    :goto_0
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic o0〇〇00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O0Oo〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic o0〇〇00〇o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o0〇8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o80()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8o8O〇O()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 17
    :goto_1
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic o8080o8〇〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "showVipFilterTimesLimitDialog ok"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o808Oo(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->oo88o8O(I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_2

    .line 8
    .line 9
    iget v0, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 10
    .line 11
    const/16 v1, 0x9

    .line 12
    .line 13
    if-eq v0, v1, :cond_1

    .line 14
    .line 15
    const/16 v1, 0x8

    .line 16
    .line 17
    if-ne v0, v1, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇〇8〇8(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V

    .line 21
    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$3;

    .line 27
    .line 28
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$3;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V

    .line 29
    .line 30
    .line 31
    const/4 p1, 0x1

    .line 32
    const-string v2, "other"

    .line 33
    .line 34
    invoke-static {v0, v1, p1, v2, v2}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/ipo/IPOCheckCallback;ZLjava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    :cond_2
    :goto_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇O88(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic o80oO(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o88(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇OO8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o88o88(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇0o〇〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o88oo〇O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Ooo8o:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8O〇008(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/google/android/material/tabs/TabLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8o0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo8O8o80()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8o0o8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/mode_ocr/OCRClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOoO8OO〇:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o8o8〇o()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "normal_multi"

    .line 4
    .line 5
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/scanner/DocTypeRecommendControl;->normalCaptureRecommend()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    return v0
    .line 21
.end method

.method private synthetic o8o〇8([Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-static {p1}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->〇O8o08O(Landroid/content/Context;)Landroid/content/Intent;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-static {p1}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    :goto_0
    const/16 p2, 0x66

    .line 25
    .line 26
    invoke-virtual {p0, p1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o8〇0o〇(JLcom/intsig/callback/Callback0;)V
    .locals 3

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    cmp-long v2, p1, v0

    .line 4
    .line 5
    if-lez v2, :cond_0

    .line 6
    .line 7
    if-eqz p3, :cond_1

    .line 8
    .line 9
    invoke-interface {p3}, Lcom/intsig/callback/Callback0;->call()V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O08o〇()V

    .line 14
    .line 15
    .line 16
    :cond_1
    :goto_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o8〇8oooO〇()V
    .locals 3

    .line 1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 10
    .line 11
    .line 12
    const-class v1, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇O00()Landroidx/lifecycle/MutableLiveData;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 27
    .line 28
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/〇08O8o〇0;

    .line 29
    .line 30
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/〇08O8o〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o8〇O〇0O0〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/owlery/TheOwlery;->〇o00〇〇Oo()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o8〇〇〇0O(Lcom/intsig/owlery/TheOwlery;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$18;

    .line 6
    .line 7
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$18;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/owlery/TheOwlery;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO0o()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ipo/IPOCheck;->〇080:Lcom/intsig/camscanner/ipo/IPOCheck;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$14;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$14;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 8
    .line 9
    .line 10
    const-string v2, "ocr"

    .line 11
    .line 12
    const-string v3, "other"

    .line 13
    .line 14
    const/4 v4, 0x1

    .line 15
    invoke-static {v0, v1, v4, v2, v3}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/ipo/IPOCheckCallback;ZLjava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic oO8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00〇88〇08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oO800o()V
    .locals 3

    .line 1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 10
    .line 11
    .line 12
    const-class v1, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 21
    .line 22
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 23
    .line 24
    if-eqz v1, :cond_0

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O〇O〇oO()Ljava/util/List;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v1, v0}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇〇888(Landroid/content/Context;Ljava/util/List;)V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O〇O〇oO()Ljava/util/List;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8Oo8〇8()Z

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->Oo08(Landroid/content/Context;Ljava/util/List;Z)V

    .line 47
    .line 48
    .line 49
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 56
    .line 57
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/oO;

    .line 58
    .line 59
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/oO;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic oO80O0()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0OOoO(Z)V

    .line 3
    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00oooo()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic oO88〇0O8O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0Oo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO8O(Ljava/util/List;Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;JILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;",
            "JI",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/tsapp/request/RequestTaskData;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p3}, Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;->〇080()Lcom/intsig/camscanner/bankcardjournal/data/BankCardJournalResultData;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;

    .line 8
    .line 9
    invoke-direct {v0, p2, p3, p4, p5}, Lcom/intsig/camscanner/bankcardjournal/request/OCRPageBankCardJournalTaskData;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;J)V

    .line 10
    .line 11
    .line 12
    const/4 p2, 0x5

    .line 13
    if-gt p6, p2, :cond_0

    .line 14
    .line 15
    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-interface {p7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    :cond_1
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method static bridge synthetic oO8o〇08〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇0〇08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO8o〇o〇8()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "initMultiImageEditViewModel"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    const-string v1, "initMultiImageEditViewModel activity == null"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 25
    .line 26
    .line 27
    const-class v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O〇O〇oO()Landroidx/lifecycle/MutableLiveData;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 42
    .line 43
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/〇00;

    .line 44
    .line 45
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/〇00;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 49
    .line 50
    .line 51
    const-class v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 52
    .line 53
    invoke-static {v0}, Lcom/intsig/singleton/Singleton;->〇080(Ljava/lang/Class;)Lcom/intsig/singleton/Singleton;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 58
    .line 59
    if-eqz v0, :cond_1

    .line 60
    .line 61
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇oo〇:Landroidx/lifecycle/MutableLiveData;

    .line 62
    .line 63
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/O〇8O8〇008;

    .line 64
    .line 65
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/multiimageedit/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v1, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 69
    .line 70
    .line 71
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇O8〇〇o:Landroidx/lifecycle/MutableLiveData;

    .line 72
    .line 73
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/O8ooOoo〇;

    .line 74
    .line 75
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/O8ooOoo〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, p0, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 79
    .line 80
    .line 81
    :cond_1
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic oOO8oo0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/view/MyViewPager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oOOO0(I)V
    .locals 1

    .line 1
    const v0, 0x7f0a0800

    .line 2
    .line 3
    .line 4
    if-ne p1, v0, :cond_0

    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/model/TaskType;->SIGNATURE:Lcom/intsig/camscanner/multiimageedit/model/TaskType;

    .line 7
    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OOO0o〇(Lcom/intsig/camscanner/multiimageedit/model/TaskType;)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const v0, 0x7f0a07e7

    .line 13
    .line 14
    .line 15
    if-ne p1, v0, :cond_1

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO0o()V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const v0, 0x7f0a1a47

    .line 22
    .line 23
    .line 24
    if-ne p1, v0, :cond_2

    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇OO008O()V

    .line 27
    .line 28
    .line 29
    :cond_2
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic oOOo(J)V
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    const-wide/16 v1, 0x0

    .line 3
    .line 4
    cmp-long v3, p1, v1

    .line 5
    .line 6
    if-ltz v3, :cond_1

    .line 7
    .line 8
    new-instance v3, Landroid/content/Intent;

    .line 9
    .line 10
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 11
    .line 12
    .line 13
    iget-wide v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO〇00〇0O:J

    .line 14
    .line 15
    cmp-long v6, v4, v1

    .line 16
    .line 17
    if-lez v6, :cond_0

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-ne v1, v0, :cond_0

    .line 38
    .line 39
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 40
    .line 41
    iget-wide v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO〇00〇0O:J

    .line 42
    .line 43
    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    const-string v2, "pageuri"

    .line 48
    .line 49
    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 50
    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 53
    .line 54
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    const/4 v2, 0x0

    .line 59
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 64
    .line 65
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 66
    .line 67
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 68
    .line 69
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0〇O0088o(Ljava/lang/String;)Landroid/net/Uri;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 74
    .line 75
    .line 76
    :cond_0
    const-string v1, "extra_key_doc_id"

    .line 77
    .line 78
    invoke-virtual {v3, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 79
    .line 80
    .line 81
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 82
    .line 83
    const/4 p2, -0x1

    .line 84
    invoke-virtual {p1, p2, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 85
    .line 86
    .line 87
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 88
    .line 89
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 94
    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    .line 96
    .line 97
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .line 99
    .line 100
    const-string v3, "cannot save paper, docId = "

    .line 101
    .line 102
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 116
    .line 117
    const-string p2, "F-trySavePaperPagesIntoDb, discardAllData in UI Thread"

    .line 118
    .line 119
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 123
    .line 124
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8〇o(Z)V

    .line 125
    .line 126
    .line 127
    return-void
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private synthetic oOOo8〇o(Ljava/util/Map;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V
    .locals 2

    .line 1
    iget-object p4, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    iget-wide v0, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 4
    .line 5
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    check-cast p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 14
    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 18
    .line 19
    const-string p2, "pagePara == null"

    .line 20
    .line 21
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    return-void

    .line 25
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {p4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    invoke-virtual {p4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 44
    .line 45
    .line 46
    :cond_1
    iget v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 47
    .line 48
    iput v0, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 49
    .line 50
    iget-object p1, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 51
    .line 52
    iput-object p1, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 53
    .line 54
    const/4 v0, 0x1

    .line 55
    const/4 v1, 0x0

    .line 56
    if-eqz p1, :cond_2

    .line 57
    .line 58
    const/4 p1, 0x1

    .line 59
    goto :goto_0

    .line 60
    :cond_2
    const/4 p1, 0x0

    .line 61
    :goto_0
    iput-boolean p1, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 62
    .line 63
    sget p1, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o〇:I

    .line 64
    .line 65
    iput p1, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 66
    .line 67
    if-eqz p2, :cond_7

    .line 68
    .line 69
    iget-object p1, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 70
    .line 71
    if-nez p1, :cond_3

    .line 72
    .line 73
    goto :goto_2

    .line 74
    :cond_3
    iget-object p1, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 75
    .line 76
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    iget-object p1, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 80
    .line 81
    iget-wide p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 82
    .line 83
    iget-wide v0, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 84
    .line 85
    cmp-long p3, p1, v0

    .line 86
    .line 87
    if-nez p3, :cond_5

    .line 88
    .line 89
    iget-object p1, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 90
    .line 91
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 92
    .line 93
    .line 94
    if-nez p5, :cond_4

    .line 95
    .line 96
    sget-object p1, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;

    .line 97
    .line 98
    invoke-virtual {p1}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;->requireWaitingInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    iput-object p1, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0OoOOo0:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 103
    .line 104
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 105
    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 107
    .line 108
    .line 109
    move-result-wide p2

    .line 110
    invoke-virtual {p1, p4, p2, p3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 111
    .line 112
    .line 113
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 114
    .line 115
    .line 116
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 117
    .line 118
    const-string p2, "handleMultiAdjustResultIntent updateDataChange"

    .line 119
    .line 120
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    goto :goto_1

    .line 124
    :cond_5
    if-nez p5, :cond_6

    .line 125
    .line 126
    sget-object p1, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;

    .line 127
    .line 128
    invoke-virtual {p1}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;->requireWaitingInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    iput-object p1, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0OoOOo0:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 133
    .line 134
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 135
    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 137
    .line 138
    .line 139
    move-result-wide p2

    .line 140
    invoke-virtual {p1, p4, p2, p3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 141
    .line 142
    .line 143
    :cond_6
    :goto_1
    return-void

    .line 144
    :cond_7
    :goto_2
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 145
    .line 146
    new-instance p3, Ljava/lang/StringBuilder;

    .line 147
    .line 148
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    .line 150
    .line 151
    const-string p4, "handleMultiAdjustResultIntent traverseMultiImageEditPage currentMultiImageEditPage is null="

    .line 152
    .line 153
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    .line 155
    .line 156
    if-nez p2, :cond_8

    .line 157
    .line 158
    goto :goto_3

    .line 159
    :cond_8
    const/4 v0, 0x0

    .line 160
    :goto_3
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object p2

    .line 167
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    return-void
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private oOO〇0o8〇(ZLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;IJII)V
    .locals 20
    .param p2    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    if-eqz v1, :cond_b

    .line 6
    .line 7
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 8
    .line 9
    if-eqz v2, :cond_b

    .line 10
    .line 11
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 12
    .line 13
    if-nez v3, :cond_0

    .line 14
    .line 15
    goto/16 :goto_4

    .line 16
    .line 17
    :cond_0
    if-eqz p1, :cond_4

    .line 18
    .line 19
    sget-object v3, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 20
    .line 21
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o0O:Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/paper/PaperUtil;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 28
    .line 29
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o0O:Ljava/lang/String;

    .line 30
    .line 31
    invoke-static {v2}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v5

    .line 35
    sget-object v6, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 36
    .line 37
    new-instance v7, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v8, "F-handleAddOrModifyOnePaper - isChangeNotInsert so we delete "

    .line 43
    .line 44
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v7

    .line 54
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iget-object v7, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 58
    .line 59
    iget-object v7, v7, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 60
    .line 61
    invoke-static {v7}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 62
    .line 63
    .line 64
    move-result v7

    .line 65
    const-string v8, " -> "

    .line 66
    .line 67
    if-eqz v7, :cond_1

    .line 68
    .line 69
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/paper/PaperUtil;->〇〇888(Ljava/lang/String;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    new-instance v3, Ljava/lang/StringBuilder;

    .line 74
    .line 75
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .line 77
    .line 78
    const-string v7, "trimmed paper image"

    .line 79
    .line 80
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    iget-object v7, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 84
    .line 85
    iget-object v7, v7, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 86
    .line 87
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v3

    .line 100
    invoke-static {v6, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 104
    .line 105
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 106
    .line 107
    invoke-static {v3, v2}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 108
    .line 109
    .line 110
    :cond_1
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 111
    .line 112
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 113
    .line 114
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 115
    .line 116
    .line 117
    move-result v2

    .line 118
    if-eqz v2, :cond_2

    .line 119
    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    .line 121
    .line 122
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .line 124
    .line 125
    const-string v3, "big image, rename(has doodle)"

    .line 126
    .line 127
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 131
    .line 132
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 133
    .line 134
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    .line 142
    .line 143
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object v2

    .line 147
    invoke-static {v6, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 151
    .line 152
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 153
    .line 154
    invoke-static {v2, v5}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 155
    .line 156
    .line 157
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 158
    .line 159
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 160
    .line 161
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 162
    .line 163
    .line 164
    goto :goto_0

    .line 165
    :cond_2
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 166
    .line 167
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 168
    .line 169
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 170
    .line 171
    .line 172
    move-result v2

    .line 173
    if-eqz v2, :cond_3

    .line 174
    .line 175
    new-instance v2, Ljava/lang/StringBuilder;

    .line 176
    .line 177
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    .line 179
    .line 180
    const-string v3, "big image, rename(no doodle)"

    .line 181
    .line 182
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 186
    .line 187
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 188
    .line 189
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v2

    .line 202
    invoke-static {v6, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 206
    .line 207
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 208
    .line 209
    invoke-static {v2, v5}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 210
    .line 211
    .line 212
    :cond_3
    :goto_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 213
    .line 214
    .line 215
    move-result v2

    .line 216
    if-nez v2, :cond_9

    .line 217
    .line 218
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 219
    .line 220
    iput-object v4, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 221
    .line 222
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 223
    .line 224
    iput-object v4, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 225
    .line 226
    goto/16 :goto_2

    .line 227
    .line 228
    :cond_4
    iget-object v5, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 229
    .line 230
    iget-object v2, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 231
    .line 232
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 233
    .line 234
    .line 235
    move-result v2

    .line 236
    if-eqz v2, :cond_6

    .line 237
    .line 238
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 239
    .line 240
    iget-object v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 241
    .line 242
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 243
    .line 244
    invoke-static {v3, v2}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 245
    .line 246
    .line 247
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 248
    .line 249
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 250
    .line 251
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 252
    .line 253
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 254
    .line 255
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 256
    .line 257
    .line 258
    move-result v2

    .line 259
    if-eqz v2, :cond_5

    .line 260
    .line 261
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 262
    .line 263
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 264
    .line 265
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 266
    .line 267
    .line 268
    goto :goto_1

    .line 269
    :cond_5
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 270
    .line 271
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 272
    .line 273
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 274
    .line 275
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 276
    .line 277
    invoke-static {v2, v3}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 278
    .line 279
    .line 280
    :cond_6
    :goto_1
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 281
    .line 282
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 283
    .line 284
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 285
    .line 286
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 287
    .line 288
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 289
    .line 290
    .line 291
    move-result v2

    .line 292
    const-string v3, "->"

    .line 293
    .line 294
    if-nez v2, :cond_7

    .line 295
    .line 296
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 297
    .line 298
    new-instance v4, Ljava/lang/StringBuilder;

    .line 299
    .line 300
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 301
    .line 302
    .line 303
    const-string v6, "F-handleAddOrModifyOnePaper, new doc, tempSmallImagePath="

    .line 304
    .line 305
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    .line 307
    .line 308
    iget-object v6, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 309
    .line 310
    iget-object v6, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 311
    .line 312
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    .line 314
    .line 315
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    .line 317
    .line 318
    iget-object v6, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 319
    .line 320
    iget-object v6, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 321
    .line 322
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    .line 324
    .line 325
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 326
    .line 327
    .line 328
    move-result-object v4

    .line 329
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    .line 331
    .line 332
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 333
    .line 334
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 335
    .line 336
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 337
    .line 338
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 339
    .line 340
    invoke-static {v2, v4}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 341
    .line 342
    .line 343
    :cond_7
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 344
    .line 345
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 346
    .line 347
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 348
    .line 349
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 350
    .line 351
    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 352
    .line 353
    .line 354
    move-result v2

    .line 355
    if-nez v2, :cond_8

    .line 356
    .line 357
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 358
    .line 359
    new-instance v4, Ljava/lang/StringBuilder;

    .line 360
    .line 361
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 362
    .line 363
    .line 364
    const-string v6, "F-handleAddOrModifyOnePaper, new doc, bigRawImagePath="

    .line 365
    .line 366
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    .line 368
    .line 369
    iget-object v6, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 370
    .line 371
    iget-object v6, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 372
    .line 373
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    .line 375
    .line 376
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    .line 378
    .line 379
    iget-object v6, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 380
    .line 381
    iget-object v6, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 382
    .line 383
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    .line 385
    .line 386
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 387
    .line 388
    .line 389
    move-result-object v4

    .line 390
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    .line 392
    .line 393
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 394
    .line 395
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 396
    .line 397
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 398
    .line 399
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 400
    .line 401
    invoke-static {v2, v4}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 402
    .line 403
    .line 404
    :cond_8
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 405
    .line 406
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 407
    .line 408
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 409
    .line 410
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 411
    .line 412
    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 413
    .line 414
    .line 415
    move-result v2

    .line 416
    if-nez v2, :cond_9

    .line 417
    .line 418
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 419
    .line 420
    new-instance v4, Ljava/lang/StringBuilder;

    .line 421
    .line 422
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 423
    .line 424
    .line 425
    const-string v6, "F-handleAddOrModifyOnePaper, new doc, trimmedPaperPath="

    .line 426
    .line 427
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    .line 429
    .line 430
    iget-object v6, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 431
    .line 432
    iget-object v6, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 433
    .line 434
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 435
    .line 436
    .line 437
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    .line 439
    .line 440
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 441
    .line 442
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 443
    .line 444
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .line 446
    .line 447
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 448
    .line 449
    .line 450
    move-result-object v3

    .line 451
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    .line 453
    .line 454
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 455
    .line 456
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 457
    .line 458
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 459
    .line 460
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 461
    .line 462
    invoke-static {v2, v3}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 463
    .line 464
    .line 465
    :cond_9
    :goto_2
    sget-object v6, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 466
    .line 467
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 468
    .line 469
    iget-object v9, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 470
    .line 471
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 472
    .line 473
    iget v11, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 474
    .line 475
    iget-object v2, v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 476
    .line 477
    iget-boolean v12, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 478
    .line 479
    iget-object v13, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 480
    .line 481
    invoke-static {v5}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 482
    .line 483
    .line 484
    move-result v14

    .line 485
    if-eqz p1, :cond_a

    .line 486
    .line 487
    iget-wide v1, v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO〇00〇0O:J

    .line 488
    .line 489
    goto :goto_3

    .line 490
    :cond_a
    const-wide/16 v1, -0x1

    .line 491
    .line 492
    :goto_3
    move-wide v15, v1

    .line 493
    iget-boolean v1, v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 494
    .line 495
    xor-int/lit8 v17, v1, 0x1

    .line 496
    .line 497
    move-wide/from16 v7, p4

    .line 498
    .line 499
    move/from16 v10, p3

    .line 500
    .line 501
    move/from16 v18, p6

    .line 502
    .line 503
    move/from16 v19, p7

    .line 504
    .line 505
    invoke-virtual/range {v6 .. v19}, Lcom/intsig/camscanner/paper/PaperUtil;->oO80(JLjava/lang/String;IIZ[IZJZII)J

    .line 506
    .line 507
    .line 508
    return-void

    .line 509
    :cond_b
    :goto_4
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 510
    .line 511
    const-string v2, "F-handleAddOrModifyOnePaper error, sth is null!"

    .line 512
    .line 513
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    .line 515
    .line 516
    return-void
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private synthetic oOO〇OO8()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->init()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇OO:Z

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇oO08〇o0()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇80oo8()V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private oOO〇o〇0O(Lcom/intsig/camscanner/datastruct/FolderDocInfo;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0o0〇8o()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 7
    .line 8
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$17;

    .line 9
    .line 10
    invoke-direct {v2, p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$17;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/datastruct/FolderDocInfo;)V

    .line 11
    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O00〇8(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic oOo〇0o8〇8(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oo〇oO8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic oO〇O0O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO〇8O8oOo:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇0o8〇8(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static synthetic oo0O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OOOo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo88(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/util/List;IZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OoOO〇(Ljava/util/List;IZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private oo8O8o80()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic oo8〇〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0o88O(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic ooO888O0〇(Ljava/util/List;)V
    .locals 1

    .line 1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Ljava/lang/String;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->oO80(Ljava/lang/String;)Ljava/io/File;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic ooo008(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private ooo0〇080()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "initPermissionAndCreatorViewModel"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    .line 9
    .line 10
    invoke-direct {v1, p0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 11
    .line 12
    .line 13
    const-class v2, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8〇8〇O80:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 24
    .line 25
    if-nez v2, :cond_0

    .line 26
    .line 27
    const-string v1, "initPermissionAndCreatorViewModel parcelDocInfo == null"

    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    iget-wide v2, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 34
    .line 35
    const/4 v0, 0x0

    .line 36
    invoke-virtual {v1, v2, v3, v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->〇oo〇(JZ)V

    .line 37
    .line 38
    .line 39
    :goto_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇8O〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic oooO8〇00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8〇〇o8〇()Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic oooo800〇〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic ooooo0O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8oO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private ooo〇880()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "turnLeft START"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 11
    .line 12
    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    sget-object v2, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->INSTANCE:Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;

    .line 21
    .line 22
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->recordOneTimeForImage(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 23
    .line 24
    .line 25
    if-nez v1, :cond_0

    .line 26
    .line 27
    const-string v1, "turnLeft multiImageEditPage == null"

    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 34
    .line 35
    if-eqz v2, :cond_1

    .line 36
    .line 37
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-eqz v2, :cond_1

    .line 42
    .line 43
    const-string v1, "turnLeft actionClient return"

    .line 44
    .line 45
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    return-void

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 50
    .line 51
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 52
    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 54
    .line 55
    .line 56
    move-result-wide v3

    .line 57
    invoke-virtual {v0, v2, v3, v4}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 58
    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 61
    .line 62
    .line 63
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0O0o8:Z

    .line 64
    .line 65
    if-eqz v0, :cond_2

    .line 66
    .line 67
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/OOO8o〇〇;

    .line 68
    .line 69
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiimageedit/OOO8o〇〇;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 70
    .line 71
    .line 72
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 73
    .line 74
    .line 75
    :cond_2
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private oo〇0〇08()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O80〇〇o()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 13
    .line 14
    new-instance v2, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v3, "updateShowRawTrimmedPaperButtonAndUI - isPaperRawNow="

    .line 20
    .line 21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇80o:Landroid/widget/TextView;

    .line 35
    .line 36
    if-eqz v1, :cond_2

    .line 37
    .line 38
    if-eqz v0, :cond_1

    .line 39
    .line 40
    const v2, 0x7f130cab

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    const v2, 0x7f130ceb

    .line 45
    .line 46
    .line 47
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 48
    .line 49
    .line 50
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇08O:Lcom/intsig/view/ImageTextButton;

    .line 51
    .line 52
    if-eqz v1, :cond_4

    .line 53
    .line 54
    xor-int/lit8 v2, v0, 0x1

    .line 55
    .line 56
    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 57
    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇08O:Lcom/intsig/view/ImageTextButton;

    .line 60
    .line 61
    if-eqz v0, :cond_3

    .line 62
    .line 63
    const-string v0, "#AAAAAA"

    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_3
    const-string v0, "#FFFFFF"

    .line 67
    .line 68
    :goto_1
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    invoke-virtual {v1, v0}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 73
    .line 74
    .line 75
    :cond_4
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic oo〇88()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showSuperFilterGuide dismiss"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇O〇0O0〇()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private oo〇8〇8〇8(Ljava/lang/String;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/〇000O0;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/multiimageedit/〇000O0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oo〇O0o〇(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->getItemCount()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-gtz v0, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-ne p1, v0, :cond_1

    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    const/4 v0, 0x0

    .line 22
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->getItemCount()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-ge v0, v1, :cond_3

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 31
    .line 32
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->oo88o8O(I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    iget v1, v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 37
    .line 38
    if-ne v1, p1, :cond_2

    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 41
    .line 42
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->oo88o8O(I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇〇8〇8(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_3
    :goto_1
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private oo〇O〇o〇8(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 13
    .line 14
    invoke-virtual {v0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 15
    .line 16
    .line 17
    :cond_1
    :goto_0
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇08oO80o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo〇0o(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o〇0〇8〇0O()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const v1, 0x7f1319bf

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/O0o〇〇Oo;

    .line 16
    .line 17
    invoke-direct {v1}, Lcom/intsig/camscanner/multiimageedit/O0o〇〇Oo;-><init>()V

    .line 18
    .line 19
    .line 20
    const v2, 0x7f130019

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0〇0o8O(Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o〇O0ooo(Lcom/intsig/view/ImageTextButton;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showSuperFilterGuide show"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->Oooo8o0〇(Z)V

    .line 10
    .line 11
    .line 12
    new-instance v1, Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;-><init>(Landroid/content/Context;)V

    .line 17
    .line 18
    .line 19
    const v2, 0x7f1312aa

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;->Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    const/4 v2, 0x4

    .line 31
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/CsTips$Builder;->〇o00〇〇Oo(I)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/view/CsTips$Builder;->〇〇888(Z)Lcom/intsig/camscanner/view/CsTips$Builder;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/CsTips$Builder;->〇080()Lcom/intsig/camscanner/view/CsTips;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/CsTips;->〇80〇808〇O(Landroid/view/View;)Landroid/widget/PopupWindow;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/Oo〇O;

    .line 48
    .line 49
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/Oo〇O;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1, v0}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$19;

    .line 64
    .line 65
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$19;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/widget/PopupWindow;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private o〇O80o8OO(Lcom/intsig/callback/Callback0;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 10
    .line 11
    const-string v1, "checkAndDoIfHavePaperBalance but not paper!"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-interface {p1}, Lcom/intsig/callback/Callback0;->call()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void

    .line 20
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/〇〇〇0〇〇0;

    .line 21
    .line 22
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/multiimageedit/〇〇〇0〇〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/callback/Callback0;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic o〇O8OO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O〇o0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o〇Oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;[II)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {p2, v0}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    if-eqz p3, :cond_0

    .line 9
    .line 10
    invoke-static {v1, p3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getScanBoundF([I[I)[I

    .line 11
    .line 12
    .line 13
    move-result-object p3

    .line 14
    :cond_0
    if-eqz p1, :cond_3

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooo800〇〇()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 23
    .line 24
    iput-boolean v0, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8o:Z

    .line 25
    .line 26
    :cond_1
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 27
    .line 28
    iput p4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇00O0:I

    .line 29
    .line 30
    invoke-static {p2}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 31
    .line 32
    .line 33
    move-result p4

    .line 34
    iput p4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O〇08oOOO0:I

    .line 35
    .line 36
    iget-object p4, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 37
    .line 38
    iput-object p2, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 39
    .line 40
    iput-object p3, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 41
    .line 42
    if-nez p3, :cond_2

    .line 43
    .line 44
    iput-boolean v0, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇oO:Z

    .line 45
    .line 46
    :cond_2
    iput-boolean v0, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 47
    .line 48
    invoke-virtual {p4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇8O0〇8()V

    .line 49
    .line 50
    .line 51
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o00〇〇Oo()V

    .line 54
    .line 55
    .line 56
    :cond_3
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic o〇OoO0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇o08〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;JLcom/intsig/callback/Callback0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇0o〇(JLcom/intsig/callback/Callback0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static synthetic o〇o0o8Oo(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p0, "CSBatchResultDelete"

    .line 9
    .line 10
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇o0oOO8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/widget/EditText;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇O8OO:Landroid/widget/EditText;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o〇o0〇0O〇o(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .line 1
    const-string p1, "filter_apply_all"

    .line 2
    .line 3
    const-string v0, "CSBatchResult"

    .line 4
    .line 5
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "isChecked="

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    if-eqz p2, :cond_2

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO00〇0o〇〇()V

    .line 33
    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 36
    .line 37
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 38
    .line 39
    invoke-virtual {p2}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    if-nez p1, :cond_0

    .line 48
    .line 49
    return-void

    .line 50
    :cond_0
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 51
    .line 52
    iget p2, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 53
    .line 54
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o00〇〇Oo:I

    .line 55
    .line 56
    if-ne p2, v0, :cond_1

    .line 57
    .line 58
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 59
    .line 60
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 61
    .line 62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 63
    .line 64
    .line 65
    move-result-wide v0

    .line 66
    invoke-virtual {p2, p1, v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->oO8o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 67
    .line 68
    .line 69
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_1
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 74
    .line 75
    iget p2, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 76
    .line 77
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o〇:I

    .line 78
    .line 79
    if-ne p2, v0, :cond_3

    .line 80
    .line 81
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 82
    .line 83
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 84
    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 86
    .line 87
    .line 88
    move-result-wide v0

    .line 89
    invoke-virtual {p2, p1, v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 90
    .line 91
    .line 92
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 93
    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_2
    const-string p1, "cancel_filter_apply_all"

    .line 97
    .line 98
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    :cond_3
    :goto_0
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic o〇o8〇〇O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇OO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇oO08〇o0(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 13
    .line 14
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_2

    .line 31
    .line 32
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    check-cast v1, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;

    .line 37
    .line 38
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 39
    .line 40
    invoke-virtual {v1}, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;->〇o00〇〇Oo()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O〇8oOo8O(Ljava/lang/String;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    if-eqz v1, :cond_1

    .line 49
    .line 50
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 51
    .line 52
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    invoke-virtual {v2}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->〇80〇808〇O()V

    .line 57
    .line 58
    .line 59
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O8o08O8O:Ljava/lang/String;

    .line 60
    .line 61
    iput-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 62
    .line 63
    const/4 v2, 0x0

    .line 64
    iput-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O8o08O8O:Ljava/lang/String;

    .line 65
    .line 66
    const/4 v2, 0x0

    .line 67
    iput-boolean v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 68
    .line 69
    sget v2, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o00〇〇Oo:I

    .line 70
    .line 71
    iput v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 72
    .line 73
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 74
    .line 75
    iget-object v3, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 76
    .line 77
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 78
    .line 79
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    move-result v2

    .line 83
    if-eqz v2, :cond_1

    .line 84
    .line 85
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 86
    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 88
    .line 89
    .line 90
    move-result-wide v3

    .line 91
    invoke-virtual {v2, v1, v3, v4}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->oO8o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 92
    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇O〇0oo0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 99
    .line 100
    .line 101
    :cond_3
    :goto_1
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic o〇oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8880(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private o〇〇8〇〇(I)Landroid/view/animation/Animation;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-static {v0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/16 v0, 0x12c

    .line 8
    .line 9
    int-to-long v0, v0

    .line 10
    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 11
    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇00O(II)V
    .locals 4

    .line 1
    const-string v0, "CamScanner_Intellect_Erase_Safe"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/https/account/UserPropertyAPI;->〇〇8O0〇8(Ljava/lang/String;)Lcom/intsig/camscanner/https/entity/CSQueryProperty;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget v1, v0, Lcom/intsig/camscanner/https/entity/CSQueryProperty;->errorCode:I

    .line 10
    .line 11
    const/16 v2, 0xc8

    .line 12
    .line 13
    if-ne v1, v2, :cond_0

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/https/entity/CSQueryProperty;->data:Lcom/intsig/tianshu/purchase/BalanceInfo;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    iget v0, v0, Lcom/intsig/tianshu/purchase/BalanceInfo;->CamScanner_Intellect_Erase_Safe:I

    .line 20
    .line 21
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 22
    .line 23
    new-instance v2, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v3, "vip enhance count = "

    .line 29
    .line 30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/o〇O8〇〇o;

    .line 44
    .line 45
    invoke-direct {v1, p0, v0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;III)V

    .line 46
    .line 47
    .line 48
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇O〇o〇8(Ljava/lang/Runnable;)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 53
    .line 54
    const-string p2, "data error occur"

    .line 55
    .line 56
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 60
    .line 61
    const p2, 0x7f1312a4

    .line 62
    .line 63
    .line 64
    invoke-static {p1, p2}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 65
    .line 66
    .line 67
    :goto_0
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic 〇00O00o(Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;)V
    .locals 12

    .line 1
    if-eqz p1, :cond_7

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;->〇o00〇〇Oo()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 13
    .line 14
    .line 15
    move-result-wide v2

    .line 16
    invoke-static {}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->〇O00()J

    .line 17
    .line 18
    .line 19
    move-result-wide v4

    .line 20
    sub-long/2addr v2, v4

    .line 21
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v2, ""

    .line 25
    .line 26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v9

    .line 33
    const-string v1, "use_time"

    .line 34
    .line 35
    const-string v3, "cs_batch_enhance_snackbar"

    .line 36
    .line 37
    const-string v4, "from_part"

    .line 38
    .line 39
    const/4 v10, -0x1

    .line 40
    const/4 v11, 0x2

    .line 41
    if-ne v11, v0, :cond_0

    .line 42
    .line 43
    const-string v2, "CSProcessSuccess"

    .line 44
    .line 45
    invoke-static {v2, v4, v3, v1, v9}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    if-ne v10, v0, :cond_1

    .line 50
    .line 51
    const-string v2, "CSProcessRestore"

    .line 52
    .line 53
    invoke-static {v2, v4, v3, v1, v9}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    const-string v3, "CSProcessFail"

    .line 58
    .line 59
    const-string v4, "from_part"

    .line 60
    .line 61
    const-string v5, "cs_batch_enhance_snackbar"

    .line 62
    .line 63
    const-string v6, "error_code"

    .line 64
    .line 65
    new-instance v1, Ljava/lang/StringBuilder;

    .line 66
    .line 67
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v7

    .line 80
    const-string v8, "use_time"

    .line 81
    .line 82
    invoke-static/range {v3 .. v9}, Lcom/intsig/camscanner/log/LogAgentData;->〇O〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    :goto_0
    const/4 v1, -0x6

    .line 86
    if-eq v0, v1, :cond_6

    .line 87
    .line 88
    const/4 v1, -0x5

    .line 89
    if-eq v0, v1, :cond_5

    .line 90
    .line 91
    const/4 v1, -0x3

    .line 92
    if-eq v0, v1, :cond_5

    .line 93
    .line 94
    const/4 v1, -0x2

    .line 95
    if-eq v0, v1, :cond_4

    .line 96
    .line 97
    if-eq v0, v10, :cond_3

    .line 98
    .line 99
    if-eq v0, v11, :cond_2

    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;->〇o〇()Ljava/util/ArrayList;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o〇0O(Ljava/util/ArrayList;)V

    .line 107
    .line 108
    .line 109
    goto :goto_1

    .line 110
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;->〇o〇()Ljava/util/ArrayList;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO08〇o0(Ljava/util/ArrayList;)V

    .line 115
    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_4
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 119
    .line 120
    const v0, 0x7f130cb5

    .line 121
    .line 122
    .line 123
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 124
    .line 125
    .line 126
    goto :goto_1

    .line 127
    :cond_5
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 128
    .line 129
    const v0, 0x7f130ae1

    .line 130
    .line 131
    .line 132
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 133
    .line 134
    .line 135
    goto :goto_1

    .line 136
    :cond_6
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 137
    .line 138
    const v0, 0x7f131f98

    .line 139
    .line 140
    .line 141
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    const v1, 0x7f130b00

    .line 146
    .line 147
    .line 148
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/app/DialogUtils;->o〇〇0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    :cond_7
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0〇()V

    .line 156
    .line 157
    .line 158
    return-void
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇00o80oo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇08oOOO0:Z

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇00o〇O8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇00:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇00〇〇〇o〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇0880O0〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "reTakePicture"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance p1, Lcom/intsig/camscanner/multiimageedit/o〇8oOO88;

    .line 9
    .line 10
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/multiimageedit/o〇8oOO88;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇O80o8OO(Lcom/intsig/callback/Callback0;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0888(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00oooo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇088O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OoO888(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇08O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0o0o8(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇0O0Oo〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "handForPadFail cancel"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0O8Oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇00:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇0OOoO8O0()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0oO〇oo00:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇0o()Landroid/view/animation/Animation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8〇8000:Landroid/view/animation/Animation;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const v0, 0x7f010043

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇〇8〇〇(I)Landroid/view/animation/Animation;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8〇8000:Landroid/view/animation/Animation;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8〇8000:Landroid/view/animation/Animation;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇OO:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0o88O(I)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "checkChangeTab pos: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :try_start_0
    sget-object v0, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇〇888:Ljava/util/HashMap;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    :goto_0
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-ge v1, v2, :cond_1

    .line 31
    .line 32
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    if-eqz v2, :cond_0

    .line 41
    .line 42
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    check-cast v2, Ljava/lang/Integer;

    .line 51
    .line 52
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-ne v2, p1, :cond_0

    .line 57
    .line 58
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 59
    .line 60
    if-eqz v2, :cond_0

    .line 61
    .line 62
    invoke-virtual {v2}, Lcom/google/android/material/tabs/TabLayout;->getTabCount()I

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    if-ne v2, v3, :cond_0

    .line 71
    .line 72
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 73
    .line 74
    invoke-virtual {v2, v1}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 75
    .line 76
    .line 77
    move-result-object v3

    .line 78
    invoke-virtual {v2, v3}, Lcom/google/android/material/tabs/TabLayout;->selectTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .line 80
    .line 81
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :catch_0
    move-exception p1

    .line 85
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 86
    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    .line 88
    .line 89
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    .line 91
    .line 92
    const-string v2, "checkChangeTab error:"

    .line 93
    .line 94
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    :cond_1
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0oO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;[II)V
    .locals 9

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v2

    .line 7
    const/4 v8, 0x1

    .line 8
    invoke-static {p2, v8}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    if-eqz p3, :cond_0

    .line 15
    .line 16
    invoke-static {v0, v0, p3, p4}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p3

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 p3, 0x0

    .line 22
    :goto_0
    move-object v6, p3

    .line 23
    sget-object p3, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 24
    .line 25
    invoke-virtual {p3, v2}, Lcom/intsig/camscanner/paper/PaperUtil;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    sget-object p3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 30
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v1, "F-handleRetakePaper, rawImagePath="

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string v1, "; "

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-static {p3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-static {p2, v3}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 57
    .line 58
    .line 59
    move-result p2

    .line 60
    if-eqz p2, :cond_1

    .line 61
    .line 62
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 63
    .line 64
    iget-wide v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 65
    .line 66
    const/4 v5, 0x1

    .line 67
    const/4 v7, 0x1

    .line 68
    move v4, p4

    .line 69
    invoke-static/range {v0 .. v7}, Lcom/intsig/camscanner/multiimageedit/util/MultiImageEditPageManagerUtil;->o〇0(JLjava/lang/String;Ljava/lang/String;IZLjava/lang/String;Z)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    iput p4, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇00O0:I

    .line 74
    .line 75
    iget-object p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 76
    .line 77
    invoke-virtual {p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o00〇〇Oo()V

    .line 78
    .line 79
    .line 80
    iput-object p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 81
    .line 82
    iput-boolean v8, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇08〇:Z

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_1
    const-string p1, "F-handleRetakePaper, copy failed!"

    .line 86
    .line 87
    invoke-static {p3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    goto :goto_1

    .line 91
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 92
    .line 93
    const-string p2, "F-handleRetakePaper but get null currentPage"

    .line 94
    .line 95
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇0〇08()V

    .line 99
    .line 100
    .line 101
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o80Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇0ooOOo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;III)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo088O〇8〇(III)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static synthetic 〇0o〇o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇0〇o:Lcom/intsig/camscanner/demoire/ImageQualityLoadingAnim;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/demoire/ImageQualityLoadingAnim;->OO0o〇〇(Lcom/intsig/camscanner/demoire/ImageQualityLoadingAnim$OnAnimationEndListener;)V

    .line 7
    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇0〇0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O0OO8O(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇0〇8o〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 10

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "doDeleteItem: START!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    const-string p1, "doDeleteItem: multiImageEditPage is NULL!"

    .line 11
    .line 12
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 17
    .line 18
    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    const/4 v2, 0x1

    .line 29
    sub-int/2addr v1, v2

    .line 30
    if-ne v0, v1, :cond_1

    .line 31
    .line 32
    add-int/lit8 v0, v0, -0x1

    .line 33
    .line 34
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 37
    .line 38
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object v4

    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 43
    .line 44
    iget-wide v5, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    xor-int/lit8 v8, v1, 0x1

    .line 51
    .line 52
    iget-boolean v9, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 53
    .line 54
    move-object v7, p1

    .line 55
    invoke-virtual/range {v3 .. v9}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->ooo8o〇o〇(Landroid/content/Context;JLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;ZZ)V

    .line 56
    .line 57
    .line 58
    const/4 p1, -0x1

    .line 59
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o0〇8:I

    .line 60
    .line 61
    if-ltz v0, :cond_2

    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o8oO〇(Ljava/util/List;)V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 75
    .line 76
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 77
    .line 78
    invoke-virtual {p1, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 79
    .line 80
    .line 81
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OOo0Oo8O(IZ)V

    .line 82
    .line 83
    .line 84
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇0〇o8〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O〇〇00(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇0〇〇o0(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO8〇0O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇80O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O008o8oo(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇80O80O〇0()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇Oo〇O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "showSuperFilterGuide gray close"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8〇8〇O80()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 20
    .line 21
    const-string v1, "showSuperFilterGuide shown before"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 28
    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇oO〇()Lcom/intsig/owlery/DialogOwl;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/owlery/TheOwlery;->〇O00(Lcom/intsig/owlery/BaseOwl;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 39
    .line 40
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇〇〇0O(Lcom/intsig/owlery/TheOwlery;)V

    .line 41
    .line 42
    .line 43
    :cond_2
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇80oo8()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;->setNeedToastForMultiImage(Z)V

    .line 5
    .line 6
    .line 7
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Ooo08:Z

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 v0, 0x0

    .line 33
    :goto_0
    if-lez v0, :cond_1

    .line 34
    .line 35
    sget-object v1, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 38
    .line 39
    iget-wide v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 40
    .line 41
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->o〇O8〇〇o(ILjava/lang/Long;)V

    .line 46
    .line 47
    .line 48
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇OO:Z

    .line 49
    .line 50
    const/4 v1, 0x0

    .line 51
    if-eqz v0, :cond_2

    .line 52
    .line 53
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇o〇0O(Lcom/intsig/camscanner/datastruct/FolderDocInfo;)V

    .line 54
    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_2
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇08oOOO0:Z

    .line 58
    .line 59
    if-eqz v0, :cond_3

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 62
    .line 63
    iget-boolean v2, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 64
    .line 65
    if-eqz v2, :cond_3

    .line 66
    .line 67
    iget-object v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 68
    .line 69
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-eqz v0, :cond_3

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 76
    .line 77
    invoke-static {v0, v1, v1}, Lcom/intsig/camscanner/db/dao/DirDao;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    if-lez v0, :cond_3

    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇O〇88O8O()V

    .line 84
    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_3
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 88
    .line 89
    if-eqz v0, :cond_4

    .line 90
    .line 91
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 92
    .line 93
    const-string v1, "trySaveResult mIsOCRMulti"

    .line 94
    .line 95
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO0o()V

    .line 99
    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_4
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0O0o8:Z

    .line 103
    .line 104
    if-eqz v0, :cond_5

    .line 105
    .line 106
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OOo08()V

    .line 107
    .line 108
    .line 109
    goto :goto_1

    .line 110
    :cond_5
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇o〇0O(Lcom/intsig/camscanner/datastruct/FolderDocInfo;)V

    .line 111
    .line 112
    .line 113
    :goto_1
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static synthetic 〇80oo〇0〇o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "F-showNoNetworkDialog click no network dialog cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇80〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O80〇0o:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇88(Landroid/content/Intent;)V
    .locals 4

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 4
    .line 5
    const-string v0, "handleMultiAdjustResultIntent, intent == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    const-string v0, "extra_multi_capture_status"

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    instance-of v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;

    .line 18
    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 22
    .line 23
    const-string v0, "parcelable is not MultiCaptureResultStatus"

    .line 24
    .line 25
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;->〇80〇808〇O()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_2

    .line 36
    .line 37
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 38
    .line 39
    const-string v0, "handleMultiAdjustResultIntent, !isReturnChange"

    .line 40
    .line 41
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;->o〇0()Ljava/util/List;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-eqz v0, :cond_3

    .line 54
    .line 55
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 56
    .line 57
    const-string v0, "handleMultiAdjustResultIntent, imageChangeList.isEmpty()"

    .line 58
    .line 59
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    return-void

    .line 63
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 64
    .line 65
    if-nez v0, :cond_4

    .line 66
    .line 67
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 68
    .line 69
    const-string v0, "handleMultiAdjustResultIntent imageViewPager == null"

    .line 70
    .line 71
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    return-void

    .line 75
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 76
    .line 77
    if-nez v0, :cond_5

    .line 78
    .line 79
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 80
    .line 81
    const-string v1, "handleMultiAdjustResultIntent multiImageEditViewModel == null"

    .line 82
    .line 83
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO8o〇o〇8()V

    .line 87
    .line 88
    .line 89
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 90
    .line 91
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    if-eqz v0, :cond_9

    .line 96
    .line 97
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 98
    .line 99
    .line 100
    move-result v1

    .line 101
    if-nez v1, :cond_6

    .line 102
    .line 103
    goto :goto_1

    .line 104
    :cond_6
    new-instance v0, Ljava/util/HashMap;

    .line 105
    .line 106
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 107
    .line 108
    .line 109
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    if-eqz v1, :cond_7

    .line 118
    .line 119
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    check-cast v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 124
    .line 125
    iget-wide v2, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 126
    .line 127
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 128
    .line 129
    .line 130
    move-result-object v2

    .line 131
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    goto :goto_0

    .line 135
    :cond_7
    new-instance p1, Ljava/util/ArrayList;

    .line 136
    .line 137
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .line 139
    .line 140
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 141
    .line 142
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 143
    .line 144
    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 145
    .line 146
    .line 147
    move-result v2

    .line 148
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 153
    .line 154
    new-instance v3, Lcom/intsig/camscanner/multiimageedit/〇0O〇Oo;

    .line 155
    .line 156
    invoke-direct {v3, p0, v0, v1, p1}, Lcom/intsig/camscanner/multiimageedit/〇0O〇Oo;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/util/Map;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/util/List;)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o08oOO(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel$MultiImageEditModelTraverse;)V

    .line 160
    .line 161
    .line 162
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0O0o8:Z

    .line 163
    .line 164
    if-eqz v0, :cond_8

    .line 165
    .line 166
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 167
    .line 168
    .line 169
    move-result v0

    .line 170
    if-lez v0, :cond_8

    .line 171
    .line 172
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/〇00O0O0;

    .line 173
    .line 174
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/multiimageedit/〇00O0O0;-><init>(Ljava/util/List;)V

    .line 175
    .line 176
    .line 177
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 178
    .line 179
    .line 180
    :cond_8
    return-void

    .line 181
    :cond_9
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 182
    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    .line 184
    .line 185
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .line 187
    .line 188
    const-string v2, "handleMultiAdjustResultIntent, multiImageEditPageList is null = "

    .line 189
    .line 190
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    if-nez v0, :cond_a

    .line 194
    .line 195
    const/4 v0, 0x1

    .line 196
    goto :goto_2

    .line 197
    :cond_a
    const/4 v0, 0x0

    .line 198
    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v0

    .line 205
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .line 207
    .line 208
    return-void
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private synthetic 〇8880(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V
    .locals 2

    .line 1
    const-wide/16 v0, -0x1

    .line 2
    .line 3
    iput-wide v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->o〇0:J

    .line 4
    .line 5
    iget-object p2, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 6
    .line 7
    iget p3, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 8
    .line 9
    if-ne p1, p3, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇0888o(I)Z

    .line 13
    .line 14
    .line 15
    move-result p3

    .line 16
    if-nez p3, :cond_1

    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    iput p1, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 20
    .line 21
    const/4 p1, 0x0

    .line 22
    iput-boolean p1, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 23
    .line 24
    iget p1, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 25
    .line 26
    sget p3, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇080:I

    .line 27
    .line 28
    if-ne p1, p3, :cond_2

    .line 29
    .line 30
    sget p1, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o00〇〇Oo:I

    .line 31
    .line 32
    iput p1, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 33
    .line 34
    :cond_2
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇8O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇o0O0OO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8O0880(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇0〇〇8O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8Oo8〇8()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "normal_multi"

    .line 4
    .line 5
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/scanner/DocTypeRecommendControl;->isOpenShotFilter()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    return v0
    .line 21
.end method

.method private synthetic 〇8OooO0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0ooOOo:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇8〇0〇o〇O;

    .line 19
    .line 20
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/〇8〇0〇o〇O;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 24
    .line 25
    .line 26
    :cond_1
    :goto_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇8o088〇0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo0〇〇O:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇8o0o0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o〇O0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8o80O(Z)V
    .locals 5

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    fill-array-data v1, :array_0

    .line 5
    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    :goto_0
    if-ge v2, v0, :cond_2

    .line 9
    .line 10
    aget v3, v1, v2

    .line 11
    .line 12
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 13
    .line 14
    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    instance-of v4, v3, Lcom/intsig/view/ImageTextButton;

    .line 19
    .line 20
    if-eqz v4, :cond_1

    .line 21
    .line 22
    check-cast v3, Lcom/intsig/view/ImageTextButton;

    .line 23
    .line 24
    invoke-virtual {v3, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 25
    .line 26
    .line 27
    if-eqz p1, :cond_0

    .line 28
    .line 29
    const/high16 v4, 0x3f800000    # 1.0f

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_0
    const v4, 0x3e99999a    # 0.3f

    .line 33
    .line 34
    .line 35
    :goto_1
    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 36
    .line 37
    .line 38
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    return-void

    .line 42
    nop

    .line 43
    :array_0
    .array-data 4
        0x7f0a07f5
        0x7f0a0751
        0x7f0a0754
        0x7f0a07d2
        0x7f0a07ca
        0x7f0a0810
    .end array-data
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic 〇8o8o(Lcom/intsig/camscanner/purchase/entity/Function;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p3, "vip enhance limit purchase vip"

    .line 4
    .line 5
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    new-instance p3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 11
    .line 12
    invoke-direct {p3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p3, p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    sget-object p3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_BATCH_RESULT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 20
    .line 21
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    sget-object p3, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPage:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 26
    .line 27
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    sget-object p3, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 32
    .line 33
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-static {p2, p1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇8oo0oO0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8oo8888(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇8ooOO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;IZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OOo0Oo8O(IZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇8ooo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇O〇o〇8(Ljava/lang/Runnable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇8oo〇〇oO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇〇8〇8(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇8〇()V
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "dealImg saveResult()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 17
    .line 18
    iget-wide v4, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 19
    .line 20
    iget-boolean v6, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 21
    .line 22
    const/4 v7, 0x0

    .line 23
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O0〇oo(Landroid/content/Context;JZZ)I

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇8〇0O〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8o088〇0()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8〇80o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;JJLjava/lang/Long;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0oOo(JJLjava/lang/Long;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇8〇8o00()Ljava/lang/Boolean;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oooO888:Ljava/lang/Boolean;

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oooO888:Ljava/lang/Boolean;

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 18
    .line 19
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$6;

    .line 20
    .line 21
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$6;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->oO8008O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)Ljava/lang/Boolean;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    return-object v0

    .line 29
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 30
    .line 31
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/view/ImageTextButton;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇O0ooo(Lcom/intsig/view/ImageTextButton;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇8〇o〇OoO8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇〇8o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO00〇o:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8〇〇8〇8(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V
    .locals 3

    .line 1
    iget v0, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x7

    .line 12
    if-eq v0, p1, :cond_4

    .line 13
    .line 14
    iget-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 15
    .line 16
    if-nez p1, :cond_4

    .line 17
    .line 18
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇0888o(I)Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    if-nez p1, :cond_4

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O0oo008o()V

    .line 25
    .line 26
    .line 27
    goto/16 :goto_1

    .line 28
    .line 29
    :cond_0
    iget v0, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 30
    .line 31
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇0888o(I)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 38
    .line 39
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_1

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08〇()V

    .line 46
    .line 47
    .line 48
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 49
    .line 50
    const-string v0, "changFilter but no network!"

    .line 51
    .line 52
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    return-void

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 57
    .line 58
    iget v1, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o〇〇0〇(I)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 64
    .line 65
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 66
    .line 67
    .line 68
    new-instance v0, Lorg/json/JSONObject;

    .line 69
    .line 70
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 71
    .line 72
    .line 73
    :try_start_0
    const-string v1, "scheme"

    .line 74
    .line 75
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 76
    .line 77
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    invoke-static {v2}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇o00〇〇Oo(I)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :catch_0
    move-exception v1

    .line 90
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 91
    .line 92
    .line 93
    :goto_0
    const-string v1, "CSBatchResult"

    .line 94
    .line 95
    const-string v2, "filter_switch_filter"

    .line 96
    .line 97
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 98
    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 101
    .line 102
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 103
    .line 104
    .line 105
    move-result v0

    .line 106
    if-eqz v0, :cond_2

    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO00〇0o〇〇()V

    .line 109
    .line 110
    .line 111
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 112
    .line 113
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 114
    .line 115
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 116
    .line 117
    .line 118
    move-result v1

    .line 119
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    if-nez v0, :cond_3

    .line 124
    .line 125
    return-void

    .line 126
    :cond_3
    const-wide/16 v1, -0x1

    .line 127
    .line 128
    iput-wide v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->o〇0:J

    .line 129
    .line 130
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 131
    .line 132
    iget p1, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 133
    .line 134
    invoke-static {p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceMode(I)I

    .line 135
    .line 136
    .line 137
    move-result p1

    .line 138
    iput p1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 139
    .line 140
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 141
    .line 142
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 143
    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 145
    .line 146
    .line 147
    move-result-wide v1

    .line 148
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->oO8o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 149
    .line 150
    .line 151
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 152
    .line 153
    .line 154
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 155
    .line 156
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 157
    .line 158
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->OoO8()I

    .line 159
    .line 160
    .line 161
    move-result v0

    .line 162
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 163
    .line 164
    .line 165
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 166
    .line 167
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 168
    .line 169
    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 170
    .line 171
    .line 172
    move-result v0

    .line 173
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 174
    .line 175
    .line 176
    move-result-object p1

    .line 177
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇O〇0oo0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 178
    .line 179
    .line 180
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8ooO8o()V

    .line 181
    .line 182
    .line 183
    return-void
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇8〇〇〇O0(Ljava/lang/String;I)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 12
    .line 13
    iput-object p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 16
    .line 17
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 18
    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/〇〇o8;

    .line 25
    .line 26
    invoke-direct {v0, p0, p2}, Lcom/intsig/camscanner/multiimageedit/〇〇o8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
.end method

.method private 〇O08o〇()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "reallyDelete, observe "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8oOo0:Z

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8oOo0:Z

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 18
    .line 19
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    const v1, 0x7f131d10

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const v1, 0x7f1319cb

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/OO8oO0o〇;

    .line 37
    .line 38
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/OO8oO0o〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 39
    .line 40
    .line 41
    const v2, 0x7f13057e

    .line 42
    .line 43
    .line 44
    const v3, 0x7f060226

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v2, v3, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/o0O0;

    .line 52
    .line 53
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/o0O0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 54
    .line 55
    .line 56
    const v2, 0x7f131a0b

    .line 57
    .line 58
    .line 59
    const v3, 0x7f060123

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v2, v3, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇oOO8O8(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇0;

    .line 67
    .line 68
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->O〇8O8〇008(Landroid/content/DialogInterface$OnDismissListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 80
    .line 81
    .line 82
    const-string v0, "type"

    .line 83
    .line 84
    const-string v1, "already_taken"

    .line 85
    .line 86
    const-string v2, "CSTestLimitPop"

    .line 87
    .line 88
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    :cond_0
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static synthetic 〇O0OO8O(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "F-showServerErrorDialog click no network dialog cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O0oo008o()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo0〇〇O:Landroid/view/View;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo80:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    const/16 v2, 0x30

    .line 18
    .line 19
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    add-int/2addr v0, v1

    .line 24
    :cond_0
    if-lez v0, :cond_1

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 27
    .line 28
    invoke-virtual {v1, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 29
    .line 30
    .line 31
    :cond_1
    const-string v0, "CSBatchResult"

    .line 32
    .line 33
    const-string v1, "modify"

    .line 34
    .line 35
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 47
    .line 48
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 49
    .line 50
    mul-int v1, v1, v0

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 53
    .line 54
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8ooOoo〇(I)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 60
    .line 61
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    const/16 v1, 0x32

    .line 70
    .line 71
    const/16 v2, 0x64

    .line 72
    .line 73
    if-nez v0, :cond_2

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 76
    .line 77
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇8o8o〇(II)V

    .line 78
    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 81
    .line 82
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO0o〇〇〇〇0(II)V

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 86
    .line 87
    invoke-virtual {v0, v2, v2}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇O8o08O(II)V

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_2
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 92
    .line 93
    iget-object v4, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 94
    .line 95
    iget v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇08O:I

    .line 96
    .line 97
    add-int/2addr v4, v1

    .line 98
    invoke-virtual {v3, v4, v2}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇8o8o〇(II)V

    .line 99
    .line 100
    .line 101
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 102
    .line 103
    iget-object v4, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 104
    .line 105
    iget v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O0O:I

    .line 106
    .line 107
    add-int/2addr v4, v1

    .line 108
    invoke-virtual {v3, v4, v2}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO0o〇〇〇〇0(II)V

    .line 109
    .line 110
    .line 111
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 112
    .line 113
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 114
    .line 115
    iget v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8oOOo:I

    .line 116
    .line 117
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇O8o08O(II)V

    .line 118
    .line 119
    .line 120
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇〇:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 121
    .line 122
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇80〇808〇O()V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O88O:Landroid/view/View;

    .line 126
    .line 127
    const/4 v1, 0x0

    .line 128
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O88O:Landroid/view/View;

    .line 132
    .line 133
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇〇O()Landroid/view/animation/Animation;

    .line 134
    .line 135
    .line 136
    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 138
    .line 139
    .line 140
    return-void
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇O0o〇〇o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/util/Map;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOOo8〇o(Ljava/util/Map;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private synthetic 〇O80O(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "CSTestLimitPop"

    .line 2
    .line 3
    const-string p2, "cancel"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8oOo0:Z

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8〇8000(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o808Oo(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8〇8O0oO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8O〇8〇〇8〇(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇O8〇〇o8〇()Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "getBatchScanDocViewModel --> batchScanDocViewModel == null "

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 21
    .line 22
    .line 23
    const-class v1, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 32
    .line 33
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 34
    .line 35
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇OO0oO()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->〇0000OOO()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇080:Lcom/intsig/camscanner/demoire/DeMoireManager;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o〇88〇8:Lcom/intsig/camscanner/demoire/DeMoireManager$OnDeMoireFinishListener;

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 13
    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/demoire/DeMoireManager;->O8〇o(Lcom/intsig/camscanner/demoire/DeMoireManager$OnDeMoireFinishListener;Landroid/app/Activity;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 20
    .line 21
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇O〇0oo0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇OO8〇0O8()V
    .locals 2

    .line 1
    const-string v0, "CSTestPaper"

    .line 2
    .line 3
    const-string v1, "view_orig"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 11
    .line 12
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 23
    .line 24
    const-string v1, "showRawTrimmedPaper multiImageEditPage == null"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 31
    .line 32
    iget-boolean v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->Ooo08:Z

    .line 33
    .line 34
    xor-int/lit8 v1, v1, 0x1

    .line 35
    .line 36
    iput-boolean v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->Ooo08:Z

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇0〇08()V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇OOo08()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$12;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$12;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 6
    .line 7
    .line 8
    const-string v2, "bankcard_journal"

    .line 9
    .line 10
    const-string v3, "other"

    .line 11
    .line 12
    const/4 v4, 0x1

    .line 13
    invoke-static {v0, v1, v4, v2, v3}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/ipo/IPOCheckCallback;ZLjava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇OOo0Oo8O(IZ)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v1, 0x0

    .line 7
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00〇88〇08:Z

    .line 8
    .line 9
    invoke-virtual {v0, p1, p2}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇OoO0o0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO80O0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇OoOO〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "book_mode"

    .line 4
    .line 5
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/capture/capturemode/CaptureModePreferenceHelper;->Oooo8o0〇()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    return v0
    .line 21
.end method

.method private static synthetic 〇OoOo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 4
    .line 5
    invoke-static {p0}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->oO80(Ljava/lang/String;)Ljava/io/File;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇Oooo088〇()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 17
    .line 18
    if-eqz v1, :cond_3

    .line 19
    .line 20
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 21
    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    iget v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 26
    .line 27
    invoke-static {v2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceIndex(I)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o〇〇0〇(I)V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 35
    .line 36
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8ooO8o()V

    .line 40
    .line 41
    .line 42
    iget-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 43
    .line 44
    if-eqz v1, :cond_2

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    goto :goto_0

    .line 51
    :cond_2
    const/4 v1, 0x0

    .line 52
    :goto_0
    move-object v5, v1

    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 54
    .line 55
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 56
    .line 57
    iget-object v4, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o〇O8〇〇o()I

    .line 62
    .line 63
    .line 64
    move-result v6

    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->〇oo〇()I

    .line 68
    .line 69
    .line 70
    move-result v7

    .line 71
    iget-object v8, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 72
    .line 73
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇8o8O〇O(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 74
    .line 75
    .line 76
    return-void

    .line 77
    :cond_3
    :goto_1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 78
    .line 79
    const-string v1, "requestEnhanceThumb == null"

    .line 80
    .line 81
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇Oo〇O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0880O0〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇Oo〇oO8O(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 6
    .line 7
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 8
    .line 9
    const v2, 0x7f130420

    .line 10
    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    new-instance v6, Lcom/intsig/camscanner/multiimageedit/O〇O〇oO;

    .line 14
    .line 15
    invoke-direct {v6, p0, p1}, Lcom/intsig/camscanner/multiimageedit/O〇O〇oO;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    new-instance v7, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$11;

    .line 19
    .line 20
    invoke-direct {v7, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$11;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 21
    .line 22
    .line 23
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 24
    .line 25
    iget-wide v8, v4, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 26
    .line 27
    move-object v4, p1

    .line 28
    move-object v5, p2

    .line 29
    invoke-static/range {v0 .. v9}, Lcom/intsig/camscanner/app/DialogUtils;->o88〇OO08〇(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;Lcom/intsig/camscanner/app/DialogUtils$OnTemplateSettingsListener;J)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
.end method

.method private 〇O〇0(Lcom/intsig/owlery/DialogOwl;)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/owlery/BaseOwl;->〇o00〇〇Oo()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const-string v2, "TIPS_SUPER_FILTER"

    .line 10
    .line 11
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O00oo0()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    goto :goto_0

    .line 22
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/owlery/BaseOwl;->〇o00〇〇Oo()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const-string v1, "TIPS_MULTI_PREVIEW_ADJUST_BOUNDS"

    .line 27
    .line 28
    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-eqz p1, :cond_2

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8O()Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    :cond_2
    :goto_0
    return v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic 〇O〇o0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇oo〇:Landroidx/lifecycle/MutableLiveData;

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O08o〇()V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇00o08(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇o008o08O()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v0, v1, v2, v2}, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o08(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/purchase/entity/Function;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8o8o(Lcom/intsig/camscanner/purchase/entity/Function;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇o0o()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-eqz v1, :cond_2

    .line 27
    .line 28
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 33
    .line 34
    if-eqz v1, :cond_1

    .line 35
    .line 36
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 37
    .line 38
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 39
    .line 40
    const-wide/16 v3, 0x0

    .line 41
    .line 42
    invoke-virtual {v2, v1, v3, v4}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    return-void

    .line 47
    :cond_3
    :goto_1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 48
    .line 49
    const-string v1, "F-reHandleAllErrorPaper and get null ptr"

    .line 50
    .line 51
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇o88〇O()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_7

    .line 14
    .line 15
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 16
    .line 17
    if-nez v1, :cond_0

    .line 18
    .line 19
    goto/16 :goto_3

    .line 20
    .line 21
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .line 25
    .line 26
    iget-object v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v2}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->〇00()Z

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    const-string v3, "CSBatchEnhanceSnackBar"

    .line 37
    .line 38
    if-eqz v2, :cond_1

    .line 39
    .line 40
    iget-object v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 41
    .line 42
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 43
    .line 44
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    if-nez v4, :cond_6

    .line 49
    .line 50
    new-instance v4, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;

    .line 51
    .line 52
    new-instance v5, Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 53
    .line 54
    invoke-direct {v5}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;-><init>()V

    .line 55
    .line 56
    .line 57
    const/4 v6, 0x0

    .line 58
    invoke-direct {v4, v2, v6, v5}, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/demoire/ImageQualityHelper;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    new-instance v2, Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;

    .line 65
    .line 66
    const/4 v4, -0x1

    .line 67
    invoke-direct {v2, v4, v6, v1}, Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;-><init>(ILandroid/graphics/Bitmap;Ljava/util/ArrayList;)V

    .line 68
    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o〇88〇8:Lcom/intsig/camscanner/demoire/DeMoireManager$OnDeMoireFinishListener;

    .line 71
    .line 72
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/demoire/DeMoireManager$OnDeMoireFinishListener;->〇000O0(Lcom/intsig/camscanner/demoire/DeMoireManager$DeMoireFinishResult;)V

    .line 73
    .line 74
    .line 75
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->OoO8()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    const-string v1, "cancel"

    .line 86
    .line 87
    const-string v2, "type"

    .line 88
    .line 89
    invoke-static {v3, v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    goto :goto_2

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 94
    .line 95
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    if-eqz v2, :cond_3

    .line 108
    .line 109
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 110
    .line 111
    .line 112
    move-result-object v2

    .line 113
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 114
    .line 115
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 116
    .line 117
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 118
    .line 119
    .line 120
    move-result-object v4

    .line 121
    invoke-virtual {v4}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->O8ooOoo〇()Z

    .line 122
    .line 123
    .line 124
    move-result v4

    .line 125
    if-eqz v4, :cond_2

    .line 126
    .line 127
    new-instance v4, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;

    .line 128
    .line 129
    iget-object v5, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 130
    .line 131
    iget-object v6, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 132
    .line 133
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    invoke-direct {v4, v5, v6, v2}, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/demoire/ImageQualityHelper;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    goto :goto_0

    .line 144
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 145
    .line 146
    .line 147
    move-result v0

    .line 148
    if-lez v0, :cond_6

    .line 149
    .line 150
    const-string v0, "clear_moire"

    .line 151
    .line 152
    invoke-static {v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 156
    .line 157
    .line 158
    move-result-wide v2

    .line 159
    invoke-static {v2, v3}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->oo〇(J)V

    .line 160
    .line 161
    .line 162
    sget-object v0, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇080:Lcom/intsig/camscanner/demoire/DeMoireManager;

    .line 163
    .line 164
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇o(Ljava/util/ArrayList;)Z

    .line 165
    .line 166
    .line 167
    move-result v2

    .line 168
    if-eqz v2, :cond_4

    .line 169
    .line 170
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇088O:Z

    .line 171
    .line 172
    if-nez v2, :cond_5

    .line 173
    .line 174
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o〇0〇80()V

    .line 175
    .line 176
    .line 177
    goto :goto_1

    .line 178
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o〇0〇80()V

    .line 179
    .line 180
    .line 181
    :cond_5
    :goto_1
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/demoire/DeMoireManager;->O08000(Ljava/util/ArrayList;)V

    .line 182
    .line 183
    .line 184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 185
    .line 186
    .line 187
    move-result-wide v0

    .line 188
    iput-wide v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇OOoooo:J

    .line 189
    .line 190
    :cond_6
    :goto_2
    return-void

    .line 191
    :cond_7
    :goto_3
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 192
    .line 193
    const-string v1, "clickImageQuality multiImageEditPage == null"

    .line 194
    .line 195
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    .line 197
    .line 198
    return-void
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static bridge synthetic 〇o8〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇o8〇〇(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O8〇〇o(I)Lcom/intsig/camscanner/view/ZoomImageView;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 24
    .line 25
    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    if-eqz v1, :cond_2

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 36
    .line 37
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 38
    .line 39
    invoke-virtual {v2, v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇08O8o〇0(Lcom/intsig/camscanner/view/ZoomImageView;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-eqz v1, :cond_2

    .line 44
    .line 45
    new-instance v1, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 46
    .line 47
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    .line 48
    .line 49
    .line 50
    const/4 p1, 0x1

    .line 51
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 56
    .line 57
    .line 58
    :goto_0
    return-void

    .line 59
    :cond_3
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 60
    .line 61
    const-string v0, "multiImageEditAdapter == null || imageViewPager == null"

    .line 62
    .line 63
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO80O0o8O(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇oOO80o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oO〇()Lcom/intsig/owlery/DialogOwl;
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "createSuperFilterDialogOwl"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/owlery/DialogOwl;

    .line 9
    .line 10
    const-string v1, "TIPS_SUPER_FILTER"

    .line 11
    .line 12
    const/high16 v2, 0x3fa00000    # 1.25f

    .line 13
    .line 14
    invoke-direct {v0, v1, v2}, Lcom/intsig/owlery/DialogOwl;-><init>(Ljava/lang/String;F)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇oO〇08o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8OooO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static synthetic 〇oo8(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V
    .locals 2

    .line 1
    const-wide/16 v0, -0x1

    .line 2
    .line 3
    iput-wide v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->o〇0:J

    .line 4
    .line 5
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 6
    .line 7
    iget p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 8
    .line 9
    if-ne p0, p2, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    iput p0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 13
    .line 14
    const/4 p0, 0x0

    .line 15
    iput-boolean p0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 16
    .line 17
    iget p0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 18
    .line 19
    sget p2, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇080:I

    .line 20
    .line 21
    if-ne p0, p2, :cond_1

    .line 22
    .line 23
    sget p0, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o00〇〇Oo:I

    .line 24
    .line 25
    iput p0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇oo88〇O〇8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇o08:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "enhanceThumbAdapter == null"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceMode(I)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 22
    .line 23
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/oO00OOO;

    .line 24
    .line 25
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/multiimageedit/oO00OOO;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o08oOO(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel$MultiImageEditModelTraverse;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8o:Landroid/widget/CheckBox;

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇8o8O〇O()Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇ooO〇000(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0〇〇o0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇o〇0〇80()V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇088O:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 7
    .line 8
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O8〇〇o(I)Lcom/intsig/camscanner/view/ZoomImageView;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getDisplayBoundRectOnScreen()Landroid/graphics/RectF;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getImageBm()Landroid/graphics/Bitmap;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/demoire/ImageQualityLoadingAnim;->OO0o〇〇〇〇0(Landroid/content/Context;Landroid/graphics/RectF;Landroid/graphics/Bitmap;)Lcom/intsig/camscanner/demoire/ImageQualityLoadingAnim;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇0〇o:Lcom/intsig/camscanner/demoire/ImageQualityLoadingAnim;

    .line 33
    .line 34
    if-eqz v0, :cond_0

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityLoadingAnim;->〇O00()V

    .line 37
    .line 38
    .line 39
    :cond_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇o〇88(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o〇88〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇8OO(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇o〇OO80oO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO〇oo:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇o〇〇88〇8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 4
    .line 5
    .line 6
    const-string v0, "CSTestPaper"

    .line 7
    .line 8
    const-string v1, "continue_capture"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8〇o0〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/util/List;Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;JILjava/util/List;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p7}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO8O(Ljava/util/List;Ljava/lang/String;Lcom/intsig/camscanner/bankcardjournal/model/BankCardJournalPageData;JILjava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method private 〇〇08〇0oo0()Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0ooOOo:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/Oo8Oo00oo;

    .line 10
    .line 11
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/Oo8Oo00oo;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O8〇〇o8〇()Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 15
    .line 16
    .line 17
    move-result-object v3

    .line 18
    invoke-virtual {v3}, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$ImageDealListener;Landroidx/lifecycle/MutableLiveData;Landroidx/lifecycle/LifecycleOwner;)V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0ooOOo:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 30
    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0ooOOo:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 32
    .line 33
    return-object v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇〇0Oo0880(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "discard"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00o0O〇〇o()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇〇0〇〇8O()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇80O()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSBatchResultDelete"

    .line 5
    .line 6
    const-string v1, "retake"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇8088()V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/owlery/TheOwlery;->OO0o〇〇〇〇0(Landroidx/fragment/app/Fragment;)Lcom/intsig/owlery/TheOwlery;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇0000OOO;

    .line 8
    .line 9
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/〇0000OOO;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/owlery/TheOwlery;->OO0o〇〇(Lcom/intsig/owlery/DialogShowListener;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇80O()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/o〇〇0〇;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/o〇〇0〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/util/PermissionUtil;->O8(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇80o〇o0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇〇(I)V

    .line 5
    .line 6
    .line 7
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇8〇Oo0()V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 16
    .line 17
    iget-boolean v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 22
    .line 23
    iget-object v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 24
    .line 25
    invoke-virtual {v1, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 30
    .line 31
    const-string v1, ""

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 34
    .line 35
    .line 36
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08〇808()V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇〇8OO(I)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 6
    .line 7
    iget-wide v2, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 8
    .line 9
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 10
    .line 11
    invoke-static {v0, v2, v3, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o0O0O8(Landroid/content/Context;JLjava/lang/String;)V

    .line 12
    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 21
    .line 22
    iget-wide v2, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 23
    .line 24
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇00〇8(Landroid/content/Context;JI)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic 〇〇8o0OOOo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇8〇Oo0()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v1, ""

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const v1, 0x7f0d0556

    .line 17
    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const v1, 0x7f0a0afb

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    check-cast v1, Landroid/widget/LinearLayout;

    .line 32
    .line 33
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo8ooo8O:Landroid/widget/LinearLayout;

    .line 34
    .line 35
    const/4 v2, 0x1

    .line 36
    new-array v2, v2, [Landroid/view/View;

    .line 37
    .line 38
    const/4 v3, 0x0

    .line 39
    aput-object v1, v2, v3

    .line 40
    .line 41
    invoke-virtual {p0, v2}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 42
    .line 43
    .line 44
    new-instance v1, Landroidx/appcompat/widget/Toolbar$LayoutParams;

    .line 45
    .line 46
    const/4 v2, -0x1

    .line 47
    const/16 v3, 0x11

    .line 48
    .line 49
    const/4 v4, -0x2

    .line 50
    invoke-direct {v1, v4, v2, v3}, Landroidx/appcompat/widget/Toolbar$LayoutParams;-><init>(III)V

    .line 51
    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 54
    .line 55
    const v3, 0x7f0a11ed

    .line 56
    .line 57
    .line 58
    invoke-virtual {v2, v3}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    check-cast v2, Landroid/widget/LinearLayout;

    .line 63
    .line 64
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 68
    .line 69
    invoke-virtual {v1, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarWrapMenu(Landroid/view/View;)V

    .line 70
    .line 71
    .line 72
    :cond_0
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇〇O()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80oo〇0〇o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->O8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/owlery/DialogOwl;

    .line 19
    .line 20
    const-string v1, "TIPS_MULTI_PREVIEW_ADJUST_BOUNDS"

    .line 21
    .line 22
    const v2, 0x3fa66666    # 1.3f

    .line 23
    .line 24
    .line 25
    invoke-direct {v0, v1, v2}, Lcom/intsig/owlery/DialogOwl;-><init>(Ljava/lang/String;F)V

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 29
    .line 30
    invoke-virtual {v1, v0}, Lcom/intsig/owlery/TheOwlery;->〇O00(Lcom/intsig/owlery/BaseOwl;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0O0〇:Lcom/intsig/owlery/TheOwlery;

    .line 34
    .line 35
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8〇〇〇0O(Lcom/intsig/owlery/TheOwlery;)V

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void

    .line 39
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08O:Landroid/view/View;

    .line 40
    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    const/16 v1, 0x8

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 46
    .line 47
    .line 48
    :cond_3
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇〇OO()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08〇()V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 13
    .line 14
    const-string v1, "F-tryToSavePaper but no network!"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0ooO()Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 29
    .line 30
    const-string v1, "F-tryToSavePaper but not finish yet!"

    .line 31
    .line 32
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo〇88〇()V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0〇O80ooo()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 46
    .line 47
    const-string v1, "F-tryToSavePaper click, currentListHasError"

    .line 48
    .line 49
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o08()V

    .line 53
    .line 54
    .line 55
    return-void

    .line 56
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 57
    .line 58
    const-string v1, "F-tryToSavePaper"

    .line 59
    .line 60
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 64
    .line 65
    iget-wide v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 66
    .line 67
    const-wide/16 v2, 0x0

    .line 68
    .line 69
    cmp-long v4, v0, v2

    .line 70
    .line 71
    if-gez v4, :cond_3

    .line 72
    .line 73
    sget-object v0, Lcom/intsig/camscanner/paper/PaperPropertySelectActivity;->o8〇OO0〇0o:Lcom/intsig/camscanner/paper/PaperPropertySelectActivity$Companion;

    .line 74
    .line 75
    const/16 v1, 0x6c

    .line 76
    .line 77
    invoke-virtual {v0, p0, v1}, Lcom/intsig/camscanner/paper/PaperPropertySelectActivity$Companion;->〇080(Landroidx/fragment/app/Fragment;I)V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_3
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇8〇8〇8(Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    :goto_0
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇〇Ooo0o()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "logForClickDelete : START!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->〇〇888()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    const-string v0, "CSBatchResult"

    .line 20
    .line 21
    const-string v1, "delete"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    const-string v0, "CSBatchResultDelete"

    .line 27
    .line 28
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇〇Oo〇0O〇8(Z)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/4 p1, 0x4

    .line 7
    :goto_0
    const/4 v1, 0x1

    .line 8
    new-array v1, v1, [Landroid/view/View;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇80o:Landroid/widget/TextView;

    .line 11
    .line 12
    aput-object v2, v1, v0

    .line 13
    .line 14
    invoke-static {p1, v1}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇O〇〇00(I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a09e8

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 11
    .line 12
    const v2, 0x7f0a09a6

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const/4 v2, 0x0

    .line 20
    const v3, 0x3e99999a    # 0.3f

    .line 21
    .line 22
    .line 23
    const/high16 v4, 0x3f800000    # 1.0f

    .line 24
    .line 25
    const/4 v5, 0x1

    .line 26
    if-nez p1, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 39
    .line 40
    .line 41
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    sub-int/2addr v0, v5

    .line 48
    if-ne p1, v0, :cond_1

    .line 49
    .line 50
    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 54
    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0000OOO()Z

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    if-eqz p1, :cond_2

    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O:Landroid/widget/TextView;

    .line 65
    .line 66
    invoke-virtual {p1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 67
    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_1
    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v4}, Landroid/view/View;->setAlpha(F)V

    .line 74
    .line 75
    .line 76
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 77
    .line 78
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0000OOO()Z

    .line 79
    .line 80
    .line 81
    move-result p1

    .line 82
    if-eqz p1, :cond_2

    .line 83
    .line 84
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O:Landroid/widget/TextView;

    .line 85
    .line 86
    invoke-virtual {p1, v4}, Landroid/view/View;->setAlpha(F)V

    .line 87
    .line 88
    .line 89
    :cond_2
    :goto_1
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇〇o08()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    const v1, 0x7f130c8f

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const v1, 0x7f130ae1

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/O08000;

    .line 25
    .line 26
    invoke-direct {v1}, Lcom/intsig/camscanner/multiimageedit/O08000;-><init>()V

    .line 27
    .line 28
    .line 29
    const v2, 0x7f130d3e

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇O80O(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇〇o80Oo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8〇〇ooo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 12
    .line 13
    iget-object v3, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 14
    .line 15
    const/4 v5, 0x0

    .line 16
    new-instance v6, Lcom/intsig/camscanner/multiimageedit/Ooo;

    .line 17
    .line 18
    invoke-direct {v6, p0, p2}, Lcom/intsig/camscanner/multiimageedit/Ooo;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    new-instance v7, Lcom/intsig/camscanner/multiimageedit/〇O〇80o08O;

    .line 22
    .line 23
    invoke-direct {v7, p0, p2, p1}, Lcom/intsig/camscanner/multiimageedit/〇O〇80o08O;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    move-object v4, p2

    .line 27
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/mainmenu/SensitiveWordsChecker;->〇080(Ljava/lang/Boolean;Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
.end method

.method private 〇〇o〇0O(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 13
    .line 14
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-eqz v0, :cond_2

    .line 23
    .line 24
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-eqz v1, :cond_2

    .line 33
    .line 34
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    check-cast v1, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;

    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 41
    .line 42
    invoke-virtual {v1}, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;->〇o00〇〇Oo()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O〇8oOo8O(Ljava/lang/String;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    if-eqz v2, :cond_1

    .line 51
    .line 52
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 53
    .line 54
    if-eqz v2, :cond_1

    .line 55
    .line 56
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    invoke-virtual {v3}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->o8()V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1}, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;->〇o〇()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    iput-object v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O8o08O8O:Ljava/lang/String;

    .line 68
    .line 69
    invoke-virtual {v1}, Lcom/intsig/camscanner/demoire/DeMoireManager$ImageQualityEntity;->O8()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    iput-object v1, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 74
    .line 75
    const/4 v1, 0x0

    .line 76
    iput-boolean v1, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 77
    .line 78
    sget v1, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o00〇〇Oo:I

    .line 79
    .line 80
    iput v1, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 81
    .line 82
    iget-object v1, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 83
    .line 84
    iget-object v3, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 85
    .line 86
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 87
    .line 88
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    if-eqz v1, :cond_1

    .line 93
    .line 94
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 95
    .line 96
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 97
    .line 98
    .line 99
    move-result-wide v3

    .line 100
    invoke-virtual {v1, v2, v3, v4}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->oO8o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 101
    .line 102
    .line 103
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 104
    .line 105
    .line 106
    goto :goto_0

    .line 107
    :cond_2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇O〇0oo0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 108
    .line 109
    .line 110
    :cond_3
    :goto_1
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇〇〇0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/owlery/DialogOwl;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00(Lcom/intsig/owlery/DialogOwl;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇〇00(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇oo8(ILcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇〇〇OOO〇〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOoo80oO:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇〇O〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0OOoO8O0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O08OO8o8O(Lcom/google/android/material/tabs/TabLayout;)V
    .locals 6
    .param p1    # Lcom/google/android/material/tabs/TabLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout;->getTabCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    instance-of v4, v3, Landroid/view/ViewGroup;

    .line 14
    .line 15
    if-eqz v4, :cond_0

    .line 16
    .line 17
    check-cast v3, Landroid/view/ViewGroup;

    .line 18
    .line 19
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 24
    .line 25
    .line 26
    move-result-object v4

    .line 27
    invoke-virtual {v3, v1}, Landroid/view/View;->setMinimumWidth(I)V

    .line 28
    .line 29
    .line 30
    const/4 v5, -0x2

    .line 31
    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 32
    .line 33
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 37
    .line 38
    .line 39
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O0o8〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Z)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "changeCompareState isDownMovement:"

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    if-nez p1, :cond_0

    .line 24
    .line 25
    const-string p1, "multiImageEditPage is null"

    .line 26
    .line 27
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_0
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 32
    .line 33
    if-nez p1, :cond_1

    .line 34
    .line 35
    const-string p1, "modifyMultiImageEditModel is null"

    .line 36
    .line 37
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_1
    if-eqz p2, :cond_2

    .line 42
    .line 43
    iget p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 44
    .line 45
    invoke-static {p2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceIndex(I)I

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    invoke-static {p2}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇o00〇〇Oo(I)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p2

    .line 53
    const-string v1, "CSBatchResult"

    .line 54
    .line 55
    const-string v2, "compare"

    .line 56
    .line 57
    const-string v3, "type"

    .line 58
    .line 59
    invoke-static {v1, v2, v3, p2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    const/4 p2, 0x1

    .line 63
    iput p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO0880O:I

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_2
    const/4 p2, 0x0

    .line 67
    iput p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO0880O:I

    .line 68
    .line 69
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 72
    .line 73
    .line 74
    const-string p1, "changeCompareState: START"

    .line 75
    .line 76
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method Oo0()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a075e

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/camscanner/view/MyViewPager;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setOffscreenPageLimit(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 27
    .line 28
    .line 29
    new-instance v0, Lcom/intsig/view/viewpager/AlphaScaleTransformer;

    .line 30
    .line 31
    invoke-direct {v0}, Lcom/intsig/view/viewpager/AlphaScaleTransformer;-><init>()V

    .line 32
    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 35
    .line 36
    invoke-virtual {v2, v1, v0}, Landroidx/viewpager/widget/ViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    .line 37
    .line 38
    .line 39
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 40
    .line 41
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 44
    .line 45
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 46
    .line 47
    .line 48
    move-result-object v5

    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00o80oo()Z

    .line 50
    .line 51
    .line 52
    move-result v6

    .line 53
    iget v7, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OO8ooO8〇:I

    .line 54
    .line 55
    iget-object v8, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 56
    .line 57
    move-object v3, v0

    .line 58
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;ZILjava/lang/String;)V

    .line 59
    .line 60
    .line 61
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 62
    .line 63
    iget v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO8:I

    .line 64
    .line 65
    const/4 v3, 0x1

    .line 66
    if-lez v2, :cond_0

    .line 67
    .line 68
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇O:Z

    .line 69
    .line 70
    if-eqz v2, :cond_0

    .line 71
    .line 72
    const/4 v2, 0x1

    .line 73
    goto :goto_0

    .line 74
    :cond_0
    const/4 v2, 0x0

    .line 75
    :goto_0
    iput-boolean v2, v0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0〇O0088o:Z

    .line 76
    .line 77
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O〇O〇oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;)V

    .line 78
    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 81
    .line 82
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 83
    .line 84
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇8oOO88(Lcom/intsig/camscanner/view/MyViewPager;)V

    .line 85
    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 88
    .line 89
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 90
    .line 91
    const v4, 0x7f0a0c7a

    .line 92
    .line 93
    .line 94
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇80(Landroid/view/View;)V

    .line 99
    .line 100
    .line 101
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 102
    .line 103
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 104
    .line 105
    if-nez v2, :cond_1

    .line 106
    .line 107
    iget-boolean v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO0880O:Z

    .line 108
    .line 109
    if-nez v2, :cond_1

    .line 110
    .line 111
    const/4 v1, 0x1

    .line 112
    :cond_1
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O(Z)V

    .line 113
    .line 114
    .line 115
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 116
    .line 117
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$8;

    .line 118
    .line 119
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 126
    .line 127
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 128
    .line 129
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 130
    .line 131
    .line 132
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 6

    .line 1
    sget-object p1, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->INSTANCE:Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->init()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇80o〇o0()V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇8088()V

    .line 10
    .line 11
    .line 12
    iget-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇08oOOO0:Z

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    new-instance p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$BatchImageTaskForMultiEdit;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 21
    .line 22
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 23
    .line 24
    new-instance v4, Lcom/intsig/camscanner/multiimageedit/o88〇OO08〇;

    .line 25
    .line 26
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/multiimageedit/o88〇OO08〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8Oo8〇8()Z

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    move-object v0, p1

    .line 34
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment$BatchImageTaskForMultiEdit;-><init>(Landroid/app/Activity;Ljava/util/List;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/Runnable;Z)V

    .line 35
    .line 36
    .line 37
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    const/4 v1, 0x0

    .line 42
    new-array v1, v1, [Ljava/lang/Void;

    .line 43
    .line 44
    invoke-virtual {p1, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->init()V

    .line 49
    .line 50
    .line 51
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 52
    .line 53
    const-string v0, "onCreateView"

    .line 54
    .line 55
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public interceptBackPressed()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8oo0OOO(Z)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o008()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 12
    .line 13
    const-string v1, "CSTestPaper"

    .line 14
    .line 15
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    return v0
    .line 20
    .line 21
.end method

.method public o08O()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "addItem START"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->〇080()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v2, "addItem: intercept by actionClient="

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 29
    .line 30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_1

    .line 46
    .line 47
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/o〇O;

    .line 48
    .line 49
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/o〇O;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇O80o8OO(Lcom/intsig/callback/Callback0;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    const-string v0, "CSBatchResult"

    .line 57
    .line 58
    const-string v1, "add"

    .line 59
    .line 60
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 64
    .line 65
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 66
    .line 67
    .line 68
    :goto_0
    return-void
.end method

.method public o0〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo0O0o8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 1

    .line 1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "retakeItem"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇80O()V

    .line 9
    .line 10
    .line 11
    const-string p1, "CSBatchResult"

    .line 12
    .line 13
    const-string v0, "retake"

    .line 14
    .line 15
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public o88o0O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "deleteItem"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇Ooo0o()V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 18
    .line 19
    .line 20
    const v1, 0x7f130a04

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const v1, 0x7f130a03

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const/4 v1, 0x1

    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->O8〇o(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const v1, 0x800005

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->o8(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/OOO〇O0;

    .line 47
    .line 48
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/multiimageedit/OOO〇O0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 49
    .line 50
    .line 51
    const p1, 0x7f130533

    .line 52
    .line 53
    .line 54
    const v2, 0x7f0602f1

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, p1, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇oOO8O8(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/oo〇;

    .line 62
    .line 63
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/oo〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 64
    .line 65
    .line 66
    const v1, 0x7f1317d7

    .line 67
    .line 68
    .line 69
    invoke-virtual {p1, v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇oo〇(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/O8〇o;

    .line 74
    .line 75
    invoke-direct {v0}, Lcom/intsig/camscanner/multiimageedit/O8〇o;-><init>()V

    .line 76
    .line 77
    .line 78
    const v1, 0x7f13057e

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public o8oo0OOO(Z)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O88O:Landroid/view/View;

    .line 2
    .line 3
    const-string v1, "CSBatchResult"

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const-string v0, "modify_quit"

    .line 15
    .line 16
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo〇〇〇〇(Z)Lorg/json/JSONObject;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-static {v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8oO0()V

    .line 24
    .line 25
    .line 26
    return v2

    .line 27
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8o088〇0()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    const-string v0, "filter_quit"

    .line 34
    .line 35
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo〇〇〇〇(Z)Lorg/json/JSONObject;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-static {v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OOOo〇()V

    .line 43
    .line 44
    .line 45
    return v2

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 47
    .line 48
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 49
    .line 50
    invoke-virtual {v3}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O8〇〇o(I)Lcom/intsig/camscanner/view/ZoomImageView;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    if-eqz v0, :cond_2

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    const/high16 v4, 0x3f800000    # 1.0f

    .line 65
    .line 66
    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    if-lez v3, :cond_2

    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->〇80()V

    .line 73
    .line 74
    .line 75
    return v2

    .line 76
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    const/4 v3, 0x0

    .line 81
    if-eqz v0, :cond_3

    .line 82
    .line 83
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 84
    .line 85
    if-nez v0, :cond_3

    .line 86
    .line 87
    const-string p1, "CSTestPaper"

    .line 88
    .line 89
    const-string v0, "continue_capture"

    .line 90
    .line 91
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇()Z

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    const-string v4, "back_to_scan"

    .line 100
    .line 101
    if-eqz v0, :cond_4

    .line 102
    .line 103
    const/4 p1, 0x2

    .line 104
    new-array p1, p1, [Landroid/util/Pair;

    .line 105
    .line 106
    const-string v0, "from"

    .line 107
    .line 108
    const-string v5, "bank_statement"

    .line 109
    .line 110
    invoke-static {v0, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    aput-object v0, p1, v3

    .line 115
    .line 116
    const-string v0, "from_part"

    .line 117
    .line 118
    const-string v5, "cs_scan"

    .line 119
    .line 120
    invoke-static {v0, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    aput-object v0, p1, v2

    .line 125
    .line 126
    invoke-static {v1, v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 127
    .line 128
    .line 129
    goto :goto_0

    .line 130
    :cond_4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo〇〇〇〇(Z)Lorg/json/JSONObject;

    .line 131
    .line 132
    .line 133
    move-result-object p1

    .line 134
    invoke-static {v1, v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 135
    .line 136
    .line 137
    :goto_0
    iget-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooO:Z

    .line 138
    .line 139
    if-nez p1, :cond_6

    .line 140
    .line 141
    iget-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇08oOOO0:Z

    .line 142
    .line 143
    if-nez p1, :cond_6

    .line 144
    .line 145
    iget-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o8O:Z

    .line 146
    .line 147
    if-eqz p1, :cond_5

    .line 148
    .line 149
    goto :goto_1

    .line 150
    :cond_5
    return v3

    .line 151
    :cond_6
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇0O088()V

    .line 152
    .line 153
    .line 154
    return v2
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "onActivityResult requestCode="

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v2, " resultCode="

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 35
    .line 36
    if-eqz v1, :cond_0

    .line 37
    .line 38
    invoke-virtual {v1, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->o〇0(IILandroid/content/Intent;)Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-eqz v1, :cond_0

    .line 43
    .line 44
    new-instance p1, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string p2, "onActivityResult handle by actionClient="

    .line 50
    .line 51
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 55
    .line 56
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 67
    .line 68
    .line 69
    return-void

    .line 70
    :cond_0
    const/16 v1, 0x66

    .line 71
    .line 72
    const-string v2, "extra_border"

    .line 73
    .line 74
    const/4 v3, 0x1

    .line 75
    const/4 v4, 0x0

    .line 76
    if-ne p1, v1, :cond_6

    .line 77
    .line 78
    if-eqz p3, :cond_4

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 81
    .line 82
    if-eqz p1, :cond_4

    .line 83
    .line 84
    invoke-static {}, Lcom/intsig/utils/DocumentUtil;->Oo08()Lcom/intsig/utils/DocumentUtil;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    invoke-virtual {p1, p2, v1}, Lcom/intsig/utils/DocumentUtil;->〇〇888(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-virtual {p3, v2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    .line 101
    .line 102
    .line 103
    move-result-object p2

    .line 104
    const-string v1, "EXTRA_CAPTURE_SETTING_ROTATION"

    .line 105
    .line 106
    invoke-virtual {p3, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 107
    .line 108
    .line 109
    move-result p3

    .line 110
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 111
    .line 112
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 113
    .line 114
    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 115
    .line 116
    .line 117
    move-result v2

    .line 118
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 123
    .line 124
    .line 125
    move-result v2

    .line 126
    if-eqz v2, :cond_1

    .line 127
    .line 128
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0oO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;[II)V

    .line 129
    .line 130
    .line 131
    goto :goto_0

    .line 132
    :cond_1
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇Oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;[II)V

    .line 133
    .line 134
    .line 135
    :goto_0
    if-eqz v1, :cond_3

    .line 136
    .line 137
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 138
    .line 139
    invoke-virtual {p3}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 140
    .line 141
    .line 142
    move-result p3

    .line 143
    if-nez p3, :cond_2

    .line 144
    .line 145
    iget-object p3, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 146
    .line 147
    sget-object v2, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;

    .line 148
    .line 149
    invoke-virtual {v2}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;->requireWaitingInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 150
    .line 151
    .line 152
    move-result-object v2

    .line 153
    iput-object v2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0OoOOo0:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 154
    .line 155
    :cond_2
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 156
    .line 157
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 158
    .line 159
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 160
    .line 161
    .line 162
    move-result-wide v2

    .line 163
    invoke-virtual {p3, v1, v2, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 164
    .line 165
    .line 166
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 167
    .line 168
    .line 169
    new-instance p3, Ljava/lang/StringBuilder;

    .line 170
    .line 171
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    .line 173
    .line 174
    const-string v1, "retake imagePath="

    .line 175
    .line 176
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    const-string p1, " borders="

    .line 183
    .line 184
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    .line 186
    .line 187
    invoke-static {p2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object p1

    .line 191
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object p1

    .line 198
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .line 200
    .line 201
    goto/16 :goto_5

    .line 202
    .line 203
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    .line 204
    .line 205
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    .line 207
    .line 208
    const-string p2, "imageViewPager == null is "

    .line 209
    .line 210
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 214
    .line 215
    if-nez p2, :cond_5

    .line 216
    .line 217
    goto :goto_1

    .line 218
    :cond_5
    const/4 v3, 0x0

    .line 219
    :goto_1
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object p1

    .line 226
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    .line 228
    .line 229
    goto/16 :goto_5

    .line 230
    .line 231
    :cond_6
    const/16 v1, 0x67

    .line 232
    .line 233
    if-ne p1, v1, :cond_7

    .line 234
    .line 235
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇O8OO:Landroid/widget/EditText;

    .line 236
    .line 237
    if-eqz p1, :cond_16

    .line 238
    .line 239
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 240
    .line 241
    .line 242
    move-result-object p1

    .line 243
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇O8OO:Landroid/widget/EditText;

    .line 244
    .line 245
    invoke-static {p1, p2}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 246
    .line 247
    .line 248
    goto/16 :goto_5

    .line 249
    .line 250
    :cond_7
    const/16 v1, 0x68

    .line 251
    .line 252
    const/4 v5, -0x1

    .line 253
    if-ne p1, v1, :cond_b

    .line 254
    .line 255
    if-eqz p3, :cond_16

    .line 256
    .line 257
    if-ne p2, v5, :cond_16

    .line 258
    .line 259
    invoke-virtual {p3, v2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    .line 260
    .line 261
    .line 262
    move-result-object p1

    .line 263
    const-string p2, "image_rotation"

    .line 264
    .line 265
    invoke-virtual {p3, p2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 266
    .line 267
    .line 268
    move-result p2

    .line 269
    new-instance p3, Ljava/lang/StringBuilder;

    .line 270
    .line 271
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 272
    .line 273
    .line 274
    const-string v1, "borders="

    .line 275
    .line 276
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    .line 278
    .line 279
    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v1

    .line 283
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    .line 285
    .line 286
    const-string v1, " rotation="

    .line 287
    .line 288
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    .line 290
    .line 291
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 292
    .line 293
    .line 294
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 295
    .line 296
    .line 297
    move-result-object p3

    .line 298
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    .line 300
    .line 301
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 302
    .line 303
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 304
    .line 305
    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 306
    .line 307
    .line 308
    move-result v0

    .line 309
    invoke-virtual {p3, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 310
    .line 311
    .line 312
    move-result-object p3

    .line 313
    if-eqz p3, :cond_16

    .line 314
    .line 315
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 316
    .line 317
    if-eqz v0, :cond_16

    .line 318
    .line 319
    iget v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 320
    .line 321
    if-ne v1, p2, :cond_8

    .line 322
    .line 323
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 324
    .line 325
    invoke-static {v0, p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isSameBorder([I[I)Z

    .line 326
    .line 327
    .line 328
    move-result v0

    .line 329
    if-nez v0, :cond_16

    .line 330
    .line 331
    :cond_8
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 332
    .line 333
    iput-object p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 334
    .line 335
    iput p2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 336
    .line 337
    if-eqz p1, :cond_9

    .line 338
    .line 339
    goto :goto_2

    .line 340
    :cond_9
    const/4 v3, 0x0

    .line 341
    :goto_2
    iput-boolean v3, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 342
    .line 343
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 344
    .line 345
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 346
    .line 347
    .line 348
    move-result p1

    .line 349
    if-nez p1, :cond_a

    .line 350
    .line 351
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 352
    .line 353
    sget-object p2, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;

    .line 354
    .line 355
    invoke-virtual {p2}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;->requireWaitingInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 356
    .line 357
    .line 358
    move-result-object p2

    .line 359
    iput-object p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0OoOOo0:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 360
    .line 361
    :cond_a
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 362
    .line 363
    iget-object p2, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 364
    .line 365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 366
    .line 367
    .line 368
    move-result-wide v0

    .line 369
    invoke-virtual {p1, p2, v0, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 370
    .line 371
    .line 372
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 373
    .line 374
    .line 375
    goto/16 :goto_5

    .line 376
    .line 377
    :cond_b
    const/16 v1, 0x69

    .line 378
    .line 379
    if-ne p1, v1, :cond_e

    .line 380
    .line 381
    if-eqz p3, :cond_c

    .line 382
    .line 383
    if-ne p2, v5, :cond_c

    .line 384
    .line 385
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇88(Landroid/content/Intent;)V

    .line 386
    .line 387
    .line 388
    goto/16 :goto_5

    .line 389
    .line 390
    :cond_c
    new-instance p1, Ljava/lang/StringBuilder;

    .line 391
    .line 392
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 393
    .line 394
    .line 395
    const-string p2, "onActivityResult, REQ_PAGE_BATCH_REEDIT, data is null="

    .line 396
    .line 397
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    .line 399
    .line 400
    if-nez p3, :cond_d

    .line 401
    .line 402
    goto :goto_3

    .line 403
    :cond_d
    const/4 v3, 0x0

    .line 404
    :goto_3
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 405
    .line 406
    .line 407
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 408
    .line 409
    .line 410
    move-result-object p1

    .line 411
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    .line 413
    .line 414
    goto/16 :goto_5

    .line 415
    .line 416
    :cond_e
    const/16 v0, 0x6a

    .line 417
    .line 418
    if-ne p1, v0, :cond_f

    .line 419
    .line 420
    if-eqz p3, :cond_16

    .line 421
    .line 422
    if-ne p2, v5, :cond_16

    .line 423
    .line 424
    const-string p1, "key_chose_file_path_info"

    .line 425
    .line 426
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 427
    .line 428
    .line 429
    move-result-object p1

    .line 430
    check-cast p1, Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 431
    .line 432
    if-eqz p1, :cond_16

    .line 433
    .line 434
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOO〇o〇0O(Lcom/intsig/camscanner/datastruct/FolderDocInfo;)V

    .line 435
    .line 436
    .line 437
    goto/16 :goto_5

    .line 438
    .line 439
    :cond_f
    const/16 v0, 0x6b

    .line 440
    .line 441
    if-ne p1, v0, :cond_10

    .line 442
    .line 443
    if-eqz p3, :cond_16

    .line 444
    .line 445
    if-ne p2, v5, :cond_16

    .line 446
    .line 447
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 448
    .line 449
    .line 450
    goto :goto_5

    .line 451
    :cond_10
    const/16 v0, 0x6c

    .line 452
    .line 453
    if-ne p1, v0, :cond_11

    .line 454
    .line 455
    if-eqz p3, :cond_16

    .line 456
    .line 457
    if-ne p2, v5, :cond_16

    .line 458
    .line 459
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 460
    .line 461
    if-eqz p1, :cond_16

    .line 462
    .line 463
    const-string p1, "extra_key_paper_property_result"

    .line 464
    .line 465
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 466
    .line 467
    .line 468
    move-result-object p1

    .line 469
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oo〇8〇8〇8(Ljava/lang/String;)V

    .line 470
    .line 471
    .line 472
    goto :goto_5

    .line 473
    :cond_11
    const/16 v0, 0x6f

    .line 474
    .line 475
    if-ne p1, v0, :cond_12

    .line 476
    .line 477
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oO0o()V

    .line 478
    .line 479
    .line 480
    goto :goto_5

    .line 481
    :cond_12
    const/16 v0, 0x6e

    .line 482
    .line 483
    if-ne p1, v0, :cond_15

    .line 484
    .line 485
    if-ne p2, v5, :cond_14

    .line 486
    .line 487
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇00O0:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 488
    .line 489
    iget-boolean p1, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 490
    .line 491
    if-eqz p1, :cond_13

    .line 492
    .line 493
    const-string p1, "com.intsig.camscanner.NEW_DOC_MULTIPLE"

    .line 494
    .line 495
    goto :goto_4

    .line 496
    :cond_13
    const-string p1, "com.intsig.camscanner.NEW_PAGE_MULTIPLE"

    .line 497
    .line 498
    :goto_4
    new-instance p2, Landroid/content/Intent;

    .line 499
    .line 500
    invoke-direct {p2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 501
    .line 502
    .line 503
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 504
    .line 505
    invoke-virtual {p1, v5, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 506
    .line 507
    .line 508
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 509
    .line 510
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 511
    .line 512
    .line 513
    goto :goto_5

    .line 514
    :cond_14
    if-eqz p3, :cond_16

    .line 515
    .line 516
    const-class p1, Lcom/intsig/camscanner/tools/OCRDataListHolder;

    .line 517
    .line 518
    invoke-static {p1}, Lcom/intsig/singleton/Singleton;->〇080(Ljava/lang/Class;)Lcom/intsig/singleton/Singleton;

    .line 519
    .line 520
    .line 521
    move-result-object p1

    .line 522
    check-cast p1, Lcom/intsig/camscanner/tools/OCRDataListHolder;

    .line 523
    .line 524
    invoke-virtual {p1, v4}, Lcom/intsig/camscanner/tools/OCRDataListHolder;->〇o00〇〇Oo(Z)Ljava/util/List;

    .line 525
    .line 526
    .line 527
    move-result-object p1

    .line 528
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 529
    .line 530
    .line 531
    move-result-object p2

    .line 532
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->〇80〇808〇O(Ljava/util/List;)V

    .line 533
    .line 534
    .line 535
    goto :goto_5

    .line 536
    :cond_15
    const/16 p3, 0x70

    .line 537
    .line 538
    if-ne p1, p3, :cond_16

    .line 539
    .line 540
    if-ne p2, v5, :cond_16

    .line 541
    .line 542
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 543
    .line 544
    invoke-virtual {p1, v5}, Landroid/app/Activity;->setResult(I)V

    .line 545
    .line 546
    .line 547
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 548
    .line 549
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 550
    .line 551
    .line 552
    :cond_16
    :goto_5
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onAttach"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onAttach(Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    move-object v0, p1

    .line 12
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8080〇O8o(Landroid/content/Intent;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0〇o8o〇〇()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    .line 35
    .line 36
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇80O8o8O〇:I

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const v1, 0x7f0a09e8

    .line 6
    .line 7
    .line 8
    const-string v2, "last_next_photo"

    .line 9
    .line 10
    const/4 v3, 0x1

    .line 11
    const-string v4, "CSBatchResult"

    .line 12
    .line 13
    if-ne v0, v1, :cond_1

    .line 14
    .line 15
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 16
    .line 17
    const-string v0, "pre page"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-static {v4, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 26
    .line 27
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-lez p1, :cond_0

    .line 32
    .line 33
    sub-int/2addr p1, v3

    .line 34
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OOo0Oo8O(IZ)V

    .line 35
    .line 36
    .line 37
    :cond_0
    return-void

    .line 38
    :cond_1
    const v1, 0x7f0a09a6

    .line 39
    .line 40
    .line 41
    if-ne v0, v1, :cond_3

    .line 42
    .line 43
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 44
    .line 45
    const-string v0, "next page"

    .line 46
    .line 47
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-static {v4, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MyViewPager;

    .line 54
    .line 55
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->getCount()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    sub-int/2addr v0, v3

    .line 66
    if-ge p1, v0, :cond_2

    .line 67
    .line 68
    add-int/2addr p1, v3

    .line 69
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇OOo0Oo8O(IZ)V

    .line 70
    .line 71
    .line 72
    :cond_2
    return-void

    .line 73
    :cond_3
    const v1, 0x7f0a0810

    .line 74
    .line 75
    .line 76
    const-string v2, "CSTestPaper"

    .line 77
    .line 78
    if-ne v0, v1, :cond_4

    .line 79
    .line 80
    const-string p1, "smudge"

    .line 81
    .line 82
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇88()V

    .line 86
    .line 87
    .line 88
    return-void

    .line 89
    :cond_4
    const-wide/16 v5, 0xc8

    .line 90
    .line 91
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇08O〇00〇o:Lcom/intsig/utils/ClickLimit;

    .line 92
    .line 93
    invoke-virtual {v1, p1, v5, v6}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    if-nez p1, :cond_5

    .line 98
    .line 99
    return-void

    .line 100
    :cond_5
    const p1, 0x7f0a0808

    .line 101
    .line 102
    .line 103
    if-ne v0, p1, :cond_6

    .line 104
    .line 105
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 106
    .line 107
    const-string v0, "take next"

    .line 108
    .line 109
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 113
    .line 114
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 115
    .line 116
    .line 117
    const-string p1, "add_another"

    .line 118
    .line 119
    invoke-static {v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    goto/16 :goto_3

    .line 123
    .line 124
    :cond_6
    const p1, 0x7f0a07f5

    .line 125
    .line 126
    .line 127
    const-string v1, "cs_scan"

    .line 128
    .line 129
    const-string v5, "from_part"

    .line 130
    .line 131
    const-string v6, "bank_statement"

    .line 132
    .line 133
    const-string v7, "from"

    .line 134
    .line 135
    const/4 v8, 0x2

    .line 136
    const/4 v9, 0x0

    .line 137
    if-ne v0, p1, :cond_9

    .line 138
    .line 139
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 140
    .line 141
    const-string v0, "retake"

    .line 142
    .line 143
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 147
    .line 148
    .line 149
    move-result p1

    .line 150
    if-eqz p1, :cond_7

    .line 151
    .line 152
    const-string p1, "replace"

    .line 153
    .line 154
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    goto :goto_0

    .line 158
    :cond_7
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇()Z

    .line 159
    .line 160
    .line 161
    move-result p1

    .line 162
    if-eqz p1, :cond_8

    .line 163
    .line 164
    new-array p1, v8, [Landroid/util/Pair;

    .line 165
    .line 166
    invoke-static {v7, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 167
    .line 168
    .line 169
    move-result-object v2

    .line 170
    aput-object v2, p1, v9

    .line 171
    .line 172
    invoke-static {v5, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 173
    .line 174
    .line 175
    move-result-object v1

    .line 176
    aput-object v1, p1, v3

    .line 177
    .line 178
    invoke-static {v4, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 179
    .line 180
    .line 181
    goto :goto_0

    .line 182
    :cond_8
    invoke-static {v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    :goto_0
    new-instance p1, Lcom/intsig/camscanner/multiimageedit/〇〇8O0〇8;

    .line 186
    .line 187
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/multiimageedit/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;)V

    .line 188
    .line 189
    .line 190
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇O80o8OO(Lcom/intsig/callback/Callback0;)V

    .line 191
    .line 192
    .line 193
    goto/16 :goto_3

    .line 194
    .line 195
    :cond_9
    const p1, 0x7f0a0751

    .line 196
    .line 197
    .line 198
    if-ne v0, p1, :cond_a

    .line 199
    .line 200
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 201
    .line 202
    const-string v0, "turn left"

    .line 203
    .line 204
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o()Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object p1

    .line 211
    const-string v0, "turn_left"

    .line 212
    .line 213
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->ooo〇880()V

    .line 217
    .line 218
    .line 219
    goto/16 :goto_3

    .line 220
    .line 221
    :cond_a
    const p1, 0x7f0a07d2

    .line 222
    .line 223
    .line 224
    if-ne v0, p1, :cond_c

    .line 225
    .line 226
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 227
    .line 228
    const-string v0, "filter"

    .line 229
    .line 230
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    .line 232
    .line 233
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇()Z

    .line 234
    .line 235
    .line 236
    move-result p1

    .line 237
    if-eqz p1, :cond_b

    .line 238
    .line 239
    new-array p1, v8, [Landroid/util/Pair;

    .line 240
    .line 241
    invoke-static {v7, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 242
    .line 243
    .line 244
    move-result-object v2

    .line 245
    aput-object v2, p1, v9

    .line 246
    .line 247
    invoke-static {v5, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 248
    .line 249
    .line 250
    move-result-object v1

    .line 251
    aput-object v1, p1, v3

    .line 252
    .line 253
    invoke-static {v4, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 254
    .line 255
    .line 256
    goto :goto_1

    .line 257
    :cond_b
    invoke-static {v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    .line 259
    .line 260
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00oooo()V

    .line 261
    .line 262
    .line 263
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O〇o0O0OO0()V

    .line 264
    .line 265
    .line 266
    goto/16 :goto_3

    .line 267
    .line 268
    :cond_c
    const p1, 0x7f0a07ca

    .line 269
    .line 270
    .line 271
    if-ne v0, p1, :cond_f

    .line 272
    .line 273
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 274
    .line 275
    const-string v0, "adjust"

    .line 276
    .line 277
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    .line 279
    .line 280
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 281
    .line 282
    .line 283
    move-result p1

    .line 284
    if-eqz p1, :cond_d

    .line 285
    .line 286
    const-string p1, "cut"

    .line 287
    .line 288
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    .line 290
    .line 291
    goto :goto_2

    .line 292
    :cond_d
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇()Z

    .line 293
    .line 294
    .line 295
    move-result p1

    .line 296
    const-string v0, "crop"

    .line 297
    .line 298
    if-eqz p1, :cond_e

    .line 299
    .line 300
    new-array p1, v8, [Landroid/util/Pair;

    .line 301
    .line 302
    invoke-static {v7, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 303
    .line 304
    .line 305
    move-result-object v2

    .line 306
    aput-object v2, p1, v9

    .line 307
    .line 308
    invoke-static {v5, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 309
    .line 310
    .line 311
    move-result-object v1

    .line 312
    aput-object v1, p1, v3

    .line 313
    .line 314
    invoke-static {v4, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 315
    .line 316
    .line 317
    goto :goto_2

    .line 318
    :cond_e
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O8o()Ljava/lang/String;

    .line 319
    .line 320
    .line 321
    move-result-object p1

    .line 322
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    :goto_2
    invoke-static {v9}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0OOoO(Z)V

    .line 326
    .line 327
    .line 328
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o00oooo()V

    .line 329
    .line 330
    .line 331
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OooO〇080()V

    .line 332
    .line 333
    .line 334
    goto/16 :goto_3

    .line 335
    .line 336
    :cond_f
    const p1, 0x7f0a1a47

    .line 337
    .line 338
    .line 339
    if-ne v0, p1, :cond_11

    .line 340
    .line 341
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 342
    .line 343
    const-string v1, "scan_finish"

    .line 344
    .line 345
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    .line 347
    .line 348
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 349
    .line 350
    if-eqz v1, :cond_10

    .line 351
    .line 352
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->O8()Z

    .line 353
    .line 354
    .line 355
    move-result v1

    .line 356
    if-eqz v1, :cond_10

    .line 357
    .line 358
    new-instance p1, Ljava/lang/StringBuilder;

    .line 359
    .line 360
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 361
    .line 362
    .line 363
    const-string v1, "scan_finish intercept by:"

    .line 364
    .line 365
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    .line 367
    .line 368
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 369
    .line 370
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 371
    .line 372
    .line 373
    const-string v1, ", saveResult --> discardAllData"

    .line 374
    .line 375
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    .line 377
    .line 378
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 379
    .line 380
    .line 381
    move-result-object p1

    .line 382
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    .line 384
    .line 385
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 386
    .line 387
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8〇o(Z)V

    .line 388
    .line 389
    .line 390
    return-void

    .line 391
    :cond_10
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08o(I)V

    .line 392
    .line 393
    .line 394
    goto/16 :goto_3

    .line 395
    .line 396
    :cond_11
    const p1, 0x7f0a05c8

    .line 397
    .line 398
    .line 399
    if-ne v0, p1, :cond_12

    .line 400
    .line 401
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 402
    .line 403
    const-string v0, "exit_enhance"

    .line 404
    .line 405
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    .line 407
    .line 408
    const-string p1, "filter_save"

    .line 409
    .line 410
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o088〇〇()Lorg/json/JSONObject;

    .line 411
    .line 412
    .line 413
    move-result-object v0

    .line 414
    invoke-static {v4, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 415
    .line 416
    .line 417
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OOOo〇()V

    .line 418
    .line 419
    .line 420
    goto :goto_3

    .line 421
    :cond_12
    const p1, 0x7f0a0afb

    .line 422
    .line 423
    .line 424
    if-ne v0, p1, :cond_13

    .line 425
    .line 426
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 427
    .line 428
    const-string v0, "click ocr lang"

    .line 429
    .line 430
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    .line 432
    .line 433
    const/16 p1, 0x6d

    .line 434
    .line 435
    invoke-static {p0, v3, p1}, Lcom/intsig/camscanner/ocrapi/OcrIntent;->o〇0(Landroidx/fragment/app/Fragment;II)V

    .line 436
    .line 437
    .line 438
    goto :goto_3

    .line 439
    :cond_13
    const p1, 0x7f0a0754

    .line 440
    .line 441
    .line 442
    if-ne v0, p1, :cond_14

    .line 443
    .line 444
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 445
    .line 446
    const-string v0, "click scan_turn_right"

    .line 447
    .line 448
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    .line 450
    .line 451
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO0O8()V

    .line 452
    .line 453
    .line 454
    goto :goto_3

    .line 455
    :cond_14
    const p1, 0x7f0a0800

    .line 456
    .line 457
    .line 458
    if-ne v0, p1, :cond_15

    .line 459
    .line 460
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 461
    .line 462
    const-string v1, "click signature"

    .line 463
    .line 464
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    .line 466
    .line 467
    const-string p1, "signature_icon"

    .line 468
    .line 469
    invoke-static {v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    .line 471
    .line 472
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08o(I)V

    .line 473
    .line 474
    .line 475
    goto :goto_3

    .line 476
    :cond_15
    const p1, 0x7f0a07e7

    .line 477
    .line 478
    .line 479
    if-ne v0, p1, :cond_16

    .line 480
    .line 481
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 482
    .line 483
    const-string v1, "click ocr"

    .line 484
    .line 485
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    .line 487
    .line 488
    const-string p1, "ocr"

    .line 489
    .line 490
    invoke-static {v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    .line 492
    .line 493
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08o(I)V

    .line 494
    .line 495
    .line 496
    goto :goto_3

    .line 497
    :cond_16
    const p1, 0x7f0a160b

    .line 498
    .line 499
    .line 500
    if-ne v0, p1, :cond_17

    .line 501
    .line 502
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 503
    .line 504
    invoke-static {p1, v9}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇o〇(Landroid/content/Context;I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 505
    .line 506
    .line 507
    move-result-object p1

    .line 508
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇8〇〇8〇8(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V

    .line 509
    .line 510
    .line 511
    :cond_17
    :goto_3
    return-void
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇80O8o8O〇:I

    .line 5
    .line 6
    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    .line 7
    .line 8
    if-eq v0, p1, :cond_1

    .line 9
    .line 10
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇80O8o8O〇:I

    .line 11
    .line 12
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇O()V

    .line 20
    .line 21
    .line 22
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-static {p1}, Lcom/intsig/utils/DeviceUtil;->o〇O8〇〇o(Landroid/content/Context;)Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    if-eqz p1, :cond_2

    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 33
    .line 34
    if-eqz p1, :cond_2

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇0()V

    .line 37
    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 42
    .line 43
    .line 44
    :cond_2
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onDestroyView()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->INSTANCE:Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/MultiDirectionDetectCollectManager;->reset()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O8ooOoo〇()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/util/TimeLogger;->〇o〇()V

    .line 5
    .line 6
    .line 7
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇o〇Oo88:Z

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0O8o00()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 5

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->oO80()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 15
    .line 16
    new-instance v1, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v2, "onStart: intercept by actionClient="

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->OO:Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;

    .line 27
    .line 28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o008()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    .line 45
    const-string v0, "CSTestPaper"

    .line 46
    .line 47
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o0〇()Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    const-string v1, "from_part"

    .line 56
    .line 57
    const-string v2, "CSBatchResult"

    .line 58
    .line 59
    if-eqz v0, :cond_2

    .line 60
    .line 61
    const-string v0, "bank_statement"

    .line 62
    .line 63
    const-string v3, "cs_scan"

    .line 64
    .line 65
    const-string v4, "from"

    .line 66
    .line 67
    invoke-static {v2, v4, v0, v1, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 72
    .line 73
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    if-eqz v0, :cond_3

    .line 78
    .line 79
    invoke-static {v2}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇0O〇O00O:Ljava/lang/String;

    .line 84
    .line 85
    invoke-static {v2, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    :goto_0
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public o〇0〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 4
    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 8
    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO:Ljava/lang/String;

    .line 13
    .line 14
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇00O:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 21
    .line 22
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 29
    .line 30
    const-string v0, "F-retryHandleOnePage click, no network"

    .line 31
    .line 32
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o08〇()V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->O0〇O80ooo()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 46
    .line 47
    const-string v0, "F-retryHandleOnePage click, currentListHasError"

    .line 48
    .line 49
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->〇〇o08()V

    .line 53
    .line 54
    .line 55
    return-void

    .line 56
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 57
    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    .line 59
    .line 60
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .line 62
    .line 63
    const-string v2, "F-retryHandleOnePage click for "

    .line 64
    .line 65
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 69
    .line 70
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 71
    .line 72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->oOo〇8o008:Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;

    .line 83
    .line 84
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 85
    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 87
    .line 88
    .line 89
    move-result-wide v1

    .line 90
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 91
    .line 92
    .line 93
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->Oo8oo()V

    .line 94
    .line 95
    .line 96
    return-void

    .line 97
    :cond_3
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oo:Ljava/lang/String;

    .line 98
    .line 99
    const-string v0, "F-retryHandleOnePage click but find null!"

    .line 100
    .line 101
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d030e

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected 〇O〇〇〇(I)Lcom/google/android/material/tabs/TabLayout$Tab;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    const v3, 0x7f0d015c

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const v1, 0x7f0a182a

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    check-cast v1, Landroid/widget/TextView;

    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewFragment;->o〇oO:Lcom/google/android/material/tabs/TabLayout;

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/google/android/material/tabs/TabLayout;->newTab()Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 33
    .line 34
    const v4, 0x7f060268

    .line 35
    .line 36
    .line 37
    invoke-static {v3, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 42
    .line 43
    .line 44
    const/high16 v3, 0x41600000    # 14.0f

    .line 45
    .line 46
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v2, v0}, Lcom/google/android/material/tabs/TabLayout$Tab;->setCustomView(Landroid/view/View;)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 53
    .line 54
    .line 55
    return-object v2
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.class public Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "MultiImageEditViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel$MultiImageEditModelTraverse;
    }
.end annotation


# instance fields
.field private O8o08O8O:Lcom/intsig/callback/Callback0;

.field private OO:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

.field private final o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

.field public o〇00O:Z

.field public 〇08O〇00〇o:Z

.field private 〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lo8O/o〇8〇;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lo8O/o〇8〇;-><init>(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->OO:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇08O〇00〇o:Z

    .line 13
    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇00O:Z

    .line 15
    .line 16
    new-instance v0, Lo8O/〇〇00OO;

    .line 17
    .line 18
    invoke-direct {v0, p0}, Lo8O/〇〇00OO;-><init>(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8o08O8O:Lcom/intsig/callback/Callback0;

    .line 22
    .line 23
    const-class v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 24
    .line 25
    invoke-static {v0}, Lcom/intsig/singleton/Singleton;->〇080(Ljava/lang/Class;)Lcom/intsig/singleton/Singleton;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->OO:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o800o8O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic O8oOo80(Landroid/content/Context;JLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;ZI)V
    .locals 2

    .line 1
    iget-object v0, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    iget-wide v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 4
    .line 5
    invoke-static {p1, p2, p3, v0, v1}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->o〇0(Landroid/content/Context;JJ)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇080()V

    .line 9
    .line 10
    .line 11
    invoke-static {p1, p2, p3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    if-eqz p5, :cond_0

    .line 18
    .line 19
    new-instance p1, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string p2, "discardAllData index="

    .line 25
    .line 26
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const-string p2, "MultiImageEditViewModel"

    .line 37
    .line 38
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    const/4 p1, 0x0

    .line 42
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8〇o(Z)V

    .line 43
    .line 44
    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O〇O〇oO()Landroidx/lifecycle/MutableLiveData;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    iget-object p2, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 50
    .line 51
    invoke-virtual {p1, p2}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private synthetic Oo0oOo〇0()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇00O:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;Landroid/content/Context;JLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;ZI)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p6}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8oOo80(Landroid/content/Context;JLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;ZI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private synthetic o8o〇〇0O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 3

    .line 1
    const-string v0, "MultiImageEditViewModel"

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    const-string v1, "multiImageEditModel == null"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v2, "postValue multiImageEditModel="

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O〇O〇oO()Landroidx/lifecycle/MutableLiveData;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇00(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->〇0000OOO()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->O8ooOoo〇()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 21
    .line 22
    iget p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 23
    .line 24
    invoke-static {v0, p1}, Lcom/intsig/camscanner/demoire/ImageQualityUtil;->〇o00〇〇Oo(Ljava/lang/String;I)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->Oo0oOo〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o8o〇〇0O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O0〇oo(Landroid/content/Context;JZZ)I
    .locals 15

    .line 1
    move-object v1, p0

    .line 2
    move-wide/from16 v8, p2

    .line 3
    .line 4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 5
    .line 6
    .line 7
    move-result-object v10

    .line 8
    iget-object v0, v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8ooOoo〇()Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    new-instance v2, Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .line 18
    .line 19
    new-instance v11, Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    const/4 v12, 0x0

    .line 33
    const-string v4, "MultiImageEditViewModel"

    .line 34
    .line 35
    if-eqz v3, :cond_d

    .line 36
    .line 37
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    check-cast v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 42
    .line 43
    if-nez v3, :cond_0

    .line 44
    .line 45
    const-string v3, "saveResult multiImageEditPage == null"

    .line 46
    .line 47
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    iget-object v5, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 52
    .line 53
    const/4 v6, 0x1

    .line 54
    if-eqz v5, :cond_2

    .line 55
    .line 56
    invoke-virtual {v5}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v5

    .line 60
    invoke-static {v5}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 61
    .line 62
    .line 63
    move-result v5

    .line 64
    if-eqz v5, :cond_2

    .line 65
    .line 66
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 67
    .line 68
    new-instance v5, Ljava/lang/StringBuilder;

    .line 69
    .line 70
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .line 72
    .line 73
    const-string v7, "save model = "

    .line 74
    .line 75
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    iget v7, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 79
    .line 80
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v5

    .line 87
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    iget v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 91
    .line 92
    sget v5, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o00〇〇Oo:I

    .line 93
    .line 94
    if-ne v4, v5, :cond_1

    .line 95
    .line 96
    iget v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 97
    .line 98
    const/4 v5, -0x1

    .line 99
    if-eq v4, v5, :cond_1

    .line 100
    .line 101
    invoke-virtual {v3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v5

    .line 105
    iget-object v7, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 106
    .line 107
    invoke-static {v4, v5, v7}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->o〇O8〇〇o(ILjava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    invoke-static {}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->Oo08()V

    .line 111
    .line 112
    .line 113
    :cond_1
    iput-boolean v6, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇08O〇00〇o:Z

    .line 114
    .line 115
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 116
    .line 117
    invoke-virtual {v4, v3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->ooOO(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 118
    .line 119
    .line 120
    goto :goto_0

    .line 121
    :cond_2
    iget-object v5, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->O8:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 122
    .line 123
    iget-object v7, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 124
    .line 125
    invoke-static {v5, v7}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 126
    .line 127
    .line 128
    move-result v5

    .line 129
    if-eqz v5, :cond_3

    .line 130
    .line 131
    goto :goto_0

    .line 132
    :cond_3
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 133
    .line 134
    iget-boolean v5, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇08O〇00〇o:Z

    .line 135
    .line 136
    if-eqz v5, :cond_4

    .line 137
    .line 138
    iget-boolean v5, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇00O:Z

    .line 139
    .line 140
    if-eqz v5, :cond_4

    .line 141
    .line 142
    goto :goto_0

    .line 143
    :cond_4
    iget-object v5, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o0O:Ljava/lang/String;

    .line 144
    .line 145
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 146
    .line 147
    .line 148
    move-result v5

    .line 149
    if-nez v5, :cond_5

    .line 150
    .line 151
    iget-object v5, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o0O:Ljava/lang/String;

    .line 152
    .line 153
    iput-object v5, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 154
    .line 155
    sget-object v7, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 156
    .line 157
    invoke-static {v7, v5}, Lcom/intsig/camscanner/db/dao/ImageDao;->O〇O〇oO(Landroid/content/Context;Ljava/lang/String;)J

    .line 158
    .line 159
    .line 160
    move-result-wide v13

    .line 161
    iput-wide v13, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 162
    .line 163
    :cond_5
    new-instance v5, Landroid/content/ContentValues;

    .line 164
    .line 165
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 166
    .line 167
    .line 168
    new-instance v7, Ljava/lang/StringBuilder;

    .line 169
    .line 170
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .line 172
    .line 173
    const-string v13, "multiImageEditModel="

    .line 174
    .line 175
    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->toString()Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object v13

    .line 182
    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v7

    .line 189
    invoke-static {v4, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    .line 191
    .line 192
    new-instance v7, Ljava/lang/StringBuilder;

    .line 193
    .line 194
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 195
    .line 196
    .line 197
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v13

    .line 201
    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    iget-object v13, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o0O:Ljava/lang/String;

    .line 205
    .line 206
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 207
    .line 208
    .line 209
    move-result v13

    .line 210
    if-eqz v13, :cond_6

    .line 211
    .line 212
    iget-object v13, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 213
    .line 214
    goto :goto_1

    .line 215
    :cond_6
    iget-object v13, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o0O:Ljava/lang/String;

    .line 216
    .line 217
    :goto_1
    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    const-string v13, ".jpg"

    .line 221
    .line 222
    invoke-virtual {v7, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    .line 224
    .line 225
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v7

    .line 229
    if-eqz p4, :cond_8

    .line 230
    .line 231
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 232
    .line 233
    .line 234
    move-result-object v13

    .line 235
    iget-object v14, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 236
    .line 237
    invoke-virtual {v13, v14}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->O8(Ljava/lang/String;)Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 238
    .line 239
    .line 240
    move-result-object v13

    .line 241
    if-nez v13, :cond_7

    .line 242
    .line 243
    const-string v13, "ocrData == null"

    .line 244
    .line 245
    invoke-static {v4, v13}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    .line 247
    .line 248
    goto :goto_2

    .line 249
    :cond_7
    invoke-virtual {v13, v12}, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo〇(Z)V

    .line 250
    .line 251
    .line 252
    iput-object v7, v13, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 253
    .line 254
    :cond_8
    :goto_2
    iget-object v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 255
    .line 256
    invoke-static {v7, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 257
    .line 258
    .line 259
    move-result v4

    .line 260
    if-nez v4, :cond_9

    .line 261
    .line 262
    iget-object v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 263
    .line 264
    invoke-static {v4, v7}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 265
    .line 266
    .line 267
    :cond_9
    const-string v4, "image_confirm_state"

    .line 268
    .line 269
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 270
    .line 271
    .line 272
    move-result-object v13

    .line 273
    invoke-virtual {v5, v4, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 274
    .line 275
    .line 276
    const-string v4, "cache_state"

    .line 277
    .line 278
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 279
    .line 280
    .line 281
    move-result-object v13

    .line 282
    invoke-virtual {v5, v4, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 283
    .line 284
    .line 285
    iget-object v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o0O:Ljava/lang/String;

    .line 286
    .line 287
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 288
    .line 289
    .line 290
    move-result v4

    .line 291
    const-string v13, "status"

    .line 292
    .line 293
    if-nez v4, :cond_b

    .line 294
    .line 295
    if-eqz p5, :cond_a

    .line 296
    .line 297
    goto :goto_3

    .line 298
    :cond_a
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 299
    .line 300
    .line 301
    move-result-object v4

    .line 302
    invoke-virtual {v5, v13, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 303
    .line 304
    .line 305
    goto :goto_4

    .line 306
    :cond_b
    :goto_3
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 307
    .line 308
    .line 309
    move-result-object v4

    .line 310
    invoke-virtual {v5, v13, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 311
    .line 312
    .line 313
    :goto_4
    const-string v4, "raw_data"

    .line 314
    .line 315
    invoke-virtual {v5, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    .line 317
    .line 318
    iget v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 319
    .line 320
    invoke-static {v4}, Lcom/intsig/camscanner/app/DBUtil;->oo〇(I)I

    .line 321
    .line 322
    .line 323
    move-result v4

    .line 324
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 325
    .line 326
    .line 327
    move-result-object v4

    .line 328
    const-string v12, "enhance_mode"

    .line 329
    .line 330
    invoke-virtual {v5, v12, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 331
    .line 332
    .line 333
    invoke-static {v7, v6}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 334
    .line 335
    .line 336
    move-result-object v4

    .line 337
    iget-object v6, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 338
    .line 339
    if-nez v6, :cond_c

    .line 340
    .line 341
    invoke-static {v4}, Lcom/intsig/camscanner/app/DBUtil;->〇08O8o〇0([I)[I

    .line 342
    .line 343
    .line 344
    move-result-object v6

    .line 345
    :cond_c
    iget v7, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 346
    .line 347
    invoke-static {v4, v4, v6, v7}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 348
    .line 349
    .line 350
    move-result-object v4

    .line 351
    const-string v6, "image_border"

    .line 352
    .line 353
    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    .line 355
    .line 356
    iget v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 357
    .line 358
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 359
    .line 360
    .line 361
    move-result-object v4

    .line 362
    const-string v6, "image_rotation"

    .line 363
    .line 364
    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 365
    .line 366
    .line 367
    iget v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇08O:I

    .line 368
    .line 369
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 370
    .line 371
    .line 372
    move-result-object v4

    .line 373
    const-string v6, "contrast_index"

    .line 374
    .line 375
    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 376
    .line 377
    .line 378
    iget v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O0O:I

    .line 379
    .line 380
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 381
    .line 382
    .line 383
    move-result-object v4

    .line 384
    const-string v6, "bright_index"

    .line 385
    .line 386
    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 387
    .line 388
    .line 389
    iget v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8oOOo:I

    .line 390
    .line 391
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 392
    .line 393
    .line 394
    move-result-object v4

    .line 395
    const-string v6, "detail_index"

    .line 396
    .line 397
    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 398
    .line 399
    .line 400
    invoke-virtual {v3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o〇0()Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 401
    .line 402
    .line 403
    move-result-object v4

    .line 404
    invoke-virtual {v4}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;->〇O888o0o()I

    .line 405
    .line 406
    .line 407
    move-result v4

    .line 408
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 409
    .line 410
    .line 411
    move-result-object v4

    .line 412
    const-string v6, "image_quality_status"

    .line 413
    .line 414
    invoke-virtual {v5, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 415
    .line 416
    .line 417
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇00(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 418
    .line 419
    .line 420
    const-string v4, "2"

    .line 421
    .line 422
    const-string v6, "5"

    .line 423
    .line 424
    filled-new-array {v4, v6}, [Ljava/lang/String;

    .line 425
    .line 426
    .line 427
    move-result-object v4

    .line 428
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 429
    .line 430
    iget-wide v12, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 431
    .line 432
    invoke-static {v6, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 433
    .line 434
    .line 435
    move-result-object v6

    .line 436
    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 437
    .line 438
    .line 439
    move-result-object v6

    .line 440
    invoke-virtual {v6, v5}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 441
    .line 442
    .line 443
    move-result-object v5

    .line 444
    const-string v6, "sync_jpage_state != ?  AND sync_jpage_state != ?"

    .line 445
    .line 446
    invoke-virtual {v5, v6, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 447
    .line 448
    .line 449
    move-result-object v4

    .line 450
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 451
    .line 452
    .line 453
    move-result-object v4

    .line 454
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    .line 456
    .line 457
    iget-wide v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 458
    .line 459
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 460
    .line 461
    .line 462
    move-result-object v3

    .line 463
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    .line 465
    .line 466
    goto/16 :goto_0

    .line 467
    .line 468
    :cond_d
    invoke-static {v10, v8, v9}, Lcom/intsig/camscanner/db/dao/ImageDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)V

    .line 469
    .line 470
    .line 471
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 472
    .line 473
    .line 474
    move-result v0

    .line 475
    if-lez v0, :cond_e

    .line 476
    .line 477
    :try_start_0
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 478
    .line 479
    .line 480
    move-result-object v0

    .line 481
    sget-object v3, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 482
    .line 483
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    .line 485
    .line 486
    goto :goto_5

    .line 487
    :catch_0
    move-exception v0

    .line 488
    const-string v2, "Exception "

    .line 489
    .line 490
    invoke-static {v4, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 491
    .line 492
    .line 493
    :goto_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 494
    .line 495
    .line 496
    move-result-wide v2

    .line 497
    const/4 v0, 0x3

    .line 498
    invoke-static {v10, v11, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO〇00〇8oO(Landroid/content/Context;Ljava/util/List;I)V

    .line 499
    .line 500
    .line 501
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 502
    .line 503
    .line 504
    move-result-wide v5

    .line 505
    invoke-static {v10, v11, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O8o08O8O(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 506
    .line 507
    .line 508
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 509
    .line 510
    .line 511
    move-result-wide v13

    .line 512
    new-instance v0, Ljava/lang/StringBuilder;

    .line 513
    .line 514
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 515
    .line 516
    .line 517
    const-string v7, " saveChange SyncUtil.updatePageSyncStat time="

    .line 518
    .line 519
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    .line 521
    .line 522
    sub-long v2, v5, v2

    .line 523
    .line 524
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 525
    .line 526
    .line 527
    const-string v2, " SyncUtil.updateJpagePageSyncStat time="

    .line 528
    .line 529
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    .line 531
    .line 532
    sub-long/2addr v13, v5

    .line 533
    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 534
    .line 535
    .line 536
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 537
    .line 538
    .line 539
    move-result-object v0

    .line 540
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    .line 542
    .line 543
    invoke-static {v10, v8, v9}, Lcom/intsig/camscanner/app/DBUtil;->Ooo8(Landroid/content/Context;J)V

    .line 544
    .line 545
    .line 546
    const/4 v5, 0x3

    .line 547
    const/4 v6, 0x1

    .line 548
    const/4 v7, 0x0

    .line 549
    move-object v2, v10

    .line 550
    move-wide/from16 v3, p2

    .line 551
    .line 552
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 553
    .line 554
    .line 555
    :cond_e
    invoke-static {v10, v8, v9}, Lcom/intsig/camscanner/db/dao/ImageDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)V

    .line 556
    .line 557
    .line 558
    invoke-static {v10, v8, v9}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 559
    .line 560
    .line 561
    if-eqz p5, :cond_f

    .line 562
    .line 563
    iput-boolean v12, v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇00O:Z

    .line 564
    .line 565
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 566
    .line 567
    .line 568
    move-result-object v0

    .line 569
    iget-object v2, v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->O8o08O8O:Lcom/intsig/callback/Callback0;

    .line 570
    .line 571
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇08O8o〇0(Lcom/intsig/callback/Callback0;)V

    .line 572
    .line 573
    .line 574
    :cond_f
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 575
    .line 576
    .line 577
    move-result-object v0

    .line 578
    invoke-virtual {v0, v8, v9}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O〇O〇oO(J)Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 579
    .line 580
    .line 581
    move-result-object v0

    .line 582
    invoke-virtual {v0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0OOo〇0()V

    .line 583
    .line 584
    .line 585
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    .line 586
    .line 587
    .line 588
    move-result v0

    .line 589
    return v0
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method public O8ooOoo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0O〇Oo(I)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8〇o(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Ooo8〇〇()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O(Z)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O〇8oOo8O(Ljava/lang/String;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
    .locals 4

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return-object v1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8ooOoo〇()Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_2

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 30
    .line 31
    iget-object v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 32
    .line 33
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 34
    .line 35
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-eqz v3, :cond_1

    .line 40
    .line 41
    return-object v2

    .line 42
    :cond_2
    return-object v1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O〇O〇oO()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 2

    .line 1
    iput-wide p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 4
    .line 5
    const-wide/16 v0, 0x0

    .line 6
    .line 7
    invoke-virtual {p2, p1, v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o0O0O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o08oOO(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel$MultiImageEditModelTraverse;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8ooOoo〇()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_2

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 23
    .line 24
    if-nez v2, :cond_1

    .line 25
    .line 26
    const-string v2, "MultiImageEditViewModel"

    .line 27
    .line 28
    const-string v3, "traverseMultiImageEditPage multiImageEditPage == null"

    .line 29
    .line 30
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    if-eqz p1, :cond_0

    .line 35
    .line 36
    add-int/lit8 v3, v1, 0x1

    .line 37
    .line 38
    invoke-interface {p1, v2, v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel$MultiImageEditModelTraverse;->〇080(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;I)V

    .line 39
    .line 40
    .line 41
    move v1, v3

    .line 42
    goto :goto_0

    .line 43
    :cond_2
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o0ooO()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇oo〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O〇8O8〇008()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO8008O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)Ljava/lang/Boolean;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->Oo〇O(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$HandleListener;)Ljava/lang/Boolean;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO8o(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 2

    .line 1
    iput-wide p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 2
    .line 3
    iget p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 4
    .line 5
    const/16 p3, -0xc

    .line 6
    .line 7
    if-ne p2, p3, :cond_0

    .line 8
    .line 9
    const/4 p2, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p2, 0x0

    .line 12
    :goto_0
    iput-boolean p2, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇08O〇00〇o:Z

    .line 13
    .line 14
    iget-object p3, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 15
    .line 16
    const-wide/16 v0, 0x0

    .line 17
    .line 18
    invoke-virtual {p3, p1, p2, v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o88〇OO08〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ZJ)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onCleared()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 2
    .line 3
    .line 4
    const-string v0, "MultiImageEditViewModel"

    .line 5
    .line 6
    const-string v1, "onCleared"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->OO:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o0O0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V

    .line 16
    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const/4 v1, 0x0

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇08O8o〇0(Lcom/intsig/callback/Callback0;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oo0O〇0〇〇〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 2

    .line 1
    iput-wide p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 4
    .line 5
    const-wide/16 v0, 0x0

    .line 6
    .line 7
    invoke-virtual {p2, p1, v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇〇o8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oo88o8O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OoO8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0000OOO()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p2, :cond_0

    .line 13
    .line 14
    if-ltz p1, :cond_1

    .line 15
    .line 16
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇〇0〇()I

    .line 19
    .line 20
    .line 21
    move-result p2

    .line 22
    add-int/lit8 p2, p2, -0x1

    .line 23
    .line 24
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OOO8o〇〇(I)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public ooo8o〇o〇(Landroid/content/Context;JLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;ZZ)V
    .locals 9

    .line 1
    iget-object p6, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {p6}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8ooOoo〇()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object p6

    .line 7
    invoke-interface {p6, p4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 8
    .line 9
    .line 10
    move-result v7

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "removeOnePage index="

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v1, "MultiImageEditViewModel"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    if-gez v7, :cond_0

    .line 34
    .line 35
    return-void

    .line 36
    :cond_0
    invoke-interface {p6, p4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 40
    .line 41
    .line 42
    move-result-object p6

    .line 43
    new-instance v8, Lo8O/〇0OO8;

    .line 44
    .line 45
    move-object v0, v8

    .line 46
    move-object v1, p0

    .line 47
    move-object v2, p1

    .line 48
    move-wide v3, p2

    .line 49
    move-object v5, p4

    .line 50
    move v6, p5

    .line 51
    invoke-direct/range {v0 .. v7}, Lo8O/〇0OO8;-><init>(Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;Landroid/content/Context;JLcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;ZI)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p6, v8}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public oo〇(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8ooOoo〇()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    if-ge v2, v3, :cond_2

    .line 14
    .line 15
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    check-cast v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 20
    .line 21
    if-nez v3, :cond_0

    .line 22
    .line 23
    const-string v3, "MultiImageEditViewModel"

    .line 24
    .line 25
    const-string v4, "getPosition multiImageEditPage == null"

    .line 26
    .line 27
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_0
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 32
    .line 33
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 34
    .line 35
    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-eqz v3, :cond_1

    .line 40
    .line 41
    const/4 p1, 0x1

    .line 42
    return p1

    .line 43
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    return v1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o〇0OOo〇0()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇〇0〇()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇8oOO88()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8ooOoo〇()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇O(I)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8ooOoo〇()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-ltz p1, :cond_1

    .line 8
    .line 9
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-lt p1, v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    check-cast p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 21
    .line 22
    return-object p1

    .line 23
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o〇〇0〇88(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o0O0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇008〇oo(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 2

    .line 1
    iput-wide p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 2
    .line 3
    const/4 p2, -0x1

    .line 4
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OoO8(I)V

    .line 5
    .line 6
    .line 7
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 8
    .line 9
    const-wide/16 v0, 0x0

    .line 10
    .line 11
    invoke-virtual {p2, p1, v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->O8O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8o8O〇O()Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o〇8oOO88()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x0

    .line 8
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v4

    .line 12
    if-ge v2, v4, :cond_2

    .line 13
    .line 14
    if-nez v2, :cond_0

    .line 15
    .line 16
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    check-cast v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 21
    .line 22
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 23
    .line 24
    iget v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    check-cast v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 32
    .line 33
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 34
    .line 35
    iget v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 36
    .line 37
    if-eq v3, v4, :cond_1

    .line 38
    .line 39
    return v1

    .line 40
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    const/4 v0, 0x1

    .line 44
    return v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇8o〇〇8080()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0000OOO()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    :cond_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O00(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->oo88o8O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇OO8Oo0〇(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->OOO8o〇〇(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇oo(Landroid/app/Activity;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iget v0, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 10
    .line 11
    iget p1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 12
    .line 13
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-gtz p1, :cond_0

    .line 18
    .line 19
    const/16 p1, 0x438

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/16 v0, 0x960

    .line 23
    .line 24
    if-le p1, v0, :cond_1

    .line 25
    .line 26
    const/16 p1, 0x960

    .line 27
    .line 28
    :cond_1
    :goto_0
    int-to-double v0, p1

    .line 29
    const-wide v2, 0x3fe999999999999aL    # 0.8

    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    mul-double v0, v0, v2

    .line 35
    .line 36
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 37
    .line 38
    mul-double v0, v0, v2

    .line 39
    .line 40
    double-to-int p1, v0

    .line 41
    const/16 v0, 0xb40

    .line 42
    .line 43
    if-le p1, v0, :cond_2

    .line 44
    .line 45
    const/16 p1, 0xb40

    .line 46
    .line 47
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 48
    .line 49
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0O〇Oo(I)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇oo〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o800o8O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇0〇0o8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 6
    .line 7
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isSuperFilterMode(I)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->〇08O〇00〇o:Z

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 16
    .line 17
    invoke-virtual {v1, p1, v0, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o88〇OO08〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ZJ)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇〇0〇〇0()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o〇O8〇〇o()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

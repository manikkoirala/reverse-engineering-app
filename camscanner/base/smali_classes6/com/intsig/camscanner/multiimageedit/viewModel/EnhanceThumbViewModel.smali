.class public Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "EnhanceThumbViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$GenerateCallBack;
    }
.end annotation


# instance fields
.field private OO:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private o0:Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

.field private o〇00O:Landroid/os/Handler;

.field private 〇08O〇00〇o:Landroid/os/HandlerThread;

.field private 〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private Oo0oOo〇0(I)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    const/16 v0, 0x8

    .line 4
    .line 5
    if-eq p1, v0, :cond_1

    .line 6
    .line 7
    const/16 v0, 0x9

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 15
    :goto_1
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;Landroidx/fragment/app/FragmentActivity;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇oo(Landroidx/fragment/app/FragmentActivity;Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static synthetic O〇8oOo8O(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->〇80〇808〇O(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0ooO()Landroid/util/LruCache;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->OO:Landroid/util/LruCache;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇o〇(Landroid/content/Context;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/high16 v1, 0x1000000

    .line 14
    .line 15
    if-le v0, v1, :cond_0

    .line 16
    .line 17
    const/high16 v0, 0x1000000

    .line 18
    .line 19
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$1;

    .line 20
    .line 21
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$1;-><init>(Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;I)V

    .line 22
    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->OO:Landroid/util/LruCache;

    .line 25
    .line 26
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->OO:Landroid/util/LruCache;

    .line 27
    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o8o〇〇0O()V
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :catch_0
    move-exception v0

    .line 11
    const-string v1, "EnhanceThumbViewModel"

    .line 12
    .line 13
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    :goto_0
    if-nez v0, :cond_0

    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇080()V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oO(Ljava/util/List;I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;",
            ">;I)",
            "Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 16
    .line 17
    iget v1, v0, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 18
    .line 19
    if-ne v1, p2, :cond_0

    .line 20
    .line 21
    return-object v0

    .line 22
    :cond_1
    const/4 p1, 0x0

    .line 23
    return-object p1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oo88o8O(Landroidx/fragment/app/FragmentActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇8o〇〇8080(Landroidx/fragment/app/FragmentActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;",
            ">;",
            "Landroid/graphics/Bitmap;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p2, p4}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oO(Ljava/util/List;I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    if-nez p2, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o0ooO()Landroid/util/LruCache;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    new-instance v1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p5

    .line 27
    invoke-virtual {v0, p5}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    check-cast v1, Landroid/graphics/Bitmap;

    .line 32
    .line 33
    if-nez v1, :cond_3

    .line 34
    .line 35
    invoke-static {p3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 36
    .line 37
    .line 38
    move-result-object p3

    .line 39
    const/16 v1, 0x6e

    .line 40
    .line 41
    if-ne p4, v1, :cond_1

    .line 42
    .line 43
    invoke-static {p3}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->〇80〇808〇O(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    move-object v1, p1

    .line 48
    goto :goto_0

    .line 49
    :cond_1
    invoke-direct {p0, p4}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->Oo0oOo〇0(I)Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-nez v1, :cond_2

    .line 54
    .line 55
    invoke-static {p4}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceMode(I)I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    invoke-static {p1, p3, v1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->enhanceImage(ILandroid/graphics/Bitmap;I)Z

    .line 60
    .line 61
    .line 62
    :cond_2
    move-object v1, p3

    .line 63
    :goto_0
    invoke-virtual {v0, p5, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    :cond_3
    invoke-static {v1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    iput-object p1, p2, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->Oo08:Landroid/graphics/Bitmap;

    .line 71
    .line 72
    new-instance p1, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    const-string p2, "enhanceBitmap targetEnhanceIndex="

    .line 78
    .line 79
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    const-string p2, "EnhanceThumbViewModel"

    .line 90
    .line 91
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private static synthetic o〇8oOO88(Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)Landroid/graphics/Bitmap;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->OO:I

    .line 4
    .line 5
    iget p0, p0, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇08O〇00〇o:I

    .line 6
    .line 7
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 8
    .line 9
    const/4 v3, 0x1

    .line 10
    invoke-static {v0, v1, p0, v2, v3}, Lcom/intsig/utils/ImageUtil;->oo88o8O(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    return-object p0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static synthetic o〇O(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)Landroid/graphics/Bitmap;
    .locals 3

    .line 1
    iget v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->OO:I

    .line 2
    .line 3
    iget p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇08O〇00〇o:I

    .line 4
    .line 5
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 6
    .line 7
    const/4 v2, 0x1

    .line 8
    invoke-static {p0, v0, p1, v1, v2}, Lcom/intsig/utils/ImageUtil;->oo88o8O(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    return-object p0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇8oOO88(Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)Landroid/graphics/Bitmap;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8o8o〇(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇O(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)Landroid/graphics/Bitmap;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic 〇8o〇〇8080(Landroidx/fragment/app/FragmentActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/capture/writeboard/PadLocalModelEnhance;->OO0o〇〇〇〇0(Landroid/app/Activity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O00(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O〇8oOo8O(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇oo(Landroidx/fragment/app/FragmentActivity;Landroid/os/Message;)Z
    .locals 3

    .line 1
    iget-object p2, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2
    .line 3
    instance-of v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

    .line 4
    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    check-cast p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

    .line 8
    .line 9
    iget-object v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->o〇00O:Ljava/lang/String;

    .line 10
    .line 11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/4 v1, 0x1

    .line 16
    if-nez v0, :cond_2

    .line 17
    .line 18
    iget-object v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->o〇00O:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    const-string v2, "EnhanceThumbViewModel"

    .line 25
    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    new-instance p1, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v0, "startGenerateThumb not exist padPath="

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    iget-object p2, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->o〇00O:Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    return v1

    .line 51
    :cond_0
    iget-object v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇OOo8〇0:Ljava/lang/String;

    .line 52
    .line 53
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-nez v0, :cond_1

    .line 58
    .line 59
    new-instance p1, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v0, "startGenerateThumb not exist imagePath="

    .line 65
    .line 66
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    iget-object p2, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇OOo8〇0:Ljava/lang/String;

    .line 70
    .line 71
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    return v1

    .line 82
    :cond_1
    iget-object v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->o〇00O:Ljava/lang/String;

    .line 83
    .line 84
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇00(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_2
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇oo〇(Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)V

    .line 89
    .line 90
    .line 91
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

    .line 96
    .line 97
    invoke-virtual {p1, p2}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 98
    .line 99
    .line 100
    return v1

    .line 101
    :cond_3
    const/4 p1, 0x0

    .line 102
    return p1
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇oo〇(Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)V
    .locals 12

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const-string v2, "EnhanceThumbViewModel"

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string p1, "imageBound == null"

    .line 13
    .line 14
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    const/4 v3, 0x0

    .line 19
    aget v3, v0, v3

    .line 20
    .line 21
    aget v0, v0, v1

    .line 22
    .line 23
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-gez v0, :cond_1

    .line 28
    .line 29
    new-instance p1, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v1, "minImageSide = "

    .line 35
    .line 36
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 51
    .line 52
    .line 53
    move-result-wide v3

    .line 54
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o0ooO()Landroid/util/LruCache;

    .line 59
    .line 60
    .line 61
    move-result-object v5

    .line 62
    iget-object v6, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇OOo8〇0:Ljava/lang/String;

    .line 63
    .line 64
    const-string v7, "ori"

    .line 65
    .line 66
    invoke-direct {p0, v6, v7}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇〇〇0〇〇0(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v11

    .line 70
    invoke-virtual {v5, v11}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object v6

    .line 74
    check-cast v6, Landroid/graphics/Bitmap;

    .line 75
    .line 76
    if-nez v6, :cond_4

    .line 77
    .line 78
    iget-object v6, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇OOo8〇0:Ljava/lang/String;

    .line 79
    .line 80
    iget v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->OO:I

    .line 81
    .line 82
    iget v8, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇08O〇00〇o:I

    .line 83
    .line 84
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->O8〇o()Landroid/graphics/Bitmap$Config;

    .line 85
    .line 86
    .line 87
    move-result-object v9

    .line 88
    invoke-static {v6, v7, v8, v9, v1}, Lcom/intsig/utils/ImageUtil;->oo88o8O(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 89
    .line 90
    .line 91
    move-result-object v6

    .line 92
    if-nez v6, :cond_2

    .line 93
    .line 94
    const-string v6, "cacheOriBitmap == null\uff0c try Bitmap.Config.RGB_565"

    .line 95
    .line 96
    invoke-static {v2, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    iget-object v6, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇OOo8〇0:Ljava/lang/String;

    .line 100
    .line 101
    iget v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->OO:I

    .line 102
    .line 103
    int-to-float v7, v7

    .line 104
    const v8, 0x3f4ccccd    # 0.8f

    .line 105
    .line 106
    .line 107
    mul-float v7, v7, v8

    .line 108
    .line 109
    float-to-int v7, v7

    .line 110
    iget v9, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇08O〇00〇o:I

    .line 111
    .line 112
    int-to-float v9, v9

    .line 113
    mul-float v9, v9, v8

    .line 114
    .line 115
    float-to-int v8, v9

    .line 116
    sget-object v9, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 117
    .line 118
    invoke-static {v6, v7, v8, v9, v1}, Lcom/intsig/utils/ImageUtil;->oo88o8O(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 119
    .line 120
    .line 121
    move-result-object v6

    .line 122
    :cond_2
    if-nez v6, :cond_3

    .line 123
    .line 124
    const-string p1, "cacheOriBitmap == null"

    .line 125
    .line 126
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    return-void

    .line 130
    :cond_3
    invoke-virtual {v5, v11, v6}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    :cond_4
    move-object v1, v6

    .line 134
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 135
    .line 136
    const/4 v9, 0x0

    .line 137
    move-object v5, p0

    .line 138
    move v6, v0

    .line 139
    move-object v8, v1

    .line 140
    move-object v10, v11

    .line 141
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 142
    .line 143
    .line 144
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 145
    .line 146
    const/4 v9, 0x1

    .line 147
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 148
    .line 149
    .line 150
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 151
    .line 152
    const/4 v9, 0x2

    .line 153
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 154
    .line 155
    .line 156
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 157
    .line 158
    const/4 v9, 0x3

    .line 159
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 160
    .line 161
    .line 162
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 163
    .line 164
    const/4 v9, 0x4

    .line 165
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 166
    .line 167
    .line 168
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 169
    .line 170
    const/4 v9, 0x5

    .line 171
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 172
    .line 173
    .line 174
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 175
    .line 176
    const/4 v9, 0x6

    .line 177
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 178
    .line 179
    .line 180
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 181
    .line 182
    const/16 v9, 0x8

    .line 183
    .line 184
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 185
    .line 186
    .line 187
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 188
    .line 189
    const/16 v9, 0x9

    .line 190
    .line 191
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 192
    .line 193
    .line 194
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 195
    .line 196
    const/16 v9, 0x6e

    .line 197
    .line 198
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 199
    .line 200
    .line 201
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 202
    .line 203
    const/4 v9, 0x7

    .line 204
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 205
    .line 206
    .line 207
    iget-object v7, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 208
    .line 209
    const/16 v9, 0x7c

    .line 210
    .line 211
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->oo〇(ILjava/util/List;Landroid/graphics/Bitmap;ILjava/lang/String;)V

    .line 212
    .line 213
    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    .line 215
    .line 216
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 217
    .line 218
    .line 219
    const-string v5, "createEnhanceThumb costTime="

    .line 220
    .line 221
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    .line 223
    .line 224
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 225
    .line 226
    .line 227
    move-result-wide v5

    .line 228
    sub-long/2addr v5, v3

    .line 229
    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 230
    .line 231
    .line 232
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object v1

    .line 236
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    if-eqz v0, :cond_5

    .line 240
    .line 241
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 242
    .line 243
    .line 244
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 245
    .line 246
    .line 247
    move-result-object v0

    .line 248
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 249
    .line 250
    .line 251
    return-void
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇〇〇0〇〇0(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->toString()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    return-object p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O8ooOoo〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->OO:Landroid/util/LruCache;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8〇o(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$GenerateCallBack;)Landroid/graphics/Bitmap;
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o0ooO()Landroid/util/LruCache;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    check-cast v1, Landroid/graphics/Bitmap;

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    if-eqz v2, :cond_1

    .line 18
    .line 19
    :cond_0
    invoke-interface {p2}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$GenerateCallBack;->generate()Landroid/graphics/Bitmap;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    if-eqz v1, :cond_1

    .line 24
    .line 25
    invoke-virtual {v0, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    :cond_1
    return-object v1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇O〇oO()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 4
    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onCleared()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->OO:Landroid/util/LruCache;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 9
    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 17
    .line 18
    .line 19
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 20
    .line 21
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇00O:Landroid/os/Handler;

    .line 22
    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 26
    .line 27
    .line 28
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇00O:Landroid/os/Handler;

    .line 29
    .line 30
    :cond_2
    const-string v0, "EnhanceThumbViewModel"

    .line 31
    .line 32
    const-string v1, "onCleared"

    .line 33
    .line 34
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇00(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;Ljava/lang/String;)V
    .locals 8

    .line 1
    iget-object v0, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->O8o08O8O:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_5

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 18
    .line 19
    new-instance v2, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    iget-object v3, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->o0:Ljava/lang/String;

    .line 25
    .line 26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v3, "_"

    .line 30
    .line 31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    iget v4, v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 35
    .line 36
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    new-instance v4, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    iget-object v5, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->o0:Ljava/lang/String;

    .line 49
    .line 50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const/16 v3, 0x7a

    .line 57
    .line 58
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    iget v5, v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 66
    .line 67
    if-eqz v5, :cond_4

    .line 68
    .line 69
    const/16 v6, 0x7b

    .line 70
    .line 71
    const/16 v7, 0x77

    .line 72
    .line 73
    if-eq v5, v7, :cond_0

    .line 74
    .line 75
    if-eq v5, v3, :cond_0

    .line 76
    .line 77
    if-eq v5, v6, :cond_0

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_0
    new-instance v3, Lcom/intsig/camscanner/multiimageedit/viewModel/〇o00〇〇Oo;

    .line 81
    .line 82
    invoke-direct {v3, p3, p2}, Lcom/intsig/camscanner/multiimageedit/viewModel/〇o00〇〇Oo;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p0, v4, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O8〇o(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$GenerateCallBack;)Landroid/graphics/Bitmap;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    if-nez v3, :cond_1

    .line 90
    .line 91
    const-string v1, "EnhanceThumbViewModel"

    .line 92
    .line 93
    const-string v2, "pad bitmap is null"

    .line 94
    .line 95
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_1
    iget v4, v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 100
    .line 101
    if-ne v4, v7, :cond_2

    .line 102
    .line 103
    new-instance v4, Lcom/intsig/camscanner/multiimageedit/viewModel/〇o〇;

    .line 104
    .line 105
    invoke-direct {v4, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/〇o〇;-><init>(Landroid/graphics/Bitmap;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p0, v2, v4}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O8〇o(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$GenerateCallBack;)Landroid/graphics/Bitmap;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    iput-object v2, v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->Oo08:Landroid/graphics/Bitmap;

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_2
    if-ne v4, v6, :cond_3

    .line 116
    .line 117
    new-instance v4, Lcom/intsig/camscanner/multiimageedit/viewModel/O8;

    .line 118
    .line 119
    invoke-direct {v4, p1, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/O8;-><init>(Landroidx/fragment/app/FragmentActivity;Landroid/graphics/Bitmap;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {p0, v2, v4}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O8〇o(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$GenerateCallBack;)Landroid/graphics/Bitmap;

    .line 123
    .line 124
    .line 125
    move-result-object v2

    .line 126
    iput-object v2, v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->Oo08:Landroid/graphics/Bitmap;

    .line 127
    .line 128
    goto :goto_0

    .line 129
    :cond_3
    iput-object v3, v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->Oo08:Landroid/graphics/Bitmap;

    .line 130
    .line 131
    goto :goto_0

    .line 132
    :cond_4
    new-instance v3, Lcom/intsig/camscanner/multiimageedit/viewModel/〇080;

    .line 133
    .line 134
    invoke-direct {v3, p2}, Lcom/intsig/camscanner/multiimageedit/viewModel/〇080;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {p0, v2, v3}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O8〇o(Ljava/lang/String;Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel$GenerateCallBack;)Landroid/graphics/Bitmap;

    .line 138
    .line 139
    .line 140
    move-result-object v2

    .line 141
    iput-object v2, v1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->Oo08:Landroid/graphics/Bitmap;

    .line 142
    .line 143
    goto/16 :goto_0

    .line 144
    .line 145
    :cond_5
    return-void
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public 〇8o8O〇O(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "startGenerateThumb imagePath="

    .line 6
    .line 7
    const-string v2, "EnhanceThumbViewModel"

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    new-instance p1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string p2, " is not exist"

    .line 23
    .line 24
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o8o〇〇0O()V

    .line 35
    .line 36
    .line 37
    return-void

    .line 38
    :cond_0
    if-gtz p4, :cond_1

    .line 39
    .line 40
    new-instance p1, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string p2, "startGenerateThumb illegal thumbWidth="

    .line 46
    .line 47
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o8o〇〇0O()V

    .line 61
    .line 62
    .line 63
    return-void

    .line 64
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    const-string v1, " imageUUID="

    .line 76
    .line 77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 91
    .line 92
    if-nez v0, :cond_2

    .line 93
    .line 94
    new-instance v0, Landroid/os/HandlerThread;

    .line 95
    .line 96
    const-string v1, "EnhanceThumb Thread"

    .line 97
    .line 98
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 102
    .line 103
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 104
    .line 105
    .line 106
    new-instance v0, Landroid/os/Handler;

    .line 107
    .line 108
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 109
    .line 110
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    new-instance v3, Lo8O/Oo0oO〇O〇O;

    .line 115
    .line 116
    invoke-direct {v3, p0, p1}, Lo8O/Oo0oO〇O〇O;-><init>(Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;Landroidx/fragment/app/FragmentActivity;)V

    .line 117
    .line 118
    .line 119
    invoke-direct {v0, v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 120
    .line 121
    .line 122
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇00O:Landroid/os/Handler;

    .line 123
    .line 124
    :cond_2
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o0:Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;

    .line 125
    .line 126
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->clone()Ljava/lang/Object;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    check-cast p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .line 132
    goto :goto_0

    .line 133
    :catch_0
    move-exception p1

    .line 134
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 135
    .line 136
    .line 137
    const/4 p1, 0x0

    .line 138
    :goto_0
    if-nez p1, :cond_3

    .line 139
    .line 140
    return-void

    .line 141
    :cond_3
    iput-object p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇OOo8〇0:Ljava/lang/String;

    .line 142
    .line 143
    iput p4, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->OO:I

    .line 144
    .line 145
    iput p5, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇08O〇00〇o:I

    .line 146
    .line 147
    iput-object p6, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->o0:Ljava/lang/String;

    .line 148
    .line 149
    iput-object p3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->o〇00O:Ljava/lang/String;

    .line 150
    .line 151
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 152
    .line 153
    .line 154
    move-result p2

    .line 155
    if-eqz p2, :cond_4

    .line 156
    .line 157
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiEditEnhanceThumb;->〇080()V

    .line 158
    .line 159
    .line 160
    :cond_4
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇00O:Landroid/os/Handler;

    .line 161
    .line 162
    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 163
    .line 164
    .line 165
    move-result-object p2

    .line 166
    iput-object p1, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 167
    .line 168
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇00O:Landroid/os/Handler;

    .line 169
    .line 170
    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 171
    .line 172
    .line 173
    return-void
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

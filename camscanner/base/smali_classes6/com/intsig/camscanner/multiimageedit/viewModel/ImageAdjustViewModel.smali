.class public Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "ImageAdjustViewModel.java"


# instance fields
.field private O8o08O8O:I

.field private OO:Landroid/os/HandlerThread;

.field private o0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private o〇00O:I

.field private 〇08O〇00〇o:Landroid/os/Handler;

.field private 〇OOo8〇0:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    sget v0, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0:I

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->o〇00O:I

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8o08O8O:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;-><init>(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->toString()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    return-object p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oo88o8O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->OO:Landroid/os/HandlerThread;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroid/os/HandlerThread;

    .line 6
    .line 7
    const-string v1, "ImageAdjustViewMolder"

    .line 8
    .line 9
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->OO:Landroid/os/HandlerThread;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 15
    .line 16
    .line 17
    new-instance v0, Landroid/os/Handler;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->OO:Landroid/os/HandlerThread;

    .line 20
    .line 21
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    new-instance v2, Lo8O/O0oO008;

    .line 26
    .line 27
    invoke-direct {v2, p0}, Lo8O/O0oO008;-><init>(Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇08O〇00〇o:Landroid/os/Handler;

    .line 34
    .line 35
    :cond_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇oo〇(Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8o8o〇()Landroid/util/LruCache;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇OOo8〇0:Landroid/util/LruCache;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇o〇(Landroid/content/Context;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/high16 v1, 0x4000000

    .line 14
    .line 15
    if-le v0, v1, :cond_0

    .line 16
    .line 17
    const/high16 v0, 0x4000000

    .line 18
    .line 19
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel$1;

    .line 20
    .line 21
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel$1;-><init>(Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;I)V

    .line 22
    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇OOo8〇0:Landroid/util/LruCache;

    .line 25
    .line 26
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇OOo8〇0:Landroid/util/LruCache;

    .line 27
    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇oo〇(Landroid/os/Message;)Z
    .locals 7

    .line 1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2
    .line 3
    instance-of v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 4
    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    check-cast p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇8o8o〇()Landroid/util/LruCache;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 14
    .line 15
    new-instance v2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v3, "ori_adjust"

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    iget v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 26
    .line 27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    check-cast v2, Landroid/graphics/Bitmap;

    .line 43
    .line 44
    iget v3, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8o08O8O:I

    .line 45
    .line 46
    if-nez v3, :cond_0

    .line 47
    .line 48
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    iput v3, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8o08O8O:I

    .line 53
    .line 54
    :cond_0
    if-nez v2, :cond_1

    .line 55
    .line 56
    iget-object v2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 57
    .line 58
    sget v3, Lcom/intsig/camscanner/app/AppConfig;->Oo08:I

    .line 59
    .line 60
    iget v4, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->o〇00O:I

    .line 61
    .line 62
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->O8〇o()Landroid/graphics/Bitmap$Config;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    const/4 v6, 0x1

    .line 67
    invoke-static {v2, v3, v4, v5, v6}, Lcom/intsig/camscanner/util/Util;->〇0O〇Oo(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    iget v3, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8o08O8O:I

    .line 72
    .line 73
    iget v4, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 74
    .line 75
    invoke-static {v3, v2, v4}, Lcom/intsig/camscanner/scanner/ScannerUtils;->enhanceImage(ILandroid/graphics/Bitmap;I)Z

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    :cond_1
    invoke-static {v2}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8o08O8O:I

    .line 86
    .line 87
    iget v2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O0O:I

    .line 88
    .line 89
    iget v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇08O:I

    .line 90
    .line 91
    iget p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8oOOo:I

    .line 92
    .line 93
    invoke-static {v1, v0, v2, v3, p1}, Lcom/intsig/scanner/ScannerEngine;->adjustBitmap(ILandroid/graphics/Bitmap;III)I

    .line 94
    .line 95
    .line 96
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇O00()Landroidx/lifecycle/MutableLiveData;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 101
    .line 102
    .line 103
    :cond_2
    const/4 p1, 0x0

    .line 104
    return p1
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public O8ooOoo〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->o〇00O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onCleared()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->OO:Landroid/os/HandlerThread;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 10
    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->OO:Landroid/os/HandlerThread;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇08O〇00〇o:Landroid/os/Handler;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇08O〇00〇o:Landroid/os/Handler;

    .line 22
    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇OOo8〇0:Landroid/util/LruCache;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 28
    .line 29
    .line 30
    :cond_2
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8o08O8O:I

    .line 31
    .line 32
    if-eqz v0, :cond_3

    .line 33
    .line 34
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 35
    .line 36
    .line 37
    const/4 v0, 0x0

    .line 38
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8o08O8O:I

    .line 39
    .line 40
    :cond_3
    const-string v0, "ImageAdjustViewMolder"

    .line 41
    .line 42
    const-string v1, "onCleared"

    .line 43
    .line 44
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇00(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->oo88o8O()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇08O〇00〇o:Landroid/os/Handler;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇08O〇00〇o:Landroid/os/Handler;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇08O〇00〇o:Landroid/os/Handler;

    .line 19
    .line 20
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇O00()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->o0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->o0:Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->o0:Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

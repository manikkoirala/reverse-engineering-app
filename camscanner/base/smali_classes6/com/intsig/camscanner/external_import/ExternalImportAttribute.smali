.class public final Lcom/intsig/camscanner/external_import/ExternalImportAttribute;
.super Ljava/lang/Object;
.source "ExternalImportAttribute.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final data:Lcom/intsig/camscanner/external_import/ExternalImportData;

.field private final ret:I


# direct methods
.method public constructor <init>(ILcom/intsig/camscanner/external_import/ExternalImportData;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/external_import/ExternalImportAttribute;->ret:I

    iput-object p2, p0, Lcom/intsig/camscanner/external_import/ExternalImportAttribute;->data:Lcom/intsig/camscanner/external_import/ExternalImportData;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/intsig/camscanner/external_import/ExternalImportData;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    .line 3
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/external_import/ExternalImportAttribute;-><init>(ILcom/intsig/camscanner/external_import/ExternalImportData;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getData()Lcom/intsig/camscanner/external_import/ExternalImportData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/external_import/ExternalImportAttribute;->data:Lcom/intsig/camscanner/external_import/ExternalImportData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getRet()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/external_import/ExternalImportAttribute;->ret:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final isOk()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/external_import/ExternalImportAttribute;->ret:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

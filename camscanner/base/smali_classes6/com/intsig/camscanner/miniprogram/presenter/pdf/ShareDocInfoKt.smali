.class public final Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfoKt;
.super Ljava/lang/Object;
.source "ShareDocInfo.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public static final 〇080(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;)Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;
    .locals 10
    .param p0    # Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "<this>"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;->getFile_name()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x1

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const-string v3, ".jdoc"

    .line 15
    .line 16
    const-string v4, ""

    .line 17
    .line 18
    invoke-static {v0, v3, v4, v2}, Lkotlin/text/StringsKt;->O〇8O8〇008(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    move-object v0, v1

    .line 24
    :goto_0
    new-instance v3, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;->getTitle()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    invoke-direct {v3, p1, v0, v4}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;-><init>(Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v3}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->OO0o〇〇()Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    if-nez p1, :cond_5

    .line 38
    .line 39
    new-instance p1, Ljava/util/ArrayList;

    .line 40
    .line 41
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;->getPages()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    if-eqz p0, :cond_2

    .line 49
    .line 50
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-nez v0, :cond_1

    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_1
    const/4 v0, 0x0

    .line 58
    goto :goto_2

    .line 59
    :cond_2
    :goto_1
    const/4 v0, 0x1

    .line 60
    :goto_2
    xor-int/2addr v0, v2

    .line 61
    if-eqz v0, :cond_3

    .line 62
    .line 63
    move-object v4, p0

    .line 64
    goto :goto_3

    .line 65
    :cond_3
    move-object v4, v1

    .line 66
    :goto_3
    if-eqz v4, :cond_4

    .line 67
    .line 68
    const-string p0, ","

    .line 69
    .line 70
    filled-new-array {p0}, [Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    const/4 v6, 0x0

    .line 75
    const/4 v7, 0x0

    .line 76
    const/4 v8, 0x6

    .line 77
    const/4 v9, 0x0

    .line 78
    invoke-static/range {v4 .. v9}, Lkotlin/text/StringsKt;->Oo(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    .line 79
    .line 80
    .line 81
    move-result-object p0

    .line 82
    check-cast p0, Ljava/lang/Iterable;

    .line 83
    .line 84
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 85
    .line 86
    .line 87
    move-result-object p0

    .line 88
    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    if-eqz v0, :cond_4

    .line 93
    .line 94
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    check-cast v0, Ljava/lang/String;

    .line 99
    .line 100
    new-instance v1, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter$FileDownloadEntity;

    .line 101
    .line 102
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter$FileDownloadEntity;-><init>(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    goto :goto_4

    .line 109
    :cond_4
    invoke-virtual {v3, p1}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇O00(Ljava/util/ArrayList;)V

    .line 110
    .line 111
    .line 112
    :cond_5
    return-object v3
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.class public final Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;
.super Ljava/lang/Object;
.source "ShowPdfDocPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;)Z
    .locals 10
    .param p1    # Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "shareDoc"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;->getData()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->getShare_info()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;->getDoc_id()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    move-object v0, v1

    .line 25
    :goto_0
    const/4 v2, 0x1

    .line 26
    const/4 v3, 0x0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    if-nez v4, :cond_1

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_1
    const/4 v4, 0x0

    .line 37
    goto :goto_2

    .line 38
    :cond_2
    :goto_1
    const/4 v4, 0x1

    .line 39
    :goto_2
    if-nez v4, :cond_3

    .line 40
    .line 41
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->OOO〇O0(Ljava/lang/String;)Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    return p1

    .line 46
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;->getData()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    if-eqz p1, :cond_4

    .line 51
    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->getShare_info()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    if-eqz p1, :cond_4

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;->getDocs()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    if-eqz v4, :cond_4

    .line 63
    .line 64
    const-string p1, ","

    .line 65
    .line 66
    filled-new-array {p1}, [Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v5

    .line 70
    const/4 v6, 0x0

    .line 71
    const/4 v7, 0x0

    .line 72
    const/4 v8, 0x6

    .line 73
    const/4 v9, 0x0

    .line 74
    invoke-static/range {v4 .. v9}, Lkotlin/text/StringsKt;->Oo(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    :cond_4
    move-object p1, v1

    .line 79
    check-cast p1, Ljava/util/Collection;

    .line 80
    .line 81
    if-eqz p1, :cond_6

    .line 82
    .line 83
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 84
    .line 85
    .line 86
    move-result p1

    .line 87
    if-eqz p1, :cond_5

    .line 88
    .line 89
    goto :goto_3

    .line 90
    :cond_5
    const/4 p1, 0x0

    .line 91
    goto :goto_4

    .line 92
    :cond_6
    :goto_3
    const/4 p1, 0x1

    .line 93
    :goto_4
    if-nez p1, :cond_a

    .line 94
    .line 95
    check-cast v1, Ljava/lang/Iterable;

    .line 96
    .line 97
    instance-of p1, v1, Ljava/util/Collection;

    .line 98
    .line 99
    if-eqz p1, :cond_8

    .line 100
    .line 101
    move-object p1, v1

    .line 102
    check-cast p1, Ljava/util/Collection;

    .line 103
    .line 104
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    if-eqz p1, :cond_8

    .line 109
    .line 110
    :cond_7
    const/4 v2, 0x0

    .line 111
    goto :goto_5

    .line 112
    :cond_8
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    :cond_9
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 117
    .line 118
    .line 119
    move-result v0

    .line 120
    if-eqz v0, :cond_7

    .line 121
    .line 122
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    check-cast v0, Ljava/lang/String;

    .line 127
    .line 128
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->OOO〇O0(Ljava/lang/String;)Z

    .line 129
    .line 130
    .line 131
    move-result v0

    .line 132
    if-eqz v0, :cond_9

    .line 133
    .line 134
    :goto_5
    return v2

    .line 135
    :cond_a
    return v3
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

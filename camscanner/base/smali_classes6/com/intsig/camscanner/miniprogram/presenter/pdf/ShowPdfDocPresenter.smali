.class public final Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;
.super Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;
.source "ShowPdfDocPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇〇8O0〇8:Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private OO0o〇〇:Z

.field private final OO0o〇〇〇〇0:Z

.field private final Oooo8o0〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO80:Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;

.field private final 〇80〇808〇O:Ljava/lang/String;

.field private 〇8o8o〇:Z

.field private 〇O00:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter$PageEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O8o08O:Ljava/lang/String;

.field private 〇O〇:Z

.field private 〇〇808〇:Landroid/net/Uri;

.field private final 〇〇888:Lcom/intsig/miniprogram/OtherShareDocToCSEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇8O0〇8:Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/miniprogram/OtherShareDocView;Lcom/intsig/miniprogram/OtherShareDocToCSEntity;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/miniprogram/OtherShareDocView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;-><init>(Lcom/intsig/camscanner/miniprogram/OtherShareDocView;)V

    .line 7
    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇888:Lcom/intsig/miniprogram/OtherShareDocToCSEntity;

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    if-eqz p2, :cond_0

    .line 13
    .line 14
    invoke-virtual {p2}, Lcom/intsig/miniprogram/OtherShareDocToCSEntity;->getContent()Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    move-object v0, p1

    .line 20
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->oO80:Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;

    .line 21
    .line 22
    if-eqz p2, :cond_1

    .line 23
    .line 24
    invoke-virtual {p2}, Lcom/intsig/miniprogram/OtherShareDocToCSEntity;->getDoc_id()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    goto :goto_1

    .line 29
    :cond_1
    move-object v0, p1

    .line 30
    :goto_1
    iput-object v0, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇80〇808〇O:Ljava/lang/String;

    .line 31
    .line 32
    if-eqz p2, :cond_2

    .line 33
    .line 34
    invoke-virtual {p2}, Lcom/intsig/miniprogram/OtherShareDocToCSEntity;->getDocType()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    :cond_2
    const-string p2, "doc"

    .line 39
    .line 40
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    iput-boolean p1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->OO0o〇〇〇〇0:Z

    .line 45
    .line 46
    new-instance p1, Ljava/util/ArrayList;

    .line 47
    .line 48
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .line 50
    .line 51
    iput-object p1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->Oooo8o0〇:Ljava/util/ArrayList;

    .line 52
    .line 53
    new-instance p1, Ljava/util/ArrayList;

    .line 54
    .line 55
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .line 57
    .line 58
    iput-object p1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇O00:Ljava/util/ArrayList;

    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final O8ooOoo〇(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;JI)V
    .locals 3

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 7
    .line 8
    .line 9
    move-result-object p3

    .line 10
    invoke-interface {p3}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 11
    .line 12
    .line 13
    move-result-object p3

    .line 14
    const/4 v0, 0x0

    .line 15
    invoke-static {p3, p1, p2, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇O〇80o08O(Landroid/content/Context;JLjava/lang/String;)Ljava/util/ArrayList;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const/4 p2, 0x0

    .line 24
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 25
    .line 26
    .line 27
    move-result p3

    .line 28
    if-eqz p3, :cond_1

    .line 29
    .line 30
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object p3

    .line 34
    add-int/lit8 v0, p2, 0x1

    .line 35
    .line 36
    if-gez p2, :cond_0

    .line 37
    .line 38
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 39
    .line 40
    .line 41
    :cond_0
    check-cast p3, Ljava/lang/String;

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇O00:Ljava/util/ArrayList;

    .line 44
    .line 45
    new-instance v2, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter$PageEntity;

    .line 46
    .line 47
    invoke-direct {v2, p2, p3}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter$PageEntity;-><init>(ILjava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move p2, v0

    .line 54
    goto :goto_0

    .line 55
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇0000OOO()V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic OoO8(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->Oooo8o0〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇8O8〇008()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇808〇:Landroid/net/Uri;

    .line 2
    .line 3
    iget-boolean v1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->OO0o〇〇:Z

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇O00:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 12
    .line 13
    .line 14
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    invoke-interface {v2, v3}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->OO88o(Ljava/lang/Long;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    const/4 v3, 0x1

    .line 34
    invoke-interface {v2, v3}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->O0o〇(Z)V

    .line 35
    .line 36
    .line 37
    sget-object v2, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->〇080:Lcom/intsig/camscanner/transfer/CsTransferDocUtil;

    .line 38
    .line 39
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->Oooo8o0〇(Ljava/lang/Long;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-interface {v2}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    new-instance v3, L〇OO〇00〇0O/〇080;

    .line 55
    .line 56
    invoke-direct {v3, p0, v0, v1}, L〇OO〇00〇0O/〇080;-><init>(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;J)V

    .line 57
    .line 58
    .line 59
    const/4 v4, 0x0

    .line 60
    invoke-static {v2, v0, v1, v3, v4}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇0000OOO()V

    .line 65
    .line 66
    .line 67
    :goto_0
    return-void
    .line 68
.end method

.method public static final synthetic o800o8O(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->O〇8O8〇008()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;Landroid/net/Uri;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇808〇:Landroid/net/Uri;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇O8〇〇o(Landroid/net/Uri;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0, p1}, Lcom/intsig/camscanner/mainmenu/MainPageRoute;->OO0o〇〇(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    const-string v0, "constant_is_show_doc_location"

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-interface {p1}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇00(Landroid/net/Uri;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0, p1}, Lcom/intsig/camscanner/mainmenu/MainPageRoute;->〇〇808〇(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    const-string v0, "constant_is_show_doc_location"

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-interface {p1}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;JI)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->O8ooOoo〇(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;JI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇O888o0o(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇O〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final 〇oo〇(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;)Z
    .locals 1
    .param p0    # Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇8O0〇8:Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$Companion;->〇080(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;)Z

    .line 4
    .line 5
    .line 6
    move-result p0

    .line 7
    return p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public Oooo8o0〇(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;)V
    .locals 17
    .param p1    # Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    const-string v0, "docData"

    .line 4
    .line 5
    move-object/from16 v2, p1

    .line 6
    .line 7
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-super/range {p0 .. p1}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->Oooo8o0〇(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->Oooo8o0〇:Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 16
    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    iput-boolean v0, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->OO0o〇〇:Z

    .line 20
    .line 21
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;->getData()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    const/4 v4, 0x0

    .line 26
    if-eqz v3, :cond_0

    .line 27
    .line 28
    invoke-virtual {v3}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->getShare_info()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    if-eqz v3, :cond_0

    .line 33
    .line 34
    invoke-virtual {v3}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;->getDoc_id()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    move-object v3, v4

    .line 40
    :goto_0
    const/4 v5, 0x1

    .line 41
    if-eqz v3, :cond_2

    .line 42
    .line 43
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    .line 44
    .line 45
    .line 46
    move-result v6

    .line 47
    if-nez v6, :cond_1

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_1
    const/4 v6, 0x0

    .line 51
    goto :goto_2

    .line 52
    :cond_2
    :goto_1
    const/4 v6, 0x1

    .line 53
    :goto_2
    xor-int/2addr v6, v5

    .line 54
    if-eqz v6, :cond_3

    .line 55
    .line 56
    goto :goto_3

    .line 57
    :cond_3
    move-object v3, v4

    .line 58
    :goto_3
    const-string v6, "pdf"

    .line 59
    .line 60
    const-string v7, "ShowPdfDocPresenter"

    .line 61
    .line 62
    const-string v8, ""

    .line 63
    .line 64
    if-eqz v3, :cond_7

    .line 65
    .line 66
    new-instance v9, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;

    .line 67
    .line 68
    iget-object v10, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->oO80:Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;

    .line 69
    .line 70
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;->getData()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;

    .line 71
    .line 72
    .line 73
    move-result-object v11

    .line 74
    if-eqz v11, :cond_4

    .line 75
    .line 76
    invoke-virtual {v11}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->getDoc_info()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;

    .line 77
    .line 78
    .line 79
    move-result-object v11

    .line 80
    if-eqz v11, :cond_4

    .line 81
    .line 82
    invoke-virtual {v11}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;->getTitle()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v11

    .line 86
    goto :goto_4

    .line 87
    :cond_4
    move-object v11, v4

    .line 88
    :goto_4
    invoke-direct {v9, v10, v3, v11}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;-><init>(Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->〇〇888()Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;

    .line 92
    .line 93
    .line 94
    move-result-object v3

    .line 95
    invoke-virtual {v9, v3}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇O〇(Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;)V

    .line 96
    .line 97
    .line 98
    iget-object v3, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇888:Lcom/intsig/miniprogram/OtherShareDocToCSEntity;

    .line 99
    .line 100
    if-eqz v3, :cond_5

    .line 101
    .line 102
    invoke-virtual {v3}, Lcom/intsig/miniprogram/OtherShareDocToCSEntity;->getServer_url_v2()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v3

    .line 106
    goto :goto_5

    .line 107
    :cond_5
    move-object v3, v4

    .line 108
    :goto_5
    if-nez v3, :cond_6

    .line 109
    .line 110
    move-object v3, v8

    .line 111
    :cond_6
    invoke-virtual {v9, v3}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇〇8O0〇8(Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    iget-object v3, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->Oooo8o0〇:Ljava/util/ArrayList;

    .line 115
    .line 116
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    iput-boolean v5, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->OO0o〇〇:Z

    .line 120
    .line 121
    invoke-virtual {v9}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇080()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v3

    .line 125
    new-instance v9, Ljava/lang/StringBuilder;

    .line 126
    .line 127
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    .line 129
    .line 130
    const-string v10, "initDocData, share one doc: "

    .line 131
    .line 132
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v3

    .line 142
    invoke-static {v7, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    const-string v3, "single"

    .line 146
    .line 147
    move-object v9, v6

    .line 148
    goto :goto_6

    .line 149
    :cond_7
    move-object v3, v8

    .line 150
    move-object v9, v3

    .line 151
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;->getData()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;

    .line 152
    .line 153
    .line 154
    move-result-object v2

    .line 155
    if-eqz v2, :cond_21

    .line 156
    .line 157
    invoke-virtual {v2}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->getDirs()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    if-eqz v2, :cond_21

    .line 162
    .line 163
    invoke-virtual {v2}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean;->getDocs()Ljava/util/List;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    if-eqz v2, :cond_21

    .line 168
    .line 169
    check-cast v2, Ljava/lang/Iterable;

    .line 170
    .line 171
    instance-of v3, v2, Ljava/util/Collection;

    .line 172
    .line 173
    const-string v9, ".jdoc"

    .line 174
    .line 175
    const-string v10, "file_name"

    .line 176
    .line 177
    if-eqz v3, :cond_9

    .line 178
    .line 179
    move-object v11, v2

    .line 180
    check-cast v11, Ljava/util/Collection;

    .line 181
    .line 182
    invoke-interface {v11}, Ljava/util/Collection;->isEmpty()Z

    .line 183
    .line 184
    .line 185
    move-result v11

    .line 186
    if-eqz v11, :cond_9

    .line 187
    .line 188
    :cond_8
    const/4 v11, 0x1

    .line 189
    goto :goto_8

    .line 190
    :cond_9
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 191
    .line 192
    .line 193
    move-result-object v11

    .line 194
    :cond_a
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    .line 195
    .line 196
    .line 197
    move-result v12

    .line 198
    if-eqz v12, :cond_8

    .line 199
    .line 200
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 201
    .line 202
    .line 203
    move-result-object v12

    .line 204
    check-cast v12, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;

    .line 205
    .line 206
    invoke-virtual {v12}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;->getFile_name()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v12

    .line 210
    if-eqz v12, :cond_b

    .line 211
    .line 212
    invoke-static {v12, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    invoke-static {v12, v9, v8, v5}, Lkotlin/text/StringsKt;->O〇8O8〇008(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v12

    .line 219
    goto :goto_7

    .line 220
    :cond_b
    move-object v12, v4

    .line 221
    :goto_7
    invoke-static {v12}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->OOO〇O0(Ljava/lang/String;)Z

    .line 222
    .line 223
    .line 224
    move-result v12

    .line 225
    if-nez v12, :cond_a

    .line 226
    .line 227
    const/4 v11, 0x0

    .line 228
    :goto_8
    xor-int/2addr v11, v5

    .line 229
    iget-object v12, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇80〇808〇O:Ljava/lang/String;

    .line 230
    .line 231
    if-eqz v12, :cond_d

    .line 232
    .line 233
    invoke-interface {v12}, Ljava/lang/CharSequence;->length()I

    .line 234
    .line 235
    .line 236
    move-result v13

    .line 237
    if-nez v13, :cond_c

    .line 238
    .line 239
    goto :goto_9

    .line 240
    :cond_c
    const/4 v13, 0x0

    .line 241
    goto :goto_a

    .line 242
    :cond_d
    :goto_9
    const/4 v13, 0x1

    .line 243
    :goto_a
    if-nez v13, :cond_e

    .line 244
    .line 245
    iget-boolean v13, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->OO0o〇〇〇〇0:Z

    .line 246
    .line 247
    if-nez v13, :cond_e

    .line 248
    .line 249
    const/4 v13, 0x1

    .line 250
    goto :goto_b

    .line 251
    :cond_e
    const/4 v13, 0x0

    .line 252
    :goto_b
    if-eqz v13, :cond_f

    .line 253
    .line 254
    goto :goto_c

    .line 255
    :cond_f
    move-object v12, v4

    .line 256
    :goto_c
    if-eqz v12, :cond_16

    .line 257
    .line 258
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 259
    .line 260
    .line 261
    move-result-object v13

    .line 262
    :goto_d
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    .line 263
    .line 264
    .line 265
    move-result v14

    .line 266
    if-eqz v14, :cond_13

    .line 267
    .line 268
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 269
    .line 270
    .line 271
    move-result-object v14

    .line 272
    move-object v15, v14

    .line 273
    check-cast v15, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;

    .line 274
    .line 275
    invoke-virtual {v15}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;->getFile_name()Ljava/lang/String;

    .line 276
    .line 277
    .line 278
    move-result-object v15

    .line 279
    if-eqz v15, :cond_10

    .line 280
    .line 281
    invoke-static {v15, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    .line 283
    .line 284
    move-object/from16 v16, v6

    .line 285
    .line 286
    const/4 v6, 0x2

    .line 287
    invoke-static {v15, v12, v0, v6, v4}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 288
    .line 289
    .line 290
    move-result v6

    .line 291
    if-ne v6, v5, :cond_11

    .line 292
    .line 293
    const/4 v6, 0x1

    .line 294
    goto :goto_e

    .line 295
    :cond_10
    move-object/from16 v16, v6

    .line 296
    .line 297
    :cond_11
    const/4 v6, 0x0

    .line 298
    :goto_e
    if-eqz v6, :cond_12

    .line 299
    .line 300
    goto :goto_f

    .line 301
    :cond_12
    move-object/from16 v6, v16

    .line 302
    .line 303
    goto :goto_d

    .line 304
    :cond_13
    move-object/from16 v16, v6

    .line 305
    .line 306
    move-object v14, v4

    .line 307
    :goto_f
    check-cast v14, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;

    .line 308
    .line 309
    if-eqz v14, :cond_17

    .line 310
    .line 311
    iget-object v6, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->oO80:Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;

    .line 312
    .line 313
    invoke-static {v14, v6}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfoKt;->〇080(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;)Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;

    .line 314
    .line 315
    .line 316
    move-result-object v6

    .line 317
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->〇80〇808〇O()Ljava/lang/String;

    .line 318
    .line 319
    .line 320
    move-result-object v12

    .line 321
    invoke-virtual {v6, v12}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->OoO8(Ljava/lang/String;)V

    .line 322
    .line 323
    .line 324
    iget-object v12, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇888:Lcom/intsig/miniprogram/OtherShareDocToCSEntity;

    .line 325
    .line 326
    if-eqz v12, :cond_14

    .line 327
    .line 328
    invoke-virtual {v12}, Lcom/intsig/miniprogram/OtherShareDocToCSEntity;->getServer_url_v2()Ljava/lang/String;

    .line 329
    .line 330
    .line 331
    move-result-object v12

    .line 332
    goto :goto_10

    .line 333
    :cond_14
    move-object v12, v4

    .line 334
    :goto_10
    if-nez v12, :cond_15

    .line 335
    .line 336
    move-object v12, v8

    .line 337
    :cond_15
    invoke-virtual {v6, v12}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇〇8O0〇8(Ljava/lang/String;)V

    .line 338
    .line 339
    .line 340
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->〇〇888()Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;

    .line 341
    .line 342
    .line 343
    move-result-object v12

    .line 344
    invoke-virtual {v6, v12}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇O〇(Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;)V

    .line 345
    .line 346
    .line 347
    invoke-virtual {v6, v5}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇〇808〇(Z)V

    .line 348
    .line 349
    .line 350
    invoke-virtual {v6, v11}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->Oooo8o0〇(Z)V

    .line 351
    .line 352
    .line 353
    iget-object v12, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->Oooo8o0〇:Ljava/util/ArrayList;

    .line 354
    .line 355
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    .line 357
    .line 358
    invoke-virtual {v6}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇080()Ljava/lang/String;

    .line 359
    .line 360
    .line 361
    move-result-object v6

    .line 362
    new-instance v12, Ljava/lang/StringBuilder;

    .line 363
    .line 364
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 365
    .line 366
    .line 367
    const-string v13, "initDocData, share multi doc, select one: "

    .line 368
    .line 369
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    .line 371
    .line 372
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    .line 374
    .line 375
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 376
    .line 377
    .line 378
    move-result-object v6

    .line 379
    invoke-static {v7, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    .line 381
    .line 382
    sget-object v6, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 383
    .line 384
    goto :goto_11

    .line 385
    :cond_16
    move-object/from16 v16, v6

    .line 386
    .line 387
    :cond_17
    move-object v6, v4

    .line 388
    :goto_11
    if-nez v6, :cond_1b

    .line 389
    .line 390
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 391
    .line 392
    .line 393
    move-result-object v6

    .line 394
    :goto_12
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 395
    .line 396
    .line 397
    move-result v12

    .line 398
    if-eqz v12, :cond_1a

    .line 399
    .line 400
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 401
    .line 402
    .line 403
    move-result-object v12

    .line 404
    check-cast v12, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;

    .line 405
    .line 406
    const-string v13, "it"

    .line 407
    .line 408
    invoke-static {v12, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 409
    .line 410
    .line 411
    iget-object v13, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->oO80:Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;

    .line 412
    .line 413
    invoke-static {v12, v13}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfoKt;->〇080(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;Lcom/intsig/miniprogram/OtherShareDocToCSEntity$ContentBean;)Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;

    .line 414
    .line 415
    .line 416
    move-result-object v12

    .line 417
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->〇80〇808〇O()Ljava/lang/String;

    .line 418
    .line 419
    .line 420
    move-result-object v13

    .line 421
    invoke-virtual {v12, v13}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->OoO8(Ljava/lang/String;)V

    .line 422
    .line 423
    .line 424
    iget-object v13, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇888:Lcom/intsig/miniprogram/OtherShareDocToCSEntity;

    .line 425
    .line 426
    if-eqz v13, :cond_18

    .line 427
    .line 428
    invoke-virtual {v13}, Lcom/intsig/miniprogram/OtherShareDocToCSEntity;->getServer_url_v2()Ljava/lang/String;

    .line 429
    .line 430
    .line 431
    move-result-object v13

    .line 432
    goto :goto_13

    .line 433
    :cond_18
    move-object v13, v4

    .line 434
    :goto_13
    if-nez v13, :cond_19

    .line 435
    .line 436
    move-object v13, v8

    .line 437
    :cond_19
    invoke-virtual {v12, v13}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇〇8O0〇8(Ljava/lang/String;)V

    .line 438
    .line 439
    .line 440
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->〇〇888()Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;

    .line 441
    .line 442
    .line 443
    move-result-object v13

    .line 444
    invoke-virtual {v12, v13}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇O〇(Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;)V

    .line 445
    .line 446
    .line 447
    invoke-virtual {v12, v5}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->〇〇808〇(Z)V

    .line 448
    .line 449
    .line 450
    invoke-virtual {v12, v11}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShareDocInfo;->Oooo8o0〇(Z)V

    .line 451
    .line 452
    .line 453
    iget-object v13, v1, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->Oooo8o0〇:Ljava/util/ArrayList;

    .line 454
    .line 455
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    .line 457
    .line 458
    goto :goto_12

    .line 459
    :cond_1a
    const-string v6, "initDocData, share multi doc"

    .line 460
    .line 461
    invoke-static {v7, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    .line 463
    .line 464
    :cond_1b
    if-eqz v3, :cond_1c

    .line 465
    .line 466
    move-object v3, v2

    .line 467
    check-cast v3, Ljava/util/Collection;

    .line 468
    .line 469
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 470
    .line 471
    .line 472
    move-result v3

    .line 473
    if-eqz v3, :cond_1c

    .line 474
    .line 475
    goto :goto_15

    .line 476
    :cond_1c
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 477
    .line 478
    .line 479
    move-result-object v2

    .line 480
    :cond_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 481
    .line 482
    .line 483
    move-result v3

    .line 484
    if-eqz v3, :cond_1f

    .line 485
    .line 486
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 487
    .line 488
    .line 489
    move-result-object v3

    .line 490
    check-cast v3, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;

    .line 491
    .line 492
    invoke-virtual {v3}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean$DocsBean;->getFile_name()Ljava/lang/String;

    .line 493
    .line 494
    .line 495
    move-result-object v3

    .line 496
    if-eqz v3, :cond_1e

    .line 497
    .line 498
    invoke-static {v3, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    .line 500
    .line 501
    invoke-static {v3, v9, v8, v5}, Lkotlin/text/StringsKt;->O〇8O8〇008(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 502
    .line 503
    .line 504
    move-result-object v3

    .line 505
    goto :goto_14

    .line 506
    :cond_1e
    move-object v3, v4

    .line 507
    :goto_14
    invoke-static {v3}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oOO8O8(Ljava/lang/String;)Z

    .line 508
    .line 509
    .line 510
    move-result v3

    .line 511
    xor-int/2addr v3, v5

    .line 512
    if-eqz v3, :cond_1d

    .line 513
    .line 514
    const/4 v0, 0x1

    .line 515
    :cond_1f
    :goto_15
    if-eqz v0, :cond_20

    .line 516
    .line 517
    const-string v6, "pdf_pic"

    .line 518
    .line 519
    goto :goto_16

    .line 520
    :cond_20
    move-object/from16 v6, v16

    .line 521
    .line 522
    :goto_16
    const-string v3, "batch"

    .line 523
    .line 524
    move-object v9, v6

    .line 525
    :cond_21
    const-string v0, "CSShareList"

    .line 526
    .line 527
    const-string v2, "type"

    .line 528
    .line 529
    const-string v5, "doc_scheme"

    .line 530
    .line 531
    invoke-static {v0, v2, v3, v5, v9}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    .line 533
    .line 534
    :try_start_0
    sget-object v0, Lkotlin/Result;->Companion:Lkotlin/Result$Companion;

    .line 535
    .line 536
    new-instance v0, Lorg/json/JSONObject;

    .line 537
    .line 538
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 539
    .line 540
    .line 541
    const-string v2, "from"

    .line 542
    .line 543
    const-string v3, "share_link_h5"

    .line 544
    .line 545
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 546
    .line 547
    .line 548
    const-string v2, "link_type"

    .line 549
    .line 550
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->〇〇888()Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;

    .line 551
    .line 552
    .line 553
    move-result-object v3

    .line 554
    if-eqz v3, :cond_22

    .line 555
    .line 556
    invoke-virtual {v3}, Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;->〇080()Ljava/lang/String;

    .line 557
    .line 558
    .line 559
    move-result-object v3

    .line 560
    goto :goto_17

    .line 561
    :cond_22
    move-object v3, v4

    .line 562
    :goto_17
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 563
    .line 564
    .line 565
    const-string v2, "link_flag"

    .line 566
    .line 567
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->〇〇888()Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;

    .line 568
    .line 569
    .line 570
    move-result-object v3

    .line 571
    if-eqz v3, :cond_23

    .line 572
    .line 573
    invoke-virtual {v3}, Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;->〇o〇()Ljava/lang/String;

    .line 574
    .line 575
    .line 576
    move-result-object v4

    .line 577
    :cond_23
    invoke-virtual {v0, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 578
    .line 579
    .line 580
    invoke-virtual {v0, v5, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 581
    .line 582
    .line 583
    const-string v2, "CSStart"

    .line 584
    .line 585
    const-string v3, "start"

    .line 586
    .line 587
    invoke-static {v2, v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 588
    .line 589
    .line 590
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 591
    .line 592
    invoke-static {v0}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 593
    .line 594
    .line 595
    goto :goto_18

    .line 596
    :catchall_0
    move-exception v0

    .line 597
    sget-object v2, Lkotlin/Result;->Companion:Lkotlin/Result$Companion;

    .line 598
    .line 599
    invoke-static {v0}, Lkotlin/ResultKt;->〇080(Ljava/lang/Throwable;)Ljava/lang/Object;

    .line 600
    .line 601
    .line 602
    move-result-object v0

    .line 603
    invoke-static {v0}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    .line 605
    .line 606
    :goto_18
    return-void
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public 〇0000OOO()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->〇o〇Oo0()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇〇808〇:Landroid/net/Uri;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string v0, "ShowPdfDocPresenter"

    .line 13
    .line 14
    const-string v1, "submit, uri == null"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const v1, 0x7f130089

    .line 28
    .line 29
    .line 30
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->〇oOo〇()V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_0
    iget-boolean v1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->OO0o〇〇:Z

    .line 42
    .line 43
    if-eqz v1, :cond_1

    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇O00:Ljava/util/ArrayList;

    .line 50
    .line 51
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->O0o8〇O(Ljava/util/ArrayList;)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    iget-boolean v1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇O〇:Z

    .line 56
    .line 57
    if-eqz v1, :cond_2

    .line 58
    .line 59
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇00(Landroid/net/Uri;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->o〇O8〇〇o(Landroid/net/Uri;)V

    .line 64
    .line 65
    .line 66
    :goto_0
    return-void
    .line 67
    .line 68
.end method

.method public 〇8o8o〇()I
    .locals 1

    .line 1
    const v0, 0x7f0d00c8

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8o08O()Ljava/lang/String;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇O8o08O:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->〇o〇()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;->getData()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->getDoc_info()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;->getTitle()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    goto :goto_0

    .line 30
    :cond_1
    move-object v0, v1

    .line 31
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇O8o08O:Ljava/lang/String;

    .line 32
    .line 33
    const/4 v2, 0x1

    .line 34
    if-eqz v0, :cond_3

    .line 35
    .line 36
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    if-nez v3, :cond_2

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_2
    const/4 v3, 0x0

    .line 44
    goto :goto_2

    .line 45
    :cond_3
    :goto_1
    const/4 v3, 0x1

    .line 46
    :goto_2
    xor-int/2addr v2, v3

    .line 47
    if-eqz v2, :cond_4

    .line 48
    .line 49
    move-object v1, v0

    .line 50
    :cond_4
    if-nez v1, :cond_5

    .line 51
    .line 52
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    const v1, 0x7f130ad4

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const-string v0, "view.context.getString(R\u2026g.cs_536_multidoc_share1)"

    .line 68
    .line 69
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    :cond_5
    return-object v1
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇oOO8O8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;->〇8o8o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇808〇()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/miniprogram/presenter/ShowTypePresenter;->OO0o〇〇()Lcom/intsig/camscanner/miniprogram/OtherShareDocView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/miniprogram/OtherShareDocView;->getContext()Landroid/app/Activity;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    instance-of v1, v0, Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    check-cast v0, Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v0, v2

    .line 18
    :goto_0
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    if-eqz v3, :cond_1

    .line 25
    .line 26
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    const/4 v5, 0x0

    .line 31
    new-instance v6, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$loadDoc$1;

    .line 32
    .line 33
    invoke-direct {v6, p0, v2}, Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter$loadDoc$1;-><init>(Lcom/intsig/camscanner/miniprogram/presenter/pdf/ShowPdfDocPresenter;Lkotlin/coroutines/Continuation;)V

    .line 34
    .line 35
    .line 36
    const/4 v7, 0x2

    .line 37
    const/4 v8, 0x0

    .line 38
    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 39
    .line 40
    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

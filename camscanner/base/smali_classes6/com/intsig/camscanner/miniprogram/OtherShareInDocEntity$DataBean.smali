.class public Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;
.super Ljava/lang/Object;
.source "OtherShareInDocEntity.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DataBean"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;,
        Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean;,
        Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;
    }
.end annotation


# instance fields
.field private dirs:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean;

.field private doc_info:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;

.field private share_info:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getDirs()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->dirs:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDoc_info()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->doc_info:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShare_info()Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->share_info:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->token:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDirs(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->dirs:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DirsBean;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDoc_info(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->doc_info:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$DocInfoBean;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setShare_info(Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->share_info:Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean$ShareInfoBean;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/miniprogram/OtherShareInDocEntity$DataBean;->token:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

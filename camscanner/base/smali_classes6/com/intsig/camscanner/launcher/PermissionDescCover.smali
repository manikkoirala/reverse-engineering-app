.class public final Lcom/intsig/camscanner/launcher/PermissionDescCover;
.super Ljava/lang/Object;
.source "PermissionDescCover.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;,
        Lcom/intsig/camscanner/launcher/PermissionDescCover$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oo08:Lcom/intsig/camscanner/launcher/PermissionDescCover$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:Landroid/widget/PopupWindow;

.field private final 〇080:Landroid/app/Activity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/permission/PermissionFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/launcher/PermissionDescCover$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/launcher/PermissionDescCover$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->Oo08:Lcom/intsig/camscanner/launcher/PermissionDescCover$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/launcher/PermissionDescCover;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "PermissionDescCover::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->o〇0:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/intsig/permission/PermissionFragment;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/permission/PermissionFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "permissionFragment"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇080:Landroid/app/Activity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇o00〇〇Oo:Lcom/intsig/permission/PermissionFragment;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O8()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->o〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/launcher/PermissionDescCover;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/launcher/PermissionDescCover;->oO80()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oO80()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇o〇:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->O8:Landroid/widget/PopupWindow;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 9
    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->o〇0:Ljava/lang/String;

    .line 12
    .line 13
    const-string v1, "dismiss permission tips"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇0(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    const v1, 0x7f0d0567

    .line 4
    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 12
    .line 13
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v3, "#FFFFFFFF"

    .line 17
    .line 18
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    invoke-virtual {v1, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    const-string v3, "#00FFFFFF"

    .line 27
    .line 28
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    invoke-virtual {v1, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇oo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 37
    .line 38
    invoke-virtual {v1, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->o〇O8〇〇o(Landroid/graphics/drawable/GradientDrawable$Orientation;)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 47
    .line 48
    .line 49
    new-instance v1, LoO0〇〇o8〇/OO0o〇〇;

    .line 50
    .line 51
    invoke-direct {v1, p0}, LoO0〇〇o8〇/OO0o〇〇;-><init>(Lcom/intsig/camscanner/launcher/PermissionDescCover;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    .line 56
    .line 57
    const v1, 0x7f0a0f1c

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    .line 65
    .line 66
    new-instance v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 67
    .line 68
    iget-object v4, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇080:Landroid/app/Activity;

    .line 69
    .line 70
    invoke-direct {v3, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 74
    .line 75
    .line 76
    new-instance v4, Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$adapter$1;

    .line 77
    .line 78
    invoke-direct {v4, p1}, Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$adapter$1;-><init>(Ljava/util/List;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v1, v4}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 82
    .line 83
    .line 84
    iget-object p1, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇080:Landroid/app/Activity;

    .line 85
    .line 86
    invoke-virtual {v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->getOrientation()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    new-instance v5, Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$dividerItemDecoration$1;

    .line 91
    .line 92
    invoke-direct {v5, v4, p1, v3}, Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$dividerItemDecoration$1;-><init>(Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$adapter$1;Landroid/app/Activity;I)V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇080:Landroid/app/Activity;

    .line 96
    .line 97
    const v3, 0x7f08041b

    .line 98
    .line 99
    .line 100
    invoke-static {p1, v3}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    if-eqz p1, :cond_0

    .line 105
    .line 106
    invoke-virtual {v5, p1}, Landroidx/recyclerview/widget/DividerItemDecoration;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 107
    .line 108
    .line 109
    :cond_0
    invoke-virtual {v1, v5}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 110
    .line 111
    .line 112
    new-instance p1, Landroid/widget/PopupWindow;

    .line 113
    .line 114
    const/4 v1, -0x1

    .line 115
    invoke-direct {p1, v0, v1, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    .line 116
    .line 117
    .line 118
    const/4 v0, 0x0

    .line 119
    invoke-virtual {p1, v0}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 120
    .line 121
    .line 122
    const/4 v0, 0x1

    .line 123
    invoke-virtual {p1, v0}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 124
    .line 125
    .line 126
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    .line 127
    .line 128
    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 129
    .line 130
    .line 131
    invoke-virtual {p1, v0}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇080:Landroid/app/Activity;

    .line 135
    .line 136
    instance-of v1, v0, Landroidx/lifecycle/LifecycleOwner;

    .line 137
    .line 138
    if-eqz v1, :cond_1

    .line 139
    .line 140
    check-cast v0, Landroidx/lifecycle/LifecycleOwner;

    .line 141
    .line 142
    goto :goto_0

    .line 143
    :cond_1
    move-object v0, v2

    .line 144
    :goto_0
    if-eqz v0, :cond_2

    .line 145
    .line 146
    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    if-eqz v0, :cond_2

    .line 151
    .line 152
    new-instance v1, Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$popupWindow$2$1;

    .line 153
    .line 154
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$popupWindow$2$1;-><init>(Landroid/widget/PopupWindow;)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 158
    .line 159
    .line 160
    :cond_2
    iput-object p1, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->O8:Landroid/widget/PopupWindow;

    .line 161
    .line 162
    iget-object v0, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇080:Landroid/app/Activity;

    .line 163
    .line 164
    instance-of v1, v0, Landroidx/lifecycle/LifecycleOwner;

    .line 165
    .line 166
    if-eqz v1, :cond_3

    .line 167
    .line 168
    check-cast v0, Landroidx/lifecycle/LifecycleOwner;

    .line 169
    .line 170
    goto :goto_1

    .line 171
    :cond_3
    move-object v0, v2

    .line 172
    :goto_1
    if-eqz v0, :cond_4

    .line 173
    .line 174
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    if-eqz v0, :cond_4

    .line 179
    .line 180
    new-instance v1, Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$3;

    .line 181
    .line 182
    invoke-direct {v1, p0, p1, v2}, Lcom/intsig/camscanner/launcher/PermissionDescCover$addCoverView$3;-><init>(Lcom/intsig/camscanner/launcher/PermissionDescCover;Landroid/widget/PopupWindow;Lkotlin/coroutines/Continuation;)V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 186
    .line 187
    .line 188
    :cond_4
    return-void
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/launcher/PermissionDescCover;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇〇888(Lcom/intsig/camscanner/launcher/PermissionDescCover;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/launcher/PermissionDescCover;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launcher/PermissionDescCover;->o〇0(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/launcher/PermissionDescCover;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇o〇:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇888(Lcom/intsig/camscanner/launcher/PermissionDescCover;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->O8:Landroid/widget/PopupWindow;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final getContext()Landroid/app/Activity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇808〇O([Ljava/lang/String;Ljava/util/Map;)V
    .locals 8
    .param p1    # [Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "permissions"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    array-length v1, p1

    .line 12
    const/4 v2, 0x0

    .line 13
    const/4 v3, 0x0

    .line 14
    :goto_0
    if-ge v3, v1, :cond_f

    .line 15
    .line 16
    aget-object v4, p1, v3

    .line 17
    .line 18
    if-eqz p2, :cond_0

    .line 19
    .line 20
    invoke-interface {p2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    move-result v5

    .line 24
    if-eqz v5, :cond_0

    .line 25
    .line 26
    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v5

    .line 30
    check-cast v5, Ljava/lang/Integer;

    .line 31
    .line 32
    if-eqz v5, :cond_0

    .line 33
    .line 34
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    goto :goto_1

    .line 39
    :cond_0
    const/4 v5, 0x0

    .line 40
    :goto_1
    const-string v6, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 41
    .line 42
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    move-result v6

    .line 46
    const-string v7, "android.permission.READ_MEDIA_IMAGES"

    .line 47
    .line 48
    if-nez v6, :cond_b

    .line 49
    .line 50
    invoke-static {v7, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result v6

    .line 54
    if-eqz v6, :cond_1

    .line 55
    .line 56
    goto/16 :goto_7

    .line 57
    .line 58
    :cond_1
    const-string v6, "android.permission.CAMERA"

    .line 59
    .line 60
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    move-result v6

    .line 64
    if-eqz v6, :cond_3

    .line 65
    .line 66
    new-instance v4, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;

    .line 67
    .line 68
    if-eqz v5, :cond_2

    .line 69
    .line 70
    goto :goto_2

    .line 71
    :cond_2
    const v5, 0x7f130a55

    .line 72
    .line 73
    .line 74
    :goto_2
    const v6, 0x7f080596

    .line 75
    .line 76
    .line 77
    const v7, 0x7f130a56

    .line 78
    .line 79
    .line 80
    invoke-direct {v4, v6, v7, v5}, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;-><init>(III)V

    .line 81
    .line 82
    .line 83
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    goto/16 :goto_a

    .line 87
    .line 88
    :cond_3
    const-string v6, "android.permission.ACCESS_FINE_LOCATION"

    .line 89
    .line 90
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    move-result v6

    .line 94
    if-eqz v6, :cond_5

    .line 95
    .line 96
    new-instance v4, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;

    .line 97
    .line 98
    if-eqz v5, :cond_4

    .line 99
    .line 100
    goto :goto_3

    .line 101
    :cond_4
    const v5, 0x7f130a81

    .line 102
    .line 103
    .line 104
    :goto_3
    const v6, 0x7f080b0d

    .line 105
    .line 106
    .line 107
    const v7, 0x7f130a82

    .line 108
    .line 109
    .line 110
    invoke-direct {v4, v6, v7, v5}, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;-><init>(III)V

    .line 111
    .line 112
    .line 113
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    goto/16 :goto_a

    .line 117
    .line 118
    :cond_5
    const-string v6, "android.permission.READ_PHONE_STATE"

    .line 119
    .line 120
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    move-result v6

    .line 124
    const v7, 0x7f08092c

    .line 125
    .line 126
    .line 127
    if-eqz v6, :cond_7

    .line 128
    .line 129
    new-instance v4, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;

    .line 130
    .line 131
    if-eqz v5, :cond_6

    .line 132
    .line 133
    goto :goto_4

    .line 134
    :cond_6
    const v5, 0x7f130a6a

    .line 135
    .line 136
    .line 137
    :goto_4
    const v6, 0x7f130a6b

    .line 138
    .line 139
    .line 140
    invoke-direct {v4, v7, v6, v5}, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;-><init>(III)V

    .line 141
    .line 142
    .line 143
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    goto :goto_a

    .line 147
    :cond_7
    const-string v6, "android.permission.BLUETOOTH_SCAN"

    .line 148
    .line 149
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 150
    .line 151
    .line 152
    move-result v6

    .line 153
    if-eqz v6, :cond_9

    .line 154
    .line 155
    new-instance v4, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;

    .line 156
    .line 157
    if-eqz v5, :cond_8

    .line 158
    .line 159
    goto :goto_5

    .line 160
    :cond_8
    const v5, 0x7f1311c1

    .line 161
    .line 162
    .line 163
    :goto_5
    const v6, 0x7f1311c4

    .line 164
    .line 165
    .line 166
    invoke-direct {v4, v7, v6, v5}, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;-><init>(III)V

    .line 167
    .line 168
    .line 169
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    .line 171
    .line 172
    goto :goto_a

    .line 173
    :cond_9
    const-string v6, "android.permission.POST_NOTIFICATIONS"

    .line 174
    .line 175
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    move-result v4

    .line 179
    if-eqz v4, :cond_e

    .line 180
    .line 181
    new-instance v4, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;

    .line 182
    .line 183
    if-eqz v5, :cond_a

    .line 184
    .line 185
    goto :goto_6

    .line 186
    :cond_a
    const v5, 0x7f13188d

    .line 187
    .line 188
    .line 189
    :goto_6
    const v6, 0x7f13188c

    .line 190
    .line 191
    .line 192
    invoke-direct {v4, v7, v6, v5}, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;-><init>(III)V

    .line 193
    .line 194
    .line 195
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    .line 197
    .line 198
    goto :goto_a

    .line 199
    :cond_b
    :goto_7
    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 200
    .line 201
    .line 202
    move-result v4

    .line 203
    if-eqz v4, :cond_c

    .line 204
    .line 205
    const v4, 0x7f1319b1

    .line 206
    .line 207
    .line 208
    const v6, 0x7f1319b3

    .line 209
    .line 210
    .line 211
    goto :goto_8

    .line 212
    :cond_c
    const v4, 0x7f130a8b

    .line 213
    .line 214
    .line 215
    const v6, 0x7f130a8a

    .line 216
    .line 217
    .line 218
    :goto_8
    new-instance v7, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;

    .line 219
    .line 220
    if-eqz v5, :cond_d

    .line 221
    .line 222
    goto :goto_9

    .line 223
    :cond_d
    move v5, v6

    .line 224
    :goto_9
    const v6, 0x7f080bfe

    .line 225
    .line 226
    .line 227
    invoke-direct {v7, v6, v4, v5}, Lcom/intsig/camscanner/launcher/PermissionDescCover$PermissionDescItem;-><init>(III)V

    .line 228
    .line 229
    .line 230
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    .line 232
    .line 233
    :cond_e
    :goto_a
    add-int/lit8 v3, v3, 0x1

    .line 234
    .line 235
    goto/16 :goto_0

    .line 236
    .line 237
    :cond_f
    iget-object p1, p0, Lcom/intsig/camscanner/launcher/PermissionDescCover;->〇o00〇〇Oo:Lcom/intsig/permission/PermissionFragment;

    .line 238
    .line 239
    new-instance p2, Lcom/intsig/camscanner/launcher/PermissionDescCover$setPermissions$1;

    .line 240
    .line 241
    invoke-direct {p2, p0, v0}, Lcom/intsig/camscanner/launcher/PermissionDescCover$setPermissions$1;-><init>(Lcom/intsig/camscanner/launcher/PermissionDescCover;Ljava/util/List;)V

    .line 242
    .line 243
    .line 244
    invoke-virtual {p1, p2}, Lcom/intsig/permission/PermissionFragment;->〇80O8o8O〇(Lcom/intsig/permission/PermissionFragment$PermissionGrantedOrRejectListener;)V

    .line 245
    .line 246
    .line 247
    return-void
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.class public Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "BatchOCRPrepareActivity.java"

# interfaces
.implements Lcom/intsig/adapter/RecycleItemTouchHelper$ItemTouchHelperCallback;
.implements Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;,
        Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$ProgressAnimCallBackImpl;
    }
.end annotation


# static fields
.field private static final 〇OO〇00〇0O:Ljava/lang/String; = "BatchOCRPrepareActivity"


# instance fields
.field private O0O:Landroid/view/View;

.field private O88O:Landroid/widget/LinearLayout;

.field private Oo80:Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback<",
            "Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;",
            ">;"
        }
    .end annotation
.end field

.field private Ooo08:Z

.field private O〇08oOOO0:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

.field private O〇o88o08〇:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

.field private o8o:Landroid/widget/TextView;

.field private o8oOOo:Landroid/view/View;

.field private o8〇OO:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRClientCallback;

.field private oOO〇〇:Landroid/view/View;

.field private oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

.field private ooO:Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$ProgressAnimCallBackImpl;

.field private ooo0〇〇O:Landroid/widget/TextView;

.field private o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

.field private volatile 〇00O0:Z

.field private 〇08〇o0O:Landroid/os/Handler;

.field private 〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/control/ProgressAnimHandler<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O〇〇O8:Landroid/view/View;

.field private 〇o0O:Landroidx/recyclerview/widget/RecyclerView;

.field private 〇〇08O:Landroid/view/View;

.field private 〇〇o〇:Lcom/intsig/camscanner/tools/UndoTool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/tools/UndoTool<",
            "Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/recycler_adapter/ViewHolderFactory;->〇o00〇〇Oo()Lcom/intsig/camscanner/recycler_adapter/ViewHolderFactory;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-direct {v0, v1}, Lcom/intsig/adapter/UniversalRecyclerAdapter;-><init>(Lcom/intsig/adapter/AbsViewHolderFactory;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;

    .line 16
    .line 17
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;-><init>(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 21
    .line 22
    new-instance v0, Landroid/os/Handler;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/〇080;

    .line 25
    .line 26
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/〇080;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇08〇o0O:Landroid/os/Handler;

    .line 33
    .line 34
    new-instance v0, Lcom/intsig/camscanner/tools/UndoTool;

    .line 35
    .line 36
    invoke-direct {v0}, Lcom/intsig/camscanner/tools/UndoTool;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇〇o〇:Lcom/intsig/camscanner/tools/UndoTool;

    .line 40
    .line 41
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/〇o00〇〇Oo;

    .line 42
    .line 43
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 44
    .line 45
    .line 46
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Oo80:Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback;

    .line 47
    .line 48
    const/4 v0, 0x1

    .line 49
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇00O0:Z

    .line 50
    .line 51
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$2;

    .line 52
    .line 53
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$2;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 54
    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇08oOOO0:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 57
    .line 58
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$3;

    .line 59
    .line 60
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$3;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 61
    .line 62
    .line 63
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o8〇OO:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRClientCallback;

    .line 64
    .line 65
    const/4 v0, 0x0

    .line 66
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 67
    .line 68
    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 70
    .line 71
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$ProgressAnimCallBackImpl;

    .line 72
    .line 73
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$ProgressAnimCallBackImpl;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 74
    .line 75
    .line 76
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->ooO:Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$ProgressAnimCallBackImpl;

    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static O00OoO〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Landroid/content/Intent;
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    const-class v1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;

    .line 4
    .line 5
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 6
    .line 7
    .line 8
    const-string p0, "extra_doc_info"

    .line 9
    .line 10
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O0o0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇o88o08〇:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;->o〇0()Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇o88o08〇:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇o88o08〇:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;->〇080()I

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const/4 v1, 0x1

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 24
    .line 25
    const-string v2, "finish activity"

    .line 26
    .line 27
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇00O0:Z

    .line 31
    .line 32
    return-void

    .line 33
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/oO80;

    .line 34
    .line 35
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/oO80;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 39
    .line 40
    .line 41
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇00O0:Z

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oO〇O0O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O80OO()V
    .locals 5

    .line 1
    const v0, 0x7f0a19a3

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇O〇〇O8:Landroid/view/View;

    .line 9
    .line 10
    const v0, 0x7f0a14f1

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o8oOOo:Landroid/view/View;

    .line 18
    .line 19
    const v0, 0x7f0a0c6b

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O0O:Landroid/view/View;

    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    const/16 v1, 0x8

    .line 33
    .line 34
    if-eqz v0, :cond_0

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O0O:Landroid/view/View;

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 39
    .line 40
    .line 41
    :cond_0
    const v0, 0x7f0a1930

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    const v0, 0x7f0a1460

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    check-cast v0, Landroid/widget/TextView;

    .line 59
    .line 60
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->ooo0〇〇O:Landroid/widget/TextView;

    .line 61
    .line 62
    const/4 v2, 0x1

    .line 63
    new-array v2, v2, [Ljava/lang/Object;

    .line 64
    .line 65
    const/4 v3, 0x0

    .line 66
    const-string v4, ""

    .line 67
    .line 68
    aput-object v4, v2, v3

    .line 69
    .line 70
    const v3, 0x7f130716

    .line 71
    .line 72
    .line 73
    invoke-virtual {p0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    .line 79
    .line 80
    const v0, 0x7f0a18dd

    .line 81
    .line 82
    .line 83
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    check-cast v0, Landroid/widget/TextView;

    .line 88
    .line 89
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 93
    .line 94
    .line 95
    const v0, 0x7f0a0c67

    .line 96
    .line 97
    .line 98
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇〇08O:Landroid/view/View;

    .line 103
    .line 104
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    .line 106
    .line 107
    const v0, 0x7f0a0f18

    .line 108
    .line 109
    .line 110
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 115
    .line 116
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 117
    .line 118
    const v0, 0x7f0a0afb

    .line 119
    .line 120
    .line 121
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    check-cast v0, Landroid/widget/LinearLayout;

    .line 126
    .line 127
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O88O:Landroid/widget/LinearLayout;

    .line 128
    .line 129
    const v0, 0x7f0a0b22

    .line 130
    .line 131
    .line 132
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oOO〇〇:Landroid/view/View;

    .line 137
    .line 138
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O880O〇(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇0o88Oo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8O(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oOO8oo0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O8〇o0〇〇8(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o0O0O〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OO0O(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇〇〇OOO〇〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private OO0o(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->OO0o〇〇〇〇0(I)Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/16 v0, 0x69

    .line 8
    .line 9
    invoke-static {p0, p1, v0}, Lcom/intsig/utils/TransitionUtil;->〇o00〇〇Oo(Landroid/app/Activity;Landroid/content/Intent;I)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO8〇O8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 4
    .line 5
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O88()Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem$ItemCallback;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    invoke-interface {v1, v2, v3}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->Oo08(ZLcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem$ItemCallback;)Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/adapter/UniversalRecyclerAdapter;->〇0〇O0088o(Ljava/util/List;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 19
    .line 20
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OO〇〇o0oO(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->OO0o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic OoO〇OOo8o()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic OooO〇(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇oOO80o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O〇00O(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 5
    .line 6
    iget v1, p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;->〇080:I

    .line 7
    .line 8
    iget-object v2, p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;->〇o〇:Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 9
    .line 10
    invoke-interface {v0, v1, v2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->O8(ILcom/intsig/camscanner/mode_ocr/OCRData;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 14
    .line 15
    iget v1, p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;->〇080:I

    .line 16
    .line 17
    iget-object v2, p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;->〇o00〇〇Oo:Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 18
    .line 19
    invoke-virtual {v0, v1, v2}, Lcom/intsig/adapter/UniversalRecyclerAdapter;->〇O00(ILcom/intsig/adapter/AbsRecyclerViewItem;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 23
    .line 24
    iget p1, p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;->〇080:I

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemInserted(I)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o8O〇008()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇o〇OO80oO(Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O〇0o8o8〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->ooo0〇〇O:Landroid/widget/TextView;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    new-array v1, v1, [Ljava/lang/Object;

    .line 5
    .line 6
    new-instance v2, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v3, ""

    .line 12
    .line 13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇o88o08〇:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 17
    .line 18
    invoke-virtual {v3}, Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;->〇o〇()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    const/4 v3, 0x0

    .line 30
    aput-object v2, v1, v3

    .line 31
    .line 32
    const v2, 0x7f130716

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O0o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇〇o8O()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o0O0O〇〇〇0()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 2
    .line 3
    xor-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oOO〇〇:Landroid/view/View;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    const/16 v3, 0x8

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const/16 v0, 0x8

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇〇08O:Landroid/view/View;

    .line 22
    .line 23
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 24
    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    const/16 v1, 0x8

    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_1
    const/4 v1, 0x0

    .line 31
    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o8oOOo:Landroid/view/View;

    .line 35
    .line 36
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 37
    .line 38
    if-eqz v1, :cond_2

    .line 39
    .line 40
    const/16 v1, 0x8

    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_2
    const/4 v1, 0x0

    .line 44
    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇O〇〇O8:Landroid/view/View;

    .line 48
    .line 49
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 50
    .line 51
    if-eqz v1, :cond_3

    .line 52
    .line 53
    const/16 v1, 0x8

    .line 54
    .line 55
    goto :goto_3

    .line 56
    :cond_3
    const/4 v1, 0x0

    .line 57
    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 58
    .line 59
    .line 60
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    if-eqz v0, :cond_4

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O0O:Landroid/view/View;

    .line 67
    .line 68
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 69
    .line 70
    .line 71
    goto :goto_4

    .line 72
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O0O:Landroid/view/View;

    .line 73
    .line 74
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 75
    .line 76
    if-eqz v1, :cond_5

    .line 77
    .line 78
    const/16 v2, 0x8

    .line 79
    .line 80
    :cond_5
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 81
    .line 82
    .line 83
    :goto_4
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o8o:Landroid/widget/TextView;

    .line 84
    .line 85
    if-eqz v0, :cond_7

    .line 86
    .line 87
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 88
    .line 89
    if-eqz v1, :cond_6

    .line 90
    .line 91
    const v1, 0x7f130534

    .line 92
    .line 93
    .line 94
    goto :goto_5

    .line 95
    :cond_6
    const v1, 0x7f130227

    .line 96
    .line 97
    .line 98
    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 99
    .line 100
    .line 101
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇80〇()V

    .line 102
    .line 103
    .line 104
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 105
    .line 106
    if-nez v0, :cond_9

    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇0o88Oo〇()V

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇08〇o0O:Landroid/os/Handler;

    .line 112
    .line 113
    const/16 v1, 0x2711

    .line 114
    .line 115
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 116
    .line 117
    .line 118
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 119
    .line 120
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->OO0o〇〇()I

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    if-nez v0, :cond_8

    .line 125
    .line 126
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->〇o〇()V

    .line 131
    .line 132
    .line 133
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 134
    .line 135
    .line 136
    goto :goto_6

    .line 137
    :cond_8
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 142
    .line 143
    invoke-interface {v1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->〇〇888()Ljava/util/List;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->oO80(Ljava/util/Collection;)V

    .line 148
    .line 149
    .line 150
    :cond_9
    :goto_6
    return-void
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o0OO()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;"
        }
    .end annotation

    .line 1
    const-class v0, Lcom/intsig/camscanner/tools/OCRDataListHolder;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/singleton/Singleton;->〇080(Ljava/lang/Class;)Lcom/intsig/singleton/Singleton;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/tools/OCRDataListHolder;

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tools/OCRDataListHolder;->〇o00〇〇Oo(Z)Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o0Oo(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇0o8o8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇00O(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o88o88()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇00O0:Z

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    const/4 v0, 0x0

    .line 23
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇00O0:Z

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/Oo08;

    .line 30
    .line 31
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/Oo08;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 35
    .line 36
    .line 37
    nop

    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic o8O〇008()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇o〇88()V

    .line 13
    .line 14
    .line 15
    :cond_1
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private oOO8oo0()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O0O:Landroid/view/View;

    .line 8
    .line 9
    const/16 v1, 0x8

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private oO〇O0O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/O8;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/O8;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 6
    .line 7
    .line 8
    const-wide/16 v2, 0x12c

    .line 9
    .line 10
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic o〇08oO80o(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇OoO0()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    const/16 v1, 0x6e

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic o〇o08〇(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o0O0O〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0o0oO〇〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;-><init>(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->ooO:Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$ProgressAnimCallBackImpl;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oOO8O8(Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->ooO:Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$ProgressAnimCallBackImpl;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/control/ProgressAnimHandleCallbackImpl;->o〇0(Lcom/intsig/camscanner/control/ProgressAnimHandler;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇0o88Oo〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇〇o〇:Lcom/intsig/camscanner/tools/UndoTool;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/tools/UndoTool;->〇o00〇〇Oo()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇80〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/adapter/UniversalRecyclerAdapter;->〇O〇()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_2

    .line 19
    .line 20
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    check-cast v1, Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 25
    .line 26
    instance-of v2, v1, Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem;

    .line 27
    .line 28
    if-nez v2, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    check-cast v1, Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem;

    .line 32
    .line 33
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 34
    .line 35
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem;->〇〇808〇(Z)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 40
    .line 41
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇8o0o0()V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->o〇0(I)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/〇o〇;

    .line 11
    .line 12
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/〇o〇;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DialogUtils;->O0o〇〇Oo(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇O〇()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    sget-object v1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_NEW_OCR:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    sget-object v1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_OCR:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 46
    .line 47
    .line 48
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o8〇OO:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRClientCallback;

    .line 51
    .line 52
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇08oOOO0:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 53
    .line 54
    invoke-interface {v0, v1, v2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->o〇0(Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRClientCallback;Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇8〇〇8o()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O88O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/nativelib/OcrLanguage$LangMode;->OCR:Lcom/intsig/nativelib/OcrLanguage$LangMode;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/ocrapi/OcrLanguagesCompat;->〇o00〇〇Oo(Lcom/intsig/nativelib/OcrLanguage$LangMode;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, ","

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    array-length v1, v0

    .line 19
    const/4 v2, 0x0

    .line 20
    const/4 v3, 0x0

    .line 21
    const/4 v4, 0x0

    .line 22
    :goto_0
    if-ge v3, v1, :cond_3

    .line 23
    .line 24
    aget-object v5, v0, v3

    .line 25
    .line 26
    const-string v6, "zh"

    .line 27
    .line 28
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result v6

    .line 32
    if-eqz v6, :cond_0

    .line 33
    .line 34
    const-string v5, "zh-s"

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_0
    const-string v6, "zht"

    .line 38
    .line 39
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v6

    .line 43
    if-eqz v6, :cond_1

    .line 44
    .line 45
    const-string v5, "zh-t"

    .line 46
    .line 47
    :cond_1
    :goto_1
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 48
    .line 49
    .line 50
    move-result-object v6

    .line 51
    const v7, 0x7f0d045d

    .line 52
    .line 53
    .line 54
    iget-object v8, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O88O:Landroid/widget/LinearLayout;

    .line 55
    .line 56
    invoke-virtual {v6, v7, v8, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v6

    .line 60
    check-cast v6, Landroid/widget/TextView;

    .line 61
    .line 62
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 63
    .line 64
    .line 65
    move-result-object v7

    .line 66
    if-lez v4, :cond_2

    .line 67
    .line 68
    instance-of v8, v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 69
    .line 70
    if-eqz v8, :cond_2

    .line 71
    .line 72
    move-object v8, v7

    .line 73
    check-cast v8, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 74
    .line 75
    const/4 v9, 0x6

    .line 76
    invoke-static {p0, v9}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 77
    .line 78
    .line 79
    move-result v9

    .line 80
    iput v9, v8, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 81
    .line 82
    invoke-virtual {v6, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    .line 84
    .line 85
    :cond_2
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    .line 87
    .line 88
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O88O:Landroid/widget/LinearLayout;

    .line 89
    .line 90
    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 91
    .line 92
    .line 93
    add-int/lit8 v4, v4, 0x1

    .line 94
    .line 95
    add-int/lit8 v3, v3, 0x1

    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_3
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇OoO0o0()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇OoO0()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O〇〇o8O()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-lez v0, :cond_0

    .line 16
    .line 17
    if-lez v1, :cond_0

    .line 18
    .line 19
    sub-int/2addr v0, v2

    .line 20
    add-int/2addr v1, v2

    .line 21
    div-int/2addr v0, v1

    .line 22
    :cond_0
    const/4 v0, 0x3

    .line 23
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O8〇o0〇〇8(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇oOO80o(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->〇o〇(I)Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    invoke-virtual {v1, p1, v2}, Lcom/intsig/adapter/UniversalRecyclerAdapter;->〇〇8O0〇8(IZ)Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;

    .line 15
    .line 16
    const/4 v3, 0x0

    .line 17
    invoke-direct {v2, v3}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;-><init>(Lcom/intsig/camscanner/mode_ocr/〇80〇808〇O;)V

    .line 18
    .line 19
    .line 20
    iput p1, v2, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;->〇080:I

    .line 21
    .line 22
    iput-object v1, v2, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;->〇o00〇〇Oo:Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 23
    .line 24
    iput-object v0, v2, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$UndoData;->〇o〇:Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇〇o〇:Lcom/intsig/camscanner/tools/UndoTool;

    .line 27
    .line 28
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/tools/UndoTool;->Oo08(Ljava/lang/Object;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇〇o〇:Lcom/intsig/camscanner/tools/UndoTool;

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Oo80:Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback;

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/tools/UndoTool;->o〇0(Lcom/intsig/camscanner/tools/UndoTool$UndoToolCallback;)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇〇o〇:Lcom/intsig/camscanner/tools/UndoTool;

    .line 39
    .line 40
    const v0, 0x7f0a1004

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const/16 v1, 0xbb8

    .line 48
    .line 49
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/tools/UndoTool;->〇〇888(Landroid/view/View;I)V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇08〇o0O:Landroid/os/Handler;

    .line 53
    .line 54
    const/16 v0, 0x2711

    .line 55
    .line 56
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 57
    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇08〇o0O:Landroid/os/Handler;

    .line 60
    .line 61
    int-to-long v1, v1

    .line 62
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->OO8〇O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇ooO〇000(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o88o88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o〇88()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OoO0o0()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Lcom/intsig/recycleviewLayoutmanager/TrycatchGridLayoutManager;

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 8
    .line 9
    invoke-direct {v1, v2, v0}, Lcom/intsig/recycleviewLayoutmanager/TrycatchGridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 26
    .line 27
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O88()Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem$ItemCallback;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    invoke-interface {v1, v2, v3}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->Oo08(ZLcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem$ItemCallback;)Ljava/util/List;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/adapter/UniversalRecyclerAdapter;->〇0〇O0088o(Ljava/util/List;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 45
    .line 46
    .line 47
    new-instance v0, Lcom/intsig/adapter/RecycleItemTouchHelper;

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oo8ooo8O:Lcom/intsig/adapter/UniversalRecyclerAdapter;

    .line 50
    .line 51
    invoke-direct {v0, v1, p0}, Lcom/intsig/adapter/RecycleItemTouchHelper;-><init>(Lcom/intsig/adapter/UniversalRecyclerAdapter;Lcom/intsig/adapter/RecycleItemTouchHelper$ItemTouchHelperCallback;)V

    .line 52
    .line 53
    .line 54
    new-instance v1, Landroidx/recyclerview/widget/ItemTouchHelper;

    .line 55
    .line 56
    invoke-direct {v1, v0}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 60
    .line 61
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇o〇OO80oO(Landroid/os/Message;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget p1, p1, Landroid/os/Message;->what:I

    .line 10
    .line 11
    const/16 v0, 0x2711

    .line 12
    .line 13
    if-ne p1, v0, :cond_2

    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 16
    .line 17
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->OO0o〇〇()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-nez p1, :cond_1

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->〇o〇()V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 31
    .line 32
    .line 33
    :cond_1
    const/4 p1, 0x1

    .line 34
    return p1

    .line 35
    :cond_2
    return v1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic 〇〇〇OOO〇〇(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    const/4 p2, 0x1

    .line 4
    const/16 v0, 0x65

    .line 5
    .line 6
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/ocrapi/OcrIntent;->O8(Landroid/app/Activity;II)V

    .line 7
    .line 8
    .line 9
    sget-object p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 10
    .line 11
    const-string p2, "User Operation: go to ocr language setting"

    .line 12
    .line 13
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O88()Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem$ItemCallback;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity$1;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o800o8O()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO8oO0o〇(JF)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->O8ooOoo〇(J)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 7
    .line 8
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇00〇8(F)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇oO〇oo8o(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇0o0oO〇〇0()V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇〇0〇()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const v0, 0x7f0a18dd

    .line 6
    .line 7
    .line 8
    if-ne p1, v0, :cond_0

    .line 9
    .line 10
    sget-object p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 11
    .line 12
    const-string v0, "upgrade"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 18
    .line 19
    invoke-direct {p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 20
    .line 21
    .line 22
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_BATCH_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const/16 v0, 0x67

    .line 29
    .line 30
    invoke-static {p0, p1, v0}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->o〇0OOo〇0(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const v0, 0x7f0a0c67

    .line 35
    .line 36
    .line 37
    const/4 v1, 0x1

    .line 38
    if-ne p1, v0, :cond_1

    .line 39
    .line 40
    sget-object p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 41
    .line 42
    const-string v0, "start batchocr"

    .line 43
    .line 44
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 48
    .line 49
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->OO0o〇〇()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    const-string v3, ""

    .line 62
    .line 63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-static {p1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    new-array p1, v1, [Landroid/util/Pair;

    .line 74
    .line 75
    new-instance v1, Landroid/util/Pair;

    .line 76
    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    const-string v2, "num"

    .line 93
    .line 94
    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 95
    .line 96
    .line 97
    const/4 v0, 0x0

    .line 98
    aput-object v1, p1, v0

    .line 99
    .line 100
    const-string v0, "CSBatchOcr"

    .line 101
    .line 102
    const-string v1, "ocr_recognize"

    .line 103
    .line 104
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 105
    .line 106
    .line 107
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇8o0o0()V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_1
    const v0, 0x7f0a0b22

    .line 112
    .line 113
    .line 114
    if-ne p1, v0, :cond_2

    .line 115
    .line 116
    sget-object p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 117
    .line 118
    const-string v0, "click ocr language"

    .line 119
    .line 120
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 124
    .line 125
    const/16 v0, 0x66

    .line 126
    .line 127
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/ocrapi/OcrIntent;->O8(Landroid/app/Activity;II)V

    .line 128
    .line 129
    .line 130
    :cond_2
    :goto_0
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public getCurrentActivity()Landroid/app/Activity;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    new-instance p1, Lcom/intsig/camscanner/mode_ocr/o〇0;

    .line 2
    .line 3
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mode_ocr/o〇0;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 4
    .line 5
    .line 6
    const v0, 0x7f130227

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0, v0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇O0o〇〇o(ILandroid/view/View$OnClickListener;)Landroid/widget/TextView;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o8o:Landroid/widget/TextView;

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->O80OO()V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 19
    .line 20
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/〇〇888;

    .line 25
    .line 26
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/〇〇888;-><init>(Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;)V

    .line 27
    .line 28
    .line 29
    invoke-interface {p1, v0, v1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->〇O8o08O(Landroid/content/Intent;Ljava/lang/Runnable;)V

    .line 30
    .line 31
    .line 32
    sget-object p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 33
    .line 34
    const-string v0, "onCreate"

    .line 35
    .line 36
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o88o88()V

    .line 40
    .line 41
    .line 42
    const-string p1, "CSBatchOcr"

    .line 43
    .line 44
    invoke-static {p1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public oO80(II)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "fromPosition="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, " toPosition="

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 32
    .line 33
    invoke-interface {v0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->oO80(II)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p3    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "onActivityResult requestCode="

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v2, " resultCode="

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/16 v0, 0x66

    .line 35
    .line 36
    if-ne p1, v0, :cond_0

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/16 v0, 0x65

    .line 40
    .line 41
    if-ne p1, v0, :cond_1

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇8o0o0()V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    const/16 v0, 0x68

    .line 48
    .line 49
    if-ne p1, v0, :cond_5

    .line 50
    .line 51
    const/4 p1, -0x1

    .line 52
    if-ne p2, p1, :cond_3

    .line 53
    .line 54
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 55
    .line 56
    invoke-interface {p2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->Oooo8o0〇()Z

    .line 57
    .line 58
    .line 59
    move-result p2

    .line 60
    if-eqz p2, :cond_2

    .line 61
    .line 62
    if-eqz p3, :cond_2

    .line 63
    .line 64
    new-instance p2, Landroid/content/Intent;

    .line 65
    .line 66
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    const-class v1, Lcom/intsig/camscanner/DocumentActivity;

    .line 71
    .line 72
    const-string v2, "com.intsig.camscanner.NEW_BATOCR_DOC"

    .line 73
    .line 74
    invoke-direct {p2, v2, v0, p0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    .line 76
    .line 77
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->〇o〇()V

    .line 82
    .line 83
    .line 84
    invoke-virtual {p0, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 85
    .line 86
    .line 87
    :cond_2
    invoke-virtual {p0, p1, p3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_3
    if-eqz p3, :cond_4

    .line 95
    .line 96
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o0OO()Ljava/util/List;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 101
    .line 102
    .line 103
    move-result-object p2

    .line 104
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->〇80〇808〇O(Ljava/util/List;)V

    .line 105
    .line 106
    .line 107
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o88o88()V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_5
    const/16 p2, 0x69

    .line 112
    .line 113
    if-ne p1, p2, :cond_6

    .line 114
    .line 115
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 116
    .line 117
    invoke-interface {p1, p3}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->〇o00〇〇Oo(Landroid/content/Intent;)V

    .line 118
    .line 119
    .line 120
    :cond_6
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oOO8oo0()V

    .line 121
    .line 122
    .line 123
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    sget-object p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 5
    .line 6
    const-string v0, "onConfigurationChanged"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->oO〇O0O()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onResume()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->〇8〇〇8o()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public ooO〇00O()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d0025

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇Oo〇O()Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->Ooo08:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o0O0O〇〇〇0()V

    .line 7
    .line 8
    .line 9
    return v1

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;->o〇oO:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;

    .line 11
    .line 12
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;->〇80〇808〇O()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    return v1

    .line 19
    :cond_1
    invoke-super {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇Oo〇O()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇o(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇oo〇()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

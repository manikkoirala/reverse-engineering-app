.class public Lcom/intsig/camscanner/mode_ocr/OCRData;
.super Ljava/lang/Object;
.source "OCRData.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public O0O:I

.field O88O:Z

.field private O8o08O8O:Ljava/lang/String;

.field public OO:Ljava/lang/String;

.field private OO〇00〇8oO:I

.field public final o0:Ljava/lang/String;

.field public o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

.field public o8oOOo:I

.field private o8〇OO0〇0o:Ljava/lang/String;

.field public oOO〇〇:Z

.field private oOo0:Z

.field private oOo〇8o008:Z

.field public oo8ooo8O:[I

.field public ooo0〇〇O:I

.field private o〇00O:Ljava/lang/String;

.field public o〇oO:[I

.field private final 〇080OO8〇0:Ljava/lang/String;

.field private 〇08O〇00〇o:Ljava/lang/String;

.field public 〇08〇o0O:J

.field private 〇0O:Ljava/lang/String;

.field public 〇8〇oO〇〇8o:I

.field public 〇OOo8〇0:Ljava/lang/String;

.field public 〇O〇〇O8:I

.field private 〇o0O:Z

.field public 〇〇08O:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/OCRData$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/OCRData$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/OCRData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "OCRData"

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o0:Ljava/lang/String;

    const/4 v0, 0x0

    .line 18
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    const/4 v1, 0x1

    .line 19
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo0:Z

    .line 20
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO〇00〇8oO:I

    const-string v2, ""

    .line 21
    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8〇OO0〇0o:Ljava/lang/String;

    const/4 v2, -0x1

    .line 22
    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇8〇oO〇〇8o:I

    .line 23
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->ooo0〇〇O:I

    .line 24
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇〇08O:I

    const/16 v2, 0x64

    .line 25
    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O0O:I

    .line 26
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 27
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇O〇〇O8:I

    .line 28
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOO〇〇:Z

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO:Ljava/lang/String;

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08O〇00〇o:Ljava/lang/String;

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇080OO8〇0:Ljava/lang/String;

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO〇00〇8oO:I

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8〇OO0〇0o:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇8〇oO〇〇8o:I

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->ooo0〇〇O:I

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇〇08O:I

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O0O:I

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o0O:Z

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O88O:Z

    .line 44
    const-class v2, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08〇o0O:J

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result p1

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :cond_3
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOO〇〇:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "OCRData"

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o0:Ljava/lang/String;

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    const/4 v1, 0x1

    .line 4
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo0:Z

    const-string v2, ""

    .line 5
    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8〇OO0〇0o:Ljava/lang/String;

    const/4 v2, -0x1

    .line 6
    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇8〇oO〇〇8o:I

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->ooo0〇〇O:I

    .line 8
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇〇08O:I

    const/16 v2, 0x64

    .line 9
    iput v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O0O:I

    .line 10
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇O〇〇O8:I

    .line 12
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOO〇〇:Z

    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08O〇00〇o:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇080OO8〇0:Ljava/lang/String;

    .line 15
    iput p3, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO〇00〇8oO:I

    return-void
.end method

.method private 〇080()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 4
    .line 5
    const/16 v2, 0x5a

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x1

    .line 9
    if-eq v1, v2, :cond_1

    .line 10
    .line 11
    const/16 v2, 0x10e

    .line 12
    .line 13
    if-ne v1, v2, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 17
    .line 18
    aget v2, v1, v3

    .line 19
    .line 20
    iput v2, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 21
    .line 22
    aget v1, v1, v4

    .line 23
    .line 24
    iput v1, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 28
    .line 29
    aget v2, v1, v4

    .line 30
    .line 31
    iput v2, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 32
    .line 33
    aget v1, v1, v3

    .line 34
    .line 35
    iput v1, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 36
    .line 37
    :goto_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇080OO8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8ooOoo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8〇o(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08O〇00〇o:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO0o〇〇〇〇0()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OOO〇O0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oo8Oo00oo(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method OoO8()Ljava/lang/String;
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 7
    .line 8
    if-eqz v1, :cond_3

    .line 9
    .line 10
    iget-object v1, v1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 11
    .line 12
    if-eqz v1, :cond_3

    .line 13
    .line 14
    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_3

    .line 23
    .line 24
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;

    .line 29
    .line 30
    iget-object v3, v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->lines:Ljava/util/List;

    .line 31
    .line 32
    if-eqz v3, :cond_0

    .line 33
    .line 34
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    if-lez v3, :cond_0

    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-lez v3, :cond_1

    .line 45
    .line 46
    const-string v3, "\r\n"

    .line 47
    .line 48
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    :cond_1
    iget-object v2, v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->lines:Ljava/util/List;

    .line 52
    .line 53
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-eqz v3, :cond_0

    .line 62
    .line 63
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    check-cast v3, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;

    .line 68
    .line 69
    if-eqz v3, :cond_2

    .line 70
    .line 71
    iget-object v4, v3, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->text:Ljava/lang/String;

    .line 72
    .line 73
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    if-nez v4, :cond_2

    .line 78
    .line 79
    iget-object v3, v3, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->text:Ljava/lang/String;

    .line 80
    .line 81
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    return-object v0
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public Oooo8o0〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO〇00〇8oO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public clone()Ljava/lang/Object;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->clone()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 16
    .line 17
    iput-object v1, v0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 18
    .line 19
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 20
    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    array-length v2, v1

    .line 24
    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([II)[I

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    iput-object v1, v0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 29
    .line 30
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 31
    .line 32
    if-eqz v1, :cond_2

    .line 33
    .line 34
    array-length v2, v1

    .line 35
    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([II)[I

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    iput-object v1, v0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 40
    .line 41
    :cond_2
    return-object v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    if-eqz p1, :cond_3

    .line 7
    .line 8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    if-eq v2, v3, :cond_1

    .line 17
    .line 18
    goto/16 :goto_1

    .line 19
    .line 20
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 21
    .line 22
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 23
    .line 24
    iget-boolean v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 25
    .line 26
    if-ne v2, v3, :cond_2

    .line 27
    .line 28
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO〇00〇8oO:I

    .line 29
    .line 30
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO〇00〇8oO:I

    .line 31
    .line 32
    if-ne v2, v3, :cond_2

    .line 33
    .line 34
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇8〇oO〇〇8o:I

    .line 35
    .line 36
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇8〇oO〇〇8o:I

    .line 37
    .line 38
    if-ne v2, v3, :cond_2

    .line 39
    .line 40
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->ooo0〇〇O:I

    .line 41
    .line 42
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->ooo0〇〇O:I

    .line 43
    .line 44
    if-ne v2, v3, :cond_2

    .line 45
    .line 46
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇〇08O:I

    .line 47
    .line 48
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇〇08O:I

    .line 49
    .line 50
    if-ne v2, v3, :cond_2

    .line 51
    .line 52
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O0O:I

    .line 53
    .line 54
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->O0O:I

    .line 55
    .line 56
    if-ne v2, v3, :cond_2

    .line 57
    .line 58
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 59
    .line 60
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 61
    .line 62
    if-ne v2, v3, :cond_2

    .line 63
    .line 64
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o0O:Z

    .line 65
    .line 66
    iget-boolean v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o0O:Z

    .line 67
    .line 68
    if-ne v2, v3, :cond_2

    .line 69
    .line 70
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O88O:Z

    .line 71
    .line 72
    iget-boolean v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->O88O:Z

    .line 73
    .line 74
    if-ne v2, v3, :cond_2

    .line 75
    .line 76
    const-string v2, "OCRData"

    .line 77
    .line 78
    invoke-virtual {v2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    if-eqz v2, :cond_2

    .line 83
    .line 84
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 85
    .line 86
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 87
    .line 88
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    if-eqz v2, :cond_2

    .line 93
    .line 94
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO:Ljava/lang/String;

    .line 95
    .line 96
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO:Ljava/lang/String;

    .line 97
    .line 98
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    if-eqz v2, :cond_2

    .line 103
    .line 104
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08O〇00〇o:Ljava/lang/String;

    .line 105
    .line 106
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08O〇00〇o:Ljava/lang/String;

    .line 107
    .line 108
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    move-result v2

    .line 112
    if-eqz v2, :cond_2

    .line 113
    .line 114
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇080OO8〇0:Ljava/lang/String;

    .line 115
    .line 116
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇080OO8〇0:Ljava/lang/String;

    .line 117
    .line 118
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 119
    .line 120
    .line 121
    move-result v2

    .line 122
    if-eqz v2, :cond_2

    .line 123
    .line 124
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 125
    .line 126
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 127
    .line 128
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    move-result v2

    .line 132
    if-eqz v2, :cond_2

    .line 133
    .line 134
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8〇OO0〇0o:Ljava/lang/String;

    .line 135
    .line 136
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8〇OO0〇0o:Ljava/lang/String;

    .line 137
    .line 138
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 139
    .line 140
    .line 141
    move-result v2

    .line 142
    if-eqz v2, :cond_2

    .line 143
    .line 144
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 145
    .line 146
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 147
    .line 148
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 149
    .line 150
    .line 151
    move-result v2

    .line 152
    if-eqz v2, :cond_2

    .line 153
    .line 154
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 155
    .line 156
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 157
    .line 158
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    .line 159
    .line 160
    .line 161
    move-result v2

    .line 162
    if-eqz v2, :cond_2

    .line 163
    .line 164
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 165
    .line 166
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 167
    .line 168
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    .line 169
    .line 170
    .line 171
    move-result v2

    .line 172
    if-eqz v2, :cond_2

    .line 173
    .line 174
    iget-wide v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08〇o0O:J

    .line 175
    .line 176
    iget-wide v4, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08〇o0O:J

    .line 177
    .line 178
    cmp-long v6, v2, v4

    .line 179
    .line 180
    if-nez v6, :cond_2

    .line 181
    .line 182
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOO〇〇:Z

    .line 183
    .line 184
    iget-boolean p1, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOO〇〇:Z

    .line 185
    .line 186
    if-ne v2, p1, :cond_2

    .line 187
    .line 188
    goto :goto_0

    .line 189
    :cond_2
    const/4 v0, 0x0

    .line 190
    :goto_0
    return v0

    .line 191
    :cond_3
    :goto_1
    return v1
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public hashCode()I
    .locals 3

    .line 1
    const/16 v0, 0x13

    .line 2
    .line 3
    new-array v0, v0, [Ljava/lang/Object;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    const-string v2, "OCRData"

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO:Ljava/lang/String;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08O〇00〇o:Ljava/lang/String;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇080OO8〇0:Ljava/lang/String;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 36
    .line 37
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    const/4 v2, 0x6

    .line 42
    aput-object v1, v0, v2

    .line 43
    .line 44
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO〇00〇8oO:I

    .line 45
    .line 46
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const/4 v2, 0x7

    .line 51
    aput-object v1, v0, v2

    .line 52
    .line 53
    const/16 v1, 0x8

    .line 54
    .line 55
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8〇OO0〇0o:Ljava/lang/String;

    .line 56
    .line 57
    aput-object v2, v0, v1

    .line 58
    .line 59
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇8〇oO〇〇8o:I

    .line 60
    .line 61
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    const/16 v2, 0x9

    .line 66
    .line 67
    aput-object v1, v0, v2

    .line 68
    .line 69
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->ooo0〇〇O:I

    .line 70
    .line 71
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    const/16 v2, 0xa

    .line 76
    .line 77
    aput-object v1, v0, v2

    .line 78
    .line 79
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇〇08O:I

    .line 80
    .line 81
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    const/16 v2, 0xb

    .line 86
    .line 87
    aput-object v1, v0, v2

    .line 88
    .line 89
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O0O:I

    .line 90
    .line 91
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 92
    .line 93
    .line 94
    move-result-object v1

    .line 95
    const/16 v2, 0xc

    .line 96
    .line 97
    aput-object v1, v0, v2

    .line 98
    .line 99
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 100
    .line 101
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    const/16 v2, 0xd

    .line 106
    .line 107
    aput-object v1, v0, v2

    .line 108
    .line 109
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o0O:Z

    .line 110
    .line 111
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    const/16 v2, 0xe

    .line 116
    .line 117
    aput-object v1, v0, v2

    .line 118
    .line 119
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O88O:Z

    .line 120
    .line 121
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    const/16 v2, 0xf

    .line 126
    .line 127
    aput-object v1, v0, v2

    .line 128
    .line 129
    const/16 v1, 0x10

    .line 130
    .line 131
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 132
    .line 133
    aput-object v2, v0, v1

    .line 134
    .line 135
    iget-wide v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08〇o0O:J

    .line 136
    .line 137
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 138
    .line 139
    .line 140
    move-result-object v1

    .line 141
    const/16 v2, 0x11

    .line 142
    .line 143
    aput-object v1, v0, v2

    .line 144
    .line 145
    iget-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOO〇〇:Z

    .line 146
    .line 147
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    const/16 v2, 0x12

    .line 152
    .line 153
    aput-object v1, v0, v2

    .line 154
    .line 155
    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    .line 156
    .line 157
    .line 158
    move-result v0

    .line 159
    mul-int/lit8 v0, v0, 0x1f

    .line 160
    .line 161
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 162
    .line 163
    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    .line 164
    .line 165
    .line 166
    move-result v1

    .line 167
    add-int/2addr v0, v1

    .line 168
    mul-int/lit8 v0, v0, 0x1f

    .line 169
    .line 170
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 171
    .line 172
    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    .line 173
    .line 174
    .line 175
    move-result v1

    .line 176
    add-int/2addr v0, v1

    .line 177
    return v0
.end method

.method public o0ooO(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oo88o8O()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oo〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇0()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇oOO8O8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇80〇808〇O()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method o〇0OOo〇0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    :cond_0
    const/4 v0, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const/4 v0, 0x0

    .line 20
    :goto_0
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇00〇8(Z)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇8(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇O8〇〇o()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇00O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇〇0〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08O〇00〇o:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇080OO8〇0:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 29
    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO〇00〇8oO:I

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8〇OO0〇0o:Ljava/lang/String;

    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇8〇oO〇〇8o:I

    .line 42
    .line 43
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 44
    .line 45
    .line 46
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->ooo0〇〇O:I

    .line 47
    .line 48
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    .line 50
    .line 51
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇〇08O:I

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    .line 55
    .line 56
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O0O:I

    .line 57
    .line 58
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    .line 60
    .line 61
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 62
    .line 63
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    .line 65
    .line 66
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o0O:Z

    .line 67
    .line 68
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 69
    .line 70
    .line 71
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O88O:Z

    .line 72
    .line 73
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 77
    .line 78
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 79
    .line 80
    .line 81
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 82
    .line 83
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 84
    .line 85
    .line 86
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 87
    .line 88
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 89
    .line 90
    .line 91
    iget-wide v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08〇o0O:J

    .line 92
    .line 93
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 94
    .line 95
    .line 96
    iget-boolean p2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOO〇〇:Z

    .line 97
    .line 98
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 99
    .line 100
    .line 101
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇00()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o0O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0000OOO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O88O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇00〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->O88O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇0〇O0088o()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 19
    .line 20
    const-class v2, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 21
    .line 22
    invoke-static {v0, v2}, Lcom/intsig/okgo/utils/GsonUtils;->o〇0(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    const-string v2, "OCRData"

    .line 29
    .line 30
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    :goto_0
    return-object v1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇80〇808〇O()Ljava/lang/String;
    .locals 10

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇oOO8O8()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_5

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 13
    .line 14
    iget-object v1, v1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    const/4 v3, 0x0

    .line 18
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    if-ge v3, v4, :cond_5

    .line 23
    .line 24
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    check-cast v4, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;

    .line 29
    .line 30
    if-eqz v4, :cond_4

    .line 31
    .line 32
    const/4 v5, 0x0

    .line 33
    const/4 v6, 0x0

    .line 34
    :goto_1
    iget-object v7, v4, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->lines:Ljava/util/List;

    .line 35
    .line 36
    invoke-interface {v7}, Ljava/util/List;->size()I

    .line 37
    .line 38
    .line 39
    move-result v7

    .line 40
    if-ge v5, v7, :cond_4

    .line 41
    .line 42
    iget-object v7, v4, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->lines:Ljava/util/List;

    .line 43
    .line 44
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v7

    .line 48
    check-cast v7, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;

    .line 49
    .line 50
    iget-object v8, v7, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->text:Ljava/lang/String;

    .line 51
    .line 52
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 53
    .line 54
    .line 55
    move-result v8

    .line 56
    if-nez v8, :cond_3

    .line 57
    .line 58
    iget-object v8, v7, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->poly:[F

    .line 59
    .line 60
    if-eqz v8, :cond_3

    .line 61
    .line 62
    array-length v8, v8

    .line 63
    const/16 v9, 0x8

    .line 64
    .line 65
    if-ne v8, v9, :cond_3

    .line 66
    .line 67
    iget-object v8, v7, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->text:Ljava/lang/String;

    .line 68
    .line 69
    invoke-virtual {v7}, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->isSelectText()Z

    .line 70
    .line 71
    .line 72
    move-result v7

    .line 73
    if-eqz v7, :cond_2

    .line 74
    .line 75
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v7

    .line 79
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 80
    .line 81
    .line 82
    move-result v7

    .line 83
    if-nez v7, :cond_1

    .line 84
    .line 85
    iget-boolean v7, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOO〇〇:Z

    .line 86
    .line 87
    const-string v9, "\n"

    .line 88
    .line 89
    if-nez v7, :cond_0

    .line 90
    .line 91
    new-instance v6, Ljava/lang/StringBuilder;

    .line 92
    .line 93
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    .line 95
    .line 96
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v8

    .line 106
    goto :goto_2

    .line 107
    :cond_0
    if-nez v6, :cond_1

    .line 108
    .line 109
    new-instance v6, Ljava/lang/StringBuilder;

    .line 110
    .line 111
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v8

    .line 124
    :cond_1
    :goto_2
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    const/4 v6, 0x1

    .line 128
    goto :goto_3

    .line 129
    :cond_2
    const/4 v6, 0x0

    .line 130
    :cond_3
    :goto_3
    add-int/lit8 v5, v5, 0x1

    .line 131
    .line 132
    goto :goto_1

    .line 133
    :cond_4
    add-int/lit8 v3, v3, 0x1

    .line 134
    .line 135
    goto :goto_0

    .line 136
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    return-object v0
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇O888o0o()Ljava/lang/String;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->OoO8()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0O:Ljava/lang/String;

    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method 〇o(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 6
    .line 7
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x1

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->o0ooO(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 18
    .line 19
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-nez p1, :cond_2

    .line 24
    .line 25
    :try_start_0
    const-class p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 26
    .line 27
    invoke-static {p2, p1}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 32
    .line 33
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->isSupportCurrentVer()Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-eqz p1, :cond_1

    .line 40
    .line 41
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oOo〇8o008:Z

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :catch_0
    move-exception p1

    .line 48
    const-string p2, "OCRData"

    .line 49
    .line 50
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    .line 52
    .line 53
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇0OOo〇0()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇o00〇〇Oo()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 2
    .line 3
    if-eqz v0, :cond_6

    .line 4
    .line 5
    array-length v0, v0

    .line 6
    const/4 v1, 0x2

    .line 7
    if-ne v0, v1, :cond_6

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 10
    .line 11
    if-eqz v0, :cond_6

    .line 12
    .line 13
    array-length v0, v0

    .line 14
    if-eq v0, v1, :cond_0

    .line 15
    .line 16
    goto :goto_3

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 18
    .line 19
    if-eqz v0, :cond_6

    .line 20
    .line 21
    iget-object v1, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 22
    .line 23
    if-nez v1, :cond_1

    .line 24
    .line 25
    goto :goto_3

    .line 26
    :cond_1
    iget v1, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 27
    .line 28
    const/16 v2, 0x5a

    .line 29
    .line 30
    if-eq v1, v2, :cond_3

    .line 31
    .line 32
    const/16 v2, 0x10e

    .line 33
    .line 34
    if-ne v1, v2, :cond_2

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_2
    iget v1, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 38
    .line 39
    iget v0, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_3
    :goto_0
    iget v1, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 43
    .line 44
    iget v0, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 45
    .line 46
    :goto_1
    new-instance v2, Landroid/graphics/RectF;

    .line 47
    .line 48
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo8ooo8O:[I

    .line 49
    .line 50
    const/4 v4, 0x0

    .line 51
    aget v5, v3, v4

    .line 52
    .line 53
    int-to-float v6, v5

    .line 54
    const/4 v7, 0x1

    .line 55
    aget v3, v3, v7

    .line 56
    .line 57
    int-to-float v8, v3

    .line 58
    add-int/2addr v5, v1

    .line 59
    int-to-float v1, v5

    .line 60
    add-int/2addr v3, v0

    .line 61
    int-to-float v0, v3

    .line 62
    invoke-direct {v2, v6, v8, v1, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 66
    .line 67
    iget v0, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 68
    .line 69
    if-eqz v0, :cond_4

    .line 70
    .line 71
    rsub-int v0, v0, 0x2d0

    .line 72
    .line 73
    rem-int/lit16 v0, v0, 0x168

    .line 74
    .line 75
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇oO:[I

    .line 76
    .line 77
    aget v3, v1, v4

    .line 78
    .line 79
    aget v1, v1, v7

    .line 80
    .line 81
    invoke-static {v0, v3, v1}, Lcom/intsig/utils/ImageUtil;->O〇8O8〇008(III)Landroid/graphics/Matrix;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 86
    .line 87
    .line 88
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 89
    .line 90
    iget-object v0, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 91
    .line 92
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    if-eqz v1, :cond_5

    .line 101
    .line 102
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;

    .line 107
    .line 108
    iget v3, v2, Landroid/graphics/RectF;->left:F

    .line 109
    .line 110
    float-to-int v3, v3

    .line 111
    iget v4, v2, Landroid/graphics/RectF;->top:F

    .line 112
    .line 113
    float-to-int v4, v4

    .line 114
    invoke-virtual {v1, v3, v4}, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->offsetPoly(II)V

    .line 115
    .line 116
    .line 117
    goto :goto_2

    .line 118
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇080()V

    .line 119
    .line 120
    .line 121
    :cond_6
    :goto_3
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇oOO8O8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08O〇00〇o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇〇0〇〇0(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇00O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

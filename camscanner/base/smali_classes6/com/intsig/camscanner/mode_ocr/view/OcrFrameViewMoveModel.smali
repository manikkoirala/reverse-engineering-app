.class public final Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;
.super Ljava/lang/Object;
.source "OcrFrameViewMoveModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇80〇808〇O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:Lkotlinx/coroutines/Job;

.field private final Oo08:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mode_ocr/bean/PathBean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO80:I

.field private final o〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/graphics/Region;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue<",
            "Lcom/intsig/camscanner/mode_ocr/bean/PointBean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:Z

.field private 〇〇888:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇80〇808〇O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "uiHandler"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇080:Landroid/os/Handler;

    .line 10
    .line 11
    new-instance p1, Ljava/util/concurrent/LinkedBlockingQueue;

    .line 12
    .line 13
    const/16 v0, 0x100

    .line 14
    .line 15
    invoke-direct {p1, v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 16
    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o00〇〇Oo:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 19
    .line 20
    new-instance p1, Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->Oo08:Ljava/util/ArrayList;

    .line 26
    .line 27
    new-instance p1, Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .line 31
    .line 32
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->o〇0:Ljava/util/ArrayList;

    .line 33
    .line 34
    const/4 p1, 0x5

    .line 35
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->oO80:I

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->Oo08:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO0o〇〇()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->o〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->Oo08:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;

    .line 23
    .line 24
    new-instance v2, Landroid/graphics/RectF;

    .line 25
    .line 26
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 27
    .line 28
    .line 29
    new-instance v3, Landroid/graphics/Path;

    .line 30
    .line 31
    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    const/4 v5, 0x0

    .line 42
    aget v4, v4, v5

    .line 43
    .line 44
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 45
    .line 46
    .line 47
    move-result-object v6

    .line 48
    const/4 v7, 0x1

    .line 49
    aget v6, v6, v7

    .line 50
    .line 51
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 55
    .line 56
    .line 57
    move-result-object v4

    .line 58
    const/4 v6, 0x2

    .line 59
    aget v4, v4, v6

    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 62
    .line 63
    .line 64
    move-result-object v6

    .line 65
    const/4 v8, 0x3

    .line 66
    aget v6, v6, v8

    .line 67
    .line 68
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 72
    .line 73
    .line 74
    move-result-object v4

    .line 75
    const/4 v6, 0x4

    .line 76
    aget v4, v4, v6

    .line 77
    .line 78
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 79
    .line 80
    .line 81
    move-result-object v6

    .line 82
    const/4 v8, 0x5

    .line 83
    aget v6, v6, v8

    .line 84
    .line 85
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 89
    .line 90
    .line 91
    move-result-object v4

    .line 92
    const/4 v6, 0x6

    .line 93
    aget v4, v4, v6

    .line 94
    .line 95
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 96
    .line 97
    .line 98
    move-result-object v6

    .line 99
    const/4 v8, 0x7

    .line 100
    aget v6, v6, v8

    .line 101
    .line 102
    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 106
    .line 107
    .line 108
    move-result-object v4

    .line 109
    aget v4, v4, v5

    .line 110
    .line 111
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇080()[F

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    aget v1, v1, v7

    .line 116
    .line 117
    invoke-virtual {v3, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 121
    .line 122
    .line 123
    invoke-virtual {v3, v2, v7}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 124
    .line 125
    .line 126
    new-instance v1, Landroid/graphics/Region;

    .line 127
    .line 128
    invoke-direct {v1}, Landroid/graphics/Region;-><init>()V

    .line 129
    .line 130
    .line 131
    new-instance v4, Landroid/graphics/Rect;

    .line 132
    .line 133
    iget v5, v2, Landroid/graphics/RectF;->left:F

    .line 134
    .line 135
    float-to-int v5, v5

    .line 136
    iget v6, v2, Landroid/graphics/RectF;->top:F

    .line 137
    .line 138
    float-to-int v6, v6

    .line 139
    iget v7, v2, Landroid/graphics/RectF;->right:F

    .line 140
    .line 141
    float-to-int v7, v7

    .line 142
    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    .line 143
    .line 144
    float-to-int v2, v2

    .line 145
    invoke-direct {v4, v5, v6, v7, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 146
    .line 147
    .line 148
    new-instance v2, Landroid/graphics/Region;

    .line 149
    .line 150
    invoke-direct {v2, v4}, Landroid/graphics/Region;-><init>(Landroid/graphics/Rect;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 154
    .line 155
    .line 156
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->o〇0:Ljava/util/ArrayList;

    .line 157
    .line 158
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    goto/16 :goto_0

    .line 162
    .line 163
    :cond_0
    return-void
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o〇:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇0(Lcom/intsig/camscanner/mode_ocr/bean/PointBean;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o00〇〇Oo:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x100

    .line 8
    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o00〇〇Oo:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/AbstractQueue;->remove()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o00〇〇Oo:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 17
    .line 18
    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇〇888(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;)Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o00〇〇Oo:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->o〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇888(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$dealPoints$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$dealPoints$2;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-ne p1, v0, :cond_0

    .line 20
    .line 21
    return-object p1

    .line 22
    :cond_0
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 23
    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final OO0o〇〇〇〇0()V
    .locals 3

    .line 1
    const-string v0, "OcrFrameViewMoveModel"

    .line 2
    .line 3
    const-string v1, "release "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o00〇〇Oo:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 9
    .line 10
    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o〇:Z

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->O8:Lkotlinx/coroutines/Job;

    .line 17
    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    const/4 v2, 0x0

    .line 22
    invoke-static {v0, v2, v1, v2}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final Oooo8o0〇(Lcom/intsig/camscanner/mode_ocr/bean/PointBean;)V
    .locals 13
    .param p1    # Lcom/intsig/camscanner/mode_ocr/bean/PointBean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "OcrFrameViewMoveModel"

    .line 2
    .line 3
    const-string v1, "pointBean"

    .line 4
    .line 5
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->O8:Lkotlinx/coroutines/Job;

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    const/4 v3, 0x1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    invoke-interface {v1}, Lkotlinx/coroutines/Job;->isActive()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-ne v1, v3, :cond_0

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v1, 0x0

    .line 23
    :goto_0
    if-eqz v1, :cond_4

    .line 24
    .line 25
    iput-boolean v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o〇:Z

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;->〇080()[F

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇〇888:[F

    .line 32
    .line 33
    const/4 v5, 0x2

    .line 34
    if-eqz v4, :cond_1

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;->〇o00〇〇Oo()I

    .line 37
    .line 38
    .line 39
    move-result v6

    .line 40
    if-ne v6, v5, :cond_1

    .line 41
    .line 42
    array-length v6, v1

    .line 43
    if-ne v6, v5, :cond_1

    .line 44
    .line 45
    array-length v6, v4

    .line 46
    if-ne v6, v5, :cond_1

    .line 47
    .line 48
    aget v6, v1, v2

    .line 49
    .line 50
    aget v7, v4, v2

    .line 51
    .line 52
    sub-float/2addr v6, v7

    .line 53
    float-to-double v6, v6

    .line 54
    int-to-double v8, v5

    .line 55
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    .line 56
    .line 57
    .line 58
    move-result-wide v6

    .line 59
    double-to-float v6, v6

    .line 60
    aget v7, v1, v3

    .line 61
    .line 62
    aget v10, v4, v3

    .line 63
    .line 64
    sub-float/2addr v7, v10

    .line 65
    float-to-double v10, v7

    .line 66
    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->pow(DD)D

    .line 67
    .line 68
    .line 69
    move-result-wide v7

    .line 70
    double-to-float v7, v7

    .line 71
    add-float/2addr v6, v7

    .line 72
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    .line 73
    .line 74
    .line 75
    move-result v6

    .line 76
    float-to-double v6, v6

    .line 77
    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    .line 78
    .line 79
    .line 80
    move-result-wide v6

    .line 81
    double-to-float v6, v6

    .line 82
    iget v7, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->oO80:I

    .line 83
    .line 84
    int-to-float v8, v7

    .line 85
    cmpl-float v8, v6, v8

    .line 86
    .line 87
    if-lez v8, :cond_1

    .line 88
    .line 89
    if-eqz v7, :cond_1

    .line 90
    .line 91
    int-to-float v7, v7

    .line 92
    div-float/2addr v6, v7

    .line 93
    float-to-int v6, v6

    .line 94
    aget v7, v1, v2

    .line 95
    .line 96
    aget v8, v4, v2

    .line 97
    .line 98
    sub-float/2addr v7, v8

    .line 99
    aget v1, v1, v3

    .line 100
    .line 101
    aget v8, v4, v3

    .line 102
    .line 103
    sub-float/2addr v1, v8

    .line 104
    add-int/lit8 v8, v6, 0x1

    .line 105
    .line 106
    int-to-float v8, v8

    .line 107
    div-float v9, v7, v8

    .line 108
    .line 109
    div-float v8, v1, v8

    .line 110
    .line 111
    new-instance v10, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    const-string v11, "xSpace:"

    .line 117
    .line 118
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    const-string v7, ",ySpace:"

    .line 125
    .line 126
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    const-string v1, ";xStep:"

    .line 133
    .line 134
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    const-string v1, ",yStep:"

    .line 141
    .line 142
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    const/4 v1, 0x1

    .line 156
    :goto_1
    if-ge v1, v6, :cond_1

    .line 157
    .line 158
    new-array v7, v5, [F

    .line 159
    .line 160
    aget v10, v4, v2

    .line 161
    .line 162
    int-to-float v11, v1

    .line 163
    mul-float v12, v9, v11

    .line 164
    .line 165
    add-float/2addr v10, v12

    .line 166
    aput v10, v7, v2

    .line 167
    .line 168
    aget v10, v4, v3

    .line 169
    .line 170
    mul-float v11, v11, v8

    .line 171
    .line 172
    add-float/2addr v10, v11

    .line 173
    aput v10, v7, v3

    .line 174
    .line 175
    new-instance v10, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;

    .line 176
    .line 177
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;->〇o00〇〇Oo()I

    .line 178
    .line 179
    .line 180
    move-result v11

    .line 181
    invoke-direct {v10, v7, v11}, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;-><init>([FI)V

    .line 182
    .line 183
    .line 184
    invoke-direct {p0, v10}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->o〇0(Lcom/intsig/camscanner/mode_ocr/bean/PointBean;)V

    .line 185
    .line 186
    .line 187
    add-int/lit8 v1, v1, 0x1

    .line 188
    .line 189
    goto :goto_1

    .line 190
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->o〇0(Lcom/intsig/camscanner/mode_ocr/bean/PointBean;)V

    .line 191
    .line 192
    .line 193
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇〇888:[F

    .line 194
    .line 195
    if-nez v1, :cond_2

    .line 196
    .line 197
    new-array v1, v5, [F

    .line 198
    .line 199
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇〇888:[F

    .line 200
    .line 201
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇〇888:[F

    .line 202
    .line 203
    if-eqz v1, :cond_3

    .line 204
    .line 205
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;->〇080()[F

    .line 206
    .line 207
    .line 208
    move-result-object v4

    .line 209
    aget v4, v4, v2

    .line 210
    .line 211
    aput v4, v1, v2

    .line 212
    .line 213
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇〇888:[F

    .line 214
    .line 215
    if-eqz v1, :cond_4

    .line 216
    .line 217
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;->〇080()[F

    .line 218
    .line 219
    .line 220
    move-result-object p1

    .line 221
    aget p1, p1, v3

    .line 222
    .line 223
    aput p1, v1, v3
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    .line 225
    goto :goto_2

    .line 226
    :catch_0
    move-exception p1

    .line 227
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 228
    .line 229
    .line 230
    move-result-object p1

    .line 231
    new-instance v1, Ljava/lang/StringBuilder;

    .line 232
    .line 233
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    .line 235
    .line 236
    const-string v2, "touchPointDeal error:"

    .line 237
    .line 238
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    .line 240
    .line 241
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    .line 243
    .line 244
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 245
    .line 246
    .line 247
    move-result-object p1

    .line 248
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    .line 250
    .line 251
    goto :goto_2

    .line 252
    :catch_1
    const-string p1, "touchPointDeal NoSuchElementException"

    .line 253
    .line 254
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    .line 256
    .line 257
    :cond_4
    :goto_2
    return-void
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final oO80()Landroid/os/Handler;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇080:Landroid/os/Handler;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇808〇O()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->OO0o〇〇〇〇0()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lkotlinx/coroutines/GlobalScope;->o0:Lkotlinx/coroutines/GlobalScope;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    new-instance v3, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$launchDeal$1;

    .line 9
    .line 10
    const/4 v4, 0x0

    .line 11
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel$launchDeal$1;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;Lkotlin/coroutines/Continuation;)V

    .line 12
    .line 13
    .line 14
    const/4 v4, 0x3

    .line 15
    const/4 v5, 0x0

    .line 16
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->O8:Lkotlinx/coroutines/Job;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇8o8o〇()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇〇888:[F

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->Oo08:Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x0

    .line 12
    :goto_0
    if-ge v2, v0, :cond_0

    .line 13
    .line 14
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->Oo08:Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    check-cast v3, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;

    .line 21
    .line 22
    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;->〇o〇(Z)V

    .line 23
    .line 24
    .line 25
    add-int/lit8 v2, v2, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇O8o08O(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/bean/PathBean;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "pathList"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇o〇:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->Oo08:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->Oo08:Ljava/util/ArrayList;

    .line 15
    .line 16
    check-cast p1, Ljava/util/Collection;

    .line 17
    .line 18
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->OO0o〇〇()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

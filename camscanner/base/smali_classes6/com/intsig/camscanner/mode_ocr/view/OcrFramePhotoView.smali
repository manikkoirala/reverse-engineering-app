.class public Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;
.super Lcom/github/chrisbanes/photoview/PhotoView;
.source "OcrFramePhotoView.java"


# instance fields
.field private O0O:Landroid/os/Handler;

.field private O8o08O8O:Landroid/graphics/Paint;

.field private OO:Landroid/graphics/Paint;

.field private OO〇00〇8oO:F

.field private o8oOOo:I

.field private o8〇OO0〇0o:F

.field private oOo0:[F

.field private oOo〇8o008:[F

.field private ooo0〇〇O:Z

.field private o〇00O:Landroid/graphics/Paint;

.field private 〇080OO8〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[F>;"
        }
    .end annotation
.end field

.field private 〇08O〇00〇o:Landroid/graphics/Paint;

.field private 〇0O:Landroid/graphics/Path;

.field private 〇8〇oO〇〇8o:I

.field private 〇〇08O:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/github/chrisbanes/photoview/PhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 p1, 0x8

    new-array p1, p1, [F

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    const/high16 p1, 0x3f800000    # 1.0f

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OO〇00〇8oO:F

    const/high16 p1, 0x41200000    # 10.0f

    .line 4
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8〇OO0〇0o:F

    const/4 p1, -0x1

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇8〇oO〇〇8o:I

    const/4 p1, 0x0

    .line 6
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->ooo0〇〇O:Z

    .line 7
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇〇08O:Z

    .line 8
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, LoO00〇o/〇o〇;

    invoke-direct {v1, p0}, LoO00〇o/〇o〇;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;)V

    invoke-direct {p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->O0O:Landroid/os/Handler;

    .line 9
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8oOOo:I

    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇〇8O0〇8()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 11
    invoke-direct {p0, p1, p2, p3}, Lcom/github/chrisbanes/photoview/PhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 p1, 0x8

    new-array p1, p1, [F

    .line 12
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    const/high16 p1, 0x3f800000    # 1.0f

    .line 13
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OO〇00〇8oO:F

    const/high16 p1, 0x41200000    # 10.0f

    .line 14
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8〇OO0〇0o:F

    const/4 p1, -0x1

    .line 15
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇8〇oO〇〇8o:I

    const/4 p1, 0x0

    .line 16
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->ooo0〇〇O:Z

    .line 17
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇〇08O:Z

    .line 18
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    new-instance v0, LoO00〇o/〇o〇;

    invoke-direct {v0, p0}, LoO00〇o/〇o〇;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;)V

    invoke-direct {p2, p3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->O0O:Landroid/os/Handler;

    .line 19
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8oOOo:I

    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇〇8O0〇8()V

    return-void
.end method

.method private OO0o〇〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇080OO8〇0:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private OO0o〇〇〇〇0()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo0:[F

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇O00([F)Landroid/graphics/RectF;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 12
    .line 13
    .line 14
    new-instance v1, Landroid/graphics/RectF;

    .line 15
    .line 16
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8〇OO0〇0o:F

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    int-to-float v3, v3

    .line 23
    iget v4, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8〇OO0〇0o:F

    .line 24
    .line 25
    sub-float/2addr v3, v4

    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    int-to-float v4, v4

    .line 31
    iget v5, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8〇OO0〇0o:F

    .line 32
    .line 33
    sub-float/2addr v4, v5

    .line 34
    invoke-direct {v1, v2, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-nez v2, :cond_1

    .line 42
    .line 43
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇〇808〇(Landroid/graphics/RectF;Landroid/graphics/RectF;)I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇O〇(Landroid/graphics/RectF;Landroid/graphics/RectF;)I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-nez v2, :cond_0

    .line 52
    .line 53
    if-eqz v0, :cond_1

    .line 54
    .line 55
    :cond_0
    const/4 v1, 0x0

    .line 56
    invoke-virtual {p0, v1, v1, v2, v0}, Lcom/github/chrisbanes/photoview/PhotoView;->〇〇888(IIII)V

    .line 57
    .line 58
    .line 59
    :cond_1
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic OoO8(FFF)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OO0o〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private Oooo8o0〇(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇080OO8〇0:Ljava/util/List;

    .line 6
    .line 7
    if-eqz v1, :cond_2

    .line 8
    .line 9
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-lez v1, :cond_2

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo0:[F

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    array-length v2, v1

    .line 20
    if-nez v2, :cond_0

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oo88o8O(Landroid/graphics/Matrix;[F)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o〇00O:Landroid/graphics/Paint;

    .line 29
    .line 30
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->O8o08O8O:Landroid/graphics/Paint;

    .line 36
    .line 37
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 38
    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇080OO8〇0:Ljava/util/List;

    .line 42
    .line 43
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    if-eqz v2, :cond_2

    .line 52
    .line 53
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    check-cast v2, [F

    .line 58
    .line 59
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oo88o8O(Landroid/graphics/Matrix;[F)V

    .line 60
    .line 61
    .line 62
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 63
    .line 64
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 65
    .line 66
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 67
    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 70
    .line 71
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OO:Landroid/graphics/Paint;

    .line 72
    .line 73
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 74
    .line 75
    .line 76
    goto :goto_1

    .line 77
    :cond_2
    :goto_2
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private synthetic o800o8O(Landroid/os/Message;)Z
    .locals 1

    .line 1
    iget p1, p1, Landroid/os/Message;->what:I

    .line 2
    .line 3
    const/16 v0, 0x65

    .line 4
    .line 5
    if-ne p1, v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇oo〇()V

    .line 8
    .line 9
    .line 10
    const/4 p1, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;FFF)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OoO8(FFF)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private oo88o8O(Landroid/graphics/Matrix;[F)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    .line 7
    .line 8
    invoke-virtual {p1, v0, p2}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 12
    .line 13
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    aget v1, p2, v0

    .line 17
    .line 18
    const/4 v2, 0x1

    .line 19
    aget p2, p2, v2

    .line 20
    .line 21
    invoke-virtual {p1, v1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 25
    .line 26
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    .line 27
    .line 28
    const/4 v1, 0x2

    .line 29
    aget v1, p2, v1

    .line 30
    .line 31
    const/4 v3, 0x3

    .line 32
    aget p2, p2, v3

    .line 33
    .line 34
    invoke-virtual {p1, v1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 38
    .line 39
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    .line 40
    .line 41
    const/4 v1, 0x4

    .line 42
    aget v1, p2, v1

    .line 43
    .line 44
    const/4 v3, 0x5

    .line 45
    aget p2, p2, v3

    .line 46
    .line 47
    invoke-virtual {p1, v1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 48
    .line 49
    .line 50
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 51
    .line 52
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    .line 53
    .line 54
    const/4 v1, 0x6

    .line 55
    aget v1, p2, v1

    .line 56
    .line 57
    const/4 v3, 0x7

    .line 58
    aget p2, p2, v3

    .line 59
    .line 60
    invoke-virtual {p1, v1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 64
    .line 65
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    .line 66
    .line 67
    aget v0, p2, v0

    .line 68
    .line 69
    aget p2, p2, v2

    .line 70
    .line 71
    invoke-virtual {p1, v0, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 75
    .line 76
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇0〇O0088o()V
    .locals 5

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 7
    .line 8
    const-string v1, "#0A19BCAA"

    .line 9
    .line 10
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 18
    .line 19
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 28
    .line 29
    .line 30
    new-instance v0, Landroid/graphics/Paint;

    .line 31
    .line 32
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 33
    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OO:Landroid/graphics/Paint;

    .line 36
    .line 37
    const-string v2, "#E619BCAA"

    .line 38
    .line 39
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OO:Landroid/graphics/Paint;

    .line 47
    .line 48
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 49
    .line 50
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OO:Landroid/graphics/Paint;

    .line 54
    .line 55
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    const/4 v3, 0x1

    .line 60
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    int-to-float v2, v2

    .line 65
    const/high16 v4, 0x3f400000    # 0.75f

    .line 66
    .line 67
    mul-float v2, v2, v4

    .line 68
    .line 69
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 70
    .line 71
    .line 72
    new-instance v0, Landroid/graphics/Paint;

    .line 73
    .line 74
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 75
    .line 76
    .line 77
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o〇00O:Landroid/graphics/Paint;

    .line 78
    .line 79
    const-string v2, "#6619BC9C"

    .line 80
    .line 81
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v2

    .line 85
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 86
    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o〇00O:Landroid/graphics/Paint;

    .line 89
    .line 90
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 91
    .line 92
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 93
    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o〇00O:Landroid/graphics/Paint;

    .line 96
    .line 97
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 98
    .line 99
    .line 100
    new-instance v0, Landroid/graphics/Paint;

    .line 101
    .line 102
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 103
    .line 104
    .line 105
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->O8o08O8O:Landroid/graphics/Paint;

    .line 106
    .line 107
    const-string v1, "#FFFFFF"

    .line 108
    .line 109
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->O8o08O8O:Landroid/graphics/Paint;

    .line 117
    .line 118
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 119
    .line 120
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 121
    .line 122
    .line 123
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->O8o08O8O:Landroid/graphics/Paint;

    .line 124
    .line 125
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    invoke-static {v1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    int-to-float v1, v1

    .line 134
    const/high16 v2, 0x3f000000    # 0.5f

    .line 135
    .line 136
    mul-float v1, v1, v2

    .line 137
    .line 138
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 139
    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o800o8O(Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8o8o〇()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->O8()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    if-nez v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    goto :goto_0

    .line 16
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    :goto_0
    if-gtz v0, :cond_2

    .line 25
    .line 26
    return-void

    .line 27
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    int-to-float v1, v1

    .line 32
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8〇OO0〇0o:F

    .line 33
    .line 34
    const/high16 v3, 0x40000000    # 2.0f

    .line 35
    .line 36
    mul-float v2, v2, v3

    .line 37
    .line 38
    sub-float/2addr v1, v2

    .line 39
    float-to-int v1, v1

    .line 40
    if-gtz v1, :cond_3

    .line 41
    .line 42
    return-void

    .line 43
    :cond_3
    const/4 v2, 0x0

    .line 44
    invoke-virtual {p0, v2}, Lcom/github/chrisbanes/photoview/PhotoView;->〇080(Z)Landroid/graphics/RectF;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    if-nez v2, :cond_4

    .line 49
    .line 50
    return-void

    .line 51
    :cond_4
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo0:[F

    .line 52
    .line 53
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇O00([F)Landroid/graphics/RectF;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 58
    .line 59
    .line 60
    move-result-object v4

    .line 61
    invoke-virtual {v4, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 62
    .line 63
    .line 64
    int-to-float v1, v1

    .line 65
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    div-float/2addr v1, v4

    .line 70
    int-to-float v0, v0

    .line 71
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    .line 72
    .line 73
    .line 74
    move-result v4

    .line 75
    div-float/2addr v0, v4

    .line 76
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    mul-float v1, v1, v0

    .line 85
    .line 86
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 87
    .line 88
    .line 89
    move-result v2

    .line 90
    int-to-float v2, v2

    .line 91
    cmpg-float v1, v1, v2

    .line 92
    .line 93
    if-gez v1, :cond_5

    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->O8()V

    .line 96
    .line 97
    .line 98
    return-void

    .line 99
    :cond_5
    const v1, 0x3f8147ae    # 1.01f

    .line 100
    .line 101
    .line 102
    cmpg-float v1, v0, v1

    .line 103
    .line 104
    if-gez v1, :cond_6

    .line 105
    .line 106
    const v1, 0x3f7d70a4    # 0.99f

    .line 107
    .line 108
    .line 109
    cmpl-float v1, v0, v1

    .line 110
    .line 111
    if-lez v1, :cond_6

    .line 112
    .line 113
    return-void

    .line 114
    :cond_6
    iget v1, v3, Landroid/graphics/RectF;->left:F

    .line 115
    .line 116
    iget v2, v3, Landroid/graphics/RectF;->top:F

    .line 117
    .line 118
    invoke-virtual {p0, v0, v1, v2}, Lcom/github/chrisbanes/photoview/PhotoView;->Oo08(FFF)V

    .line 119
    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇O00([F)Landroid/graphics/RectF;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    aget v2, p1, v1

    .line 8
    .line 9
    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    aget v2, p1, v2

    .line 13
    .line 14
    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 15
    .line 16
    const/4 v2, 0x2

    .line 17
    aget v2, p1, v2

    .line 18
    .line 19
    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 20
    .line 21
    const/4 v2, 0x3

    .line 22
    aget v2, p1, v2

    .line 23
    .line 24
    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 25
    .line 26
    :goto_0
    array-length v2, p1

    .line 27
    if-ge v1, v2, :cond_4

    .line 28
    .line 29
    iget v2, v0, Landroid/graphics/RectF;->left:F

    .line 30
    .line 31
    aget v3, p1, v1

    .line 32
    .line 33
    cmpl-float v2, v2, v3

    .line 34
    .line 35
    if-lez v2, :cond_0

    .line 36
    .line 37
    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 38
    .line 39
    :cond_0
    iget v2, v0, Landroid/graphics/RectF;->right:F

    .line 40
    .line 41
    cmpg-float v2, v2, v3

    .line 42
    .line 43
    if-gez v2, :cond_1

    .line 44
    .line 45
    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 46
    .line 47
    :cond_1
    iget v2, v0, Landroid/graphics/RectF;->top:F

    .line 48
    .line 49
    add-int/lit8 v3, v1, 0x1

    .line 50
    .line 51
    aget v3, p1, v3

    .line 52
    .line 53
    cmpl-float v2, v2, v3

    .line 54
    .line 55
    if-lez v2, :cond_2

    .line 56
    .line 57
    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 58
    .line 59
    :cond_2
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 60
    .line 61
    cmpg-float v2, v2, v3

    .line 62
    .line 63
    if-gez v2, :cond_3

    .line 64
    .line 65
    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 66
    .line 67
    :cond_3
    add-int/lit8 v1, v1, 0x2

    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_4
    return-object v0
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇O〇(Landroid/graphics/RectF;Landroid/graphics/RectF;)I
    .locals 3

    .line 1
    iget v0, p2, Landroid/graphics/RectF;->top:F

    .line 2
    .line 3
    iget v1, p1, Landroid/graphics/RectF;->top:F

    .line 4
    .line 5
    cmpg-float v2, v0, v1

    .line 6
    .line 7
    if-gez v2, :cond_0

    .line 8
    .line 9
    sub-float/2addr v1, v0

    .line 10
    float-to-int p1, v1

    .line 11
    goto :goto_1

    .line 12
    :cond_0
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    .line 13
    .line 14
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    .line 15
    .line 16
    cmpl-float v0, v0, v1

    .line 17
    .line 18
    if-lez v0, :cond_2

    .line 19
    .line 20
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    int-to-float v1, v1

    .line 29
    cmpg-float v0, v0, v1

    .line 30
    .line 31
    if-gez v0, :cond_1

    .line 32
    .line 33
    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    .line 34
    .line 35
    iget p2, p2, Landroid/graphics/RectF;->bottom:F

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    iget p1, p1, Landroid/graphics/RectF;->top:F

    .line 39
    .line 40
    iget p2, p2, Landroid/graphics/RectF;->top:F

    .line 41
    .line 42
    :goto_0
    sub-float/2addr p1, p2

    .line 43
    float-to-int p1, p1

    .line 44
    goto :goto_1

    .line 45
    :cond_2
    const/4 p1, 0x0

    .line 46
    :goto_1
    return p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇oo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo0:[F

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    array-length v0, v0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇8o8o〇()V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->OO0o〇〇〇〇0()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    :goto_0
    const-string v0, "OcrFramePhotoView"

    .line 20
    .line 21
    const-string v1, "updateSelectOcrFrame selectOcrFrame is empty"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇〇808〇(Landroid/graphics/RectF;Landroid/graphics/RectF;)I
    .locals 3

    .line 1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    .line 2
    .line 3
    iget v1, p1, Landroid/graphics/RectF;->left:F

    .line 4
    .line 5
    cmpg-float v2, v0, v1

    .line 6
    .line 7
    if-gez v2, :cond_0

    .line 8
    .line 9
    sub-float/2addr v1, v0

    .line 10
    float-to-int p1, v1

    .line 11
    goto :goto_1

    .line 12
    :cond_0
    iget v0, p2, Landroid/graphics/RectF;->right:F

    .line 13
    .line 14
    iget v1, p1, Landroid/graphics/RectF;->right:F

    .line 15
    .line 16
    cmpl-float v0, v0, v1

    .line 17
    .line 18
    if-lez v0, :cond_2

    .line 19
    .line 20
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    int-to-float v1, v1

    .line 29
    cmpg-float v0, v0, v1

    .line 30
    .line 31
    if-gez v0, :cond_1

    .line 32
    .line 33
    iget p1, p1, Landroid/graphics/RectF;->right:F

    .line 34
    .line 35
    iget p2, p2, Landroid/graphics/RectF;->right:F

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    iget p1, p1, Landroid/graphics/RectF;->left:F

    .line 39
    .line 40
    iget p2, p2, Landroid/graphics/RectF;->left:F

    .line 41
    .line 42
    :goto_0
    sub-float/2addr p1, p2

    .line 43
    float-to-int p1, p1

    .line 44
    goto :goto_1

    .line 45
    :cond_2
    const/4 p1, 0x0

    .line 46
    :goto_1
    return p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇〇8O0〇8()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0xa

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    int-to-float v0, v0

    .line 12
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->o8〇OO0〇0o:F

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0〇O0088o()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getAttacher()Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    new-instance v1, LoO00〇o/O8;

    .line 22
    .line 23
    invoke-direct {v1, p0}, LoO00〇o/O8;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;->O8O〇(Lcom/github/chrisbanes/photoview/OnScaleChangedListener;)V

    .line 27
    .line 28
    .line 29
    new-instance v0, Landroid/graphics/Path;

    .line 30
    .line 31
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇0O:Landroid/graphics/Path;

    .line 35
    .line 36
    const/16 v0, 0x8

    .line 37
    .line 38
    new-array v0, v0, [F

    .line 39
    .line 40
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->oOo〇8o008:[F

    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->ooo0〇〇O:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->〇〇08O:Z

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFramePhotoView;->Oooo8o0〇(Landroid/graphics/Canvas;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

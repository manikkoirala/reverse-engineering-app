.class public Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;
.super Lcom/intsig/view/EditViewMultiLine;
.source "EditViewDividerMultiLine.java"


# instance fields
.field private O8o08O8O:Landroid/graphics/Rect;

.field private OO:I

.field private o〇00O:Z

.field private 〇080OO8〇0:Landroid/graphics/Paint;

.field private 〇08O〇00〇o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/view/EditViewMultiLine;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->Oo08(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/view/EditViewMultiLine;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->Oo08(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private O8(Landroid/graphics/Canvas;)V
    .locals 13

    .line 1
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->bottom:F

    .line 14
    .line 15
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    float-to-int v1, v1

    .line 20
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    const/4 v3, 0x0

    .line 25
    const/4 v4, 0x0

    .line 26
    const/4 v5, 0x0

    .line 27
    :goto_0
    if-ge v3, v0, :cond_1

    .line 28
    .line 29
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->O8o08O8O:Landroid/graphics/Rect;

    .line 30
    .line 31
    invoke-virtual {p0, v3, v5}, Landroid/widget/TextView;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    add-int/2addr v5, v1

    .line 36
    if-lez v4, :cond_0

    .line 37
    .line 38
    sub-int v2, v5, v4

    .line 39
    .line 40
    :cond_0
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->O8o08O8O:Landroid/graphics/Rect;

    .line 41
    .line 42
    iget v6, v4, Landroid/graphics/Rect;->left:I

    .line 43
    .line 44
    int-to-float v8, v6

    .line 45
    int-to-float v11, v5

    .line 46
    iget v4, v4, Landroid/graphics/Rect;->right:I

    .line 47
    .line 48
    int-to-float v10, v4

    .line 49
    iget-object v12, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 50
    .line 51
    move-object v7, p1

    .line 52
    move v9, v11

    .line 53
    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 54
    .line 55
    .line 56
    add-int/lit8 v3, v3, 0x1

    .line 57
    .line 58
    move v4, v5

    .line 59
    goto :goto_0

    .line 60
    :cond_1
    if-gtz v2, :cond_2

    .line 61
    .line 62
    return-void

    .line 63
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    :goto_1
    if-ge v5, v0, :cond_3

    .line 68
    .line 69
    add-int/2addr v5, v2

    .line 70
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->O8o08O8O:Landroid/graphics/Rect;

    .line 71
    .line 72
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 73
    .line 74
    int-to-float v7, v3

    .line 75
    int-to-float v10, v5

    .line 76
    iget v1, v1, Landroid/graphics/Rect;->right:I

    .line 77
    .line 78
    int-to-float v9, v1

    .line 79
    iget-object v11, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 80
    .line 81
    move-object v6, p1

    .line 82
    move v8, v10

    .line 83
    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 84
    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_3
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private Oo08(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/R$styleable;->LineDividerTextView:[I

    .line 2
    .line 3
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    const v0, 0x7f0600ec

    .line 12
    .line 13
    .line 14
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-virtual {p1, v0, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 20
    .line 21
    .line 22
    move-result p2

    .line 23
    iput p2, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->OO:I

    .line 24
    .line 25
    const/4 p2, 0x1

    .line 26
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    .line 27
    .line 28
    .line 29
    move-result p2

    .line 30
    iput p2, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇08O〇00〇o:I

    .line 31
    .line 32
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 33
    .line 34
    .line 35
    new-instance p1, Landroid/graphics/Rect;

    .line 36
    .line 37
    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 38
    .line 39
    .line 40
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->O8o08O8O:Landroid/graphics/Rect;

    .line 41
    .line 42
    new-instance p1, Landroid/graphics/Paint;

    .line 43
    .line 44
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 45
    .line 46
    .line 47
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 48
    .line 49
    iget p2, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->OO:I

    .line 50
    .line 51
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 52
    .line 53
    .line 54
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 55
    .line 56
    iget p2, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇08O〇00〇o:I

    .line 57
    .line 58
    int-to-float p2, p2

    .line 59
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇o〇(Landroid/graphics/Canvas;)V
    .locals 11

    .line 1
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-lez v1, :cond_1

    .line 10
    .line 11
    if-gtz v0, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->O8o08O8O:Landroid/graphics/Rect;

    .line 15
    .line 16
    const/4 v3, 0x0

    .line 17
    invoke-virtual {p0, v3, v2}, Landroid/widget/TextView;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 18
    .line 19
    .line 20
    :goto_0
    if-ge v3, v1, :cond_1

    .line 21
    .line 22
    add-int/2addr v3, v0

    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->O8o08O8O:Landroid/graphics/Rect;

    .line 24
    .line 25
    iget v4, v2, Landroid/graphics/Rect;->left:I

    .line 26
    .line 27
    int-to-float v6, v4

    .line 28
    int-to-float v9, v3

    .line 29
    iget v2, v2, Landroid/graphics/Rect;->right:I

    .line 30
    .line 31
    int-to-float v8, v2

    .line 32
    iget-object v10, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 33
    .line 34
    move-object v5, p1

    .line 35
    move v7, v9

    .line 36
    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    :goto_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/widget/EditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 5
    .line 6
    if-eqz v0, :cond_4

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->O8o08O8O:Landroid/graphics/Rect;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->o〇00O:Z

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    return-void

    .line 18
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-gtz v0, :cond_3

    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇8〇0〇o〇O()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_2

    .line 29
    .line 30
    const-string v0, "EditViewDividerMultiLine"

    .line 31
    .line 32
    const-string v1, "lineCount=0"

    .line 33
    .line 34
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->〇o〇(Landroid/graphics/Canvas;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_3
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->O8(Landroid/graphics/Canvas;)V

    .line 42
    .line 43
    .line 44
    :cond_4
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setShowLineDivider(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->o〇00O:Z

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

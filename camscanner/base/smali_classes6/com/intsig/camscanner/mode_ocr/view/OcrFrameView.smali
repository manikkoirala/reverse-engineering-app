.class public final Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;
.super Lcom/github/chrisbanes/photoview/PhotoView;
.source "OcrFrameView.kt"

# interfaces
.implements Lcom/github/chrisbanes/photoview/OnViewGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇0O〇O00O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Landroid/graphics/Path;

.field private final O88O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/bean/PathBean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o08O8O:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Landroid/graphics/Path;

.field private Oo0〇Ooo:Z

.field private Oo80:Z

.field private final Ooo08:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇08oOOO0:Z

.field private O〇o88o08〇:Z

.field private o8o:Z

.field private o8oOOo:F

.field private final o8〇OO:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:[F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOO〇〇:I

.field private final oOo0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO〇8O8oOo:Lcom/intsig/camscanner/mode_ocr/OCRData;

.field private oo8ooo8O:Z

.field private final ooO:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooo0〇〇O:F

.field private o〇00O:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇oO:Z

.field private 〇00O0:Z

.field private 〇080OO8〇0:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08〇o0O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;

.field private 〇0O:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:[F

.field private 〇OO8ooO8〇:Z

.field private 〇OO〇00〇0O:Z

.field private 〇O〇〇O8:F

.field private 〇o0O:F

.field private 〇〇08O:F

.field private 〇〇o〇:Z

.field private 〇〇〇0o〇〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0O〇O00O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/github/chrisbanes/photoview/PhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO:Landroid/graphics/Paint;

    .line 3
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 4
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇00O:Landroid/graphics/Paint;

    .line 5
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8o08O8O:Landroid/graphics/Paint;

    .line 6
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 7
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0O:Landroid/graphics/Paint;

    .line 8
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 9
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    const/16 p1, 0x8

    new-array p1, p1, [F

    .line 10
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    const/high16 p1, 0x3f800000    # 1.0f

    .line 11
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooo0〇〇O:F

    const/high16 p1, 0x41200000    # 10.0f

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    const/high16 p1, 0x40a00000    # 5.0f

    .line 13
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇o0O:F

    .line 14
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O88O:Ljava/util/List;

    const/4 p1, -0x1

    .line 15
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOO〇〇:I

    .line 16
    new-instance p2, Landroidx/lifecycle/MutableLiveData;

    invoke-direct {p2}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO:Landroidx/lifecycle/MutableLiveData;

    .line 17
    new-instance p2, Landroidx/lifecycle/MutableLiveData;

    invoke-direct {p2}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Ooo08:Landroidx/lifecycle/MutableLiveData;

    .line 18
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, LoO00〇o/Oo08;

    invoke-direct {v1, p0}, LoO00〇o/Oo08;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;)V

    invoke-direct {p2, v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    const/4 p2, 0x1

    .line 19
    iput-boolean p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo0〇Ooo:Z

    .line 20
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇〇0o〇〇0:I

    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇oo〇()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/github/chrisbanes/photoview/PhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO:Landroid/graphics/Paint;

    .line 25
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 26
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇00O:Landroid/graphics/Paint;

    .line 27
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8o08O8O:Landroid/graphics/Paint;

    .line 28
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 29
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0O:Landroid/graphics/Paint;

    .line 30
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 31
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    const/16 p1, 0x8

    new-array p1, p1, [F

    .line 32
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    const/high16 p1, 0x3f800000    # 1.0f

    .line 33
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooo0〇〇O:F

    const/high16 p1, 0x41200000    # 10.0f

    .line 34
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    const/high16 p1, 0x40a00000    # 5.0f

    .line 35
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇o0O:F

    .line 36
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O88O:Ljava/util/List;

    const/4 p1, -0x1

    .line 37
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOO〇〇:I

    .line 38
    new-instance p2, Landroidx/lifecycle/MutableLiveData;

    invoke-direct {p2}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO:Landroidx/lifecycle/MutableLiveData;

    .line 39
    new-instance p2, Landroidx/lifecycle/MutableLiveData;

    invoke-direct {p2}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Ooo08:Landroidx/lifecycle/MutableLiveData;

    .line 40
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    new-instance v0, LoO00〇o/Oo08;

    invoke-direct {v0, p0}, LoO00〇o/Oo08;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;)V

    invoke-direct {p2, p3, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    const/4 p2, 0x1

    .line 41
    iput-boolean p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo0〇Ooo:Z

    .line 42
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇〇0o〇〇0:I

    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇oo〇()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 22
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final O8ooOoo〇(Landroid/graphics/Matrix;[F)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    .line 9
    .line 10
    invoke-virtual {p1, v1, p2}, Landroid/graphics/Matrix;->mapPoints([F[F)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    .line 14
    .line 15
    const/4 p2, 0x0

    .line 16
    aget v1, p1, p2

    .line 17
    .line 18
    const/4 v2, 0x1

    .line 19
    aget p1, p1, v2

    .line 20
    .line 21
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    .line 25
    .line 26
    const/4 v1, 0x2

    .line 27
    aget v1, p1, v1

    .line 28
    .line 29
    const/4 v3, 0x3

    .line 30
    aget p1, p1, v3

    .line 31
    .line 32
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 33
    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    .line 36
    .line 37
    const/4 v1, 0x4

    .line 38
    aget v1, p1, v1

    .line 39
    .line 40
    const/4 v3, 0x5

    .line 41
    aget p1, p1, v3

    .line 42
    .line 43
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 44
    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    .line 47
    .line 48
    const/4 v1, 0x6

    .line 49
    aget v1, p1, v1

    .line 50
    .line 51
    const/4 v3, 0x7

    .line 52
    aget p1, p1, v3

    .line 53
    .line 54
    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    .line 58
    .line 59
    aget p2, p1, p2

    .line 60
    .line 61
    aget p1, p1, v2

    .line 62
    .line 63
    invoke-virtual {v0, p2, p1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 67
    .line 68
    .line 69
    :cond_0
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O8〇o(FFI)V
    .locals 1

    .line 1
    new-instance v0, LoO00〇o/o〇0;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1, p2, p3}, LoO00〇o/o〇0;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;FFI)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final OO0o〇〇()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇8〇oO〇〇8o:[F

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oo88o8O([F)Landroid/graphics/RectF;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 12
    .line 13
    .line 14
    new-instance v1, Landroid/graphics/RectF;

    .line 15
    .line 16
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    int-to-float v3, v3

    .line 23
    iget v4, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    .line 24
    .line 25
    sub-float/2addr v3, v4

    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    int-to-float v4, v4

    .line 31
    iget v5, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    .line 32
    .line 33
    sub-float/2addr v4, v5

    .line 34
    invoke-direct {v1, v2, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    sub-float/2addr v2, v3

    .line 46
    float-to-int v2, v2

    .line 47
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    sub-float/2addr v1, v0

    .line 56
    float-to-int v0, v1

    .line 57
    if-nez v2, :cond_0

    .line 58
    .line 59
    if-eqz v0, :cond_1

    .line 60
    .line 61
    :cond_0
    const/4 v1, 0x0

    .line 62
    invoke-virtual {p0, v1, v1, v2, v0}, Lcom/github/chrisbanes/photoview/PhotoView;->〇〇888(IIII)V

    .line 63
    .line 64
    .line 65
    :cond_1
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;Lcom/intsig/camscanner/mode_ocr/OCRData;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o0ooO(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;Lcom/intsig/camscanner/mode_ocr/OCRData;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final OOO〇O0(Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;)V
    .locals 7

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    iget-object v0, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 4
    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    iget v0, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 8
    .line 9
    if-lez v0, :cond_4

    .line 10
    .line 11
    iget v1, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 12
    .line 13
    if-lez v1, :cond_4

    .line 14
    .line 15
    iget v1, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 16
    .line 17
    if-lez v1, :cond_4

    .line 18
    .line 19
    new-instance v1, Landroid/graphics/Matrix;

    .line 20
    .line 21
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 22
    .line 23
    .line 24
    iget v2, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 25
    .line 26
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 27
    .line 28
    neg-int v4, v2

    .line 29
    int-to-float v4, v4

    .line 30
    const/high16 v5, 0x40000000    # 2.0f

    .line 31
    .line 32
    div-float/2addr v4, v5

    .line 33
    neg-int v6, v3

    .line 34
    int-to-float v6, v6

    .line 35
    div-float/2addr v6, v5

    .line 36
    invoke-virtual {v1, v4, v6}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 37
    .line 38
    .line 39
    int-to-float v4, v0

    .line 40
    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 41
    .line 42
    .line 43
    const/16 v4, 0x5a

    .line 44
    .line 45
    if-eq v0, v4, :cond_0

    .line 46
    .line 47
    const/16 v4, 0x10e

    .line 48
    .line 49
    if-eq v0, v4, :cond_0

    .line 50
    .line 51
    int-to-float v0, v2

    .line 52
    div-float/2addr v0, v5

    .line 53
    int-to-float v2, v3

    .line 54
    div-float/2addr v2, v5

    .line 55
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    int-to-float v0, v3

    .line 60
    div-float/2addr v0, v5

    .line 61
    int-to-float v2, v2

    .line 62
    div-float/2addr v2, v5

    .line 63
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 64
    .line 65
    .line 66
    :goto_0
    iget-object p1, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 67
    .line 68
    invoke-virtual {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-eqz v0, :cond_4

    .line 77
    .line 78
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;

    .line 83
    .line 84
    iget-object v2, v0, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->poly:[F

    .line 85
    .line 86
    if-eqz v2, :cond_2

    .line 87
    .line 88
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 89
    .line 90
    .line 91
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->lines:Ljava/util/List;

    .line 92
    .line 93
    if-eqz v0, :cond_1

    .line 94
    .line 95
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 100
    .line 101
    .line 102
    move-result v2

    .line 103
    if-eqz v2, :cond_1

    .line 104
    .line 105
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 106
    .line 107
    .line 108
    move-result-object v2

    .line 109
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;

    .line 110
    .line 111
    if-eqz v2, :cond_3

    .line 112
    .line 113
    iget-object v2, v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->poly:[F

    .line 114
    .line 115
    if-eqz v2, :cond_3

    .line 116
    .line 117
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 118
    .line 119
    .line 120
    goto :goto_1

    .line 121
    :cond_4
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final Oo8Oo00oo(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;Landroid/os/Message;)Z
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "msg"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 12
    .line 13
    iget p1, p1, Landroid/os/Message;->what:I

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    const-string v2, "OcrFrameView"

    .line 17
    .line 18
    const/4 v3, 0x0

    .line 19
    packed-switch p1, :pswitch_data_0

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :pswitch_0
    const-string p1, "MSG_DEAL_TOUCH_UP"

    .line 24
    .line 25
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO:Landroidx/lifecycle/MutableLiveData;

    .line 29
    .line 30
    new-instance p1, Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;

    .line 31
    .line 32
    const/4 v0, -0x1

    .line 33
    const/4 v1, 0x3

    .line 34
    invoke-direct {p1, v0, v3, v1}, Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;-><init>(IZI)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :pswitch_1
    const-string p1, "MSG_DEAL_CLICK_DOWN"

    .line 42
    .line 43
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇0o(IZ)V

    .line 47
    .line 48
    .line 49
    goto :goto_1

    .line 50
    :pswitch_2
    const-string p1, "MSG_DEAL_EDIT_CLICK"

    .line 51
    .line 52
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 56
    .line 57
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    if-ge v0, p1, :cond_0

    .line 62
    .line 63
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Ooo08:Landroidx/lifecycle/MutableLiveData;

    .line 64
    .line 65
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    invoke-virtual {p0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :pswitch_3
    invoke-direct {p0, v0, v3}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇0o(IZ)V

    .line 74
    .line 75
    .line 76
    goto :goto_1

    .line 77
    :pswitch_4
    const-string p1, "MSG_START_TRANSLATE"

    .line 78
    .line 79
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇0OOo〇0()V

    .line 83
    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 87
    :goto_1
    return v1

    .line 88
    nop

    .line 89
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final OoO8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08〇o0O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->OO0o〇〇〇〇0()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final Oooo8o0〇()V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOO〇〇:I

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->O8()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    if-nez v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    goto :goto_0

    .line 16
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    :goto_0
    if-gtz v0, :cond_2

    .line 25
    .line 26
    return-void

    .line 27
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    int-to-float v1, v1

    .line 32
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    .line 33
    .line 34
    const/4 v3, 0x2

    .line 35
    int-to-float v3, v3

    .line 36
    mul-float v2, v2, v3

    .line 37
    .line 38
    sub-float/2addr v1, v2

    .line 39
    float-to-int v1, v1

    .line 40
    if-gtz v1, :cond_3

    .line 41
    .line 42
    return-void

    .line 43
    :cond_3
    int-to-double v2, v1

    .line 44
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 45
    .line 46
    .line 47
    move-result v4

    .line 48
    int-to-double v4, v4

    .line 49
    const-wide v6, 0x3feccccccccccccdL    # 0.9

    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    mul-double v4, v4, v6

    .line 55
    .line 56
    cmpl-double v8, v2, v4

    .line 57
    .line 58
    if-lez v8, :cond_4

    .line 59
    .line 60
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    int-to-double v1, v1

    .line 65
    mul-double v1, v1, v6

    .line 66
    .line 67
    double-to-int v1, v1

    .line 68
    :cond_4
    const/4 v2, 0x0

    .line 69
    invoke-virtual {p0, v2}, Lcom/github/chrisbanes/photoview/PhotoView;->〇080(Z)Landroid/graphics/RectF;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    if-nez v3, :cond_5

    .line 74
    .line 75
    return-void

    .line 76
    :cond_5
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇8〇oO〇〇8o:[F

    .line 77
    .line 78
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oo88o8O([F)Landroid/graphics/RectF;

    .line 79
    .line 80
    .line 81
    move-result-object v4

    .line 82
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 83
    .line 84
    .line 85
    move-result-object v5

    .line 86
    invoke-virtual {v5, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 87
    .line 88
    .line 89
    int-to-float v1, v1

    .line 90
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    .line 91
    .line 92
    .line 93
    move-result v5

    .line 94
    div-float/2addr v1, v5

    .line 95
    int-to-float v0, v0

    .line 96
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    .line 97
    .line 98
    .line 99
    move-result v5

    .line 100
    div-float/2addr v0, v5

    .line 101
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    mul-float v1, v1, v0

    .line 110
    .line 111
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 112
    .line 113
    .line 114
    move-result v3

    .line 115
    int-to-float v3, v3

    .line 116
    cmpg-float v1, v1, v3

    .line 117
    .line 118
    if-gez v1, :cond_6

    .line 119
    .line 120
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->O8()V

    .line 121
    .line 122
    .line 123
    return-void

    .line 124
    :cond_6
    const v1, 0x3f8147ae    # 1.01f

    .line 125
    .line 126
    .line 127
    cmpg-float v1, v0, v1

    .line 128
    .line 129
    if-gez v1, :cond_7

    .line 130
    .line 131
    const v1, 0x3f7d70a4    # 0.99f

    .line 132
    .line 133
    .line 134
    cmpl-float v1, v0, v1

    .line 135
    .line 136
    if-lez v1, :cond_7

    .line 137
    .line 138
    return-void

    .line 139
    :cond_7
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getScale()F

    .line 140
    .line 141
    .line 142
    move-result v1

    .line 143
    const/4 v3, 0x0

    .line 144
    cmpg-float v1, v1, v3

    .line 145
    .line 146
    if-nez v1, :cond_8

    .line 147
    .line 148
    const/4 v2, 0x1

    .line 149
    :cond_8
    if-nez v2, :cond_9

    .line 150
    .line 151
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getScale()F

    .line 152
    .line 153
    .line 154
    move-result v1

    .line 155
    mul-float v1, v1, v0

    .line 156
    .line 157
    const/high16 v2, 0x40400000    # 3.0f

    .line 158
    .line 159
    cmpl-float v1, v1, v2

    .line 160
    .line 161
    if-lez v1, :cond_9

    .line 162
    .line 163
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getScale()F

    .line 164
    .line 165
    .line 166
    move-result v0

    .line 167
    div-float v0, v2, v0

    .line 168
    .line 169
    :cond_9
    iget v1, v4, Landroid/graphics/RectF;->left:F

    .line 170
    .line 171
    iget v2, v4, Landroid/graphics/RectF;->top:F

    .line 172
    .line 173
    invoke-virtual {p0, v0, v1, v2}, Lcom/github/chrisbanes/photoview/PhotoView;->Oo08(FFF)V

    .line 174
    .line 175
    .line 176
    return-void
    .line 177
.end method

.method private final O〇8O8〇008([F[F)Z
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_5

    .line 3
    .line 4
    if-nez p2, :cond_0

    .line 5
    .line 6
    goto :goto_2

    .line 7
    :cond_0
    array-length v1, p1

    .line 8
    array-length v2, p2

    .line 9
    if-eq v1, v2, :cond_1

    .line 10
    .line 11
    return v0

    .line 12
    :cond_1
    array-length v1, p1

    .line 13
    const/4 v2, 0x0

    .line 14
    :goto_0
    const/4 v3, 0x1

    .line 15
    if-ge v2, v1, :cond_4

    .line 16
    .line 17
    aget v4, p1, v2

    .line 18
    .line 19
    aget v5, p2, v2

    .line 20
    .line 21
    cmpg-float v4, v4, v5

    .line 22
    .line 23
    if-nez v4, :cond_2

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_2
    const/4 v3, 0x0

    .line 27
    :goto_1
    if-nez v3, :cond_3

    .line 28
    .line 29
    return v0

    .line 30
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_4
    return v3

    .line 34
    :cond_5
    :goto_2
    const-string p1, "OcrFrameView"

    .line 35
    .line 36
    const-string p2, "frameData or selectOcrFrame is null"

    .line 37
    .line 38
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final o0ooO(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;Lcom/intsig/camscanner/mode_ocr/OCRData;I)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$ocrData"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇O00(Lcom/intsig/camscanner/mode_ocr/OCRData;I)V

    .line 16
    .line 17
    .line 18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 19
    .line 20
    .line 21
    move-result-wide p0

    .line 22
    sub-long/2addr p0, v0

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v1, "doRealSetOcrData cost time="

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string p0, " bitmapMaxSize:"

    .line 37
    .line 38
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    const-string p1, "OcrFrameView"

    .line 49
    .line 50
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o800o8O(Ljava/lang/String;I)F
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/high16 v0, 0x3f800000    # 1.0f

    .line 6
    .line 7
    int-to-float p2, p2

    .line 8
    mul-float p2, p2, v0

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    int-to-float p1, p1

    .line 23
    div-float/2addr p2, p1

    .line 24
    return p2
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo8Oo00oo(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oo88o8O([F)Landroid/graphics/RectF;
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_4

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    aget v2, p1, v1

    .line 10
    .line 11
    iput v2, v0, Landroid/graphics/RectF;->left:F

    .line 12
    .line 13
    const/4 v2, 0x1

    .line 14
    aget v2, p1, v2

    .line 15
    .line 16
    iput v2, v0, Landroid/graphics/RectF;->top:F

    .line 17
    .line 18
    const/4 v2, 0x2

    .line 19
    aget v2, p1, v2

    .line 20
    .line 21
    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 22
    .line 23
    const/4 v2, 0x3

    .line 24
    aget v2, p1, v2

    .line 25
    .line 26
    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 27
    .line 28
    :goto_0
    array-length v2, p1

    .line 29
    if-ge v1, v2, :cond_4

    .line 30
    .line 31
    iget v2, v0, Landroid/graphics/RectF;->left:F

    .line 32
    .line 33
    aget v3, p1, v1

    .line 34
    .line 35
    cmpl-float v2, v2, v3

    .line 36
    .line 37
    if-lez v2, :cond_0

    .line 38
    .line 39
    iput v3, v0, Landroid/graphics/RectF;->left:F

    .line 40
    .line 41
    :cond_0
    iget v2, v0, Landroid/graphics/RectF;->right:F

    .line 42
    .line 43
    cmpg-float v2, v2, v3

    .line 44
    .line 45
    if-gez v2, :cond_1

    .line 46
    .line 47
    iput v3, v0, Landroid/graphics/RectF;->right:F

    .line 48
    .line 49
    :cond_1
    iget v2, v0, Landroid/graphics/RectF;->top:F

    .line 50
    .line 51
    add-int/lit8 v3, v1, 0x1

    .line 52
    .line 53
    aget v3, p1, v3

    .line 54
    .line 55
    cmpl-float v2, v2, v3

    .line 56
    .line 57
    if-lez v2, :cond_2

    .line 58
    .line 59
    iput v3, v0, Landroid/graphics/RectF;->top:F

    .line 60
    .line 61
    :cond_2
    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 62
    .line 63
    cmpg-float v2, v2, v3

    .line 64
    .line 65
    if-gez v2, :cond_3

    .line 66
    .line 67
    iput v3, v0, Landroid/graphics/RectF;->bottom:F

    .line 68
    .line 69
    :cond_3
    add-int/lit8 v1, v1, 0x2

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_4
    return-object v0
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final o〇0OOo〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇8〇oO〇〇8o:[F

    .line 2
    .line 3
    const-string v1, "OcrFrameView"

    .line 4
    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    array-length v0, v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    if-eqz v0, :cond_1

    .line 14
    .line 15
    const-string v0, "updateSelectOcrFrame selectOcrFrame is empty"

    .line 16
    .line 17
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oooo8o0〇()V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO0o〇〇()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 28
    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_2
    const-string v0, "mSelectOcrFrame == null"

    .line 32
    .line 33
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    :goto_1
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final o〇O8〇〇o(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;FFF)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇O〇()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final setDefaultPoint(Landroid/view/MotionEvent;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O0O:Landroid/graphics/Path;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 14
    .line 15
    .line 16
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8oOOo:F

    .line 21
    .line 22
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇O〇〇O8:F

    .line 27
    .line 28
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8oOOo:F

    .line 29
    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v2, "setDefaultPoint ==> mPreX:"

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v0, ",mPreY:"

    .line 44
    .line 45
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    const-string v0, "OcrFrameView"

    .line 56
    .line 57
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇00()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO:Landroid/graphics/Paint;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO:Landroid/graphics/Paint;

    .line 8
    .line 9
    const-string v2, "#19BCAA"

    .line 10
    .line 11
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO:Landroid/graphics/Paint;

    .line 19
    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    const/4 v4, 0x6

    .line 25
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    int-to-float v3, v3

    .line 30
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO:Landroid/graphics/Paint;

    .line 34
    .line 35
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 36
    .line 37
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO:Landroid/graphics/Paint;

    .line 41
    .line 42
    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    .line 43
    .line 44
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8o08O8O:Landroid/graphics/Paint;

    .line 48
    .line 49
    const-string v3, "#4D19BCAA"

    .line 50
    .line 51
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8o08O8O:Landroid/graphics/Paint;

    .line 59
    .line 60
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 61
    .line 62
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8o08O8O:Landroid/graphics/Paint;

    .line 66
    .line 67
    const/4 v3, 0x0

    .line 68
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 72
    .line 73
    const-string v4, "#149588"

    .line 74
    .line 75
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 83
    .line 84
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 85
    .line 86
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 87
    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 90
    .line 91
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 92
    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 95
    .line 96
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 97
    .line 98
    .line 99
    move-result-object v4

    .line 100
    invoke-static {v4, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 101
    .line 102
    .line 103
    move-result v4

    .line 104
    int-to-float v4, v4

    .line 105
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 106
    .line 107
    .line 108
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇00O:Landroid/graphics/Paint;

    .line 109
    .line 110
    const-string v4, "#2F80ED"

    .line 111
    .line 112
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 117
    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇00O:Landroid/graphics/Paint;

    .line 120
    .line 121
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 122
    .line 123
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 124
    .line 125
    .line 126
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇00O:Landroid/graphics/Paint;

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 129
    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇00O:Landroid/graphics/Paint;

    .line 132
    .line 133
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 134
    .line 135
    .line 136
    move-result-object v4

    .line 137
    invoke-static {v4, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 138
    .line 139
    .line 140
    move-result v4

    .line 141
    int-to-float v4, v4

    .line 142
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 143
    .line 144
    .line 145
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 146
    .line 147
    const-string v4, "#4D2F80ED"

    .line 148
    .line 149
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 150
    .line 151
    .line 152
    move-result v4

    .line 153
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 154
    .line 155
    .line 156
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 157
    .line 158
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 159
    .line 160
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 161
    .line 162
    .line 163
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 164
    .line 165
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 166
    .line 167
    .line 168
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0O:Landroid/graphics/Paint;

    .line 169
    .line 170
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 171
    .line 172
    .line 173
    move-result v2

    .line 174
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 175
    .line 176
    .line 177
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0O:Landroid/graphics/Paint;

    .line 178
    .line 179
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 180
    .line 181
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 182
    .line 183
    .line 184
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0O:Landroid/graphics/Paint;

    .line 185
    .line 186
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 187
    .line 188
    .line 189
    move-result v1

    .line 190
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 191
    .line 192
    .line 193
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 194
    .line 195
    const/16 v1, 0xbc

    .line 196
    .line 197
    const/16 v2, 0xaa

    .line 198
    .line 199
    const/16 v4, 0x33

    .line 200
    .line 201
    const/16 v5, 0x19

    .line 202
    .line 203
    invoke-static {v4, v5, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    .line 204
    .line 205
    .line 206
    move-result v1

    .line 207
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 208
    .line 209
    .line 210
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 211
    .line 212
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 213
    .line 214
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 215
    .line 216
    .line 217
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 218
    .line 219
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 220
    .line 221
    .line 222
    return-void
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final 〇0000OOO()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O0O:Landroid/graphics/Path;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇00〇8(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;FFI)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    new-instance v1, Landroid/graphics/Matrix;

    .line 11
    .line 12
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x2

    .line 16
    new-array v2, v2, [F

    .line 17
    .line 18
    const/4 v3, 0x0

    .line 19
    aput p1, v2, v3

    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    aput p2, v2, p1

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 28
    .line 29
    .line 30
    aget p2, v2, v3

    .line 31
    .line 32
    aget p1, v2, p1

    .line 33
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v1, "selectOrUnSelect points\uff1a"

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string p2, "-"

    .line 48
    .line 49
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    const-string p2, "OcrFrameView"

    .line 60
    .line 61
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    new-instance p1, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;

    .line 65
    .line 66
    invoke-direct {p1, v2, p3}, Lcom/intsig/camscanner/mode_ocr/bean/PointBean;-><init>([FI)V

    .line 67
    .line 68
    .line 69
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08〇o0O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;

    .line 70
    .line 71
    if-eqz p0, :cond_0

    .line 72
    .line 73
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->Oooo8o0〇(Lcom/intsig/camscanner/mode_ocr/bean/PointBean;)V

    .line 74
    .line 75
    .line 76
    :cond_0
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;FFF)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇O8〇〇o(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;FFF)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;FFI)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇00〇8(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;FFI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇O00(Lcom/intsig/camscanner/mode_ocr/OCRData;I)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oO〇8O8oOo:Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, "OcrFrameView"

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string p1, "ocrData equals lastOCRData"

    .line 12
    .line 13
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->clone()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const-string v2, "null cannot be cast to non-null type com.intsig.camscanner.mode_ocr.OCRData"

    .line 22
    .line 23
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 27
    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oO〇8O8oOo:Lcom/intsig/camscanner/mode_ocr/OCRData;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :catch_0
    move-exception v0

    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    .line 34
    .line 35
    :goto_0
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->〇o〇()V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 39
    .line 40
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O88O:Ljava/util/List;

    .line 44
    .line 45
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 46
    .line 47
    .line 48
    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇08oOOO0:Z

    .line 50
    .line 51
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8o:Z

    .line 52
    .line 53
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇oO:Z

    .line 54
    .line 55
    iget-object v2, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 56
    .line 57
    const/4 v3, 0x1

    .line 58
    if-eqz v2, :cond_5

    .line 59
    .line 60
    iget-object v2, v2, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 61
    .line 62
    if-eqz v2, :cond_5

    .line 63
    .line 64
    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    .line 65
    .line 66
    .line 67
    move-result v2

    .line 68
    if-lez v2, :cond_5

    .line 69
    .line 70
    if-lez p2, :cond_5

    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o〇()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    const-string v4, "ocrData.inputImagePath"

    .line 77
    .line 78
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    invoke-direct {p0, v2, p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o800o8O(Ljava/lang/String;I)F

    .line 82
    .line 83
    .line 84
    move-result p2

    .line 85
    iput p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooo0〇〇O:F

    .line 86
    .line 87
    :try_start_1
    iget-object p2, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 88
    .line 89
    invoke-virtual {p2}, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->clone()Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    move-result-object p2

    .line 93
    const-string v2, "null cannot be cast to non-null type com.intsig.camscanner.mode_ocr.bean.ParagraphOcrDataBean"

    .line 94
    .line 95
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    check-cast p2, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 99
    .line 100
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OOO〇O0(Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;)V

    .line 101
    .line 102
    .line 103
    iget-object p2, p2, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 104
    .line 105
    invoke-virtual {p2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    if-eqz v1, :cond_5

    .line 114
    .line 115
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;

    .line 120
    .line 121
    if-eqz v1, :cond_1

    .line 122
    .line 123
    iget-object v2, v1, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->lines:Ljava/util/List;

    .line 124
    .line 125
    if-eqz v2, :cond_1

    .line 126
    .line 127
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 128
    .line 129
    .line 130
    move-result v2

    .line 131
    if-lez v2, :cond_1

    .line 132
    .line 133
    iget-object v1, v1, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->lines:Ljava/util/List;

    .line 134
    .line 135
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 136
    .line 137
    .line 138
    move-result-object v1

    .line 139
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 140
    .line 141
    .line 142
    move-result v2

    .line 143
    if-eqz v2, :cond_1

    .line 144
    .line 145
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 146
    .line 147
    .line 148
    move-result-object v2

    .line 149
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;

    .line 150
    .line 151
    if-eqz v2, :cond_2

    .line 152
    .line 153
    iget-object v4, v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->poly:[F

    .line 154
    .line 155
    if-eqz v4, :cond_2

    .line 156
    .line 157
    array-length v5, v4

    .line 158
    const/16 v6, 0x8

    .line 159
    .line 160
    if-ne v5, v6, :cond_2

    .line 161
    .line 162
    new-array v5, v6, [F

    .line 163
    .line 164
    array-length v4, v4

    .line 165
    const/4 v6, 0x0

    .line 166
    :goto_2
    if-ge v6, v4, :cond_3

    .line 167
    .line 168
    iget-object v7, v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->poly:[F

    .line 169
    .line 170
    aget v7, v7, v6

    .line 171
    .line 172
    iget v8, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooo0〇〇O:F

    .line 173
    .line 174
    mul-float v7, v7, v8

    .line 175
    .line 176
    aput v7, v5, v6

    .line 177
    .line 178
    add-int/lit8 v6, v6, 0x1

    .line 179
    .line 180
    goto :goto_2

    .line 181
    :cond_3
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 182
    .line 183
    new-instance v6, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;

    .line 184
    .line 185
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/bean/OcrLineBean;->isSelectText()Z

    .line 186
    .line 187
    .line 188
    move-result v2

    .line 189
    if-eqz v2, :cond_4

    .line 190
    .line 191
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇OO8ooO8〇:Z

    .line 192
    .line 193
    if-nez v2, :cond_4

    .line 194
    .line 195
    const/4 v2, 0x1

    .line 196
    goto :goto_3

    .line 197
    :cond_4
    const/4 v2, 0x0

    .line 198
    :goto_3
    invoke-direct {v6, v5, v2}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;-><init>([FZ)V

    .line 199
    .line 200
    .line 201
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    .line 203
    .line 204
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O88O:Ljava/util/List;

    .line 205
    .line 206
    new-instance v4, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;

    .line 207
    .line 208
    invoke-direct {v4, v5, v0}, Lcom/intsig/camscanner/mode_ocr/bean/PathBean;-><init>([FZ)V

    .line 209
    .line 210
    .line 211
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    .line 213
    .line 214
    goto :goto_1

    .line 215
    :catch_1
    move-exception p1

    .line 216
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 217
    .line 218
    .line 219
    return-void

    .line 220
    :cond_5
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 221
    .line 222
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 223
    .line 224
    .line 225
    move-result p2

    .line 226
    if-nez p2, :cond_6

    .line 227
    .line 228
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇08oOOO0:Z

    .line 229
    .line 230
    invoke-virtual {p0, v0}, Lcom/github/chrisbanes/photoview/PhotoView;->setIsOnSmear(Z)V

    .line 231
    .line 232
    .line 233
    goto :goto_4

    .line 234
    :cond_6
    iput-boolean v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇08oOOO0:Z

    .line 235
    .line 236
    invoke-virtual {p0, v3}, Lcom/github/chrisbanes/photoview/PhotoView;->setIsOnSmear(Z)V

    .line 237
    .line 238
    .line 239
    :goto_4
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08〇o0O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;

    .line 240
    .line 241
    if-eqz p2, :cond_7

    .line 242
    .line 243
    invoke-virtual {p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇80〇808〇O()V

    .line 244
    .line 245
    .line 246
    :cond_7
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08〇o0O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;

    .line 247
    .line 248
    if-eqz p2, :cond_8

    .line 249
    .line 250
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O88O:Ljava/util/List;

    .line 251
    .line 252
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇O8o08O(Ljava/util/List;)V

    .line 253
    .line 254
    .line 255
    :cond_8
    iget-object p2, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 256
    .line 257
    if-eqz p2, :cond_9

    .line 258
    .line 259
    iget p2, p2, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 260
    .line 261
    if-lez p2, :cond_9

    .line 262
    .line 263
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getAttacher()Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    .line 264
    .line 265
    .line 266
    move-result-object p2

    .line 267
    iget-object p1, p1, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 268
    .line 269
    iget p1, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 270
    .line 271
    int-to-float p1, p1

    .line 272
    neg-float p1, p1

    .line 273
    invoke-virtual {p2, p1}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;->o〇8oOO88(F)V

    .line 274
    .line 275
    .line 276
    :cond_9
    iput-boolean v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇oO:Z

    .line 277
    .line 278
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 279
    .line 280
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 281
    .line 282
    .line 283
    move-result p1

    .line 284
    if-lez p1, :cond_a

    .line 285
    .line 286
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 287
    .line 288
    .line 289
    :cond_a
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    .line 290
    .line 291
    const/16 p2, 0x65

    .line 292
    .line 293
    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    .line 294
    .line 295
    .line 296
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    .line 297
    .line 298
    const-wide/16 v0, 0x12c

    .line 299
    .line 300
    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 301
    .line 302
    .line 303
    return-void
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇O〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, "OcrFrameView"

    .line 8
    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    new-instance v2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v3, "doDraw: "

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const-string v0, "doDraw: mOcrFrameDatas is empty "

    .line 40
    .line 41
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oo〇()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/PreferenceOcrHelper;->o〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo80:Z

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    .line 10
    .line 11
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;-><init>(Landroid/os/Handler;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08〇o0O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;

    .line 15
    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const/16 v1, 0xa

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    int-to-float v0, v0

    .line 27
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇00()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getAttacher()Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    new-instance v1, LoO00〇o/〇〇888;

    .line 37
    .line 38
    invoke-direct {v1, p0}, LoO00〇o/〇〇888;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;->O8O〇(Lcom/github/chrisbanes/photoview/OnScaleChangedListener;)V

    .line 42
    .line 43
    .line 44
    new-instance v0, Landroid/graphics/Path;

    .line 45
    .line 46
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 47
    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 50
    .line 51
    new-instance v0, Landroid/graphics/Path;

    .line 52
    .line 53
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 54
    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O0O:Landroid/graphics/Path;

    .line 57
    .line 58
    const/16 v0, 0x8

    .line 59
    .line 60
    new-array v0, v0, [F

    .line 61
    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO0〇0o:[F

    .line 63
    .line 64
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    const/4 v1, 0x2

    .line 69
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    int-to-float v0, v0

    .line 74
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇o0O:F

    .line 75
    .line 76
    invoke-virtual {p0, p0}, Lcom/github/chrisbanes/photoview/PhotoView;->setOnViewGestureListener(Lcom/github/chrisbanes/photoview/OnViewGestureListener;)V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇〇0o(IZ)V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-ltz p1, :cond_0

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    if-ge p1, v2, :cond_0

    .line 12
    .line 13
    const/4 v2, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v2, 0x0

    .line 16
    :goto_0
    if-eqz v2, :cond_6

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 19
    .line 20
    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇o00〇〇Oo()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    new-instance v3, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v4, "MSG_DEAL_SELECT index:"

    .line 36
    .line 37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v4, "  select"

    .line 44
    .line 45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    const-string v4, "OcrFrameView"

    .line 56
    .line 57
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    if-nez v2, :cond_2

    .line 61
    .line 62
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 63
    .line 64
    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    check-cast v3, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;

    .line 69
    .line 70
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇o〇(Z)V

    .line 71
    .line 72
    .line 73
    iget-boolean v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇OO8ooO8〇:Z

    .line 74
    .line 75
    if-eqz v3, :cond_4

    .line 76
    .line 77
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 78
    .line 79
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    const/4 v4, 0x0

    .line 84
    :goto_1
    if-ge v4, v3, :cond_4

    .line 85
    .line 86
    if-eq v4, p1, :cond_1

    .line 87
    .line 88
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 89
    .line 90
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    move-result-object v5

    .line 94
    check-cast v5, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;

    .line 95
    .line 96
    invoke-virtual {v5, v1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇o〇(Z)V

    .line 97
    .line 98
    .line 99
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_2
    iget-boolean v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo80:Z

    .line 103
    .line 104
    if-nez v3, :cond_3

    .line 105
    .line 106
    if-nez p2, :cond_3

    .line 107
    .line 108
    return-void

    .line 109
    :cond_3
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 110
    .line 111
    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 112
    .line 113
    .line 114
    move-result-object v3

    .line 115
    check-cast v3, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;

    .line 116
    .line 117
    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇o〇(Z)V

    .line 118
    .line 119
    .line 120
    :cond_4
    if-eqz p2, :cond_5

    .line 121
    .line 122
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO:Landroidx/lifecycle/MutableLiveData;

    .line 123
    .line 124
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;

    .line 125
    .line 126
    xor-int/2addr v0, v2

    .line 127
    const/4 v2, 0x5

    .line 128
    invoke-direct {v1, p1, v0, v2}, Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;-><init>(IZI)V

    .line 129
    .line 130
    .line 131
    invoke-virtual {p2, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 132
    .line 133
    .line 134
    goto :goto_2

    .line 135
    :cond_5
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO:Landroidx/lifecycle/MutableLiveData;

    .line 136
    .line 137
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;

    .line 138
    .line 139
    xor-int/2addr v0, v2

    .line 140
    const/4 v2, 0x2

    .line 141
    invoke-direct {v1, p1, v0, v2}, Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;-><init>(IZI)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {p2, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 145
    .line 146
    .line 147
    :goto_2
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 148
    .line 149
    .line 150
    :cond_6
    return-void
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇〇808〇(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x1

    .line 7
    if-eq v0, v2, :cond_5

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v3, 0x3

    .line 14
    if-ne v0, v3, :cond_0

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iget v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    .line 22
    .line 23
    neg-float v3, v3

    .line 24
    cmpg-float v0, v0, v3

    .line 25
    .line 26
    if-lez v0, :cond_3

    .line 27
    .line 28
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    int-to-float v3, v3

    .line 37
    iget v4, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇08O:F

    .line 38
    .line 39
    add-float/2addr v3, v4

    .line 40
    cmpl-float v0, v0, v3

    .line 41
    .line 42
    if-lez v0, :cond_1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo0〇Ooo:Z

    .line 46
    .line 47
    if-nez v0, :cond_2

    .line 48
    .line 49
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->setDefaultPoint(Landroid/view/MotionEvent;)V

    .line 50
    .line 51
    .line 52
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo0〇Ooo:Z

    .line 53
    .line 54
    :cond_2
    return v1

    .line 55
    :cond_3
    :goto_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo0〇Ooo:Z

    .line 56
    .line 57
    if-eqz v0, :cond_4

    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0000OOO()V

    .line 60
    .line 61
    .line 62
    :cond_4
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Oo0〇Ooo:Z

    .line 63
    .line 64
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8oOOo:F

    .line 69
    .line 70
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇O〇〇O8:F

    .line 75
    .line 76
    return v2

    .line 77
    :cond_5
    :goto_1
    const-string p1, "OcrFrameView"

    .line 78
    .line 79
    const-string v0, " ACTION_UP "

    .line 80
    .line 81
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    .line 85
    .line 86
    const/16 v0, 0x69

    .line 87
    .line 88
    const-wide/16 v3, 0xc8

    .line 89
    .line 90
    invoke-virtual {p1, v0, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 91
    .line 92
    .line 93
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0000OOO()V

    .line 94
    .line 95
    .line 96
    const-string p1, "CSOcrResult"

    .line 97
    .line 98
    const-string v0, "apply_times"

    .line 99
    .line 100
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇o88o08〇:Z

    .line 104
    .line 105
    return v2
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇〇8O0〇8(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    const-string v0, "drawOcrFrame: "

    .line 2
    .line 3
    const-string v1, "OcrFrameView"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide v2

    .line 12
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 17
    .line 18
    if-eqz v4, :cond_5

    .line 19
    .line 20
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 21
    .line 22
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v5

    .line 26
    if-lez v5, :cond_4

    .line 27
    .line 28
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 29
    .line 30
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 31
    .line 32
    .line 33
    move-result-object v5

    .line 34
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 35
    .line 36
    .line 37
    move-result v6

    .line 38
    if-eqz v6, :cond_6

    .line 39
    .line 40
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v6

    .line 44
    check-cast v6, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;

    .line 45
    .line 46
    const-string v7, "imageMatrix"

    .line 47
    .line 48
    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v6}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇080()[F

    .line 52
    .line 53
    .line 54
    move-result-object v7

    .line 55
    invoke-direct {p0, v0, v7}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8ooOoo〇(Landroid/graphics/Matrix;[F)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v6}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇o00〇〇Oo()Z

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    if-eqz v7, :cond_3

    .line 63
    .line 64
    iget-boolean v7, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇o〇:Z

    .line 65
    .line 66
    if-nez v7, :cond_1

    .line 67
    .line 68
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8o08O8O:Landroid/graphics/Paint;

    .line 69
    .line 70
    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 71
    .line 72
    .line 73
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 74
    .line 75
    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_1
    invoke-virtual {v6}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇080()[F

    .line 80
    .line 81
    .line 82
    move-result-object v6

    .line 83
    iget-object v7, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇8〇oO〇〇8o:[F

    .line 84
    .line 85
    invoke-direct {p0, v6, v7}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇8O8〇008([F[F)Z

    .line 86
    .line 87
    .line 88
    move-result v6

    .line 89
    if-eqz v6, :cond_2

    .line 90
    .line 91
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 92
    .line 93
    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 94
    .line 95
    .line 96
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇00O:Landroid/graphics/Paint;

    .line 97
    .line 98
    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 99
    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_2
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8o08O8O:Landroid/graphics/Paint;

    .line 103
    .line 104
    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 105
    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_3
    iget-boolean v6, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇o〇:Z

    .line 109
    .line 110
    if-nez v6, :cond_0

    .line 111
    .line 112
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 113
    .line 114
    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_4
    const-string p1, "drawOcrFrame: mOcrFrameDatas is empty "

    .line 119
    .line 120
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    goto :goto_1

    .line 124
    :cond_5
    const-string p1, "drawOcrFrame: mTempPath == null "

    .line 125
    .line 126
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    :cond_6
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 130
    .line 131
    .line 132
    move-result-wide v4

    .line 133
    sub-long/2addr v4, v2

    .line 134
    new-instance p1, Ljava/lang/StringBuilder;

    .line 135
    .line 136
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .line 138
    .line 139
    const-string v0, "drawOcrFrame cost time=: "

    .line 140
    .line 141
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object p1

    .line 151
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method


# virtual methods
.method public final getFocusLivedata()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->Ooo08:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getNeedForbidTouchSelect()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇OO〇00〇0O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getNeedSingleSelect()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇OO8ooO8〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getSelectLineLivedata()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oo8ooo8O:Z

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    const-string v0, "OcrFrameView"

    .line 5
    .line 6
    const-string v1, "onDetachedFromWindow"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OoO8()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 7
    .line 8
    .line 9
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇o88o08〇:Z

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O0O:Landroid/graphics/Path;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO:Landroid/graphics/Paint;

    .line 18
    .line 19
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8o:Z

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇oO:Z

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇8O0〇8(Landroid/graphics/Canvas;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oo8ooo8O:Z

    .line 34
    .line 35
    if-eqz v0, :cond_4

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇8〇oO〇〇8o:[F

    .line 38
    .line 39
    if-eqz v0, :cond_2

    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    const-string v2, "imageMatrix"

    .line 46
    .line 47
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8ooOoo〇(Landroid/graphics/Matrix;[F)V

    .line 51
    .line 52
    .line 53
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 54
    .line 55
    .line 56
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    int-to-float v0, v0

    .line 61
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    int-to-float v1, v1

    .line 66
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->OO〇00〇8oO:Landroid/graphics/Path;

    .line 70
    .line 71
    if-eqz v0, :cond_3

    .line 72
    .line 73
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0O:Landroid/graphics/Paint;

    .line 74
    .line 75
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 76
    .line 77
    .line 78
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo〇8o008:Landroid/graphics/Paint;

    .line 79
    .line 80
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 81
    .line 82
    .line 83
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 84
    .line 85
    .line 86
    :cond_4
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "event"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "event"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇o〇:Z

    .line 7
    .line 8
    new-instance v1, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v2, "onSingleTapUp =>isOnEdit:"

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "OcrFrameView"

    .line 26
    .line 27
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇o〇:Z

    .line 31
    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    const/4 v1, 0x4

    .line 43
    invoke-direct {p0, v0, p1, v1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8〇o(FFI)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    const/4 v1, 0x5

    .line 56
    invoke-direct {p0, v0, p1, v1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8〇o(FFI)V

    .line 57
    .line 58
    .line 59
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "event"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇08oOOO0:Z

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return v1

    .line 12
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇oO:Z

    .line 13
    .line 14
    if-eqz v0, :cond_e

    .line 15
    .line 16
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8o:Z

    .line 17
    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    goto/16 :goto_1

    .line 21
    .line 22
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇o〇:Z

    .line 23
    .line 24
    const/4 v2, 0x1

    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-virtual {p0, v1}, Lcom/github/chrisbanes/photoview/PhotoView;->setForbidOneFinger(Z)V

    .line 28
    .line 29
    .line 30
    return v2

    .line 31
    :cond_2
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇o88o08〇:Z

    .line 32
    .line 33
    if-eqz v0, :cond_3

    .line 34
    .line 35
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇808〇(Landroid/view/MotionEvent;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-eqz v0, :cond_3

    .line 40
    .line 41
    return v2

    .line 42
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    const-string v3, "OcrFrameView"

    .line 47
    .line 48
    if-eqz v0, :cond_a

    .line 49
    .line 50
    const/4 v4, 0x2

    .line 51
    if-eq v0, v4, :cond_5

    .line 52
    .line 53
    const/16 p1, 0x105

    .line 54
    .line 55
    if-eq v0, p1, :cond_4

    .line 56
    .line 57
    goto/16 :goto_0

    .line 58
    .line 59
    :cond_4
    const-string p1, "ACTION_POINTER_2_DOWN"

    .line 60
    .line 61
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇0000OOO()V

    .line 65
    .line 66
    .line 67
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇o88o08〇:Z

    .line 68
    .line 69
    goto/16 :goto_0

    .line 70
    .line 71
    :cond_5
    const-string v0, " ACTION_MOVE "

    .line 72
    .line 73
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇o88o08〇:Z

    .line 77
    .line 78
    if-nez v0, :cond_6

    .line 79
    .line 80
    return v1

    .line 81
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8oOOo:F

    .line 86
    .line 87
    sub-float/2addr v0, v1

    .line 88
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    iget v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇O〇〇O8:F

    .line 97
    .line 98
    sub-float/2addr v1, v3

    .line 99
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    iget v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇o0O:F

    .line 104
    .line 105
    cmpl-float v0, v0, v3

    .line 106
    .line 107
    if-gez v0, :cond_7

    .line 108
    .line 109
    cmpl-float v0, v1, v3

    .line 110
    .line 111
    if-ltz v0, :cond_9

    .line 112
    .line 113
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O0O:Landroid/graphics/Path;

    .line 114
    .line 115
    if-eqz v0, :cond_8

    .line 116
    .line 117
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8oOOo:F

    .line 118
    .line 119
    iget v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇O〇〇O8:F

    .line 120
    .line 121
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 122
    .line 123
    .line 124
    move-result v5

    .line 125
    iget v6, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8oOOo:F

    .line 126
    .line 127
    add-float/2addr v5, v6

    .line 128
    int-to-float v6, v4

    .line 129
    div-float/2addr v5, v6

    .line 130
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 131
    .line 132
    .line 133
    move-result v7

    .line 134
    iget v8, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇O〇〇O8:F

    .line 135
    .line 136
    add-float/2addr v7, v8

    .line 137
    div-float/2addr v7, v6

    .line 138
    invoke-virtual {v0, v1, v3, v5, v7}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 139
    .line 140
    .line 141
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 142
    .line 143
    .line 144
    move-result v0

    .line 145
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8oOOo:F

    .line 146
    .line 147
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 148
    .line 149
    .line 150
    move-result v0

    .line 151
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇O〇〇O8:F

    .line 152
    .line 153
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇00O0:Z

    .line 154
    .line 155
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 156
    .line 157
    .line 158
    :cond_9
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇00O0:Z

    .line 159
    .line 160
    if-eqz v0, :cond_d

    .line 161
    .line 162
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 163
    .line 164
    .line 165
    move-result v0

    .line 166
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 167
    .line 168
    .line 169
    move-result p1

    .line 170
    invoke-direct {p0, v0, p1, v4}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O8〇o(FFI)V

    .line 171
    .line 172
    .line 173
    goto :goto_0

    .line 174
    :cond_a
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇OO〇00〇0O:Z

    .line 175
    .line 176
    if-nez v0, :cond_b

    .line 177
    .line 178
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->O〇o88o08〇:Z

    .line 179
    .line 180
    invoke-virtual {p0, v2}, Lcom/github/chrisbanes/photoview/PhotoView;->setForbidOneFinger(Z)V

    .line 181
    .line 182
    .line 183
    :cond_b
    const-string v0, " ACTION_DOWN "

    .line 184
    .line 185
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    .line 187
    .line 188
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇08〇o0O:Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;

    .line 189
    .line 190
    if-eqz v0, :cond_c

    .line 191
    .line 192
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameViewMoveModel;->〇8o8o〇()V

    .line 193
    .line 194
    .line 195
    :cond_c
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->setDefaultPoint(Landroid/view/MotionEvent;)V

    .line 196
    .line 197
    .line 198
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇00O0:Z

    .line 199
    .line 200
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8〇OO:Landroidx/lifecycle/MutableLiveData;

    .line 201
    .line 202
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;

    .line 203
    .line 204
    const/4 v3, -0x1

    .line 205
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/mode_ocr/bean/SelectLine;-><init>(IZI)V

    .line 206
    .line 207
    .line 208
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 209
    .line 210
    .line 211
    :cond_d
    :goto_0
    return v2

    .line 212
    :cond_e
    :goto_1
    return v1
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final oo〇(Z)V
    .locals 5

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇OO8ooO8〇:Z

    .line 6
    .line 7
    if-eqz v2, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    const/4 v2, 0x0

    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    :goto_0
    if-ge v2, p1, :cond_2

    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 22
    .line 23
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    check-cast v3, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;

    .line 28
    .line 29
    const/4 v4, 0x1

    .line 30
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇o〇(Z)V

    .line 31
    .line 32
    .line 33
    add-int/lit8 v2, v2, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 37
    .line 38
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    const/4 v3, 0x0

    .line 43
    :goto_1
    if-ge v3, p1, :cond_2

    .line 44
    .line 45
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOo0:Ljava/util/List;

    .line 46
    .line 47
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    check-cast v4, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;

    .line 52
    .line 53
    invoke-virtual {v4, v2}, Lcom/intsig/camscanner/mode_ocr/bean/OcrFrameData;->〇o〇(Z)V

    .line 54
    .line 55
    .line 56
    add-int/lit8 v3, v3, 0x1

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 60
    .line 61
    .line 62
    move-result-wide v2

    .line 63
    sub-long/2addr v2, v0

    .line 64
    new-instance p1, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v0, "selectAll time: "

    .line 70
    .line 71
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    const-string v0, "OcrFrameView"

    .line 82
    .line 83
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final o〇8([FI)V
    .locals 4

    .line 1
    iput p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oOO〇〇:I

    .line 2
    .line 3
    if-eqz p1, :cond_3

    .line 4
    .line 5
    array-length p2, p1

    .line 6
    const/4 v0, 0x0

    .line 7
    if-nez p2, :cond_0

    .line 8
    .line 9
    const/4 p2, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 p2, 0x0

    .line 12
    :goto_0
    if-eqz p2, :cond_1

    .line 13
    .line 14
    goto :goto_2

    .line 15
    :cond_1
    array-length p2, p1

    .line 16
    new-array p2, p2, [F

    .line 17
    .line 18
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇8〇oO〇〇8o:[F

    .line 19
    .line 20
    array-length v1, p1

    .line 21
    :goto_1
    if-ge v0, v1, :cond_2

    .line 22
    .line 23
    aget v2, p1, v0

    .line 24
    .line 25
    iget v3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooo0〇〇O:F

    .line 26
    .line 27
    mul-float v2, v2, v3

    .line 28
    .line 29
    aput v2, p2, v0

    .line 30
    .line 31
    add-int/lit8 v0, v0, 0x1

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    .line 35
    .line 36
    const/16 p2, 0x65

    .line 37
    .line 38
    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeMessages(I)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooO:Landroid/os/Handler;

    .line 42
    .line 43
    const-wide/16 v0, 0x12c

    .line 44
    .line 45
    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 46
    .line 47
    .line 48
    return-void

    .line 49
    :cond_3
    :goto_2
    const/4 p1, 0x0

    .line 50
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇8〇oO〇〇8o:[F

    .line 51
    .line 52
    const-string p1, "OcrFrameView"

    .line 53
    .line 54
    const-string p2, "setSelectOcrFrame selectOcrFrame is empty"

    .line 55
    .line 56
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final o〇〇0〇()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setNeedForbidTouchSelect(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇OO〇00〇0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setNeedSingleSelect(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇OO8ooO8〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setOnEdit(Z)V
    .locals 1

    .line 1
    xor-int/lit8 v0, p1, 0x1

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/github/chrisbanes/photoview/PhotoView;->setIsOnSmear(Z)V

    .line 4
    .line 5
    .line 6
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇o〇:Z

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇0〇O0088o(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o8o:Z

    .line 2
    .line 3
    const-string p1, "OcrFrameView"

    .line 4
    .line 5
    const-string v0, "enableDrawOcrFrame"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o(Lcom/intsig/camscanner/mode_ocr/OCRData;I)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mode_ocr/OCRData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "ocrData"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, LoO00〇o/oO80;

    .line 7
    .line 8
    invoke-direct {v0, p0, p1, p2}, LoO00〇o/oO80;-><init>(Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;Lcom/intsig/camscanner/mode_ocr/OCRData;I)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇oOO8O8(I)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇〇0o〇〇0:I

    .line 2
    .line 3
    if-eq v0, p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p1, 0x0

    .line 8
    :goto_0
    return p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇〇0〇〇0(Ljava/lang/String;II)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "inputImagePath"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput p3, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->〇〇〇0o〇〇0:I

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->〇o〇()V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o800o8O(Ljava/lang/String;I)F

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->ooo0〇〇O:F

    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/github/chrisbanes/photoview/PhotoView;->getAttacher()Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    int-to-float p2, p3

    .line 22
    neg-float p2, p2

    .line 23
    invoke-virtual {p1, p2}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;->o〇8oOO88(F)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

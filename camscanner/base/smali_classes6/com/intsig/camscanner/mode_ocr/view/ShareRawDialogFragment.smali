.class public Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;
.super Lcom/intsig/app/BaseBottomSheetDialogFragment;
.source "ShareRawDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;,
        Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogTransClickListener;,
        Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;
    }
.end annotation


# instance fields
.field O0O:[Ljava/lang/String;

.field O88O:[Ljava/lang/String;

.field private O8o08O8O:Landroid/content/Intent;

.field private OO:Landroid/widget/HorizontalScrollView;

.field private OO〇00〇8oO:Landroid/view/View;

.field private o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

.field o8oOOo:[Ljava/lang/String;

.field private o8〇OO0〇0o:Z

.field oOO〇〇:[Ljava/lang/String;

.field private oOo0:Landroid/view/View;

.field private oOo〇8o008:Landroid/widget/TextView;

.field ooo0〇〇O:[Ljava/lang/String;

.field private o〇00O:[Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;

.field private final 〇080OO8〇0:Ljava/lang/String;

.field private final 〇08O〇00〇o:I

.field private 〇0O:Landroid/widget/TextView;

.field 〇8〇oO〇〇8o:[Ljava/lang/String;

.field private 〇OOo8〇0:Landroid/widget/LinearLayout;

.field 〇O〇〇O8:[Ljava/lang/String;

.field 〇o0O:[Ljava/lang/String;

.field 〇〇08O:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x5

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇08O〇00〇o:I

    .line 6
    .line 7
    new-array v0, v0, [Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o〇00O:[Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;

    .line 10
    .line 11
    const-string v0, "ShareRawDialogFragment"

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇080OO8〇0:Ljava/lang/String;

    .line 14
    .line 15
    const-string v0, "com.tencent.mm/com.tencent.mm.ui.tools.ShareImgUI"

    .line 16
    .line 17
    filled-new-array {v0}, [Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇8〇oO〇〇8o:[Ljava/lang/String;

    .line 22
    .line 23
    const-string v0, "com.tencent.eim/com.tencent.mobileqq.activity.JumpActivity"

    .line 24
    .line 25
    const-string v1, "com.tencent.mobileqq/com.tencent.mobileqq.activity.JumpActivity"

    .line 26
    .line 27
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->ooo0〇〇O:[Ljava/lang/String;

    .line 32
    .line 33
    const-string v0, "com.tencent.eim/com.tencent.mobileqq.activity.qfileJumpActivity"

    .line 34
    .line 35
    const-string v1, "com.tencent.mobileqq/com.tencent.mobileqq.activity.qfileJumpActivity"

    .line 36
    .line 37
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇〇08O:[Ljava/lang/String;

    .line 42
    .line 43
    const-string v0, "com.tencent.wework/com.tencent.wework.launch.AppSchemeLaunchActivity"

    .line 44
    .line 45
    filled-new-array {v0}, [Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O0O:[Ljava/lang/String;

    .line 50
    .line 51
    const-string v0, "com.alibaba.android.rimet/com.alibaba.android.rimet.biz.BokuiActivity"

    .line 52
    .line 53
    filled-new-array {v0}, [Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o8oOOo:[Ljava/lang/String;

    .line 58
    .line 59
    const-string v0, "com.whatsapp/com.whatsapp.contact.picker.ContactPicker"

    .line 60
    .line 61
    filled-new-array {v0}, [Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇O〇〇O8:[Ljava/lang/String;

    .line 66
    .line 67
    const-string v0, "com.facebook.orca/com.facebook.messenger.intents.ShareIntentHandler"

    .line 68
    .line 69
    filled-new-array {v0}, [Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇o0O:[Ljava/lang/String;

    .line 74
    .line 75
    const-string v0, "com.google.android.apps.messaging"

    .line 76
    .line 77
    const-string v1, "com.samsung.android.messaging"

    .line 78
    .line 79
    const-string v2, "com.android.mms"

    .line 80
    .line 81
    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O88O:[Ljava/lang/String;

    .line 86
    .line 87
    const-string v0, "com.android.email"

    .line 88
    .line 89
    filled-new-array {v0}, [Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOO〇〇:[Ljava/lang/String;

    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o00〇88〇08(Landroid/content/Intent;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    if-eqz p2, :cond_1

    .line 8
    .line 9
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->OO:Landroid/widget/HorizontalScrollView;

    .line 17
    .line 18
    new-instance v1, LoO00〇o/OO0o〇〇〇〇0;

    .line 19
    .line 20
    invoke-direct {v1, p0, p2, p1}, LoO00〇o/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;Ljava/util/List;Landroid/content/Intent;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 24
    .line 25
    .line 26
    :cond_1
    :goto_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o880(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;Landroid/content/Intent;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o00〇88〇08(Landroid/content/Intent;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/content/Intent;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o〇0〇o(Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/content/Intent;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;Ljava/util/List;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇088O(Ljava/util/List;Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic oooO888(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;)Landroid/content/Intent;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O8o08O8O:Landroid/content/Intent;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o〇0〇o(Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/content/Intent;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o〇O8OO(Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o〇O8OO(Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/content/Intent;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p1, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->〇08O〇00〇o:Ljava/lang/String;

    .line 5
    .line 6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_2

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const-string v1, "android.intent.extra.TEXT"

    .line 17
    .line 18
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    :cond_0
    iget-object v0, p1, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->o〇00O:Ljava/lang/String;

    .line 26
    .line 27
    iget-object v1, p1, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->O8o08O8O:Ljava/lang/String;

    .line 28
    .line 29
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const/16 v1, 0x400

    .line 43
    .line 44
    invoke-virtual {v0, p2, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {p0, p2}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 49
    .line 50
    .line 51
    :goto_0
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 52
    .line 53
    if-eqz p2, :cond_3

    .line 54
    .line 55
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->Oo08(Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;)V

    .line 56
    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 60
    .line 61
    if-eqz p1, :cond_3

    .line 62
    .line 63
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->o〇0()V

    .line 64
    .line 65
    .line 66
    :cond_3
    :goto_1
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic 〇088O(Ljava/util/List;Landroid/content/Intent;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->OO:Landroid/widget/HorizontalScrollView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->OO:Landroid/widget/HorizontalScrollView;

    .line 8
    .line 9
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    int-to-float v2, v0

    .line 14
    const/high16 v3, 0x40900000    # 4.5f

    .line 15
    .line 16
    div-float/2addr v2, v3

    .line 17
    float-to-int v2, v2

    .line 18
    new-instance v3, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v4, "measuredWidth="

    .line 24
    .line 25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v0, " itemWidth="

    .line 32
    .line 33
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const-string v3, "ShareRawDialogFragment"

    .line 44
    .line 45
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    const/4 v0, 0x0

    .line 49
    const/4 v3, 0x0

    .line 50
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    if-ge v3, v4, :cond_0

    .line 55
    .line 56
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 57
    .line 58
    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 63
    .line 64
    .line 65
    move-result-object v4

    .line 66
    const v5, 0x7f0d04bb

    .line 67
    .line 68
    .line 69
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 70
    .line 71
    invoke-virtual {v4, v5, v6, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 72
    .line 73
    .line 74
    move-result-object v4

    .line 75
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o〇00O:[Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;

    .line 76
    .line 77
    new-instance v6, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;

    .line 78
    .line 79
    invoke-direct {v6, p0, v4}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;-><init>(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;Landroid/view/View;)V

    .line 80
    .line 81
    .line 82
    aput-object v6, v5, v3

    .line 83
    .line 84
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 85
    .line 86
    invoke-virtual {v5, v4, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 87
    .line 88
    .line 89
    add-int/lit8 v3, v3, 0x1

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_0
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    if-ge v0, v1, :cond_3

    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o〇00O:[Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;

    .line 99
    .line 100
    aget-object v1, v1, v0

    .line 101
    .line 102
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 107
    .line 108
    if-eqz v2, :cond_2

    .line 109
    .line 110
    iget-object v3, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;->〇o00〇〇Oo:Landroid/widget/TextView;

    .line 111
    .line 112
    iget-object v4, v2, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->〇OOo8〇0:Ljava/lang/String;

    .line 113
    .line 114
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    .line 116
    .line 117
    iget-object v3, v2, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->OO:Landroid/graphics/drawable/Drawable;

    .line 118
    .line 119
    if-eqz v3, :cond_1

    .line 120
    .line 121
    iget-object v4, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;->〇080:Landroid/widget/ImageView;

    .line 122
    .line 123
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 124
    .line 125
    .line 126
    :cond_1
    iget-object v1, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$InnerViewHolder;->〇o〇:Landroid/view/View;

    .line 127
    .line 128
    new-instance v3, LoO00〇o/〇8o8o〇;

    .line 129
    .line 130
    invoke-direct {v3, p0, v2, p2}, LoO00〇o/〇8o8o〇;-><init>(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/content/Intent;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    .line 135
    .line 136
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 137
    .line 138
    goto :goto_1

    .line 139
    :cond_3
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇8〇OOoooo()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O8o08O8O:Landroid/content/Intent;

    .line 7
    .line 8
    const-string v1, "android.intent.action.SEND"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O8o08O8O:Landroid/content/Intent;

    .line 14
    .line 15
    const-string v1, "text/plain"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 21
    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O8o08O8O:Landroid/content/Intent;

    .line 25
    .line 26
    const-string v2, "android.intent.extra.TEXT"

    .line 27
    .line 28
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    .line 34
    .line 35
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇0O:Landroid/widget/TextView;

    .line 36
    .line 37
    new-instance v1, LoO00〇o/〇80〇808〇O;

    .line 38
    .line 39
    invoke-direct {v1, p0}, LoO00〇o/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 43
    .line 44
    .line 45
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$1;

    .line 50
    .line 51
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$1;-><init>(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;Landroid/content/Context;)V

    .line 52
    .line 53
    .line 54
    const-string v0, "ShareRawDialogFragment"

    .line 55
    .line 56
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oooo8o0〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0()V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇o〇88〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇o〇88〇8()V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇0O:Landroid/widget/TextView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 8
    .line 9
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    if-eq v2, v0, :cond_0

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇0O:Landroid/widget/TextView;

    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 26
    .line 27
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 28
    .line 29
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇0O:Landroid/widget/TextView;

    .line 30
    .line 31
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    if-eq v2, v1, :cond_1

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 37
    .line 38
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 43
    .line 44
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 47
    .line 48
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    const-string v1, "ShareRawDialogFragment"

    .line 54
    .line 55
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    .line 57
    .line 58
    :cond_1
    :goto_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇〇o0〇8(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇0O:Landroid/widget/TextView;

    .line 2
    .line 3
    const v1, 0x7f080275

    .line 4
    .line 5
    .line 6
    const v2, 0x7f060669

    .line 7
    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    const v3, 0x7f080275

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const v3, 0x7f060669

    .line 16
    .line 17
    .line 18
    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 22
    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    const v1, 0x7f060669

    .line 26
    .line 27
    .line 28
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇0O:Landroid/widget/TextView;

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 41
    .line 42
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    xor-int/lit8 v1, p1, 0x1

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 52
    .line 53
    if-eqz v0, :cond_2

    .line 54
    .line 55
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->oO80(Z)V

    .line 56
    .line 57
    .line 58
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 59
    .line 60
    if-eqz v0, :cond_3

    .line 61
    .line 62
    invoke-static {}, Lcom/intsig/utils/SharedPreferencesHelper;->O8()Lcom/intsig/utils/SharedPreferencesHelper;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 67
    .line 68
    invoke-interface {v1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->O8()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    invoke-virtual {v0, v1, p1}, Lcom/intsig/utils/SharedPreferencesHelper;->oO80(Ljava/lang/String;Z)V

    .line 73
    .line 74
    .line 75
    :cond_3
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public O0〇0()Ljava/util/List;
    .locals 21
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->Oooo8o0〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v2, 0x8

    .line 8
    .line 9
    const/4 v3, 0x7

    .line 10
    const/4 v4, 0x6

    .line 11
    const/4 v5, 0x5

    .line 12
    const/4 v6, 0x3

    .line 13
    const/4 v7, 0x2

    .line 14
    const/16 v8, 0x9

    .line 15
    .line 16
    const/4 v9, 0x4

    .line 17
    const/4 v10, 0x0

    .line 18
    const/4 v11, 0x1

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    new-array v8, v8, [[Ljava/lang/String;

    .line 22
    .line 23
    iget-object v12, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇8〇oO〇〇8o:[Ljava/lang/String;

    .line 24
    .line 25
    aput-object v12, v8, v10

    .line 26
    .line 27
    iget-object v12, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->ooo0〇〇O:[Ljava/lang/String;

    .line 28
    .line 29
    aput-object v12, v8, v11

    .line 30
    .line 31
    iget-object v12, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇〇08O:[Ljava/lang/String;

    .line 32
    .line 33
    aput-object v12, v8, v7

    .line 34
    .line 35
    iget-object v7, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O0O:[Ljava/lang/String;

    .line 36
    .line 37
    aput-object v7, v8, v6

    .line 38
    .line 39
    iget-object v6, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o8oOOo:[Ljava/lang/String;

    .line 40
    .line 41
    aput-object v6, v8, v9

    .line 42
    .line 43
    iget-object v6, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇O〇〇O8:[Ljava/lang/String;

    .line 44
    .line 45
    aput-object v6, v8, v5

    .line 46
    .line 47
    iget-object v5, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇o0O:[Ljava/lang/String;

    .line 48
    .line 49
    aput-object v5, v8, v4

    .line 50
    .line 51
    iget-object v4, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O88O:[Ljava/lang/String;

    .line 52
    .line 53
    aput-object v4, v8, v3

    .line 54
    .line 55
    iget-object v3, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOO〇〇:[Ljava/lang/String;

    .line 56
    .line 57
    aput-object v3, v8, v2

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    new-array v8, v8, [[Ljava/lang/String;

    .line 61
    .line 62
    iget-object v12, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇O〇〇O8:[Ljava/lang/String;

    .line 63
    .line 64
    aput-object v12, v8, v10

    .line 65
    .line 66
    iget-object v12, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇o0O:[Ljava/lang/String;

    .line 67
    .line 68
    aput-object v12, v8, v11

    .line 69
    .line 70
    iget-object v12, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O88O:[Ljava/lang/String;

    .line 71
    .line 72
    aput-object v12, v8, v7

    .line 73
    .line 74
    iget-object v7, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOO〇〇:[Ljava/lang/String;

    .line 75
    .line 76
    aput-object v7, v8, v6

    .line 77
    .line 78
    iget-object v6, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇8〇oO〇〇8o:[Ljava/lang/String;

    .line 79
    .line 80
    aput-object v6, v8, v9

    .line 81
    .line 82
    iget-object v6, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->ooo0〇〇O:[Ljava/lang/String;

    .line 83
    .line 84
    aput-object v6, v8, v5

    .line 85
    .line 86
    iget-object v5, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇〇08O:[Ljava/lang/String;

    .line 87
    .line 88
    aput-object v5, v8, v4

    .line 89
    .line 90
    iget-object v4, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O0O:[Ljava/lang/String;

    .line 91
    .line 92
    aput-object v4, v8, v3

    .line 93
    .line 94
    iget-object v3, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o8oOOo:[Ljava/lang/String;

    .line 95
    .line 96
    aput-object v3, v8, v2

    .line 97
    .line 98
    :goto_0
    if-eqz v0, :cond_1

    .line 99
    .line 100
    const-string v12, "wechat"

    .line 101
    .line 102
    const-string v13, "qq"

    .line 103
    .line 104
    const-string v14, "qq_to_pc"

    .line 105
    .line 106
    const-string v15, "business_wechat"

    .line 107
    .line 108
    const-string v16, "ding_talk"

    .line 109
    .line 110
    const-string v17, "whatsapp"

    .line 111
    .line 112
    const-string v18, "facebook_msg"

    .line 113
    .line 114
    const-string v19, "short_msg"

    .line 115
    .line 116
    const-string v20, "email"

    .line 117
    .line 118
    filled-new-array/range {v12 .. v20}, [Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    goto :goto_1

    .line 123
    :cond_1
    const-string v12, "whatsapp"

    .line 124
    .line 125
    const-string v13, "facebook_msg"

    .line 126
    .line 127
    const-string v14, "short_msg"

    .line 128
    .line 129
    const-string v15, "email"

    .line 130
    .line 131
    const-string v16, "wechat"

    .line 132
    .line 133
    const-string v17, "qq"

    .line 134
    .line 135
    const-string v18, "qq_to_pc"

    .line 136
    .line 137
    const-string v19, "business_wechat"

    .line 138
    .line 139
    const-string v20, "ding_talk"

    .line 140
    .line 141
    filled-new-array/range {v12 .. v20}, [Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    :goto_1
    move-object v2, v0

    .line 146
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    new-instance v3, Ljava/util/ArrayList;

    .line 151
    .line 152
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .line 154
    .line 155
    if-nez v0, :cond_2

    .line 156
    .line 157
    return-object v3

    .line 158
    :cond_2
    new-instance v4, Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 159
    .line 160
    invoke-direct {v4, v0}, Lcom/intsig/camscanner/share/ShareDataPresenter;-><init>(Landroid/content/Context;)V

    .line 161
    .line 162
    .line 163
    iget-object v5, v1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->O8o08O8O:Landroid/content/Intent;

    .line 164
    .line 165
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇80〇808〇O(Landroid/content/Intent;)Ljava/util/ArrayList;

    .line 166
    .line 167
    .line 168
    move-result-object v5

    .line 169
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 170
    .line 171
    .line 172
    move-result-object v6

    .line 173
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 174
    .line 175
    .line 176
    move-result-object v5

    .line 177
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 178
    .line 179
    .line 180
    move-result v0

    .line 181
    if-eqz v0, :cond_8

    .line 182
    .line 183
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 184
    .line 185
    .line 186
    move-result-object v0

    .line 187
    move-object v7, v0

    .line 188
    check-cast v7, Landroid/content/pm/ResolveInfo;

    .line 189
    .line 190
    invoke-virtual {v4, v7}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇8o8o〇(Landroid/content/pm/ResolveInfo;)Landroid/content/pm/ComponentInfo;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    if-eqz v0, :cond_7

    .line 195
    .line 196
    iget-object v12, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    .line 197
    .line 198
    iget-object v13, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    .line 199
    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    .line 201
    .line 202
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 203
    .line 204
    .line 205
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    .line 207
    .line 208
    const-string v14, "/"

    .line 209
    .line 210
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    .line 215
    .line 216
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    const/4 v15, 0x0

    .line 221
    :goto_3
    array-length v10, v8

    .line 222
    if-ge v15, v10, :cond_7

    .line 223
    .line 224
    aget-object v10, v8, v15

    .line 225
    .line 226
    const/4 v9, 0x0

    .line 227
    :goto_4
    array-length v11, v10

    .line 228
    if-ge v9, v11, :cond_6

    .line 229
    .line 230
    aget-object v11, v8, v15

    .line 231
    .line 232
    aget-object v11, v11, v9

    .line 233
    .line 234
    invoke-virtual {v11, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 235
    .line 236
    .line 237
    move-result v19

    .line 238
    const/16 v18, 0x1

    .line 239
    .line 240
    xor-int/lit8 v19, v19, 0x1

    .line 241
    .line 242
    invoke-static {v11, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 243
    .line 244
    .line 245
    move-result v20

    .line 246
    if-nez v20, :cond_4

    .line 247
    .line 248
    if-eqz v19, :cond_3

    .line 249
    .line 250
    invoke-static {v11, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 251
    .line 252
    .line 253
    move-result v11

    .line 254
    if-eqz v11, :cond_3

    .line 255
    .line 256
    goto :goto_5

    .line 257
    :cond_3
    add-int/lit8 v9, v9, 0x1

    .line 258
    .line 259
    goto :goto_4

    .line 260
    :cond_4
    :goto_5
    if-eqz v19, :cond_5

    .line 261
    .line 262
    move-object v9, v12

    .line 263
    goto :goto_6

    .line 264
    :cond_5
    move-object v9, v0

    .line 265
    :goto_6
    :try_start_0
    invoke-virtual {v7, v6}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    .line 266
    .line 267
    .line 268
    move-result-object v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 269
    :try_start_1
    invoke-virtual {v7, v6}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    .line 270
    .line 271
    .line 272
    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 273
    goto :goto_8

    .line 274
    :catch_0
    move-exception v0

    .line 275
    goto :goto_7

    .line 276
    :catch_1
    move-exception v0

    .line 277
    const-string v10, ""

    .line 278
    .line 279
    :goto_7
    const-string v11, "ShareRawDialogFragment"

    .line 280
    .line 281
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 282
    .line 283
    .line 284
    const/4 v0, 0x0

    .line 285
    :goto_8
    new-instance v11, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 286
    .line 287
    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    .line 288
    .line 289
    .line 290
    move-result-object v10

    .line 291
    invoke-direct {v11, v7, v10, v0, v9}, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;-><init>(Landroid/content/pm/ResolveInfo;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 292
    .line 293
    .line 294
    invoke-virtual {v11, v12, v13}, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->〇o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 295
    .line 296
    .line 297
    move-result-object v0

    .line 298
    aget-object v7, v2, v15

    .line 299
    .line 300
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 301
    .line 302
    .line 303
    move-result-object v0

    .line 304
    invoke-virtual {v0, v15}, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->O8(I)Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 305
    .line 306
    .line 307
    move-result-object v0

    .line 308
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    .line 310
    .line 311
    goto :goto_9

    .line 312
    :cond_6
    const/16 v18, 0x1

    .line 313
    .line 314
    add-int/lit8 v15, v15, 0x1

    .line 315
    .line 316
    const/4 v9, 0x4

    .line 317
    const/4 v11, 0x1

    .line 318
    goto :goto_3

    .line 319
    :cond_7
    const/16 v18, 0x1

    .line 320
    .line 321
    :goto_9
    const/4 v9, 0x4

    .line 322
    const/4 v10, 0x0

    .line 323
    const/4 v11, 0x1

    .line 324
    goto/16 :goto_2

    .line 325
    .line 326
    :cond_8
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 327
    .line 328
    .line 329
    new-instance v0, Ljava/util/ArrayList;

    .line 330
    .line 331
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 332
    .line 333
    .line 334
    const/4 v2, 0x4

    .line 335
    const/4 v10, 0x0

    .line 336
    :goto_a
    if-ge v10, v2, :cond_a

    .line 337
    .line 338
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 339
    .line 340
    .line 341
    move-result v4

    .line 342
    if-le v4, v10, :cond_9

    .line 343
    .line 344
    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 345
    .line 346
    .line 347
    move-result-object v4

    .line 348
    check-cast v4, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 349
    .line 350
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    .line 352
    .line 353
    :cond_9
    add-int/lit8 v10, v10, 0x1

    .line 354
    .line 355
    goto :goto_a

    .line 356
    :cond_a
    return-object v0
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public getTheme()I
    .locals 1

    .line 1
    const v0, 0x7f140193

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇8〇OOoooo()V

    .line 5
    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->〇80〇808〇O()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o8〇OO0〇0o:Z

    .line 16
    .line 17
    :cond_0
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o8〇OO0〇0o:Z

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    const/16 v1, 0x8

    .line 21
    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 25
    .line 26
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo0:Landroid/view/View;

    .line 30
    .line 31
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    if-eqz p1, :cond_1

    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/utils/SharedPreferencesHelper;->O8()Lcom/intsig/utils/SharedPreferencesHelper;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 44
    .line 45
    invoke-interface {v1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->O8()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {p1, v1, v0}, Lcom/intsig/utils/SharedPreferencesHelper;->〇o00〇〇Oo(Ljava/lang/String;Z)Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    :cond_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇〇o0〇8(Z)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 58
    .line 59
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 60
    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo0:Landroid/view/View;

    .line 63
    .line 64
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 65
    .line 66
    .line 67
    :goto_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    sparse-switch p1, :sswitch_data_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :sswitch_0
    const/4 p1, 0x0

    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇〇o0〇8(Z)V

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :sswitch_1
    const/4 p1, 0x1

    .line 20
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇〇o0〇8(Z)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :sswitch_2
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 25
    .line 26
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->〇080()V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :sswitch_3
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 34
    .line 35
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->〇o00〇〇Oo()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :sswitch_4
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 43
    .line 44
    instance-of v0, p1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogTransClickListener;

    .line 45
    .line 46
    if-eqz v0, :cond_1

    .line 47
    .line 48
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogTransClickListener;

    .line 49
    .line 50
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogTransClickListener;->〇〇888()V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :sswitch_5
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 58
    .line 59
    .line 60
    :cond_1
    :goto_0
    return-void

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x7f0a0885 -> :sswitch_5
        0x7f0a1130 -> :sswitch_4
        0x7f0a1133 -> :sswitch_3
        0x7f0a1134 -> :sswitch_2
        0x7f0a126a -> :sswitch_1
        0x7f0a17de -> :sswitch_0
    .end sparse-switch
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    const v0, 0x7f140193

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/DialogFragment;->setStyle(II)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const p2, 0x7f0a1134

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 12
    .line 13
    .line 14
    const p2, 0x7f0a1133

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    .line 23
    .line 24
    const p2, 0x7f0a0885

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    .line 33
    .line 34
    const p2, 0x7f0a1417

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo0:Landroid/view/View;

    .line 42
    .line 43
    const p2, 0x7f0a0cc2

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object p2

    .line 50
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 51
    .line 52
    const p2, 0x7f0a126a

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    check-cast p2, Landroid/widget/TextView;

    .line 60
    .line 61
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇0O:Landroid/widget/TextView;

    .line 62
    .line 63
    const p2, 0x7f0a17de

    .line 64
    .line 65
    .line 66
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 67
    .line 68
    .line 69
    move-result-object p2

    .line 70
    check-cast p2, Landroid/widget/TextView;

    .line 71
    .line 72
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 73
    .line 74
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇0O:Landroid/widget/TextView;

    .line 75
    .line 76
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    .line 78
    .line 79
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 80
    .line 81
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    .line 83
    .line 84
    const p2, 0x7f0a0ce0

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 88
    .line 89
    .line 90
    move-result-object p2

    .line 91
    check-cast p2, Landroid/widget/LinearLayout;

    .line 92
    .line 93
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 94
    .line 95
    const p2, 0x7f0a070e

    .line 96
    .line 97
    .line 98
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 99
    .line 100
    .line 101
    move-result-object p2

    .line 102
    check-cast p2, Landroid/widget/HorizontalScrollView;

    .line 103
    .line 104
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->OO:Landroid/widget/HorizontalScrollView;

    .line 105
    .line 106
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 107
    .line 108
    if-eqz p2, :cond_3

    .line 109
    .line 110
    const p2, 0x7f0a1130

    .line 111
    .line 112
    .line 113
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    const v0, 0x7f0a19b7

    .line 118
    .line 119
    .line 120
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 125
    .line 126
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;->〇o〇()Z

    .line 127
    .line 128
    .line 129
    move-result v0

    .line 130
    if-eqz v0, :cond_1

    .line 131
    .line 132
    const/4 v0, 0x0

    .line 133
    if-eqz p2, :cond_0

    .line 134
    .line 135
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 136
    .line 137
    .line 138
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    .line 140
    .line 141
    :cond_0
    if-eqz p1, :cond_3

    .line 142
    .line 143
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 144
    .line 145
    .line 146
    goto :goto_0

    .line 147
    :cond_1
    const/16 v0, 0x8

    .line 148
    .line 149
    if-eqz p2, :cond_2

    .line 150
    .line 151
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 152
    .line 153
    .line 154
    :cond_2
    if-eqz p1, :cond_3

    .line 155
    .line 156
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 157
    .line 158
    .line 159
    :cond_3
    :goto_0
    return-void
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method protected provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0341

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1, p0, p2}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :catch_0
    move-exception p1

    .line 13
    const-string p2, "WxInstallDialog"

    .line 14
    .line 15
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8〇80o(Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment;->o0:Lcom/intsig/camscanner/mode_ocr/view/ShareRawDialogFragment$ShareDialogClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

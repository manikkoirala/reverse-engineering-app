.class final Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ImageDealInterceptor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$ImageDealListener;Landroidx/lifecycle/MutableLiveData;Landroidx/lifecycle/LifecycleOwner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->〇080(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 7
    .line 8
    return-object p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇080(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V
    .locals 7

    .line 1
    const-string v0, "ImageDealInterceptor"

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    const-string p1, "backScanPageModel == null"

    .line 6
    .line 7
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 12
    .line 13
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 14
    .line 15
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->〇O〇(J)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-nez v1, :cond_1

    .line 20
    .line 21
    const-string p1, "pageId is not in deal list"

    .line 22
    .line 23
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    iget-object v1, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 28
    .line 29
    const-string v2, "backScanPageModel.imageRawPath"

    .line 30
    .line 31
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->O8(Ljava/lang/String;)Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    if-nez v2, :cond_2

    .line 43
    .line 44
    const-string p1, "ocrData == null"

    .line 45
    .line 46
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_2
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 51
    .line 52
    invoke-static {v3}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->〇O8o08O(Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;)I

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    add-int/lit8 v4, v4, 0x1

    .line 57
    .line 58
    invoke-static {v3, v4}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->Oooo8o0〇(Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;I)V

    .line 59
    .line 60
    .line 61
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 62
    .line 63
    invoke-static {v3}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->〇O8o08O(Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;)I

    .line 64
    .line 65
    .line 66
    move-result v4

    .line 67
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->〇O888o0o(I)V

    .line 68
    .line 69
    .line 70
    iget-wide v3, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 71
    .line 72
    new-instance v5, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    const-string v6, "observe imageId "

    .line 78
    .line 79
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    iput-object v1, v2, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 93
    .line 94
    iget-object v3, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 95
    .line 96
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8〇o(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    const/4 v3, 0x0

    .line 100
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo〇(Z)V

    .line 101
    .line 102
    .line 103
    const/4 v4, 0x0

    .line 104
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/mode_ocr/OCRData;->o0ooO(Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    iput-object v4, v2, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8o:Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 108
    .line 109
    iget v4, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇8o8o〇:I

    .line 110
    .line 111
    iput v4, v2, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 112
    .line 113
    invoke-static {v1}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    iget-object p1, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 118
    .line 119
    if-eqz p1, :cond_3

    .line 120
    .line 121
    invoke-static {v1, v1, p1, v3}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    iput-object p1, v2, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO:Ljava/lang/String;

    .line 126
    .line 127
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 128
    .line 129
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->〇O8o08O(Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;)I

    .line 130
    .line 131
    .line 132
    move-result p1

    .line 133
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 134
    .line 135
    invoke-static {v1}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->〇8o8o〇(Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;)I

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    if-ne p1, v1, :cond_5

    .line 140
    .line 141
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 142
    .line 143
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->OO0o〇〇(Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;)Z

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    if-nez p1, :cond_4

    .line 148
    .line 149
    return-void

    .line 150
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 151
    .line 152
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->〇O00()V

    .line 153
    .line 154
    .line 155
    const-string p1, "observe deal end "

    .line 156
    .line 157
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor$1;->o0:Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;

    .line 161
    .line 162
    invoke-static {p1, v3}, Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;->〇〇808〇(Lcom/intsig/camscanner/mode_ocr/ImageDealInterceptor;Z)V

    .line 163
    .line 164
    .line 165
    :cond_5
    return-void
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

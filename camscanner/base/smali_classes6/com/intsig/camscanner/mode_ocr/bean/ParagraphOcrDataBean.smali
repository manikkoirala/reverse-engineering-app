.class public Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;
.super Ljava/lang/Object;
.source "ParagraphOcrDataBean.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;",
            ">;"
        }
    .end annotation
.end field

.field public static OCR_VERSION:Ljava/lang/String; = "1.0"


# instance fields
.field public autoWrap:I

.field public image_height:I

.field public image_width:I

.field public ocr_version:Ljava/lang/String;

.field public position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;",
            ">;"
        }
    .end annotation
.end field

.field public rotate_angle:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->autoWrap:I

    const-string v0, "0.0"

    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->ocr_version:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->autoWrap:I

    const-string v0, "0.0"

    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->ocr_version:Ljava/lang/String;

    .line 7
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 9
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 10
    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->addAll(Ljava/util/Collection;)Z

    .line 11
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 12
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 14
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->ocr_version:Ljava/lang/String;

    .line 15
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->autoWrap:I

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-lez v1, :cond_0

    .line 16
    .line 17
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 20
    .line 21
    .line 22
    iput-object v1, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    if-eqz v2, :cond_0

    .line 35
    .line 36
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;

    .line 41
    .line 42
    iget-object v3, v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 43
    .line 44
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->clone()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;

    .line 49
    .line 50
    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    if-eqz p1, :cond_3

    .line 7
    .line 8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    if-eq v2, v3, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;

    .line 20
    .line 21
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 22
    .line 23
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 24
    .line 25
    if-ne v2, v3, :cond_2

    .line 26
    .line 27
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 28
    .line 29
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 30
    .line 31
    if-ne v2, v3, :cond_2

    .line 32
    .line 33
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 34
    .line 35
    iget v3, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 36
    .line 37
    if-ne v2, v3, :cond_2

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->ocr_version:Ljava/lang/String;

    .line 40
    .line 41
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->ocr_version:Ljava/lang/String;

    .line 42
    .line 43
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-eqz v2, :cond_2

    .line 48
    .line 49
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 50
    .line 51
    iget-object v3, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 52
    .line 53
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    if-eqz v2, :cond_2

    .line 58
    .line 59
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->autoWrap:I

    .line 60
    .line 61
    iget p1, p1, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->autoWrap:I

    .line 62
    .line 63
    if-ne v2, p1, :cond_2

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_2
    const/4 v0, 0x0

    .line 67
    :goto_0
    return v0

    .line 68
    :cond_3
    :goto_1
    return v1
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public fixRotateParagraphOcrDataBean()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hashCode()I
    .locals 3

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 10
    .line 11
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const/4 v2, 0x1

    .line 16
    aput-object v1, v0, v2

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 19
    .line 20
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const/4 v2, 0x2

    .line 25
    aput-object v1, v0, v2

    .line 26
    .line 27
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 28
    .line 29
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    const/4 v2, 0x3

    .line 34
    aput-object v1, v0, v2

    .line 35
    .line 36
    const/4 v1, 0x4

    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->ocr_version:Ljava/lang/String;

    .line 38
    .line 39
    aput-object v2, v0, v1

    .line 40
    .line 41
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->autoWrap:I

    .line 42
    .line 43
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    const/4 v2, 0x5

    .line 48
    aput-object v1, v0, v2

    .line 49
    .line 50
    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    return v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public isSupportCurrentVer()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->OCR_VERSION:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->ocr_version:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public scalePosition(F)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    mul-float v0, v0, p1

    .line 5
    .line 6
    float-to-int v0, v0

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 10
    .line 11
    int-to-float v0, v0

    .line 12
    mul-float v0, v0, p1

    .line 13
    .line 14
    float-to-int v0, v0

    .line 15
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_1

    .line 31
    .line 32
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;

    .line 37
    .line 38
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrParagraphBean;->scalePosition(F)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->position_detail:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2
    .line 3
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 4
    .line 5
    .line 6
    iget p2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_width:I

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    .line 10
    .line 11
    iget p2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->image_height:I

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 14
    .line 15
    .line 16
    iget p2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->rotate_angle:I

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    .line 20
    .line 21
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->ocr_version:Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget p2, p0, Lcom/intsig/camscanner/mode_ocr/bean/ParagraphOcrDataBean;->autoWrap:I

    .line 27
    .line 28
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

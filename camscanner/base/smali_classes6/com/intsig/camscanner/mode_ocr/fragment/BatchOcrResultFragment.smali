.class public final Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "BatchOcrResultFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$SaveOcrResultDialog;,
        Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOO0880O:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic oOoo80oO:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O0O:I

.field private final O88O:Lcom/intsig/camscanner/mode_ocr/OCRClient;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o08O8O:Landroid/widget/TextView;

.field private final OO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

.field private final Oo0〇Ooo:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mClipListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo80:F

.field private Ooo08:Z

.field private O〇08oOOO0:Z

.field private O〇o88o08〇:F

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0OoOOo0:I

.field private o8o:Lcom/intsig/camscanner/tools/OcrTextShareClient;

.field private o8oOOo:Z

.field private o8〇OO:Z

.field private o8〇OO0〇0o:Z

.field private oO00〇o:I

.field private oOO〇〇:Lcom/intsig/camscanner/tools/TranslateClient;

.field private oOo0:Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;

.field private oOo〇8o008:Landroid/widget/TextView;

.field private final oO〇8O8oOo:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mImgTouchBack$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oo8ooo8O:Lcom/intsig/app/AlertDialog;

.field private ooO:Z

.field private ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Landroid/widget/LinearLayout;

.field private o〇oO:Z

.field private o〇o〇Oo88:Landroid/app/Dialog;

.field private 〇00O0:I

.field private 〇080OO8〇0:Landroid/widget/ImageView;

.field private final 〇08O〇00〇o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08〇o0O:Z

.field private 〇0O:Landroid/widget/ImageView;

.field private final 〇0O〇O00O:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$ocrProgressListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:Z

.field private 〇OO8ooO8〇:Z

.field private final 〇OOo8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OO〇00〇0O:Landroid/content/res/Configuration;

.field private 〇O〇〇O8:I

.field private 〇o0O:I

.field private 〇〇08O:Ljava/lang/String;

.field private 〇〇o〇:Z

.field private final 〇〇〇0o〇〇0:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOoo80oO:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOO0880O:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 19
    .line 20
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mViewModel$2;

    .line 21
    .line 22
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mViewModel$2;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 30
    .line 31
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mImgViewModel$2;

    .line 32
    .line 33
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mImgViewModel$2;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 34
    .line 35
    .line 36
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO:Lkotlin/Lazy;

    .line 41
    .line 42
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mDataDealViewModel$2;

    .line 43
    .line 44
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mDataDealViewModel$2;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 45
    .line 46
    .line 47
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08O〇00〇o:Lkotlin/Lazy;

    .line 52
    .line 53
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 54
    .line 55
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 56
    .line 57
    .line 58
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 59
    .line 60
    const/4 v0, -0x1

    .line 61
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0O:I

    .line 62
    .line 63
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 64
    .line 65
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient;-><init>()V

    .line 66
    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O88O:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 69
    .line 70
    const/4 v0, 0x1

    .line 71
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇oO:Z

    .line 72
    .line 73
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00O0:I

    .line 74
    .line 75
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇OO8ooO8〇:Z

    .line 76
    .line 77
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mClipListener$1;

    .line 78
    .line 79
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mClipListener$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 80
    .line 81
    .line 82
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo0〇Ooo:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mClipListener$1;

    .line 83
    .line 84
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;

    .line 85
    .line 86
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 87
    .line 88
    .line 89
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;

    .line 90
    .line 91
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mImgTouchBack$1;

    .line 92
    .line 93
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mImgTouchBack$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 94
    .line 95
    .line 96
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mImgTouchBack$1;

    .line 97
    .line 98
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$ocrProgressListener$1;

    .line 99
    .line 100
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$ocrProgressListener$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 101
    .line 102
    .line 103
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O〇O00O:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$ocrProgressListener$1;

    .line 104
    .line 105
    const/4 v0, 0x5

    .line 106
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO00〇o:I

    .line 107
    .line 108
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final O008o8oo(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O008oO0()V
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0O:I

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "refreshCloudOceBtn mCloudOcrLeftNum:"

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "BatchOcrResultFragment"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0O:I

    .line 32
    .line 33
    const/16 v2, 0x64

    .line 34
    .line 35
    const/4 v3, 0x0

    .line 36
    const/4 v4, 0x1

    .line 37
    if-ge v1, v2, :cond_1

    .line 38
    .line 39
    if-gtz v1, :cond_0

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const/4 v2, 0x0

    .line 43
    goto :goto_1

    .line 44
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 45
    :goto_1
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇oO:Z

    .line 46
    .line 47
    if-eqz v2, :cond_2

    .line 48
    .line 49
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇8〇oO〇〇8o:Lcom/intsig/view/ImageTextButton;

    .line 50
    .line 51
    const-wide/16 v2, -0x1

    .line 52
    .line 53
    invoke-virtual {v1, v2, v3}, Lcom/intsig/view/ImageTextButton;->setDotNum(J)V

    .line 54
    .line 55
    .line 56
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇8〇oO〇〇8o:Lcom/intsig/view/ImageTextButton;

    .line 57
    .line 58
    invoke-virtual {v0, v4}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 59
    .line 60
    .line 61
    goto :goto_2

    .line 62
    :cond_2
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇8〇oO〇〇8o:Lcom/intsig/view/ImageTextButton;

    .line 63
    .line 64
    int-to-long v4, v1

    .line 65
    invoke-virtual {v2, v4, v5}, Lcom/intsig/view/ImageTextButton;->setDotNum(J)V

    .line 66
    .line 67
    .line 68
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇8〇oO〇〇8o:Lcom/intsig/view/ImageTextButton;

    .line 69
    .line 70
    invoke-virtual {v0, v3}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 71
    .line 72
    .line 73
    :cond_3
    :goto_2
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O00OoO〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇O〇〇O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08O〇00〇o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O08o(Z)V
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "showDeletedHintDialog:"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "BatchOcrResultFragment"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo8ooo8O:Lcom/intsig/app/AlertDialog;

    .line 24
    .line 25
    const v1, 0x7f131701

    .line 26
    .line 27
    .line 28
    if-nez v0, :cond_0

    .line 29
    .line 30
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 33
    .line 34
    invoke-direct {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 35
    .line 36
    .line 37
    const v2, 0x7f131700

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    new-instance v2, L〇0O〇O00O/o〇O8〇〇o;

    .line 49
    .line 50
    invoke-direct {v2}, L〇0O〇O00O/o〇O8〇〇o;-><init>()V

    .line 51
    .line 52
    .line 53
    const v3, 0x7f130c86

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo8ooo8O:Lcom/intsig/app/AlertDialog;

    .line 65
    .line 66
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo8ooo8O:Lcom/intsig/app/AlertDialog;

    .line 67
    .line 68
    if-eqz v0, :cond_2

    .line 69
    .line 70
    if-eqz p1, :cond_1

    .line 71
    .line 72
    const p1, 0x7f1319ca

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {v0, p1}, Lcom/intsig/app/AlertDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    invoke-virtual {v0, p1}, Lcom/intsig/app/AlertDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 88
    .line 89
    .line 90
    :goto_0
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 91
    .line 92
    .line 93
    move-result p1

    .line 94
    if-nez p1, :cond_2

    .line 95
    .line 96
    const-string p1, "CSOcrDeletePop"

    .line 97
    .line 98
    invoke-static {p1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 102
    .line 103
    .line 104
    :cond_2
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic O08〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O08〇oO8〇(Landroid/view/MotionEvent;)V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 16
    .line 17
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    const/4 v4, 0x0

    .line 26
    const/4 v5, 0x1

    .line 27
    const-string v6, "BatchOcrResultFragment"

    .line 28
    .line 29
    if-eqz p1, :cond_2

    .line 30
    .line 31
    if-eq p1, v5, :cond_1

    .line 32
    .line 33
    const/4 v0, 0x2

    .line 34
    if-eq p1, v0, :cond_0

    .line 35
    .line 36
    goto/16 :goto_0

    .line 37
    .line 38
    :cond_0
    const-string p1, "MotionEvent.ACTION_MOVE"

    .line 39
    .line 40
    invoke-static {v6, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇o〇:Z

    .line 44
    .line 45
    if-eqz p1, :cond_4

    .line 46
    .line 47
    iget p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo80:F

    .line 48
    .line 49
    sub-float/2addr p1, v1

    .line 50
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00O0:I

    .line 55
    .line 56
    int-to-float v0, v0

    .line 57
    cmpl-float p1, p1, v0

    .line 58
    .line 59
    if-lez p1, :cond_4

    .line 60
    .line 61
    iget p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo80:F

    .line 62
    .line 63
    sub-float v3, v1, p1

    .line 64
    .line 65
    iput v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇o88o08〇:F

    .line 66
    .line 67
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo80:F

    .line 68
    .line 69
    const/4 v4, 0x0

    .line 70
    const/4 v5, 0x0

    .line 71
    const/4 v6, 0x6

    .line 72
    const/4 v7, 0x0

    .line 73
    move-object v2, p0

    .line 74
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0oO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;FZZILjava/lang/Object;)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇o〇:Z

    .line 79
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    .line 81
    .line 82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    .line 84
    .line 85
    const-string v1, "ACTION_UP -isCanDrag\uff1a"

    .line 86
    .line 87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    invoke-static {v6, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇o〇:Z

    .line 101
    .line 102
    if-eqz p1, :cond_4

    .line 103
    .line 104
    const/4 p1, 0x0

    .line 105
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇o88o08〇:F

    .line 106
    .line 107
    iput-boolean v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇o〇:Z

    .line 108
    .line 109
    goto :goto_0

    .line 110
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    .line 111
    .line 112
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    .line 114
    .line 115
    const-string v7, "ACTION_DOWN (x,y)\uff1a("

    .line 116
    .line 117
    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    const-string v2, ","

    .line 124
    .line 125
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    const-string v2, ")"

    .line 132
    .line 133
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    invoke-static {v6, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇O〇〇O8:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 144
    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    .line 146
    .line 147
    .line 148
    move-result p1

    .line 149
    int-to-float v0, p1

    .line 150
    cmpl-float v0, v1, v0

    .line 151
    .line 152
    if-ltz v0, :cond_3

    .line 153
    .line 154
    add-int/2addr p1, v3

    .line 155
    int-to-float p1, p1

    .line 156
    cmpg-float p1, v1, p1

    .line 157
    .line 158
    if-gtz p1, :cond_3

    .line 159
    .line 160
    iput-boolean v5, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇o〇:Z

    .line 161
    .line 162
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo80:F

    .line 163
    .line 164
    goto :goto_0

    .line 165
    :cond_3
    iput-boolean v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇o〇:Z

    .line 166
    .line 167
    :cond_4
    :goto_0
    return-void
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic O0O0〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8〇8o00(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final O0o0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic O0oO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;FZZILjava/lang/Object;)V
    .locals 1

    .line 1
    and-int/lit8 p5, p4, 0x2

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p5, :cond_0

    .line 5
    .line 6
    const/4 p2, 0x0

    .line 7
    :cond_0
    and-int/lit8 p4, p4, 0x4

    .line 8
    .line 9
    if-eqz p4, :cond_1

    .line 10
    .line 11
    const/4 p3, 0x0

    .line 12
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO8o〇08〇(FZZ)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static final synthetic O0〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o0oOO8()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇0o8o8〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic O0〇8〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;I[FIILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p4, p4, 0x4

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const/4 p3, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇80oO〇(I[FI)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static final synthetic O80OO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8o80O(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O88(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08〇o0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O880O〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇O800oo(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O888Oo()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 4
    .line 5
    iget-wide v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 6
    .line 7
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O8O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/List;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo0O〇8800(Ljava/util/List;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final O8〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iget p2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 14
    .line 15
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->ooo8o〇o〇(I)V

    .line 16
    .line 17
    .line 18
    const-string p1, "CSOcrGiveUpPop"

    .line 19
    .line 20
    const-string p2, "give_up"

    .line 21
    .line 22
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 p1, 0x0

    .line 26
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08〇o0O:Z

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O8〇8〇O80(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇O8〇〇o8〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO00〇0o〇〇()V
    .locals 1

    .line 1
    const-string v0, "complete"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8o80O(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic OO0O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Z)Ljava/util/List;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇000(Z)Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OO0o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O08o(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO0〇O(Ljava/lang/String;Landroid/util/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo8〇〇()Lorg/json/JSONObject;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_2

    .line 13
    .line 14
    if-eqz p2, :cond_1

    .line 15
    .line 16
    :try_start_0
    iget-object v1, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 17
    .line 18
    check-cast v1, Ljava/lang/String;

    .line 19
    .line 20
    iget-object p2, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 21
    .line 22
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :catch_0
    move-exception p2

    .line 27
    const-string v1, "BatchOcrResultFragment"

    .line 28
    .line 29
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 30
    .line 31
    .line 32
    :cond_1
    :goto_0
    const-string p2, "CSOcrResult"

    .line 33
    .line 34
    invoke-static {p2, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 35
    .line 36
    .line 37
    :cond_2
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final OO8〇O8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "BatchOcrResultFragment"

    .line 7
    .line 8
    const-string p2, "User Operation: go to ocr language setting"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    const/4 p1, 0x1

    .line 16
    const/16 p2, 0x68

    .line 17
    .line 18
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/ocrapi/OcrIntent;->O8(Landroid/app/Activity;II)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final OOo00(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO〇000(Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .line 16
    .line 17
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-ge v0, v1, :cond_1

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const-string v1, "mViewModel.inputOcrDataList[mCurPage]"

    .line 48
    .line 49
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    :cond_1
    :goto_0
    return-object p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final OO〇80oO〇(I[FI)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "setSelectOcrFrame "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "BatchOcrResultFragment"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇oO:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 30
    .line 31
    new-instance v2, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;

    .line 51
    .line 52
    if-eqz p1, :cond_0

    .line 53
    .line 54
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇8([FI)V

    .line 55
    .line 56
    .line 57
    :cond_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic OO〇〇o0oO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;ZI)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8ooOO(ZI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final Oo0O〇8800(Ljava/util/List;I)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;I)V"
        }
    .end annotation

    .line 1
    const-string v0, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v1, "reStartOcr"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v4, Ljava/util/ArrayList;

    .line 9
    .line 10
    check-cast p1, Ljava/util/Collection;

    .line 11
    .line 12
    invoke-direct {v4, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 13
    .line 14
    .line 15
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo〇(Z)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->OOO〇O0(Z)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O88O:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 40
    .line 41
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_CLOUD_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 42
    .line 43
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->〇8(Lcom/intsig/camscanner/purchase/entity/Function;)V

    .line 44
    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O88O:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 47
    .line 48
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_OCR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 49
    .line 50
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->O08000(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 51
    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O88O:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 54
    .line 55
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 56
    .line 57
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O〇O00O:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$ocrProgressListener$1;

    .line 58
    .line 59
    const/4 v6, 0x0

    .line 60
    const-string v8, "paragraph"

    .line 61
    .line 62
    const/4 v9, 0x0

    .line 63
    const-string v10, ""

    .line 64
    .line 65
    move v7, p2

    .line 66
    invoke-virtual/range {v2 .. v10}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->oo〇(Landroid/app/Activity;Ljava/util/List;Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;ILjava/lang/String;Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic OoO〇OOo8o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/lang/String;Landroid/util/Pair;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO0〇O(Ljava/lang/String;Landroid/util/Pair;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic Ooo8o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8O〇008(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OooO〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/lang/String;JLjava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo0oO0(Ljava/lang/String;JLjava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final Oo〇〇〇〇()V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/nativelib/OcrLanguage$LangMode;->OCR:Lcom/intsig/nativelib/OcrLanguage$LangMode;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/ocrapi/OcrLanguagesCompat;->〇o00〇〇Oo(Lcom/intsig/nativelib/OcrLanguage$LangMode;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v0, "languageString"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-string v0, ","

    .line 13
    .line 14
    filled-new-array {v0}, [Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    const/4 v3, 0x0

    .line 19
    const/4 v4, 0x0

    .line 20
    const/4 v5, 0x6

    .line 21
    const/4 v6, 0x0

    .line 22
    invoke-static/range {v1 .. v6}, Lkotlin/text/StringsKt;->Oo(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    check-cast v0, Ljava/util/Collection;

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    new-array v2, v1, [Ljava/lang/String;

    .line 30
    .line 31
    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, [Ljava/lang/String;

    .line 36
    .line 37
    new-instance v2, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    array-length v3, v0

    .line 43
    const/4 v4, 0x0

    .line 44
    :goto_0
    if-ge v4, v3, :cond_5

    .line 45
    .line 46
    aget-object v5, v0, v4

    .line 47
    .line 48
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    .line 49
    .line 50
    .line 51
    move-result v6

    .line 52
    if-lez v6, :cond_0

    .line 53
    .line 54
    const/4 v6, 0x1

    .line 55
    goto :goto_1

    .line 56
    :cond_0
    const/4 v6, 0x0

    .line 57
    :goto_1
    if-eqz v6, :cond_1

    .line 58
    .line 59
    const-string v6, "\u3001"

    .line 60
    .line 61
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 65
    .line 66
    .line 67
    move-result v6

    .line 68
    if-eqz v6, :cond_2

    .line 69
    .line 70
    goto :goto_3

    .line 71
    :cond_2
    const-string v6, "zh"

    .line 72
    .line 73
    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    move-result v6

    .line 77
    if-eqz v6, :cond_3

    .line 78
    .line 79
    const-string v5, "zh-s"

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_3
    const-string v6, "zht"

    .line 83
    .line 84
    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    move-result v6

    .line 88
    if-eqz v6, :cond_4

    .line 89
    .line 90
    const-string v5, "zh-t"

    .line 91
    .line 92
    :cond_4
    :goto_2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    :goto_3
    add-int/lit8 v4, v4, 0x1

    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    if-eqz v0, :cond_6

    .line 103
    .line 104
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O〇o88o08〇:Landroid/widget/TextView;

    .line 105
    .line 106
    goto :goto_4

    .line 107
    :cond_6
    const/4 v0, 0x0

    .line 108
    :goto_4
    if-nez v0, :cond_7

    .line 109
    .line 110
    goto :goto_5

    .line 111
    :cond_7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    .line 117
    .line 118
    :goto_5
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final O〇00O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇00o08()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O〇8oOo8O()Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v1, Lcom/intsig/camscanner/mode_ocr/PreferenceOcrHelper;->〇080:Lcom/intsig/camscanner/mode_ocr/PreferenceOcrHelper;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/PreferenceOcrHelper;->〇080()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇oOO8O8()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/view/GuideToOcrResultFragment;

    .line 26
    .line 27
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/view/GuideToOcrResultFragment;-><init>()V

    .line 28
    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    const-string v2, "BatchOcrResultFragment"

    .line 37
    .line 38
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    :cond_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O〇080〇o0(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/mode_ocr/PageFromType;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oOOo:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final O〇0o8o8〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O〇0o8〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8o0o0(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final O〇8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O:Landroid/widget/ImageView;

    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8〇o(Landroid/widget/ImageView;Landroid/view/MotionEvent;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    const/4 p0, 0x0

    .line 20
    return p0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O〇88(Landroid/view/View;)V
    .locals 8

    .line 1
    const-string v0, "showUpgradedOcrTips() "

    .line 2
    .line 3
    const-string v1, "BatchOcrResultFragment"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8o8〇o()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o〇Oo88:Landroid/app/Dialog;

    .line 16
    .line 17
    if-nez v0, :cond_2

    .line 18
    .line 19
    new-instance v0, Landroid/app/Dialog;

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 22
    .line 23
    const v3, 0x7f1401d6

    .line 24
    .line 25
    .line 26
    invoke-direct {v0, v2, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 27
    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o〇Oo88:Landroid/app/Dialog;

    .line 30
    .line 31
    new-instance v2, L〇0O〇O00O/o〇〇0〇;

    .line 32
    .line 33
    invoke-direct {v2}, L〇0O〇O00O/o〇〇0〇;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 40
    .line 41
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const v2, 0x7f0d0760

    .line 46
    .line 47
    .line 48
    const/4 v3, 0x0

    .line 49
    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    const v2, 0x7f0a1867

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    const-string v3, "rootView.findViewById(R.id.tv_tips)"

    .line 61
    .line 62
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    check-cast v2, Lcom/intsig/comm/widget/CustomTextView;

    .line 66
    .line 67
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    new-instance v4, L〇0O〇O00O/OOO〇O0;

    .line 72
    .line 73
    invoke-direct {v4, p1, v0, v2}, L〇0O〇O00O/OOO〇O0;-><init>(Landroid/view/View;Landroid/view/View;Lcom/intsig/comm/widget/CustomTextView;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 77
    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o〇Oo88:Landroid/app/Dialog;

    .line 80
    .line 81
    if-eqz p1, :cond_1

    .line 82
    .line 83
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 84
    .line 85
    .line 86
    :cond_1
    new-instance p1, L〇0O〇O00O/〇o00〇〇Oo;

    .line 87
    .line 88
    invoke-direct {p1, p0}, L〇0O〇O00O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    .line 93
    .line 94
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o〇Oo88:Landroid/app/Dialog;

    .line 95
    .line 96
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    .line 100
    .line 101
    .line 102
    move-result p1

    .line 103
    if-nez p1, :cond_4

    .line 104
    .line 105
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o〇Oo88:Landroid/app/Dialog;

    .line 106
    .line 107
    if-eqz p1, :cond_3

    .line 108
    .line 109
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 110
    .line 111
    .line 112
    :cond_3
    new-instance v2, Ljava/util/Timer;

    .line 113
    .line 114
    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    .line 115
    .line 116
    .line 117
    new-instance v3, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$showUpgradedOcrTips$4;

    .line 118
    .line 119
    invoke-direct {v3, p0, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$showUpgradedOcrTips$4;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/Timer;)V

    .line 120
    .line 121
    .line 122
    const-wide/16 v4, 0x0

    .line 123
    .line 124
    const-wide/16 v6, 0x3e8

    .line 125
    .line 126
    invoke-virtual/range {v2 .. v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .line 128
    .line 129
    goto :goto_0

    .line 130
    :catch_0
    move-exception p1

    .line 131
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    .line 133
    .line 134
    :cond_4
    :goto_0
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final O〇8O0O80〇()V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇00O0:Landroid/view/ViewStub;

    .line 8
    .line 9
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const v2, 0x7f0a12f9

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Landroid/widget/TextView;

    .line 21
    .line 22
    const/16 v2, 0xc

    .line 23
    .line 24
    new-array v2, v2, [Landroid/view/View;

    .line 25
    .line 26
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->OO:Landroid/widget/Button;

    .line 27
    .line 28
    const/4 v4, 0x0

    .line 29
    aput-object v3, v2, v4

    .line 30
    .line 31
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O〇o88o08〇:Landroid/widget/TextView;

    .line 32
    .line 33
    const/4 v5, 0x1

    .line 34
    aput-object v3, v2, v5

    .line 35
    .line 36
    const/4 v3, 0x2

    .line 37
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇8〇oO〇〇8o:Lcom/intsig/view/ImageTextButton;

    .line 38
    .line 39
    aput-object v6, v2, v3

    .line 40
    .line 41
    const/4 v3, 0x3

    .line 42
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->ooo0〇〇O:Lcom/intsig/view/ImageTextButton;

    .line 43
    .line 44
    aput-object v6, v2, v3

    .line 45
    .line 46
    const/4 v3, 0x4

    .line 47
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇08O:Lcom/intsig/view/ImageTextButton;

    .line 48
    .line 49
    aput-object v6, v2, v3

    .line 50
    .line 51
    const/4 v3, 0x5

    .line 52
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 53
    .line 54
    aput-object v6, v2, v3

    .line 55
    .line 56
    const/4 v3, 0x6

    .line 57
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 58
    .line 59
    aput-object v6, v2, v3

    .line 60
    .line 61
    const/4 v3, 0x7

    .line 62
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o8〇OO0〇0o:Landroid/widget/Button;

    .line 63
    .line 64
    aput-object v6, v2, v3

    .line 65
    .line 66
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->oOo0:Lcom/intsig/camscanner/databinding/IncludeEditKeyboardBtnBinding;

    .line 67
    .line 68
    iget-object v6, v3, Lcom/intsig/camscanner/databinding/IncludeEditKeyboardBtnBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 69
    .line 70
    const/16 v7, 0x8

    .line 71
    .line 72
    aput-object v6, v2, v7

    .line 73
    .line 74
    const/16 v6, 0x9

    .line 75
    .line 76
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/IncludeEditKeyboardBtnBinding;->OO:Landroid/widget/TextView;

    .line 77
    .line 78
    aput-object v3, v2, v6

    .line 79
    .line 80
    const/16 v3, 0xa

    .line 81
    .line 82
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 83
    .line 84
    aput-object v6, v2, v3

    .line 85
    .line 86
    const/16 v3, 0xb

    .line 87
    .line 88
    aput-object v1, v2, v3

    .line 89
    .line 90
    invoke-virtual {p0, v2}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 91
    .line 92
    .line 93
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 94
    .line 95
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$initClickAndListener$1$1;

    .line 96
    .line 97
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$initClickAndListener$1$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;)V

    .line 98
    .line 99
    .line 100
    invoke-static {v1, v2}, Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener;->〇o〇(Landroid/app/Activity;Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;)V

    .line 101
    .line 102
    .line 103
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 104
    .line 105
    invoke-virtual {v1, v5}, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;->setShowLineDivider(Z)V

    .line 106
    .line 107
    .line 108
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 109
    .line 110
    invoke-static {}, Lcom/intsig/util/WordFilter;->〇o〇()[Landroid/text/InputFilter;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 115
    .line 116
    .line 117
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 118
    .line 119
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo0〇Ooo:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mClipListener$1;

    .line 120
    .line 121
    invoke-virtual {v1, v2}, Lcom/intsig/view/EditViewMultiLine;->setTVClipboardListener(Lcom/intsig/comm/logagent/ocr/TVClipboardListener;)V

    .line 122
    .line 123
    .line 124
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 125
    .line 126
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;

    .line 127
    .line 128
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 129
    .line 130
    .line 131
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 132
    .line 133
    new-instance v2, L〇0O〇O00O/〇O00;

    .line 134
    .line 135
    invoke-direct {v2}, L〇0O〇O00O/〇O00;-><init>()V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 139
    .line 140
    .line 141
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 142
    .line 143
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 144
    .line 145
    .line 146
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 147
    .line 148
    new-instance v2, L〇0O〇O00O/〇〇8O0〇8;

    .line 149
    .line 150
    invoke-direct {v2, p0, v0}, L〇0O〇O00O/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v1, v2}, Lcom/intsig/view/EditViewMultiLine;->setSelectionCallBack(Lcom/intsig/view/EditViewMultiLine$SelectionCallBack;)V

    .line 154
    .line 155
    .line 156
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 157
    .line 158
    const-string v2, "mActivity"

    .line 159
    .line 160
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    invoke-static {v1, v5}, Lcom/intsig/utils/ext/ContextExtKt;->〇o〇(Landroid/content/Context;I)I

    .line 164
    .line 165
    .line 166
    move-result v1

    .line 167
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00O0:I

    .line 168
    .line 169
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 170
    .line 171
    new-instance v1, L〇0O〇O00O/〇0〇O0088o;

    .line 172
    .line 173
    invoke-direct {v1, p0}, L〇0O〇O00O/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 174
    .line 175
    .line 176
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 177
    .line 178
    .line 179
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O:Landroid/widget/ImageView;

    .line 180
    .line 181
    if-eqz v0, :cond_0

    .line 182
    .line 183
    new-instance v1, L〇0O〇O00O/OoO8;

    .line 184
    .line 185
    invoke-direct {v1, p0}, L〇0O〇O00O/OoO8;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 186
    .line 187
    .line 188
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 189
    .line 190
    .line 191
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 192
    .line 193
    if-eqz v0, :cond_1

    .line 194
    .line 195
    new-instance v1, L〇0O〇O00O/o800o8O;

    .line 196
    .line 197
    invoke-direct {v1, p0}, L〇0O〇O00O/o800o8O;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 198
    .line 199
    .line 200
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 201
    .line 202
    .line 203
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O:Landroid/widget/ImageView;

    .line 204
    .line 205
    if-eqz v0, :cond_2

    .line 206
    .line 207
    new-instance v1, L〇0O〇O00O/〇O888o0o;

    .line 208
    .line 209
    invoke-direct {v1, p0}, L〇0O〇O00O/〇O888o0o;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 213
    .line 214
    .line 215
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 216
    .line 217
    if-eqz v0, :cond_3

    .line 218
    .line 219
    new-instance v1, L〇0O〇O00O/oo88o8O;

    .line 220
    .line 221
    invoke-direct {v1, p0}, L〇0O〇O00O/oo88o8O;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 225
    .line 226
    .line 227
    :cond_3
    return-void
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static final synthetic O〇8〇008(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇O800oo(Z)V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 17
    .line 18
    .line 19
    const-string p1, "translate_all"

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O〇8oOo8O()Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    if-eqz p1, :cond_1

    .line 31
    .line 32
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    :cond_1
    const-string p1, "translate_current"

    .line 36
    .line 37
    :goto_0
    const-string v1, "CSOcrMore"

    .line 38
    .line 39
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    sget-object p1, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->〇080:Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;

    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 45
    .line 46
    const-string v2, "mActivity"

    .line 47
    .line 48
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->〇〇888(Landroidx/fragment/app/FragmentActivity;Ljava/util/List;)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O〇o8()V
    .locals 5

    .line 1
    const-string v0, "save"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8o80O(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8〇OO0〇0o:Z

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇8o0OOOo()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO00〇0o〇〇()V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 18
    .line 19
    iget-wide v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 20
    .line 21
    const-wide/16 v2, 0x0

    .line 22
    .line 23
    cmp-long v4, v0, v2

    .line 24
    .line 25
    if-lez v4, :cond_1

    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0〇O80ooo(Ljava/util/List;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO00〇0o〇〇()V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const/4 v1, 0x2

    .line 51
    const/4 v2, 0x0

    .line 52
    const/4 v3, 0x0

    .line 53
    invoke-static {p0, v0, v3, v1, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/List;ZILjava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O〇oo8O80(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)Z
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "BatchOcrResultFragment"

    .line 7
    .line 8
    const-string v0, "ivNext long click"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    const/4 p1, 0x1

    .line 18
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o08〇〇0O(Z)V

    .line 19
    .line 20
    .line 21
    return p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇〇O()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o8〇OO0〇0o:Landroid/widget/Button;

    .line 8
    .line 9
    new-instance v2, L〇0O〇O00O/OO0o〇〇;

    .line 10
    .line 11
    invoke-direct {v2, p0, v0}, L〇0O〇O00O/OO0o〇〇;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O〇〇O80o8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O〇〇o8O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o0OoOOo0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o00o0O〇〇o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->oo0O〇0〇〇〇()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-le v0, v1, :cond_0

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;

    .line 13
    .line 14
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;-><init>()V

    .line 15
    .line 16
    .line 17
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$translationDeal$1;

    .line 18
    .line 19
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$translationDeal$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;->〇80O8o8O〇(Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog$ChoseOperationListener;)Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 27
    .line 28
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const-string v2, "BatchOcrResultFragment"

    .line 33
    .line 34
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->〇080:Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->oO80()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    const/4 v2, 0x0

    .line 45
    if-eqz v0, :cond_1

    .line 46
    .line 47
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇O800oo(Z)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOO〇〇:Lcom/intsig/camscanner/tools/TranslateClient;

    .line 52
    .line 53
    if-eqz v0, :cond_2

    .line 54
    .line 55
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/tools/TranslateClient;->OOO〇O0(ZZ)V

    .line 56
    .line 57
    .line 58
    :cond_2
    :goto_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o00〇88〇08(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0o0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o088O8800(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0o〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o0OO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇oO08〇o0(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o0Oo(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o0〇OO008O()V
    .locals 3

    .line 1
    const-string v0, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v1, "showUpdateDataTips"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇〇00O〇0o()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o0oOO8()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    const-string v2, "mActivity"

    .line 25
    .line 26
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;->oO(Landroid/app/Activity;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o0oOO8()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;->oo88o8O()V

    .line 38
    .line 39
    .line 40
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic o0〇〇00(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o808o8o08(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o88(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8〇〇8o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o880(Landroid/view/View;Landroid/view/View;Lcom/intsig/comm/widget/CustomTextView;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇O〇〇〇(Landroid/view/View;Landroid/view/View;Lcom/intsig/comm/widget/CustomTextView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final o88o88(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o88oo〇O()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const v1, 0x7f130b3d

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 11
    .line 12
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/Util;->oo〇(Ljava/lang/String;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    const/4 v2, 0x1

    .line 17
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/util/Util;->OOO(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const-string v1, "getPreferName(mActivity,\u2026Util.BRACKET_SUFFIXSTYLE)"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final o8O〇008(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o8o0()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tools/OcrTextShareClient;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$initOcrTextShareClient$1;

    .line 6
    .line 7
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$initOcrTextShareClient$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 8
    .line 9
    .line 10
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/tools/OcrTextShareClient;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/tools/OcrTextShareClient$OcrTextShareClientCallback;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8o:Lcom/intsig/camscanner/tools/OcrTextShareClient;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o8o0o8()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->oo0O〇0〇〇〇()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-le v0, v1, :cond_0

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;

    .line 13
    .line 14
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;-><init>()V

    .line 15
    .line 16
    .line 17
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$copyTxt$1;

    .line 18
    .line 19
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$copyTxt$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;->〇80O8o8O〇(Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog$ChoseOperationListener;)Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 27
    .line 28
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const-string v2, "BatchOcrResultFragment"

    .line 33
    .line 34
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const/4 v1, 0x0

    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇008〇oo(Z)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    const v3, 0x7f131bcf

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    invoke-static {v2, v0, v3}, Lcom/intsig/camscanner/app/AppUtil;->〇O〇(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/String;)I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    const-string v2, "copy"

    .line 61
    .line 62
    invoke-direct {p0, v2, v0, v1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0〇8o〇(Ljava/lang/String;IZ)V

    .line 63
    .line 64
    .line 65
    :goto_0
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method private final varargs o8oo0OOO(Z[Landroid/view/View;)V
    .locals 3

    .line 1
    array-length v0, p2

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    if-ge v1, v0, :cond_1

    .line 4
    .line 5
    aget-object v2, p2, v1

    .line 6
    .line 7
    if-eqz v2, :cond_0

    .line 8
    .line 9
    invoke-static {v2, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 10
    .line 11
    .line 12
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o8〇O〇0O0〇(Ljava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o88oo〇O()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 6
    .line 7
    iput-object v0, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 8
    .line 9
    new-instance v1, Lcom/intsig/utils/CommonLoadingTask;

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    new-instance v3, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$startSaveNewOcrDoc$1;

    .line 14
    .line 15
    invoke-direct {v3, p2, p0, p1, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$startSaveNewOcrDoc$1;-><init>(ZLcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/List;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    const p2, 0x7f1300a3

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-direct {v1, v2, v3, p1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 31
    .line 32
    .line 33
    return-void
.end method

.method private static final oO8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    const/4 p2, 0x0

    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-static {p0, v0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇80〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;ZILjava/lang/Object;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oO88〇0O8O(J)V
    .locals 3

    .line 1
    const-string v0, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v1, "localOriginPdf2Office"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$localOriginPdf2Office$1;

    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-direct {v1, p0, p1, p2, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$localOriginPdf2Office$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;JLkotlin/coroutines/Continuation;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final oO8o〇08〇(FZZ)V
    .locals 9

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "resizeImgEditLayout  -> moveDistance:"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, " ;isDefault:"

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "BatchOcrResultFragment"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_9

    .line 36
    .line 37
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->oOo〇8o008:Landroid/widget/FrameLayout;

    .line 38
    .line 39
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    const-string v3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    .line 44
    .line 45
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    check-cast v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 49
    .line 50
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇O〇〇O8:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 51
    .line 52
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    check-cast v4, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 60
    .line 61
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->oOo〇8o008:Landroid/widget/FrameLayout;

    .line 62
    .line 63
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    .line 64
    .line 65
    .line 66
    move-result v3

    .line 67
    iget-object v5, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇O〇〇O8:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 68
    .line 69
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    .line 70
    .line 71
    .line 72
    move-result v5

    .line 73
    iget-object v6, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 74
    .line 75
    const-string v7, "mActivity"

    .line 76
    .line 77
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    const/16 v7, 0x64

    .line 81
    .line 82
    invoke-static {v6, v7}, Lcom/intsig/utils/ext/ContextExtKt;->〇o〇(Landroid/content/Context;I)I

    .line 83
    .line 84
    .line 85
    move-result v6

    .line 86
    new-instance v7, Ljava/lang/StringBuilder;

    .line 87
    .line 88
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    .line 90
    .line 91
    const-string v8, " imgRootHeight:"

    .line 92
    .line 93
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    const-string v8, ",editRootHeight:"

    .line 100
    .line 101
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    const-string v8, " "

    .line 108
    .line 109
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v7

    .line 116
    invoke-static {v1, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 120
    .line 121
    if-nez v1, :cond_0

    .line 122
    .line 123
    return-void

    .line 124
    :cond_0
    const/high16 v7, 0x3f800000    # 1.0f

    .line 125
    .line 126
    const/4 v8, 0x0

    .line 127
    if-eqz p3, :cond_1

    .line 128
    .line 129
    iput v8, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 130
    .line 131
    iput v7, v4, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->verticalWeight:F

    .line 132
    .line 133
    iput v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 134
    .line 135
    goto :goto_2

    .line 136
    :cond_1
    if-eqz p2, :cond_3

    .line 137
    .line 138
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oOOo:Z

    .line 139
    .line 140
    if-eqz p1, :cond_2

    .line 141
    .line 142
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    .line 143
    .line 144
    .line 145
    move-result p1

    .line 146
    iget-object p2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇0O:Landroid/widget/FrameLayout;

    .line 147
    .line 148
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    .line 149
    .line 150
    .line 151
    move-result p2

    .line 152
    sub-int/2addr p1, p2

    .line 153
    div-int/lit8 p1, p1, 0x2

    .line 154
    .line 155
    iget-object p2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇0O:Landroid/widget/FrameLayout;

    .line 156
    .line 157
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    .line 158
    .line 159
    .line 160
    move-result p2

    .line 161
    add-int/2addr p1, p2

    .line 162
    iput p1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 163
    .line 164
    goto :goto_0

    .line 165
    :cond_2
    iput v8, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 166
    .line 167
    iput v7, v4, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->verticalWeight:F

    .line 168
    .line 169
    :goto_0
    iput v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 170
    .line 171
    iput v7, v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->verticalWeight:F

    .line 172
    .line 173
    goto :goto_2

    .line 174
    :cond_3
    iget-boolean p2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oOOo:Z

    .line 175
    .line 176
    if-eqz p2, :cond_4

    .line 177
    .line 178
    iget-object p2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇0O:Landroid/widget/FrameLayout;

    .line 179
    .line 180
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    .line 181
    .line 182
    .line 183
    move-result p2

    .line 184
    sub-int p2, v5, p2

    .line 185
    .line 186
    goto :goto_1

    .line 187
    :cond_4
    move p2, v5

    .line 188
    :goto_1
    const/4 p3, 0x0

    .line 189
    cmpl-float v1, p1, p3

    .line 190
    .line 191
    if-lez v1, :cond_6

    .line 192
    .line 193
    int-to-float p3, p2

    .line 194
    sub-float/2addr p3, p1

    .line 195
    int-to-float v1, v6

    .line 196
    cmpl-float p3, p3, v1

    .line 197
    .line 198
    if-lez p3, :cond_5

    .line 199
    .line 200
    if-le p2, v6, :cond_5

    .line 201
    .line 202
    int-to-float p2, v5

    .line 203
    sub-float/2addr p2, p1

    .line 204
    float-to-int p1, p2

    .line 205
    iput p1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 206
    .line 207
    iput v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 208
    .line 209
    iput v7, v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->verticalWeight:F

    .line 210
    .line 211
    goto :goto_2

    .line 212
    :cond_5
    return-void

    .line 213
    :cond_6
    cmpg-float p2, p1, p3

    .line 214
    .line 215
    if-gez p2, :cond_8

    .line 216
    .line 217
    int-to-float p2, v3

    .line 218
    add-float/2addr p2, p1

    .line 219
    int-to-float p1, v6

    .line 220
    cmpl-float p1, p2, p1

    .line 221
    .line 222
    if-lez p1, :cond_7

    .line 223
    .line 224
    if-le v3, v6, :cond_7

    .line 225
    .line 226
    float-to-int p1, p2

    .line 227
    iput p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 228
    .line 229
    iput v8, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 230
    .line 231
    iput v7, v4, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->verticalWeight:F

    .line 232
    .line 233
    goto :goto_2

    .line 234
    :cond_7
    return-void

    .line 235
    :cond_8
    :goto_2
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->oOo〇8o008:Landroid/widget/FrameLayout;

    .line 236
    .line 237
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    .line 239
    .line 240
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇O〇〇O8:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 241
    .line 242
    invoke-virtual {p1, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    .line 244
    .line 245
    :cond_9
    return-void
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static final synthetic oOO8oo0(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇00o08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final oOOO0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p0, "CSOcrResultBackPop"

    .line 2
    .line 3
    const-string p1, "cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo〇O0o〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oO〇O0O()Z
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O888Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->〇oo〇()J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    const-wide/16 v2, 0x0

    .line 16
    .line 17
    const/4 v4, 0x1

    .line 18
    cmp-long v5, v0, v2

    .line 19
    .line 20
    if-lez v5, :cond_0

    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->〇oo〇()J

    .line 27
    .line 28
    .line 29
    move-result-wide v0

    .line 30
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/db/dao/ImageDao;->o8oO〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 37
    .line 38
    invoke-static {v3, v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    invoke-direct {p0, v3, v0, v1, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo0oO0(Ljava/lang/String;JLjava/util/ArrayList;)V

    .line 43
    .line 44
    .line 45
    return v4

    .line 46
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-direct {p0, v0, v4}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8〇O〇0O0〇(Ljava/util/List;Z)V

    .line 55
    .line 56
    .line 57
    return v4

    .line 58
    :cond_1
    const/4 v0, 0x0

    .line 59
    return v0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O008o8oo(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oo0O(IZ)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "setSelectAll "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "BatchOcrResultFragment"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇oO:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 30
    .line 31
    new-instance v2, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;

    .line 51
    .line 52
    if-eqz p1, :cond_0

    .line 53
    .line 54
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->oo〇(Z)V

    .line 55
    .line 56
    .line 57
    :cond_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final oo8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)Z
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "BatchOcrResultFragment"

    .line 7
    .line 8
    const-string v0, "ivPrevious long click"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    const/4 p1, 0x0

    .line 18
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o08〇〇0O(Z)V

    .line 19
    .line 20
    .line 21
    const/4 p0, 0x1

    .line 22
    return p0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final oo88(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O:Landroid/widget/ImageView;

    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 13
    .line 14
    .line 15
    move-result-object p0

    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8〇o(Landroid/widget/ImageView;Landroid/view/MotionEvent;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    const/4 p0, 0x0

    .line 20
    return p0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8〇0O〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oooO8〇00()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const-string v1, " "

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const v1, 0x7f0d04ff

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 26
    .line 27
    const v1, 0x7f0a0c76

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    check-cast v1, Landroid/widget/LinearLayout;

    .line 35
    .line 36
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇00O:Landroid/widget/LinearLayout;

    .line 37
    .line 38
    const v1, 0x7f0a09b2

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    check-cast v1, Landroid/widget/ImageView;

    .line 46
    .line 47
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 48
    .line 49
    const v1, 0x7f0a09b1

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    check-cast v1, Landroid/widget/ImageView;

    .line 57
    .line 58
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O:Landroid/widget/ImageView;

    .line 59
    .line 60
    const v1, 0x7f0a162a

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    check-cast v1, Landroid/widget/TextView;

    .line 68
    .line 69
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 70
    .line 71
    const v1, 0x7f0a1427

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    check-cast v1, Landroid/widget/TextView;

    .line 79
    .line 80
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 81
    .line 82
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 83
    .line 84
    if-eqz v1, :cond_0

    .line 85
    .line 86
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    sget-object v3, Lcom/intsig/base/ToolbarThemeGet;->〇080:Lcom/intsig/base/ToolbarThemeGet;

    .line 91
    .line 92
    invoke-virtual {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->getToolbarTheme()I

    .line 93
    .line 94
    .line 95
    move-result v4

    .line 96
    invoke-virtual {v3, v4}, Lcom/intsig/base/ToolbarThemeGet;->O8(I)I

    .line 97
    .line 98
    .line 99
    move-result v3

    .line 100
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    .line 101
    .line 102
    .line 103
    move-result v2

    .line 104
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 105
    .line 106
    .line 107
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 108
    .line 109
    if-eqz v1, :cond_1

    .line 110
    .line 111
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 112
    .line 113
    .line 114
    move-result-object v2

    .line 115
    sget-object v3, Lcom/intsig/base/ToolbarThemeGet;->〇080:Lcom/intsig/base/ToolbarThemeGet;

    .line 116
    .line 117
    invoke-virtual {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->getToolbarTheme()I

    .line 118
    .line 119
    .line 120
    move-result v4

    .line 121
    invoke-virtual {v3, v4}, Lcom/intsig/base/ToolbarThemeGet;->O8(I)I

    .line 122
    .line 123
    .line 124
    move-result v3

    .line 125
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    .line 131
    .line 132
    :cond_1
    const/4 v1, 0x3

    .line 133
    new-array v1, v1, [Landroid/view/View;

    .line 134
    .line 135
    const/4 v2, 0x0

    .line 136
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 137
    .line 138
    aput-object v3, v1, v2

    .line 139
    .line 140
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O:Landroid/widget/ImageView;

    .line 141
    .line 142
    const/4 v3, 0x1

    .line 143
    aput-object v2, v1, v3

    .line 144
    .line 145
    const/4 v2, 0x2

    .line 146
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 147
    .line 148
    aput-object v4, v1, v2

    .line 149
    .line 150
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 151
    .line 152
    .line 153
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarMenu:Landroid/widget/FrameLayout;

    .line 154
    .line 155
    if-eqz v1, :cond_2

    .line 156
    .line 157
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 158
    .line 159
    const/4 v2, -0x1

    .line 160
    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 161
    .line 162
    .line 163
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 164
    .line 165
    const/4 v5, -0x2

    .line 166
    invoke-direct {v4, v2, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 167
    .line 168
    .line 169
    const/16 v2, 0x11

    .line 170
    .line 171
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 172
    .line 173
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarMenu:Landroid/widget/FrameLayout;

    .line 174
    .line 175
    invoke-virtual {v2, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    .line 177
    .line 178
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    .line 180
    .line 181
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarMenu:Landroid/widget/FrameLayout;

    .line 182
    .line 183
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 184
    .line 185
    .line 186
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarMenu:Landroid/widget/FrameLayout;

    .line 187
    .line 188
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 189
    .line 190
    .line 191
    :cond_2
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbar:Landroidx/appcompat/widget/Toolbar;

    .line 192
    .line 193
    if-eqz v0, :cond_3

    .line 194
    .line 195
    invoke-static {v0, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 196
    .line 197
    .line 198
    :cond_3
    return-void
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final oooo800〇〇(Z)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 13
    .line 14
    .line 15
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 18
    .line 19
    const v1, 0x7f0601ee

    .line 20
    .line 21
    .line 22
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 31
    .line 32
    const/4 v1, 0x0

    .line 33
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 34
    .line 35
    .line 36
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 39
    .line 40
    const v1, 0x7f060207

    .line 41
    .line 42
    .line 43
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 48
    .line 49
    .line 50
    :cond_1
    :goto_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final ooooo0O(Landroid/view/View;Z)V
    .locals 1

    .line 1
    if-nez p1, :cond_1

    .line 2
    .line 3
    const-string p1, "null cannot be cast to non-null type com.intsig.camscanner.mode_ocr.view.EditViewDividerMultiLine"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    check-cast p0, Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 9
    .line 10
    invoke-virtual {p0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    const/4 v0, 0x0

    .line 23
    if-nez p1, :cond_0

    .line 24
    .line 25
    const/4 p1, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 p1, 0x0

    .line 28
    :goto_0
    if-eqz p1, :cond_1

    .line 29
    .line 30
    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 31
    .line 32
    .line 33
    :cond_1
    return-void
.end method

.method private static final oo〇O0o〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O88o〇()V

    .line 14
    .line 15
    .line 16
    const/4 p1, 0x0

    .line 17
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08〇o0O:Z

    .line 18
    .line 19
    const-string p0, "CSOcrGiveUpPop"

    .line 20
    .line 21
    const-string p1, "save"

    .line 22
    .line 23
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic o〇08oO80o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o0OoOOo0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇O80o8OO(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    .line 2
    .line 3
    .line 4
    const-string p0, "CSOcrDeletePop"

    .line 5
    .line 6
    const-string p1, "click"

    .line 7
    .line 8
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇O8OO(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇〇8〇〇(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇OoO0(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8〇〇8〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇o08〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/tools/TranslateClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOO〇〇:Lcom/intsig/camscanner/tools/TranslateClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇o0oOO8()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic o〇o8〇〇O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/List;IILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo0O〇8800(Ljava/util/List;I)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final o〇oO08〇o0(Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o88O8()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oOOo:Z

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->OO:Landroid/widget/Button;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    if-eqz p1, :cond_1

    .line 34
    .line 35
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->OO:Landroid/widget/Button;

    .line 36
    .line 37
    if-eqz p1, :cond_1

    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 41
    .line 42
    .line 43
    :cond_1
    :goto_0
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic o〇oo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇〇OOO〇〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇〇8〇〇(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    const/4 p0, 0x1

    .line 2
    invoke-static {p0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o88080oO〇(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOoo80oO:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇00〇〇〇o〇8()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo0:Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;

    .line 8
    .line 9
    if-eqz v1, :cond_1

    .line 10
    .line 11
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 12
    .line 13
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o88〇O(I)V

    .line 14
    .line 15
    .line 16
    iget v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;->getCount()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-ge v2, v1, :cond_0

    .line 23
    .line 24
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇oO:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 25
    .line 26
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 29
    .line 30
    .line 31
    :cond_0
    const/4 v0, 0x1

    .line 32
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇oO08〇o0(Z)V

    .line 33
    .line 34
    .line 35
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o0〇OO008O()V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇0888(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;II)V
    .locals 0

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$this_apply"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-boolean p3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oOOo:Z

    .line 12
    .line 13
    if-eqz p3, :cond_0

    .line 14
    .line 15
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 16
    .line 17
    const/4 p3, 0x1

    .line 18
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O〇oO〇oo8o(I)V

    .line 26
    .line 27
    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇088O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇08O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo88(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇0o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;)V
    .locals 5

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$this_apply"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    invoke-static {p0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o8〇OO0〇0o:Landroid/widget/Button;

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 24
    .line 25
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o8〇OO0〇0o:Landroid/widget/Button;

    .line 26
    .line 27
    const-string v2, "itbOcrEditResultSave"

    .line 28
    .line 29
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    instance-of v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 37
    .line 38
    const/4 v4, 0x0

    .line 39
    if-eqz v3, :cond_0

    .line 40
    .line 41
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 42
    .line 43
    invoke-static {v1}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    goto :goto_0

    .line 48
    :cond_0
    const/4 v1, 0x0

    .line 49
    :goto_0
    add-int/2addr v0, v1

    .line 50
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o8〇OO0〇0o:Landroid/widget/Button;

    .line 51
    .line 52
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    instance-of v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 60
    .line 61
    if-eqz v2, :cond_1

    .line 62
    .line 63
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 64
    .line 65
    invoke-static {v1}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    :cond_1
    add-int/2addr v0, v4

    .line 70
    if-lez v0, :cond_2

    .line 71
    .line 72
    sub-int/2addr p0, v0

    .line 73
    int-to-float p0, p0

    .line 74
    const/high16 v0, 0x40900000    # 4.5f

    .line 75
    .line 76
    div-float/2addr p0, v0

    .line 77
    float-to-int p0, p0

    .line 78
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->ooo0〇〇O:Lcom/intsig/view/ImageTextButton;

    .line 79
    .line 80
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    iput p0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 85
    .line 86
    iget-object p0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->ooo0〇〇O:Lcom/intsig/view/ImageTextButton;

    .line 87
    .line 88
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    .line 90
    .line 91
    iget-object p0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇08O:Lcom/intsig/view/ImageTextButton;

    .line 92
    .line 93
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 94
    .line 95
    .line 96
    iget-object p0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 97
    .line 98
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    .line 100
    .line 101
    iget-object p0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->OO〇00〇8oO:Lcom/intsig/view/ImageTextButton;

    .line 102
    .line 103
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    .line 105
    .line 106
    iget-object p0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇8〇oO〇〇8o:Lcom/intsig/view/ImageTextButton;

    .line 107
    .line 108
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    .line 110
    .line 111
    :cond_2
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/lang/String;IZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0〇8o〇(Ljava/lang/String;IZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇0o88O()V
    .locals 3

    .line 1
    const-string v0, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v1, "showEditBackTip"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 13
    .line 14
    .line 15
    const v1, 0x7f13047a

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    new-instance v1, L〇0O〇O00O/〇oOO8O8;

    .line 23
    .line 24
    invoke-direct {v1, p0}, L〇0O〇O00O/〇oOO8O8;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 25
    .line 26
    .line 27
    const v2, 0x7f130c00

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    new-instance v1, L〇0O〇O00O/〇0000OOO;

    .line 35
    .line 36
    invoke-direct {v1, p0}, L〇0O〇O00O/〇0000OOO;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 37
    .line 38
    .line 39
    const v2, 0x7f130614

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const/4 v1, 0x1

    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇888(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oOOo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO8〇O8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇0ooOOo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o88o88(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0o〇o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;

    .line 10
    .line 11
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 12
    .line 13
    .line 14
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 15
    .line 16
    invoke-static {v1}, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->O8(Landroid/widget/TextView;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic 〇0〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/List;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8〇O〇0O0〇(Ljava/util/List;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic 〇0〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇00O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0〇8o〇(Ljava/lang/String;IZ)V
    .locals 2

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "txt_num"

    .line 7
    .line 8
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p2

    .line 12
    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 13
    .line 14
    .line 15
    const-string p2, "copy"

    .line 16
    .line 17
    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 18
    .line 19
    .line 20
    move-result p2

    .line 21
    if-eqz p2, :cond_1

    .line 22
    .line 23
    const-string p2, "from_part"

    .line 24
    .line 25
    if-eqz p3, :cond_0

    .line 26
    .line 27
    const-string p3, "cs_ocr_result_select"

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const-string p3, "cs_ocr_result_bottom"

    .line 31
    .line 32
    :goto_0
    invoke-virtual {v0, p2, p3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    .line 34
    .line 35
    goto :goto_1

    .line 36
    :catch_0
    move-exception p2

    .line 37
    const-string p3, "BatchOcrResultFragment"

    .line 38
    .line 39
    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    .line 41
    .line 42
    :cond_1
    :goto_1
    const-string p2, "CSOcrResult"

    .line 43
    .line 44
    invoke-static {p2, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇0〇o8〇(Ljava/lang/String;)V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "BatchOcrResultFragment"

    .line 6
    .line 7
    if-eqz v0, :cond_6

    .line 8
    .line 9
    const-string v2, "refreshEditChangeView"

    .line 10
    .line 11
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 15
    .line 16
    const-string v2, "etOcrResult"

    .line 17
    .line 18
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mTextWatcher$1;

    .line 22
    .line 23
    invoke-static {v1, p1, v3}, Lcom/intsig/camscanner/mode_ocr/ext/ViewExtKt;->〇o00〇〇Oo(Landroid/widget/EditText;Ljava/lang/String;Landroid/text/TextWatcher;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇8O0O808〇()Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-eqz v1, :cond_0

    .line 35
    .line 36
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->OO:Landroid/widget/Button;

    .line 37
    .line 38
    const v3, 0x7f1300f8

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->OO:Landroid/widget/Button;

    .line 50
    .line 51
    const v3, 0x7f1301b4

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    .line 60
    .line 61
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8888()Z

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    const/4 v3, 0x2

    .line 70
    const/4 v4, 0x0

    .line 71
    const/4 v5, 0x1

    .line 72
    if-nez v1, :cond_1

    .line 73
    .line 74
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇00O0:Landroid/view/ViewStub;

    .line 75
    .line 76
    const-string v2, "vsNotRecognize"

    .line 77
    .line 78
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    invoke-static {v1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 82
    .line 83
    .line 84
    new-array v1, v3, [Landroid/view/View;

    .line 85
    .line 86
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->Oo80:Landroid/widget/TextView;

    .line 87
    .line 88
    aput-object v2, v1, v4

    .line 89
    .line 90
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 91
    .line 92
    aput-object v2, v1, v5

    .line 93
    .line 94
    invoke-direct {p0, v4, v1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oo0OOO(Z[Landroid/view/View;)V

    .line 95
    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o88O〇8()Z

    .line 103
    .line 104
    .line 105
    move-result v1

    .line 106
    if-eqz v1, :cond_2

    .line 107
    .line 108
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->Oo80:Landroid/widget/TextView;

    .line 109
    .line 110
    const-string v2, "tvOcrResultEmptyView"

    .line 111
    .line 112
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    invoke-static {v1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 116
    .line 117
    .line 118
    new-array v1, v3, [Landroid/view/View;

    .line 119
    .line 120
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 121
    .line 122
    aput-object v2, v1, v4

    .line 123
    .line 124
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇00O0:Landroid/view/ViewStub;

    .line 125
    .line 126
    aput-object v2, v1, v5

    .line 127
    .line 128
    invoke-direct {p0, v4, v1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oo0OOO(Z[Landroid/view/View;)V

    .line 129
    .line 130
    .line 131
    goto :goto_1

    .line 132
    :cond_2
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 133
    .line 134
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    invoke-static {v1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 138
    .line 139
    .line 140
    new-array v1, v3, [Landroid/view/View;

    .line 141
    .line 142
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇00O0:Landroid/view/ViewStub;

    .line 143
    .line 144
    aput-object v2, v1, v4

    .line 145
    .line 146
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->Oo80:Landroid/widget/TextView;

    .line 147
    .line 148
    aput-object v2, v1, v5

    .line 149
    .line 150
    invoke-direct {p0, v4, v1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oo0OOO(Z[Landroid/view/View;)V

    .line 151
    .line 152
    .line 153
    :goto_1
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 154
    .line 155
    if-eqz p1, :cond_4

    .line 156
    .line 157
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 158
    .line 159
    .line 160
    move-result p1

    .line 161
    if-nez p1, :cond_3

    .line 162
    .line 163
    goto :goto_2

    .line 164
    :cond_3
    const/4 p1, 0x0

    .line 165
    goto :goto_3

    .line 166
    :cond_4
    :goto_2
    const/4 p1, 0x1

    .line 167
    :goto_3
    xor-int/2addr p1, v5

    .line 168
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 169
    .line 170
    .line 171
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oOOo:Z

    .line 172
    .line 173
    if-nez p1, :cond_7

    .line 174
    .line 175
    if-nez p1, :cond_5

    .line 176
    .line 177
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0o〇o()V

    .line 178
    .line 179
    .line 180
    :cond_5
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Ooo08:Z

    .line 181
    .line 182
    if-eqz p1, :cond_7

    .line 183
    .line 184
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇OO8ooO8〇:Z

    .line 185
    .line 186
    if-eqz p1, :cond_7

    .line 187
    .line 188
    iput-boolean v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇OO8ooO8〇:Z

    .line 189
    .line 190
    const/4 v6, 0x0

    .line 191
    const/4 v7, 0x0

    .line 192
    const/4 v8, 0x1

    .line 193
    const/4 v9, 0x2

    .line 194
    const/4 v10, 0x0

    .line 195
    move-object v5, p0

    .line 196
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0oO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;FZZILjava/lang/Object;)V

    .line 197
    .line 198
    .line 199
    goto :goto_4

    .line 200
    :cond_6
    const-string p1, "refreshEditChangeView mBinding == null"

    .line 201
    .line 202
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    :cond_7
    :goto_4
    return-void
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇80O80O〇0()V
    .locals 4

    .line 1
    const-string v0, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v1, "showFromMultiCaptureBackDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 13
    .line 14
    .line 15
    const v1, 0x7f131bcb

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const v1, 0x7f130c05

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    new-instance v1, L〇0O〇O00O/〇080;

    .line 34
    .line 35
    invoke-direct {v1, p0}, L〇0O〇O00O/〇080;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 36
    .line 37
    .line 38
    const v2, 0x7f130bff

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    new-instance v1, L〇0O〇O00O/〇O8o08O;

    .line 46
    .line 47
    invoke-direct {v1}, L〇0O〇O00O/〇O8o08O;-><init>()V

    .line 48
    .line 49
    .line 50
    const v2, 0x7f13057e

    .line 51
    .line 52
    .line 53
    const v3, 0x7f060207

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v2, v3, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    const/4 v1, 0x1

    .line 61
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇888(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 70
    .line 71
    .line 72
    const-string v0, "CSOcrResultBackPop"

    .line 73
    .line 74
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static synthetic 〇80〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8o0o0(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇8O(Z)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->oo0O〇0〇〇〇()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-le v0, v1, :cond_0

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;

    .line 13
    .line 14
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;-><init>()V

    .line 15
    .line 16
    .line 17
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$reCloudOcr$1;

    .line 18
    .line 19
    invoke-direct {v1, p1, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$reCloudOcr$1;-><init>(ZLcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;->〇80O8o8O〇(Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog$ChoseOperationListener;)Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 27
    .line 28
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-string v1, "BatchOcrResultFragment"

    .line 33
    .line 34
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/mode_ocr/view/ChoseOperationRangeDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇000(Z)Ljava/util/List;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    const/4 v0, 0x2

    .line 43
    const/4 v1, 0x0

    .line 44
    const/4 v2, 0x0

    .line 45
    invoke-static {p0, p1, v2, v0, v1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o8〇〇O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/List;IILjava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    :goto_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇8O0880(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8o0o0(Z)V
    .locals 3

    .line 1
    const-string v0, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v1, "clickReOcr"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->o〇0(I)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    new-instance v0, L〇0O〇O00O/〇8o8o〇;

    .line 18
    .line 19
    invoke-direct {v0, p0}, L〇0O〇O00O/〇8o8o〇;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 20
    .line 21
    .line 22
    invoke-static {p1, v0}, Lcom/intsig/camscanner/app/DialogUtils;->O0o〇〇Oo(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    if-eqz p1, :cond_1

    .line 27
    .line 28
    const/4 p1, 0x0

    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇000(Z)Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const/4 v1, 0x2

    .line 34
    const/4 v2, 0x0

    .line 35
    invoke-static {p0, v0, p1, v1, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o8〇〇O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/List;IILjava/lang/Object;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8O(Z)V

    .line 40
    .line 41
    .line 42
    :goto_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇8o80O(Ljava/lang/String;)V
    .locals 2

    .line 1
    const-string v0, "CSOcrResult"

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo8〇〇()Lorg/json/JSONObject;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {v0, p1, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8oo0oO0(Ljava/lang/String;JLjava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    cmp-long v2, p2, v0

    .line 4
    .line 5
    if-lez v2, :cond_2

    .line 6
    .line 7
    invoke-interface {p4}, Ljava/util/Collection;->isEmpty()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    xor-int/lit8 v0, v0, 0x1

    .line 12
    .line 13
    if-eqz v0, :cond_2

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 16
    .line 17
    sget-object v1, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_ORIGIN_PDF_PREVIEW:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 18
    .line 19
    if-ne v0, v1, :cond_0

    .line 20
    .line 21
    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO88〇0O8O(J)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-static {p2, p3}, Lcom/intsig/camscanner/image2word/Image2WordNavigator;->〇80〇808〇O(J)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    const-string v1, "mActivity"

    .line 30
    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 34
    .line 35
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    const-string p4, "ocr_to_word"

    .line 39
    .line 40
    invoke-static {p1, p2, p3, p4}, Lcom/intsig/camscanner/image2word/Image2WordNavigator;->Oo08(Landroidx/fragment/app/FragmentActivity;JLjava/lang/String;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/pagelist/WordPreviewActivity;->o8oOOo:Lcom/intsig/camscanner/pagelist/WordPreviewActivity$Companion;

    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 47
    .line 48
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const/4 v6, 0x0

    .line 52
    const/4 v7, 0x0

    .line 53
    const/4 v8, 0x0

    .line 54
    const/16 v9, 0x40

    .line 55
    .line 56
    const/4 v10, 0x0

    .line 57
    move-object v1, v2

    .line 58
    move-object v2, p1

    .line 59
    move-wide v3, p2

    .line 60
    move-object v5, p4

    .line 61
    invoke-static/range {v0 .. v10}, Lcom/intsig/camscanner/pagelist/WordPreviewActivity$Companion;->〇080(Lcom/intsig/camscanner/pagelist/WordPreviewActivity$Companion;Landroid/app/Activity;Ljava/lang/String;JLjava/util/ArrayList;ILjava/lang/String;ZILjava/lang/Object;)V

    .line 62
    .line 63
    .line 64
    :cond_2
    :goto_0
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8oo8888()Ljava/lang/String;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8〇OO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v0, "ocr_result"

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const-string v0, "check_result"

    .line 9
    .line 10
    :goto_0
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇8ooOO(ZI)V
    .locals 11

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "keyBoardShow"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, " --> height: "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    const-string v0, "BatchOcrResultFragment"

    .line 27
    .line 28
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    xor-int/lit8 p2, p1, 0x1

    .line 32
    .line 33
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇oO08〇o0(Z)V

    .line 34
    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    if-eqz p2, :cond_2

    .line 41
    .line 42
    const/4 v1, 0x2

    .line 43
    const/4 v2, 0x3

    .line 44
    const-string v3, "flOcrEditBottomKeyboardBtn"

    .line 45
    .line 46
    const/4 v4, 0x0

    .line 47
    const/4 v5, 0x1

    .line 48
    if-eqz p1, :cond_0

    .line 49
    .line 50
    const-string v0, "proofread"

    .line 51
    .line 52
    const/4 v6, 0x0

    .line 53
    invoke-direct {p0, v0, v6}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO0〇O(Ljava/lang/String;Landroid/util/Pair;)V

    .line 54
    .line 55
    .line 56
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o08O()V

    .line 61
    .line 62
    .line 63
    iget-object v0, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 64
    .line 65
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 66
    .line 67
    .line 68
    iget-object v0, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 69
    .line 70
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 71
    .line 72
    .line 73
    iget-object v0, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 74
    .line 75
    invoke-virtual {v0}, Landroid/widget/TextView;->getSelectionStart()I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 80
    .line 81
    .line 82
    move-result-object v6

    .line 83
    invoke-virtual {v6, v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O〇oO〇oo8o(I)V

    .line 84
    .line 85
    .line 86
    iget-object v0, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇0O:Landroid/widget/FrameLayout;

    .line 87
    .line 88
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-static {v0, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 92
    .line 93
    .line 94
    const/4 v0, 0x4

    .line 95
    new-array v0, v0, [Landroid/view/View;

    .line 96
    .line 97
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbar:Landroidx/appcompat/widget/Toolbar;

    .line 98
    .line 99
    aput-object v3, v0, v4

    .line 100
    .line 101
    iget-object v3, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇o0O:Landroid/widget/RelativeLayout;

    .line 102
    .line 103
    aput-object v3, v0, v5

    .line 104
    .line 105
    iget-object v3, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 106
    .line 107
    aput-object v3, v0, v1

    .line 108
    .line 109
    iget-object v1, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 110
    .line 111
    aput-object v1, v0, v2

    .line 112
    .line 113
    invoke-direct {p0, v4, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oo0OOO(Z[Landroid/view/View;)V

    .line 114
    .line 115
    .line 116
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 117
    .line 118
    const v0, 0x7f081108

    .line 119
    .line 120
    .line 121
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 122
    .line 123
    .line 124
    const/4 v6, 0x0

    .line 125
    const/4 v7, 0x1

    .line 126
    const/4 v8, 0x0

    .line 127
    const/4 v9, 0x4

    .line 128
    const/4 v10, 0x0

    .line 129
    move-object v5, p0

    .line 130
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0oO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;FZZILjava/lang/Object;)V

    .line 131
    .line 132
    .line 133
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 134
    .line 135
    .line 136
    move-result-object p2

    .line 137
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇008〇oo(Z)Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 146
    .line 147
    .line 148
    move-result v0

    .line 149
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->〇8o8o〇(I)V

    .line 150
    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_0
    iget-object v6, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 154
    .line 155
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 156
    .line 157
    .line 158
    iget-object v6, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇0O:Landroid/widget/FrameLayout;

    .line 159
    .line 160
    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    invoke-static {v6, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 164
    .line 165
    .line 166
    iget-object v3, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 167
    .line 168
    const-string v6, "tvOcrAutoWrap"

    .line 169
    .line 170
    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    .line 172
    .line 173
    invoke-static {v3, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 174
    .line 175
    .line 176
    new-array v2, v2, [Landroid/view/View;

    .line 177
    .line 178
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbar:Landroidx/appcompat/widget/Toolbar;

    .line 179
    .line 180
    aput-object v3, v2, v4

    .line 181
    .line 182
    iget-object v3, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇o0O:Landroid/widget/RelativeLayout;

    .line 183
    .line 184
    aput-object v3, v2, v5

    .line 185
    .line 186
    iget-object v3, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 187
    .line 188
    aput-object v3, v2, v1

    .line 189
    .line 190
    invoke-direct {p0, v5, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oo0OOO(Z[Landroid/view/View;)V

    .line 191
    .line 192
    .line 193
    iget-object v1, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 194
    .line 195
    const v2, 0x7f0810fb

    .line 196
    .line 197
    .line 198
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 199
    .line 200
    .line 201
    const/4 v4, 0x0

    .line 202
    const/4 v5, 0x1

    .line 203
    const/4 v6, 0x0

    .line 204
    const/4 v7, 0x4

    .line 205
    const/4 v8, 0x0

    .line 206
    move-object v3, p0

    .line 207
    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0oO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;FZZILjava/lang/Object;)V

    .line 208
    .line 209
    .line 210
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇oO:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 211
    .line 212
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 213
    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    .line 215
    .line 216
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 217
    .line 218
    .line 219
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 223
    .line 224
    .line 225
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v0

    .line 229
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    .line 230
    .line 231
    .line 232
    move-result-object p2

    .line 233
    check-cast p2, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;

    .line 234
    .line 235
    if-eqz p2, :cond_1

    .line 236
    .line 237
    invoke-virtual {p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->o〇〇0〇()V

    .line 238
    .line 239
    .line 240
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 241
    .line 242
    .line 243
    move-result-object p2

    .line 244
    invoke-virtual {p2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o88O8()Z

    .line 245
    .line 246
    .line 247
    move-result p2

    .line 248
    if-eqz p2, :cond_2

    .line 249
    .line 250
    iget p2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 251
    .line 252
    invoke-direct {p0, p2, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8〇o〇OoO8(IZ)V

    .line 253
    .line 254
    .line 255
    :cond_2
    return-void
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇8ooo()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_6

    .line 8
    .line 9
    const-string v1, "extra_from_page_type"

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const-string v2, "null cannot be cast to non-null type com.intsig.camscanner.mode_ocr.PageFromType"

    .line 16
    .line 17
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 21
    .line 22
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 23
    .line 24
    sget-object v2, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_OCR_PREVIEW:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 25
    .line 26
    const/4 v3, 0x1

    .line 27
    const/4 v4, 0x0

    .line 28
    if-eq v1, v2, :cond_1

    .line 29
    .line 30
    sget-object v2, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_SINGLE_CAPTURE_OCR:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 31
    .line 32
    if-ne v1, v2, :cond_0

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const/4 v2, 0x0

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 38
    :goto_1
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8〇OO0〇0o:Z

    .line 39
    .line 40
    sget-object v2, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_PDF_PREVIEW:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 41
    .line 42
    if-ne v1, v2, :cond_2

    .line 43
    .line 44
    const/4 v1, 0x1

    .line 45
    goto :goto_2

    .line 46
    :cond_2
    const/4 v1, 0x0

    .line 47
    :goto_2
    iput-boolean v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8〇oO〇〇8o:Z

    .line 48
    .line 49
    const-string v1, "extra_spec_action"

    .line 50
    .line 51
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇08O:Ljava/lang/String;

    .line 56
    .line 57
    const-string v1, "extra_doc_info"

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    check-cast v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 64
    .line 65
    if-nez v1, :cond_3

    .line 66
    .line 67
    new-instance v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 68
    .line 69
    invoke-direct {v1}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 70
    .line 71
    .line 72
    :cond_3
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 73
    .line 74
    const-string v1, "EXTRA_LEFT_OCR_BALANCE"

    .line 75
    .line 76
    const/4 v2, -0x1

    .line 77
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0O:I

    .line 82
    .line 83
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 84
    .line 85
    sget-object v2, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_OCR_MULTI_CAPTURE_EDIT:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 86
    .line 87
    if-ne v1, v2, :cond_4

    .line 88
    .line 89
    const/4 v2, 0x1

    .line 90
    goto :goto_3

    .line 91
    :cond_4
    const/4 v2, 0x0

    .line 92
    :goto_3
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇08oOOO0:Z

    .line 93
    .line 94
    sget-object v2, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_PAGE_LIST_VIEW_OCR:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 95
    .line 96
    if-ne v1, v2, :cond_5

    .line 97
    .line 98
    goto :goto_4

    .line 99
    :cond_5
    const/4 v3, 0x0

    .line 100
    :goto_4
    iput-boolean v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Ooo08:Z

    .line 101
    .line 102
    const-string v1, "EXTRA_FROM_IS_OCR_RESULT"

    .line 103
    .line 104
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8〇OO:Z

    .line 109
    .line 110
    :cond_6
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇8〇0O〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O08〇oO8〇(Landroid/view/MotionEvent;)V

    .line 9
    .line 10
    .line 11
    :cond_0
    iget-boolean p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇o〇:Z

    .line 12
    .line 13
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇8〇80o(Landroid/view/View;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooooo0O(Landroid/view/View;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇8〇8o00(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇o8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇0o8〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8〇o〇OoO8(IZ)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 8
    .line 9
    new-instance v2, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v3, "setOnEdit "

    .line 15
    .line 16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    const-string v2, "BatchOcrResultFragment"

    .line 27
    .line 28
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇oO:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 32
    .line 33
    new-instance v1, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;

    .line 53
    .line 54
    if-eqz p1, :cond_0

    .line 55
    .line 56
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/mode_ocr/view/OcrFrameView;->setOnEdit(Z)V

    .line 57
    .line 58
    .line 59
    :cond_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇8〇〇8o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8〇〇8〇8()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08〇o0O:Z

    .line 2
    .line 3
    const-string v1, "BatchOcrResultFragment"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "showCancelDialog mHasEditData false"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/utils/SoftKeyboardUtils;->〇080(Landroid/app/Activity;)V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    const-string v0, "showCancelDialog"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :try_start_0
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 26
    .line 27
    invoke-direct {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 28
    .line 29
    .line 30
    const v2, 0x7f1316fb

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    const v2, 0x7f1316fc

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    new-instance v2, L〇0O〇O00O/Oooo8o0〇;

    .line 45
    .line 46
    invoke-direct {v2, p0}, L〇0O〇O00O/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 47
    .line 48
    .line 49
    const v3, 0x7f1316fd

    .line 50
    .line 51
    .line 52
    const v4, 0x7f060207

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v3, v4, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    new-instance v2, L〇0O〇O00O/〇〇808〇;

    .line 60
    .line 61
    invoke-direct {v2, p0}, L〇0O〇O00O/〇〇808〇;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 62
    .line 63
    .line 64
    const v3, 0x7f130c00

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    const/4 v2, 0x0

    .line 72
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 81
    .line 82
    .line 83
    const-string v0, "CSOcrGiveUpPop"

    .line 84
    .line 85
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    .line 92
    .line 93
    :goto_0
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇O0o〇〇o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇O80o8OO(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇oO〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8〇8000(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOOO0(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O8〇8O0oO(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O8〇〇o8〇(I)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "updateTranslateCountView --> translateLeftNum:"

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const-string v2, "BatchOcrResultFragment"

    .line 25
    .line 26
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    const/16 v1, 0x64

    .line 30
    .line 31
    if-ge p1, v1, :cond_1

    .line 32
    .line 33
    if-gtz p1, :cond_0

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 37
    .line 38
    int-to-long v2, p1

    .line 39
    invoke-virtual {v1, v2, v3}, Lcom/intsig/view/ImageTextButton;->setDotNum(J)V

    .line 40
    .line 41
    .line 42
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 43
    .line 44
    const/4 v0, 0x0

    .line 45
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 46
    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_1
    :goto_0
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 50
    .line 51
    const-wide/16 v1, -0x1

    .line 52
    .line 53
    invoke-virtual {p1, v1, v2}, Lcom/intsig/view/ImageTextButton;->setDotNum(J)V

    .line 54
    .line 55
    .line 56
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 57
    .line 58
    const/4 v0, 0x1

    .line 59
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 60
    .line 61
    .line 62
    :cond_2
    :goto_1
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇OoO0o0(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇Oo〇O(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o088O8800(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇O〇〇〇(Landroid/view/View;Landroid/view/View;Lcom/intsig/comm/widget/CustomTextView;)V
    .locals 5

    .line 1
    const-string v0, "$anchor"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$tipTV"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-lez v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-lez v0, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x2

    .line 24
    new-array v1, v0, [I

    .line 25
    .line 26
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 27
    .line 28
    .line 29
    new-array v2, v0, [I

    .line 30
    .line 31
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 32
    .line 33
    .line 34
    const/4 p1, 0x0

    .line 35
    aget v3, v1, p1

    .line 36
    .line 37
    aget v4, v2, p1

    .line 38
    .line 39
    sub-int/2addr v3, v4

    .line 40
    aput v3, v1, p1

    .line 41
    .line 42
    const/4 p1, 0x1

    .line 43
    aget v3, v1, p1

    .line 44
    .line 45
    aget v2, v2, p1

    .line 46
    .line 47
    sub-int/2addr v3, v2

    .line 48
    aput v3, v1, p1

    .line 49
    .line 50
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    const-string v3, "null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams"

    .line 55
    .line 56
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 60
    .line 61
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 66
    .line 67
    aget p1, v1, p1

    .line 68
    .line 69
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 70
    .line 71
    .line 72
    move-result p0

    .line 73
    div-int/2addr p0, v0

    .line 74
    add-int/2addr p1, p0

    .line 75
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 76
    .line 77
    .line 78
    move-result p0

    .line 79
    div-int/2addr p0, v0

    .line 80
    sub-int/2addr p1, p0

    .line 81
    iput p1, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 82
    .line 83
    invoke-virtual {p2, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 87
    .line 88
    .line 89
    move-result p0

    .line 90
    div-int/2addr p0, v0

    .line 91
    invoke-virtual {p2, p0}, Lcom/intsig/comm/widget/CustomTextView;->setArrowMarginLeft(I)V

    .line 92
    .line 93
    .line 94
    :cond_0
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic 〇o08(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o〇88(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o88〇O(I)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    add-int/lit8 v0, p1, 0x1

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->oo0O〇0〇〇〇()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-gt v1, v3, :cond_0

    .line 14
    .line 15
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇00O:Landroid/widget/LinearLayout;

    .line 16
    .line 17
    if-eqz v4, :cond_1

    .line 18
    .line 19
    invoke-static {v4, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇00O:Landroid/widget/LinearLayout;

    .line 24
    .line 25
    if-eqz v4, :cond_1

    .line 26
    .line 27
    invoke-static {v4, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_0
    if-le v0, v1, :cond_2

    .line 31
    .line 32
    new-instance p1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v2, "showPageNum curPage:"

    .line 38
    .line 39
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v0, ", totalPageNum:"

    .line 46
    .line 47
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    const-string v0, "BatchOcrResultFragment"

    .line 58
    .line 59
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    return-void

    .line 63
    :cond_2
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 64
    .line 65
    if-nez v4, :cond_3

    .line 66
    .line 67
    goto :goto_2

    .line 68
    :cond_3
    if-eqz p1, :cond_4

    .line 69
    .line 70
    const/4 p1, 0x1

    .line 71
    goto :goto_1

    .line 72
    :cond_4
    const/4 p1, 0x0

    .line 73
    :goto_1
    invoke-virtual {v4, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 74
    .line 75
    .line 76
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O:Landroid/widget/ImageView;

    .line 77
    .line 78
    if-nez p1, :cond_5

    .line 79
    .line 80
    goto :goto_3

    .line 81
    :cond_5
    if-eq v0, v1, :cond_6

    .line 82
    .line 83
    const/4 v2, 0x1

    .line 84
    :cond_6
    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 85
    .line 86
    .line 87
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 88
    .line 89
    if-nez p1, :cond_7

    .line 90
    .line 91
    goto :goto_4

    .line 92
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    const-string v0, "/"

    .line 101
    .line 102
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    .line 114
    .line 115
    :goto_4
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇o8〇8()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->〇080:Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->oO80()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/tools/TranslateClient;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$initTranslateClient$1;

    .line 14
    .line 15
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$initTranslateClient$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 16
    .line 17
    .line 18
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_OCR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 19
    .line 20
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/tools/TranslateClient;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/tools/TranslateClient$TranslateClientCallback;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOO〇〇:Lcom/intsig/camscanner/tools/TranslateClient;

    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Landroid/app/Dialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o〇Oo88:Landroid/app/Dialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇oOO80o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇oO〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    :try_start_0
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o〇Oo88:Landroid/app/Dialog;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p0

    .line 15
    const-string p1, "BatchOcrResultFragment"

    .line 16
    .line 17
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    :goto_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇oO〇08o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo0:Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0〇o8〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇ooO〇000(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O008oO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇o〇88(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o〇88〇8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O8〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇o〇OO80oO()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇8o8O〇O()Landroidx/lifecycle/MutableLiveData;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    new-instance v3, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$1;

    .line 20
    .line 21
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 22
    .line 23
    .line 24
    new-instance v4, L〇0O〇O00O/〇o〇;

    .line 25
    .line 26
    invoke-direct {v4, v3}, L〇0O〇O00O/〇o〇;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, v2, v4}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇oo()Landroidx/lifecycle/MutableLiveData;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    new-instance v3, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$2;

    .line 45
    .line 46
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$2;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 47
    .line 48
    .line 49
    new-instance v4, L〇0O〇O00O/O8;

    .line 50
    .line 51
    invoke-direct {v4, v3}, L〇0O〇O00O/O8;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v1, v2, v4}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇OO8Oo0〇()Landroidx/lifecycle/MutableLiveData;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    new-instance v3, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$3;

    .line 70
    .line 71
    invoke-direct {v3, v0, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$3;-><init>(Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 72
    .line 73
    .line 74
    new-instance v0, L〇0O〇O00O/Oo08;

    .line 75
    .line 76
    invoke-direct {v0, v3}, L〇0O〇O00O/Oo08;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1, v2, v0}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 80
    .line 81
    .line 82
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o8o〇〇0O()Landroidx/lifecycle/MutableLiveData;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$4;

    .line 95
    .line 96
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$4;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 97
    .line 98
    .line 99
    new-instance v3, L〇0O〇O00O/o〇0;

    .line 100
    .line 101
    invoke-direct {v3, v2}, L〇0O〇O00O/o〇0;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 105
    .line 106
    .line 107
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O0〇oo()Landroidx/lifecycle/MutableLiveData;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$5;

    .line 120
    .line 121
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$5;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 122
    .line 123
    .line 124
    new-instance v3, L〇0O〇O00O/〇〇888;

    .line 125
    .line 126
    invoke-direct {v3, v2}, L〇0O〇O00O/〇〇888;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 127
    .line 128
    .line 129
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 130
    .line 131
    .line 132
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->〇O00()Landroidx/lifecycle/MutableLiveData;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$6;

    .line 145
    .line 146
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$6;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 147
    .line 148
    .line 149
    new-instance v3, L〇0O〇O00O/oO80;

    .line 150
    .line 151
    invoke-direct {v3, v2}, L〇0O〇O00O/oO80;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 152
    .line 153
    .line 154
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 155
    .line 156
    .line 157
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->O8ooOoo〇()Landroidx/lifecycle/MutableLiveData;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 166
    .line 167
    .line 168
    move-result-object v1

    .line 169
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$7;

    .line 170
    .line 171
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$7;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 172
    .line 173
    .line 174
    new-instance v3, L〇0O〇O00O/〇80〇808〇O;

    .line 175
    .line 176
    invoke-direct {v3, v2}, L〇0O〇O00O/〇80〇808〇O;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 177
    .line 178
    .line 179
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 180
    .line 181
    .line 182
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o0oOO8()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;

    .line 183
    .line 184
    .line 185
    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;->O8〇o()Landroidx/lifecycle/MutableLiveData;

    .line 187
    .line 188
    .line 189
    move-result-object v0

    .line 190
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$8;

    .line 195
    .line 196
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$addObserver$1$8;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 197
    .line 198
    .line 199
    new-instance v3, L〇0O〇O00O/OO0o〇〇〇〇0;

    .line 200
    .line 201
    invoke-direct {v3, v2}, L〇0O〇O00O/OO0o〇〇〇〇0;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 205
    .line 206
    .line 207
    :cond_0
    return-void
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static final synthetic 〇〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇0()V
    .locals 10

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo〇〇〇〇()V

    .line 8
    .line 9
    .line 10
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    invoke-virtual {v1, v2}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 14
    .line 15
    .line 16
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇08O:Lcom/intsig/view/ImageTextButton;

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇〇O()V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->Oo0oOo〇0()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    iget v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 39
    .line 40
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->ooo8o〇o〇(I)V

    .line 41
    .line 42
    .line 43
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;

    .line 44
    .line 45
    const-string v5, "BatchOcrResultFragment"

    .line 46
    .line 47
    iget-object v6, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    const-string v3, "mActivity"

    .line 50
    .line 51
    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    invoke-virtual {v3}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 59
    .line 60
    .line 61
    move-result-object v7

    .line 62
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 63
    .line 64
    .line 65
    move-result-object v8

    .line 66
    const-string v3, "viewLifecycleOwner"

    .line 67
    .line 68
    invoke-static {v8, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    iget-object v9, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$mImgTouchBack$1;

    .line 72
    .line 73
    move-object v4, v1

    .line 74
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;-><init>(Ljava/lang/String;Landroid/app/Activity;Ljava/util/List;Landroidx/lifecycle/LifecycleOwner;Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter$ImgTouchCallback;)V

    .line 75
    .line 76
    .line 77
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo0:Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;

    .line 78
    .line 79
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇oO:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 80
    .line 81
    const/4 v3, 0x2

    .line 82
    invoke-virtual {v1, v3}, Landroidx/viewpager/widget/ViewPager;->setOffscreenPageLimit(I)V

    .line 83
    .line 84
    .line 85
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇oO:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 86
    .line 87
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo0:Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;

    .line 88
    .line 89
    invoke-virtual {v1, v3}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 90
    .line 91
    .line 92
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->o〇oO:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 93
    .line 94
    const/4 v3, 0x0

    .line 95
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/view/MyViewPager;->setScrollable(Z)V

    .line 96
    .line 97
    .line 98
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00〇〇〇o〇8()V

    .line 99
    .line 100
    .line 101
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇8〇oO〇〇8o:Lcom/intsig/view/ImageTextButton;

    .line 102
    .line 103
    invoke-virtual {v1, v2}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 104
    .line 105
    .line 106
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O0O:I

    .line 107
    .line 108
    if-gtz v1, :cond_0

    .line 109
    .line 110
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->o0ooO()V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O008oO0()V

    .line 119
    .line 120
    .line 121
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇00o08()V

    .line 122
    .line 123
    .line 124
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇oO08〇o0(Z)V

    .line 125
    .line 126
    .line 127
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O〇o88o08〇:Landroid/widget/TextView;

    .line 128
    .line 129
    const-string v4, "tvTranslateLanguage"

    .line 130
    .line 131
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇88(Landroid/view/View;)V

    .line 135
    .line 136
    .line 137
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 138
    .line 139
    invoke-static {v1}, Lcom/intsig/camscanner/util/DarkModeUtils;->〇080(Landroid/content/Context;)Z

    .line 140
    .line 141
    .line 142
    move-result v1

    .line 143
    if-eqz v1, :cond_2

    .line 144
    .line 145
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 146
    .line 147
    const v4, 0x7f08106f

    .line 148
    .line 149
    .line 150
    invoke-static {v1, v4}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    if-eqz v1, :cond_1

    .line 155
    .line 156
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    .line 157
    .line 158
    .line 159
    move-result v4

    .line 160
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    .line 161
    .line 162
    .line 163
    move-result v5

    .line 164
    invoke-virtual {v1, v3, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 165
    .line 166
    .line 167
    :cond_1
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 168
    .line 169
    const/4 v4, 0x0

    .line 170
    invoke-virtual {v3, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 171
    .line 172
    .line 173
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 174
    .line 175
    const-string v1, "tvOcrAutoWrap"

    .line 176
    .line 177
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 181
    .line 182
    .line 183
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 184
    .line 185
    .line 186
    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o〇〇0〇88()Z

    .line 188
    .line 189
    .line 190
    move-result v0

    .line 191
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oooo800〇〇(Z)V

    .line 192
    .line 193
    .line 194
    :cond_3
    return-void
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final 〇〇8o0OOOo()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o08oOO()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    new-instance v1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v2, "saveChangeData, isDataChange:"

    .line 15
    .line 16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "BatchOcrResultFragment"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o08oOO()Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-nez v0, :cond_1

    .line 40
    .line 41
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇08oOOO0:Z

    .line 42
    .line 43
    if-eqz v0, :cond_0

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 47
    .line 48
    new-instance v1, Landroid/content/Intent;

    .line 49
    .line 50
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 51
    .line 52
    .line 53
    const/4 v2, -0x1

    .line 54
    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00()V

    .line 58
    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    const/4 v2, 0x1

    .line 74
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->o〇0OOo〇0(ZLjava/util/List;)V

    .line 75
    .line 76
    .line 77
    :goto_1
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final 〇〇O(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CSOcrResultBackPop"

    .line 7
    .line 8
    const-string p2, "confirm"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇oo8O80(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Landroid/view/View;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇〇0(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;II)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0888(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇〇〇00(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OOo00(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇〇〇OOO〇〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇〇O〇(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final O0〇O80ooo(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "inputOcrDataList"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$startAppendOcrImage$1;

    .line 11
    .line 12
    invoke-direct {v2, p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$startAppendOcrImage$1;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;Ljava/util/List;)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    const v3, 0x7f1300a3

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 12

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object p1, v0

    .line 17
    :goto_0
    const-string v1, "CSOcrResult"

    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    const-string v3, "BatchOcrResultFragment"

    .line 21
    .line 22
    const/4 v4, 0x1

    .line 23
    if-nez p1, :cond_1

    .line 24
    .line 25
    goto/16 :goto_1

    .line 26
    .line 27
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    const v6, 0x7f0a0285

    .line 32
    .line 33
    .line 34
    if-ne v5, v6, :cond_7

    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇8O0O808〇()Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    new-instance v5, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string v6, "select_all:"

    .line 50
    .line 51
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇8O0O808〇()Z

    .line 69
    .line 70
    .line 71
    move-result p1

    .line 72
    if-eqz p1, :cond_4

    .line 73
    .line 74
    const-string p1, "cancel_select_all_pop"

    .line 75
    .line 76
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    iget p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 80
    .line 81
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo0O(IZ)V

    .line 82
    .line 83
    .line 84
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->OoOOo8(Z)V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    if-eqz p1, :cond_2

    .line 96
    .line 97
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 98
    .line 99
    :cond_2
    if-nez v0, :cond_3

    .line 100
    .line 101
    goto/16 :goto_12

    .line 102
    .line 103
    :cond_3
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 104
    .line 105
    .line 106
    goto/16 :goto_12

    .line 107
    .line 108
    :cond_4
    new-array p1, v4, [Landroid/util/Pair;

    .line 109
    .line 110
    new-instance v3, Landroid/util/Pair;

    .line 111
    .line 112
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 113
    .line 114
    .line 115
    move-result-object v5

    .line 116
    invoke-virtual {v5, v2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇〇0〇0o8(Z)Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v5

    .line 120
    const-string v6, "txt_num"

    .line 121
    .line 122
    invoke-direct {v3, v6, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 123
    .line 124
    .line 125
    aput-object v3, p1, v2

    .line 126
    .line 127
    const-string v3, "select_all_pop"

    .line 128
    .line 129
    invoke-static {v1, v3, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 130
    .line 131
    .line 132
    iget p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 133
    .line 134
    invoke-direct {p0, p1, v4}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oo0O(IZ)V

    .line 135
    .line 136
    .line 137
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 138
    .line 139
    .line 140
    move-result-object p1

    .line 141
    invoke-virtual {p1, v4}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->OoOOo8(Z)V

    .line 142
    .line 143
    .line 144
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 145
    .line 146
    .line 147
    move-result-object p1

    .line 148
    if-eqz p1, :cond_5

    .line 149
    .line 150
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/EditViewDividerMultiLine;

    .line 151
    .line 152
    :cond_5
    if-nez v0, :cond_6

    .line 153
    .line 154
    goto/16 :goto_12

    .line 155
    .line 156
    :cond_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 157
    .line 158
    .line 159
    goto/16 :goto_12

    .line 160
    .line 161
    :cond_7
    :goto_1
    if-nez p1, :cond_8

    .line 162
    .line 163
    goto :goto_2

    .line 164
    :cond_8
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 165
    .line 166
    .line 167
    move-result v5

    .line 168
    const v6, 0x7f0a18ce

    .line 169
    .line 170
    .line 171
    if-ne v5, v6, :cond_9

    .line 172
    .line 173
    const-string p1, "select language"

    .line 174
    .line 175
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    const-string p1, "set_language"

    .line 179
    .line 180
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO0〇O(Ljava/lang/String;Landroid/util/Pair;)V

    .line 181
    .line 182
    .line 183
    const/16 p1, 0x65

    .line 184
    .line 185
    invoke-static {p0, v4, v0, v4, p1}, Lcom/intsig/camscanner/ocrapi/OcrIntent;->〇〇888(Landroidx/fragment/app/Fragment;ZLjava/lang/String;II)V

    .line 186
    .line 187
    .line 188
    goto/16 :goto_12

    .line 189
    .line 190
    :cond_9
    :goto_2
    if-nez p1, :cond_a

    .line 191
    .line 192
    goto :goto_3

    .line 193
    :cond_a
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 194
    .line 195
    .line 196
    move-result v5

    .line 197
    const v6, 0x7f0a1427

    .line 198
    .line 199
    .line 200
    if-ne v5, v6, :cond_b

    .line 201
    .line 202
    const-string p1, "click ll_feed_back"

    .line 203
    .line 204
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    const-string p1, "feedback"

    .line 208
    .line 209
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 213
    .line 214
    .line 215
    move-result-object p1

    .line 216
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 217
    .line 218
    .line 219
    move-result-object v1

    .line 220
    invoke-static {v1}, Lcom/intsig/camscanner/web/UrlUtil;->o800o8O(Landroid/content/Context;)Ljava/lang/String;

    .line 221
    .line 222
    .line 223
    move-result-object v1

    .line 224
    const-string v2, "CSOcrFeedBack"

    .line 225
    .line 226
    invoke-static {p1, v1, v2, v0}, Lcom/intsig/webview/util/WebUtil;->o800o8O(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/webview/data/WebArgs;)V

    .line 227
    .line 228
    .line 229
    goto/16 :goto_12

    .line 230
    .line 231
    :cond_b
    :goto_3
    const-string v0, "page_number_select"

    .line 232
    .line 233
    const-string v5, "type"

    .line 234
    .line 235
    if-nez p1, :cond_c

    .line 236
    .line 237
    goto :goto_4

    .line 238
    :cond_c
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 239
    .line 240
    .line 241
    move-result v6

    .line 242
    const v7, 0x7f0a09b2

    .line 243
    .line 244
    .line 245
    if-ne v6, v7, :cond_e

    .line 246
    .line 247
    iget p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 248
    .line 249
    sub-int/2addr p1, v4

    .line 250
    new-instance v6, Ljava/lang/StringBuilder;

    .line 251
    .line 252
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 253
    .line 254
    .line 255
    const-string v7, "previous_page:"

    .line 256
    .line 257
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    .line 259
    .line 260
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 261
    .line 262
    .line 263
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 264
    .line 265
    .line 266
    move-result-object v6

    .line 267
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    .line 269
    .line 270
    if-gez p1, :cond_d

    .line 271
    .line 272
    return-void

    .line 273
    :cond_d
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 274
    .line 275
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 276
    .line 277
    .line 278
    move-result-object v3

    .line 279
    invoke-virtual {v3, p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->ooo8o〇o〇(I)V

    .line 280
    .line 281
    .line 282
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00〇〇〇o〇8()V

    .line 283
    .line 284
    .line 285
    new-array p1, v4, [Landroid/util/Pair;

    .line 286
    .line 287
    new-instance v3, Landroid/util/Pair;

    .line 288
    .line 289
    iget v6, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 290
    .line 291
    add-int/2addr v6, v4

    .line 292
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 293
    .line 294
    .line 295
    move-result-object v4

    .line 296
    invoke-direct {v3, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 297
    .line 298
    .line 299
    aput-object v3, p1, v2

    .line 300
    .line 301
    invoke-static {v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 302
    .line 303
    .line 304
    goto/16 :goto_12

    .line 305
    .line 306
    :cond_e
    :goto_4
    if-nez p1, :cond_f

    .line 307
    .line 308
    goto :goto_5

    .line 309
    :cond_f
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 310
    .line 311
    .line 312
    move-result v6

    .line 313
    const v7, 0x7f0a09b1

    .line 314
    .line 315
    .line 316
    if-ne v6, v7, :cond_11

    .line 317
    .line 318
    iget p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 319
    .line 320
    add-int/2addr p1, v4

    .line 321
    new-instance v6, Ljava/lang/StringBuilder;

    .line 322
    .line 323
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 324
    .line 325
    .line 326
    const-string v7, "next_page:"

    .line 327
    .line 328
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    .line 330
    .line 331
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 332
    .line 333
    .line 334
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 335
    .line 336
    .line 337
    move-result-object v6

    .line 338
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    .line 340
    .line 341
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 342
    .line 343
    .line 344
    move-result-object v3

    .line 345
    invoke-virtual {v3}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->oo0O〇0〇〇〇()I

    .line 346
    .line 347
    .line 348
    move-result v3

    .line 349
    if-lt p1, v3, :cond_10

    .line 350
    .line 351
    return-void

    .line 352
    :cond_10
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 353
    .line 354
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 355
    .line 356
    .line 357
    move-result-object v3

    .line 358
    invoke-virtual {v3, p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->ooo8o〇o〇(I)V

    .line 359
    .line 360
    .line 361
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00〇〇〇o〇8()V

    .line 362
    .line 363
    .line 364
    new-array p1, v4, [Landroid/util/Pair;

    .line 365
    .line 366
    new-instance v3, Landroid/util/Pair;

    .line 367
    .line 368
    iget v6, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 369
    .line 370
    add-int/2addr v6, v4

    .line 371
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 372
    .line 373
    .line 374
    move-result-object v4

    .line 375
    invoke-direct {v3, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 376
    .line 377
    .line 378
    aput-object v3, p1, v2

    .line 379
    .line 380
    invoke-static {v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 381
    .line 382
    .line 383
    goto/16 :goto_12

    .line 384
    .line 385
    :cond_11
    :goto_5
    const/4 v0, 0x2

    .line 386
    if-nez p1, :cond_12

    .line 387
    .line 388
    goto :goto_6

    .line 389
    :cond_12
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 390
    .line 391
    .line 392
    move-result v6

    .line 393
    const v7, 0x7f0a07e8

    .line 394
    .line 395
    .line 396
    if-ne v6, v7, :cond_14

    .line 397
    .line 398
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8〇OO0〇0o:Z

    .line 399
    .line 400
    new-instance v6, Ljava/lang/StringBuilder;

    .line 401
    .line 402
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 403
    .line 404
    .line 405
    const-string v7, "itb_ocr_edit_result_save:"

    .line 406
    .line 407
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    .line 409
    .line 410
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 411
    .line 412
    .line 413
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 414
    .line 415
    .line 416
    move-result-object p1

    .line 417
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    .line 419
    .line 420
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 421
    .line 422
    if-eqz p1, :cond_13

    .line 423
    .line 424
    new-array v0, v0, [Landroid/util/Pair;

    .line 425
    .line 426
    new-instance v3, Landroid/util/Pair;

    .line 427
    .line 428
    const-string v6, "from_part"

    .line 429
    .line 430
    iget-object p1, p1, Lcom/intsig/camscanner/mode_ocr/PageFromType;->pageFromPoint:Ljava/lang/String;

    .line 431
    .line 432
    invoke-direct {v3, v6, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 433
    .line 434
    .line 435
    aput-object v3, v0, v2

    .line 436
    .line 437
    new-instance p1, Landroid/util/Pair;

    .line 438
    .line 439
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo8888()Ljava/lang/String;

    .line 440
    .line 441
    .line 442
    move-result-object v2

    .line 443
    invoke-direct {p1, v5, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 444
    .line 445
    .line 446
    aput-object p1, v0, v4

    .line 447
    .line 448
    const-string p1, "save"

    .line 449
    .line 450
    invoke-static {v1, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 451
    .line 452
    .line 453
    :cond_13
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇o8()V

    .line 454
    .line 455
    .line 456
    goto/16 :goto_12

    .line 457
    .line 458
    :cond_14
    :goto_6
    if-nez p1, :cond_15

    .line 459
    .line 460
    goto :goto_7

    .line 461
    :cond_15
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 462
    .line 463
    .line 464
    move-result v6

    .line 465
    const v7, 0x7f0a07fa

    .line 466
    .line 467
    .line 468
    if-ne v6, v7, :cond_16

    .line 469
    .line 470
    const-string p1, "copyTxt"

    .line 471
    .line 472
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    .line 474
    .line 475
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8o0o8()V

    .line 476
    .line 477
    .line 478
    goto/16 :goto_12

    .line 479
    .line 480
    :cond_16
    :goto_7
    if-nez p1, :cond_17

    .line 481
    .line 482
    goto :goto_8

    .line 483
    :cond_17
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 484
    .line 485
    .line 486
    move-result v6

    .line 487
    const v7, 0x7f0a07d5

    .line 488
    .line 489
    .line 490
    if-ne v6, v7, :cond_18

    .line 491
    .line 492
    const-string p1, "exportOcr"

    .line 493
    .line 494
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    .line 496
    .line 497
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8o:Lcom/intsig/camscanner/tools/OcrTextShareClient;

    .line 498
    .line 499
    if-eqz p1, :cond_2e

    .line 500
    .line 501
    invoke-virtual {p1, v4}, Lcom/intsig/camscanner/tools/OcrTextShareClient;->oO80(Z)V

    .line 502
    .line 503
    .line 504
    goto/16 :goto_12

    .line 505
    .line 506
    :cond_18
    :goto_8
    if-nez p1, :cond_19

    .line 507
    .line 508
    goto :goto_9

    .line 509
    :cond_19
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 510
    .line 511
    .line 512
    move-result v6

    .line 513
    const v7, 0x7f0a0813

    .line 514
    .line 515
    .line 516
    if-ne v6, v7, :cond_1a

    .line 517
    .line 518
    const-string p1, "translationDeal"

    .line 519
    .line 520
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    .line 522
    .line 523
    const-string p1, "CSOcrMore"

    .line 524
    .line 525
    const-string v0, "translate"

    .line 526
    .line 527
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    .line 529
    .line 530
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o00o0O〇〇o()V

    .line 531
    .line 532
    .line 533
    goto/16 :goto_12

    .line 534
    .line 535
    :cond_1a
    :goto_9
    if-nez p1, :cond_1b

    .line 536
    .line 537
    goto :goto_a

    .line 538
    :cond_1b
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 539
    .line 540
    .line 541
    move-result v6

    .line 542
    const v7, 0x7f0a1544

    .line 543
    .line 544
    .line 545
    if-ne v6, v7, :cond_1c

    .line 546
    .line 547
    const-string p1, "showCancelDialog"

    .line 548
    .line 549
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    .line 551
    .line 552
    const-string p1, "cancel_to_edit"

    .line 553
    .line 554
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    .line 556
    .line 557
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o0OoOOo0:I

    .line 558
    .line 559
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8〇〇8〇8()V

    .line 560
    .line 561
    .line 562
    goto/16 :goto_12

    .line 563
    .line 564
    :cond_1c
    :goto_a
    if-nez p1, :cond_1d

    .line 565
    .line 566
    goto :goto_c

    .line 567
    :cond_1d
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 568
    .line 569
    .line 570
    move-result v6

    .line 571
    const v7, 0x7f0a1545

    .line 572
    .line 573
    .line 574
    if-ne v6, v7, :cond_20

    .line 575
    .line 576
    const-string p1, "tv_keyboard_finish"

    .line 577
    .line 578
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    .line 580
    .line 581
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 582
    .line 583
    .line 584
    move-result-object p1

    .line 585
    iget-boolean v5, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08〇o0O:Z

    .line 586
    .line 587
    if-eqz v5, :cond_1e

    .line 588
    .line 589
    const-string v5, "1"

    .line 590
    .line 591
    goto :goto_b

    .line 592
    :cond_1e
    const-string v5, "0"

    .line 593
    .line 594
    :goto_b
    const/4 v6, 0x3

    .line 595
    new-array v6, v6, [Landroid/util/Pair;

    .line 596
    .line 597
    new-instance v7, Landroid/util/Pair;

    .line 598
    .line 599
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 600
    .line 601
    .line 602
    move-result-object v8

    .line 603
    invoke-virtual {v8, v2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇008〇oo(Z)Ljava/lang/String;

    .line 604
    .line 605
    .line 606
    move-result-object v8

    .line 607
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    .line 608
    .line 609
    .line 610
    move-result v8

    .line 611
    invoke-virtual {p1, v8}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->〇00(I)Ljava/lang/String;

    .line 612
    .line 613
    .line 614
    move-result-object v8

    .line 615
    const-string v9, "txt_num_gap"

    .line 616
    .line 617
    invoke-direct {v7, v9, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 618
    .line 619
    .line 620
    aput-object v7, v6, v2

    .line 621
    .line 622
    new-instance v7, Landroid/util/Pair;

    .line 623
    .line 624
    const-string v8, "if_edit"

    .line 625
    .line 626
    invoke-direct {v7, v8, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 627
    .line 628
    .line 629
    aput-object v7, v6, v4

    .line 630
    .line 631
    new-instance v5, Landroid/util/Pair;

    .line 632
    .line 633
    const-string v7, "length_of_stay"

    .line 634
    .line 635
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->oo88o8O()Ljava/lang/String;

    .line 636
    .line 637
    .line 638
    move-result-object p1

    .line 639
    invoke-direct {v5, v7, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 640
    .line 641
    .line 642
    aput-object v5, v6, v0

    .line 643
    .line 644
    const-string p1, "complete"

    .line 645
    .line 646
    invoke-static {v1, p1, v6}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 647
    .line 648
    .line 649
    iput v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o0OoOOo0:I

    .line 650
    .line 651
    iget-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08〇o0O:Z

    .line 652
    .line 653
    if-eqz p1, :cond_1f

    .line 654
    .line 655
    const-string p1, "keyboard_finish => saveEditData "

    .line 656
    .line 657
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    .line 659
    .line 660
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 661
    .line 662
    .line 663
    move-result-object p1

    .line 664
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O88o〇()V

    .line 665
    .line 666
    .line 667
    iput-boolean v2, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇08〇o0O:Z

    .line 668
    .line 669
    :cond_1f
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 670
    .line 671
    invoke-static {p1}, Lcom/intsig/utils/SoftKeyboardUtils;->〇080(Landroid/app/Activity;)V

    .line 672
    .line 673
    .line 674
    goto/16 :goto_12

    .line 675
    .line 676
    :cond_20
    :goto_c
    const v0, 0x7f060207

    .line 677
    .line 678
    .line 679
    const v6, 0x7f13057e

    .line 680
    .line 681
    .line 682
    const v7, 0x7f131e36

    .line 683
    .line 684
    .line 685
    const v8, 0x7f13030d

    .line 686
    .line 687
    .line 688
    const v9, 0x7f131d10

    .line 689
    .line 690
    .line 691
    if-nez p1, :cond_21

    .line 692
    .line 693
    goto :goto_d

    .line 694
    :cond_21
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 695
    .line 696
    .line 697
    move-result v10

    .line 698
    const v11, 0x7f0a12f9

    .line 699
    .line 700
    .line 701
    if-ne v10, v11, :cond_22

    .line 702
    .line 703
    const-string p1, "tv_click_ocr_recognize"

    .line 704
    .line 705
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    .line 707
    .line 708
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 709
    .line 710
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 711
    .line 712
    invoke-direct {p1, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 713
    .line 714
    .line 715
    invoke-virtual {p1, v9}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 716
    .line 717
    .line 718
    move-result-object p1

    .line 719
    invoke-virtual {p1, v8}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 720
    .line 721
    .line 722
    move-result-object p1

    .line 723
    new-instance v1, L〇0O〇O00O/〇oo〇;

    .line 724
    .line 725
    invoke-direct {v1, p0}, L〇0O〇O00O/〇oo〇;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 726
    .line 727
    .line 728
    invoke-virtual {p1, v7, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 729
    .line 730
    .line 731
    move-result-object p1

    .line 732
    new-instance v1, L〇0O〇O00O/〇00;

    .line 733
    .line 734
    invoke-direct {v1}, L〇0O〇O00O/〇00;-><init>()V

    .line 735
    .line 736
    .line 737
    invoke-virtual {p1, v6, v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 738
    .line 739
    .line 740
    move-result-object p1

    .line 741
    invoke-virtual {p1, v4}, Lcom/intsig/app/AlertDialog$Builder;->〇〇888(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 742
    .line 743
    .line 744
    move-result-object p1

    .line 745
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 746
    .line 747
    .line 748
    move-result-object p1

    .line 749
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 750
    .line 751
    .line 752
    goto/16 :goto_12

    .line 753
    .line 754
    :cond_22
    :goto_d
    if-nez p1, :cond_23

    .line 755
    .line 756
    goto :goto_e

    .line 757
    :cond_23
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 758
    .line 759
    .line 760
    move-result v10

    .line 761
    const v11, 0x7f0a07f4

    .line 762
    .line 763
    .line 764
    if-ne v10, v11, :cond_24

    .line 765
    .line 766
    const-string p1, "itb_re_cloud_ocr"

    .line 767
    .line 768
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    .line 770
    .line 771
    const-string p1, "ocr_anew"

    .line 772
    .line 773
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    .line 775
    .line 776
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 777
    .line 778
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 779
    .line 780
    invoke-direct {p1, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 781
    .line 782
    .line 783
    invoke-virtual {p1, v9}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 784
    .line 785
    .line 786
    move-result-object p1

    .line 787
    invoke-virtual {p1, v8}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 788
    .line 789
    .line 790
    move-result-object p1

    .line 791
    new-instance v1, L〇0O〇O00O/O〇8O8〇008;

    .line 792
    .line 793
    invoke-direct {v1, p0}, L〇0O〇O00O/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 794
    .line 795
    .line 796
    invoke-virtual {p1, v7, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 797
    .line 798
    .line 799
    move-result-object p1

    .line 800
    new-instance v1, L〇0O〇O00O/O8ooOoo〇;

    .line 801
    .line 802
    invoke-direct {v1}, L〇0O〇O00O/O8ooOoo〇;-><init>()V

    .line 803
    .line 804
    .line 805
    invoke-virtual {p1, v6, v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 806
    .line 807
    .line 808
    move-result-object p1

    .line 809
    invoke-virtual {p1, v4}, Lcom/intsig/app/AlertDialog$Builder;->〇〇888(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 810
    .line 811
    .line 812
    move-result-object p1

    .line 813
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 814
    .line 815
    .line 816
    move-result-object p1

    .line 817
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 818
    .line 819
    .line 820
    goto/16 :goto_12

    .line 821
    .line 822
    :cond_24
    :goto_e
    if-nez p1, :cond_25

    .line 823
    .line 824
    goto :goto_10

    .line 825
    :cond_25
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 826
    .line 827
    .line 828
    move-result v0

    .line 829
    const v6, 0x7f0a1622

    .line 830
    .line 831
    .line 832
    if-ne v0, v6, :cond_27

    .line 833
    .line 834
    const-string p1, "click ocr auto wrap"

    .line 835
    .line 836
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    .line 838
    .line 839
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00o〇O8()Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;

    .line 840
    .line 841
    .line 842
    move-result-object p1

    .line 843
    if-eqz p1, :cond_2e

    .line 844
    .line 845
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentBatchOcrResultBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 846
    .line 847
    if-eqz p1, :cond_2e

    .line 848
    .line 849
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    .line 850
    .line 851
    .line 852
    move-result p1

    .line 853
    xor-int/lit8 v0, p1, 0x1

    .line 854
    .line 855
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oooo800〇〇(Z)V

    .line 856
    .line 857
    .line 858
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 859
    .line 860
    .line 861
    move-result-object v0

    .line 862
    xor-int/lit8 v2, p1, 0x1

    .line 863
    .line 864
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->〇008〇o0〇〇(Z)V

    .line 865
    .line 866
    .line 867
    if-nez p1, :cond_26

    .line 868
    .line 869
    const-string p1, "open"

    .line 870
    .line 871
    goto :goto_f

    .line 872
    :cond_26
    const-string p1, "close"

    .line 873
    .line 874
    :goto_f
    const-string v0, "auto_segment"

    .line 875
    .line 876
    invoke-static {v1, v0, v5, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    .line 878
    .line 879
    goto :goto_12

    .line 880
    :cond_27
    :goto_10
    if-nez p1, :cond_28

    .line 881
    .line 882
    goto :goto_12

    .line 883
    :cond_28
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 884
    .line 885
    .line 886
    move-result p1

    .line 887
    const v0, 0x7f0a080c

    .line 888
    .line 889
    .line 890
    if-ne p1, v0, :cond_2e

    .line 891
    .line 892
    const-string p1, "click itb_to_word"

    .line 893
    .line 894
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    .line 896
    .line 897
    const-string p1, "transfer_word"

    .line 898
    .line 899
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    .line 901
    .line 902
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO〇O0O()Z

    .line 903
    .line 904
    .line 905
    move-result p1

    .line 906
    if-eqz p1, :cond_29

    .line 907
    .line 908
    return-void

    .line 909
    :cond_29
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 910
    .line 911
    iget-object p1, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 912
    .line 913
    if-eqz p1, :cond_2a

    .line 914
    .line 915
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 916
    .line 917
    .line 918
    move-result p1

    .line 919
    if-nez p1, :cond_2b

    .line 920
    .line 921
    :cond_2a
    const/4 v2, 0x1

    .line 922
    :cond_2b
    if-eqz v2, :cond_2c

    .line 923
    .line 924
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 925
    .line 926
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 927
    .line 928
    iget-wide v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 929
    .line 930
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    .line 931
    .line 932
    .line 933
    move-result-object p1

    .line 934
    goto :goto_11

    .line 935
    :cond_2c
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 936
    .line 937
    iget-object p1, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 938
    .line 939
    :goto_11
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 940
    .line 941
    .line 942
    move-result-object v0

    .line 943
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O〇〇()Ljava/util/ArrayList;

    .line 944
    .line 945
    .line 946
    move-result-object v0

    .line 947
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 948
    .line 949
    .line 950
    move-result v1

    .line 951
    if-eqz v1, :cond_2d

    .line 952
    .line 953
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 954
    .line 955
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 956
    .line 957
    iget-wide v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 958
    .line 959
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->o8oO〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 960
    .line 961
    .line 962
    move-result-object v0

    .line 963
    :cond_2d
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooo0〇〇O:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 964
    .line 965
    iget-wide v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 966
    .line 967
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo0oO0(Ljava/lang/String;JLjava/util/ArrayList;)V

    .line 968
    .line 969
    .line 970
    :cond_2e
    :goto_12
    return-void
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public enableToolbar()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    const-string p1, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v0, "onCreate"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇OO〇00〇0O:Landroid/content/res/Configuration;

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8ooo()V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o〇o0oOO8()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OCRFrameViewModel;->〇00(Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->OOo88OOo()V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8o0()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oooO8〇00()V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇0()V

    .line 42
    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇8O0O80〇()V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o〇OO80oO()V

    .line 48
    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o8〇8()V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 54
    .line 55
    if-eqz p1, :cond_0

    .line 56
    .line 57
    iget-object p1, p1, Lcom/intsig/camscanner/mode_ocr/PageFromType;->pageFromPoint:Ljava/lang/String;

    .line 58
    .line 59
    const-string v0, "type"

    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo8888()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    const-string v2, "CSOcrResult"

    .line 66
    .line 67
    const-string v3, "from_part"

    .line 68
    .line 69
    invoke-static {v2, v3, p1, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    :cond_0
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public interceptBackPressed()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->onNavigationClick()Z

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0〇〇00〇o()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO00〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    new-instance p3, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v0, "onActivityResult requestCode="

    .line 10
    .line 11
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v0, "requestCode="

    .line 18
    .line 19
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p3

    .line 29
    const-string v0, "BatchOcrResultFragment"

    .line 30
    .line 31
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/16 p3, 0x68

    .line 35
    .line 36
    if-ne p3, p1, :cond_0

    .line 37
    .line 38
    const/4 p1, 0x1

    .line 39
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8O(Z)V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo〇〇〇〇()V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    const/16 p3, 0x65

    .line 47
    .line 48
    if-ne p3, p1, :cond_2

    .line 49
    .line 50
    const/4 p1, -0x1

    .line 51
    if-ne p2, p1, :cond_1

    .line 52
    .line 53
    const-string p1, "ocr_recognize_again"

    .line 54
    .line 55
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8o80O(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    const/4 p1, 0x0

    .line 59
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8O(Z)V

    .line 60
    .line 61
    .line 62
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->Oo〇〇〇〇()V

    .line 63
    .line 64
    .line 65
    :cond_2
    :goto_0
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "newConfig"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇OO〇00〇0O:Landroid/content/res/Configuration;

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget v0, v0, Landroid/content/res/Configuration;->screenHeightDp:I

    .line 15
    .line 16
    iget v1, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    .line 17
    .line 18
    if-ne v0, v1, :cond_2

    .line 19
    .line 20
    iget v1, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    .line 21
    .line 22
    if-eq v0, v1, :cond_1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/4 v0, 0x0

    .line 26
    goto :goto_1

    .line 27
    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 28
    :goto_1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇OO〇00〇0O:Landroid/content/res/Configuration;

    .line 29
    .line 30
    new-instance p1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v1, "onConfigurationChanged: isSizeChanged: "

    .line 36
    .line 37
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    const-string v1, "BatchOcrResultFragment"

    .line 48
    .line 49
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    if-eqz v0, :cond_3

    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇〇O()V

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo0:Lcom/intsig/camscanner/mode_ocr/adapter/OcrResultImgAdapter;

    .line 58
    .line 59
    if-eqz p1, :cond_3

    .line 60
    .line 61
    invoke-virtual {p1}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    .line 62
    .line 63
    .line 64
    :cond_3
    return-void
    .line 65
    .line 66
    .line 67
.end method

.method public onDestroyView()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->ooO:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O088O()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataDealViewModel;->Oooo8o0〇()V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onNavigationClick()Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->O〇08oOOO0:Z

    .line 2
    .line 3
    if-nez v0, :cond_3

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 6
    .line 7
    sget-object v1, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_SINGLE_CAPTURE_OCR:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 8
    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8〇oO〇〇8o:Z

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8o:Lcom/intsig/camscanner/tools/OcrTextShareClient;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇80〇808〇O()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0Oo〇8()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$SaveOcrResultDialog;

    .line 36
    .line 37
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment$SaveOcrResultDialog;-><init>()V

    .line 38
    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 41
    .line 42
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const-string v2, "BatchOcrResultFragment"

    .line 47
    .line 48
    invoke-virtual {v0, v1, v2}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8〇OO0〇0o:Z

    .line 53
    .line 54
    if-nez v0, :cond_2

    .line 55
    .line 56
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->o08oOO()Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    if-eqz v0, :cond_2

    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0o88O()V

    .line 67
    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇0O8Oo()V

    .line 71
    .line 72
    .line 73
    const-string v0, "back"

    .line 74
    .line 75
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8o80O(Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇80O80O〇0()V

    .line 80
    .line 81
    .line 82
    :goto_1
    const/4 v0, 0x1

    .line 83
    return v0
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public onResume()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇08O:Ljava/lang/String;

    .line 5
    .line 6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x0

    .line 11
    const/4 v2, 0x1

    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    const-string v0, "action_open_word_share"

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇08O:Ljava/lang/String;

    .line 17
    .line 18
    invoke-static {v0, v3, v2}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8o:Lcom/intsig/camscanner/tools/OcrTextShareClient;

    .line 25
    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tools/OcrTextShareClient;->〇8o8o〇(Z)V

    .line 29
    .line 30
    .line 31
    :cond_0
    const/4 v0, 0x0

    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇〇08O:Ljava/lang/String;

    .line 33
    .line 34
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 35
    .line 36
    if-eqz v0, :cond_3

    .line 37
    .line 38
    iget-boolean v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->o8oOOo:Z

    .line 39
    .line 40
    if-nez v3, :cond_2

    .line 41
    .line 42
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    if-eqz v3, :cond_2

    .line 47
    .line 48
    const/4 v1, 0x1

    .line 49
    :cond_2
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 50
    .line 51
    .line 52
    :cond_3
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final oo8〇〇()Lorg/json/JSONObject;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 2
    .line 3
    const-string v1, "BatchOcrResultFragment"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    const-string v0, "mPageFromType == null"

    .line 9
    .line 10
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-object v2

    .line 14
    :cond_0
    if-eqz v0, :cond_1

    .line 15
    .line 16
    iget-object v0, v0, Lcom/intsig/camscanner/mode_ocr/PageFromType;->pageFromPoint:Ljava/lang/String;

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    move-object v0, v2

    .line 20
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_4

    .line 25
    .line 26
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    .line 27
    .line 28
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v3, "from_part"

    .line 32
    .line 33
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 34
    .line 35
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 36
    .line 37
    .line 38
    iget-object v4, v4, Lcom/intsig/camscanner/mode_ocr/PageFromType;->pageFromPoint:Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 41
    .line 42
    .line 43
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 44
    .line 45
    sget-object v4, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_OCR_PREVIEW:Lcom/intsig/camscanner/mode_ocr/PageFromType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .line 47
    const-string v5, "from"

    .line 48
    .line 49
    if-ne v3, v4, :cond_2

    .line 50
    .line 51
    :try_start_1
    const-string v3, "batch"

    .line 52
    .line 53
    invoke-virtual {v0, v5, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 54
    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_2
    sget-object v4, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_SINGLE_CAPTURE_OCR:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 58
    .line 59
    if-ne v3, v4, :cond_3

    .line 60
    .line 61
    const-string v3, "single"

    .line 62
    .line 63
    invoke-virtual {v0, v5, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 64
    .line 65
    .line 66
    :cond_3
    :goto_1
    return-object v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    .line 70
    .line 71
    :cond_4
    return-object v2
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final ooo008(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->oO00〇o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d029e

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/utils/SoftKeyboardUtils;->〇080(Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0O8Oo()V
    .locals 4

    .line 1
    const-string v0, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v1, "closeActivity"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Landroid/content/Intent;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 11
    .line 12
    .line 13
    const-class v1, Lcom/intsig/camscanner/tools/OCRDataListHolder;

    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/singleton/Singleton;->〇080(Ljava/lang/Class;)Lcom/intsig/singleton/Singleton;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const-string v2, "null cannot be cast to non-null type com.intsig.camscanner.tools.OCRDataListHolder"

    .line 20
    .line 21
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    check-cast v1, Lcom/intsig/camscanner/tools/OCRDataListHolder;

    .line 25
    .line 26
    new-instance v2, Ljava/util/ArrayList;

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    invoke-virtual {v3}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tools/OCRDataListHolder;->〇o〇(Ljava/util/List;)V

    .line 40
    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 43
    .line 44
    const/4 v2, 0x0

    .line 45
    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇00()V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇80O(Z)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "BatchOcrResultFragment"

    .line 2
    .line 3
    const-string v1, "getTransWordImageSyncIds"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    new-instance v1, Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 16
    .line 17
    .line 18
    if-eqz p1, :cond_0

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇o0O:I

    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-ge p1, v2, :cond_1

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/fragment/BatchOcrResultFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OcrDataResultViewModel;->O8oOo80()Ljava/util/ArrayList;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    const-string v2, "mViewModel.inputOcrDataList[currentPosition]"

    .line 58
    .line 59
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 63
    .line 64
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    .line 66
    .line 67
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    if-eqz v1, :cond_3

    .line 76
    .line 77
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 82
    .line 83
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 88
    .line 89
    .line 90
    move-result v2

    .line 91
    if-eqz v2, :cond_2

    .line 92
    .line 93
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    :cond_2
    const-string v2, "inputImageSyncId"

    .line 98
    .line 99
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    goto :goto_1

    .line 106
    :cond_3
    return-object v0
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇O00()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;
.super Ljava/lang/Object;
.source "SinglePageOcrEdgeScanDialogCallback.kt"

# interfaces
.implements Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;-><init>(Landroidx/appcompat/app/AppCompatActivity;Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/LifecycleOwner;ILandroid/view/Window;Landroidx/appcompat/widget/Toolbar;Landroid/widget/ImageView;Lcom/intsig/camscanner/loadimage/RotateBitmap;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public 〇080(Ljava/lang/Object;)V
    .locals 5

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O〇()Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const-string v0, "SinglePageOcrEdgeScanDialogCallback"

    .line 8
    .line 9
    if-nez p1, :cond_0

    .line 10
    .line 11
    const-string p1, "onStart isShowProgressDialog false"

    .line 12
    .line 13
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 18
    .line 19
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o〇〇0〇(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)J

    .line 20
    .line 21
    .line 22
    move-result-wide v1

    .line 23
    const-wide/16 v3, 0x0

    .line 24
    .line 25
    cmp-long p1, v1, v3

    .line 26
    .line 27
    if-nez p1, :cond_1

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 30
    .line 31
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 32
    .line 33
    .line 34
    move-result-wide v1

    .line 35
    invoke-static {p1, v1, v2}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->O8〇o(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;J)V

    .line 36
    .line 37
    .line 38
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇〇8O0〇8()V

    .line 41
    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 44
    .line 45
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇00〇8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 46
    .line 47
    .line 48
    const-string p1, "onStart progress"

    .line 49
    .line 50
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇o00〇〇Oo(IIILjava/lang/Object;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public 〇o〇(Ljava/lang/Object;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oo〇()Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "onEnd progress isCancel="

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v0, "SinglePageOcrEdgeScanDialogCallback"

    .line 29
    .line 30
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oo〇()Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-eqz p1, :cond_0

    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 46
    .line 47
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o〇O8〇〇o(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 52
    .line 53
    const/4 v0, 0x1

    .line 54
    invoke-static {p1, v0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oo〇(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;Z)V

    .line 55
    .line 56
    .line 57
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O8o08O()Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    if-eqz p1, :cond_1

    .line 64
    .line 65
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->Oo08()V

    .line 66
    .line 67
    .line 68
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;->〇080:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇00()V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

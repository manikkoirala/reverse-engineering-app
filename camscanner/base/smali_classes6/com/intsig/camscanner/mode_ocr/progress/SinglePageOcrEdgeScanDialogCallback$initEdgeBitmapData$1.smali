.class public final Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;
.super Ljava/lang/Object;
.source "SinglePageOcrEdgeScanDialogCallback.kt"

# interfaces
.implements Landroidx/lifecycle/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇08O8o〇0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroidx/lifecycle/Observer<",
        "Lkotlin/Pair<",
        "+",
        "Landroid/graphics/Bitmap;",
        "+",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;->o0:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public bridge synthetic onChanged(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lkotlin/Pair;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;->〇080(Lkotlin/Pair;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080(Lkotlin/Pair;)V
    .locals 4
    .param p1    # Lkotlin/Pair;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "it"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroid/graphics/Bitmap;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const-string v1, "SinglePageOcrEdgeScanDialogCallback"

    .line 17
    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    const-string p1, "edgeBitmapData.observe first isRecycled"

    .line 21
    .line 22
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    const-string v0, "edgeBitmapData.observe startAnimation"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    const/4 v2, 0x1

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    check-cast v0, Landroid/graphics/Bitmap;

    .line 43
    .line 44
    const/4 v3, 0x0

    .line 45
    if-eqz v0, :cond_1

    .line 46
    .line 47
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-nez v0, :cond_1

    .line 52
    .line 53
    const/4 v0, 0x1

    .line 54
    goto :goto_0

    .line 55
    :cond_1
    const/4 v0, 0x0

    .line 56
    :goto_0
    if-eqz v0, :cond_2

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;->o0:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 59
    .line 60
    invoke-static {v0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->O〇8O8〇008(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    if-eqz v0, :cond_3

    .line 65
    .line 66
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    check-cast v1, Landroid/graphics/Bitmap;

    .line 71
    .line 72
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    check-cast p1, Landroid/graphics/Bitmap;

    .line 77
    .line 78
    invoke-virtual {v0, v3, v1, p1, v2}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setVisibility(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_2
    const-string p1, "edgeBitmapData second isRecycled"

    .line 83
    .line 84
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;->o0:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 88
    .line 89
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇oOO8O8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    if-eqz p1, :cond_4

    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;->o0:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 96
    .line 97
    invoke-static {v0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇00(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Landroidx/appcompat/app/AppCompatActivity;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    const v1, 0x7f01003c

    .line 102
    .line 103
    .line 104
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 109
    .line 110
    .line 111
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;->o0:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;

    .line 112
    .line 113
    invoke-static {p1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇oOO8O8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Landroid/view/View;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    if-eqz p1, :cond_5

    .line 118
    .line 119
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 120
    .line 121
    .line 122
    :cond_5
    return-void
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

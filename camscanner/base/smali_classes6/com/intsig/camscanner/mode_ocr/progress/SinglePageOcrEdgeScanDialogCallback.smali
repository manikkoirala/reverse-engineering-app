.class public final Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;
.super Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;
.source "SinglePageOcrEdgeScanDialogCallback.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final o〇O8〇〇o:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO0o〇〇:Landroidx/appcompat/widget/Toolbar;

.field private final OO0o〇〇〇〇0:Landroidx/lifecycle/LifecycleOwner;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OoO8:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oooo8o0〇:Landroid/widget/ImageView;

.field private o800o8O:Landroid/app/Dialog;

.field private final oO80:Landroidx/appcompat/app/AppCompatActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oo88o8O:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

.field private 〇0〇O0088o:J

.field private final 〇80〇808〇O:Landroidx/lifecycle/ViewModelStoreOwner;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8o8o〇:I

.field private final 〇O00:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O888o0o:Landroid/view/View;

.field private final 〇O8o08O:Landroid/view/Window;

.field private final 〇O〇:F

.field private 〇oo〇:Landroidx/appcompat/widget/Toolbar;

.field private final 〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

.field private 〇〇8O0〇8:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o〇O8〇〇o:Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/LifecycleOwner;ILandroid/view/Window;Landroidx/appcompat/widget/Toolbar;Landroid/widget/ImageView;Lcom/intsig/camscanner/loadimage/RotateBitmap;F)V
    .locals 1
    .param p1    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/lifecycle/ViewModelStoreOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "viewModelStoreOwner"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "lifecycleOwner"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "SinglePageOcrEdgeScanDialogCallback"

    .line 17
    .line 18
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oO80:Landroidx/appcompat/app/AppCompatActivity;

    .line 22
    .line 23
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇80〇808〇O:Landroidx/lifecycle/ViewModelStoreOwner;

    .line 24
    .line 25
    iput-object p3, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->OO0o〇〇〇〇0:Landroidx/lifecycle/LifecycleOwner;

    .line 26
    .line 27
    iput p4, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇8o8o〇:I

    .line 28
    .line 29
    iput-object p5, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O8o08O:Landroid/view/Window;

    .line 30
    .line 31
    iput-object p6, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->OO0o〇〇:Landroidx/appcompat/widget/Toolbar;

    .line 32
    .line 33
    iput-object p7, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->Oooo8o0〇:Landroid/widget/ImageView;

    .line 34
    .line 35
    iput-object p8, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 36
    .line 37
    iput p9, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O〇:F

    .line 38
    .line 39
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 40
    .line 41
    new-instance p2, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$ocrEdgeDataViewModel$2;

    .line 42
    .line 43
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$ocrEdgeDataViewModel$2;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 44
    .line 45
    .line 46
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O00:Lkotlin/Lazy;

    .line 51
    .line 52
    new-instance p1, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;

    .line 53
    .line 54
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$progressAnimationCallBack$1;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 55
    .line 56
    .line 57
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->OoO8:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

    .line 58
    .line 59
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 60
    .line 61
    .line 62
    move-result-object p2

    .line 63
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oOO8O8(Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
.end method

.method private static final O08000(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    const-string p1, "$this_run"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇0o()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCREdgeDataViewModel;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OCREdgeDataViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->OO0o〇〇〇〇0:Landroidx/lifecycle/LifecycleOwner;

    .line 15
    .line 16
    invoke-virtual {p1, p0}, Landroidx/lifecycle/LiveData;->removeObservers(Landroidx/lifecycle/LifecycleOwner;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O8ooOoo〇(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Lcom/intsig/camscanner/mode_ocr/viewmodel/OCREdgeDataViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇0o()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCREdgeDataViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O8〇o(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇0〇O0088o:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OOO〇O0(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Landroidx/lifecycle/ViewModelStoreOwner;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇80〇808〇O:Landroidx/lifecycle/ViewModelStoreOwner;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final Oo8Oo00oo(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oo88o8O:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const/16 v1, 0x8

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setVisibility(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    .line 15
    .line 16
    .line 17
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇〇0〇〇0()V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method public static synthetic OoO8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->O08000(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O〇8O8〇008(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oo88o8O:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o0ooO()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O888o0o:Landroid/view/View;

    .line 2
    .line 3
    const-string v1, "SinglePageOcrEdgeScanDialogCallback"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "adjustToolbarPosition ocrEdgeScanDialogRootView == null"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->OO0o〇〇:Landroidx/appcompat/widget/Toolbar;

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    const-string v0, "adjustToolbarPosition reToolbar == null"

    .line 18
    .line 19
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇oo〇:Landroidx/appcompat/widget/Toolbar;

    .line 24
    .line 25
    if-nez v2, :cond_2

    .line 26
    .line 27
    const-string v0, "adjustToolbarPosition toolbar == null"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_2
    const/4 v2, 0x2

    .line 34
    new-array v3, v2, [I

    .line 35
    .line 36
    fill-array-data v3, :array_0

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 40
    .line 41
    .line 42
    new-array v0, v2, [I

    .line 43
    .line 44
    fill-array-data v0, :array_1

    .line 45
    .line 46
    .line 47
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O888o0o:Landroid/view/View;

    .line 48
    .line 49
    if-eqz v2, :cond_3

    .line 50
    .line 51
    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 52
    .line 53
    .line 54
    :cond_3
    const/4 v2, 0x1

    .line 55
    aget v4, v3, v2

    .line 56
    .line 57
    aget v0, v0, v2

    .line 58
    .line 59
    sub-int/2addr v4, v0

    .line 60
    aput v4, v3, v2

    .line 61
    .line 62
    if-lez v4, :cond_4

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇oo〇:Landroidx/appcompat/widget/Toolbar;

    .line 65
    .line 66
    if-eqz v0, :cond_4

    .line 67
    .line 68
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 73
    .line 74
    .line 75
    move-result-object v5

    .line 76
    iget v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 77
    .line 78
    aget v7, v3, v2

    .line 79
    .line 80
    add-int/2addr v6, v7

    .line 81
    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 82
    .line 83
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    .line 84
    .line 85
    .line 86
    move-result v5

    .line 87
    aget v6, v3, v2

    .line 88
    .line 89
    add-int/2addr v4, v6

    .line 90
    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    .line 91
    .line 92
    .line 93
    move-result v6

    .line 94
    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    .line 95
    .line 96
    .line 97
    move-result v7

    .line 98
    invoke-virtual {v0, v5, v4, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 99
    .line 100
    .line 101
    :cond_4
    aget v0, v3, v2

    .line 102
    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    const-string v3, "adjustToolbarPosition topMargin == "

    .line 109
    .line 110
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    return-void

    .line 124
    nop

    .line 125
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o8()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O00(Z)V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oO80:Landroidx/appcompat/app/AppCompatActivity;

    .line 6
    .line 7
    new-instance v1, Lo0OoOOo0/O8;

    .line 8
    .line 9
    invoke-direct {v1, p0}, Lo0OoOOo0/O8;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->Oo8Oo00oo(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oO()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o800o8O:Landroid/app/Dialog;

    .line 2
    .line 3
    if-nez v0, :cond_7

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/app/NoChangingStatusBarDialog;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oO80:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    const v2, 0x7f1401d7

    .line 10
    .line 11
    .line 12
    invoke-direct {v0, v1, v2}, Lcom/intsig/app/NoChangingStatusBarDialog;-><init>(Landroid/content/Context;I)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O8o08O:Landroid/view/Window;

    .line 22
    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    invoke-virtual {v4}, Landroid/view/View;->getSystemUiVisibility()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v2}, Landroid/view/Window;->getStatusBarColor()I

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    invoke-virtual {v1, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2}, Landroid/view/Window;->getNavigationBarColor()I

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    invoke-virtual {v1, v2}, Landroid/view/Window;->setNavigationBarColor(I)V

    .line 52
    .line 53
    .line 54
    :cond_0
    const/4 v1, 0x0

    .line 55
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 56
    .line 57
    .line 58
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initOcrEdgeScanDialog$1$1$2;

    .line 59
    .line 60
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initOcrEdgeScanDialog$1$1$2;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o〇0OOo〇0()Landroid/view/View;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 71
    .line 72
    .line 73
    iput-object v2, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O888o0o:Landroid/view/View;

    .line 74
    .line 75
    if-eqz v2, :cond_1

    .line 76
    .line 77
    iget v3, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇8o8o〇:I

    .line 78
    .line 79
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 80
    .line 81
    .line 82
    :cond_1
    const/4 v3, 0x4

    .line 83
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 84
    .line 85
    .line 86
    const v3, 0x7f0a11df

    .line 87
    .line 88
    .line 89
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object v4

    .line 93
    check-cast v4, Landroidx/appcompat/widget/Toolbar;

    .line 94
    .line 95
    new-instance v5, Lo0OoOOo0/〇o00〇〇Oo;

    .line 96
    .line 97
    invoke-direct {v5, p0}, Lo0OoOOo0/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v4, v5}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    .line 102
    .line 103
    const v4, 0x7f0a0666

    .line 104
    .line 105
    .line 106
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object v4

    .line 110
    check-cast v4, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 111
    .line 112
    iput-object v4, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oo88o8O:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 113
    .line 114
    if-nez v4, :cond_2

    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_2
    const-wide/16 v5, 0xbb8

    .line 118
    .line 119
    invoke-virtual {v4, v5, v6}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setAnimDuration(J)V

    .line 120
    .line 121
    .line 122
    :goto_0
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oo88o8O:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 123
    .line 124
    if-nez v4, :cond_3

    .line 125
    .line 126
    goto :goto_1

    .line 127
    :cond_3
    invoke-virtual {v4, v1}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setMBgColor(I)V

    .line 128
    .line 129
    .line 130
    :goto_1
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    check-cast v1, Landroidx/appcompat/widget/Toolbar;

    .line 135
    .line 136
    iput-object v1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇oo〇:Landroidx/appcompat/widget/Toolbar;

    .line 137
    .line 138
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->OO0o〇〇:Landroidx/appcompat/widget/Toolbar;

    .line 139
    .line 140
    if-eqz v2, :cond_6

    .line 141
    .line 142
    if-nez v1, :cond_4

    .line 143
    .line 144
    goto :goto_2

    .line 145
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    .line 146
    .line 147
    .line 148
    move-result-object v3

    .line 149
    invoke-virtual {v1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 150
    .line 151
    .line 152
    :goto_2
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇oo〇:Landroidx/appcompat/widget/Toolbar;

    .line 153
    .line 154
    if-nez v1, :cond_5

    .line 155
    .line 156
    goto :goto_3

    .line 157
    :cond_5
    invoke-virtual {v2}, Landroidx/appcompat/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 162
    .line 163
    .line 164
    :cond_6
    :goto_3
    new-instance v1, Lo0OoOOo0/〇o〇;

    .line 165
    .line 166
    invoke-direct {v1, p0}, Lo0OoOOo0/〇o〇;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 167
    .line 168
    .line 169
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 170
    .line 171
    .line 172
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o800o8O:Landroid/app/Dialog;

    .line 173
    .line 174
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇08O8o〇0()V

    .line 175
    .line 176
    .line 177
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 178
    .line 179
    :cond_7
    return-void
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oo〇(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇8O0〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇0OOo〇0()Landroid/view/View;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oO80:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f0d06a8

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "from(activity).inflate(R\u2026.pnl_ocr_edge_scan, null)"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-object v0
    .line 21
.end method

.method private static final o〇8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8〇〇o(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇〇0〇(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇0〇O0088o:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇00(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oO80:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0000OOO(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Lcom/intsig/camscanner/loadimage/RotateBitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇00〇8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇8〇0〇o〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇08O8o〇0()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇0o()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCREdgeDataViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/viewmodel/OCREdgeDataViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->OO0o〇〇〇〇0:Landroidx/lifecycle/LifecycleOwner;

    .line 10
    .line 11
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;

    .line 12
    .line 13
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$initEdgeBitmapData$1;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o〇8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "$this_run"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "SinglePageOcrEdgeScanDialogCallback"

    .line 7
    .line 8
    const-string v0, "Navigation back"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇8o8o〇()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8〇0〇o〇O()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oO()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o800o8O:Landroid/app/Dialog;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v2, 0x1

    .line 14
    if-ne v0, v2, :cond_0

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    :cond_0
    if-eqz v1, :cond_2

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O888o0o:Landroid/view/View;

    .line 20
    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 v1, 0x4

    .line 25
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o800o8O:Landroid/app/Dialog;

    .line 30
    .line 31
    if-eqz v0, :cond_3

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    const-string v1, "SinglePageOcrEdgeScanDialogCallback"

    .line 39
    .line 40
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 41
    .line 42
    .line 43
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O888o0o:Landroid/view/View;

    .line 44
    .line 45
    if-eqz v0, :cond_4

    .line 46
    .line 47
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$showOcrEdgeScanDialog$1$1;

    .line 52
    .line 53
    invoke-direct {v2, v0, p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback$showOcrEdgeScanDialog$1$1;-><init>(Landroid/view/View;Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 60
    .line 61
    .line 62
    :cond_4
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇O888o0o(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->Oooo8o0〇:Landroid/widget/ImageView;

    .line 2
    .line 3
    const-string v1, "SinglePageOcrEdgeScanDialogCallback"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "adjustImageArea refImageView == null"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O888o0o:Landroid/view/View;

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    const-string v0, "adjustImageArea ocrEdgeScanDialogRootView == null"

    .line 18
    .line 19
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oo88o8O:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 24
    .line 25
    const-string v2, "adjustImageArea flushView == null"

    .line 26
    .line 27
    if-nez v0, :cond_2

    .line 28
    .line 29
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 34
    .line 35
    if-eqz v0, :cond_a

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    if-nez v0, :cond_3

    .line 42
    .line 43
    goto/16 :goto_3

    .line 44
    .line 45
    :cond_3
    const/4 v0, 0x2

    .line 46
    new-array v2, v0, [I

    .line 47
    .line 48
    fill-array-data v2, :array_0

    .line 49
    .line 50
    .line 51
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->Oooo8o0〇:Landroid/widget/ImageView;

    .line 52
    .line 53
    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 54
    .line 55
    .line 56
    new-array v3, v0, [I

    .line 57
    .line 58
    fill-array-data v3, :array_1

    .line 59
    .line 60
    .line 61
    iget-object v4, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O888o0o:Landroid/view/View;

    .line 62
    .line 63
    if-eqz v4, :cond_4

    .line 64
    .line 65
    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 66
    .line 67
    .line 68
    :cond_4
    const/4 v4, 0x0

    .line 69
    aget v5, v2, v4

    .line 70
    .line 71
    aget v6, v3, v4

    .line 72
    .line 73
    sub-int/2addr v5, v6

    .line 74
    aput v5, v2, v4

    .line 75
    .line 76
    const/4 v4, 0x1

    .line 77
    aget v5, v2, v4

    .line 78
    .line 79
    aget v3, v3, v4

    .line 80
    .line 81
    sub-int/2addr v5, v3

    .line 82
    aput v5, v2, v4

    .line 83
    .line 84
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->Oooo8o0〇:Landroid/widget/ImageView;

    .line 85
    .line 86
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    int-to-float v3, v3

    .line 91
    iget v5, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O〇:F

    .line 92
    .line 93
    sub-float/2addr v3, v5

    .line 94
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->Oooo8o0〇:Landroid/widget/ImageView;

    .line 95
    .line 96
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    .line 97
    .line 98
    .line 99
    move-result v5

    .line 100
    int-to-float v5, v5

    .line 101
    iget v6, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O〇:F

    .line 102
    .line 103
    sub-float/2addr v5, v6

    .line 104
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 105
    .line 106
    invoke-virtual {v6}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 107
    .line 108
    .line 109
    move-result v6

    .line 110
    rem-int/lit16 v6, v6, 0xb4

    .line 111
    .line 112
    if-nez v6, :cond_5

    .line 113
    .line 114
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 115
    .line 116
    invoke-virtual {v6}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 117
    .line 118
    .line 119
    move-result-object v6

    .line 120
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    .line 121
    .line 122
    .line 123
    move-result v6

    .line 124
    iget-object v7, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 125
    .line 126
    invoke-virtual {v7}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 127
    .line 128
    .line 129
    move-result-object v7

    .line 130
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    .line 131
    .line 132
    .line 133
    move-result v7

    .line 134
    goto :goto_0

    .line 135
    :cond_5
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 136
    .line 137
    invoke-virtual {v6}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 138
    .line 139
    .line 140
    move-result-object v6

    .line 141
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    .line 142
    .line 143
    .line 144
    move-result v6

    .line 145
    iget-object v7, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇808〇:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 146
    .line 147
    invoke-virtual {v7}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 148
    .line 149
    .line 150
    move-result-object v7

    .line 151
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    .line 152
    .line 153
    .line 154
    move-result v7

    .line 155
    :goto_0
    int-to-float v6, v6

    .line 156
    div-float/2addr v3, v6

    .line 157
    const/high16 v8, 0x40400000    # 3.0f

    .line 158
    .line 159
    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    .line 160
    .line 161
    .line 162
    move-result v3

    .line 163
    int-to-float v7, v7

    .line 164
    div-float/2addr v5, v7

    .line 165
    invoke-static {v5, v8}, Ljava/lang/Math;->min(FF)F

    .line 166
    .line 167
    .line 168
    move-result v5

    .line 169
    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    .line 170
    .line 171
    .line 172
    move-result v3

    .line 173
    mul-float v6, v6, v3

    .line 174
    .line 175
    mul-float v7, v7, v3

    .line 176
    .line 177
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oo88o8O:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 178
    .line 179
    if-eqz v3, :cond_6

    .line 180
    .line 181
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 182
    .line 183
    .line 184
    move-result-object v3

    .line 185
    goto :goto_1

    .line 186
    :cond_6
    const/4 v3, 0x0

    .line 187
    :goto_1
    if-nez v3, :cond_7

    .line 188
    .line 189
    return-void

    .line 190
    :cond_7
    float-to-int v5, v7

    .line 191
    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 192
    .line 193
    instance-of v5, v3, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 194
    .line 195
    if-eqz v5, :cond_8

    .line 196
    .line 197
    move-object v5, v3

    .line 198
    check-cast v5, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 199
    .line 200
    aget v4, v2, v4

    .line 201
    .line 202
    iget-object v8, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->Oooo8o0〇:Landroid/widget/ImageView;

    .line 203
    .line 204
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    .line 205
    .line 206
    .line 207
    move-result v8

    .line 208
    int-to-float v8, v8

    .line 209
    sub-float/2addr v8, v7

    .line 210
    int-to-float v0, v0

    .line 211
    div-float/2addr v8, v0

    .line 212
    float-to-int v0, v8

    .line 213
    add-int/2addr v4, v0

    .line 214
    iput v4, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 215
    .line 216
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->oo88o8O:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 217
    .line 218
    if-nez v0, :cond_9

    .line 219
    .line 220
    goto :goto_2

    .line 221
    :cond_9
    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 222
    .line 223
    .line 224
    :goto_2
    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 225
    .line 226
    .line 227
    move-result-object v0

    .line 228
    const-string v2, "toString(this)"

    .line 229
    .line 230
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    .line 232
    .line 233
    new-instance v2, Ljava/lang/StringBuilder;

    .line 234
    .line 235
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 236
    .line 237
    .line 238
    const-string v3, "adjustImageArea adjustImageArea:"

    .line 239
    .line 240
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    .line 242
    .line 243
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    .line 245
    .line 246
    const-string v0, ", with:"

    .line 247
    .line 248
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    .line 250
    .line 251
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 252
    .line 253
    .line 254
    const-string v0, ", height:"

    .line 255
    .line 256
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 260
    .line 261
    .line 262
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 263
    .line 264
    .line 265
    move-result-object v0

    .line 266
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    .line 268
    .line 269
    return-void

    .line 270
    :cond_a
    :goto_3
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    return-void

    .line 274
    nop

    .line 275
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static final synthetic 〇oOO8O8(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O888o0o:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇oo〇(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o0ooO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇0o()Lcom/intsig/camscanner/mode_ocr/viewmodel/OCREdgeDataViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇O00:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/viewmodel/OCREdgeDataViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇〇0〇〇0()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o800o8O:Landroid/app/Dialog;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    :cond_0
    if-eqz v1, :cond_1

    .line 14
    .line 15
    return-void

    .line 16
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o800o8O:Landroid/app/Dialog;

    .line 17
    .line 18
    if-eqz v0, :cond_2

    .line 19
    .line 20
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 21
    .line 22
    .line 23
    :cond_2
    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o800o8O:Landroid/app/Dialog;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    const-string v1, "SinglePageOcrEdgeScanDialogCallback"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    .line 32
    .line 33
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;->〇〇888:Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount$Companion;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount$Companion;->〇080()Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 40
    .line 41
    .line 42
    move-result-wide v2

    .line 43
    iget-wide v4, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇0〇O0088o:J

    .line 44
    .line 45
    sub-long/2addr v2, v4

    .line 46
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;->o〇0(J)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount$Companion;->〇080()Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;->〇o00〇〇Oo()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O8()Z
    .locals 2

    .line 1
    const-string v0, "SinglePageOcrEdgeScanDialogCallback"

    .line 2
    .line 3
    const-string v1, "dismissOnAnimation"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o800o8O()V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public dismiss()V
    .locals 4

    .line 1
    const-string v0, "SinglePageOcrEdgeScanDialogCallback"

    .line 2
    .line 3
    const-string v1, "dismiss"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇8O0〇8:Z

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    new-instance v0, Landroid/os/Handler;

    .line 13
    .line 14
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 19
    .line 20
    .line 21
    new-instance v1, Lo0OoOOo0/Oo08;

    .line 22
    .line 23
    invoke-direct {v1, p0}, Lo0OoOOo0/Oo08;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;)V

    .line 24
    .line 25
    .line 26
    const-wide/16 v2, 0xc8

    .line 27
    .line 28
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->o8()V

    .line 33
    .line 34
    .line 35
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public init()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->Oooo8o0〇()Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇80〇808〇O(F)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->Oooo8o0〇()Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/high16 v1, 0x42c80000    # 100.0f

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->OO0o〇〇〇〇0(F)V

    .line 16
    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->〇〇8O0〇8:Z

    .line 20
    .line 21
    const-string v0, "SinglePageOcrEdgeScanDialogCallback"

    .line 22
    .line 23
    const-string v1, "init"

    .line 24
    .line 25
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oO80()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oo〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "SinglePageOcrEdgeScanDialogCallback"

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const-string v0, "requestShow isCancel true"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O〇()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    const-string v0, "requestShow isShowProgressDialog true"

    .line 26
    .line 27
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/SinglePageOcrEdgeScanDialogCallback;->init()V

    .line 32
    .line 33
    .line 34
    const/4 v0, 0x1

    .line 35
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O00(Z)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    const/16 v2, 0x64

    .line 43
    .line 44
    int-to-long v2, v2

    .line 45
    const-wide/16 v4, 0x3e8

    .line 46
    .line 47
    div-long/2addr v4, v2

    .line 48
    invoke-virtual {v1, v4, v5}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->O8ooOoo〇(J)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇0000OOO(I)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->O〇8O8〇008(Z)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇〇0〇()V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇80〇808〇O(IJ)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->Oooo8o0〇()Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    int-to-float p1, p1

    .line 6
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇080(F)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->Oooo8o0〇()Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o〇()F

    .line 18
    .line 19
    .line 20
    move-result p2

    .line 21
    invoke-static {p2}, Lkotlin/math/MathKt;->〇o00〇〇Oo(F)I

    .line 22
    .line 23
    .line 24
    move-result p2

    .line 25
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public final Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;
.super Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;
.source "DefaultOCRProgressDialogCallback.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8o8o〇:Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO0o〇〇〇〇0:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO80:Landroid/app/Activity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;->〇8o8o〇:Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "DefaultOCRProgressDialogCallback"

    .line 7
    .line 8
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;->oO80:Landroid/app/Activity;

    .line 12
    .line 13
    new-instance p1, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 14
    .line 15
    invoke-direct {p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;-><init>()V

    .line 16
    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;->〇80〇808〇O:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 19
    .line 20
    new-instance p1, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback$progressAnimationCallBack$1;

    .line 21
    .line 22
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback$progressAnimationCallBack$1;-><init>(Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;)V

    .line 23
    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oOO8O8(Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇0〇O0088o(Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;)Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;->〇80〇808〇O:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O8()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o800o8O()V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public dismiss()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "dismiss isShowProgressDialog:"

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "DefaultOCRProgressDialogCallback"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;->〇80〇808〇O:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->〇080()V

    .line 30
    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O00(Z)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final getActivity()Landroid/app/Activity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;->oO80:Landroid/app/Activity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public init()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->Oooo8o0〇()Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇80〇808〇O(F)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->Oooo8o0〇()Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/high16 v1, 0x42c80000    # 100.0f

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->OO0o〇〇〇〇0(F)V

    .line 16
    .line 17
    .line 18
    const-string v0, "DefaultOCRProgressDialogCallback"

    .line 19
    .line 20
    const-string v1, "init"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oO80()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oo〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "DefaultOCRProgressDialogCallback"

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const-string v0, "requestShow isCancel = true"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O〇()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const-string v2, "requestShow isShowProgressDialog:"

    .line 24
    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O〇()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    new-instance v3, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O〇()Z

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    invoke-virtual {v3}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇oo〇()Z

    .line 59
    .line 60
    .line 61
    move-result v3

    .line 62
    new-instance v4, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    const-string v0, " isCancel="

    .line 74
    .line 75
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/DefaultOCRProgressDialogCallback;->init()V

    .line 89
    .line 90
    .line 91
    const/4 v0, 0x1

    .line 92
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->〇O00(Z)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇〇0〇()V

    .line 100
    .line 101
    .line 102
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇80〇808〇O(IJ)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->Oooo8o0〇()Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    int-to-float p1, p1

    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇080(F)V

    .line 7
    .line 8
    .line 9
    const-wide/16 v0, 0x0

    .line 10
    .line 11
    cmp-long p1, p2, v0

    .line 12
    .line 13
    if-lez p1, :cond_0

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->O8ooOoo〇(J)V

    .line 20
    .line 21
    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->OO0o〇〇()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/progress/BaseOCRProgressDialogCallback;->Oooo8o0〇()Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    invoke-virtual {p2}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o〇()F

    .line 31
    .line 32
    .line 33
    move-result p2

    .line 34
    invoke-static {p2}, Lkotlin/math/MathKt;->〇o00〇〇Oo(F)I

    .line 35
    .line 36
    .line 37
    move-result p2

    .line 38
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.class public Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;
.super Ljava/lang/Object;
.source "BatchOCRPreparePresenter.java"

# interfaces
.implements Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$Presenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter$TransFormatTask;
    }
.end annotation


# instance fields
.field private O8:Z

.field private final Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

.field private oO80:Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;

.field private o〇0:Lcom/intsig/app/BaseProgressDialog;

.field private 〇080:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

.field private 〇o〇:Lcom/intsig/camscanner/mode_ocr/OCRClient;

.field private 〇〇888:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 12
    .line 13
    invoke-direct {v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇o〇:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->O8:Z

    .line 20
    .line 21
    new-instance v0, Ljava/util/HashMap;

    .line 22
    .line 23
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇〇888:Ljava/util/HashMap;

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter$1;

    .line 29
    .line 30
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter$1;-><init>(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;)V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->oO80:Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;

    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic O8ooOoo〇(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :catchall_0
    move-exception p1

    .line 12
    new-instance p2, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v0, "loadDataFromIntent - AlertDialog,\n"

    .line 18
    .line 19
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    const-string p2, "BatchOCRPreparePresenter"

    .line 30
    .line 31
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :goto_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;)Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇8O8〇008(Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 11
    .line 12
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->o〇0()Ljava/util/List;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 26
    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v1, "inputOCRDataList size="

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 38
    .line 39
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const-string v1, "BatchOCRPreparePresenter"

    .line 51
    .line 52
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    if-eqz p1, :cond_1

    .line 56
    .line 57
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 58
    .line 59
    .line 60
    :cond_1
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;)Ljava/util/HashMap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇〇888:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static oo88o8O(Ljava/util/ArrayList;)Landroidx/collection/SparseArrayCompat;
    .locals 6
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroidx/collection/SparseArrayCompat<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "checkCompatibility API = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "BatchOCRPreparePresenter"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    const/4 v2, 0x0

    .line 27
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    if-ge v2, v3, :cond_2

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/utils/DocumentUtil;->Oo08()Lcom/intsig/utils/DocumentUtil;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 38
    .line 39
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v5

    .line 43
    check-cast v5, Landroid/net/Uri;

    .line 44
    .line 45
    invoke-virtual {v3, v4, v5}, Lcom/intsig/utils/DocumentUtil;->〇〇888(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    if-nez v4, :cond_1

    .line 54
    .line 55
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    invoke-static {v4}, Lcom/intsig/camscanner/gallery/CustomGalleryUtil;->Oo08(Ljava/lang/String;)Z

    .line 60
    .line 61
    .line 62
    move-result v4

    .line 63
    if-eqz v4, :cond_1

    .line 64
    .line 65
    if-nez v0, :cond_0

    .line 66
    .line 67
    new-instance v0, Landroidx/collection/SparseArrayCompat;

    .line 68
    .line 69
    invoke-direct {v0}, Landroidx/collection/SparseArrayCompat;-><init>()V

    .line 70
    .line 71
    .line 72
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    const-string v5, "get a needTrans image = "

    .line 78
    .line 79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v4

    .line 89
    invoke-static {v1, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, v2, v3}, Landroidx/collection/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 93
    .line 94
    .line 95
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_2
    return-object v0
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private o〇O8〇〇o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->o〇0:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-static {v0, v1}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->o〇0:Lcom/intsig/app/BaseProgressDialog;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 19
    .line 20
    invoke-interface {v1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const v2, 0x7f131ec6

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o〇〇0〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇〇888:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 23
    .line 24
    iget-object v2, v1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 25
    .line 26
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_0

    .line 31
    .line 32
    iget-object v2, v1, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o〇()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    :goto_1
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇〇888:Ljava/util/HashMap;

    .line 40
    .line 41
    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇00(Ljava/util/List;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->oO80:Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->oO80(Ljava/util/List;Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static synthetic 〇0000OOO(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p0, "BatchOCRPreparePresenter"

    .line 2
    .line 3
    const-string p1, "cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇oOO8O8(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇O00(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇0000OOO(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇O888o0o(Ljava/util/ArrayList;)Landroidx/collection/SparseArrayCompat;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->oo88o8O(Ljava/util/ArrayList;)Landroidx/collection/SparseArrayCompat;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O〇(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇00(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇oOO8O8(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 10
    .line 11
    .line 12
    move-result p2

    .line 13
    if-nez p2, :cond_0

    .line 14
    .line 15
    const-string p2, "BatchOCRPreparePresenter"

    .line 16
    .line 17
    const-string v0, "discard"

    .line 18
    .line 19
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    invoke-virtual {p2}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->〇o〇()V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
.end method

.method private 〇oo〇(Landroid/content/Intent;Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->o〇O8〇〇o()V

    .line 2
    .line 3
    .line 4
    const-string v0, "android.intent.extra.STREAM"

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const/4 v0, 0x0

    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-gtz v1, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter$TransFormatTask;

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->o〇0:Lcom/intsig/app/BaseProgressDialog;

    .line 23
    .line 24
    invoke-direct {v1, p1, p2, v2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter$TransFormatTask;-><init>(Ljava/util/ArrayList;Ljava/lang/Runnable;Lcom/intsig/app/BaseProgressDialog;)V

    .line 25
    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇8o8o〇()Ljava/util/concurrent/ExecutorService;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    new-array p2, v0, [Ljava/lang/Void;

    .line 32
    .line 33
    invoke-virtual {v1, p1, p2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 34
    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v1, "importOcrImageData BUT imageUri is null ="

    .line 43
    .line 44
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    if-nez p1, :cond_2

    .line 48
    .line 49
    const/4 v0, 0x1

    .line 50
    :cond_2
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string p1, " !"

    .line 54
    .line 55
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    const-string p2, "BatchOCRPreparePresenter"

    .line 63
    .line 64
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇〇808〇(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->O8ooOoo〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->O〇8O8〇008(Ljava/lang/Runnable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O8(ILcom/intsig/camscanner/mode_ocr/OCRData;)V
    .locals 1

    .line 1
    if-ltz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-le p1, v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 15
    .line 16
    .line 17
    :cond_1
    :goto_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0o〇〇()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇〇〇0(I)Landroid/content/Intent;
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    if-eqz v1, :cond_3

    .line 10
    .line 11
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-lez v1, :cond_3

    .line 16
    .line 17
    new-instance v1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;

    .line 18
    .line 19
    invoke-direct {v1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;-><init>()V

    .line 20
    .line 21
    .line 22
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 23
    .line 24
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    const-wide/16 v4, 0x0

    .line 29
    .line 30
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v6

    .line 34
    if-eqz v6, :cond_2

    .line 35
    .line 36
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v6

    .line 40
    check-cast v6, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 41
    .line 42
    const-wide/16 v7, 0x1

    .line 43
    .line 44
    sub-long/2addr v4, v7

    .line 45
    iget-object v7, v6, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 46
    .line 47
    invoke-static {v7}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 48
    .line 49
    .line 50
    move-result v7

    .line 51
    if-eqz v7, :cond_0

    .line 52
    .line 53
    iget-object v7, v6, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇OOo8〇0:Ljava/lang/String;

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_0
    invoke-virtual {v6}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o〇()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v7

    .line 60
    :goto_1
    iget v8, v6, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 61
    .line 62
    invoke-virtual {v1, v7, v8}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->Oooo8o0〇(Ljava/lang/String;I)V

    .line 63
    .line 64
    .line 65
    iget-object v8, v6, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO:Ljava/lang/String;

    .line 66
    .line 67
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 68
    .line 69
    .line 70
    move-result v8

    .line 71
    if-nez v8, :cond_1

    .line 72
    .line 73
    iget-object v8, v6, Lcom/intsig/camscanner/mode_ocr/OCRData;->OO:Ljava/lang/String;

    .line 74
    .line 75
    invoke-static {v8}, Lcom/intsig/camscanner/db/dao/ImageDaoUtil;->〇080(Ljava/lang/String;)[I

    .line 76
    .line 77
    .line 78
    move-result-object v8

    .line 79
    invoke-virtual {v1, v7, v8}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->OO0o〇〇〇〇0(Ljava/lang/String;[I)V

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_1
    move-object v8, v2

    .line 84
    :goto_2
    iget v6, v6, Lcom/intsig/camscanner/mode_ocr/OCRData;->o8oOOo:I

    .line 85
    .line 86
    invoke-static {v4, v5, v7, v6, v8}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->O8(JLjava/lang/String;I[I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 87
    .line 88
    .line 89
    move-result-object v6

    .line 90
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_2
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->〇O00(I)V

    .line 95
    .line 96
    .line 97
    goto :goto_3

    .line 98
    :cond_3
    move-object v1, v2

    .line 99
    :goto_3
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 100
    .line 101
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->clone()Ljava/lang/Object;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    check-cast p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .line 107
    goto :goto_4

    .line 108
    :catch_0
    move-exception p1

    .line 109
    const-string v3, "BatchOCRPreparePresenter"

    .line 110
    .line 111
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    .line 113
    .line 114
    move-object p1, v2

    .line 115
    :goto_4
    if-nez p1, :cond_4

    .line 116
    .line 117
    return-object v2

    .line 118
    :cond_4
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 119
    .line 120
    invoke-interface {v2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 121
    .line 122
    .line 123
    move-result-object v2

    .line 124
    const/4 v3, 0x3

    .line 125
    invoke-static {v2, p1, v1, v3, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultActivity;->O0〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;ILjava/util/List;)Landroid/content/Intent;

    .line 126
    .line 127
    .line 128
    move-result-object p1

    .line 129
    return-object p1
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public Oo08(ZLcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem$ItemCallback;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem$ItemCallback;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/adapter/AbsRecyclerViewItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 23
    .line 24
    new-instance v3, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o〇()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    invoke-direct {v3, v4}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;-><init>(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    new-instance v4, Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem;

    .line 34
    .line 35
    iget-object v5, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 36
    .line 37
    invoke-interface {v5}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    invoke-direct {v4, v5, v2, v3, p1}, Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;Z)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v4, p2}, Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem;->〇O〇(Lcom/intsig/camscanner/recycler_adapter/item/BatchOcrPrepareItem$ItemCallback;)V

    .line 49
    .line 50
    .line 51
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public Oooo8o0〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->O8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80(II)V
    .locals 2

    .line 1
    if-ge p1, p2, :cond_0

    .line 2
    .line 3
    :goto_0
    if-ge p1, p2, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 6
    .line 7
    add-int/lit8 v1, p1, 0x1

    .line 8
    .line 9
    invoke-static {v0, p1, v1}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 10
    .line 11
    .line 12
    move p1, v1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    :goto_1
    if-le p1, p2, :cond_1

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 17
    .line 18
    add-int/lit8 v1, p1, -0x1

    .line 19
    .line 20
    invoke-static {v0, p1, v1}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 21
    .line 22
    .line 23
    add-int/lit8 p1, p1, -0x1

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇0(Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRClientCallback;Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇o〇:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->o8oO〇(Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRClientCallback;)V

    .line 4
    .line 5
    .line 6
    const-string v7, "paragraph"

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇o〇:Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 17
    .line 18
    const/4 v5, 0x0

    .line 19
    const/4 v6, 0x0

    .line 20
    const/4 v8, 0x0

    .line 21
    const-string v9, ""

    .line 22
    .line 23
    move-object v4, p2

    .line 24
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->oo〇(Landroid/app/Activity;Ljava/util/List;Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;ILjava/lang/String;Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O()Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->O8:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 14
    .line 15
    .line 16
    const v1, 0x7f130276

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    new-instance v1, L〇〇〇0o〇〇0/〇o00〇〇Oo;

    .line 24
    .line 25
    invoke-direct {v1, p0}, L〇〇〇0o〇〇0/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;)V

    .line 26
    .line 27
    .line 28
    const v2, 0x7f130128

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    new-instance v1, L〇〇〇0o〇〇0/〇o〇;

    .line 36
    .line 37
    invoke-direct {v1}, L〇〇〇0o〇〇0/〇o〇;-><init>()V

    .line 38
    .line 39
    .line 40
    const v2, 0x7f13057e

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 52
    .line 53
    .line 54
    const/4 v0, 0x1

    .line 55
    return v0

    .line 56
    :cond_0
    const/4 v0, 0x0

    .line 57
    return v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇8o8o〇(Ljava/lang/String;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8ooOoo〇()Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const/4 p1, 0x0

    .line 38
    :goto_0
    return p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇O8o08O(Landroid/content/Intent;Ljava/lang/Runnable;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, L〇〇〇0o〇〇0/O8;

    .line 8
    .line 9
    invoke-direct {v1, p0, p2}, L〇〇〇0o〇〇0/O8;-><init>(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;Ljava/lang/Runnable;)V

    .line 10
    .line 11
    .line 12
    const-string p2, "android.intent.action.SEND_MULTIPLE"

    .line 13
    .line 14
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    if-eqz p2, :cond_1

    .line 19
    .line 20
    const/4 p2, 0x1

    .line 21
    iput-boolean p2, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->O8:Z

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->o〇0()Ljava/util/List;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    if-lt v2, p2, :cond_0

    .line 38
    .line 39
    new-instance p1, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string p2, "loadDataFromIntent from import, but ocrDataList.size="

    .line 45
    .line 46
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 50
    .line 51
    .line 52
    move-result p2

    .line 53
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    const-string p2, "BatchOCRPreparePresenter"

    .line 61
    .line 62
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 66
    .line 67
    iget-object p2, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->Oo08:Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;

    .line 68
    .line 69
    invoke-interface {p2}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPrepareContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    invoke-direct {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 74
    .line 75
    .line 76
    const p2, 0x7f130c72

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    const p2, 0x7f130c73

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    new-instance p2, L〇〇〇0o〇〇0/Oo08;

    .line 91
    .line 92
    invoke-direct {p2, p0}, L〇〇〇0o〇〇0/Oo08;-><init>(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;)V

    .line 93
    .line 94
    .line 95
    const v0, 0x7f130d3e

    .line 96
    .line 97
    .line 98
    invoke-virtual {p1, v0, p2}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 107
    .line 108
    .line 109
    return-void

    .line 110
    :cond_0
    invoke-static {}, Lcom/intsig/comm/util/AppLaunchSourceStatistic;->〇080()V

    .line 111
    .line 112
    .line 113
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇oo〇(Landroid/content/Intent;Ljava/lang/Runnable;)V

    .line 114
    .line 115
    .line 116
    return-void

    .line 117
    :cond_1
    const-string p2, "extra_doc_info"

    .line 118
    .line 119
    invoke-virtual {p1, p2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    check-cast p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 124
    .line 125
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 126
    .line 127
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 128
    .line 129
    .line 130
    :cond_2
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇o00〇〇Oo(Landroid/content/Intent;)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->〇〇888()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->Oo08()Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mode_ocr/CaptureOCRImageData;->oO80(Ljava/util/Collection;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    if-nez p1, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    const-string v0, "extra_multi_capture_status"

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    instance-of v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;

    .line 30
    .line 31
    if-nez v0, :cond_2

    .line 32
    .line 33
    const-string p1, "BatchOCRPreparePresenter"

    .line 34
    .line 35
    const-string v0, "parcelable is not MultiCaptureResultStatus"

    .line 36
    .line 37
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_2
    check-cast p1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;->〇80〇808〇O()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_7

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;->o〇0()Ljava/util/List;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-eqz v0, :cond_3

    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->o〇〇0〇()V

    .line 61
    .line 62
    .line 63
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    if-eqz v1, :cond_6

    .line 72
    .line 73
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    check-cast v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 78
    .line 79
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇〇888:Ljava/util/HashMap;

    .line 80
    .line 81
    iget-object v3, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 82
    .line 83
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 88
    .line 89
    if-nez v2, :cond_4

    .line 90
    .line 91
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇〇888:Ljava/util/HashMap;

    .line 92
    .line 93
    iget-object v3, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo0:Ljava/lang/String;

    .line 94
    .line 95
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 100
    .line 101
    :cond_4
    if-nez v2, :cond_5

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_5
    iget v2, v2, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇8〇oO〇〇8o:I

    .line 105
    .line 106
    invoke-static {v2}, Lcom/intsig/camscanner/app/DBUtil;->〇00(I)I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    iput v2, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇O〇〇O8:I

    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_6
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    new-instance v1, L〇〇〇0o〇〇0/〇080;

    .line 118
    .line 119
    invoke-direct {v1, p0, p1}, L〇〇〇0o〇〇0/〇080;-><init>(Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;Ljava/util/List;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 123
    .line 124
    .line 125
    :cond_7
    :goto_1
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇o〇(I)Lcom/intsig/camscanner/mode_ocr/OCRData;
    .locals 1

    .line 1
    if-ltz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lt p1, v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 19
    .line 20
    return-object p1

    .line 21
    :cond_1
    :goto_0
    const/4 p1, 0x0

    .line 22
    return-object p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇〇888()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/contract/BatchOCRPreparePresenter;->〇080:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "SelectOcrItemProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;,
        Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8o08O8O:Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o〇00O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$glideRequestOptionsGrid$2;->o0:Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$glideRequestOptionsGrid$2;

    .line 7
    .line 8
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider;->o〇00O:Lkotlin/Lazy;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oo88o8O()Lcom/bumptech/glide/request/RequestOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider;->o〇00O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇oo〇(Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;Landroid/widget/ImageView;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider;->oo88o8O()Lcom/bumptech/glide/request/RequestOptions;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "glideRequestOptionsGrid"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 11
    .line 12
    const/16 v2, 0x1d

    .line 13
    .line 14
    if-lt v1, v2, :cond_0

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    invoke-static {p2, v1}, Lo〇o〇Oo88/〇080;->〇080(Landroid/widget/ImageView;Z)V

    .line 18
    .line 19
    .line 20
    :cond_0
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-static {v1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-nez v1, :cond_1

    .line 31
    .line 32
    const v1, 0x7f080908

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇8o8o〇(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const-string v1, "{\n            //\u56fe\u7247\u6ca1\u6709\u4e0b\u8f7d\u8fc7\uff0c\u2026mg_wifi_broken)\n        }"

    .line 46
    .line 47
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    const v1, 0x7f0802b5

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇8o8o〇(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    const-string v1, "{\n            requestOpt\u2026g_image_upload)\n        }"

    .line 67
    .line 68
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 72
    .line 73
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;->getInputPath()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-eqz v1, :cond_2

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;->getInputPath()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    goto :goto_1

    .line 88
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;->getRawPath()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .line 96
    .line 97
    const-string v2, "displayPath == "

    .line 98
    .line 99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    const-string v2, "SelectOcrItemProvider"

    .line 110
    .line 111
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    invoke-static {v1}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    invoke-virtual {v1, p1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    invoke-virtual {v1, v0}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    const v1, 0x3f19999a    # 0.6f

    .line 131
    .line 132
    .line 133
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestBuilder;->O00(F)Lcom/bumptech/glide/RequestBuilder;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;

    .line 138
    .line 139
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;-><init>(Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇0(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    check-cast p1, Lcom/bumptech/glide/RequestBuilder;

    .line 147
    .line 148
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 149
    .line 150
    .line 151
    return-void
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    new-instance p2, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;

    .line 11
    .line 12
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string v0, "baseViewHolder.itemView"

    .line 15
    .line 16
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;-><init>(Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider;Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object p2
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;)V
    .locals 6
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p1, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->O〇8O8〇008()Landroid/widget/ImageView;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider;->〇oo〇(Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;Landroid/widget/ImageView;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->O8ooOoo〇()Landroid/widget/TextView;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 25
    .line 26
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    const/4 v2, 0x1

    .line 31
    new-array v3, v2, [Ljava/lang/Object;

    .line 32
    .line 33
    invoke-virtual {p2}, Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;->getPageIndex()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    const/4 v5, 0x0

    .line 42
    aput-object v4, v3, v5

    .line 43
    .line 44
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    const-string v4, "%1$02d"

    .line 49
    .line 50
    invoke-static {v1, v4, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    const-string v3, "format(locale, format, *args)"

    .line 55
    .line 56
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p2}, Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;->isSelect()Z

    .line 63
    .line 64
    .line 65
    move-result p2

    .line 66
    if-eqz p2, :cond_0

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->〇00()Landroid/widget/CheckBox;

    .line 69
    .line 70
    .line 71
    move-result-object p2

    .line 72
    invoke-virtual {p2, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->O8ooOoo〇()Landroid/widget/TextView;

    .line 76
    .line 77
    .line 78
    move-result-object p2

    .line 79
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    const v1, 0x7f060204

    .line 84
    .line 85
    .line 86
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->O8ooOoo〇()Landroid/widget/TextView;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    const v0, 0x7f0803ee

    .line 98
    .line 99
    .line 100
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->〇oOO8O8()Landroid/view/View;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    const p2, 0x7f0810c7

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 111
    .line 112
    .line 113
    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->〇00()Landroid/widget/CheckBox;

    .line 115
    .line 116
    .line 117
    move-result-object p2

    .line 118
    invoke-virtual {p2, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->O8ooOoo〇()Landroid/widget/TextView;

    .line 122
    .line 123
    .line 124
    move-result-object p2

    .line 125
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    const v1, 0x7f060208

    .line 130
    .line 131
    .line 132
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 137
    .line 138
    .line 139
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->O8ooOoo〇()Landroid/widget/TextView;

    .line 140
    .line 141
    .line 142
    move-result-object p2

    .line 143
    const/4 v0, 0x0

    .line 144
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider$OcrViewHolder;->〇oOO8O8()Landroid/view/View;

    .line 148
    .line 149
    .line 150
    move-result-object p1

    .line 151
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 152
    .line 153
    .line 154
    :goto_0
    return-void
.end method

.method public oO80()I
    .locals 1

    .line 1
    const v0, 0x7f0d04aa

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mode_ocr/provider/SelectOcrItemProvider;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mode_ocr/bean/OcrSelectData;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

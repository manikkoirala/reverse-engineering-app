.class public Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;
.super Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;
.source "OCRClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mode_ocr/OCRClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BatchOcrInterceptor"
.end annotation


# instance fields
.field private final O8:Landroid/content/Context;

.field private final OO0o〇〇:Ljava/lang/String;

.field private final OO0o〇〇〇〇0:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

.field private final Oo08:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private Oooo8o0〇:I

.field private volatile oO80:Lcom/intsig/tianshu/exception/TianShuException;

.field private final o〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇80〇808〇O:Lcom/intsig/camscanner/tsapp/HttpCodeTips;

.field private final 〇8o8o〇:I

.field private final 〇O00:[B

.field private final 〇O8o08O:Ljava/lang/String;

.field private 〇O〇:F

.field private 〇〇808〇:I

.field private final 〇〇888:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

.field private volatile 〇〇8O0〇8:Z


# direct methods
.method constructor <init>(Landroid/app/Activity;Ljava/util/List;Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;Lcom/intsig/camscanner/tsapp/HttpCodeTips$ReLoginCallBack;ILjava/lang/String;Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;Ljava/lang/String;)V
    .locals 2
    .param p7    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;",
            "Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;",
            "Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;",
            "Lcom/intsig/camscanner/tsapp/HttpCodeTips$ReLoginCallBack;",
            "I",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->Oooo8o0〇:I

    .line 6
    .line 7
    const/16 v1, 0x64

    .line 8
    .line 9
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇808〇:I

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O〇:F

    .line 13
    .line 14
    new-array v0, v0, [B

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O00:[B

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇8O0〇8:Z

    .line 20
    .line 21
    invoke-virtual {p0, p8}, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇〇888(Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object p8

    .line 28
    iput-object p8, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->O8:Landroid/content/Context;

    .line 29
    .line 30
    new-instance p8, Ljava/lang/ref/WeakReference;

    .line 31
    .line 32
    invoke-direct {p8, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 33
    .line 34
    .line 35
    iput-object p8, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->Oo08:Ljava/lang/ref/WeakReference;

    .line 36
    .line 37
    iput-object p2, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 38
    .line 39
    iput-object p3, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇888:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 40
    .line 41
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/HttpCodeTips;->〇o〇(Landroid/app/Activity;)Lcom/intsig/camscanner/tsapp/HttpCodeTips;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    iput-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇80〇808〇O:Lcom/intsig/camscanner/tsapp/HttpCodeTips;

    .line 46
    .line 47
    invoke-virtual {p1, p5}, Lcom/intsig/camscanner/tsapp/HttpCodeTips;->〇〇888(Lcom/intsig/camscanner/tsapp/HttpCodeTips$ReLoginCallBack;)V

    .line 48
    .line 49
    .line 50
    iput-object p4, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 51
    .line 52
    iput p6, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇8o8o〇:I

    .line 53
    .line 54
    iput-object p7, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O8o08O:Ljava/lang/String;

    .line 55
    .line 56
    iput-object p9, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->OO0o〇〇:Ljava/lang/String;

    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
.end method

.method private synthetic OO0o〇〇(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->dismiss()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇888:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 7
    .line 8
    const-string v1, "OCRClient"

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string p1, "ocrProgressListener == null"

    .line 13
    .line 14
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 19
    .line 20
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->〇080()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇888:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 29
    .line 30
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;->〇o00〇〇Oo(Ljava/util/List;)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 35
    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;->〇o〇()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    iget-object v2, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 43
    .line 44
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;->O8()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    goto :goto_0

    .line 49
    :cond_2
    const/4 v0, 0x0

    .line 50
    const/4 v2, 0x0

    .line 51
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->oO80:Lcom/intsig/tianshu/exception/TianShuException;

    .line 52
    .line 53
    if-eqz v3, :cond_4

    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 56
    .line 57
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->o〇0()Z

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    if-eqz p1, :cond_3

    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇888:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 66
    .line 67
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;->〇o〇(Ljava/util/List;)V

    .line 68
    .line 69
    .line 70
    const-string p1, "don\'t handle next error tips"

    .line 71
    .line 72
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    return-void

    .line 76
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇888:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 79
    .line 80
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;->〇o〇(Ljava/util/List;)V

    .line 81
    .line 82
    .line 83
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇80〇808〇O:Lcom/intsig/camscanner/tsapp/HttpCodeTips;

    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->oO80:Lcom/intsig/tianshu/exception/TianShuException;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/tsapp/HttpCodeTips;->o〇0(I)V

    .line 92
    .line 93
    .line 94
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇80〇808〇O:Lcom/intsig/camscanner/tsapp/HttpCodeTips;

    .line 95
    .line 96
    invoke-virtual {p1}, Lcom/intsig/camscanner/tsapp/HttpCodeTips;->oO80()V

    .line 97
    .line 98
    .line 99
    goto :goto_1

    .line 100
    :cond_4
    if-gtz v0, :cond_5

    .line 101
    .line 102
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 103
    .line 104
    invoke-static {v3}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->o0ooO(Ljava/util/List;)I

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    if-lez v3, :cond_5

    .line 109
    .line 110
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇888:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 111
    .line 112
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 113
    .line 114
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;->O8(Ljava/util/List;)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇o〇()V

    .line 118
    .line 119
    .line 120
    goto :goto_1

    .line 121
    :cond_5
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 122
    .line 123
    invoke-interface {v3}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->Oo08()V

    .line 124
    .line 125
    .line 126
    const-string v3, "ocr handle finishOCR"

    .line 127
    .line 128
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇888:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 132
    .line 133
    iget-object v3, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 134
    .line 135
    invoke-interface {v1, v3, v0, v2, p1}, Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;->〇080(Ljava/util/List;IIZ)V

    .line 136
    .line 137
    .line 138
    sget-object p1, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;->〇〇888:Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount$Companion;

    .line 139
    .line 140
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount$Companion;->〇080()Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;

    .line 141
    .line 142
    .line 143
    move-result-object p1

    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 145
    .line 146
    .line 147
    move-result-wide v0

    .line 148
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;->O8(J)V

    .line 149
    .line 150
    .line 151
    :goto_1
    return-void
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->OO0o〇〇(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic Oooo8o0〇(Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O〇(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 5
    .line 6
    invoke-interface {p1}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->O8()Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-nez p1, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->Oo08()V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->Oooo8o0〇(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;FLcom/intsig/camscanner/mode_ocr/OCRData;)Ljava/lang/Void;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O8o08O(Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;FLcom/intsig/camscanner/mode_ocr/OCRData;)Ljava/lang/Void;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇8o8o〇(Lcom/intsig/camscanner/mode_ocr/OCRData;FLcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            "F",
            "Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;",
            ")",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇o00〇〇Oo()Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/mode_ocr/〇〇808〇;

    .line 6
    .line 7
    invoke-direct {v1, p0, p3, p2, p1}, Lcom/intsig/camscanner/mode_ocr/〇〇808〇;-><init>(Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;FLcom/intsig/camscanner/mode_ocr/OCRData;)V

    .line 8
    .line 9
    .line 10
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇O00(Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->Oo08:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/app/Activity;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {v0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    :goto_0
    const-string p1, "OCRClient"

    .line 23
    .line 24
    const-string v0, "activity == null || activity.isFinishing()"

    .line 25
    .line 26
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic 〇O8o08O(Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;FLcom/intsig/camscanner/mode_ocr/OCRData;)Ljava/lang/Void;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    move-object v1, p0

    .line 2
    move-object/from16 v0, p3

    .line 3
    .line 4
    iget-boolean v2, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇8O0〇8:Z

    .line 5
    .line 6
    const/4 v3, 0x0

    .line 7
    if-nez v2, :cond_0

    .line 8
    .line 9
    return-object v3

    .line 10
    :cond_0
    iget-object v2, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->oO80:Lcom/intsig/tianshu/exception/TianShuException;

    .line 11
    .line 12
    if-eqz v2, :cond_1

    .line 13
    .line 14
    return-object v3

    .line 15
    :cond_1
    invoke-static {}, Lcom/intsig/CsHosts;->o〇〇0〇()Z

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    const-string v4, "OCRClient"

    .line 20
    .line 21
    if-eqz v2, :cond_2

    .line 22
    .line 23
    const-string v2, "OApi is not fetch, and try load one time "

    .line 24
    .line 25
    invoke-static {v4, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->〇〇888()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_2
    const-string v2, "OApi is ready"

    .line 33
    .line 34
    invoke-static {v4, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    :goto_0
    const v2, 0x3f666666    # 0.9f

    .line 38
    .line 39
    .line 40
    mul-float v2, v2, p2

    .line 41
    .line 42
    move-object/from16 v5, p1

    .line 43
    .line 44
    invoke-virtual {v5, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇080(F)V

    .line 45
    .line 46
    .line 47
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇o〇()F

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    const-wide/16 v5, 0x28

    .line 52
    .line 53
    invoke-direct {p0, v2, v5, v6}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇8O0〇8(FJ)V

    .line 54
    .line 55
    .line 56
    const/4 v2, 0x0

    .line 57
    :try_start_0
    new-instance v5, Lcom/intsig/camscanner/mode_ocr/ServerOCRStrategy;

    .line 58
    .line 59
    iget-object v6, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->O8:Landroid/content/Context;

    .line 60
    .line 61
    invoke-direct {v5, v6}, Lcom/intsig/camscanner/mode_ocr/ServerOCRStrategy;-><init>(Landroid/content/Context;)V

    .line 62
    .line 63
    .line 64
    iget v6, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇8o8o〇:I

    .line 65
    .line 66
    iget-object v7, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O8o08O:Ljava/lang/String;

    .line 67
    .line 68
    iget-object v8, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->OO0o〇〇:Ljava/lang/String;

    .line 69
    .line 70
    invoke-virtual {v5, v0, v6, v7, v8}, Lcom/intsig/camscanner/mode_ocr/ServerOCRStrategy;->〇o00〇〇Oo(Lcom/intsig/camscanner/mode_ocr/OCRData;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v5}, Lcom/intsig/camscanner/mode_ocr/ServerOCRStrategy;->Oo08()Z

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    if-eqz v5, :cond_4

    .line 78
    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 80
    .line 81
    .line 82
    move-result-wide v5

    .line 83
    iput-wide v5, v0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08〇o0O:J

    .line 84
    .line 85
    const/4 v5, 0x1

    .line 86
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/mode_ocr/OCRData;->oo〇(Z)V

    .line 87
    .line 88
    .line 89
    iget-object v5, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->O8:Landroid/content/Context;

    .line 90
    .line 91
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v6

    .line 95
    invoke-static {v5, v6}, Lcom/intsig/camscanner/db/dao/ImageDao;->O〇O〇oO(Landroid/content/Context;Ljava/lang/String;)J

    .line 96
    .line 97
    .line 98
    move-result-wide v5
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_1

    .line 99
    const-wide/16 v7, 0x0

    .line 100
    .line 101
    cmp-long v9, v5, v7

    .line 102
    .line 103
    if-ltz v9, :cond_3

    .line 104
    .line 105
    :try_start_1
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/mode_ocr/OCRData;->clone()Ljava/lang/Object;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    check-cast v0, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 110
    .line 111
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇o00〇〇Oo()V

    .line 112
    .line 113
    .line 114
    iget-object v7, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->O8:Landroid/content/Context;

    .line 115
    .line 116
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇O888o0o()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v8

    .line 120
    invoke-virtual {v0}, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇0〇O0088o()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v9

    .line 124
    iget-wide v10, v0, Lcom/intsig/camscanner/mode_ocr/OCRData;->〇08〇o0O:J

    .line 125
    .line 126
    const/4 v14, 0x0

    .line 127
    move-wide v12, v5

    .line 128
    invoke-static/range {v7 .. v14}, Lcom/intsig/camscanner/app/DBUtil;->o88O〇8(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJZ)V

    .line 129
    .line 130
    .line 131
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    const/4 v7, 0x4

    .line 136
    invoke-static {v0, v5, v6, v7}, Lcom/intsig/camscanner/db/dao/ImageDao;->Oo〇O8o〇8(Landroid/content/Context;JI)I
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_1 .. :try_end_1} :catch_1

    .line 137
    .line 138
    .line 139
    goto :goto_1

    .line 140
    :catch_0
    move-exception v0

    .line 141
    :try_start_2
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 142
    .line 143
    .line 144
    :cond_3
    :goto_1
    const-string v0, "ocr handle updateProgress"

    .line 145
    .line 146
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    goto :goto_2

    .line 150
    :cond_4
    iput-boolean v2, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇8O0〇8:Z
    :try_end_2
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_2 .. :try_end_2} :catch_1

    .line 151
    .line 152
    goto :goto_2

    .line 153
    :catch_1
    move-exception v0

    .line 154
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v0}, Lcom/intsig/tianshu/exception/TianShuException;->getErrorCode()I

    .line 158
    .line 159
    .line 160
    move-result v4

    .line 161
    const/16 v5, 0x67

    .line 162
    .line 163
    if-ne v4, v5, :cond_5

    .line 164
    .line 165
    iput-boolean v2, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇8O0〇8:Z

    .line 166
    .line 167
    goto :goto_2

    .line 168
    :cond_5
    iput-object v0, v1, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->oO80:Lcom/intsig/tianshu/exception/TianShuException;

    .line 169
    .line 170
    :goto_2
    return-object v3
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private 〇O〇(Ljava/util/List;)V
    .locals 8
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/OCRData;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->oO80:Lcom/intsig/tianshu/exception/TianShuException;

    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇8O0〇8:Z

    .line 6
    .line 7
    const-string v0, "OCRClient"

    .line 8
    .line 9
    if-eqz p1, :cond_6

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-nez v1, :cond_0

    .line 16
    .line 17
    goto/16 :goto_3

    .line 18
    .line 19
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 20
    .line 21
    .line 22
    move-result-wide v1

    .line 23
    new-instance v3, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;

    .line 24
    .line 25
    invoke-direct {v3}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;-><init>()V

    .line 26
    .line 27
    .line 28
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    int-to-float v4, v4

    .line 33
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->OO0o〇〇〇〇0(F)V

    .line 34
    .line 35
    .line 36
    new-instance v4, Ljava/util/ArrayList;

    .line 37
    .line 38
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .line 40
    .line 41
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 46
    .line 47
    .line 48
    move-result v5

    .line 49
    if-eqz v5, :cond_3

    .line 50
    .line 51
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v5

    .line 55
    check-cast v5, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 56
    .line 57
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 58
    .line 59
    invoke-interface {v6}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->〇080()Z

    .line 60
    .line 61
    .line 62
    move-result v6

    .line 63
    if-nez v6, :cond_3

    .line 64
    .line 65
    iget-boolean v6, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇8O0〇8:Z

    .line 66
    .line 67
    if-eqz v6, :cond_3

    .line 68
    .line 69
    iget-object v6, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->oO80:Lcom/intsig/tianshu/exception/TianShuException;

    .line 70
    .line 71
    if-eqz v6, :cond_1

    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_1
    invoke-virtual {v5}, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8ooOoo〇()Z

    .line 75
    .line 76
    .line 77
    move-result v6

    .line 78
    const/high16 v7, 0x3f800000    # 1.0f

    .line 79
    .line 80
    if-eqz v6, :cond_2

    .line 81
    .line 82
    invoke-virtual {v3, v7}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->〇080(F)V

    .line 83
    .line 84
    .line 85
    goto :goto_0

    .line 86
    :cond_2
    invoke-direct {p0, v5, v7, v3}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇8o8o〇(Lcom/intsig/camscanner/mode_ocr/OCRData;FLcom/intsig/camscanner/tsapp/sync/SyncProgressValue;)Ljava/util/concurrent/Future;

    .line 87
    .line 88
    .line 89
    move-result-object v5

    .line 90
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 99
    .line 100
    .line 101
    move-result v4

    .line 102
    if-eqz v4, :cond_4

    .line 103
    .line 104
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 105
    .line 106
    .line 107
    move-result-object v4

    .line 108
    check-cast v4, Ljava/util/concurrent/Future;

    .line 109
    .line 110
    :try_start_0
    invoke-interface {v4}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    .line 112
    .line 113
    goto :goto_2

    .line 114
    :catch_0
    move-exception v4

    .line 115
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 116
    .line 117
    .line 118
    move-result-object v5

    .line 119
    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 120
    .line 121
    .line 122
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 123
    .line 124
    .line 125
    goto :goto_2

    .line 126
    :cond_4
    sget-object p1, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;->〇〇888:Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount$Companion;

    .line 127
    .line 128
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount$Companion;->〇080()Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 133
    .line 134
    .line 135
    move-result-wide v4

    .line 136
    sub-long/2addr v4, v1

    .line 137
    invoke-virtual {p1, v4, v5}, Lcom/intsig/camscanner/mode_ocr/bean/OcrTimeCount;->〇o〇(J)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v3}, Lcom/intsig/camscanner/tsapp/sync/SyncProgressValue;->o〇0()F

    .line 141
    .line 142
    .line 143
    move-result p1

    .line 144
    const-wide/16 v0, 0xa

    .line 145
    .line 146
    invoke-direct {p0, p1, v0, v1}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇8O0〇8(FJ)V

    .line 147
    .line 148
    .line 149
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;

    .line 150
    .line 151
    if-eqz p1, :cond_5

    .line 152
    .line 153
    invoke-virtual {p1}, Lcom/intsig/camscanner/mode_ocr/OCRBalanceManager;->〇080()I

    .line 154
    .line 155
    .line 156
    :cond_5
    return-void

    .line 157
    :cond_6
    :goto_3
    const-string p1, "requestBatchOcr ocrDataList is empty"

    .line 158
    .line 159
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇〇808〇(Z)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/Oooo8o0〇;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/mode_ocr/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;Z)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O00(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇8O0〇8(FJ)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O00:[B

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->Oooo8o0〇:I

    .line 5
    .line 6
    const/high16 v2, 0x3f800000    # 1.0f

    .line 7
    .line 8
    if-lez v1, :cond_0

    .line 9
    .line 10
    int-to-float v1, v1

    .line 11
    div-float/2addr p1, v1

    .line 12
    invoke-static {p1, v2}, Ljava/lang/Math;->min(FF)F

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇808〇:I

    .line 17
    .line 18
    int-to-float p1, p1

    .line 19
    mul-float p1, p1, v2

    .line 20
    .line 21
    iget v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O〇:F

    .line 22
    .line 23
    sub-float v1, p1, v1

    .line 24
    .line 25
    iput p1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O〇:F

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 28
    .line 29
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    invoke-interface {p1, v1, p2, p3}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->〇80〇808〇O(IJ)V

    .line 34
    .line 35
    .line 36
    monitor-exit v0

    .line 37
    return-void

    .line 38
    :catchall_0
    move-exception p1

    .line 39
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    throw p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public Oo08()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇808〇(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇O〇:F

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 5
    .line 6
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->〇o〇(Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 10
    .line 11
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->OO0o〇〇〇〇0()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iput v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇808〇:I

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 18
    .line 19
    if-eqz v0, :cond_5

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_0

    .line 26
    .line 27
    goto :goto_2

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 29
    .line 30
    invoke-interface {v0}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->〇080()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-eqz v0, :cond_1

    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    .line 38
    .line 39
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->o〇0:Ljava/util/List;

    .line 43
    .line 44
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    if-eqz v2, :cond_3

    .line 53
    .line 54
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    check-cast v2, Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 59
    .line 60
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/OCRData;->O8ooOoo〇()Z

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    if-nez v3, :cond_2

    .line 65
    .line 66
    invoke-virtual {v2}, Lcom/intsig/camscanner/mode_ocr/OCRData;->o〇〇0〇()Z

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    if-eqz v3, :cond_2

    .line 71
    .line 72
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 77
    .line 78
    .line 79
    move-result v1

    .line 80
    iput v1, p0, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->Oooo8o0〇:I

    .line 81
    .line 82
    if-nez v1, :cond_4

    .line 83
    .line 84
    const/4 v0, 0x0

    .line 85
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;->〇〇808〇(Z)V

    .line 86
    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_4
    iget-object v1, p0, Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;->〇080:Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;

    .line 90
    .line 91
    invoke-interface {v1}, Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;->oO80()V

    .line 92
    .line 93
    .line 94
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/OO0o〇〇;

    .line 99
    .line 100
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/mode_ocr/OO0o〇〇;-><init>(Lcom/intsig/camscanner/mode_ocr/OCRClient$BatchOcrInterceptor;Ljava/util/List;)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {v1, v2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 104
    .line 105
    .line 106
    :goto_1
    return-void

    .line 107
    :cond_5
    :goto_2
    const-string v0, "OCRClient"

    .line 108
    .line 109
    const-string v1, "processed ocrDataList is empty"

    .line 110
    .line 111
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

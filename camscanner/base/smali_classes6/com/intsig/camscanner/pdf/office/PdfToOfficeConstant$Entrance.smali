.class public final enum Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;
.super Ljava/lang/Enum;
.source "PdfToOfficeConstant.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum DOCUMENT_MORE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum DOCUMENT_OPERATION:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum IMAGE_DETAIL:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum IMAGE_TO_OFFICE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum MAIN:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum OUTSIDE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum OUTSIDE_TO_WORD:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum PDF_PREVIEW:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum PDF_PREVIEW_TO_WORD:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum PDF_TOOLS:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field public static final enum SHARE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;
    .locals 3

    .line 1
    const/16 v0, 0xb

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->OUTSIDE_TO_WORD:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->OUTSIDE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_TOOLS:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->SHARE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->DOCUMENT_OPERATION:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->DOCUMENT_MORE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_PREVIEW:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->IMAGE_DETAIL:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->MAIN:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_PREVIEW_TO_WORD:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->IMAGE_TO_OFFICE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    return-object v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 2
    .line 3
    const-string v1, "OUTSIDE_TO_WORD"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->OUTSIDE_TO_WORD:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 12
    .line 13
    const-string v1, "OUTSIDE"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->OUTSIDE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 22
    .line 23
    const-string v1, "PDF_TOOLS"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_TOOLS:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 30
    .line 31
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 32
    .line 33
    const-string v1, "SHARE"

    .line 34
    .line 35
    const/4 v2, 0x3

    .line 36
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->SHARE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 40
    .line 41
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 42
    .line 43
    const-string v1, "DOCUMENT_OPERATION"

    .line 44
    .line 45
    const/4 v2, 0x4

    .line 46
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->DOCUMENT_OPERATION:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 50
    .line 51
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 52
    .line 53
    const-string v1, "DOCUMENT_MORE"

    .line 54
    .line 55
    const/4 v2, 0x5

    .line 56
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->DOCUMENT_MORE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 60
    .line 61
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 62
    .line 63
    const-string v1, "PDF_PREVIEW"

    .line 64
    .line 65
    const/4 v2, 0x6

    .line 66
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 67
    .line 68
    .line 69
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_PREVIEW:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 70
    .line 71
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 72
    .line 73
    const-string v1, "IMAGE_DETAIL"

    .line 74
    .line 75
    const/4 v2, 0x7

    .line 76
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 77
    .line 78
    .line 79
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->IMAGE_DETAIL:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 80
    .line 81
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 82
    .line 83
    const-string v1, "MAIN"

    .line 84
    .line 85
    const/16 v2, 0x8

    .line 86
    .line 87
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 88
    .line 89
    .line 90
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->MAIN:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 91
    .line 92
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 93
    .line 94
    const-string v1, "PDF_PREVIEW_TO_WORD"

    .line 95
    .line 96
    const/16 v2, 0x9

    .line 97
    .line 98
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 99
    .line 100
    .line 101
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_PREVIEW_TO_WORD:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 102
    .line 103
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 104
    .line 105
    const-string v1, "IMAGE_TO_OFFICE"

    .line 106
    .line 107
    const/16 v2, 0xa

    .line 108
    .line 109
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;-><init>(Ljava/lang/String;I)V

    .line 110
    .line 111
    .line 112
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->IMAGE_TO_OFFICE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 113
    .line 114
    invoke-static {}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->$values()[Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    sput-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->$VALUES:[Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->$VALUES:[Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

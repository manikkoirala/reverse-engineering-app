.class public final Lcom/intsig/camscanner/pdf/office/PdfToOfficeEngineCore$Companion;
.super Ljava/lang/Object;
.source "PdfToOfficeEngineCore.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdf/office/PdfToOfficeEngineCore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeEngineCore$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "go2OpenDoc()"

    .line 7
    .line 8
    const-string v1, "PdfToOfficeEngineCore"

    .line 9
    .line 10
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    const/4 v2, 0x0

    .line 15
    if-eqz p2, :cond_1

    .line 16
    .line 17
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    if-nez v3, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v3, 0x0

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    :goto_0
    const/4 v3, 0x1

    .line 27
    :goto_1
    if-eqz v3, :cond_2

    .line 28
    .line 29
    return-void

    .line 30
    :cond_2
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    .line 31
    .line 32
    const-string v4, "android.intent.action.VIEW"

    .line 33
    .line 34
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const-string v4, "android.intent.category.DEFAULT"

    .line 38
    .line 39
    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    .line 41
    .line 42
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 43
    .line 44
    const/16 v5, 0x18

    .line 45
    .line 46
    if-lt v4, v5, :cond_3

    .line 47
    .line 48
    invoke-static {p1, p2}, Lcom/intsig/utils/FileUtil;->O8ooOoo〇(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    const-string v5, "getUriFromFileProvider(context, downloadPath)"

    .line 53
    .line 54
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 58
    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_3
    new-instance v0, Ljava/io/File;

    .line 62
    .line 63
    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    .line 67
    .line 68
    .line 69
    move-result-object v4

    .line 70
    const-string v0, "fromFile(File(downloadPath))"

    .line 71
    .line 72
    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    :goto_2
    const/high16 v0, 0x10000000

    .line 76
    .line 77
    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 78
    .line 79
    .line 80
    const-string v0, ".xlsx"

    .line 81
    .line 82
    const/4 v5, 0x0

    .line 83
    const/4 v6, 0x2

    .line 84
    invoke-static {p2, v0, v2, v6, v5}, Lkotlin/text/StringsKt;->〇〇8O0〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    if-eqz v0, :cond_4

    .line 89
    .line 90
    const-string p2, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    .line 91
    .line 92
    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    .line 94
    .line 95
    goto :goto_3

    .line 96
    :cond_4
    const-string v0, ".pptx"

    .line 97
    .line 98
    invoke-static {p2, v0, v2, v6, v5}, Lkotlin/text/StringsKt;->〇〇8O0〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    move-result p2

    .line 102
    if-eqz p2, :cond_5

    .line 103
    .line 104
    const-string p2, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    .line 105
    .line 106
    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    .line 108
    .line 109
    goto :goto_3

    .line 110
    :cond_5
    const-string p2, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    .line 111
    .line 112
    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    .line 114
    .line 115
    :goto_3
    invoke-virtual {p1, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    .line 117
    .line 118
    goto :goto_4

    .line 119
    :catch_0
    move-exception p2

    .line 120
    const v0, 0x7f1302fb

    .line 121
    .line 122
    .line 123
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 124
    .line 125
    .line 126
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 127
    .line 128
    .line 129
    :goto_4
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final 〇o00〇〇Oo(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "PdfToOfficeEngineCore"

    .line 7
    .line 8
    const-string v1, "go2OpenOfficeDoc()"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    const/4 v1, 0x0

    .line 15
    if-eqz p2, :cond_1

    .line 16
    .line 17
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-nez v2, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v2, 0x0

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 27
    :goto_1
    if-eqz v2, :cond_2

    .line 28
    .line 29
    return-void

    .line 30
    :cond_2
    new-instance v2, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;

    .line 31
    .line 32
    const-string v3, ""

    .line 33
    .line 34
    invoke-direct {v2, p2, v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const-string p2, "FROM_PDF_TO_OFFICE"

    .line 38
    .line 39
    invoke-virtual {v2, p2}, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;->setFrom(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 43
    .line 44
    .line 45
    move-result-object p2

    .line 46
    new-array v0, v0, [Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;

    .line 47
    .line 48
    aput-object v2, v0, v1

    .line 49
    .line 50
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    new-instance v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeEngineCore$Companion$go2OpenOfficeDoc$1;

    .line 55
    .line 56
    invoke-direct {v2, p1, p3}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeEngineCore$Companion$go2OpenOfficeDoc$1;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    const/4 p1, 0x0

    .line 60
    invoke-static {p2, v0, p1, v2, v1}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportOfficeByPath(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;Z)V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final 〇o〇(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;)V
    .locals 4

    .line 1
    const-string v0, "PdfToOfficeEngineCore"

    .line 2
    .line 3
    const-string v1, "go2Share()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    if-eqz p2, :cond_1

    .line 10
    .line 11
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-nez v1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v1, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 21
    :goto_1
    if-eqz v1, :cond_2

    .line 22
    .line 23
    return-void

    .line 24
    :cond_2
    const-string v1, ".xlsx"

    .line 25
    .line 26
    const/4 v2, 0x2

    .line 27
    const/4 v3, 0x0

    .line 28
    invoke-static {p2, v1, v0, v2, v3}, Lkotlin/text/StringsKt;->〇〇8O0〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-eqz v1, :cond_3

    .line 33
    .line 34
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareOffice;

    .line 35
    .line 36
    const-string v1, "EXCEL"

    .line 37
    .line 38
    invoke-direct {v0, p1, v1, p2}, Lcom/intsig/camscanner/share/type/ShareOffice;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    goto :goto_2

    .line 42
    :cond_3
    const-string v1, ".pptx"

    .line 43
    .line 44
    invoke-static {p2, v1, v0, v2, v3}, Lkotlin/text/StringsKt;->〇〇8O0〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-eqz v0, :cond_4

    .line 49
    .line 50
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareOffice;

    .line 51
    .line 52
    const-string v1, "PPT"

    .line 53
    .line 54
    invoke-direct {v0, p1, v1, p2}, Lcom/intsig/camscanner/share/type/ShareOffice;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    goto :goto_2

    .line 58
    :cond_4
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareOffice;

    .line 59
    .line 60
    const-string v1, "WORD"

    .line 61
    .line 62
    invoke-direct {v0, p1, v1, p2}, Lcom/intsig/camscanner/share/type/ShareOffice;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    :goto_2
    invoke-static {p1}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

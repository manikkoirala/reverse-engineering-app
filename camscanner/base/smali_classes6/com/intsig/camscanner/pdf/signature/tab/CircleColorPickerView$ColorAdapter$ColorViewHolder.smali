.class public final Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter$ColorViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "CircleColorPickerView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ColorViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter;Landroid/widget/ImageView;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "itemView"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter$ColorViewHolder;->o0:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter;

    .line 7
    .line 8
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p1, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter;->o0:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 12
    .line 13
    new-instance v0, LO80OO/〇080;

    .line 14
    .line 15
    invoke-direct {v0, p0, p1}, LO80OO/〇080;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter$ColorViewHolder;Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O〇8O8〇008(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter$ColorViewHolder;Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "this$1"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 16
    .line 17
    .line 18
    move-result p2

    .line 19
    if-eqz p2, :cond_2

    .line 20
    .line 21
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getBindingAdapterPosition()I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->〇o〇(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;)Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    invoke-static {p2, p0}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object p2

    .line 33
    check-cast p2, Ljava/lang/Integer;

    .line 34
    .line 35
    if-eqz p2, :cond_0

    .line 36
    .line 37
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 38
    .line 39
    .line 40
    move-result p2

    .line 41
    goto :goto_0

    .line 42
    :cond_0
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->〇o〇(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;)Ljava/util/List;

    .line 43
    .line 44
    .line 45
    move-result-object p2

    .line 46
    const/4 v0, 0x0

    .line 47
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    check-cast p2, Ljava/lang/Number;

    .line 52
    .line 53
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    .line 54
    .line 55
    .line 56
    move-result p2

    .line 57
    :goto_0
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setCurrentSelectIndex(I)V

    .line 58
    .line 59
    .line 60
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;)Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    if-eqz v0, :cond_1

    .line 65
    .line 66
    invoke-interface {v0, p0, p2}, Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;->oo(II)V

    .line 67
    .line 68
    .line 69
    :cond_1
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->Oo08(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;)Lcom/intsig/utils/VibratorClient;

    .line 70
    .line 71
    .line 72
    move-result-object p0

    .line 73
    invoke-virtual {p0}, Lcom/intsig/utils/VibratorClient;->〇080()V

    .line 74
    .line 75
    .line 76
    :cond_2
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇00(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter$ColorViewHolder;Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter$ColorViewHolder;->O〇8O8〇008(Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView$ColorAdapter$ColorViewHolder;Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.class public final Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "PdfSignatureNewActivity.kt"

# interfaces
.implements Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;
.implements Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$View;
.implements Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;
.implements Lcom/intsig/camscanner/pdf/signature/tab/ISignatureEditView;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "StringFormatMatches"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic oOo〇08〇:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇80O8o8O〇:Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:I

.field private O88O:Z

.field private O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

.field private final O8〇o〇88:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇OOo:Z

.field private Oo0O0o8:I

.field private Oo0〇Ooo:Z

.field private Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

.field private Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

.field private O〇08oOOO0:I

.field private final O〇O:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇o88o08〇:I

.field private o0OoOOo0:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8O:Lcom/intsig/app/BaseProgressDialog;

.field private o8o:Z

.field private o8oOOo:Ljava/lang/String;

.field private o8〇OO:F

.field private oO00〇o:Lcom/intsig/app/BaseProgressDialog;

.field private oOO0880O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOO8:Z

.field private oOO〇〇:Z

.field private oOoo80oO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/signature/PdfPageModel;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO〇8O8oOo:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

.field private oo8ooo8O:I

.field private ooO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;"
        }
    .end annotation
.end field

.field private ooo0〇〇O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;"
        }
    .end annotation
.end field

.field private final o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇o〇Oo88:Z

.field private 〇00O0:Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;

.field private final 〇08〇o0O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O〇O00O:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇800OO〇0O:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8〇o88:Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OO8ooO8〇:I

.field private 〇OO〇00〇0O:Z

.field private 〇O〇〇O8:Ljava/lang/String;

.field private 〇o0O:I

.field private 〇oo〇O〇80:Z

.field private 〇〇08O:J

.field private 〇〇o〇:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

.field private 〇〇〇0o〇〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOo〇08〇:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇80O8o8O〇:Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignaturePresenter;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignaturePresenter;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$View;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 12
    .line 13
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 14
    .line 15
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇08〇o0O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 19
    .line 20
    const/4 v0, -0x1

    .line 21
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇o88o08〇:I

    .line 22
    .line 23
    const/high16 v1, -0x1000000

    .line 24
    .line 25
    iput v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇08oOOO0:I

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇〇0o〇〇0:I

    .line 28
    .line 29
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 30
    .line 31
    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0O〇O00O:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 35
    .line 36
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    .line 37
    .line 38
    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 39
    .line 40
    .line 41
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0OoOOo0:Ljava/util/concurrent/ConcurrentHashMap;

    .line 42
    .line 43
    const-string v1, ""

    .line 44
    .line 45
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO0880O:Ljava/lang/String;

    .line 46
    .line 47
    new-instance v1, Ljava/util/ArrayList;

    .line 48
    .line 49
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .line 51
    .line 52
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOoo80oO:Ljava/util/List;

    .line 53
    .line 54
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0O0o8:I

    .line 55
    .line 56
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    .line 57
    .line 58
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 59
    .line 60
    .line 61
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇800OO〇0O:Ljava/util/concurrent/ConcurrentHashMap;

    .line 62
    .line 63
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 64
    .line 65
    sget-object v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mEnableScale$2;->o0:Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mEnableScale$2;

    .line 66
    .line 67
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O8〇o〇88:Lkotlin/Lazy;

    .line 72
    .line 73
    new-instance v0, LO80OO/Oooo8o0〇;

    .line 74
    .line 75
    invoke-direct {v0, p0}, LO80OO/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 76
    .line 77
    .line 78
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇O:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;

    .line 79
    .line 80
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mSeekBarChangeListener$1;

    .line 81
    .line 82
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mSeekBarChangeListener$1;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 83
    .line 84
    .line 85
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8〇o88:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final O008o8oo(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "PdfSignatureNewActivity"

    .line 7
    .line 8
    const-string p2, "User Operation:  onclick not save"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "confirm_leave"

    .line 14
    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0oO(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇00o〇O8()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final O008oO0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo〇〇oO()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O00OoO〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0OoOOo0:Ljava/util/concurrent/ConcurrentHashMap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O088O()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/util/Collection;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x1

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 19
    :goto_1
    if-eqz v0, :cond_2

    .line 20
    .line 21
    const-string v0, "PdfSignatureNewActivity"

    .line 22
    .line 23
    const-string v2, "checkParamsOk mImageUrls is null or empty"

    .line 24
    .line 25
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return v1

    .line 29
    :cond_2
    return v2
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O08o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O08〇oO8〇()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->getPageCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    move-object v1, v2

    .line 16
    :goto_0
    const/4 v3, 0x1

    .line 17
    if-le v0, v3, :cond_9

    .line 18
    .line 19
    check-cast v1, Ljava/util/Collection;

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_1
    const/4 v3, 0x0

    .line 32
    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    .line 33
    .line 34
    goto :goto_4

    .line 35
    :cond_3
    const/4 v1, 0x0

    .line 36
    :goto_2
    if-ge v1, v0, :cond_7

    .line 37
    .line 38
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0O〇O00O:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 39
    .line 40
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 41
    .line 42
    .line 43
    move-result-object v5

    .line 44
    invoke-virtual {v3, v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    move-result v3

    .line 48
    if-nez v3, :cond_4

    .line 49
    .line 50
    goto :goto_3

    .line 51
    :cond_4
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 52
    .line 53
    if-eqz v3, :cond_5

    .line 54
    .line 55
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    if-eqz v3, :cond_5

    .line 60
    .line 61
    invoke-static {v3, v1}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    check-cast v3, Ljava/util/List;

    .line 66
    .line 67
    if-eqz v3, :cond_5

    .line 68
    .line 69
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->〇0000OOO(Ljava/util/List;)Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    check-cast v3, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;

    .line 74
    .line 75
    :cond_5
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 76
    .line 77
    if-eqz v3, :cond_6

    .line 78
    .line 79
    invoke-virtual {v3, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 80
    .line 81
    .line 82
    :cond_6
    :goto_3
    add-int/lit8 v1, v1, 0x1

    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_7
    iput-object v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 86
    .line 87
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    if-eqz v0, :cond_8

    .line 92
    .line 93
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 94
    .line 95
    if-eqz v0, :cond_8

    .line 96
    .line 97
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->〇0〇O0088o(Z)V

    .line 98
    .line 99
    .line 100
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0O〇O00O:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 101
    .line 102
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 103
    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0OoOOo0:Ljava/util/concurrent/ConcurrentHashMap;

    .line 106
    .line 107
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 108
    .line 109
    .line 110
    const-string v0, ""

    .line 111
    .line 112
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO0880O:Ljava/lang/String;

    .line 113
    .line 114
    :cond_9
    :goto_4
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O0o0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8ooOO(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O0oO(Ljava/lang/String;)V
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v1, Landroid/util/Pair;

    .line 5
    .line 6
    const-string v2, "from"

    .line 7
    .line 8
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8oOOo:Ljava/lang/String;

    .line 9
    .line 10
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    aput-object v1, v0, v2

    .line 15
    .line 16
    new-instance v1, Landroid/util/Pair;

    .line 17
    .line 18
    const-string v2, "from_part"

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 21
    .line 22
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    const/4 v2, 0x1

    .line 26
    aput-object v1, v0, v2

    .line 27
    .line 28
    const-string v1, "CSAddSignature"

    .line 29
    .line 30
    invoke-static {v1, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0O〇8800(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O0〇8〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "PdfSignatureNewActivity"

    .line 7
    .line 8
    const-string p2, "User Operation:  onclick upgrade now"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "CSFreeSignature"

    .line 14
    .line 15
    const-string p2, "upgrade_now"

    .line 16
    .line 17
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 21
    .line 22
    invoke-direct {p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 23
    .line 24
    .line 25
    sget-object p2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SAVE_PDF_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 26
    .line 27
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    sget-object p2, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 32
    .line 33
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    const/16 p2, 0x64

    .line 38
    .line 39
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O80OO(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Landroidx/recyclerview/widget/RecyclerView$LayoutManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O88(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/pdf/signature/PdfPageModel;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0〇〇00〇o(Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/pdf/signature/PdfPageModel;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O880O〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0888(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O888Oo(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 4
    .line 5
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;)V

    .line 6
    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇8O0O80〇()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇00(Z)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 25
    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    move-object v0, v1

    .line 34
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 43
    .line 44
    if-eqz v0, :cond_1

    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 49
    .line 50
    .line 51
    new-instance v2, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initAdapter$1$1;

    .line 52
    .line 53
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initAdapter$1$1;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 57
    .line 58
    .line 59
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 60
    .line 61
    if-eqz v0, :cond_2

    .line 62
    .line 63
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->O〇8O8〇008(Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;)V

    .line 64
    .line 65
    .line 66
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    .line 67
    .line 68
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .line 70
    .line 71
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    if-eqz v3, :cond_5

    .line 80
    .line 81
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    check-cast v3, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 86
    .line 87
    new-instance v4, Lcom/intsig/camscanner/util/ParcelSize;

    .line 88
    .line 89
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getImageWidth()I

    .line 90
    .line 91
    .line 92
    move-result v5

    .line 93
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getImageHeight()I

    .line 94
    .line 95
    .line 96
    move-result v6

    .line 97
    invoke-direct {v4, v5, v6}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    int-to-double v5, v5

    .line 105
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 106
    .line 107
    .line 108
    move-result v7

    .line 109
    int-to-double v7, v7

    .line 110
    div-double/2addr v5, v7

    .line 111
    invoke-direct {p0, v4, v5, v6}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇oo8O80(Lcom/intsig/camscanner/util/ParcelSize;D)Lcom/intsig/camscanner/util/ParcelSize;

    .line 112
    .line 113
    .line 114
    move-result-object v5

    .line 115
    iget v6, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 116
    .line 117
    if-lez v6, :cond_4

    .line 118
    .line 119
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 120
    .line 121
    invoke-interface {v6}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 122
    .line 123
    .line 124
    move-result v6

    .line 125
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 126
    .line 127
    .line 128
    move-result v7

    .line 129
    mul-int v6, v6, v7

    .line 130
    .line 131
    iget v7, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 132
    .line 133
    div-int/2addr v6, v7

    .line 134
    goto :goto_2

    .line 135
    :cond_4
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 136
    .line 137
    invoke-interface {v6}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 138
    .line 139
    .line 140
    move-result v6

    .line 141
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 142
    .line 143
    .line 144
    move-result v7

    .line 145
    mul-int v6, v6, v7

    .line 146
    .line 147
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 148
    .line 149
    .line 150
    move-result v7

    .line 151
    div-int/2addr v6, v7

    .line 152
    :goto_2
    iget-object v7, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 153
    .line 154
    invoke-interface {v7}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 155
    .line 156
    .line 157
    move-result v7

    .line 158
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 159
    .line 160
    .line 161
    move-result v8

    .line 162
    sub-int/2addr v7, v8

    .line 163
    div-int/lit8 v7, v7, 0x2

    .line 164
    .line 165
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 166
    .line 167
    .line 168
    move-result v8

    .line 169
    sub-int/2addr v6, v8

    .line 170
    div-int/lit8 v6, v6, 0x2

    .line 171
    .line 172
    new-instance v8, Landroid/graphics/Rect;

    .line 173
    .line 174
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 175
    .line 176
    .line 177
    move-result v9

    .line 178
    add-int/2addr v9, v7

    .line 179
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 180
    .line 181
    .line 182
    move-result v10

    .line 183
    add-int/2addr v10, v6

    .line 184
    invoke-direct {v8, v7, v6, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 185
    .line 186
    .line 187
    new-instance v6, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 188
    .line 189
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPath()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    invoke-direct {v6, v3, v4, v5, v8}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/util/ParcelSize;Lcom/intsig/camscanner/util/ParcelSize;Landroid/graphics/Rect;)V

    .line 194
    .line 195
    .line 196
    new-instance v3, Ljava/util/ArrayList;

    .line 197
    .line 198
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .line 200
    .line 201
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    .line 203
    .line 204
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    .line 206
    .line 207
    :try_start_0
    invoke-static {v6}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object v3

    .line 211
    const-class v4, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 212
    .line 213
    invoke-static {v3, v4}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 214
    .line 215
    .line 216
    move-result-object v3

    .line 217
    check-cast v3, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    .line 219
    goto :goto_3

    .line 220
    :catch_0
    move-exception v3

    .line 221
    const-string v4, "PdfSignatureNewActivity"

    .line 222
    .line 223
    const-string v5, "copy"

    .line 224
    .line 225
    invoke-static {v4, v5, v3}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 226
    .line 227
    .line 228
    move-object v3, v1

    .line 229
    :goto_3
    check-cast v3, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 230
    .line 231
    if-eqz v3, :cond_3

    .line 232
    .line 233
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOoo80oO:Ljava/util/List;

    .line 234
    .line 235
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    .line 237
    .line 238
    goto/16 :goto_1

    .line 239
    .line 240
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 241
    .line 242
    if-eqz v1, :cond_6

    .line 243
    .line 244
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 245
    .line 246
    const v3, 0x7fffffff

    .line 247
    .line 248
    .line 249
    invoke-virtual {v1, v0, p1, v2, v3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇O888o0o(Ljava/util/List;Ljava/util/List;II)V

    .line 250
    .line 251
    .line 252
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 253
    .line 254
    .line 255
    move-result-object p1

    .line 256
    if-eqz p1, :cond_7

    .line 257
    .line 258
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 259
    .line 260
    if-eqz p1, :cond_7

    .line 261
    .line 262
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->o0ooO()V

    .line 263
    .line 264
    .line 265
    :cond_7
    return-void
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic O8O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇o8〇〇O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O8〇()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO8〇0OO8(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO0880O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OO0O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8〇0O〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OO0o(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO0〇O()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O〇8O8〇008()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const-string v1, "PdfSignatureNewActivity"

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const-string v0, "add new signature"

    .line 20
    .line 21
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇80O80O〇0()V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const-string v0, "User Operation:  onclick generate signature but reach max number"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 34
    .line 35
    const/4 v1, 0x1

    .line 36
    new-array v1, v1, [Ljava/lang/Object;

    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇80〇808〇O()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    const/4 v3, 0x0

    .line 47
    aput-object v2, v1, v3

    .line 48
    .line 49
    const v2, 0x7f13021c

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_1
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic OO8〇O8(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇O80o8OO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OOo00(ILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;)V
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->getPageCount()I

    .line 2
    .line 3
    .line 4
    move-result v2

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    move-object v4, v0

    .line 16
    const/4 v0, 0x0

    .line 17
    const/4 v1, 0x1

    .line 18
    if-le v2, v1, :cond_4

    .line 19
    .line 20
    move-object v3, v4

    .line 21
    check-cast v3, Ljava/util/Collection;

    .line 22
    .line 23
    if-eqz v3, :cond_2

    .line 24
    .line 25
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    if-eqz v3, :cond_1

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    const/4 v1, 0x0

    .line 33
    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_3
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 37
    .line 38
    .line 39
    move-result-object v7

    .line 40
    const/4 v8, 0x0

    .line 41
    const/4 v9, 0x0

    .line 42
    new-instance v10, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$applyToAll$1;

    .line 43
    .line 44
    const/4 v6, 0x0

    .line 45
    move-object v0, v10

    .line 46
    move-object v1, p0

    .line 47
    move v3, p1

    .line 48
    move-object v5, p2

    .line 49
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$applyToAll$1;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;IILjava/util/List;Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lkotlin/coroutines/Continuation;)V

    .line 50
    .line 51
    .line 52
    const/4 p1, 0x3

    .line 53
    const/4 p2, 0x0

    .line 54
    move-object v5, v7

    .line 55
    move-object v6, v8

    .line 56
    move-object v7, v9

    .line 57
    move-object v8, v10

    .line 58
    move v9, p1

    .line 59
    move-object v10, p2

    .line 60
    invoke-static/range {v5 .. v10}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 61
    .line 62
    .line 63
    return-void

    .line 64
    :cond_4
    :goto_2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0〇Ooo:Z

    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final OO〇000()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO00〇o:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-ne v0, v2, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    :cond_0
    if-eqz v1, :cond_1

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO00〇o:Lcom/intsig/app/BaseProgressDialog;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 21
    .line 22
    .line 23
    :cond_1
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final OO〇80oO〇(ILcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string p3, "User Operation:  onclick continue ,lastSaveTime = "

    .line 12
    .line 13
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p2

    .line 23
    const-string p3, "PdfSignatureNewActivity"

    .line 24
    .line 25
    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO8()V

    .line 29
    .line 30
    .line 31
    add-int/lit8 p0, p0, -0x1

    .line 32
    .line 33
    invoke-static {p0}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇00(I)V

    .line 34
    .line 35
    .line 36
    const-string p0, "CSFreeSignature"

    .line 37
    .line 38
    const-string p1, "continue"

    .line 39
    .line 40
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic OO〇〇o0oO(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final Oo0O〇8800(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->OO0〇O()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OoO〇OOo8o(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O008o8oo(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OooO〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0〇8〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O〇00O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇00O0:Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇00o08()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_6

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->〇080:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->Oo08()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    goto/16 :goto_2

    .line 16
    .line 17
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo8O8o80()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    return-void

    .line 24
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const/4 v1, 0x1

    .line 29
    const/4 v2, 0x0

    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 33
    .line 34
    if-eqz v0, :cond_2

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->〇o()Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    if-ne v0, v1, :cond_2

    .line 41
    .line 42
    const/4 v0, 0x1

    .line 43
    goto :goto_0

    .line 44
    :cond_2
    const/4 v0, 0x0

    .line 45
    :goto_0
    if-nez v0, :cond_3

    .line 46
    .line 47
    return-void

    .line 48
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    if-eqz v3, :cond_4

    .line 57
    .line 58
    invoke-virtual {v3}, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 59
    .line 60
    .line 61
    move-result-object v3

    .line 62
    goto :goto_1

    .line 63
    :cond_4
    const/4 v3, 0x0

    .line 64
    :goto_1
    const v4, 0x7f0d0595

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v4, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    invoke-virtual {v0, v3, v4}, Landroid/view/View;->measure(II)V

    .line 80
    .line 81
    .line 82
    new-instance v3, Landroid/widget/PopupWindow;

    .line 83
    .line 84
    const/4 v4, -0x2

    .line 85
    invoke-direct {v3, v0, v4, v4, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v3, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v3, v1}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    .line 92
    .line 93
    .line 94
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 95
    .line 96
    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {v3, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 100
    .line 101
    .line 102
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    if-eqz v1, :cond_5

    .line 107
    .line 108
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 109
    .line 110
    if-eqz v1, :cond_5

    .line 111
    .line 112
    invoke-static {p0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 117
    .line 118
    .line 119
    move-result v4

    .line 120
    sub-int/2addr v2, v4

    .line 121
    div-int/lit8 v2, v2, 0x2

    .line 122
    .line 123
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 124
    .line 125
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 126
    .line 127
    .line 128
    move-result-object v5

    .line 129
    const/16 v6, -0x8f

    .line 130
    .line 131
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 132
    .line 133
    .line 134
    move-result v5

    .line 135
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    sub-int/2addr v5, v0

    .line 140
    const/16 v0, 0xc

    .line 141
    .line 142
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 143
    .line 144
    .line 145
    move-result-object v4

    .line 146
    invoke-static {v4, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    add-int/2addr v5, v0

    .line 151
    invoke-virtual {v3, v1, v2, v5}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 152
    .line 153
    .line 154
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->update()V

    .line 155
    .line 156
    .line 157
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo〇OO()V

    .line 158
    .line 159
    .line 160
    :cond_6
    :goto_2
    return-void
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8〇8o00(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O〇0O〇Oo〇o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O08o(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O〇0o8o8〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO88〇0O8O(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇0o8〇(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o88oo〇O(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    if-nez p1, :cond_1

    .line 12
    .line 13
    return-void

    .line 14
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇o88o08〇:I

    .line 15
    .line 16
    if-ltz v0, :cond_3

    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8ooOO(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-nez v0, :cond_2

    .line 23
    .line 24
    return-void

    .line 25
    :cond_2
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇O〇〇〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    if-eqz p1, :cond_3

    .line 33
    .line 34
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 35
    .line 36
    if-eqz p1, :cond_3

    .line 37
    .line 38
    iget v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇o88o08〇:I

    .line 39
    .line 40
    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForLayoutPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    if-eqz p1, :cond_3

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 47
    .line 48
    if-eqz v1, :cond_3

    .line 49
    .line 50
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇o88o08〇:I

    .line 51
    .line 52
    const/4 v3, 0x1

    .line 53
    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇O00(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;ILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Z)V

    .line 54
    .line 55
    .line 56
    :cond_3
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O〇8()Lcom/intsig/camscanner/pdf/signature/tab/ISignatureStrategy;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->getCurSignatureStrategy()Lcom/intsig/camscanner/pdf/signature/tab/ISignatureStrategy;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O〇88()V
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "type"

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    const-string v2, "premium"

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const-string v2, "basic"

    .line 18
    .line 19
    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 20
    .line 21
    .line 22
    const-string v1, "login_status"

    .line 23
    .line 24
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-eqz v2, :cond_1

    .line 29
    .line 30
    const-string v2, "logged_in"

    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_1
    const-string v2, "no_logged_in"

    .line 34
    .line 35
    :goto_1
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 36
    .line 37
    .line 38
    iget v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇o0O:I

    .line 39
    .line 40
    sget-object v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_HOME_TAB:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    if-ne v1, v2, :cond_2

    .line 47
    .line 48
    const-string v1, "cs_home_tab"

    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_2
    iget v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇o0O:I

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_CS_SCAN:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 54
    .line 55
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    if-ne v1, v2, :cond_3

    .line 60
    .line 61
    const-string v1, "cs_scan"

    .line 62
    .line 63
    goto :goto_2

    .line 64
    :cond_3
    const-string v1, "cs_pdfpreview"

    .line 65
    .line 66
    :goto_2
    const-string v2, "from_part"

    .line 67
    .line 68
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .line 70
    .line 71
    goto :goto_3

    .line 72
    :catch_0
    move-exception v1

    .line 73
    const-string v2, "PdfSignatureNewActivity"

    .line 74
    .line 75
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 76
    .line 77
    .line 78
    :goto_3
    const-string v1, "CSAddSignature"

    .line 79
    .line 80
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 81
    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O〇8O0O80〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O8〇o〇88:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O〇O800oo()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->getCurTabType()Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    if-nez v0, :cond_1

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-nez v1, :cond_2

    .line 25
    .line 26
    const-string v0, "signature"

    .line 27
    .line 28
    goto :goto_5

    .line 29
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 30
    .line 31
    goto :goto_2

    .line 32
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    const/4 v2, 0x1

    .line 37
    if-ne v1, v2, :cond_4

    .line 38
    .line 39
    const-string v0, "seal"

    .line 40
    .line 41
    goto :goto_5

    .line 42
    :cond_4
    :goto_2
    if-nez v0, :cond_5

    .line 43
    .line 44
    goto :goto_3

    .line 45
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    const/4 v2, 0x2

    .line 50
    if-ne v1, v2, :cond_6

    .line 51
    .line 52
    const-string v0, "logo"

    .line 53
    .line 54
    goto :goto_5

    .line 55
    :cond_6
    :goto_3
    if-nez v0, :cond_7

    .line 56
    .line 57
    goto :goto_4

    .line 58
    :cond_7
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    const/4 v1, 0x3

    .line 63
    if-ne v0, v1, :cond_8

    .line 64
    .line 65
    const-string v0, "paging_seal"

    .line 66
    .line 67
    goto :goto_5

    .line 68
    :cond_8
    :goto_4
    const-string v0, ""

    .line 69
    .line 70
    :goto_5
    return-object v0
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O〇o8(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0〇OO008O(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O〇oo8O80(Lcom/intsig/camscanner/util/ParcelSize;D)Lcom/intsig/camscanner/util/ParcelSize;
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 2
    .line 3
    if-lez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    mul-int v0, v0, v1

    .line 16
    .line 17
    iget v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 18
    .line 19
    div-int/2addr v0, v1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 22
    .line 23
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_0
    int-to-double v1, v0

    .line 28
    mul-double v3, v1, p2

    .line 29
    .line 30
    double-to-int v3, v3

    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    int-to-double v4, v4

    .line 36
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 37
    .line 38
    mul-double v4, v4, v6

    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    int-to-double v6, v6

    .line 45
    div-double/2addr v4, v6

    .line 46
    cmpl-double v6, v4, p2

    .line 47
    .line 48
    if-ltz v6, :cond_1

    .line 49
    .line 50
    int-to-double v0, v3

    .line 51
    div-double/2addr v0, v4

    .line 52
    double-to-int v0, v0

    .line 53
    goto :goto_1

    .line 54
    :cond_1
    mul-double v1, v1, v4

    .line 55
    .line 56
    double-to-int v3, v1

    .line 57
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    .line 66
    .line 67
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .line 69
    .line 70
    const-string v4, "sourceSize.width = "

    .line 71
    .line 72
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const-string v1, "  sourceSize.height = "

    .line 79
    .line 80
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string p1, "  destWidth = "

    .line 87
    .line 88
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string p1, "  destHeight = "

    .line 95
    .line 96
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    const-string p1, "  ratio = "

    .line 103
    .line 104
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    const-string p2, "PdfSignatureNewActivity"

    .line 115
    .line 116
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    new-instance p1, Lcom/intsig/camscanner/util/ParcelSize;

    .line 120
    .line 121
    invoke-direct {p1, v0, v3}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 122
    .line 123
    .line 124
    return-object p1
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O008oO0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O〇〇o8O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0O〇O00O:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o088O8800(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p1

    .line 6
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;

    .line 7
    .line 8
    iget v1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->o〇00O:I

    .line 9
    .line 10
    const/high16 v2, -0x80000000

    .line 11
    .line 12
    and-int v3, v1, v2

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    sub-int/2addr v1, v2

    .line 17
    iput v1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->o〇00O:I

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;

    .line 21
    .line 22
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lkotlin/coroutines/Continuation;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    iget-object p1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->OO:Ljava/lang/Object;

    .line 26
    .line 27
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget v2, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->o〇00O:I

    .line 32
    .line 33
    const/4 v3, 0x1

    .line 34
    if-eqz v2, :cond_2

    .line 35
    .line 36
    if-ne v2, v3, :cond_1

    .line 37
    .line 38
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 39
    .line 40
    check-cast v1, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 41
    .line 42
    iget-object v0, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->o0:Ljava/lang/Object;

    .line 43
    .line 44
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;

    .line 45
    .line 46
    invoke-static {p1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 51
    .line 52
    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    .line 53
    .line 54
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    throw p1

    .line 58
    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOoo80oO:Ljava/util/List;

    .line 62
    .line 63
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->〇8〇0〇o〇O(Ljava/util/List;)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    check-cast p1, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 68
    .line 69
    if-nez p1, :cond_3

    .line 70
    .line 71
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 72
    .line 73
    return-object p1

    .line 74
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    if-eqz v2, :cond_7

    .line 79
    .line 80
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 81
    .line 82
    if-eqz v2, :cond_7

    .line 83
    .line 84
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 85
    .line 86
    .line 87
    move-result v2

    .line 88
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 89
    .line 90
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 91
    .line 92
    .line 93
    move-result-object v5

    .line 94
    const/16 v6, 0x1a

    .line 95
    .line 96
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 97
    .line 98
    .line 99
    move-result v5

    .line 100
    sub-int/2addr v2, v5

    .line 101
    const/16 v5, 0xaa

    .line 102
    .line 103
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 104
    .line 105
    .line 106
    move-result-object v4

    .line 107
    invoke-static {v4, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    sub-int/2addr v2, v4

    .line 112
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 113
    .line 114
    .line 115
    move-result-object v4

    .line 116
    new-instance v5, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$bitmap$1;

    .line 117
    .line 118
    const/4 v6, 0x0

    .line 119
    invoke-direct {v5, p1, v2, v6}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$bitmap$1;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfPageModel;ILkotlin/coroutines/Continuation;)V

    .line 120
    .line 121
    .line 122
    iput-object p0, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->o0:Ljava/lang/Object;

    .line 123
    .line 124
    iput-object p1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 125
    .line 126
    iput v3, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$bindPagingSealData$1;->o〇00O:I

    .line 127
    .line 128
    invoke-static {v4, v5, v0}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    if-ne v0, v1, :cond_4

    .line 133
    .line 134
    return-object v1

    .line 135
    :cond_4
    move-object v1, p1

    .line 136
    move-object p1, v0

    .line 137
    move-object v0, p0

    .line 138
    :goto_1
    check-cast p1, Landroid/graphics/Bitmap;

    .line 139
    .line 140
    if-nez p1, :cond_5

    .line 141
    .line 142
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 143
    .line 144
    return-object p1

    .line 145
    :cond_5
    invoke-direct {v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    if-eqz v0, :cond_6

    .line 150
    .line 151
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 152
    .line 153
    if-eqz v0, :cond_6

    .line 154
    .line 155
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getPath()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v1

    .line 159
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->OO0o〇〇(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    :cond_6
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 163
    .line 164
    return-object p1

    .line 165
    :cond_7
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 166
    .line 167
    return-object p1
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o088O8800(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o0OO(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o0Oo(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇o0oOO8(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o0〇OO008O(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 1
    const-string v0, "type"

    .line 2
    .line 3
    const-string v1, "from"

    .line 4
    .line 5
    const-string v2, "CSSignaturePop"

    .line 6
    .line 7
    invoke-static {v2, v0, p1, v1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o0〇〇00〇o(Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/pdf/signature/PdfPageModel;)Z
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object p1, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz p2, :cond_0

    .line 9
    .line 10
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    move-object v2, v1

    .line 16
    :goto_0
    if-eqz p2, :cond_1

    .line 17
    .line 18
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;->〇o〇()Lcom/intsig/camscanner/util/ParcelSize;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    :cond_1
    const/4 p2, 0x0

    .line 23
    if-eqz v0, :cond_3

    .line 24
    .line 25
    if-eqz p1, :cond_3

    .line 26
    .line 27
    if-eqz v2, :cond_3

    .line 28
    .line 29
    if-nez v1, :cond_2

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v4, "checkCanApply, sign: ["

    .line 38
    .line 39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v4, " - "

    .line 46
    .line 47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v5, "], page: ["

    .line 54
    .line 55
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    const-string v4, "]"

    .line 68
    .line 69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    const-string v4, "PdfSignatureNewActivity"

    .line 77
    .line 78
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 82
    .line 83
    iget v4, v2, Landroid/graphics/Rect;->top:I

    .line 84
    .line 85
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 86
    .line 87
    .line 88
    move-result v5

    .line 89
    add-int/2addr v4, v5

    .line 90
    if-ge v3, v4, :cond_3

    .line 91
    .line 92
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 93
    .line 94
    iget v4, v2, Landroid/graphics/Rect;->left:I

    .line 95
    .line 96
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    add-int/2addr v4, v1

    .line 101
    if-ge v3, v4, :cond_3

    .line 102
    .line 103
    iget v1, v0, Landroid/graphics/Rect;->top:I

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 106
    .line 107
    .line 108
    move-result v3

    .line 109
    add-int/2addr v1, v3

    .line 110
    iget v3, v2, Landroid/graphics/Rect;->top:I

    .line 111
    .line 112
    if-le v1, v3, :cond_3

    .line 113
    .line 114
    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 115
    .line 116
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 117
    .line 118
    .line 119
    move-result p1

    .line 120
    add-int/2addr v0, p1

    .line 121
    iget p1, v2, Landroid/graphics/Rect;->left:I

    .line 122
    .line 123
    if-le v0, p1, :cond_3

    .line 124
    .line 125
    const/4 p2, 0x1

    .line 126
    :cond_3
    :goto_1
    return p2
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇00〇〇〇o〇8(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o88o88(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o88oo〇O(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v2, 0x1

    .line 17
    if-ne v0, v2, :cond_0

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    :cond_0
    if-eqz v1, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    const/4 v3, 0x0

    .line 28
    const/4 v4, 0x0

    .line 29
    new-instance v5, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$addPagingSealSignature$1;

    .line 30
    .line 31
    const/4 v0, 0x0

    .line 32
    invoke-direct {v5, p0, p1, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$addPagingSealSignature$1;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;Lkotlin/coroutines/Continuation;)V

    .line 33
    .line 34
    .line 35
    const/4 v6, 0x3

    .line 36
    const/4 v7, 0x0

    .line 37
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic o8O〇008(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo008(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o8o0()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_2

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_2

    .line 11
    .line 12
    check-cast v0, Ljava/lang/Iterable;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_2

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Ljava/util/List;

    .line 29
    .line 30
    const-string v3, "innerList"

    .line 31
    .line 32
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    check-cast v2, Ljava/lang/Iterable;

    .line 36
    .line 37
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    if-eqz v3, :cond_0

    .line 46
    .line 47
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    check-cast v3, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;

    .line 52
    .line 53
    instance-of v3, v3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 54
    .line 55
    if-eqz v3, :cond_1

    .line 56
    .line 57
    const/4 v1, 0x1

    .line 58
    goto :goto_0

    .line 59
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v2, "hasAddedSignature == "

    .line 65
    .line 66
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    const-string v2, "PdfSignatureNewActivity"

    .line 77
    .line 78
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    return v1
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o8o0o8()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO8:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_2

    .line 11
    .line 12
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 13
    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->getSignatureData()Ljava/util/List;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-eqz v0, :cond_2

    .line 21
    .line 22
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇8〇0〇o〇O(Ljava/util/List;)Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    check-cast v0, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 27
    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇0o8〇(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 32
    .line 33
    .line 34
    :cond_2
    :goto_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o8oo0OOO()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f131d02

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-static {p0, v0, v1, v1}, Lcom/intsig/camscanner/app/AppUtil;->OOO〇O0(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO00〇o:Lcom/intsig/app/BaseProgressDialog;

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oO8()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    move-object v5, v0

    .line 12
    if-nez v5, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 18
    .line 19
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 20
    .line 21
    invoke-interface {v0, v5, v1, v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$IPageProperty;->o〇0(Ljava/util/List;Ljava/util/List;I)V

    .line 22
    .line 23
    .line 24
    new-instance v0, Ljava/util/HashMap;

    .line 25
    .line 26
    const/4 v1, 0x1

    .line 27
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 28
    .line 29
    .line 30
    const-string v2, "scheme"

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo8〇〇()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇OO〇00〇0O:Z

    .line 40
    .line 41
    if-nez v2, :cond_3

    .line 42
    .line 43
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo8(Ljava/util/HashMap;)Lorg/json/JSONObject;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8o0()Z

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    if-eqz v2, :cond_2

    .line 52
    .line 53
    const-string v2, "sign"

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_2
    const-string v2, "empty"

    .line 57
    .line 58
    :goto_1
    const-string v3, "sign_scheme"

    .line 59
    .line 60
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    .line 62
    .line 63
    const-string v2, "CSAddSignature"

    .line 64
    .line 65
    const-string v3, "save"

    .line 66
    .line 67
    invoke-static {v2, v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 68
    .line 69
    .line 70
    :cond_3
    iget-wide v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇08O:J

    .line 71
    .line 72
    iget-boolean v4, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O88O:Z

    .line 73
    .line 74
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 75
    .line 76
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 77
    .line 78
    if-lez v0, :cond_4

    .line 79
    .line 80
    const/4 v7, 0x1

    .line 81
    goto :goto_2

    .line 82
    :cond_4
    const/4 v0, 0x0

    .line 83
    const/4 v7, 0x0

    .line 84
    :goto_2
    move-object v1, p0

    .line 85
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$View;JZLjava/util/List;Ljava/util/List;Z)V

    .line 86
    .line 87
    .line 88
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final oO88〇0O8O(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p1

    .line 6
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;

    .line 7
    .line 8
    iget v1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;->〇08O〇00〇o:I

    .line 9
    .line 10
    const/high16 v2, -0x80000000

    .line 11
    .line 12
    and-int v3, v1, v2

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    sub-int/2addr v1, v2

    .line 17
    iput v1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;->〇08O〇00〇o:I

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;

    .line 21
    .line 22
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lkotlin/coroutines/Continuation;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    iget-object p1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 26
    .line 27
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget v2, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;->〇08O〇00〇o:I

    .line 32
    .line 33
    const/4 v3, 0x1

    .line 34
    if-eqz v2, :cond_2

    .line 35
    .line 36
    if-ne v2, v3, :cond_1

    .line 37
    .line 38
    iget-object v0, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;->o0:Ljava/lang/Object;

    .line 39
    .line 40
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;

    .line 41
    .line 42
    invoke-static {p1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 43
    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 47
    .line 48
    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    .line 49
    .line 50
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    throw p1

    .line 54
    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 55
    .line 56
    .line 57
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->OO〇OOo:Z

    .line 58
    .line 59
    if-nez p1, :cond_4

    .line 60
    .line 61
    iput-object p0, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;->o0:Ljava/lang/Object;

    .line 62
    .line 63
    iput v3, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initPagingSealData$1;->〇08O〇00〇o:I

    .line 64
    .line 65
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o088O8800(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    if-ne p1, v1, :cond_3

    .line 70
    .line 71
    return-object v1

    .line 72
    :cond_3
    move-object v0, p0

    .line 73
    :goto_1
    iput-boolean v3, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->OO〇OOo:Z

    .line 74
    .line 75
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 76
    .line 77
    return-object p1

    .line 78
    :cond_4
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 79
    .line 80
    return-object p1
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final oO8o〇08〇()V
    .locals 15

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "pdf_signature_has_signed"

    .line 6
    .line 7
    const-string v2, "is_from_pdf_kit_share"

    .line 8
    .line 9
    const-string v3, "is_local_doc"

    .line 10
    .line 11
    const-string v4, "log_agent_is_from_pdf_kit"

    .line 12
    .line 13
    const-string v5, "log_agent_from_part"

    .line 14
    .line 15
    const-string v6, "log_agent_from"

    .line 16
    .line 17
    const-string v7, "EXTRA_PDF_MAX_SIZE"

    .line 18
    .line 19
    const-wide/16 v8, 0x0

    .line 20
    .line 21
    const-string v10, "pdf_signature_doc_id"

    .line 22
    .line 23
    const-string v11, "pdf_signature_image_list"

    .line 24
    .line 25
    const/4 v12, 0x0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 29
    .line 30
    .line 31
    move-result-object v13

    .line 32
    check-cast v13, Ljava/util/List;

    .line 33
    .line 34
    iput-object v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 35
    .line 36
    invoke-virtual {v0, v10, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 37
    .line 38
    .line 39
    move-result-wide v13

    .line 40
    iput-wide v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇08O:J

    .line 41
    .line 42
    invoke-virtual {v0, v7, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 43
    .line 44
    .line 45
    move-result v13

    .line 46
    iput v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 47
    .line 48
    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v13

    .line 52
    iput-object v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8oOOo:Ljava/lang/String;

    .line 53
    .line 54
    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v13

    .line 58
    iput-object v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 59
    .line 60
    const-string v13, "extra_func_entrance"

    .line 61
    .line 62
    invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 63
    .line 64
    .line 65
    move-result v13

    .line 66
    iput v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇o0O:I

    .line 67
    .line 68
    invoke-virtual {v0, v4, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 69
    .line 70
    .line 71
    move-result v13

    .line 72
    iput-boolean v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O88O:Z

    .line 73
    .line 74
    invoke-virtual {v0, v3, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 75
    .line 76
    .line 77
    move-result v13

    .line 78
    iput-boolean v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO〇〇:Z

    .line 79
    .line 80
    invoke-virtual {v0, v2, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 81
    .line 82
    .line 83
    move-result v13

    .line 84
    iput-boolean v13, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8o:Z

    .line 85
    .line 86
    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 87
    .line 88
    .line 89
    move-result v13

    .line 90
    iget-object v14, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 91
    .line 92
    invoke-interface {v14, v13}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->Oo08(Z)V

    .line 93
    .line 94
    .line 95
    const-string v13, "is_from_page_list"

    .line 96
    .line 97
    invoke-virtual {v0, v13, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇oo〇O〇80:Z

    .line 102
    .line 103
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    invoke-virtual {v0, v11}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 108
    .line 109
    .line 110
    move-result-object v11

    .line 111
    check-cast v11, Ljava/util/List;

    .line 112
    .line 113
    iput-object v11, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 114
    .line 115
    invoke-virtual {v0, v10, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 116
    .line 117
    .line 118
    move-result-wide v8

    .line 119
    iput-wide v8, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇08O:J

    .line 120
    .line 121
    invoke-virtual {v0, v7, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 122
    .line 123
    .line 124
    move-result v7

    .line 125
    iput v7, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 126
    .line 127
    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v6

    .line 131
    iput-object v6, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8oOOo:Ljava/lang/String;

    .line 132
    .line 133
    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v5

    .line 137
    iput-object v5, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 138
    .line 139
    invoke-virtual {v0, v4, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 140
    .line 141
    .line 142
    move-result v4

    .line 143
    iput-boolean v4, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O88O:Z

    .line 144
    .line 145
    invoke-virtual {v0, v3, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 146
    .line 147
    .line 148
    move-result v3

    .line 149
    iput-boolean v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO〇〇:Z

    .line 150
    .line 151
    invoke-virtual {v0, v2, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 152
    .line 153
    .line 154
    move-result v2

    .line 155
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8o:Z

    .line 156
    .line 157
    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 158
    .line 159
    .line 160
    move-result v0

    .line 161
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 162
    .line 163
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->Oo08(Z)V

    .line 164
    .line 165
    .line 166
    invoke-static {p0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 167
    .line 168
    .line 169
    move-result v0

    .line 170
    shr-int/lit8 v0, v0, 0x1

    .line 171
    .line 172
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo8ooo8O:I

    .line 173
    .line 174
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇88()V

    .line 175
    .line 176
    .line 177
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    if-eqz v0, :cond_1

    .line 182
    .line 183
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 184
    .line 185
    if-eqz v0, :cond_1

    .line 186
    .line 187
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->setFloatActionViewListener(Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;)V

    .line 188
    .line 189
    .line 190
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    if-eqz v0, :cond_2

    .line 195
    .line 196
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 197
    .line 198
    if-eqz v0, :cond_2

    .line 199
    .line 200
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    .line 201
    .line 202
    .line 203
    move-result v1

    .line 204
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setSignatureColors(Z)V

    .line 205
    .line 206
    .line 207
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇O:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;

    .line 208
    .line 209
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setOnColorSelectedListener(Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;)V

    .line 210
    .line 211
    .line 212
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 213
    .line 214
    .line 215
    move-result-object v0

    .line 216
    if-eqz v0, :cond_3

    .line 217
    .line 218
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->oOo〇8o008:Landroid/widget/SeekBar;

    .line 219
    .line 220
    if-eqz v0, :cond_3

    .line 221
    .line 222
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8〇o88:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 223
    .line 224
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 225
    .line 226
    .line 227
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8O()V

    .line 228
    .line 229
    .line 230
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇o8〇8()V

    .line 231
    .line 232
    .line 233
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    if-eqz v0, :cond_4

    .line 238
    .line 239
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 240
    .line 241
    if-eqz v0, :cond_4

    .line 242
    .line 243
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->setOnClickSignatureListener(Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;)V

    .line 244
    .line 245
    .line 246
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 247
    .line 248
    .line 249
    move-result-object v0

    .line 250
    if-eqz v0, :cond_5

    .line 251
    .line 252
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o8〇OO0〇0o:Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

    .line 253
    .line 254
    goto :goto_0

    .line 255
    :cond_5
    const/4 v0, 0x0

    .line 256
    :goto_0
    if-nez v0, :cond_6

    .line 257
    .line 258
    goto :goto_1

    .line 259
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇8O0O80〇()Z

    .line 260
    .line 261
    .line 262
    move-result v1

    .line 263
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 264
    .line 265
    .line 266
    :goto_1
    return-void
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static final synthetic oOO8oo0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8〇OO:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final oOOO0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;[Ljava/lang/String;Z)V
    .locals 2

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "<anonymous parameter 0>"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇o88〇O()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇80O()Lorg/json/JSONObject;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    const/4 p1, 0x1

    .line 19
    new-array p1, p1, [Landroid/util/Pair;

    .line 20
    .line 21
    new-instance p2, Landroid/util/Pair;

    .line 22
    .line 23
    const-string v0, "type"

    .line 24
    .line 25
    const-string v1, "import_camera"

    .line 26
    .line 27
    invoke-direct {p2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    aput-object p2, p1, v0

    .line 32
    .line 33
    const-string p2, "CSAddSignature"

    .line 34
    .line 35
    const-string v0, "create_signature_mode"

    .line 36
    .line 37
    invoke-static {p2, v0, p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->o〇0(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;[Landroid/util/Pair;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic oO〇O0O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇00O0:Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oo0O()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_4

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇800OO〇0O:Ljava/util/concurrent/ConcurrentHashMap;

    .line 6
    .line 7
    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    const/4 v1, 0x0

    .line 19
    const/4 v2, 0x0

    .line 20
    :goto_0
    if-ge v2, v0, :cond_3

    .line 21
    .line 22
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇800OO〇0O:Ljava/util/concurrent/ConcurrentHashMap;

    .line 23
    .line 24
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    check-cast v3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 33
    .line 34
    if-eqz v3, :cond_2

    .line 35
    .line 36
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 37
    .line 38
    if-eqz v4, :cond_1

    .line 39
    .line 40
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    if-eqz v4, :cond_1

    .line 45
    .line 46
    const-string v5, "basePdfImageLists"

    .line 47
    .line 48
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-static {v4, v2}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v4

    .line 55
    check-cast v4, Ljava/util/List;

    .line 56
    .line 57
    if-eqz v4, :cond_1

    .line 58
    .line 59
    invoke-interface {v4, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 63
    .line 64
    if-eqz v3, :cond_2

    .line 65
    .line 66
    invoke-virtual {v3, v2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 67
    .line 68
    .line 69
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇800OO〇0O:Ljava/util/concurrent/ConcurrentHashMap;

    .line 73
    .line 74
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 75
    .line 76
    .line 77
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO8:Z

    .line 78
    .line 79
    :cond_4
    :goto_1
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final oo8(Ljava/util/HashMap;)Lorg/json/JSONObject;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo0oO0(Ljava/util/HashMap;)Lorg/json/JSONObject;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    move-object v1, v0

    .line 16
    check-cast v1, Ljava/util/Collection;

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    const/4 v3, 0x1

    .line 20
    if-eqz v1, :cond_2

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-eqz v1, :cond_1

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_1
    const/4 v1, 0x0

    .line 30
    goto :goto_2

    .line 31
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 32
    :goto_2
    if-nez v1, :cond_c

    .line 33
    .line 34
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    const/4 v4, 0x0

    .line 39
    :goto_3
    if-ge v4, v1, :cond_c

    .line 40
    .line 41
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v5

    .line 45
    check-cast v5, Ljava/util/List;

    .line 46
    .line 47
    if-eqz v5, :cond_b

    .line 48
    .line 49
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 50
    .line 51
    .line 52
    move-result v6

    .line 53
    if-le v6, v3, :cond_b

    .line 54
    .line 55
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 56
    .line 57
    if-eqz v6, :cond_3

    .line 58
    .line 59
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 60
    .line 61
    .line 62
    move-result v6

    .line 63
    goto :goto_4

    .line 64
    :cond_3
    const/4 v6, 0x0

    .line 65
    :goto_4
    if-le v6, v4, :cond_b

    .line 66
    .line 67
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 68
    .line 69
    .line 70
    move-result-object v5

    .line 71
    :cond_4
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 72
    .line 73
    .line 74
    move-result v6

    .line 75
    if-eqz v6, :cond_b

    .line 76
    .line 77
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v6

    .line 81
    check-cast v6, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;

    .line 82
    .line 83
    instance-of v6, v6, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 84
    .line 85
    if-eqz v6, :cond_4

    .line 86
    .line 87
    sget-object v6, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 88
    .line 89
    invoke-virtual {v6}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 90
    .line 91
    .line 92
    move-result-object v6

    .line 93
    iget-wide v7, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇08O:J

    .line 94
    .line 95
    invoke-static {v6, v7, v8}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O888o0o(Landroid/content/Context;J)Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v6

    .line 99
    iget-object v7, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 100
    .line 101
    if-eqz v7, :cond_5

    .line 102
    .line 103
    invoke-static {v7, v4}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 104
    .line 105
    .line 106
    move-result-object v7

    .line 107
    check-cast v7, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 108
    .line 109
    if-eqz v7, :cond_5

    .line 110
    .line 111
    invoke-virtual {v7}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageId()J

    .line 112
    .line 113
    .line 114
    move-result-wide v7

    .line 115
    goto :goto_6

    .line 116
    :cond_5
    const-wide/16 v7, 0x0

    .line 117
    .line 118
    :goto_6
    invoke-static {p0, v7, v8}, Lcom/intsig/camscanner/db/dao/ImageDao;->O0o〇〇Oo(Landroid/content/Context;J)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v7

    .line 122
    if-eqz v6, :cond_7

    .line 123
    .line 124
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    .line 125
    .line 126
    .line 127
    move-result v8

    .line 128
    if-nez v8, :cond_6

    .line 129
    .line 130
    goto :goto_7

    .line 131
    :cond_6
    const/4 v8, 0x0

    .line 132
    goto :goto_8

    .line 133
    :cond_7
    :goto_7
    const/4 v8, 0x1

    .line 134
    :goto_8
    if-nez v8, :cond_4

    .line 135
    .line 136
    if-eqz v7, :cond_9

    .line 137
    .line 138
    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    .line 139
    .line 140
    .line 141
    move-result v8

    .line 142
    if-nez v8, :cond_8

    .line 143
    .line 144
    goto :goto_9

    .line 145
    :cond_8
    const/4 v8, 0x0

    .line 146
    goto :goto_a

    .line 147
    :cond_9
    :goto_9
    const/4 v8, 0x1

    .line 148
    :goto_a
    if-eqz v8, :cond_a

    .line 149
    .line 150
    goto :goto_5

    .line 151
    :cond_a
    :try_start_0
    const-string v0, "doc_id"

    .line 152
    .line 153
    invoke-virtual {p1, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 154
    .line 155
    .line 156
    const-string v0, "page_id"

    .line 157
    .line 158
    invoke-virtual {p1, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    .line 160
    .line 161
    goto :goto_b

    .line 162
    :catch_0
    move-exception v0

    .line 163
    const-string v1, "PdfSignatureNewActivity"

    .line 164
    .line 165
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 166
    .line 167
    .line 168
    :goto_b
    return-object p1

    .line 169
    :cond_b
    add-int/lit8 v4, v4, 0x1

    .line 170
    .line 171
    goto/16 :goto_3

    .line 172
    .line 173
    :cond_c
    return-object p1
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final oo88()Z
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->getCurTabType()Ljava/lang/Integer;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/4 v2, 0x3

    .line 24
    if-ne v0, v2, :cond_1

    .line 25
    .line 26
    const/4 v1, 0x1

    .line 27
    :cond_1
    :goto_0
    return v1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oo8〇〇()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇08oOOO0:I

    .line 2
    .line 3
    const/high16 v1, -0x1000000

    .line 4
    .line 5
    if-eq v0, v1, :cond_3

    .line 6
    .line 7
    const v1, -0xffff01

    .line 8
    .line 9
    .line 10
    if-eq v0, v1, :cond_2

    .line 11
    .line 12
    const v1, -0xee8c41

    .line 13
    .line 14
    .line 15
    if-eq v0, v1, :cond_2

    .line 16
    .line 17
    const v1, -0xeaeaeb

    .line 18
    .line 19
    .line 20
    if-eq v0, v1, :cond_3

    .line 21
    .line 22
    const v1, -0x3e5f5

    .line 23
    .line 24
    .line 25
    if-eq v0, v1, :cond_1

    .line 26
    .line 27
    const/high16 v1, -0x10000

    .line 28
    .line 29
    if-eq v0, v1, :cond_1

    .line 30
    .line 31
    const/4 v1, -0x1

    .line 32
    if-eq v0, v1, :cond_0

    .line 33
    .line 34
    const-string v0, "others"

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const-string v0, "white"

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const-string v0, "red"

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    const-string v0, "blue"

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_3
    const-string v0, "black"

    .line 47
    .line 48
    :goto_0
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final ooo008(II)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0O0o8:I

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onTabChanged, last: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v0, ", position: "

    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string p1, ", type: "

    .line 25
    .line 26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const-string v0, "PdfSignatureNewActivity"

    .line 37
    .line 38
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    iget p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0O0o8:I

    .line 42
    .line 43
    const/4 v0, 0x0

    .line 44
    const/4 v1, 0x1

    .line 45
    const/4 v2, 0x3

    .line 46
    if-ne p1, v2, :cond_1

    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    if-eqz p1, :cond_0

    .line 53
    .line 54
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 55
    .line 56
    if-eqz p1, :cond_0

    .line 57
    .line 58
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->setIsPagingSeal(Z)V

    .line 59
    .line 60
    .line 61
    :cond_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇oO〇(Z)V

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_1
    if-ne p2, v2, :cond_3

    .line 66
    .line 67
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    if-eqz p1, :cond_2

    .line 72
    .line 73
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 74
    .line 75
    if-eqz p1, :cond_2

    .line 76
    .line 77
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->setIsPagingSeal(Z)V

    .line 78
    .line 79
    .line 80
    :cond_2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇oO〇(Z)V

    .line 81
    .line 82
    .line 83
    :cond_3
    :goto_0
    iput p2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0O0o8:I

    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇08〇o0O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOo〇08〇:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oooo800〇〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇o〇Oo88:Z

    .line 9
    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 15
    .line 16
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    const/4 v0, 0x0

    .line 24
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇o〇Oo88:Z

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    if-eqz v1, :cond_2

    .line 34
    .line 35
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 36
    .line 37
    if-eqz v1, :cond_2

    .line 38
    .line 39
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->〇0〇O0088o(Z)V

    .line 40
    .line 41
    .line 42
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0O〇O00O:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 43
    .line 44
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0OoOOo0:Ljava/util/concurrent/ConcurrentHashMap;

    .line 48
    .line 49
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 50
    .line 51
    .line 52
    const-string v0, ""

    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO0880O:Ljava/lang/String;

    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final ooooo0O()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇OO〇00〇0O:Z

    .line 3
    .line 4
    new-instance v1, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iget-wide v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇08O:J

    .line 10
    .line 11
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    new-instance v2, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    invoke-direct {v2, p0, v1, v3}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 22
    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/share/type/SharePdf;->〇o8oO(Z)V

    .line 26
    .line 27
    .line 28
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooO:Ljava/util/ArrayList;

    .line 29
    .line 30
    if-eqz v3, :cond_0

    .line 31
    .line 32
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-eqz v3, :cond_1

    .line 37
    .line 38
    :cond_0
    const/4 v0, 0x1

    .line 39
    :cond_1
    if-nez v0, :cond_2

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooO:Ljava/util/ArrayList;

    .line 42
    .line 43
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->o80ooO(Ljava/util/ArrayList;)V

    .line 44
    .line 45
    .line 46
    :cond_2
    invoke-static {p0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    new-instance v1, LO80OO/〇O888o0o;

    .line 51
    .line 52
    invoke-direct {v1, p0}, LO80OO/〇O888o0o;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->O8888(Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 56
    .line 57
    .line 58
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$go2Share$2;

    .line 59
    .line 60
    invoke-direct {v1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$go2Share$2;-><init>()V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->OOo88OOo(Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareHelper;->o88O8()V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final oo〇O0o〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO0880O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x1

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    if-eqz v0, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO0880O:Ljava/lang/String;

    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    sub-int/2addr v3, v2

    .line 24
    invoke-static {v0, v3}, Lkotlin/text/StringsKt;->o0(Ljava/lang/String;I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    new-instance v3, Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    invoke-direct {v3, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 31
    .line 32
    .line 33
    const v4, 0x7f1300a9

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-virtual {v3, v4}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    new-array v2, v2, [Ljava/lang/Object;

    .line 45
    .line 46
    aput-object v0, v2, v1

    .line 47
    .line 48
    const v0, 0x7f13121e

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v3, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    new-instance v1, LO80OO/o〇0;

    .line 60
    .line 61
    invoke-direct {v1}, LO80OO/o〇0;-><init>()V

    .line 62
    .line 63
    .line 64
    const v2, 0x7f130019

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 76
    .line 77
    .line 78
    const-string v0, "size_problem"

    .line 79
    .line 80
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇O800oo()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0〇OO008O(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic o〇08oO80o(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;II)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇8o0OOOo(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇O80o8OO()V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 2
    .line 3
    instance-of v1, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    return-void

    .line 14
    :cond_1
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    const/4 v3, 0x1

    .line 23
    if-gt v1, v2, :cond_3

    .line 24
    .line 25
    move v4, v2

    .line 26
    :goto_1
    const/4 v5, 0x2

    .line 27
    new-array v5, v5, [I

    .line 28
    .line 29
    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v6

    .line 33
    if-eqz v6, :cond_2

    .line 34
    .line 35
    invoke-virtual {v6, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 36
    .line 37
    .line 38
    aget v5, v5, v3

    .line 39
    .line 40
    iget v6, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo8ooo8O:I

    .line 41
    .line 42
    if-gt v5, v6, :cond_2

    .line 43
    .line 44
    move v2, v4

    .line 45
    goto :goto_2

    .line 46
    :cond_2
    if-eq v4, v1, :cond_3

    .line 47
    .line 48
    add-int/lit8 v4, v4, -0x1

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_3
    :goto_2
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇o88o08〇:I

    .line 52
    .line 53
    if-ltz v0, :cond_4

    .line 54
    .line 55
    if-eq v0, v2, :cond_8

    .line 56
    .line 57
    :cond_4
    iput v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇o88o08〇:I

    .line 58
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v1, "finalPosition ="

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    const-string v1, "PdfSignatureNewActivity"

    .line 77
    .line 78
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    if-eqz v0, :cond_8

    .line 86
    .line 87
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->oOo0:Landroid/widget/TextView;

    .line 88
    .line 89
    if-eqz v0, :cond_8

    .line 90
    .line 91
    add-int/2addr v2, v3

    .line 92
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 93
    .line 94
    const/4 v4, 0x0

    .line 95
    if-eqz v1, :cond_5

    .line 96
    .line 97
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->getItemCount()I

    .line 98
    .line 99
    .line 100
    move-result v1

    .line 101
    goto :goto_3

    .line 102
    :cond_5
    const/4 v1, 0x0

    .line 103
    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v2, "/"

    .line 112
    .line 113
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    .line 125
    .line 126
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 127
    .line 128
    .line 129
    move-result v1

    .line 130
    if-eqz v1, :cond_7

    .line 131
    .line 132
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    if-eqz v1, :cond_6

    .line 137
    .line 138
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 139
    .line 140
    if-eqz v1, :cond_6

    .line 141
    .line 142
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 143
    .line 144
    .line 145
    move-result v1

    .line 146
    if-ne v1, v3, :cond_6

    .line 147
    .line 148
    const/4 v4, 0x1

    .line 149
    :cond_6
    if-eqz v4, :cond_8

    .line 150
    .line 151
    :cond_7
    invoke-static {v0, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 152
    .line 153
    .line 154
    :cond_8
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic o〇OoO0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇o08〇(ILcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->OO〇80oO〇(ILcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final o〇o0oOO8(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇o8〇〇O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8〇o〇OoO8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇oO08〇o0(I)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_0

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    :cond_0
    if-eqz v1, :cond_1

    .line 26
    .line 27
    return-void

    .line 28
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    if-eqz v0, :cond_6

    .line 33
    .line 34
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 35
    .line 36
    if-nez v0, :cond_2

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-ne p1, v1, :cond_3

    .line 44
    .line 45
    return-void

    .line 46
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    if-nez v1, :cond_4

    .line 51
    .line 52
    const/16 v1, 0x3c

    .line 53
    .line 54
    invoke-static {p0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    :cond_4
    const/4 v2, 0x0

    .line 59
    if-nez p1, :cond_5

    .line 60
    .line 61
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    .line 62
    .line 63
    int-to-float v1, v1

    .line 64
    invoke-direct {v3, v2, v2, v1, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_5
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    .line 69
    .line 70
    int-to-float v1, v1

    .line 71
    invoke-direct {v3, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 72
    .line 73
    .line 74
    :goto_0
    const-wide/16 v1, 0x12c

    .line 75
    .line 76
    invoke-virtual {v3, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 80
    .line 81
    .line 82
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$setFunctionContainerVisibility$1;

    .line 83
    .line 84
    invoke-direct {v1, p1, v0, p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$setFunctionContainerVisibility$1;-><init>(ILandroid/widget/LinearLayout;Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v3, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 88
    .line 89
    .line 90
    :cond_6
    :goto_1
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final o〇〇8〇〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/util/Collection;

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 17
    :goto_1
    if-nez v0, :cond_3

    .line 18
    .line 19
    if-nez p1, :cond_2

    .line 20
    .line 21
    goto :goto_2

    .line 22
    :cond_2
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    .line 23
    .line 24
    invoke-static {v0}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const/16 v2, 0x78

    .line 35
    .line 36
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    int-to-float v2, v1

    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    int-to-float v3, v3

    .line 46
    mul-float v2, v2, v3

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    int-to-float v3, v3

    .line 53
    div-float/2addr v2, v3

    .line 54
    new-instance v3, Lcom/intsig/camscanner/util/ParcelSize;

    .line 55
    .line 56
    float-to-int v2, v2

    .line 57
    invoke-direct {v3, v1, v2}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 58
    .line 59
    .line 60
    iput-object v3, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 61
    .line 62
    iput-object v0, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->rawSize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 63
    .line 64
    invoke-virtual {p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    iput p2, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 69
    .line 70
    :cond_3
    :goto_2
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇00o〇O8()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O88O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO〇〇:Z

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string v0, "PdfSignatureNewActivity"

    .line 10
    .line 11
    const-string v1, "doDelete() delete multi documents"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .line 20
    .line 21
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇08O:J

    .line 22
    .line 23
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    const/4 v1, 0x4

    .line 31
    const/4 v2, 0x0

    .line 32
    const/4 v3, 0x0

    .line 33
    invoke-static {p0, v0, v3, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇O8〇〇o(Landroid/content/Context;Ljava/util/List;ZILjava/lang/Object;)Ljava/util/ArrayList;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇o(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    new-instance v3, Ljava/util/ArrayList;

    .line 42
    .line 43
    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 44
    .line 45
    .line 46
    check-cast v2, Ljava/util/Collection;

    .line 47
    .line 48
    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    const/4 v2, 0x1

    .line 56
    invoke-static {v1, v3, v2}, Lcom/intsig/camscanner/app/DBUtil;->o〇0o〇〇(Landroid/content/Context;Ljava/util/List;I)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    const/4 v2, 0x2

    .line 64
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo8(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-static {v1, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 72
    .line 73
    .line 74
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8o:Z

    .line 75
    .line 76
    if-nez v0, :cond_1

    .line 77
    .line 78
    new-instance v0, Landroid/content/Intent;

    .line 79
    .line 80
    const-class v1, Lcom/intsig/camscanner/pdf/kit/PdfKitMainActivity;

    .line 81
    .line 82
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 86
    .line 87
    .line 88
    :cond_1
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final 〇00〇〇〇o〇8(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇00o08()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇0888(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, LO80OO/〇8o8o〇;

    .line 7
    .line 8
    invoke-direct {v0, p0}, LO80OO/〇8o8o〇;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 9
    .line 10
    .line 11
    invoke-static {p0, v0}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->〇〇o0〇8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0O8Oo(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇〇8〇〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇o88o08〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇0o88O()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 10
    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    if-ne v0, v1, :cond_1

    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    const/4 v0, 0x0

    .line 27
    :goto_1
    if-eqz v0, :cond_4

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-eqz v0, :cond_2

    .line 34
    .line 35
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 36
    .line 37
    goto :goto_2

    .line 38
    :cond_2
    const/4 v0, 0x0

    .line 39
    :goto_2
    if-eqz v0, :cond_4

    .line 40
    .line 41
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo8888(Z)V

    .line 42
    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    if-eqz v0, :cond_3

    .line 49
    .line 50
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 51
    .line 52
    if-eqz v0, :cond_3

    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇oo〇()Z

    .line 55
    .line 56
    .line 57
    :cond_3
    return-void

    .line 58
    :cond_4
    const-string v0, "discard_signature"

    .line 59
    .line 60
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0oO(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 64
    .line 65
    if-eqz v0, :cond_8

    .line 66
    .line 67
    if-eqz v0, :cond_5

    .line 68
    .line 69
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->o〇O8〇〇o()Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-nez v0, :cond_5

    .line 74
    .line 75
    goto :goto_3

    .line 76
    :cond_5
    const/4 v1, 0x0

    .line 77
    :goto_3
    if-nez v1, :cond_6

    .line 78
    .line 79
    goto :goto_5

    .line 80
    :cond_6
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O88O:Z

    .line 81
    .line 82
    if-eqz v0, :cond_7

    .line 83
    .line 84
    const v0, 0x7f130618

    .line 85
    .line 86
    .line 87
    const v1, 0x7f130617

    .line 88
    .line 89
    .line 90
    goto :goto_4

    .line 91
    :cond_7
    const v0, 0x7f13063e

    .line 92
    .line 93
    .line 94
    const v1, 0x7f130614

    .line 95
    .line 96
    .line 97
    :goto_4
    new-instance v2, Lcom/intsig/app/AlertDialog$Builder;

    .line 98
    .line 99
    invoke-direct {v2, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 100
    .line 101
    .line 102
    const v3, 0x7f131e57

    .line 103
    .line 104
    .line 105
    invoke-virtual {v2, v3}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 106
    .line 107
    .line 108
    move-result-object v2

    .line 109
    invoke-virtual {v2, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    new-instance v2, LO80OO/Oo08;

    .line 114
    .line 115
    invoke-direct {v2, p0}, LO80OO/Oo08;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 116
    .line 117
    .line 118
    const v3, 0x7f13057e

    .line 119
    .line 120
    .line 121
    invoke-virtual {v0, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    new-instance v2, LO80OO/OO0o〇〇;

    .line 126
    .line 127
    invoke-direct {v2, p0}, LO80OO/OO0o〇〇;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 139
    .line 140
    .line 141
    return-void

    .line 142
    :cond_8
    :goto_5
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇00o〇O8()V

    .line 143
    .line 144
    .line 145
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 146
    .line 147
    .line 148
    return-void
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇O800oo()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇0o〇o(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FII)Z
    .locals 22

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move/from16 v2, p5

    .line 4
    .line 5
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v3, 0x0

    .line 10
    if-eqz v0, :cond_11

    .line 11
    .line 12
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 13
    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    goto/16 :goto_c

    .line 17
    .line 18
    :cond_0
    move-object/from16 v4, p2

    .line 19
    .line 20
    iget v4, v4, Landroid/graphics/Point;->y:I

    .line 21
    .line 22
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->〇8o8o〇(I)I

    .line 23
    .line 24
    .line 25
    move-result v5

    .line 26
    iget-object v6, v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 27
    .line 28
    if-nez v6, :cond_1

    .line 29
    .line 30
    return v3

    .line 31
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    iget-object v7, v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOoo80oO:Ljava/util/List;

    .line 36
    .line 37
    invoke-interface {v7}, Ljava/util/List;->size()I

    .line 38
    .line 39
    .line 40
    move-result v7

    .line 41
    div-int v7, v0, v7

    .line 42
    .line 43
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 44
    .line 45
    .line 46
    move-result v8

    .line 47
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 48
    .line 49
    .line 50
    move-result v9

    .line 51
    const/4 v10, 0x0

    .line 52
    :goto_0
    if-ge v10, v9, :cond_d

    .line 53
    .line 54
    iget-object v0, v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 55
    .line 56
    if-eqz v0, :cond_2

    .line 57
    .line 58
    invoke-static {v0, v10}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    check-cast v0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 63
    .line 64
    move-object v13, v0

    .line 65
    goto :goto_1

    .line 66
    :cond_2
    const/4 v13, 0x0

    .line 67
    :goto_1
    iget-object v0, v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 68
    .line 69
    if-eqz v0, :cond_3

    .line 70
    .line 71
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    if-eqz v0, :cond_3

    .line 76
    .line 77
    invoke-static {v0, v10}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    check-cast v0, Ljava/util/List;

    .line 82
    .line 83
    if-eqz v0, :cond_3

    .line 84
    .line 85
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇8〇0〇o〇O(Ljava/util/List;)Ljava/lang/Object;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;

    .line 90
    .line 91
    goto :goto_2

    .line 92
    :cond_3
    const/4 v0, 0x0

    .line 93
    :goto_2
    instance-of v14, v0, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 94
    .line 95
    if-eqz v14, :cond_4

    .line 96
    .line 97
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 98
    .line 99
    move-object v14, v0

    .line 100
    goto :goto_3

    .line 101
    :cond_4
    const/4 v14, 0x0

    .line 102
    :goto_3
    if-eqz v13, :cond_c

    .line 103
    .line 104
    if-nez v14, :cond_5

    .line 105
    .line 106
    goto/16 :goto_9

    .line 107
    .line 108
    :cond_5
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 113
    .line 114
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;->〇o〇()Lcom/intsig/camscanner/util/ParcelSize;

    .line 115
    .line 116
    .line 117
    move-result-object v15

    .line 118
    invoke-virtual {v15}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 119
    .line 120
    .line 121
    move-result v15

    .line 122
    div-int/lit8 v15, v15, 0x2

    .line 123
    .line 124
    add-int/2addr v0, v15

    .line 125
    div-int/lit8 v15, v8, 0x2

    .line 126
    .line 127
    sub-int/2addr v0, v15

    .line 128
    add-int v15, v0, v5

    .line 129
    .line 130
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 131
    .line 132
    .line 133
    move-result v0

    .line 134
    add-int v12, v15, v0

    .line 135
    .line 136
    iget-object v0, v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 137
    .line 138
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 139
    .line 140
    .line 141
    move-result v3

    .line 142
    sub-int v11, v3, v7

    .line 143
    .line 144
    iget-object v0, v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 145
    .line 146
    if-eqz v0, :cond_b

    .line 147
    .line 148
    :try_start_0
    invoke-static {v0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 152
    move/from16 v16, v5

    .line 153
    .line 154
    :try_start_1
    const-class v5, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 155
    .line 156
    invoke-static {v0, v5}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 157
    .line 158
    .line 159
    move-result-object v0

    .line 160
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 161
    .line 162
    move/from16 v17, v9

    .line 163
    .line 164
    goto :goto_5

    .line 165
    :catch_0
    move-exception v0

    .line 166
    goto :goto_4

    .line 167
    :catch_1
    move-exception v0

    .line 168
    move/from16 v16, v5

    .line 169
    .line 170
    :goto_4
    const-string v5, "PdfSignatureNewActivity"

    .line 171
    .line 172
    move/from16 v17, v9

    .line 173
    .line 174
    const-string v9, "copy"

    .line 175
    .line 176
    invoke-static {v5, v9, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    .line 178
    .line 179
    const/4 v0, 0x0

    .line 180
    :goto_5
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 181
    .line 182
    if-eqz v0, :cond_b

    .line 183
    .line 184
    iget v5, v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 185
    .line 186
    if-lez v5, :cond_6

    .line 187
    .line 188
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 189
    .line 190
    .line 191
    move-result-object v5

    .line 192
    invoke-virtual {v13}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 193
    .line 194
    .line 195
    move-result v9

    .line 196
    move/from16 p2, v10

    .line 197
    .line 198
    int-to-double v9, v9

    .line 199
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;->〇o〇()Lcom/intsig/camscanner/util/ParcelSize;

    .line 200
    .line 201
    .line 202
    move-result-object v18

    .line 203
    move/from16 v19, v4

    .line 204
    .line 205
    invoke-virtual/range {v18 .. v18}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 206
    .line 207
    .line 208
    move-result v4

    .line 209
    move-object/from16 v18, v6

    .line 210
    .line 211
    move/from16 v20, v7

    .line 212
    .line 213
    int-to-double v6, v4

    .line 214
    div-double/2addr v9, v6

    .line 215
    invoke-virtual {v13}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 216
    .line 217
    .line 218
    move-result v4

    .line 219
    int-to-double v6, v4

    .line 220
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;->〇o〇()Lcom/intsig/camscanner/util/ParcelSize;

    .line 221
    .line 222
    .line 223
    move-result-object v4

    .line 224
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 225
    .line 226
    .line 227
    move-result v4

    .line 228
    int-to-double v13, v4

    .line 229
    div-double/2addr v6, v13

    .line 230
    new-instance v4, Landroid/graphics/Rect;

    .line 231
    .line 232
    iget v13, v5, Landroid/graphics/Rect;->left:I

    .line 233
    .line 234
    sub-int v14, v11, v13

    .line 235
    .line 236
    int-to-double v1, v14

    .line 237
    mul-double v1, v1, v9

    .line 238
    .line 239
    double-to-int v1, v1

    .line 240
    iget v2, v5, Landroid/graphics/Rect;->top:I

    .line 241
    .line 242
    sub-int v5, v15, v2

    .line 243
    .line 244
    move/from16 v21, v15

    .line 245
    .line 246
    int-to-double v14, v5

    .line 247
    mul-double v14, v14, v6

    .line 248
    .line 249
    double-to-int v5, v14

    .line 250
    sub-int v13, v3, v13

    .line 251
    .line 252
    int-to-double v13, v13

    .line 253
    mul-double v9, v9, v13

    .line 254
    .line 255
    double-to-int v9, v9

    .line 256
    sub-int v2, v12, v2

    .line 257
    .line 258
    int-to-double v13, v2

    .line 259
    mul-double v6, v6, v13

    .line 260
    .line 261
    double-to-int v2, v6

    .line 262
    invoke-direct {v4, v1, v5, v9, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 263
    .line 264
    .line 265
    iput-object v4, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->canvasRect:Landroid/graphics/Rect;

    .line 266
    .line 267
    goto :goto_6

    .line 268
    :cond_6
    move/from16 v19, v4

    .line 269
    .line 270
    move-object/from16 v18, v6

    .line 271
    .line 272
    move/from16 v20, v7

    .line 273
    .line 274
    move/from16 p2, v10

    .line 275
    .line 276
    move/from16 v21, v15

    .line 277
    .line 278
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 279
    .line 280
    .line 281
    move-result-object v1

    .line 282
    if-eqz v1, :cond_7

    .line 283
    .line 284
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 285
    .line 286
    if-eqz v1, :cond_7

    .line 287
    .line 288
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getPath()Ljava/lang/String;

    .line 289
    .line 290
    .line 291
    move-result-object v2

    .line 292
    move/from16 v4, p5

    .line 293
    .line 294
    invoke-virtual {v1, v2, v4}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->o〇8(Ljava/lang/String;I)V

    .line 295
    .line 296
    .line 297
    goto :goto_7

    .line 298
    :cond_7
    move/from16 v4, p5

    .line 299
    .line 300
    :goto_7
    move-object/from16 v1, p1

    .line 301
    .line 302
    iput-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    .line 303
    .line 304
    iput v4, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 305
    .line 306
    move/from16 v2, p6

    .line 307
    .line 308
    iput v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 309
    .line 310
    new-instance v5, Landroid/graphics/Rect;

    .line 311
    .line 312
    move/from16 v6, v21

    .line 313
    .line 314
    invoke-direct {v5, v11, v6, v3, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 315
    .line 316
    .line 317
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setDisplayRect(Landroid/graphics/Rect;)V

    .line 318
    .line 319
    .line 320
    new-instance v5, Landroid/graphics/Point;

    .line 321
    .line 322
    add-int/2addr v11, v3

    .line 323
    div-int/lit8 v11, v11, 0x2

    .line 324
    .line 325
    add-int v15, v6, v12

    .line 326
    .line 327
    div-int/lit8 v15, v15, 0x2

    .line 328
    .line 329
    invoke-direct {v5, v11, v15}, Landroid/graphics/Point;-><init>(II)V

    .line 330
    .line 331
    .line 332
    iput-object v5, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->centerPoint:Landroid/graphics/Point;

    .line 333
    .line 334
    new-instance v3, Lcom/intsig/camscanner/util/ParcelSize;

    .line 335
    .line 336
    move/from16 v5, v20

    .line 337
    .line 338
    invoke-direct {v3, v5, v8}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 339
    .line 340
    .line 341
    iput-object v3, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 342
    .line 343
    move/from16 v3, p4

    .line 344
    .line 345
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setRotation(F)V

    .line 346
    .line 347
    .line 348
    const/4 v6, 0x1

    .line 349
    iput-boolean v6, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->isPagingSeal:Z

    .line 350
    .line 351
    iget-object v7, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->clipIndex:[I

    .line 352
    .line 353
    const/4 v9, 0x0

    .line 354
    aput p2, v7, v9

    .line 355
    .line 356
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    .line 357
    .line 358
    .line 359
    move-result v9

    .line 360
    aput v9, v7, v6

    .line 361
    .line 362
    move-object/from16 v6, p0

    .line 363
    .line 364
    iget-object v7, v6, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 365
    .line 366
    if-eqz v7, :cond_8

    .line 367
    .line 368
    invoke-virtual {v7}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 369
    .line 370
    .line 371
    move-result-object v7

    .line 372
    if-eqz v7, :cond_8

    .line 373
    .line 374
    const-string v9, "basePdfImageLists"

    .line 375
    .line 376
    invoke-static {v7, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    .line 378
    .line 379
    move/from16 v9, p2

    .line 380
    .line 381
    invoke-static {v7, v9}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 382
    .line 383
    .line 384
    move-result-object v7

    .line 385
    check-cast v7, Ljava/util/List;

    .line 386
    .line 387
    if-eqz v7, :cond_9

    .line 388
    .line 389
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    .line 391
    .line 392
    goto :goto_8

    .line 393
    :cond_8
    move/from16 v9, p2

    .line 394
    .line 395
    :cond_9
    :goto_8
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 396
    .line 397
    .line 398
    move-result-object v7

    .line 399
    iget-object v10, v6, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇800OO〇0O:Ljava/util/concurrent/ConcurrentHashMap;

    .line 400
    .line 401
    invoke-interface {v10, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    .line 403
    .line 404
    iget-object v0, v6, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 405
    .line 406
    if-eqz v0, :cond_a

    .line 407
    .line 408
    invoke-virtual {v0, v9}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 409
    .line 410
    .line 411
    :cond_a
    const/4 v7, 0x0

    .line 412
    goto :goto_a

    .line 413
    :cond_b
    move-object v6, v1

    .line 414
    const/4 v7, 0x0

    .line 415
    return v7

    .line 416
    :cond_c
    :goto_9
    move/from16 v3, p4

    .line 417
    .line 418
    move/from16 v19, v4

    .line 419
    .line 420
    move/from16 v16, v5

    .line 421
    .line 422
    move-object/from16 v18, v6

    .line 423
    .line 424
    move v5, v7

    .line 425
    move/from16 v17, v9

    .line 426
    .line 427
    move v9, v10

    .line 428
    const/4 v7, 0x0

    .line 429
    move-object v6, v1

    .line 430
    move v4, v2

    .line 431
    move-object/from16 v1, p1

    .line 432
    .line 433
    move/from16 v2, p6

    .line 434
    .line 435
    :goto_a
    add-int/lit8 v10, v9, 0x1

    .line 436
    .line 437
    move v2, v4

    .line 438
    move v7, v5

    .line 439
    move-object v1, v6

    .line 440
    move/from16 v5, v16

    .line 441
    .line 442
    move/from16 v9, v17

    .line 443
    .line 444
    move-object/from16 v6, v18

    .line 445
    .line 446
    move/from16 v4, v19

    .line 447
    .line 448
    const/4 v3, 0x0

    .line 449
    goto/16 :goto_0

    .line 450
    .line 451
    :cond_d
    move-object v6, v1

    .line 452
    move/from16 v19, v4

    .line 453
    .line 454
    const/4 v7, 0x0

    .line 455
    move v4, v2

    .line 456
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 457
    .line 458
    .line 459
    move-result-object v0

    .line 460
    if-eqz v0, :cond_e

    .line 461
    .line 462
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 463
    .line 464
    if-eqz v0, :cond_e

    .line 465
    .line 466
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->o〇0OOo〇0(Z)V

    .line 467
    .line 468
    .line 469
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 470
    .line 471
    .line 472
    move-result-object v0

    .line 473
    if-eqz v0, :cond_f

    .line 474
    .line 475
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 476
    .line 477
    if-eqz v0, :cond_f

    .line 478
    .line 479
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->OO0o〇〇〇〇0(I)V

    .line 480
    .line 481
    .line 482
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 483
    .line 484
    .line 485
    move-result-object v0

    .line 486
    if-eqz v0, :cond_10

    .line 487
    .line 488
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 489
    .line 490
    if-eqz v0, :cond_10

    .line 491
    .line 492
    move/from16 v1, v19

    .line 493
    .line 494
    const/4 v2, 0x1

    .line 495
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->〇0〇O0088o(ZI)V

    .line 496
    .line 497
    .line 498
    goto :goto_b

    .line 499
    :cond_10
    const/4 v2, 0x1

    .line 500
    :goto_b
    iput-boolean v2, v6, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO8:Z

    .line 501
    .line 502
    return v2

    .line 503
    :cond_11
    :goto_c
    move-object v6, v1

    .line 504
    const/4 v1, 0x0

    .line 505
    return v1
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private static final 〇0〇o8〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    move-object v1, v0

    .line 15
    :goto_0
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_2

    .line 20
    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇0o8〇(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 22
    .line 23
    .line 24
    new-instance p0, Ljava/io/File;

    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->o〇〇0〇()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    const-string v2, "5_22_3_append_local_signature"

    .line 31
    .line 32
    invoke-direct {p0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    if-eqz p1, :cond_1

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    :cond_1
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result p0

    .line 49
    if-eqz p0, :cond_3

    .line 50
    .line 51
    const-string p0, "CSAddSignature"

    .line 52
    .line 53
    const-string p1, "click_default_signature"

    .line 54
    .line 55
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_2
    sget-object p0, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 60
    .line 61
    const/4 p0, 0x1

    .line 62
    new-array v0, p0, [Ljava/lang/Object;

    .line 63
    .line 64
    const/4 v1, 0x0

    .line 65
    aput-object p1, v0, v1

    .line 66
    .line 67
    invoke-static {v0, p0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    move-result-object p0

    .line 71
    const-string p1, "%s : file is deleted"

    .line 72
    .line 73
    invoke-static {p1, p0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    const-string p1, "format(format, *args)"

    .line 78
    .line 79
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    const-string p1, "PdfSignatureNewActivity"

    .line 83
    .line 84
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    :cond_3
    :goto_1
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇80O()Lorg/json/JSONObject;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo0oO0(Ljava/util/HashMap;)Lorg/json/JSONObject;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇80O80O〇0()V
    .locals 8

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇8()Lcom/intsig/camscanner/pdf/signature/tab/ISignatureStrategy;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const/4 v2, 0x2

    .line 11
    const/4 v3, 0x1

    .line 12
    const/4 v4, 0x0

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {v1}, Lcom/intsig/camscanner/pdf/signature/tab/ISignatureStrategy;->〇o00〇〇Oo()Ljava/util/List;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    if-nez v1, :cond_1

    .line 20
    .line 21
    :cond_0
    const/4 v1, 0x3

    .line 22
    new-array v1, v1, [Ljava/lang/Integer;

    .line 23
    .line 24
    const v5, 0x7f13021d

    .line 25
    .line 26
    .line 27
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 28
    .line 29
    .line 30
    move-result-object v5

    .line 31
    aput-object v5, v1, v4

    .line 32
    .line 33
    const v5, 0x7f131e8c

    .line 34
    .line 35
    .line 36
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 37
    .line 38
    .line 39
    move-result-object v5

    .line 40
    aput-object v5, v1, v3

    .line 41
    .line 42
    const v5, 0x7f131e8b

    .line 43
    .line 44
    .line 45
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 46
    .line 47
    .line 48
    move-result-object v5

    .line 49
    aput-object v5, v1, v2

    .line 50
    .line 51
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    :cond_1
    new-instance v5, Lcom/intsig/menu/MenuItem;

    .line 56
    .line 57
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v6

    .line 61
    check-cast v6, Ljava/lang/Number;

    .line 62
    .line 63
    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    .line 64
    .line 65
    .line 66
    move-result v6

    .line 67
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v6

    .line 71
    const v7, 0x7f08052e

    .line 72
    .line 73
    .line 74
    invoke-direct {v5, v4, v6, v7}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    new-instance v5, Lcom/intsig/menu/MenuItem;

    .line 81
    .line 82
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    check-cast v2, Ljava/lang/Number;

    .line 87
    .line 88
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    const v6, 0x7f08052c

    .line 97
    .line 98
    .line 99
    invoke-direct {v5, v3, v2, v6}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    new-instance v2, Lcom/intsig/app/AlertBottomDialog;

    .line 106
    .line 107
    const v3, 0x7f140004

    .line 108
    .line 109
    .line 110
    invoke-direct {v2, p0, v3}, Lcom/intsig/app/AlertBottomDialog;-><init>(Landroid/content/Context;I)V

    .line 111
    .line 112
    .line 113
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    check-cast v1, Ljava/lang/Number;

    .line 118
    .line 119
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    invoke-virtual {v2, v1, v0}, Lcom/intsig/app/AlertBottomDialog;->〇o00〇〇Oo(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 128
    .line 129
    .line 130
    new-instance v0, LO80OO/oO80;

    .line 131
    .line 132
    invoke-direct {v0, p0}, LO80OO/oO80;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {v2, v0}, Lcom/intsig/app/AlertBottomDialog;->O8(Landroid/content/DialogInterface$OnClickListener;)V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v2}, Lcom/intsig/app/AlertBottomDialog;->show()V

    .line 139
    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇80〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo〇O0o〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8O()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->〇〇8O0〇8(Lcom/intsig/camscanner/pdf/signature/tab/ISignatureEditView;)V

    .line 12
    .line 13
    .line 14
    new-instance v1, LO80OO/〇〇808〇;

    .line 15
    .line 16
    invoke-direct {v1, p0}, LO80OO/〇〇808〇;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->setAddClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    .line 21
    .line 22
    new-instance v1, LO80OO/〇O00;

    .line 23
    .line 24
    invoke-direct {v1, p0}, LO80OO/〇O00;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->setSaveClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    .line 29
    .line 30
    new-instance v1, LO80OO/〇〇8O0〇8;

    .line 31
    .line 32
    invoke-direct {v1, p0}, LO80OO/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->setTopMaskClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    .line 37
    .line 38
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initSignatureEditView$1$4;

    .line 39
    .line 40
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$initSignatureEditView$1$4;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->〇O〇(Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView$IOnTabChangeListener;)V

    .line 44
    .line 45
    .line 46
    new-instance v1, LO80OO/〇0〇O0088o;

    .line 47
    .line 48
    invoke-direct {v1, p0}, LO80OO/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->setOnSignatureItemClickListener(Lcom/intsig/camscanner/signature/SignatureAdapter$OnSignatureItemClickListener;)V

    .line 52
    .line 53
    .line 54
    new-instance v1, LO80OO/OoO8;

    .line 55
    .line 56
    invoke-direct {v1, p0}, LO80OO/OoO8;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 60
    .line 61
    .line 62
    :cond_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇8o0o0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8oo0OOO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8oo0oO0(Ljava/util/HashMap;)Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    :try_start_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Ljava/util/Map$Entry;

    .line 27
    .line 28
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    check-cast v2, Ljava/lang/String;

    .line 33
    .line 34
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    check-cast v1, Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    const-string p1, "type"

    .line 45
    .line 46
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    if-eqz v1, :cond_1

    .line 51
    .line 52
    const-string v1, "premium"

    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_1
    const-string v1, "basic"

    .line 56
    .line 57
    :goto_1
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 58
    .line 59
    .line 60
    const-string p1, "login_status"

    .line 61
    .line 62
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-eqz v1, :cond_2

    .line 67
    .line 68
    const-string v1, "logged_in"

    .line 69
    .line 70
    goto :goto_2

    .line 71
    :cond_2
    const-string v1, "no_logged_in"

    .line 72
    .line 73
    :goto_2
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 74
    .line 75
    .line 76
    const-string p1, "from_part"

    .line 77
    .line 78
    const-string v1, "cs_pdf_preview"

    .line 79
    .line 80
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    .line 82
    .line 83
    goto :goto_3

    .line 84
    :catch_0
    move-exception p1

    .line 85
    const-string v1, "PdfSignatureNewActivity"

    .line 86
    .line 87
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 88
    .line 89
    .line 90
    :goto_3
    return-object v0
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇8oo8888(Z)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_6

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 11
    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO08〇o0(I)V

    .line 19
    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 22
    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    iget v0, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 26
    .line 27
    :cond_1
    mul-int/lit8 v0, v0, 0x2

    .line 28
    .line 29
    add-int/lit8 v0, v0, 0x4

    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const/4 v1, 0x0

    .line 36
    if-eqz p1, :cond_3

    .line 37
    .line 38
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 39
    .line 40
    if-eqz p1, :cond_3

    .line 41
    .line 42
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 43
    .line 44
    if-eqz v2, :cond_2

    .line 45
    .line 46
    iget v2, v2, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 47
    .line 48
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    goto :goto_0

    .line 53
    :cond_2
    move-object v2, v1

    .line 54
    :goto_0
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setCurrentSelect(Ljava/lang/Integer;)V

    .line 55
    .line 56
    .line 57
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    if-eqz p1, :cond_4

    .line 62
    .line 63
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->oOo〇8o008:Landroid/widget/SeekBar;

    .line 64
    .line 65
    :cond_4
    if-nez v1, :cond_5

    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_5
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 69
    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    if-eqz p1, :cond_7

    .line 77
    .line 78
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 79
    .line 80
    if-eqz p1, :cond_7

    .line 81
    .line 82
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 83
    .line 84
    .line 85
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    if-eqz p1, :cond_8

    .line 90
    .line 91
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 92
    .line 93
    if-eqz p1, :cond_8

    .line 94
    .line 95
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->o〇0OOo〇0(Z)V

    .line 96
    .line 97
    .line 98
    :cond_8
    const/16 p1, 0x8

    .line 99
    .line 100
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO08〇o0(I)V

    .line 101
    .line 102
    .line 103
    :goto_1
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇8ooOO(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const-string v2, "PdfSignatureNewActivity"

    .line 14
    .line 15
    if-lez v1, :cond_6

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-gtz v1, :cond_0

    .line 22
    .line 23
    goto/16 :goto_1

    .line 24
    .line 25
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 34
    .line 35
    invoke-static {v1, v0, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    const/4 v3, 0x0

    .line 44
    if-eqz v1, :cond_1

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-static {v1, v0, v3, v3}, Lcom/intsig/nativelib/DraftEngine;->CleanImageKeepColor(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    goto :goto_0

    .line 55
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-static {v1, v0, v3, v3}, Lcom/intsig/nativelib/DraftEngine;->CleanImage(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    :goto_0
    const/4 v3, -0x1

    .line 64
    if-le v1, v3, :cond_2

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 67
    .line 68
    .line 69
    move-result v4

    .line 70
    if-eqz v4, :cond_2

    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 73
    .line 74
    .line 75
    move-result v4

    .line 76
    invoke-static {v1, v0, v4}, Lcom/intsig/nativelib/DraftEngine;->StrokeColor(ILandroid/graphics/Bitmap;I)V

    .line 77
    .line 78
    .line 79
    :cond_2
    if-le v1, v3, :cond_3

    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    if-eqz v4, :cond_3

    .line 86
    .line 87
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    int-to-float v4, v4

    .line 92
    invoke-static {v1, v0, v4}, Lcom/intsig/nativelib/DraftEngine;->StrokeSize(ILandroid/graphics/Bitmap;F)V

    .line 93
    .line 94
    .line 95
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v4

    .line 99
    new-instance v5, Ljava/lang/StringBuilder;

    .line 100
    .line 101
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .line 103
    .line 104
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    const-string v4, "AddSignature/"

    .line 108
    .line 109
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v4

    .line 116
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v5

    .line 120
    new-instance v6, Ljava/lang/StringBuilder;

    .line 121
    .line 122
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .line 124
    .line 125
    const-string v7, "PdfSignature_"

    .line 126
    .line 127
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    const-string v5, ".png"

    .line 134
    .line 135
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v5

    .line 142
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 143
    .line 144
    const/16 v7, 0x5a

    .line 145
    .line 146
    invoke-static {v0, v7, v4, v5, v6}, Lcom/intsig/utils/ImageUtil;->〇0000OOO(Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;)Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v4

    .line 150
    invoke-virtual {p1, v4}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setTempSignaturePath(Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    if-le v1, v3, :cond_4

    .line 154
    .line 155
    new-instance v3, Ljava/lang/StringBuilder;

    .line 156
    .line 157
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .line 159
    .line 160
    const-string v4, "free = "

    .line 161
    .line 162
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v3

    .line 172
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .line 174
    .line 175
    invoke-static {v1}, Lcom/intsig/nativelib/DraftEngine;->FreeContext(I)V

    .line 176
    .line 177
    .line 178
    :cond_4
    if-eqz v0, :cond_5

    .line 179
    .line 180
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 181
    .line 182
    .line 183
    :cond_5
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 184
    .line 185
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v1

    .line 189
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getTempSignaturePath()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v2

    .line 193
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 197
    .line 198
    .line 199
    move-result p1

    .line 200
    iput p1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 201
    .line 202
    return-object v0

    .line 203
    :cond_6
    :goto_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 204
    .line 205
    .line 206
    move-result p1

    .line 207
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 208
    .line 209
    .line 210
    move-result v0

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    .line 212
    .line 213
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 214
    .line 215
    .line 216
    const-string v3, "addSignature bitmapSize.getWidth()="

    .line 217
    .line 218
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 222
    .line 223
    .line 224
    const-string p1, " bitmapSize.getHeight()="

    .line 225
    .line 226
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 230
    .line 231
    .line 232
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object p1

    .line 236
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    const/4 p1, 0x0

    .line 240
    return-object p1
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇8ooo()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O088O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 8
    .line 9
    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 11
    .line 12
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    check-cast v0, Ljava/lang/Iterable;

    .line 16
    .line 17
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇〇0o(Ljava/lang/Iterable;)Ljava/util/List;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O888Oo(Ljava/util/List;)V

    .line 22
    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8o088〇0()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8〇〇8〇8()V

    .line 31
    .line 32
    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇8oo〇〇oO()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v2, 0x1

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    if-ne v0, v2, :cond_1

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    const/4 v2, 0x0

    .line 26
    :goto_1
    if-eqz v2, :cond_3

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 35
    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇oo〇()Z

    .line 39
    .line 40
    .line 41
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    if-eqz v0, :cond_3

    .line 46
    .line 47
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 48
    .line 49
    if-eqz v0, :cond_3

    .line 50
    .line 51
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->o〇0OOo〇0(Z)V

    .line 52
    .line 53
    .line 54
    :cond_3
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇8〇0O〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooooo0O()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇8〇8o00(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "PdfSignatureNewActivity"

    .line 7
    .line 8
    const-string p2, "User Operation:  onclick cancel"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "cancel_leave_signature"

    .line 14
    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0oO(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8〇o〇OoO8()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 10
    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    if-nez v0, :cond_1

    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    const/4 v0, 0x0

    .line 27
    :goto_1
    if-nez v0, :cond_3

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-eqz v0, :cond_2

    .line 34
    .line 35
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 36
    .line 37
    if-eqz v0, :cond_2

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇oo〇()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    goto :goto_2

    .line 48
    :cond_2
    const/4 v0, 0x0

    .line 49
    :goto_2
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 50
    .line 51
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_3

    .line 56
    .line 57
    return-void

    .line 58
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 59
    .line 60
    if-nez v0, :cond_4

    .line 61
    .line 62
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 63
    .line 64
    .line 65
    return-void

    .line 66
    :cond_4
    const-string v0, "confirm_signature"

    .line 67
    .line 68
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0oO(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O00()Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    const-string v3, "PdfSignatureNewActivity"

    .line 76
    .line 77
    if-nez v0, :cond_8

    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    if-eqz v0, :cond_5

    .line 84
    .line 85
    goto :goto_3

    .line 86
    :cond_5
    sget-object v0, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇080:Lcom/intsig/camscanner/ads/reward/AdRewardedManager;

    .line 87
    .line 88
    sget-object v4, Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;->SIGNATURE:Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;

    .line 89
    .line 90
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇oo〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;)Z

    .line 91
    .line 92
    .line 93
    move-result v5

    .line 94
    if-eqz v5, :cond_6

    .line 95
    .line 96
    const-string v2, "has free point to save "

    .line 97
    .line 98
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0, v4, v1}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->Oooo8o0〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;I)V

    .line 102
    .line 103
    .line 104
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO8()V

    .line 105
    .line 106
    .line 107
    return-void

    .line 108
    :cond_6
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->Oooo8o0〇()I

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    if-lez v0, :cond_7

    .line 113
    .line 114
    const-string v3, "CSFreeSignature"

    .line 115
    .line 116
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇80O()Lorg/json/JSONObject;

    .line 117
    .line 118
    .line 119
    move-result-object v4

    .line 120
    invoke-static {v3, v4}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 121
    .line 122
    .line 123
    new-instance v3, Lcom/intsig/app/AlertDialog$Builder;

    .line 124
    .line 125
    invoke-direct {v3, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 126
    .line 127
    .line 128
    const v4, 0x7f131e57

    .line 129
    .line 130
    .line 131
    invoke-virtual {v3, v4}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 132
    .line 133
    .line 134
    move-result-object v3

    .line 135
    new-array v1, v1, [Ljava/lang/Object;

    .line 136
    .line 137
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 138
    .line 139
    .line 140
    move-result-object v4

    .line 141
    aput-object v4, v1, v2

    .line 142
    .line 143
    const v2, 0x7f13060c

    .line 144
    .line 145
    .line 146
    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v1

    .line 150
    invoke-virtual {v3, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    new-instance v2, LO80OO/〇80〇808〇O;

    .line 155
    .line 156
    invoke-direct {v2, v0, p0}, LO80OO/〇80〇808〇O;-><init>(ILcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 157
    .line 158
    .line 159
    const v0, 0x7f130116

    .line 160
    .line 161
    .line 162
    invoke-virtual {v1, v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇oo〇(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    new-instance v1, LO80OO/OO0o〇〇〇〇0;

    .line 167
    .line 168
    invoke-direct {v1, p0}, LO80OO/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 169
    .line 170
    .line 171
    const v2, 0x7f130615

    .line 172
    .line 173
    .line 174
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 179
    .line 180
    .line 181
    move-result-object v0

    .line 182
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 183
    .line 184
    .line 185
    return-void

    .line 186
    :cond_7
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 187
    .line 188
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SAVE_PDF_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 189
    .line 190
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 191
    .line 192
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 193
    .line 194
    .line 195
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 196
    .line 197
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 198
    .line 199
    .line 200
    move-result-object v0

    .line 201
    const/16 v1, 0x64

    .line 202
    .line 203
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 204
    .line 205
    .line 206
    return-void

    .line 207
    :cond_8
    :goto_3
    const-string v0, "vip user or signature is free now "

    .line 208
    .line 209
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO8()V

    .line 213
    .line 214
    .line 215
    return-void
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static final synthetic 〇8〇〇8o(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8〇OO:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8〇〇8〇8()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇OoO0o0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8o0o8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O〇〇〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 8

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getSize()Lcom/intsig/camscanner/util/ParcelSize;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 9
    .line 10
    if-eqz v1, :cond_4

    .line 11
    .line 12
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇o88o08〇:I

    .line 13
    .line 14
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    check-cast v1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 19
    .line 20
    if-nez v1, :cond_1

    .line 21
    .line 22
    goto/16 :goto_1

    .line 23
    .line 24
    :cond_1
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 25
    .line 26
    if-lez v2, :cond_2

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 29
    .line 30
    invoke-interface {v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    mul-int v2, v2, v1

    .line 39
    .line 40
    iget v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 41
    .line 42
    div-int/2addr v2, v1

    .line 43
    goto :goto_0

    .line 44
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 45
    .line 46
    invoke-interface {v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    mul-int v2, v2, v3

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    div-int/2addr v2, v1

    .line 61
    :goto_0
    iget-object v1, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    .line 62
    .line 63
    invoke-static {v1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const/16 v3, 0x32

    .line 68
    .line 69
    invoke-static {p0, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 78
    .line 79
    .line 80
    move-result v5

    .line 81
    invoke-static {v4, v5}, Lkotlin/ranges/RangesKt;->o〇0(II)I

    .line 82
    .line 83
    .line 84
    move-result v4

    .line 85
    int-to-float v5, v3

    .line 86
    const/high16 v6, 0x3f800000    # 1.0f

    .line 87
    .line 88
    mul-float v5, v5, v6

    .line 89
    .line 90
    int-to-float v4, v4

    .line 91
    div-float/2addr v5, v4

    .line 92
    if-nez v0, :cond_3

    .line 93
    .line 94
    new-instance v0, Lcom/intsig/camscanner/util/ParcelSize;

    .line 95
    .line 96
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 97
    .line 98
    .line 99
    move-result v4

    .line 100
    int-to-float v4, v4

    .line 101
    mul-float v4, v4, v5

    .line 102
    .line 103
    float-to-int v4, v4

    .line 104
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 105
    .line 106
    .line 107
    move-result v6

    .line 108
    int-to-float v6, v6

    .line 109
    mul-float v6, v6, v5

    .line 110
    .line 111
    float-to-int v5, v6

    .line 112
    invoke-direct {v0, v4, v5}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 113
    .line 114
    .line 115
    :cond_3
    invoke-static {}, Lcom/intsig/utils/CommonUtil;->OO0o〇〇〇〇0()Ljava/util/Random;

    .line 116
    .line 117
    .line 118
    move-result-object v4

    .line 119
    mul-int/lit8 v5, v3, 0x2

    .line 120
    .line 121
    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    .line 122
    .line 123
    .line 124
    move-result v4

    .line 125
    sub-int/2addr v4, v3

    .line 126
    invoke-static {}, Lcom/intsig/utils/CommonUtil;->OO0o〇〇〇〇0()Ljava/util/Random;

    .line 127
    .line 128
    .line 129
    move-result-object v6

    .line 130
    invoke-virtual {v6, v5}, Ljava/util/Random;->nextInt(I)I

    .line 131
    .line 132
    .line 133
    move-result v5

    .line 134
    sub-int/2addr v5, v3

    .line 135
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 136
    .line 137
    invoke-interface {v3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 138
    .line 139
    .line 140
    move-result v3

    .line 141
    div-int/lit8 v3, v3, 0x2

    .line 142
    .line 143
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 144
    .line 145
    .line 146
    move-result v6

    .line 147
    div-int/lit8 v6, v6, 0x2

    .line 148
    .line 149
    sub-int/2addr v3, v6

    .line 150
    add-int/2addr v3, v4

    .line 151
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 152
    .line 153
    invoke-interface {v6}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 154
    .line 155
    .line 156
    move-result v6

    .line 157
    div-int/lit8 v6, v6, 0x2

    .line 158
    .line 159
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 160
    .line 161
    .line 162
    move-result v7

    .line 163
    div-int/lit8 v7, v7, 0x2

    .line 164
    .line 165
    add-int/2addr v6, v7

    .line 166
    add-int/2addr v6, v4

    .line 167
    div-int/lit8 v2, v2, 0x2

    .line 168
    .line 169
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 170
    .line 171
    .line 172
    move-result v4

    .line 173
    div-int/lit8 v4, v4, 0x2

    .line 174
    .line 175
    sub-int v4, v2, v4

    .line 176
    .line 177
    add-int/2addr v4, v5

    .line 178
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 179
    .line 180
    .line 181
    move-result v7

    .line 182
    div-int/lit8 v7, v7, 0x2

    .line 183
    .line 184
    add-int/2addr v2, v7

    .line 185
    add-int/2addr v2, v5

    .line 186
    new-instance v5, Landroid/graphics/Rect;

    .line 187
    .line 188
    invoke-direct {v5, v3, v4, v6, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 189
    .line 190
    .line 191
    iput-object v0, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 192
    .line 193
    invoke-virtual {p1, v5}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setDisplayRect(Landroid/graphics/Rect;)V

    .line 194
    .line 195
    .line 196
    iput-object v1, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->rawSize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 197
    .line 198
    invoke-virtual {p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 199
    .line 200
    .line 201
    move-result p2

    .line 202
    iput p2, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 203
    .line 204
    :cond_4
    :goto_1
    return-void
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇o88〇O()V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->Oooo8o0〇(Landroid/content/Context;)Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x67

    .line 6
    .line 7
    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o8〇8()V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f0d0736

    .line 6
    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "null cannot be cast to non-null type android.widget.RelativeLayout"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 19
    .line 20
    const v1, 0x7f0a0a3d

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Landroid/widget/ImageView;

    .line 28
    .line 29
    const v3, 0x7f0a17d0

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    check-cast v3, Landroid/widget/TextView;

    .line 37
    .line 38
    const v4, 0x7f0a0fed

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    check-cast v4, Landroid/widget/RelativeLayout;

    .line 46
    .line 47
    new-instance v5, LO80OO/o800o8O;

    .line 48
    .line 49
    invoke-direct {v5, p0}, LO80OO/o800o8O;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oo08()Z

    .line 56
    .line 57
    .line 58
    move-result v5

    .line 59
    if-nez v5, :cond_0

    .line 60
    .line 61
    const v5, 0x7f080812

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    .line 66
    .line 67
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O00()Z

    .line 68
    .line 69
    .line 70
    move-result v5

    .line 71
    const/4 v6, 0x0

    .line 72
    if-eqz v5, :cond_1

    .line 73
    .line 74
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarMenu(Landroid/view/View;)V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {v4, v6, v6, v6, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 82
    .line 83
    .line 84
    const v1, 0x7f080dc9

    .line 85
    .line 86
    .line 87
    invoke-static {p0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    if-eqz v1, :cond_2

    .line 92
    .line 93
    const/16 v4, 0xc

    .line 94
    .line 95
    invoke-static {p0, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 96
    .line 97
    .line 98
    move-result v5

    .line 99
    invoke-static {p0, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 100
    .line 101
    .line 102
    move-result v4

    .line 103
    invoke-virtual {v1, v6, v6, v5, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 104
    .line 105
    .line 106
    :cond_2
    invoke-virtual {v3, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarWrapMenu(Landroid/view/View;)V

    .line 110
    .line 111
    .line 112
    :goto_0
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇oOO80o(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->OO〇000()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇oO〇(Z)V
    .locals 10

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 7
    .line 8
    .line 9
    move-result-object v3

    .line 10
    if-eqz v3, :cond_4

    .line 11
    .line 12
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 13
    .line 14
    if-eqz v3, :cond_4

    .line 15
    .line 16
    new-instance v4, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$updateSinglePage$1;

    .line 17
    .line 18
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$updateSinglePage$1;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->o〇0(Lkotlin/jvm/functions/Function0;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    if-eqz v3, :cond_1

    .line 30
    .line 31
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 32
    .line 33
    if-eqz v3, :cond_1

    .line 34
    .line 35
    invoke-static {v3, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 36
    .line 37
    .line 38
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    if-eqz v3, :cond_2

    .line 43
    .line 44
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->oOo0:Landroid/widget/TextView;

    .line 45
    .line 46
    if-eqz v3, :cond_2

    .line 47
    .line 48
    invoke-static {v3, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 49
    .line 50
    .line 51
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    if-eqz v3, :cond_3

    .line 56
    .line 57
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 58
    .line 59
    if-eqz v3, :cond_3

    .line 60
    .line 61
    invoke-static {v3, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 62
    .line 63
    .line 64
    :cond_3
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    const/4 v5, 0x0

    .line 69
    const/4 v6, 0x0

    .line 70
    new-instance v7, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$updateSinglePage$2;

    .line 71
    .line 72
    invoke-direct {v7, p0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$updateSinglePage$2;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lkotlin/coroutines/Continuation;)V

    .line 73
    .line 74
    .line 75
    const/4 v8, 0x3

    .line 76
    const/4 v9, 0x0

    .line 77
    invoke-static/range {v4 .. v9}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 78
    .line 79
    .line 80
    :cond_4
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇8O0O80〇()Z

    .line 81
    .line 82
    .line 83
    move-result v3

    .line 84
    if-eqz v3, :cond_5

    .line 85
    .line 86
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 87
    .line 88
    .line 89
    move-result-object v3

    .line 90
    if-eqz v3, :cond_5

    .line 91
    .line 92
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o8〇OO0〇0o:Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

    .line 93
    .line 94
    if-eqz v3, :cond_5

    .line 95
    .line 96
    const/high16 v4, 0x3f800000    # 1.0f

    .line 97
    .line 98
    invoke-virtual {v3, v4, v2, v2}, Lcom/intsig/camscanner/pic2word/view/ZoomLayout;->〇O888o0o(FII)V

    .line 99
    .line 100
    .line 101
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    if-eqz v3, :cond_6

    .line 106
    .line 107
    iget-object v1, v3, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o8〇OO0〇0o:Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

    .line 108
    .line 109
    :cond_6
    if-nez v1, :cond_7

    .line 110
    .line 111
    goto :goto_2

    .line 112
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇8O0O80〇()Z

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    if-eqz v3, :cond_8

    .line 117
    .line 118
    if-eqz p1, :cond_8

    .line 119
    .line 120
    goto :goto_1

    .line 121
    :cond_8
    const/4 v0, 0x0

    .line 122
    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 123
    .line 124
    .line 125
    :goto_2
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0〇o8〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇ooO〇000(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOOO0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;[Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇o〇88(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO0880O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇OO80oO(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOoo80oO:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/view/View;)V
    .locals 4

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8o0()Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    const-string p1, "sign"

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const-string p1, "empty"

    .line 16
    .line 17
    :goto_0
    const/4 v0, 0x3

    .line 18
    new-array v0, v0, [Landroid/util/Pair;

    .line 19
    .line 20
    new-instance v1, Landroid/util/Pair;

    .line 21
    .line 22
    const-string v2, "from"

    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8oOOo:Ljava/lang/String;

    .line 25
    .line 26
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    const/4 v2, 0x0

    .line 30
    aput-object v1, v0, v2

    .line 31
    .line 32
    new-instance v1, Landroid/util/Pair;

    .line 33
    .line 34
    const-string v2, "from_part"

    .line 35
    .line 36
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 37
    .line 38
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    const/4 v2, 0x1

    .line 42
    aput-object v1, v0, v2

    .line 43
    .line 44
    new-instance v1, Landroid/util/Pair;

    .line 45
    .line 46
    const-string v3, "sign_scheme"

    .line 47
    .line 48
    invoke-direct {v1, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 49
    .line 50
    .line 51
    const/4 p1, 0x2

    .line 52
    aput-object v1, v0, p1

    .line 53
    .line 54
    const-string p1, "CSAddSignature"

    .line 55
    .line 56
    const-string v1, "share"

    .line 57
    .line 58
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 59
    .line 60
    .line 61
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇OO〇00〇0O:Z

    .line 62
    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8〇o〇OoO8()V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇〇8o0OOOo(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;II)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇08oOOO0:I

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    if-eqz p0, :cond_0

    .line 13
    .line 14
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 15
    .line 16
    if-eqz p0, :cond_0

    .line 17
    .line 18
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇O00(I)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇O(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "PdfSignatureNewActivity"

    .line 7
    .line 8
    if-eqz p2, :cond_1

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    if-eq p2, v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const-string p2, "User Operation:  select from album"

    .line 15
    .line 16
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-static {p0, v0}, Lcom/intsig/camscanner/app/IntentUtil;->o〇0(Landroid/content/Context;Z)Landroid/content/Intent;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const-string p2, "has_max_count_limit"

    .line 24
    .line 25
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 26
    .line 27
    .line 28
    const-string p2, "max_count"

    .line 29
    .line 30
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 31
    .line 32
    .line 33
    const/16 p2, 0x65

    .line 34
    .line 35
    invoke-virtual {p0, p1, p2}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇80O()Lorg/json/JSONObject;

    .line 39
    .line 40
    .line 41
    move-result-object p0

    .line 42
    new-array p1, v0, [Landroid/util/Pair;

    .line 43
    .line 44
    new-instance p2, Landroid/util/Pair;

    .line 45
    .line 46
    const-string v0, "type"

    .line 47
    .line 48
    const-string v1, "import_album"

    .line 49
    .line 50
    invoke-direct {p2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    const/4 v0, 0x0

    .line 54
    aput-object p2, p1, v0

    .line 55
    .line 56
    const-string p2, "CSAddSignature"

    .line 57
    .line 58
    const-string v0, "create_signature_mode"

    .line 59
    .line 60
    invoke-static {p2, v0, p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->o〇0(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;[Landroid/util/Pair;)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_1
    const-string p2, "User Operation:  take photo"

    .line 65
    .line 66
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    new-instance p1, LO80OO/〇O8o08O;

    .line 70
    .line 71
    invoke-direct {p1, p0}, LO80OO/〇O8o08O;-><init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V

    .line 72
    .line 73
    .line 74
    invoke-static {p0, p1}, Lcom/intsig/util/PermissionUtil;->O8(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 75
    .line 76
    .line 77
    :goto_0
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇〇〇OOO〇〇(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0〇Ooo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O8o08O8O(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FII)Z
    .locals 18
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Point;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    move-object/from16 v3, p3

    .line 8
    .line 9
    move/from16 v4, p5

    .line 10
    .line 11
    move/from16 v5, p6

    .line 12
    .line 13
    const-string v6, "path"

    .line 14
    .line 15
    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const-string v6, "centerPoint"

    .line 19
    .line 20
    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const-string v6, "transformSize"

    .line 24
    .line 25
    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 29
    .line 30
    .line 31
    move-result v6

    .line 32
    const/4 v7, 0x0

    .line 33
    if-lez v6, :cond_1b

    .line 34
    .line 35
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 36
    .line 37
    .line 38
    move-result v6

    .line 39
    if-gtz v6, :cond_0

    .line 40
    .line 41
    goto/16 :goto_10

    .line 42
    .line 43
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 44
    .line 45
    .line 46
    move-result v6

    .line 47
    const/4 v8, 0x1

    .line 48
    if-eqz v6, :cond_2

    .line 49
    .line 50
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 51
    .line 52
    .line 53
    move-result-object v6

    .line 54
    if-eqz v6, :cond_1

    .line 55
    .line 56
    iget-object v6, v6, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 57
    .line 58
    if-eqz v6, :cond_1

    .line 59
    .line 60
    invoke-virtual {v6}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 61
    .line 62
    .line 63
    move-result v6

    .line 64
    if-nez v6, :cond_1

    .line 65
    .line 66
    const/4 v6, 0x1

    .line 67
    goto :goto_0

    .line 68
    :cond_1
    const/4 v6, 0x0

    .line 69
    :goto_0
    if-eqz v6, :cond_2

    .line 70
    .line 71
    invoke-direct/range {p0 .. p6}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0o〇o(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FII)Z

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    return v1

    .line 76
    :cond_2
    iget-object v6, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇o〇:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 77
    .line 78
    instance-of v9, v6, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 79
    .line 80
    if-eqz v9, :cond_3

    .line 81
    .line 82
    check-cast v6, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_3
    const/4 v6, 0x0

    .line 86
    :goto_1
    if-nez v6, :cond_4

    .line 87
    .line 88
    return v7

    .line 89
    :cond_4
    invoke-virtual {v6}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 90
    .line 91
    .line 92
    move-result v9

    .line 93
    invoke-virtual {v6}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 94
    .line 95
    .line 96
    move-result v11

    .line 97
    iget v12, v2, Landroid/graphics/Point;->y:I

    .line 98
    .line 99
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇8O0O80〇()Z

    .line 100
    .line 101
    .line 102
    move-result v13

    .line 103
    const/4 v14, 0x2

    .line 104
    if-eqz v13, :cond_7

    .line 105
    .line 106
    if-gt v9, v11, :cond_a

    .line 107
    .line 108
    :goto_2
    invoke-virtual {v6, v11}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 109
    .line 110
    .line 111
    move-result-object v13

    .line 112
    if-nez v13, :cond_5

    .line 113
    .line 114
    goto :goto_4

    .line 115
    :cond_5
    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    .line 116
    .line 117
    .line 118
    move-result v13

    .line 119
    if-le v12, v13, :cond_6

    .line 120
    .line 121
    sub-int/2addr v12, v13

    .line 122
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 123
    .line 124
    .line 125
    move-result v6

    .line 126
    div-int/2addr v6, v14

    .line 127
    :goto_3
    sub-int/2addr v12, v6

    .line 128
    goto :goto_6

    .line 129
    :cond_6
    :goto_4
    if-eq v11, v9, :cond_a

    .line 130
    .line 131
    add-int/lit8 v11, v11, -0x1

    .line 132
    .line 133
    goto :goto_2

    .line 134
    :cond_7
    iget-object v13, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 135
    .line 136
    invoke-interface {v13}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->O8()I

    .line 137
    .line 138
    .line 139
    move-result v13

    .line 140
    add-int/2addr v12, v13

    .line 141
    if-gt v9, v11, :cond_a

    .line 142
    .line 143
    :goto_5
    invoke-virtual {v6, v11}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 144
    .line 145
    .line 146
    move-result-object v13

    .line 147
    new-array v15, v14, [I

    .line 148
    .line 149
    if-eqz v13, :cond_8

    .line 150
    .line 151
    invoke-virtual {v13, v15}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 152
    .line 153
    .line 154
    :cond_8
    aget v13, v15, v8

    .line 155
    .line 156
    if-le v12, v13, :cond_9

    .line 157
    .line 158
    sub-int/2addr v12, v13

    .line 159
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 160
    .line 161
    .line 162
    move-result v6

    .line 163
    div-int/2addr v6, v14

    .line 164
    goto :goto_3

    .line 165
    :cond_9
    if-eq v11, v9, :cond_a

    .line 166
    .line 167
    add-int/lit8 v11, v11, -0x1

    .line 168
    .line 169
    goto :goto_5

    .line 170
    :cond_a
    const/4 v11, -0x1

    .line 171
    const/4 v12, -0x1

    .line 172
    :goto_6
    const-string v6, "PdfSignatureNewActivity"

    .line 173
    .line 174
    if-gez v11, :cond_b

    .line 175
    .line 176
    const-string v1, "onFinishEdit error ! "

    .line 177
    .line 178
    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    invoke-direct {v0, v7}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo8888(Z)V

    .line 182
    .line 183
    .line 184
    return v7

    .line 185
    :cond_b
    iget v2, v2, Landroid/graphics/Point;->x:I

    .line 186
    .line 187
    iget-object v9, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 188
    .line 189
    invoke-interface {v9}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$IPageProperty;->〇o〇()I

    .line 190
    .line 191
    .line 192
    move-result v9

    .line 193
    sub-int/2addr v2, v9

    .line 194
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 195
    .line 196
    .line 197
    move-result v9

    .line 198
    div-int/2addr v9, v14

    .line 199
    sub-int/2addr v2, v9

    .line 200
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 201
    .line 202
    .line 203
    move-result v9

    .line 204
    add-int/2addr v9, v2

    .line 205
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 206
    .line 207
    .line 208
    move-result v13

    .line 209
    add-int/2addr v13, v12

    .line 210
    iget v15, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O0O:I

    .line 211
    .line 212
    if-lez v15, :cond_10

    .line 213
    .line 214
    iget-object v15, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooo0〇〇O:Ljava/util/List;

    .line 215
    .line 216
    if-eqz v15, :cond_c

    .line 217
    .line 218
    invoke-static {v15, v11}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 219
    .line 220
    .line 221
    move-result-object v15

    .line 222
    check-cast v15, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 223
    .line 224
    goto :goto_7

    .line 225
    :cond_c
    const/4 v15, 0x0

    .line 226
    :goto_7
    iget-object v10, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 227
    .line 228
    if-eqz v10, :cond_d

    .line 229
    .line 230
    invoke-virtual {v10}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 231
    .line 232
    .line 233
    move-result-object v10

    .line 234
    if-eqz v10, :cond_d

    .line 235
    .line 236
    invoke-static {v10, v11}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 237
    .line 238
    .line 239
    move-result-object v10

    .line 240
    check-cast v10, Ljava/util/List;

    .line 241
    .line 242
    if-eqz v10, :cond_d

    .line 243
    .line 244
    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 245
    .line 246
    .line 247
    move-result-object v10

    .line 248
    check-cast v10, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;

    .line 249
    .line 250
    goto :goto_8

    .line 251
    :cond_d
    const/4 v10, 0x0

    .line 252
    :goto_8
    instance-of v8, v10, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 253
    .line 254
    if-eqz v8, :cond_e

    .line 255
    .line 256
    check-cast v10, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 257
    .line 258
    goto :goto_9

    .line 259
    :cond_e
    const/4 v10, 0x0

    .line 260
    :goto_9
    if-eqz v15, :cond_10

    .line 261
    .line 262
    if-eqz v10, :cond_10

    .line 263
    .line 264
    invoke-virtual {v10}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 265
    .line 266
    .line 267
    move-result-object v8

    .line 268
    invoke-virtual {v15}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 269
    .line 270
    .line 271
    move-result v7

    .line 272
    move-object/from16 p2, v15

    .line 273
    .line 274
    int-to-double v14, v7

    .line 275
    invoke-virtual {v10}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;->〇o〇()Lcom/intsig/camscanner/util/ParcelSize;

    .line 276
    .line 277
    .line 278
    move-result-object v7

    .line 279
    invoke-virtual {v7}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 280
    .line 281
    .line 282
    move-result v7

    .line 283
    move-object/from16 v16, v6

    .line 284
    .line 285
    int-to-double v6, v7

    .line 286
    div-double/2addr v14, v6

    .line 287
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 288
    .line 289
    .line 290
    move-result v6

    .line 291
    int-to-double v6, v6

    .line 292
    invoke-virtual {v10}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;->〇o〇()Lcom/intsig/camscanner/util/ParcelSize;

    .line 293
    .line 294
    .line 295
    move-result-object v10

    .line 296
    invoke-virtual {v10}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 297
    .line 298
    .line 299
    move-result v10

    .line 300
    move/from16 v17, v11

    .line 301
    .line 302
    int-to-double v10, v10

    .line 303
    div-double/2addr v6, v10

    .line 304
    iget-object v10, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 305
    .line 306
    if-nez v10, :cond_f

    .line 307
    .line 308
    move/from16 p2, v2

    .line 309
    .line 310
    move v5, v9

    .line 311
    goto :goto_a

    .line 312
    :cond_f
    new-instance v11, Landroid/graphics/Rect;

    .line 313
    .line 314
    iget v1, v8, Landroid/graphics/Rect;->left:I

    .line 315
    .line 316
    sub-int v3, v2, v1

    .line 317
    .line 318
    move/from16 p2, v2

    .line 319
    .line 320
    int-to-double v2, v3

    .line 321
    mul-double v2, v2, v14

    .line 322
    .line 323
    double-to-int v2, v2

    .line 324
    iget v3, v8, Landroid/graphics/Rect;->top:I

    .line 325
    .line 326
    sub-int v8, v12, v3

    .line 327
    .line 328
    int-to-double v4, v8

    .line 329
    mul-double v4, v4, v6

    .line 330
    .line 331
    double-to-int v4, v4

    .line 332
    sub-int v1, v9, v1

    .line 333
    .line 334
    move v5, v9

    .line 335
    int-to-double v8, v1

    .line 336
    mul-double v14, v14, v8

    .line 337
    .line 338
    double-to-int v1, v14

    .line 339
    sub-int v3, v13, v3

    .line 340
    .line 341
    int-to-double v8, v3

    .line 342
    mul-double v6, v6, v8

    .line 343
    .line 344
    double-to-int v3, v6

    .line 345
    invoke-direct {v11, v2, v4, v1, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 346
    .line 347
    .line 348
    iput-object v11, v10, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->canvasRect:Landroid/graphics/Rect;

    .line 349
    .line 350
    goto :goto_a

    .line 351
    :cond_10
    move/from16 p2, v2

    .line 352
    .line 353
    move-object/from16 v16, v6

    .line 354
    .line 355
    move v5, v9

    .line 356
    move/from16 v17, v11

    .line 357
    .line 358
    :goto_a
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 359
    .line 360
    .line 361
    move-result-object v1

    .line 362
    if-eqz v1, :cond_12

    .line 363
    .line 364
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 365
    .line 366
    if-eqz v1, :cond_12

    .line 367
    .line 368
    iget-object v2, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 369
    .line 370
    if-eqz v2, :cond_11

    .line 371
    .line 372
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getPath()Ljava/lang/String;

    .line 373
    .line 374
    .line 375
    move-result-object v2

    .line 376
    move/from16 v3, p5

    .line 377
    .line 378
    move/from16 v4, p6

    .line 379
    .line 380
    goto :goto_b

    .line 381
    :cond_11
    move/from16 v3, p5

    .line 382
    .line 383
    move/from16 v4, p6

    .line 384
    .line 385
    const/4 v2, 0x0

    .line 386
    :goto_b
    invoke-virtual {v1, v2, v3, v4}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->o8(Ljava/lang/String;II)V

    .line 387
    .line 388
    .line 389
    goto :goto_c

    .line 390
    :cond_12
    move/from16 v3, p5

    .line 391
    .line 392
    move/from16 v4, p6

    .line 393
    .line 394
    :goto_c
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 395
    .line 396
    .line 397
    move-result-object v1

    .line 398
    if-eqz v1, :cond_14

    .line 399
    .line 400
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 401
    .line 402
    if-eqz v1, :cond_14

    .line 403
    .line 404
    iget-object v2, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 405
    .line 406
    if-eqz v2, :cond_13

    .line 407
    .line 408
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getPath()Ljava/lang/String;

    .line 409
    .line 410
    .line 411
    move-result-object v2

    .line 412
    move-object/from16 v6, p3

    .line 413
    .line 414
    goto :goto_d

    .line 415
    :cond_13
    move-object/from16 v6, p3

    .line 416
    .line 417
    const/4 v2, 0x0

    .line 418
    :goto_d
    invoke-virtual {v1, v2, v6}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->〇〇〇0〇〇0(Ljava/lang/String;Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 419
    .line 420
    .line 421
    goto :goto_e

    .line 422
    :cond_14
    move-object/from16 v6, p3

    .line 423
    .line 424
    :goto_e
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 425
    .line 426
    .line 427
    move-result-object v1

    .line 428
    if-eqz v1, :cond_15

    .line 429
    .line 430
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 431
    .line 432
    if-eqz v1, :cond_15

    .line 433
    .line 434
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->OOO〇O0()V

    .line 435
    .line 436
    .line 437
    :cond_15
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 438
    .line 439
    if-eqz v1, :cond_16

    .line 440
    .line 441
    move-object/from16 v2, p1

    .line 442
    .line 443
    iput-object v2, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    .line 444
    .line 445
    iput v3, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 446
    .line 447
    iput v4, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 448
    .line 449
    new-instance v2, Landroid/graphics/Rect;

    .line 450
    .line 451
    move/from16 v3, p2

    .line 452
    .line 453
    invoke-direct {v2, v3, v12, v5, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 454
    .line 455
    .line 456
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setDisplayRect(Landroid/graphics/Rect;)V

    .line 457
    .line 458
    .line 459
    new-instance v2, Landroid/graphics/Point;

    .line 460
    .line 461
    add-int/2addr v3, v5

    .line 462
    const/4 v4, 0x2

    .line 463
    div-int/2addr v3, v4

    .line 464
    add-int/2addr v12, v13

    .line 465
    div-int/2addr v12, v4

    .line 466
    invoke-direct {v2, v3, v12}, Landroid/graphics/Point;-><init>(II)V

    .line 467
    .line 468
    .line 469
    iput-object v2, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->centerPoint:Landroid/graphics/Point;

    .line 470
    .line 471
    iput-object v6, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 472
    .line 473
    move/from16 v2, p4

    .line 474
    .line 475
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setRotation(F)V

    .line 476
    .line 477
    .line 478
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 479
    .line 480
    .line 481
    move-result-object v1

    .line 482
    if-eqz v1, :cond_19

    .line 483
    .line 484
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 485
    .line 486
    if-eqz v1, :cond_19

    .line 487
    .line 488
    move/from16 v11, v17

    .line 489
    .line 490
    invoke-virtual {v1, v11}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForLayoutPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 491
    .line 492
    .line 493
    move-result-object v1

    .line 494
    if-eqz v1, :cond_19

    .line 495
    .line 496
    iget-object v2, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 497
    .line 498
    if-eqz v2, :cond_18

    .line 499
    .line 500
    iput v11, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇〇0o〇〇0:I

    .line 501
    .line 502
    iget-object v3, v0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 503
    .line 504
    if-eqz v3, :cond_17

    .line 505
    .line 506
    const/4 v4, 0x0

    .line 507
    invoke-virtual {v3, v1, v11, v2, v4}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇O00(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;ILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Z)V

    .line 508
    .line 509
    .line 510
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooo800〇〇()V

    .line 511
    .line 512
    .line 513
    move-object v10, v2

    .line 514
    goto :goto_f

    .line 515
    :cond_18
    const/4 v10, 0x0

    .line 516
    :goto_f
    if-nez v10, :cond_1a

    .line 517
    .line 518
    :cond_19
    const-string v1, "onFinishEdit error"

    .line 519
    .line 520
    move-object/from16 v2, v16

    .line 521
    .line 522
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    .line 524
    .line 525
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 526
    .line 527
    :cond_1a
    const/4 v1, 0x0

    .line 528
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo8888(Z)V

    .line 529
    .line 530
    .line 531
    const/4 v1, 0x1

    .line 532
    return v1

    .line 533
    :cond_1b
    :goto_10
    const/4 v1, 0x0

    .line 534
    return v1
.end method

.method public O8〇o()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->Oo08()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    if-eqz v0, :cond_2

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 23
    .line 24
    .line 25
    :cond_1
    const/4 v0, 0x0

    .line 26
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 27
    .line 28
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 29
    .line 30
    if-nez v0, :cond_3

    .line 31
    .line 32
    invoke-static {p0, v1}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 37
    .line 38
    if-eqz v0, :cond_3

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 41
    .line 42
    .line 43
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 44
    .line 45
    if-eqz v0, :cond_4

    .line 46
    .line 47
    const v1, 0x7f131ec6

    .line 48
    .line 49
    .line 50
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 55
    .line 56
    .line 57
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 58
    .line 59
    if-eqz v0, :cond_5

    .line 60
    .line 61
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->o〇0OOo〇0(I)V

    .line 62
    .line 63
    .line 64
    :cond_5
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 65
    .line 66
    if-eqz p1, :cond_6

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .line 70
    .line 71
    goto :goto_1

    .line 72
    :catch_0
    move-exception p1

    .line 73
    const-string v0, "PdfSignatureNewActivity"

    .line 74
    .line 75
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 76
    .line 77
    .line 78
    :cond_6
    :goto_1
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public Oooo8o0〇()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->o〇0()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo〇O8o〇8(Lcom/intsig/camscanner/util/ParcelSize;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "transformSize"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "onScaleEnd, size: "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v1, "PdfSignatureNewActivity"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 35
    .line 36
    if-eqz v0, :cond_0

    .line 37
    .line 38
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->〇O00(Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 39
    .line 40
    .line 41
    :cond_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getPageCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo80:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->getItemCount()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO8o〇08〇()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8ooo()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oOO0880O()Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->getPageCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0x32

    .line 6
    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    const/4 v3, 0x0

    .line 12
    const/4 v4, 0x1

    .line 13
    if-gt v0, v4, :cond_0

    .line 14
    .line 15
    new-array v0, v4, [Ljava/lang/Object;

    .line 16
    .line 17
    aput-object v2, v0, v3

    .line 18
    .line 19
    const v1, 0x7f13121d

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const/4 v1, 0x1

    .line 27
    goto :goto_1

    .line 28
    :cond_0
    if-le v0, v1, :cond_1

    .line 29
    .line 30
    const/4 v0, 0x2

    .line 31
    new-array v0, v0, [Ljava/lang/Object;

    .line 32
    .line 33
    aput-object v2, v0, v3

    .line 34
    .line 35
    aput-object v2, v0, v4

    .line 36
    .line 37
    const v1, 0x7f13121c

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    goto :goto_0

    .line 45
    :cond_1
    const-string v0, ""

    .line 46
    .line 47
    :goto_0
    const/4 v1, 0x0

    .line 48
    :goto_1
    const-string v2, "if (page <= 1) {\n       \u2026\n            \"\"\n        }"

    .line 49
    .line 50
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    if-nez v2, :cond_2

    .line 58
    .line 59
    const/4 v2, 0x1

    .line 60
    goto :goto_2

    .line 61
    :cond_2
    const/4 v2, 0x0

    .line 62
    :goto_2
    if-eqz v2, :cond_3

    .line 63
    .line 64
    return v4

    .line 65
    :cond_3
    new-instance v2, Lcom/intsig/app/AlertDialog$Builder;

    .line 66
    .line 67
    invoke-direct {v2, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    .line 69
    .line 70
    const v4, 0x7f1300a9

    .line 71
    .line 72
    .line 73
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v4

    .line 77
    invoke-virtual {v2, v4}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-virtual {v2, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    new-instance v2, LO80OO/〇〇888;

    .line 86
    .line 87
    invoke-direct {v2}, LO80OO/〇〇888;-><init>()V

    .line 88
    .line 89
    .line 90
    const v4, 0x7f130019

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0, v4, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 102
    .line 103
    .line 104
    const-string v0, "paging_seal"

    .line 105
    .line 106
    if-eqz v1, :cond_4

    .line 107
    .line 108
    const-string v1, "single_page"

    .line 109
    .line 110
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0〇OO008O(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    goto :goto_3

    .line 114
    :cond_4
    const-string v1, "many_pages"

    .line 115
    .line 116
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0〇OO008O(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    :goto_3
    return v3
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, "requestCode == "

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, "resultCode == "

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string v1, "PdfSignatureNewActivity"

    .line 30
    .line 31
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/4 v0, -0x1

    .line 35
    if-eq p2, v0, :cond_0

    .line 36
    .line 37
    return-void

    .line 38
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 39
    .line 40
    .line 41
    goto/16 :goto_1

    .line 42
    .line 43
    :pswitch_0
    if-eqz p3, :cond_1

    .line 44
    .line 45
    const-string p2, "extra_path"

    .line 46
    .line 47
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    if-nez p2, :cond_2

    .line 52
    .line 53
    :cond_1
    const-string p2, ""

    .line 54
    .line 55
    :cond_2
    new-instance p3, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 56
    .line 57
    invoke-direct {p3, p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;-><init>(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 61
    .line 62
    .line 63
    move-result p2

    .line 64
    if-eqz p2, :cond_3

    .line 65
    .line 66
    const/high16 p2, -0x10000

    .line 67
    .line 68
    invoke-virtual {p3, p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setColor(I)V

    .line 69
    .line 70
    .line 71
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 72
    .line 73
    .line 74
    move-result-object p2

    .line 75
    if-eqz p2, :cond_4

    .line 76
    .line 77
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 78
    .line 79
    if-eqz p2, :cond_4

    .line 80
    .line 81
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->〇O00(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 82
    .line 83
    .line 84
    :cond_4
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇0o8〇(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 85
    .line 86
    .line 87
    new-instance p2, Ljava/util/HashMap;

    .line 88
    .line 89
    const/4 p3, 0x1

    .line 90
    invoke-direct {p2, p3}, Ljava/util/HashMap;-><init>(I)V

    .line 91
    .line 92
    .line 93
    const/16 p3, 0x67

    .line 94
    .line 95
    if-ne p1, p3, :cond_5

    .line 96
    .line 97
    const-string p1, "scan"

    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_5
    const-string p1, "gallery"

    .line 101
    .line 102
    :goto_0
    const-string p3, "from"

    .line 103
    .line 104
    invoke-interface {p2, p3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    .line 106
    .line 107
    const-string p1, "signature_save"

    .line 108
    .line 109
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo0oO0(Ljava/util/HashMap;)Lorg/json/JSONObject;

    .line 110
    .line 111
    .line 112
    move-result-object p2

    .line 113
    const-string p3, "CSAddSignature"

    .line 114
    .line 115
    invoke-static {p3, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 116
    .line 117
    .line 118
    goto :goto_1

    .line 119
    :pswitch_1
    if-eqz p3, :cond_7

    .line 120
    .line 121
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    if-eqz p1, :cond_7

    .line 126
    .line 127
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    const/16 p2, 0x66

    .line 132
    .line 133
    const/4 p3, 0x0

    .line 134
    invoke-static {p0, p1, p3, p3, p2}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->startActivityForResult(Landroid/app/Activity;Landroid/net/Uri;FFI)V

    .line 135
    .line 136
    .line 137
    goto :goto_1

    .line 138
    :pswitch_2
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O00()Z

    .line 139
    .line 140
    .line 141
    move-result p1

    .line 142
    if-nez p1, :cond_6

    .line 143
    .line 144
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 145
    .line 146
    .line 147
    move-result p1

    .line 148
    if-eqz p1, :cond_7

    .line 149
    .line 150
    :cond_6
    const-string p1, "onActivityResult, vip user or signature is free now "

    .line 151
    .line 152
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oO8()V

    .line 156
    .line 157
    .line 158
    :cond_7
    :goto_1
    return-void

    .line 159
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0o88O()V

    .line 5
    .line 6
    .line 7
    const/4 p1, 0x1

    .line 8
    return p1

    .line 9
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->OOO〇O0()V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onScaleChanged()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇o〇Oo88:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oo88o8O()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->〇〇888()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public ooo0〇〇O(IILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Landroid/graphics/Point;)V
    .locals 11
    .param p3    # Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Point;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "pdfSignatureModel"

    .line 2
    .line 3
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "centerPoint"

    .line 7
    .line 8
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "mPdfSignatureActionView page: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string p1, " | topicIndex : "

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const-string p2, "PdfSignatureNewActivity"

    .line 37
    .line 38
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    const/4 p2, 0x1

    .line 46
    const/4 v0, 0x0

    .line 47
    if-eqz p1, :cond_1

    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    if-eqz p1, :cond_0

    .line 54
    .line 55
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 56
    .line 57
    if-eqz p1, :cond_0

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    if-nez p1, :cond_0

    .line 64
    .line 65
    const/4 p1, 0x1

    .line 66
    goto :goto_0

    .line 67
    :cond_0
    const/4 p1, 0x0

    .line 68
    :goto_0
    if-eqz p1, :cond_1

    .line 69
    .line 70
    iput-object p3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_1
    iput-object p3, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 74
    .line 75
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    if-eqz p1, :cond_3

    .line 80
    .line 81
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    if-eqz p1, :cond_2

    .line 86
    .line 87
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 88
    .line 89
    if-eqz p1, :cond_2

    .line 90
    .line 91
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-nez p1, :cond_2

    .line 96
    .line 97
    const/4 p1, 0x1

    .line 98
    goto :goto_2

    .line 99
    :cond_2
    const/4 p1, 0x0

    .line 100
    :goto_2
    if-eqz p1, :cond_3

    .line 101
    .line 102
    iget p1, p4, Landroid/graphics/Point;->y:I

    .line 103
    .line 104
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 105
    .line 106
    invoke-interface {v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->O8()I

    .line 107
    .line 108
    .line 109
    move-result v1

    .line 110
    sub-int/2addr p1, v1

    .line 111
    iput p1, p4, Landroid/graphics/Point;->y:I

    .line 112
    .line 113
    goto :goto_3

    .line 114
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇8O0O80〇()Z

    .line 115
    .line 116
    .line 117
    move-result p1

    .line 118
    if-eqz p1, :cond_4

    .line 119
    .line 120
    iget p1, p4, Landroid/graphics/Point;->x:I

    .line 121
    .line 122
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 123
    .line 124
    invoke-interface {v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$IPageProperty;->〇o〇()I

    .line 125
    .line 126
    .line 127
    move-result v1

    .line 128
    add-int/2addr p1, v1

    .line 129
    iput p1, p4, Landroid/graphics/Point;->x:I

    .line 130
    .line 131
    goto :goto_3

    .line 132
    :cond_4
    iget p1, p4, Landroid/graphics/Point;->y:I

    .line 133
    .line 134
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇oO:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 135
    .line 136
    invoke-interface {v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->O8()I

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    sub-int/2addr p1, v1

    .line 141
    iput p1, p4, Landroid/graphics/Point;->y:I

    .line 142
    .line 143
    :goto_3
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->originalDisplaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 144
    .line 145
    if-nez p1, :cond_5

    .line 146
    .line 147
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 148
    .line 149
    iput-object p1, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->originalDisplaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 150
    .line 151
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    if-eqz p1, :cond_6

    .line 156
    .line 157
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 158
    .line 159
    if-eqz v1, :cond_6

    .line 160
    .line 161
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getPath()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v2

    .line 165
    iget-object v3, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    .line 166
    .line 167
    iget v4, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 168
    .line 169
    iget v5, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 170
    .line 171
    iget-object v7, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 172
    .line 173
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getRotation()F

    .line 174
    .line 175
    .line 176
    move-result v8

    .line 177
    const/4 v9, 0x1

    .line 178
    iget-object v10, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->originalDisplaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 179
    .line 180
    move-object v6, p4

    .line 181
    invoke-virtual/range {v1 .. v10}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FZLcom/intsig/camscanner/util/ParcelSize;)V

    .line 182
    .line 183
    .line 184
    :cond_6
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo8888(Z)V

    .line 185
    .line 186
    .line 187
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 188
    .line 189
    .line 190
    move-result p1

    .line 191
    if-eqz p1, :cond_9

    .line 192
    .line 193
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 194
    .line 195
    .line 196
    move-result-object p1

    .line 197
    if-eqz p1, :cond_7

    .line 198
    .line 199
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 200
    .line 201
    if-eqz p1, :cond_7

    .line 202
    .line 203
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 204
    .line 205
    .line 206
    move-result p1

    .line 207
    if-nez p1, :cond_7

    .line 208
    .line 209
    const/4 p1, 0x1

    .line 210
    goto :goto_4

    .line 211
    :cond_7
    const/4 p1, 0x0

    .line 212
    :goto_4
    if-eqz p1, :cond_9

    .line 213
    .line 214
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 215
    .line 216
    .line 217
    move-result-object p1

    .line 218
    if-eqz p1, :cond_8

    .line 219
    .line 220
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 221
    .line 222
    if-eqz p1, :cond_8

    .line 223
    .line 224
    const/4 p3, 0x2

    .line 225
    const/4 p4, 0x0

    .line 226
    invoke-static {p1, v0, v0, p3, p4}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->OoO8(Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;ZIILjava/lang/Object;)V

    .line 227
    .line 228
    .line 229
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo0O()V

    .line 230
    .line 231
    .line 232
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 233
    .line 234
    .line 235
    move-result-object p1

    .line 236
    if-eqz p1, :cond_9

    .line 237
    .line 238
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 239
    .line 240
    if-eqz p1, :cond_9

    .line 241
    .line 242
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->o〇0OOo〇0(Z)V

    .line 243
    .line 244
    .line 245
    :cond_9
    return-void
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method public oo〇(FF)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->O8o08O8O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->〇〇888()V

    .line 12
    .line 13
    .line 14
    :cond_0
    const/4 p1, 0x1

    .line 15
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o〇o〇Oo88:Z

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇0()Landroid/content/Context;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "null cannot be cast to non-null type com.intsig.camscanner.pdf.signature.tab.PdfSignatureNewActivity"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    move-object v0, p0

    .line 20
    :goto_0
    return-object v0
    .line 21
.end method

.method public o〇00O()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 8
    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    xor-int/lit8 v2, v1, 0x1

    .line 16
    .line 17
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇oO〇(Z)V

    .line 18
    .line 19
    .line 20
    xor-int/lit8 v2, v1, 0x1

    .line 21
    .line 22
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->〇0〇O0088o(Z)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo8888(Z)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 36
    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->setIsPagingSeal(Z)V

    .line 40
    .line 41
    .line 42
    :cond_0
    if-eqz v1, :cond_1

    .line 43
    .line 44
    const-string v0, "close"

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    const-string v0, "open"

    .line 48
    .line 49
    :goto_0
    const-string v1, "single_effect"

    .line 50
    .line 51
    const-string v2, "type"

    .line 52
    .line 53
    const-string v3, "CSAddSignature"

    .line 54
    .line 55
    invoke-static {v3, v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    :cond_2
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d00a0

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇〇0〇88(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooO:Ljava/util/ArrayList;

    .line 2
    .line 3
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇OO〇00〇0O:Z

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooooo0O()V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O88O:Z

    .line 12
    .line 13
    if-eqz p1, :cond_2

    .line 14
    .line 15
    const-string p1, "PdfSignatureNewActivity"

    .line 16
    .line 17
    const-string v0, "batch handle images finish, go to view doc"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    new-instance p1, Landroid/content/Intent;

    .line 23
    .line 24
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 25
    .line 26
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇08O:J

    .line 27
    .line 28
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-class v1, Lcom/intsig/camscanner/DocumentActivity;

    .line 33
    .line 34
    const-string v2, "android.intent.action.VIEW"

    .line 35
    .line 36
    invoke-direct {p1, v2, v0, p0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .line 38
    .line 39
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇oo〇O〇80:Z

    .line 40
    .line 41
    if-nez v0, :cond_1

    .line 42
    .line 43
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 44
    .line 45
    .line 46
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_2
    new-instance p1, Landroid/content/Intent;

    .line 51
    .line 52
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 53
    .line 54
    .line 55
    const-string v0, "data_with_pdf_signature"

    .line 56
    .line 57
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->ooO:Ljava/util/ArrayList;

    .line 58
    .line 59
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 60
    .line 61
    .line 62
    const/16 v0, 0xc9

    .line 63
    .line 64
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 68
    .line 69
    .line 70
    :goto_0
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇0000OOO()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇00O0O0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇OO8ooO8〇:I

    .line 2
    .line 3
    if-gtz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->O8o08O8O:Landroidx/appcompat/widget/Toolbar;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇OO8ooO8〇:I

    .line 12
    .line 13
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇OO8ooO8〇:I

    .line 14
    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇08〇0〇o〇8(Lcom/intsig/camscanner/signature/ActionType;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O〇8O0O80〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x1

    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-ne v0, v1, :cond_1

    .line 31
    .line 32
    const/4 v0, 0x1

    .line 33
    goto :goto_0

    .line 34
    :cond_1
    const/4 v0, 0x0

    .line 35
    :goto_0
    if-eqz v0, :cond_6

    .line 36
    .line 37
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o8〇OO0〇0o:Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_3
    const/4 v0, 0x0

    .line 47
    :goto_1
    if-nez v0, :cond_4

    .line 48
    .line 49
    goto :goto_3

    .line 50
    :cond_4
    sget-object v3, Lcom/intsig/camscanner/signature/ActionType;->ActionTouch:Lcom/intsig/camscanner/signature/ActionType;

    .line 51
    .line 52
    if-eq v3, p1, :cond_5

    .line 53
    .line 54
    sget-object v3, Lcom/intsig/camscanner/signature/ActionType;->ActionControl:Lcom/intsig/camscanner/signature/ActionType;

    .line 55
    .line 56
    if-eq v3, p1, :cond_5

    .line 57
    .line 58
    goto :goto_2

    .line 59
    :cond_5
    const/4 v1, 0x0

    .line 60
    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 61
    .line 62
    .line 63
    :cond_6
    :goto_3
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇8o8O〇O(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "path"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo88()Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    const/4 v0, 0x0

    .line 11
    if-eqz p1, :cond_2

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 20
    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    if-nez p1, :cond_0

    .line 28
    .line 29
    const/4 p1, 0x1

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 p1, 0x0

    .line 32
    :goto_0
    if-eqz p1, :cond_2

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    if-eqz p1, :cond_1

    .line 39
    .line 40
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->o〇00O:Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;

    .line 41
    .line 42
    if-eqz p1, :cond_1

    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/signature/tab/PagingSealPdfView;->〇O〇()V

    .line 45
    .line 46
    .line 47
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oo0O()V

    .line 48
    .line 49
    .line 50
    :cond_2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8oo8888(Z)V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇8o8o〇(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public synthetic 〇8〇oO〇〇8o(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/〇080;->〇080(Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O80〇oOo()V
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0〇Ooo:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0〇Ooo:Z

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oooO8〇00()Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v1, 0x0

    .line 14
    if-eqz v0, :cond_5

    .line 15
    .line 16
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇0O:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;

    .line 17
    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    goto :goto_2

    .line 21
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEditNewView;->O8ooOoo〇()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    const-string v2, "close"

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_2
    const-string v2, "open"

    .line 31
    .line 32
    :goto_0
    const-string v3, "apply_all"

    .line 33
    .line 34
    const-string v4, "type"

    .line 35
    .line 36
    const-string v5, "CSAddSignature"

    .line 37
    .line 38
    invoke-static {v5, v3, v4, v2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->O08〇oO8〇()V

    .line 44
    .line 45
    .line 46
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0〇Ooo:Z

    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_3
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇〇〇0o〇〇0:I

    .line 50
    .line 51
    const/4 v2, -0x1

    .line 52
    if-eq v0, v2, :cond_4

    .line 53
    .line 54
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Ooo08:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 55
    .line 56
    if-eqz v2, :cond_4

    .line 57
    .line 58
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->OOo00(ILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;)V

    .line 62
    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_4
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0〇Ooo:Z

    .line 66
    .line 67
    :goto_1
    return-void

    .line 68
    :cond_5
    :goto_2
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->Oo0〇Ooo:Z

    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇Oo〇O()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇0o88O()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :catch_0
    move-exception v0

    .line 12
    goto :goto_1

    .line 13
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o8O:Lcom/intsig/app/BaseProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 15
    .line 16
    goto :goto_2

    .line 17
    :goto_1
    const-string v1, "PdfSignatureNewActivity"

    .line 18
    .line 19
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    .line 21
    .line 22
    :cond_1
    :goto_2
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.class public Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;
.super Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;
.source "PdfSignatureModel.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# instance fields
.field public canvasRect:Landroid/graphics/Rect;

.field public centerPoint:Landroid/graphics/Point;

.field public clipIndex:[I

.field public color:I

.field public displaySize:Lcom/intsig/camscanner/util/ParcelSize;

.field public isPagingSeal:Z

.field public mTempPath:Ljava/lang/String;

.field public originalDisplaySize:Lcom/intsig/camscanner/util/ParcelSize;

.field public rawSize:Lcom/intsig/camscanner/util/ParcelSize;

.field public size:I

.field public type:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;Lcom/intsig/camscanner/util/ParcelSize;FLandroid/graphics/Rect;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Point;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # F
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 7
    invoke-direct {p0, p1, p5}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;-><init>(Ljava/lang/String;F)V

    const/high16 p1, -0x1000000

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    const/4 p1, 0x0

    .line 9
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->isPagingSeal:Z

    const/4 p1, 0x2

    new-array p1, p1, [I

    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->clipIndex:[I

    .line 12
    iput-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->centerPoint:Landroid/graphics/Point;

    .line 13
    iput-object p3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 14
    iput-object p4, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->rawSize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 15
    invoke-virtual {p0, p6}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setDisplayRect(Landroid/graphics/Rect;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;-><init>(Ljava/lang/String;F)V

    const/high16 p1, -0x1000000

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    const/4 p1, 0x0

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 4
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->isPagingSeal:Z

    const/4 p1, 0x2

    new-array p1, p1, [I

    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->clipIndex:[I

    .line 6
    iput-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "PdfSignatureModel{displaySize="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, ", rawSize="

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->rawSize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, ", rotation="

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getRotation()F

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v1, ", displayRect="

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    const/16 v1, 0x7d

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    return-object v0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

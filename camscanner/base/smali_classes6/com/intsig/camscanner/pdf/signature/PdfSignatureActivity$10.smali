.class Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;
.super Ljava/lang/Object;
.source "PdfSignatureActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o088O8800()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;->o0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .line 1
    const-string p1, "PdfSignatureActivity"

    .line 2
    .line 3
    if-eqz p2, :cond_1

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    if-eq p2, v0, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const-string p2, "User Operation:  select from album"

    .line 10
    .line 11
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;->o0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;

    .line 15
    .line 16
    invoke-static {p1, v0}, Lcom/intsig/camscanner/app/IntentUtil;->o〇0(Landroid/content/Context;Z)Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string p2, "has_max_count_limit"

    .line 21
    .line 22
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    const-string p2, "max_count"

    .line 26
    .line 27
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 28
    .line 29
    .line 30
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;->o0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;

    .line 31
    .line 32
    const/16 v1, 0x65

    .line 33
    .line 34
    invoke-virtual {p2, p1, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;->o0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇〇o8O()Lorg/json/JSONObject;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    new-array p2, v0, [Landroid/util/Pair;

    .line 44
    .line 45
    new-instance v0, Landroid/util/Pair;

    .line 46
    .line 47
    const-string v1, "type"

    .line 48
    .line 49
    const-string v2, "import_album"

    .line 50
    .line 51
    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 52
    .line 53
    .line 54
    const/4 v1, 0x0

    .line 55
    aput-object v0, p2, v1

    .line 56
    .line 57
    const-string v0, "CSAddSignature"

    .line 58
    .line 59
    const-string v1, "create_signature_mode"

    .line 60
    .line 61
    invoke-static {v0, v1, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->o〇0(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;[Landroid/util/Pair;)V

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_1
    const-string p2, "User Operation:  take photo"

    .line 66
    .line 67
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;->o0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;

    .line 71
    .line 72
    new-instance p2, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10$1;

    .line 73
    .line 74
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10$1;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;)V

    .line 75
    .line 76
    .line 77
    invoke-static {p1, p2}, Lcom/intsig/util/PermissionUtil;->O8(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 78
    .line 79
    .line 80
    :goto_0
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

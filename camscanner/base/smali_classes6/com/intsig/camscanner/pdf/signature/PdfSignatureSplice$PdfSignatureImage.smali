.class public Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;
.super Ljava/lang/Object;
.source "PdfSignatureSplice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PdfSignatureImage"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private OO:Ljava/lang/String;

.field private o0:J

.field private 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-wide p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->o0:J

    .line 3
    iput-object p3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇OOo8〇0:Ljava/lang/String;

    .line 4
    iput-object p4, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->OO:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->o0:J

    .line 7
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇OOo8〇0:Ljava/lang/String;

    .line 8
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->OO:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public O8(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->o0:J

    .line 2
    .line 3
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 4
    .line 5
    .line 6
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇OOo8〇0:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->OO:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->o0:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->OO:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

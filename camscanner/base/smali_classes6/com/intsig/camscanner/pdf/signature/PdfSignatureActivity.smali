.class public Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "PdfSignatureActivity.java"

# interfaces
.implements Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;
.implements Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$View;
.implements Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;


# static fields
.field private static oOO8:I


# instance fields
.field private O0O:Landroid/widget/LinearLayout;

.field private final O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

.field private O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

.field private OO〇OOo:I

.field private final Oo0O0o8:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private Oo0〇Ooo:Z

.field private Oo80:Ljava/lang/String;

.field private Ooo08:Z

.field private O〇08oOOO0:J

.field private O〇o88o08〇:Ljava/lang/String;

.field private o0OoOOo0:Z

.field private o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

.field private o8oOOo:Lcom/intsig/view/ColorPickerView;

.field private o8〇OO:Z

.field private oO00〇o:I

.field private oOO0880O:I

.field private oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

.field private final oOoo80oO:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;

.field private oO〇8O8oOo:Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;

.field private oo8ooo8O:Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

.field private ooO:Z

.field private ooo0〇〇O:Landroid/widget/TextView;

.field private o〇oO:F

.field private o〇o〇Oo88:I

.field private 〇00O0:I

.field private 〇08〇o0O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;"
        }
    .end annotation
.end field

.field private 〇0O〇O00O:Z

.field private 〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

.field private 〇OO8ooO8〇:Z

.field private 〇OO〇00〇0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O〇〇O8:Landroid/widget/SeekBar;

.field private 〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

.field private 〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

.field private 〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

.field private 〇〇〇0o〇〇0:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignaturePresenter;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignaturePresenter;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$View;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o〇oO:F

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇0O〇O00O:Z

    .line 16
    .line 17
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OoOOo0:Z

    .line 18
    .line 19
    const/high16 v0, -0x1000000

    .line 20
    .line 21
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO0880O:I

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$4;

    .line 24
    .line 25
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$4;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 26
    .line 27
    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOoo80oO:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;

    .line 29
    .line 30
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$5;

    .line 31
    .line 32
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$5;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 33
    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Oo0O0o8:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 36
    .line 37
    const/4 v0, -0x1

    .line 38
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO〇OOo:I

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O00OoO〇()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO0880O:I

    .line 2
    .line 3
    const/high16 v1, -0x1000000

    .line 4
    .line 5
    if-eq v0, v1, :cond_3

    .line 6
    .line 7
    const v1, -0xffff01

    .line 8
    .line 9
    .line 10
    if-eq v0, v1, :cond_2

    .line 11
    .line 12
    const v1, -0xee8c41

    .line 13
    .line 14
    .line 15
    if-eq v0, v1, :cond_2

    .line 16
    .line 17
    const v1, -0xeaeaeb

    .line 18
    .line 19
    .line 20
    if-eq v0, v1, :cond_3

    .line 21
    .line 22
    const v1, -0x3e5f5

    .line 23
    .line 24
    .line 25
    if-eq v0, v1, :cond_1

    .line 26
    .line 27
    const/high16 v1, -0x10000

    .line 28
    .line 29
    if-eq v0, v1, :cond_1

    .line 30
    .line 31
    const/4 v1, -0x1

    .line 32
    if-eq v0, v1, :cond_0

    .line 33
    .line 34
    const-string v0, "others"

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const-string v0, "white"

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const-string v0, "red"

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    const-string v0, "blue"

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_3
    const-string v0, "black"

    .line 47
    .line 48
    :goto_0
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O08〇oO8〇()V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->Oooo8o0〇(Landroid/content/Context;)Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x67

    .line 6
    .line 7
    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O0o0()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO〇OOo:I

    .line 2
    .line 3
    if-ltz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->oo88o8O(I)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o88o88(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O80OO()V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f0d0736

    .line 6
    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 14
    .line 15
    const v1, 0x7f0a0a3d

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Landroid/widget/ImageView;

    .line 23
    .line 24
    const v3, 0x7f0a17d0

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    check-cast v3, Landroid/widget/TextView;

    .line 32
    .line 33
    const v4, 0x7f0a0fed

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    check-cast v4, Landroid/widget/RelativeLayout;

    .line 41
    .line 42
    new-instance v5, Lcom/intsig/camscanner/pdf/signature/〇80〇808〇O;

    .line 43
    .line 44
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/pdf/signature/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    .line 49
    .line 50
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oo08()Z

    .line 51
    .line 52
    .line 53
    move-result v5

    .line 54
    if-nez v5, :cond_0

    .line 55
    .line 56
    const v5, 0x7f080812

    .line 57
    .line 58
    .line 59
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 60
    .line 61
    .line 62
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O00()Z

    .line 63
    .line 64
    .line 65
    move-result v5

    .line 66
    const/4 v6, 0x0

    .line 67
    if-eqz v5, :cond_1

    .line 68
    .line 69
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarMenu(Landroid/view/View;)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {v4, v6, v6, v6, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    const v4, 0x7f080dc9

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    const/16 v4, 0xc

    .line 91
    .line 92
    invoke-static {p0, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 93
    .line 94
    .line 95
    move-result v5

    .line 96
    invoke-static {p0, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 97
    .line 98
    .line 99
    move-result v4

    .line 100
    invoke-virtual {v1, v6, v6, v5, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {v3, v2, v2, v1, v2}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarWrapMenu(Landroid/view/View;)V

    .line 107
    .line 108
    .line 109
    :goto_0
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O88(Z)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇0O8Oo(I)V

    .line 10
    .line 11
    .line 12
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 13
    .line 14
    iget v0, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 15
    .line 16
    mul-int/lit8 v0, v0, 0x2

    .line 17
    .line 18
    add-int/lit8 v0, v0, 0x4

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8oOOo:Lcom/intsig/view/ColorPickerView;

    .line 21
    .line 22
    iget p1, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 23
    .line 24
    invoke-virtual {v1, p1}, Lcom/intsig/view/ColorPickerView;->setCurrentSelect(I)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇O〇〇O8:Landroid/widget/SeekBar;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 34
    .line 35
    const/16 v0, 0x8

    .line 36
    .line 37
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇0O8Oo(I)V

    .line 41
    .line 42
    .line 43
    :goto_0
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O880O〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8O(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO0880O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OO0O(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇〇OOO〇〇(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OO0o(Ljava/util/HashMap;)Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OO(Ljava/util/HashMap;)Lorg/json/JSONObject;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_3

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-lez v1, :cond_3

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-ge v1, v2, :cond_3

    .line 25
    .line 26
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    check-cast v2, Ljava/util/List;

    .line 31
    .line 32
    if-eqz v2, :cond_2

    .line 33
    .line 34
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    const/4 v4, 0x1

    .line 39
    if-le v3, v4, :cond_2

    .line 40
    .line 41
    if-eqz p1, :cond_2

    .line 42
    .line 43
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 44
    .line 45
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    if-le v3, v1, :cond_2

    .line 50
    .line 51
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    if-eqz v3, :cond_2

    .line 60
    .line 61
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    check-cast v3, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;

    .line 66
    .line 67
    instance-of v3, v3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 68
    .line 69
    if-eqz v3, :cond_0

    .line 70
    .line 71
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 72
    .line 73
    iget-wide v4, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇08oOOO0:J

    .line 74
    .line 75
    invoke-static {v3, v4, v5}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O888o0o(Landroid/content/Context;J)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v3

    .line 79
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 80
    .line 81
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    move-result-object v4

    .line 85
    check-cast v4, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 86
    .line 87
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageId()J

    .line 88
    .line 89
    .line 90
    move-result-wide v4

    .line 91
    invoke-static {p0, v4, v5}, Lcom/intsig/camscanner/db/dao/ImageDao;->O0o〇〇Oo(Landroid/content/Context;J)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 96
    .line 97
    .line 98
    move-result v5

    .line 99
    if-nez v5, :cond_0

    .line 100
    .line 101
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 102
    .line 103
    .line 104
    move-result v5

    .line 105
    if-eqz v5, :cond_1

    .line 106
    .line 107
    goto :goto_1

    .line 108
    :cond_1
    :try_start_0
    const-string v0, "doc_id"

    .line 109
    .line 110
    invoke-virtual {p1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 111
    .line 112
    .line 113
    const-string v0, "page_id"

    .line 114
    .line 115
    invoke-virtual {p1, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    .line 117
    .line 118
    :catch_0
    return-object p1

    .line 119
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 120
    .line 121
    goto :goto_0

    .line 122
    :cond_3
    return-object p1
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private OO8〇O8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureEditView;->〇〇808〇(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureEditView;->〇80〇808〇O()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const-string v1, "PdfSignatureActivity"

    .line 14
    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    const-string v0, "add new signature"

    .line 18
    .line 19
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o088O8800()V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const-string v0, "User Operation:  onclick generate signature but reach max number"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇0o8〇()V

    .line 32
    .line 33
    .line 34
    :goto_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OO〇〇o0oO(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o〇oO:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic OoO〇OOo8o(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O08〇oO8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OooO〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇00O()V
    .locals 2

    .line 1
    const v0, 0x7f0a0af3

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/o〇0;

    .line 13
    .line 14
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/signature/o〇0;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureEditView;->setAddClickListener(Landroid/view/View$OnClickListener;)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 21
    .line 22
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/〇〇888;

    .line 23
    .line 24
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/signature/〇〇888;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureEditView;->setSaveClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 31
    .line 32
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/oO80;

    .line 33
    .line 34
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/signature/oO80;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureEditView;->setOnSignatureItemClickListener(Lcom/intsig/camscanner/signature/SignatureAdapter$OnSignatureItemClickListener;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8O〇008()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇8o0o0(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O〇0o8o8〇()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "pdf_signature_image_list"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    check-cast v1, Ljava/util/List;

    .line 12
    .line 13
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 14
    .line 15
    const-string v1, "pdf_signature_doc_id"

    .line 16
    .line 17
    const-wide/16 v2, 0x0

    .line 18
    .line 19
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 20
    .line 21
    .line 22
    move-result-wide v1

    .line 23
    iput-wide v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇08oOOO0:J

    .line 24
    .line 25
    const-string v1, "EXTRA_PDF_MAX_SIZE"

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    iput v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 33
    .line 34
    const-string v1, "log_agent_from"

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Oo80:Ljava/lang/String;

    .line 41
    .line 42
    const-string v1, "log_agent_from_part"

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇o88o08〇:Ljava/lang/String;

    .line 49
    .line 50
    const-string v1, "extra_func_entrance"

    .line 51
    .line 52
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    iput v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o〇o〇Oo88:I

    .line 57
    .line 58
    const-string v1, "log_agent_is_from_pdf_kit"

    .line 59
    .line 60
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8〇OO:Z

    .line 65
    .line 66
    const-string v1, "is_local_doc"

    .line 67
    .line 68
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Ooo08:Z

    .line 73
    .line 74
    const-string v1, "is_from_pdf_kit_share"

    .line 75
    .line 76
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 77
    .line 78
    .line 79
    move-result v1

    .line 80
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OO8ooO8〇:Z

    .line 81
    .line 82
    const-string v1, "pdf_signature_has_signed"

    .line 83
    .line 84
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 89
    .line 90
    invoke-interface {v3, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->Oo08(Z)V

    .line 91
    .line 92
    .line 93
    const-string v1, "is_from_page_list"

    .line 94
    .line 95
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇0O〇O00O:Z

    .line 100
    .line 101
    invoke-static {p0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    shr-int/lit8 v0, v0, 0x1

    .line 106
    .line 107
    sput v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO8:I

    .line 108
    .line 109
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇8oo8888()V

    .line 110
    .line 111
    .line 112
    const v0, 0x7f0a1ae5

    .line 113
    .line 114
    .line 115
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    check-cast v0, Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

    .line 120
    .line 121
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oo8ooo8O:Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

    .line 122
    .line 123
    invoke-static {}, Lcom/intsig/camscanner/signature/scale/SignatureScaleManager;->〇080()Z

    .line 124
    .line 125
    .line 126
    move-result v0

    .line 127
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OoOOo0:Z

    .line 128
    .line 129
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oo8ooo8O:Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

    .line 130
    .line 131
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 132
    .line 133
    .line 134
    const v0, 0x7f0a168f

    .line 135
    .line 136
    .line 137
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    check-cast v0, Landroid/widget/TextView;

    .line 142
    .line 143
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->ooo0〇〇O:Landroid/widget/TextView;

    .line 144
    .line 145
    const v0, 0x7f0a1051

    .line 146
    .line 147
    .line 148
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    check-cast v0, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 153
    .line 154
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 155
    .line 156
    const v0, 0x7f0a0e6f

    .line 157
    .line 158
    .line 159
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 164
    .line 165
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 166
    .line 167
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->setFloatActionViewListener(Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;)V

    .line 168
    .line 169
    .line 170
    const v0, 0x7f0a0c00

    .line 171
    .line 172
    .line 173
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 174
    .line 175
    .line 176
    move-result-object v0

    .line 177
    check-cast v0, Landroid/widget/LinearLayout;

    .line 178
    .line 179
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O0O:Landroid/widget/LinearLayout;

    .line 180
    .line 181
    const v0, 0x7f0a0484

    .line 182
    .line 183
    .line 184
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    check-cast v0, Lcom/intsig/view/ColorPickerView;

    .line 189
    .line 190
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8oOOo:Lcom/intsig/view/ColorPickerView;

    .line 191
    .line 192
    invoke-virtual {v0}, Lcom/intsig/view/ColorPickerView;->O8()V

    .line 193
    .line 194
    .line 195
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8oOOo:Lcom/intsig/view/ColorPickerView;

    .line 196
    .line 197
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOoo80oO:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;

    .line 198
    .line 199
    invoke-virtual {v0, v1}, Lcom/intsig/view/ColorPickerView;->setOnColorSelectedListener(Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;)V

    .line 200
    .line 201
    .line 202
    const v0, 0x7f0a1138

    .line 203
    .line 204
    .line 205
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 206
    .line 207
    .line 208
    move-result-object v0

    .line 209
    check-cast v0, Landroid/widget/SeekBar;

    .line 210
    .line 211
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇O〇〇O8:Landroid/widget/SeekBar;

    .line 212
    .line 213
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Oo0O0o8:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 214
    .line 215
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 216
    .line 217
    .line 218
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O80OO()V

    .line 219
    .line 220
    .line 221
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇00O()V

    .line 222
    .line 223
    .line 224
    return-void
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private O〇0o8〇()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f1308b3

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    const/4 v2, 0x1

    .line 17
    new-array v2, v2, [Ljava/lang/Object;

    .line 18
    .line 19
    const/4 v3, 0x3

    .line 20
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    const/4 v4, 0x0

    .line 25
    aput-object v3, v2, v4

    .line 26
    .line 27
    const v3, 0x7f13021c

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/app/DialogUtils;->o〇〇0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O〇o8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    if-eq v0, v1, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇oo〇()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 21
    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_1
    const-string v0, "confirm_signature"

    .line 29
    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇80〇(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O00()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    const-string v1, "PdfSignatureActivity"

    .line 38
    .line 39
    if-nez v0, :cond_5

    .line 40
    .line 41
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    goto/16 :goto_0

    .line 48
    .line 49
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇080:Lcom/intsig/camscanner/ads/reward/AdRewardedManager;

    .line 50
    .line 51
    sget-object v2, Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;->SIGNATURE:Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;

    .line 52
    .line 53
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇oo〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;)Z

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    const/4 v4, 0x1

    .line 58
    if-eqz v3, :cond_3

    .line 59
    .line 60
    const-string v3, "has free point to save "

    .line 61
    .line 62
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v2, v4}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->Oooo8o0〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;I)V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0O0O〇〇〇0()V

    .line 69
    .line 70
    .line 71
    return-void

    .line 72
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->Oooo8o0〇()I

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-lez v0, :cond_4

    .line 77
    .line 78
    const-string v1, "CSFreeSignature"

    .line 79
    .line 80
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇〇o8O()Lorg/json/JSONObject;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 85
    .line 86
    .line 87
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 88
    .line 89
    invoke-direct {v1, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 90
    .line 91
    .line 92
    const v2, 0x7f131e57

    .line 93
    .line 94
    .line 95
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 96
    .line 97
    .line 98
    move-result-object v1

    .line 99
    new-array v2, v4, [Ljava/lang/Object;

    .line 100
    .line 101
    const/4 v3, 0x0

    .line 102
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 103
    .line 104
    .line 105
    move-result-object v4

    .line 106
    aput-object v4, v2, v3

    .line 107
    .line 108
    const v3, 0x7f13060c

    .line 109
    .line 110
    .line 111
    invoke-virtual {p0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v2

    .line 115
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    new-instance v2, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$8;

    .line 120
    .line 121
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$8;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;I)V

    .line 122
    .line 123
    .line 124
    const v0, 0x7f130116

    .line 125
    .line 126
    .line 127
    invoke-virtual {v1, v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇oo〇(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$7;

    .line 132
    .line 133
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$7;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 134
    .line 135
    .line 136
    const v2, 0x7f130615

    .line 137
    .line 138
    .line 139
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 144
    .line 145
    .line 146
    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 148
    .line 149
    .line 150
    return-void

    .line 151
    :cond_4
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 152
    .line 153
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SAVE_PDF_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 154
    .line 155
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 156
    .line 157
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 158
    .line 159
    .line 160
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 161
    .line 162
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    const/16 v1, 0x64

    .line 167
    .line 168
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 169
    .line 170
    .line 171
    return-void

    .line 172
    :cond_5
    :goto_0
    const-string v0, "vip user or signature is free now "

    .line 173
    .line 174
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0O0O〇〇〇0()V

    .line 178
    .line 179
    .line 180
    return-void
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO8oo0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o088O8800()V
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 7
    .line 8
    const v2, 0x7f131e8c

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const v3, 0x7f08052f

    .line 16
    .line 17
    .line 18
    const/4 v4, 0x0

    .line 19
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 26
    .line 27
    const v2, 0x7f131e8b

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    const v3, 0x7f08052d

    .line 35
    .line 36
    .line 37
    const/4 v4, 0x1

    .line 38
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    new-instance v1, Lcom/intsig/app/AlertBottomDialog;

    .line 45
    .line 46
    const v2, 0x7f140004

    .line 47
    .line 48
    .line 49
    invoke-direct {v1, p0, v2}, Lcom/intsig/app/AlertBottomDialog;-><init>(Landroid/content/Context;I)V

    .line 50
    .line 51
    .line 52
    const v2, 0x7f13021d

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-virtual {v1, v2, v0}, Lcom/intsig/app/AlertBottomDialog;->〇o00〇〇Oo(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 60
    .line 61
    .line 62
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;

    .line 63
    .line 64
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$10;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v1, v0}, Lcom/intsig/app/AlertBottomDialog;->O8(Landroid/content/DialogInterface$OnClickListener;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v1}, Lcom/intsig/app/AlertBottomDialog;->show()V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o0O0O〇〇〇0()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v5

    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 10
    .line 11
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 12
    .line 13
    invoke-interface {v0, v5, v1, v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$IPageProperty;->o〇0(Ljava/util/List;Ljava/util/List;I)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Ljava/util/HashMap;

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 20
    .line 21
    .line 22
    const-string v2, "scheme"

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O00OoO〇()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->ooO:Z

    .line 32
    .line 33
    if-nez v2, :cond_1

    .line 34
    .line 35
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO0o(Ljava/util/HashMap;)Lorg/json/JSONObject;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇0o0oO〇〇0()Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_0

    .line 44
    .line 45
    const-string v2, "sign"

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    const-string v2, "empty"

    .line 49
    .line 50
    :goto_0
    :try_start_0
    const-string v3, "sign_scheme"

    .line 51
    .line 52
    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :catch_0
    move-exception v2

    .line 57
    const-string v3, "PdfSignatureActivity"

    .line 58
    .line 59
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 60
    .line 61
    .line 62
    :goto_1
    const-string v2, "CSAddSignature"

    .line 63
    .line 64
    const-string v3, "save"

    .line 65
    .line 66
    invoke-static {v2, v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 67
    .line 68
    .line 69
    :cond_1
    iget-wide v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇08oOOO0:J

    .line 70
    .line 71
    iget-boolean v4, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8〇OO:Z

    .line 72
    .line 73
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 74
    .line 75
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 76
    .line 77
    if-lez v0, :cond_2

    .line 78
    .line 79
    const/4 v7, 0x1

    .line 80
    goto :goto_2

    .line 81
    :cond_2
    const/4 v0, 0x0

    .line 82
    const/4 v7, 0x0

    .line 83
    :goto_2
    move-object v1, p0

    .line 84
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$View;JZLjava/util/List;Ljava/util/List;Z)V

    .line 85
    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic o0Oo(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oO〇O0O(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o0〇〇00〇o(Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 8
    .param p1    # Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getSize()Lcom/intsig/camscanner/util/ParcelSize;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 6
    .line 7
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO〇OOo:I

    .line 8
    .line 9
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 14
    .line 15
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 16
    .line 17
    if-lez v2, :cond_0

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 20
    .line 21
    invoke-interface {v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    mul-int v2, v2, v1

    .line 30
    .line 31
    iget v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 32
    .line 33
    div-int/2addr v2, v1

    .line 34
    goto :goto_0

    .line 35
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 36
    .line 37
    invoke-interface {v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    mul-int v2, v2, v3

    .line 46
    .line 47
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    div-int/2addr v2, v1

    .line 52
    :goto_0
    iget-object v1, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    .line 53
    .line 54
    invoke-static {v1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    const/16 v3, 0x32

    .line 59
    .line 60
    invoke-static {p0, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 65
    .line 66
    .line 67
    move-result v4

    .line 68
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 69
    .line 70
    .line 71
    move-result v5

    .line 72
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    .line 73
    .line 74
    .line 75
    move-result v4

    .line 76
    int-to-float v5, v3

    .line 77
    const/high16 v6, 0x3f800000    # 1.0f

    .line 78
    .line 79
    mul-float v5, v5, v6

    .line 80
    .line 81
    int-to-float v4, v4

    .line 82
    div-float/2addr v5, v4

    .line 83
    if-eqz v0, :cond_1

    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/util/ParcelSize;

    .line 87
    .line 88
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 89
    .line 90
    .line 91
    move-result v4

    .line 92
    int-to-float v4, v4

    .line 93
    mul-float v4, v4, v5

    .line 94
    .line 95
    float-to-int v4, v4

    .line 96
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 97
    .line 98
    .line 99
    move-result v6

    .line 100
    int-to-float v6, v6

    .line 101
    mul-float v6, v6, v5

    .line 102
    .line 103
    float-to-int v5, v6

    .line 104
    invoke-direct {v0, v4, v5}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 105
    .line 106
    .line 107
    :goto_1
    invoke-static {}, Lcom/intsig/utils/CommonUtil;->OO0o〇〇〇〇0()Ljava/util/Random;

    .line 108
    .line 109
    .line 110
    move-result-object v4

    .line 111
    mul-int/lit8 v5, v3, 0x2

    .line 112
    .line 113
    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    sub-int/2addr v4, v3

    .line 118
    invoke-static {}, Lcom/intsig/utils/CommonUtil;->OO0o〇〇〇〇0()Ljava/util/Random;

    .line 119
    .line 120
    .line 121
    move-result-object v6

    .line 122
    invoke-virtual {v6, v5}, Ljava/util/Random;->nextInt(I)I

    .line 123
    .line 124
    .line 125
    move-result v5

    .line 126
    sub-int/2addr v5, v3

    .line 127
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 128
    .line 129
    invoke-interface {v3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 130
    .line 131
    .line 132
    move-result v3

    .line 133
    div-int/lit8 v3, v3, 0x2

    .line 134
    .line 135
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 136
    .line 137
    .line 138
    move-result v6

    .line 139
    div-int/lit8 v6, v6, 0x2

    .line 140
    .line 141
    sub-int/2addr v3, v6

    .line 142
    add-int/2addr v3, v4

    .line 143
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 144
    .line 145
    invoke-interface {v6}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 146
    .line 147
    .line 148
    move-result v6

    .line 149
    div-int/lit8 v6, v6, 0x2

    .line 150
    .line 151
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 152
    .line 153
    .line 154
    move-result v7

    .line 155
    div-int/lit8 v7, v7, 0x2

    .line 156
    .line 157
    add-int/2addr v6, v7

    .line 158
    add-int/2addr v6, v4

    .line 159
    div-int/lit8 v2, v2, 0x2

    .line 160
    .line 161
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 162
    .line 163
    .line 164
    move-result v4

    .line 165
    div-int/lit8 v4, v4, 0x2

    .line 166
    .line 167
    sub-int v4, v2, v4

    .line 168
    .line 169
    add-int/2addr v4, v5

    .line 170
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 171
    .line 172
    .line 173
    move-result v7

    .line 174
    div-int/lit8 v7, v7, 0x2

    .line 175
    .line 176
    add-int/2addr v2, v7

    .line 177
    add-int/2addr v2, v5

    .line 178
    new-instance v5, Landroid/graphics/Rect;

    .line 179
    .line 180
    invoke-direct {v5, v3, v4, v6, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 181
    .line 182
    .line 183
    iput-object v0, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 184
    .line 185
    invoke-virtual {p1, v5}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setDisplayRect(Landroid/graphics/Rect;)V

    .line 186
    .line 187
    .line 188
    iput-object v1, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->rawSize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 189
    .line 190
    invoke-virtual {p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 191
    .line 192
    .line 193
    move-result p2

    .line 194
    iput p2, p1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 195
    .line 196
    return-void
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o〇88(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o88o88(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO8〇O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o88oo〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signature/SignatureEditView;->〇〇808〇(Z)V

    .line 5
    .line 6
    .line 7
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO8〇0OO8(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic o8O〇008()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/Oo08;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/signature/Oo08;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, v0}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->〇〇o0〇8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o8o0o8()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o88oo〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private oO8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O0O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88(Z)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇oo〇()Z

    .line 22
    .line 23
    .line 24
    return-void

    .line 25
    :cond_0
    const-string v0, "discard_signature"

    .line 26
    .line 27
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇80〇(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 31
    .line 32
    if-eqz v0, :cond_3

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->o〇O8〇〇o()Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_1

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8〇OO:Z

    .line 42
    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    const v0, 0x7f130618

    .line 46
    .line 47
    .line 48
    const v1, 0x7f130617

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_2
    const v0, 0x7f13063e

    .line 53
    .line 54
    .line 55
    const v1, 0x7f130614

    .line 56
    .line 57
    .line 58
    :goto_0
    new-instance v2, Lcom/intsig/app/AlertDialog$Builder;

    .line 59
    .line 60
    invoke-direct {v2, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 61
    .line 62
    .line 63
    const v3, 0x7f131e57

    .line 64
    .line 65
    .line 66
    invoke-virtual {v2, v3}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-virtual {v2, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    new-instance v2, Lcom/intsig/camscanner/pdf/signature/〇o00〇〇Oo;

    .line 75
    .line 76
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/signature/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 77
    .line 78
    .line 79
    const v3, 0x7f13057e

    .line 80
    .line 81
    .line 82
    invoke-virtual {v0, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    new-instance v2, Lcom/intsig/camscanner/pdf/signature/〇o〇;

    .line 87
    .line 88
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/signature/〇o〇;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 100
    .line 101
    .line 102
    return-void

    .line 103
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇oOO80o()V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private oOO8oo0()V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->ooO:Z

    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇08oOOO0:J

    .line 10
    .line 11
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    new-instance v1, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    invoke-direct {v1, p0, v0, v2}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 22
    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/type/SharePdf;->〇o8oO(Z)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OO〇00〇0O:Ljava/util/ArrayList;

    .line 29
    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-lez v0, :cond_0

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OO〇00〇0O:Ljava/util/ArrayList;

    .line 39
    .line 40
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->o80ooO(Ljava/util/ArrayList;)V

    .line 41
    .line 42
    .line 43
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    new-instance v2, Lcom/intsig/camscanner/pdf/signature/O8;

    .line 48
    .line 49
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/signature/O8;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/ShareHelper;->O8888(Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 53
    .line 54
    .line 55
    new-instance v2, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$9;

    .line 56
    .line 57
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$9;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/ShareHelper;->OOo88OOo(Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareHelper;->o88O8()V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic oO〇O0O(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "PdfSignatureActivity"

    .line 2
    .line 3
    const-string p2, "User Operation:  onclick cancel"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p1, "cancel_leave_signature"

    .line 9
    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇80〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇08oO80o(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o〇oO:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇OoO0(Lcom/intsig/camscanner/util/ParcelSize;D)Lcom/intsig/camscanner/util/ParcelSize;
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 2
    .line 3
    if-lez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    mul-int v0, v0, v1

    .line 16
    .line 17
    iget v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 18
    .line 19
    div-int/2addr v0, v1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 22
    .line 23
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_0
    int-to-double v1, v0

    .line 28
    mul-double v3, v1, p2

    .line 29
    .line 30
    double-to-int v3, v3

    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    int-to-double v4, v4

    .line 36
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 37
    .line 38
    mul-double v4, v4, v6

    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    int-to-double v6, v6

    .line 45
    div-double/2addr v4, v6

    .line 46
    cmpl-double v6, v4, p2

    .line 47
    .line 48
    if-ltz v6, :cond_1

    .line 49
    .line 50
    int-to-double v0, v3

    .line 51
    div-double/2addr v0, v4

    .line 52
    double-to-int v0, v0

    .line 53
    goto :goto_1

    .line 54
    :cond_1
    mul-double v1, v1, v4

    .line 55
    .line 56
    double-to-int v3, v1

    .line 57
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    const-string v2, "sourceSize.width = "

    .line 63
    .line 64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    const-string v2, "  sourceSize.height = "

    .line 75
    .line 76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 80
    .line 81
    .line 82
    move-result p1

    .line 83
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string p1, "  destWidth = "

    .line 87
    .line 88
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string p1, "  destHeight = "

    .line 95
    .line 96
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    const-string p1, "  ratio = "

    .line 103
    .line 104
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    const-string p2, "PdfSignatureActivity"

    .line 115
    .line 116
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    new-instance p1, Lcom/intsig/camscanner/util/ParcelSize;

    .line 120
    .line 121
    invoke-direct {p1, v0, v3}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 122
    .line 123
    .line 124
    return-object p1
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic o〇o08〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)Landroid/widget/LinearLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O0O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0o0oO〇〇0()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_5

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    if-eqz v2, :cond_1

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-eqz v2, :cond_5

    .line 29
    .line 30
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    check-cast v2, Ljava/util/List;

    .line 35
    .line 36
    if-eqz v2, :cond_2

    .line 37
    .line 38
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    if-eqz v3, :cond_3

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    if-eqz v3, :cond_2

    .line 54
    .line 55
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    check-cast v3, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;

    .line 60
    .line 61
    instance-of v3, v3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 62
    .line 63
    if-eqz v3, :cond_4

    .line 64
    .line 65
    const/4 v0, 0x1

    .line 66
    return v0

    .line 67
    :cond_5
    :goto_1
    return v1
    .line 68
.end method

.method private 〇0o88Oo〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x1

    .line 13
    return v0

    .line 14
    :cond_1
    :goto_0
    const-string v0, "PdfSignatureActivity"

    .line 15
    .line 16
    const-string v1, "checkParamsOk mImageUrls is null or empty"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇80〇(Ljava/lang/String;)V
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v1, Landroid/util/Pair;

    .line 5
    .line 6
    const-string v2, "from"

    .line 7
    .line 8
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Oo80:Ljava/lang/String;

    .line 9
    .line 10
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    aput-object v1, v0, v2

    .line 15
    .line 16
    new-instance v1, Landroid/util/Pair;

    .line 17
    .line 18
    const-string v2, "from_part"

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇o88o08〇:Ljava/lang/String;

    .line 21
    .line 22
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    const/4 v2, 0x1

    .line 26
    aput-object v1, v0, v2

    .line 27
    .line 28
    const-string v1, "CSAddSignature"

    .line 29
    .line 30
    invoke-static {v1, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic 〇8o0o0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "PdfSignatureActivity"

    .line 2
    .line 3
    const-string p2, "User Operation:  onclick not save"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p1, "confirm_leave"

    .line 9
    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇80〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇oOO80o()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8oo8888()V
    .locals 4

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "type"

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    const-string v2, "premium"

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const-string v2, "basic"

    .line 18
    .line 19
    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 20
    .line 21
    .line 22
    const-string v1, "login_status"

    .line 23
    .line 24
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-eqz v2, :cond_1

    .line 29
    .line 30
    const-string v2, "logged_in"

    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_1
    const-string v2, "no_logged_in"

    .line 34
    .line 35
    :goto_1
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 36
    .line 37
    .line 38
    const-string v1, "cs_pdfpreview"

    .line 39
    .line 40
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o〇o〇Oo88:I

    .line 41
    .line 42
    sget-object v3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_HOME_TAB:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 43
    .line 44
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 45
    .line 46
    .line 47
    move-result v3

    .line 48
    if-ne v2, v3, :cond_2

    .line 49
    .line 50
    const-string v1, "cs_home_tab"

    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_2
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o〇o〇Oo88:I

    .line 54
    .line 55
    sget-object v3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_CS_SCAN:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 56
    .line 57
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-ne v2, v3, :cond_3

    .line 62
    .line 63
    const-string v1, "cs_scan"

    .line 64
    .line 65
    :cond_3
    :goto_2
    const-string v2, "from_part"

    .line 66
    .line 67
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .line 69
    .line 70
    goto :goto_3

    .line 71
    :catch_0
    move-exception v1

    .line 72
    const-string v2, "PdfSignatureActivity"

    .line 73
    .line 74
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    .line 76
    .line 77
    :goto_3
    const-string v1, "CSAddSignature"

    .line 78
    .line 79
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 80
    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic 〇8〇〇8o(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇o8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇OoO0o0(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO〇OOo:I

    .line 2
    .line 3
    if-ltz v0, :cond_8

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O0o0()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    const/4 v2, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const v0, 0x7f1308b3

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    new-array v1, v1, [Ljava/lang/Object;

    .line 29
    .line 30
    const/4 v3, 0x3

    .line 31
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    aput-object v3, v1, v2

    .line 36
    .line 37
    const v2, 0x7f130784

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-static {p0, p1, v0}, Lcom/intsig/camscanner/app/DialogUtils;->o〇〇0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void

    .line 48
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-static {v0}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    const-string v4, "PdfSignatureActivity"

    .line 61
    .line 62
    if-lez v3, :cond_7

    .line 63
    .line 64
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 65
    .line 66
    .line 67
    move-result v3

    .line 68
    if-gtz v3, :cond_1

    .line 69
    .line 70
    goto/16 :goto_1

    .line 71
    .line 72
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 73
    .line 74
    .line 75
    move-result v3

    .line 76
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 81
    .line 82
    invoke-static {v3, v0, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    if-eqz v3, :cond_2

    .line 91
    .line 92
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v3

    .line 96
    invoke-static {v3, v0, v2, v2}, Lcom/intsig/nativelib/DraftEngine;->CleanImageKeepColor(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 97
    .line 98
    .line 99
    move-result v2

    .line 100
    goto :goto_0

    .line 101
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    invoke-static {v3, v0, v2, v2}, Lcom/intsig/nativelib/DraftEngine;->CleanImage(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 106
    .line 107
    .line 108
    move-result v2

    .line 109
    :goto_0
    const/4 v3, -0x1

    .line 110
    if-le v2, v3, :cond_3

    .line 111
    .line 112
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 113
    .line 114
    .line 115
    move-result v5

    .line 116
    if-eqz v5, :cond_3

    .line 117
    .line 118
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 119
    .line 120
    .line 121
    move-result v5

    .line 122
    invoke-static {v2, v0, v5}, Lcom/intsig/nativelib/DraftEngine;->StrokeColor(ILandroid/graphics/Bitmap;I)V

    .line 123
    .line 124
    .line 125
    :cond_3
    if-le v2, v3, :cond_4

    .line 126
    .line 127
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 128
    .line 129
    .line 130
    move-result v5

    .line 131
    if-eqz v5, :cond_4

    .line 132
    .line 133
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 134
    .line 135
    .line 136
    move-result v5

    .line 137
    int-to-float v5, v5

    .line 138
    invoke-static {v2, v0, v5}, Lcom/intsig/nativelib/DraftEngine;->StrokeSize(ILandroid/graphics/Bitmap;F)V

    .line 139
    .line 140
    .line 141
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    .line 142
    .line 143
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .line 145
    .line 146
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v6

    .line 150
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    const-string v6, "AddSignature/"

    .line 154
    .line 155
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v5

    .line 162
    new-instance v6, Ljava/lang/StringBuilder;

    .line 163
    .line 164
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .line 166
    .line 167
    const-string v7, "PdfSignature_"

    .line 168
    .line 169
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v7

    .line 176
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    const-string v7, ".png"

    .line 180
    .line 181
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v6

    .line 188
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 189
    .line 190
    const/16 v8, 0x5a

    .line 191
    .line 192
    invoke-static {v0, v8, v5, v6, v7}, Lcom/intsig/utils/ImageUtil;->〇0000OOO(Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;)Ljava/lang/String;

    .line 193
    .line 194
    .line 195
    move-result-object v5

    .line 196
    invoke-virtual {p1, v5}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setTempSignaturePath(Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    if-le v2, v3, :cond_5

    .line 200
    .line 201
    new-instance v3, Ljava/lang/StringBuilder;

    .line 202
    .line 203
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 204
    .line 205
    .line 206
    const-string v5, "free = "

    .line 207
    .line 208
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    .line 210
    .line 211
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object v3

    .line 218
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    invoke-static {v2}, Lcom/intsig/nativelib/DraftEngine;->FreeContext(I)V

    .line 222
    .line 223
    .line 224
    :cond_5
    if-eqz v0, :cond_6

    .line 225
    .line 226
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 227
    .line 228
    .line 229
    :cond_6
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 230
    .line 231
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v2

    .line 235
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getTempSignaturePath()Ljava/lang/String;

    .line 236
    .line 237
    .line 238
    move-result-object v3

    .line 239
    invoke-direct {v0, v2, v3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    .line 241
    .line 242
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 243
    .line 244
    .line 245
    move-result v2

    .line 246
    iput v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 247
    .line 248
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0〇〇00〇o(Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 249
    .line 250
    .line 251
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 252
    .line 253
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO〇OOo:I

    .line 254
    .line 255
    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForLayoutPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 256
    .line 257
    .line 258
    move-result-object p1

    .line 259
    if-eqz p1, :cond_8

    .line 260
    .line 261
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 262
    .line 263
    iget v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO〇OOo:I

    .line 264
    .line 265
    invoke-virtual {v2, p1, v3, v0, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇O00(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;ILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Z)V

    .line 266
    .line 267
    .line 268
    goto :goto_2

    .line 269
    :cond_7
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 270
    .line 271
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 272
    .line 273
    .line 274
    const-string v1, "addSignature bitmapSize.getWidth()="

    .line 275
    .line 276
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    .line 278
    .line 279
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 280
    .line 281
    .line 282
    move-result v1

    .line 283
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 284
    .line 285
    .line 286
    const-string v1, " bitmapSize.getHeight()="

    .line 287
    .line 288
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    .line 290
    .line 291
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 292
    .line 293
    .line 294
    move-result v0

    .line 295
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 296
    .line 297
    .line 298
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 299
    .line 300
    .line 301
    move-result-object p1

    .line 302
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    .line 304
    .line 305
    :cond_8
    :goto_2
    return-void
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇8〇〇8o(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇oOO80o()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8〇OO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Ooo08:Z

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string v0, "PdfSignatureActivity"

    .line 10
    .line 11
    const-string v1, "doDelete() delete multi documents"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .line 20
    .line 21
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇08oOOO0:J

    .line 22
    .line 23
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇oo〇(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/ArrayList;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇o(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    new-instance v4, Ljava/util/ArrayList;

    .line 40
    .line 41
    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 42
    .line 43
    .line 44
    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 45
    .line 46
    .line 47
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    invoke-static {v2, v4, v1}, Lcom/intsig/camscanner/app/DBUtil;->o〇0o〇〇(Landroid/content/Context;Ljava/util/List;I)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    const/4 v2, 0x2

    .line 59
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo8(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    invoke-static {v1, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 67
    .line 68
    .line 69
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OO8ooO8〇:Z

    .line 70
    .line 71
    if-nez v0, :cond_1

    .line 72
    .line 73
    new-instance v0, Landroid/content/Intent;

    .line 74
    .line 75
    const-class v1, Lcom/intsig/camscanner/pdf/kit/PdfKitMainActivity;

    .line 76
    .line 77
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 81
    .line 82
    .line 83
    :cond_1
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0O0O〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇ooO〇000(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/booksplitter/view/CustomTextureView;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇o〇88(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OoO0o0(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    new-instance v0, Ljava/io/File;

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->o〇〇0〇()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const-string v2, "5_22_3_append_local_signature"

    .line 25
    .line 26
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    if-eqz p1, :cond_1

    .line 38
    .line 39
    const-string p1, "CSAddSignature"

    .line 40
    .line 41
    const-string v0, "click_default_signature"

    .line 42
    .line 43
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    const/4 v0, 0x1

    .line 48
    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    .line 50
    const/4 v1, 0x0

    .line 51
    aput-object p1, v0, v1

    .line 52
    .line 53
    const-string p1, "%s : file is deleted"

    .line 54
    .line 55
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    const-string v0, "PdfSignatureActivity"

    .line 60
    .line 61
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    :cond_1
    :goto_0
    return-void
    .line 65
    .line 66
    .line 67
.end method

.method private 〇o〇OO80oO()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇0o88Oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 8
    .line 9
    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 11
    .line 12
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8〇o0〇〇8(Ljava/util/List;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8o088〇0()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o0o8()V

    .line 22
    .line 23
    .line 24
    :cond_1
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇〇〇OOO〇〇(Landroid/view/View;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇0o0oO〇〇0()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    const-string p1, "sign"

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const-string p1, "empty"

    .line 11
    .line 12
    :goto_0
    const/4 v0, 0x3

    .line 13
    new-array v0, v0, [Landroid/util/Pair;

    .line 14
    .line 15
    new-instance v1, Landroid/util/Pair;

    .line 16
    .line 17
    const-string v2, "from"

    .line 18
    .line 19
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Oo80:Ljava/lang/String;

    .line 20
    .line 21
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 22
    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    aput-object v1, v0, v2

    .line 26
    .line 27
    new-instance v1, Landroid/util/Pair;

    .line 28
    .line 29
    const-string v2, "from_part"

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇o88o08〇:Ljava/lang/String;

    .line 32
    .line 33
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    aput-object v1, v0, v2

    .line 38
    .line 39
    new-instance v1, Landroid/util/Pair;

    .line 40
    .line 41
    const-string v3, "sign_scheme"

    .line 42
    .line 43
    invoke-direct {v1, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 44
    .line 45
    .line 46
    const/4 p1, 0x2

    .line 47
    aput-object v1, v0, p1

    .line 48
    .line 49
    const-string p1, "CSAddSignature"

    .line 50
    .line 51
    const-string v1, "share"

    .line 52
    .line 53
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 54
    .line 55
    .line 56
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->ooO:Z

    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇o8()V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public O8o08O8O(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FII)Z
    .locals 19
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Point;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    move/from16 v3, p5

    .line 8
    .line 9
    move/from16 v4, p6

    .line 10
    .line 11
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 12
    .line 13
    .line 14
    move-result v5

    .line 15
    const/4 v6, 0x0

    .line 16
    if-lez v5, :cond_b

    .line 17
    .line 18
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 19
    .line 20
    .line 21
    move-result v5

    .line 22
    if-gtz v5, :cond_0

    .line 23
    .line 24
    goto/16 :goto_7

    .line 25
    .line 26
    :cond_0
    iget-object v5, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 27
    .line 28
    check-cast v5, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 29
    .line 30
    invoke-virtual {v5}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 31
    .line 32
    .line 33
    move-result v5

    .line 34
    iget-object v7, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 35
    .line 36
    check-cast v7, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 37
    .line 38
    invoke-virtual {v7}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 39
    .line 40
    .line 41
    move-result v7

    .line 42
    iget v8, v1, Landroid/graphics/Point;->y:I

    .line 43
    .line 44
    iget-boolean v9, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OoOOo0:Z

    .line 45
    .line 46
    const/4 v10, 0x1

    .line 47
    const/4 v11, -0x1

    .line 48
    const/4 v12, 0x2

    .line 49
    if-eqz v9, :cond_3

    .line 50
    .line 51
    :goto_0
    if-lt v7, v5, :cond_6

    .line 52
    .line 53
    iget-object v9, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 54
    .line 55
    invoke-virtual {v9, v7}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object v9

    .line 59
    if-nez v9, :cond_1

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_1
    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    .line 63
    .line 64
    .line 65
    move-result v9

    .line 66
    if-le v8, v9, :cond_2

    .line 67
    .line 68
    sub-int/2addr v8, v9

    .line 69
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 70
    .line 71
    .line 72
    move-result v5

    .line 73
    div-int/2addr v5, v12

    .line 74
    sub-int/2addr v8, v5

    .line 75
    goto :goto_3

    .line 76
    :cond_2
    :goto_1
    add-int/lit8 v7, v7, -0x1

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_3
    iget-object v9, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 80
    .line 81
    invoke-interface {v9}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->O8()I

    .line 82
    .line 83
    .line 84
    move-result v9

    .line 85
    add-int/2addr v8, v9

    .line 86
    :goto_2
    if-lt v7, v5, :cond_6

    .line 87
    .line 88
    iget-object v9, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 89
    .line 90
    invoke-virtual {v9, v7}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 91
    .line 92
    .line 93
    move-result-object v9

    .line 94
    new-array v13, v12, [I

    .line 95
    .line 96
    if-eqz v9, :cond_4

    .line 97
    .line 98
    invoke-virtual {v9, v13}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 99
    .line 100
    .line 101
    :cond_4
    aget v9, v13, v10

    .line 102
    .line 103
    if-le v8, v9, :cond_5

    .line 104
    .line 105
    sub-int/2addr v8, v9

    .line 106
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 107
    .line 108
    .line 109
    move-result v5

    .line 110
    div-int/2addr v5, v12

    .line 111
    sub-int v11, v8, v5

    .line 112
    .line 113
    move v8, v11

    .line 114
    :goto_3
    move v11, v7

    .line 115
    goto :goto_4

    .line 116
    :cond_5
    add-int/lit8 v7, v7, -0x1

    .line 117
    .line 118
    goto :goto_2

    .line 119
    :cond_6
    const/4 v8, -0x1

    .line 120
    :goto_4
    const-string v5, "PdfSignatureActivity"

    .line 121
    .line 122
    if-gez v11, :cond_7

    .line 123
    .line 124
    const-string v1, "onFinishEdit error ! "

    .line 125
    .line 126
    invoke-static {v5, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    invoke-direct {v0, v6}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88(Z)V

    .line 130
    .line 131
    .line 132
    return v6

    .line 133
    :cond_7
    iget-object v7, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 134
    .line 135
    invoke-virtual {v7, v11}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->oo88o8O(I)Z

    .line 136
    .line 137
    .line 138
    move-result v7

    .line 139
    if-eqz v7, :cond_8

    .line 140
    .line 141
    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    const v2, 0x7f1308b3

    .line 146
    .line 147
    .line 148
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v1

    .line 152
    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 153
    .line 154
    .line 155
    move-result-object v2

    .line 156
    new-array v3, v10, [Ljava/lang/Object;

    .line 157
    .line 158
    const/4 v4, 0x3

    .line 159
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 160
    .line 161
    .line 162
    move-result-object v4

    .line 163
    aput-object v4, v3, v6

    .line 164
    .line 165
    const v4, 0x7f130784

    .line 166
    .line 167
    .line 168
    invoke-virtual {v2, v4, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v2

    .line 172
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/app/DialogUtils;->o〇〇0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .line 174
    .line 175
    return v6

    .line 176
    :cond_8
    iget v1, v1, Landroid/graphics/Point;->x:I

    .line 177
    .line 178
    iget-object v7, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 179
    .line 180
    invoke-interface {v7}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$IPageProperty;->〇o〇()I

    .line 181
    .line 182
    .line 183
    move-result v7

    .line 184
    sub-int/2addr v1, v7

    .line 185
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 186
    .line 187
    .line 188
    move-result v7

    .line 189
    div-int/2addr v7, v12

    .line 190
    sub-int/2addr v1, v7

    .line 191
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 192
    .line 193
    .line 194
    move-result v7

    .line 195
    add-int/2addr v7, v1

    .line 196
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 197
    .line 198
    .line 199
    move-result v9

    .line 200
    add-int/2addr v9, v8

    .line 201
    iget v13, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 202
    .line 203
    if-lez v13, :cond_9

    .line 204
    .line 205
    iget-object v13, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇08〇o0O:Ljava/util/List;

    .line 206
    .line 207
    invoke-interface {v13, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 208
    .line 209
    .line 210
    move-result-object v13

    .line 211
    check-cast v13, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 212
    .line 213
    iget-object v14, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 214
    .line 215
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇0〇O0088o()Ljava/util/List;

    .line 216
    .line 217
    .line 218
    move-result-object v14

    .line 219
    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 220
    .line 221
    .line 222
    move-result-object v14

    .line 223
    check-cast v14, Ljava/util/List;

    .line 224
    .line 225
    invoke-interface {v14, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 226
    .line 227
    .line 228
    move-result-object v14

    .line 229
    check-cast v14, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 230
    .line 231
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 232
    .line 233
    .line 234
    move-result-object v15

    .line 235
    invoke-virtual {v13}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 236
    .line 237
    .line 238
    move-result v10

    .line 239
    move/from16 p2, v7

    .line 240
    .line 241
    int-to-double v6, v10

    .line 242
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;->〇o〇()Lcom/intsig/camscanner/util/ParcelSize;

    .line 243
    .line 244
    .line 245
    move-result-object v10

    .line 246
    invoke-virtual {v10}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 247
    .line 248
    .line 249
    move-result v10

    .line 250
    move/from16 v16, v11

    .line 251
    .line 252
    int-to-double v10, v10

    .line 253
    div-double/2addr v6, v10

    .line 254
    invoke-virtual {v13}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 255
    .line 256
    .line 257
    move-result v10

    .line 258
    int-to-double v10, v10

    .line 259
    invoke-virtual {v14}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;->〇o〇()Lcom/intsig/camscanner/util/ParcelSize;

    .line 260
    .line 261
    .line 262
    move-result-object v13

    .line 263
    invoke-virtual {v13}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 264
    .line 265
    .line 266
    move-result v13

    .line 267
    int-to-double v13, v13

    .line 268
    div-double/2addr v10, v13

    .line 269
    iget-object v13, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 270
    .line 271
    new-instance v14, Landroid/graphics/Rect;

    .line 272
    .line 273
    iget v12, v15, Landroid/graphics/Rect;->left:I

    .line 274
    .line 275
    move-object/from16 v17, v5

    .line 276
    .line 277
    sub-int v5, v1, v12

    .line 278
    .line 279
    move/from16 v18, v1

    .line 280
    .line 281
    int-to-double v1, v5

    .line 282
    mul-double v1, v1, v6

    .line 283
    .line 284
    double-to-int v1, v1

    .line 285
    iget v2, v15, Landroid/graphics/Rect;->top:I

    .line 286
    .line 287
    sub-int v5, v8, v2

    .line 288
    .line 289
    int-to-double v3, v5

    .line 290
    mul-double v3, v3, v10

    .line 291
    .line 292
    double-to-int v3, v3

    .line 293
    sub-int v4, p2, v12

    .line 294
    .line 295
    int-to-double v4, v4

    .line 296
    mul-double v6, v6, v4

    .line 297
    .line 298
    double-to-int v4, v6

    .line 299
    sub-int v2, v9, v2

    .line 300
    .line 301
    int-to-double v5, v2

    .line 302
    mul-double v10, v10, v5

    .line 303
    .line 304
    double-to-int v2, v10

    .line 305
    invoke-direct {v14, v1, v3, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 306
    .line 307
    .line 308
    iput-object v14, v13, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->canvasRect:Landroid/graphics/Rect;

    .line 309
    .line 310
    goto :goto_5

    .line 311
    :cond_9
    move/from16 v18, v1

    .line 312
    .line 313
    move-object/from16 v17, v5

    .line 314
    .line 315
    move/from16 p2, v7

    .line 316
    .line 317
    move/from16 v16, v11

    .line 318
    .line 319
    :goto_5
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 320
    .line 321
    iget-object v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 322
    .line 323
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getPath()Ljava/lang/String;

    .line 324
    .line 325
    .line 326
    move-result-object v2

    .line 327
    move/from16 v3, p5

    .line 328
    .line 329
    move/from16 v4, p6

    .line 330
    .line 331
    invoke-virtual {v1, v2, v3, v4}, Lcom/intsig/camscanner/signature/SignatureEditView;->〇O〇(Ljava/lang/String;II)V

    .line 332
    .line 333
    .line 334
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 335
    .line 336
    iget-object v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 337
    .line 338
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getPath()Ljava/lang/String;

    .line 339
    .line 340
    .line 341
    move-result-object v2

    .line 342
    move-object/from16 v5, p3

    .line 343
    .line 344
    invoke-virtual {v1, v2, v5}, Lcom/intsig/camscanner/signature/SignatureEditView;->〇O00(Ljava/lang/String;Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 345
    .line 346
    .line 347
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 348
    .line 349
    invoke-virtual {v1}, Lcom/intsig/camscanner/signature/SignatureEditView;->Oooo8o0〇()V

    .line 350
    .line 351
    .line 352
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 353
    .line 354
    move-object/from16 v2, p1

    .line 355
    .line 356
    iput-object v2, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    .line 357
    .line 358
    iput v3, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 359
    .line 360
    iput v4, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 361
    .line 362
    new-instance v2, Landroid/graphics/Rect;

    .line 363
    .line 364
    move/from16 v4, p2

    .line 365
    .line 366
    move/from16 v3, v18

    .line 367
    .line 368
    invoke-direct {v2, v3, v8, v4, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 369
    .line 370
    .line 371
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setDisplayRect(Landroid/graphics/Rect;)V

    .line 372
    .line 373
    .line 374
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 375
    .line 376
    new-instance v2, Landroid/graphics/Point;

    .line 377
    .line 378
    add-int/2addr v3, v4

    .line 379
    const/4 v4, 0x2

    .line 380
    div-int/2addr v3, v4

    .line 381
    add-int/2addr v8, v9

    .line 382
    div-int/2addr v8, v4

    .line 383
    invoke-direct {v2, v3, v8}, Landroid/graphics/Point;-><init>(II)V

    .line 384
    .line 385
    .line 386
    iput-object v2, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->centerPoint:Landroid/graphics/Point;

    .line 387
    .line 388
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 389
    .line 390
    iput-object v5, v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 391
    .line 392
    move/from16 v2, p4

    .line 393
    .line 394
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->setRotation(F)V

    .line 395
    .line 396
    .line 397
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 398
    .line 399
    move/from16 v11, v16

    .line 400
    .line 401
    invoke-virtual {v1, v11}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForLayoutPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 402
    .line 403
    .line 404
    move-result-object v1

    .line 405
    if-eqz v1, :cond_a

    .line 406
    .line 407
    iget-object v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 408
    .line 409
    iget-object v3, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 410
    .line 411
    const/4 v4, 0x0

    .line 412
    invoke-virtual {v2, v1, v11, v3, v4}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇O00(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;ILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Z)V

    .line 413
    .line 414
    .line 415
    goto :goto_6

    .line 416
    :cond_a
    const/4 v4, 0x0

    .line 417
    const-string v1, "onFinishEdit error"

    .line 418
    .line 419
    move-object/from16 v2, v17

    .line 420
    .line 421
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    .line 423
    .line 424
    :goto_6
    invoke-direct {v0, v4}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88(Z)V

    .line 425
    .line 426
    .line 427
    const/4 v1, 0x1

    .line 428
    return v1

    .line 429
    :cond_b
    :goto_7
    const/4 v4, 0x0

    .line 430
    return v4
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public O8〇o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->Oo08()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8〇o0〇〇8(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 4
    .line 5
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;)V

    .line 6
    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 9
    .line 10
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OoOOo0:Z

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇00(Z)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 31
    .line 32
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$3;

    .line 33
    .line 34
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$3;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 41
    .line 42
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->O〇8O8〇008(Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;)V

    .line 43
    .line 44
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    .line 46
    .line 47
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    if-eqz v2, :cond_1

    .line 59
    .line 60
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    check-cast v2, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 65
    .line 66
    new-instance v3, Lcom/intsig/camscanner/util/ParcelSize;

    .line 67
    .line 68
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getImageWidth()I

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getImageHeight()I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    invoke-direct {v3, v4, v5}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    int-to-double v4, v4

    .line 84
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 85
    .line 86
    .line 87
    move-result v6

    .line 88
    int-to-double v6, v6

    .line 89
    div-double/2addr v4, v6

    .line 90
    invoke-direct {p0, v3, v4, v5}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o〇OoO0(Lcom/intsig/camscanner/util/ParcelSize;D)Lcom/intsig/camscanner/util/ParcelSize;

    .line 91
    .line 92
    .line 93
    move-result-object v4

    .line 94
    iget v5, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 95
    .line 96
    if-lez v5, :cond_0

    .line 97
    .line 98
    iget-object v5, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 99
    .line 100
    invoke-interface {v5}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 105
    .line 106
    .line 107
    move-result v6

    .line 108
    mul-int v5, v5, v6

    .line 109
    .line 110
    iget v6, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 111
    .line 112
    div-int/2addr v5, v6

    .line 113
    goto :goto_1

    .line 114
    :cond_0
    iget-object v5, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 115
    .line 116
    invoke-interface {v5}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 117
    .line 118
    .line 119
    move-result v5

    .line 120
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 121
    .line 122
    .line 123
    move-result v6

    .line 124
    mul-int v5, v5, v6

    .line 125
    .line 126
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 127
    .line 128
    .line 129
    move-result v6

    .line 130
    div-int/2addr v5, v6

    .line 131
    :goto_1
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 132
    .line 133
    invoke-interface {v6}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->〇080()I

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 138
    .line 139
    .line 140
    move-result v7

    .line 141
    sub-int/2addr v6, v7

    .line 142
    div-int/lit8 v6, v6, 0x2

    .line 143
    .line 144
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 145
    .line 146
    .line 147
    move-result v7

    .line 148
    sub-int/2addr v5, v7

    .line 149
    div-int/lit8 v5, v5, 0x2

    .line 150
    .line 151
    new-instance v7, Landroid/graphics/Rect;

    .line 152
    .line 153
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 154
    .line 155
    .line 156
    move-result v8

    .line 157
    add-int/2addr v8, v6

    .line 158
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 159
    .line 160
    .line 161
    move-result v9

    .line 162
    add-int/2addr v9, v5

    .line 163
    invoke-direct {v7, v6, v5, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 164
    .line 165
    .line 166
    new-instance v5, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;

    .line 167
    .line 168
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPath()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v2

    .line 172
    invoke-direct {v5, v2, v3, v4, v7}, Lcom/intsig/camscanner/pdf/signature/PdfPageModel;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/util/ParcelSize;Lcom/intsig/camscanner/util/ParcelSize;Landroid/graphics/Rect;)V

    .line 173
    .line 174
    .line 175
    new-instance v2, Ljava/util/ArrayList;

    .line 176
    .line 177
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 178
    .line 179
    .line 180
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    .line 182
    .line 183
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    .line 185
    .line 186
    goto/16 :goto_0

    .line 187
    .line 188
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 189
    .line 190
    iget v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇00O0:I

    .line 191
    .line 192
    const/16 v3, 0x9

    .line 193
    .line 194
    invoke-virtual {v1, v0, p1, v2, v3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->〇O888o0o(Ljava/util/List;Ljava/util/List;II)V

    .line 195
    .line 196
    .line 197
    return-void
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method protected OOo00()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 2
    .line 3
    if-eqz v0, :cond_4

    .line 4
    .line 5
    instance-of v1, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    goto/16 :goto_2

    .line 10
    .line 11
    :cond_0
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 18
    .line 19
    check-cast v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 20
    .line 21
    invoke-virtual {v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    move v2, v1

    .line 26
    :goto_0
    const/4 v3, 0x1

    .line 27
    if-lt v2, v0, :cond_2

    .line 28
    .line 29
    const/4 v4, 0x2

    .line 30
    new-array v4, v4, [I

    .line 31
    .line 32
    iget-object v5, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o0O:Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 33
    .line 34
    invoke-virtual {v5, v2}, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 35
    .line 36
    .line 37
    move-result-object v5

    .line 38
    if-eqz v5, :cond_1

    .line 39
    .line 40
    invoke-virtual {v5, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 41
    .line 42
    .line 43
    aget v4, v4, v3

    .line 44
    .line 45
    sget v5, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO8:I

    .line 46
    .line 47
    if-gt v4, v5, :cond_1

    .line 48
    .line 49
    move v1, v2

    .line 50
    goto :goto_1

    .line 51
    :cond_1
    add-int/lit8 v2, v2, -0x1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_2
    :goto_1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO〇OOo:I

    .line 55
    .line 56
    if-ltz v0, :cond_3

    .line 57
    .line 58
    if-eq v0, v1, :cond_4

    .line 59
    .line 60
    :cond_3
    iput v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->OO〇OOo:I

    .line 61
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    const-string v2, "finalPosition ="

    .line 68
    .line 69
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    const-string v2, "PdfSignatureActivity"

    .line 80
    .line 81
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    add-int/2addr v1, v3

    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    const-string v1, "/"

    .line 94
    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO〇〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;

    .line 99
    .line 100
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;->getItemCount()I

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->ooo0〇〇O:Landroid/widget/TextView;

    .line 112
    .line 113
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->ooo0〇〇O:Landroid/widget/TextView;

    .line 117
    .line 118
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-eqz v0, :cond_4

    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->ooo0〇〇O:Landroid/widget/TextView;

    .line 125
    .line 126
    const/4 v1, 0x0

    .line 127
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 128
    .line 129
    .line 130
    :cond_4
    :goto_2
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public Oo08(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 20
    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    invoke-static {p0, v0}, Lcom/intsig/camscanner/app/AppUtil;->o〇〇0〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 29
    .line 30
    const/4 v1, 0x0

    .line 31
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 32
    .line 33
    .line 34
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 35
    .line 36
    const v1, 0x7f131ec6

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 47
    .line 48
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->o〇0OOo〇0(I)V

    .line 49
    .line 50
    .line 51
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :catch_0
    move-exception p1

    .line 58
    const-string v0, "PdfSignatureActivity"

    .line 59
    .line 60
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    .line 62
    .line 63
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public Oooo8o0〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->o〇0()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public synthetic Oo〇O8o〇8(Lcom/intsig/camscanner/util/ParcelSize;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/〇080;->〇o〇(Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O〇〇o8O()Lorg/json/JSONObject;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OO(Ljava/util/HashMap;)Lorg/json/JSONObject;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    iput-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇〇0o〇〇0:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇0o8o8〇()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇o〇OO80oO()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o0OO(Ljava/util/HashMap;)Lorg/json/JSONObject;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    :try_start_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Ljava/util/Map$Entry;

    .line 27
    .line 28
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    check-cast v2, Ljava/lang/String;

    .line 33
    .line 34
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const-string p1, "type"

    .line 43
    .line 44
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    if-eqz v1, :cond_1

    .line 49
    .line 50
    const-string v1, "premium"

    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_1
    const-string v1, "basic"

    .line 54
    .line 55
    :goto_1
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    .line 57
    .line 58
    const-string p1, "login_status"

    .line 59
    .line 60
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    if-eqz v1, :cond_2

    .line 65
    .line 66
    const-string v1, "logged_in"

    .line 67
    .line 68
    goto :goto_2

    .line 69
    :cond_2
    const-string v1, "no_logged_in"

    .line 70
    .line 71
    :goto_2
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    .line 73
    .line 74
    const-string p1, "from_part"

    .line 75
    .line 76
    const-string v1, "cs_pdf_preview"

    .line 77
    .line 78
    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .line 80
    .line 81
    goto :goto_3

    .line 82
    :catch_0
    move-exception p1

    .line 83
    const-string v1, "PdfSignatureActivity"

    .line 84
    .line 85
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    .line 87
    .line 88
    :goto_3
    return-object v0
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, "requestCode == "

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, "resultCode == "

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string v1, "PdfSignatureActivity"

    .line 30
    .line 31
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/4 v0, -0x1

    .line 35
    if-eq p2, v0, :cond_0

    .line 36
    .line 37
    return-void

    .line 38
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 39
    .line 40
    .line 41
    goto :goto_2

    .line 42
    :pswitch_0
    const-string p2, "extra_path"

    .line 43
    .line 44
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    new-instance p3, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 49
    .line 50
    invoke-direct {p3, p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 54
    .line 55
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/signature/SignatureEditView;->o〇0(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 56
    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O0o0()Z

    .line 59
    .line 60
    .line 61
    move-result p2

    .line 62
    const/4 v0, 0x1

    .line 63
    if-nez p2, :cond_1

    .line 64
    .line 65
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OoO0o0(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_1
    iget-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Oo0〇Ooo:Z

    .line 70
    .line 71
    if-nez p2, :cond_2

    .line 72
    .line 73
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->Oo0〇Ooo:Z

    .line 74
    .line 75
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OoO0o0(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 76
    .line 77
    .line 78
    :cond_2
    :goto_0
    new-instance p2, Ljava/util/HashMap;

    .line 79
    .line 80
    invoke-direct {p2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 81
    .line 82
    .line 83
    const/16 p3, 0x67

    .line 84
    .line 85
    if-ne p1, p3, :cond_3

    .line 86
    .line 87
    const-string p1, "scan"

    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_3
    const-string p1, "gallery"

    .line 91
    .line 92
    :goto_1
    const-string p3, "from"

    .line 93
    .line 94
    invoke-virtual {p2, p3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    .line 96
    .line 97
    const-string p1, "signature_save"

    .line 98
    .line 99
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OO(Ljava/util/HashMap;)Lorg/json/JSONObject;

    .line 100
    .line 101
    .line 102
    move-result-object p2

    .line 103
    const-string p3, "CSAddSignature"

    .line 104
    .line 105
    invoke-static {p3, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 106
    .line 107
    .line 108
    goto :goto_2

    .line 109
    :pswitch_1
    if-eqz p3, :cond_5

    .line 110
    .line 111
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    if-eqz p1, :cond_5

    .line 116
    .line 117
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    const/16 p2, 0x66

    .line 122
    .line 123
    const/4 p3, 0x0

    .line 124
    invoke-static {p0, p1, p3, p3, p2}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->startActivityForResult(Landroid/app/Activity;Landroid/net/Uri;FFI)V

    .line 125
    .line 126
    .line 127
    goto :goto_2

    .line 128
    :pswitch_2
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O00()Z

    .line 129
    .line 130
    .line 131
    move-result p1

    .line 132
    if-nez p1, :cond_4

    .line 133
    .line 134
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 135
    .line 136
    .line 137
    move-result p1

    .line 138
    if-eqz p1, :cond_5

    .line 139
    .line 140
    :cond_4
    const-string p1, "onActivityResult, vip user or signature is free now "

    .line 141
    .line 142
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0O0O〇〇〇0()V

    .line 146
    .line 147
    .line 148
    :cond_5
    :goto_2
    return-void

    .line 149
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oO8()V

    .line 5
    .line 6
    .line 7
    const/4 p1, 0x1

    .line 8
    return p1

    .line 9
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8o:Lcom/intsig/camscanner/signature/SignatureEditView;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureEditView;->Oooo8o0〇()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public synthetic onScaleChanged()V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pdf/signature/〇080;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oo88o8O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->〇〇888()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public ooo0〇〇O(IILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Landroid/graphics/Point;)V
    .locals 10
    .param p3    # Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Point;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "mPdfSignatureActionView page: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string p1, " | topicIndex : "

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const-string p2, "PdfSignatureActivity"

    .line 27
    .line 28
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iput-object p3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O8o〇O0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 32
    .line 33
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OoOOo0:Z

    .line 34
    .line 35
    if-eqz p1, :cond_0

    .line 36
    .line 37
    iget p1, p4, Landroid/graphics/Point;->x:I

    .line 38
    .line 39
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 40
    .line 41
    invoke-interface {p2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$IPageProperty;->〇o〇()I

    .line 42
    .line 43
    .line 44
    move-result p2

    .line 45
    add-int/2addr p1, p2

    .line 46
    iput p1, p4, Landroid/graphics/Point;->x:I

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    iget p1, p4, Landroid/graphics/Point;->y:I

    .line 50
    .line 51
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88O:Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;

    .line 52
    .line 53
    invoke-interface {p2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureContract$Presenter;->O8()I

    .line 54
    .line 55
    .line 56
    move-result p2

    .line 57
    sub-int/2addr p1, p2

    .line 58
    iput p1, p4, Landroid/graphics/Point;->y:I

    .line 59
    .line 60
    :goto_0
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->originalDisplaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 61
    .line 62
    if-nez p1, :cond_1

    .line 63
    .line 64
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 65
    .line 66
    iput-object p1, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->originalDisplaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 67
    .line 68
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇o〇:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 69
    .line 70
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getPath()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    iget-object v2, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->mTempPath:Ljava/lang/String;

    .line 75
    .line 76
    iget v3, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->color:I

    .line 77
    .line 78
    iget v4, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->size:I

    .line 79
    .line 80
    iget-object v6, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 81
    .line 82
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getRotation()F

    .line 83
    .line 84
    .line 85
    move-result v7

    .line 86
    const/4 v8, 0x1

    .line 87
    iget-object v9, p3, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->originalDisplaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 88
    .line 89
    move-object v5, p4

    .line 90
    invoke-virtual/range {v0 .. v9}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FZLcom/intsig/camscanner/util/ParcelSize;)V

    .line 91
    .line 92
    .line 93
    const/4 p1, 0x1

    .line 94
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88(Z)V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public oo〇(FF)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇〇08O:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;->〇〇888()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇0()Landroid/content/Context;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    move-object v0, p0

    .line 13
    :goto_0
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d009f

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇〇0〇88(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OO〇00〇0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->ooO:Z

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oOO8oo0()V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o8〇OO:Z

    .line 12
    .line 13
    if-eqz p1, :cond_2

    .line 14
    .line 15
    const-string p1, "PdfSignatureActivity"

    .line 16
    .line 17
    const-string v0, "batch handle images finish, go to view doc"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    new-instance p1, Landroid/content/Intent;

    .line 23
    .line 24
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 25
    .line 26
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O〇08oOOO0:J

    .line 27
    .line 28
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-class v1, Lcom/intsig/camscanner/DocumentActivity;

    .line 33
    .line 34
    const-string v2, "android.intent.action.VIEW"

    .line 35
    .line 36
    invoke-direct {p1, v2, v0, p0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .line 38
    .line 39
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇0O〇O00O:Z

    .line 40
    .line 41
    if-nez v0, :cond_1

    .line 42
    .line 43
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 44
    .line 45
    .line 46
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_2
    new-instance p1, Landroid/content/Intent;

    .line 51
    .line 52
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 53
    .line 54
    .line 55
    const-string v0, "data_with_pdf_signature"

    .line 56
    .line 57
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇OO〇00〇0O:Ljava/util/ArrayList;

    .line 58
    .line 59
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 60
    .line 61
    .line 62
    const/16 v0, 0xc9

    .line 63
    .line 64
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 68
    .line 69
    .line 70
    :goto_0
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇0000OOO()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇00O0O0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oO00〇o:I

    .line 2
    .line 3
    if-gtz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->O8o08O8O:Landroidx/appcompat/widget/Toolbar;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oO00〇o:I

    .line 12
    .line 13
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oO00〇o:I

    .line 14
    .line 15
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇08〇0〇o〇8(Lcom/intsig/camscanner/signature/ActionType;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oo8ooo8O:Lcom/intsig/camscanner/pic2word/view/ZoomLayout;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->o0OoOOo0:Z

    .line 6
    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    sget-object v1, Lcom/intsig/camscanner/signature/ActionType;->ActionTouch:Lcom/intsig/camscanner/signature/ActionType;

    .line 10
    .line 11
    if-eq v1, p1, :cond_0

    .line 12
    .line 13
    sget-object v1, Lcom/intsig/camscanner/signature/ActionType;->ActionControl:Lcom/intsig/camscanner/signature/ActionType;

    .line 14
    .line 15
    if-eq v1, p1, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 21
    .line 22
    .line 23
    :cond_1
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇0O8Oo(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O0O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O0O:Landroid/widget/LinearLayout;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    const/16 v0, 0x3c

    .line 19
    .line 20
    invoke-static {p0, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    :cond_1
    const/4 v1, 0x0

    .line 25
    if-nez p1, :cond_2

    .line 26
    .line 27
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .line 28
    .line 29
    int-to-float v0, v0

    .line 30
    invoke-direct {v2, v1, v1, v0, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .line 35
    .line 36
    int-to-float v0, v0

    .line 37
    invoke-direct {v2, v1, v1, v1, v0}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 38
    .line 39
    .line 40
    :goto_0
    const-wide/16 v0, 0x12c

    .line 41
    .line 42
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O0O:Landroid/widget/LinearLayout;

    .line 46
    .line 47
    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 48
    .line 49
    .line 50
    new-instance v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$6;

    .line 51
    .line 52
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity$6;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;I)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇8o8O〇O(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const/4 p1, 0x0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->O88(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8o8o〇(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public synthetic 〇8〇oO〇〇8o(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pdf/signature/〇080;->〇080(Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇Oo〇O()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->oO8()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActivity;->〇800OO〇0O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    const-string v1, "PdfSignatureActivity"

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;
.super Ljava/lang/Object;
.source "PdfSignatureAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InnerSignatureClickListener"
.end annotation


# instance fields
.field private OO:Z

.field private o0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private 〇OOo8〇0:Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;->OO:Z

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, [Ljava/lang/Integer;

    .line 6
    .line 7
    if-eqz v1, :cond_4

    .line 8
    .line 9
    check-cast v0, [Ljava/lang/Integer;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    aget-object v2, v0, v1

    .line 13
    .line 14
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    const/4 v3, 0x1

    .line 19
    aget-object v0, v0, v3

    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;->o0:Ljava/util/List;

    .line 26
    .line 27
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    check-cast v4, Ljava/util/List;

    .line 32
    .line 33
    const/4 v5, 0x0

    .line 34
    invoke-interface {v4, v0, v5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v4

    .line 38
    check-cast v4, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;

    .line 39
    .line 40
    instance-of v5, v4, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 41
    .line 42
    if-eqz v5, :cond_3

    .line 43
    .line 44
    iget-object v5, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;

    .line 45
    .line 46
    if-eqz v5, :cond_3

    .line 47
    .line 48
    check-cast v4, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;

    .line 49
    .line 50
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getRotation()F

    .line 51
    .line 52
    .line 53
    move-result v5

    .line 54
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    .line 55
    .line 56
    .line 57
    move-result v5

    .line 58
    const/4 v6, 0x0

    .line 59
    cmpl-float v5, v5, v6

    .line 60
    .line 61
    if-lez v5, :cond_0

    .line 62
    .line 63
    invoke-virtual {p1, v6}, Landroid/view/View;->setRotation(F)V

    .line 64
    .line 65
    .line 66
    :cond_0
    iget-boolean v5, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;->OO:Z

    .line 67
    .line 68
    const/4 v6, 0x2

    .line 69
    if-eqz v5, :cond_1

    .line 70
    .line 71
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 76
    .line 77
    iget-object v3, v4, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 78
    .line 79
    invoke-virtual {v3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    div-int/2addr v3, v6

    .line 84
    add-int/2addr v1, v3

    .line 85
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;->getDisplayRect()Landroid/graphics/Rect;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    iget v3, v3, Landroid/graphics/Rect;->top:I

    .line 90
    .line 91
    iget-object v5, v4, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 92
    .line 93
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 94
    .line 95
    .line 96
    move-result v5

    .line 97
    div-int/2addr v5, v6

    .line 98
    add-int/2addr v3, v5

    .line 99
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 100
    .line 101
    .line 102
    move-result-object v5

    .line 103
    check-cast v5, Landroid/view/ViewGroup;

    .line 104
    .line 105
    if-eqz v5, :cond_2

    .line 106
    .line 107
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    .line 108
    .line 109
    .line 110
    move-result v5

    .line 111
    goto :goto_0

    .line 112
    :cond_1
    new-array v5, v6, [I

    .line 113
    .line 114
    invoke-virtual {p1, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 115
    .line 116
    .line 117
    aget v1, v5, v1

    .line 118
    .line 119
    iget-object v7, v4, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 120
    .line 121
    invoke-virtual {v7}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 122
    .line 123
    .line 124
    move-result v7

    .line 125
    div-int/2addr v7, v6

    .line 126
    add-int/2addr v1, v7

    .line 127
    aget v3, v5, v3

    .line 128
    .line 129
    iget-object v5, v4, Lcom/intsig/camscanner/pdf/signature/PdfSignatureModel;->displaySize:Lcom/intsig/camscanner/util/ParcelSize;

    .line 130
    .line 131
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 132
    .line 133
    .line 134
    move-result v5

    .line 135
    div-int/2addr v5, v6

    .line 136
    :goto_0
    add-int/2addr v3, v5

    .line 137
    :cond_2
    new-instance v5, Landroid/graphics/Point;

    .line 138
    .line 139
    invoke-direct {v5, v1, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 140
    .line 141
    .line 142
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;

    .line 143
    .line 144
    invoke-interface {v1, v2, v0, v4, v5}, Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;->ooo0〇〇O(IILcom/intsig/camscanner/pdf/signature/PdfSignatureModel;Landroid/graphics/Point;)V

    .line 145
    .line 146
    .line 147
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 148
    .line 149
    .line 150
    move-result-object v0

    .line 151
    instance-of v1, v0, Landroid/view/ViewGroup;

    .line 152
    .line 153
    if-eqz v1, :cond_4

    .line 154
    .line 155
    check-cast v0, Landroid/view/ViewGroup;

    .line 156
    .line 157
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 158
    .line 159
    .line 160
    :cond_4
    return-void
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public 〇080(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;->OO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;)V
    .locals 0
    .param p1    # Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/signature/IPdfSignatureAdapter;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇o〇(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/signature/BasePdfImageModel;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureAdapter$InnerSignatureClickListener;->o0:Ljava/util/List;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public final Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mSeekBarChangeListener$1;
.super Ljava/lang/Object;
.source "PdfSignatureNewActivity.kt"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mSeekBarChangeListener$1;->o0:Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "seekBar"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    int-to-float p1, p2

    .line 7
    const/high16 p2, 0x40800000    # 4.0f

    .line 8
    .line 9
    sub-float/2addr p1, p2

    .line 10
    const/4 p2, 0x2

    .line 11
    int-to-float p2, p2

    .line 12
    div-float/2addr p1, p2

    .line 13
    new-instance p2, Ljava/text/DecimalFormat;

    .line 14
    .line 15
    const-string p3, "#.#"

    .line 16
    .line 17
    invoke-direct {p2, p3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    float-to-double v0, p1

    .line 21
    invoke-virtual {p2, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p3

    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mSeekBarChangeListener$1;->o0:Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;

    .line 26
    .line 27
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->oOO8oo0(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)F

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    float-to-double v0, v0

    .line 32
    invoke-virtual {p2, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    move-result p2

    .line 40
    if-eqz p2, :cond_0

    .line 41
    .line 42
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mSeekBarChangeListener$1;->o0:Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;

    .line 43
    .line 44
    invoke-static {p2, p1}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->〇8〇〇8o(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;F)V

    .line 45
    .line 46
    .line 47
    return-void

    .line 48
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity$mSeekBarChangeListener$1;->o0:Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;

    .line 49
    .line 50
    invoke-static {p2}, Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;->o0OO(Lcom/intsig/camscanner/pdf/signature/tab/PdfSignatureNewActivity;)Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    if-eqz p2, :cond_1

    .line 55
    .line 56
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityPdfSignatureNewBinding;->〇080OO8〇0:Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;

    .line 57
    .line 58
    if-eqz p2, :cond_1

    .line 59
    .line 60
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇8O0〇8(F)V

    .line 61
    .line 62
    .line 63
    :cond_1
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1    # Landroid/widget/SeekBar;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "seekBar"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1    # Landroid/widget/SeekBar;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "seekBar"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;
.super Landroid/view/View;
.source "PdfSignatureActionView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView$MyGestureListener;
    }
.end annotation


# instance fields
.field private final O0O:Z

.field private O8o08O8O:F

.field private OO:Landroid/view/GestureDetector;

.field private OO〇00〇8oO:Z

.field private o0:Landroid/content/Context;

.field private o8〇OO0〇0o:Z

.field private oOo0:Z

.field private oOo〇8o008:Lcom/intsig/camscanner/util/ParcelSize;

.field ooo0〇〇O:Landroid/graphics/Point;

.field private o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:Landroid/graphics/RectF;

.field private 〇0O:F

.field private 〇8〇oO〇〇8o:Z

.field private 〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

.field private 〇〇08O:Lcom/intsig/camscanner/signature/ActionType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇08O〇00〇o:Landroid/graphics/RectF;

    const/4 p2, 0x0

    .line 3
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇080OO8〇0:Z

    .line 4
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o8〇OO0〇0o:Z

    .line 5
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇8〇oO〇〇8o:Z

    .line 6
    new-instance p2, Landroid/graphics/Point;

    invoke-direct {p2}, Landroid/graphics/Point;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 7
    sget-object p2, Lcom/intsig/camscanner/signature/ActionType;->ActionNone:Lcom/intsig/camscanner/signature/ActionType;

    iput-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇08O:Lcom/intsig/camscanner/signature/ActionType;

    .line 8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object p2

    invoke-virtual {p2}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    move-result p2

    iput-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O0O:Z

    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o800o8O(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 10
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 11
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇08O〇00〇o:Landroid/graphics/RectF;

    const/4 p2, 0x0

    .line 12
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇080OO8〇0:Z

    .line 13
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o8〇OO0〇0o:Z

    .line 14
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇8〇oO〇〇8o:Z

    .line 15
    new-instance p2, Landroid/graphics/Point;

    invoke-direct {p2}, Landroid/graphics/Point;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 16
    sget-object p2, Lcom/intsig/camscanner/signature/ActionType;->ActionNone:Lcom/intsig/camscanner/signature/ActionType;

    iput-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇08O:Lcom/intsig/camscanner/signature/ActionType;

    .line 17
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object p2

    invoke-virtual {p2}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    move-result p2

    iput-boolean p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O0O:Z

    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o800o8O(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->oOo0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇0〇O0088o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OoO8(FFFF)F
    .locals 0

    .line 1
    sub-float/2addr p1, p3

    .line 2
    mul-float p1, p1, p1

    .line 3
    .line 4
    sub-float/2addr p2, p4

    .line 5
    mul-float p2, p2, p2

    .line 6
    .line 7
    add-float/2addr p1, p2

    .line 8
    float-to-double p1, p1

    .line 9
    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    .line 10
    .line 11
    .line 12
    move-result-wide p1

    .line 13
    double-to-float p1, p1

    .line 14
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;FFFF)F
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->OoO8(FFFF)F

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private o800o8O(Landroid/content/Context;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const v0, 0x7f060669

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 18
    .line 19
    .line 20
    new-instance p1, Landroid/view/GestureDetector;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    new-instance v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView$MyGestureListener;

    .line 27
    .line 28
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView$MyGestureListener;-><init>(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p1, v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 32
    .line 33
    .line 34
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->OO:Landroid/view/GestureDetector;

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)Lcom/intsig/camscanner/util/ParcelSize;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->oOo〇8o008:Lcom/intsig/camscanner/util/ParcelSize;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oo88o8O()V
    .locals 2

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->o〇0(Z)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->onDelete()V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 27
    .line 28
    invoke-interface {v1}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->getPath()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;->〇8o8O〇O(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇O8〇〇o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 6
    .line 7
    invoke-interface {v1}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->getPath()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;->〇8〇oO〇〇8o(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)Lcom/intsig/camscanner/signature/ActionType;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇08O:Lcom/intsig/camscanner/signature/ActionType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0〇O0088o()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O8o08O8O:F

    .line 2
    .line 3
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "FloatActionView"

    .line 10
    .line 11
    const-string v1, "fixNaNRotate"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O8o08O8O:F

    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇0O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇080OO8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O888o0o(Lcom/intsig/camscanner/util/ParcelSize;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->oOo〇8o008:Lcom/intsig/camscanner/util/ParcelSize;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o0:Landroid/content/Context;

    .line 4
    .line 5
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    int-to-float v0, v0

    .line 10
    const/high16 v1, 0x3f800000    # 1.0f

    .line 11
    .line 12
    mul-float v0, v0, v1

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    int-to-float v2, v2

    .line 19
    div-float/2addr v0, v2

    .line 20
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o0:Landroid/content/Context;

    .line 21
    .line 22
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    int-to-float v2, v2

    .line 27
    mul-float v2, v2, v1

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    int-to-float p1, p1

    .line 34
    div-float/2addr v2, p1

    .line 35
    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇0O:F

    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O8o08O8O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)Landroid/graphics/RectF;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇08O〇00〇o:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)Lcom/intsig/camscanner/signature/SignatureViewInterface;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O8o08O8O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇0〇O0088o(Landroid/graphics/Canvas;)V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onMeasure(II)V
    .locals 3

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/high16 v1, -0x80000000

    .line 9
    .line 10
    const/high16 v2, 0x40000000    # 2.0f

    .line 11
    .line 12
    if-eq v0, v2, :cond_1

    .line 13
    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o0:Landroid/content/Context;

    .line 18
    .line 19
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    goto :goto_1

    .line 24
    :cond_1
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    :goto_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eq v0, v2, :cond_3

    .line 33
    .line 34
    if-ne v0, v1, :cond_2

    .line 35
    .line 36
    goto :goto_2

    .line 37
    :cond_2
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o0:Landroid/content/Context;

    .line 38
    .line 39
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    goto :goto_3

    .line 44
    :cond_3
    :goto_2
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 45
    .line 46
    .line 47
    move-result p2

    .line 48
    :goto_3
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇08O〇00〇o:Landroid/graphics/RectF;

    .line 49
    .line 50
    neg-int v1, p1

    .line 51
    int-to-float v1, v1

    .line 52
    const/high16 v2, 0x40000000    # 2.0f

    .line 53
    .line 54
    int-to-float p1, p1

    .line 55
    mul-float p1, p1, v2

    .line 56
    .line 57
    int-to-float p2, p2

    .line 58
    const/4 v2, 0x0

    .line 59
    invoke-virtual {v0, v1, v2, p1, p2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 v0, 0x1

    .line 6
    if-eqz p1, :cond_5

    .line 7
    .line 8
    if-eq p1, v0, :cond_0

    .line 9
    .line 10
    const/4 v1, 0x3

    .line 11
    if-eq p1, v1, :cond_0

    .line 12
    .line 13
    goto/16 :goto_1

    .line 14
    .line 15
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇08O:Lcom/intsig/camscanner/signature/ActionType;

    .line 16
    .line 17
    sget-object v1, Lcom/intsig/camscanner/signature/ActionType;->ActionDelete:Lcom/intsig/camscanner/signature/ActionType;

    .line 18
    .line 19
    if-ne p1, v1, :cond_1

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 22
    .line 23
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    float-to-int v2, v2

    .line 28
    iput v2, p1, Landroid/graphics/Point;->x:I

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 31
    .line 32
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    float-to-int v2, v2

    .line 37
    iput v2, p1, Landroid/graphics/Point;->y:I

    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 42
    .line 43
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇o00〇〇Oo(Landroid/graphics/Point;)Lcom/intsig/camscanner/signature/ActionType;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    if-ne v1, p1, :cond_4

    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->oo88o8O()V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/signature/ActionType;->ActionEdit:Lcom/intsig/camscanner/signature/ActionType;

    .line 54
    .line 55
    if-ne p1, v1, :cond_2

    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 58
    .line 59
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    float-to-int v2, v2

    .line 64
    iput v2, p1, Landroid/graphics/Point;->x:I

    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 67
    .line 68
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    float-to-int v2, v2

    .line 73
    iput v2, p1, Landroid/graphics/Point;->y:I

    .line 74
    .line 75
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 76
    .line 77
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 78
    .line 79
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇o00〇〇Oo(Landroid/graphics/Point;)Lcom/intsig/camscanner/signature/ActionType;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    if-ne v1, p1, :cond_4

    .line 84
    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇O8〇〇o()V

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_2
    sget-object v1, Lcom/intsig/camscanner/signature/ActionType;->ActionNone:Lcom/intsig/camscanner/signature/ActionType;

    .line 90
    .line 91
    if-ne p1, v1, :cond_3

    .line 92
    .line 93
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇080OO8〇0:Z

    .line 94
    .line 95
    if-nez v1, :cond_3

    .line 96
    .line 97
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇oo〇()Z

    .line 98
    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_3
    sget-object v1, Lcom/intsig/camscanner/signature/ActionType;->ActionTouch:Lcom/intsig/camscanner/signature/ActionType;

    .line 102
    .line 103
    if-ne p1, v1, :cond_4

    .line 104
    .line 105
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 106
    .line 107
    if-eqz p1, :cond_4

    .line 108
    .line 109
    invoke-interface {p1}, Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;->oo88o8O()V

    .line 110
    .line 111
    .line 112
    :cond_4
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/signature/ActionType;->ActionNone:Lcom/intsig/camscanner/signature/ActionType;

    .line 113
    .line 114
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇08O:Lcom/intsig/camscanner/signature/ActionType;

    .line 115
    .line 116
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 117
    .line 118
    if-eqz v1, :cond_8

    .line 119
    .line 120
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;->〇08〇0〇o〇8(Lcom/intsig/camscanner/signature/ActionType;)V

    .line 121
    .line 122
    .line 123
    goto :goto_1

    .line 124
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 125
    .line 126
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 127
    .line 128
    .line 129
    move-result v1

    .line 130
    float-to-int v1, v1

    .line 131
    iput v1, p1, Landroid/graphics/Point;->x:I

    .line 132
    .line 133
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 134
    .line 135
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    float-to-int v1, v1

    .line 140
    iput v1, p1, Landroid/graphics/Point;->y:I

    .line 141
    .line 142
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 143
    .line 144
    if-eqz p1, :cond_6

    .line 145
    .line 146
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->ooo0〇〇O:Landroid/graphics/Point;

    .line 147
    .line 148
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇o00〇〇Oo(Landroid/graphics/Point;)Lcom/intsig/camscanner/signature/ActionType;

    .line 149
    .line 150
    .line 151
    move-result-object p1

    .line 152
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇08O:Lcom/intsig/camscanner/signature/ActionType;

    .line 153
    .line 154
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 155
    .line 156
    if-eqz p1, :cond_7

    .line 157
    .line 158
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇08O:Lcom/intsig/camscanner/signature/ActionType;

    .line 159
    .line 160
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;->〇08〇0〇o〇8(Lcom/intsig/camscanner/signature/ActionType;)V

    .line 161
    .line 162
    .line 163
    :cond_7
    const/4 p1, 0x0

    .line 164
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇080OO8〇0:Z

    .line 165
    .line 166
    new-instance p1, Ljava/lang/StringBuilder;

    .line 167
    .line 168
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    .line 170
    .line 171
    const-string v1, "onTouch mCurrentModel ="

    .line 172
    .line 173
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇〇08O:Lcom/intsig/camscanner/signature/ActionType;

    .line 177
    .line 178
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object p1

    .line 185
    const-string v1, "FloatActionView"

    .line 186
    .line 187
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    .line 189
    .line 190
    :cond_8
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->OO:Landroid/view/GestureDetector;

    .line 191
    .line 192
    invoke-virtual {p1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 193
    .line 194
    .line 195
    return v0
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public setFloatActionViewListener(Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setIsPagingSeal(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setShowEditIconOnSign(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O00(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->OO〇00〇8oO:Z

    .line 7
    .line 8
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->oO80(I)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O〇(Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;Lcom/intsig/camscanner/util/ParcelSize;FZZ)V
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Landroid/graphics/Point;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p7    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    move-object v0, p0

    .line 2
    move-object v1, p1

    .line 3
    move/from16 v2, p8

    .line 4
    .line 5
    new-instance v3, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v4, "path = "

    .line 11
    .line 12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    const-string v4, "FloatActionView"

    .line 23
    .line 24
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    new-instance v3, Ljava/io/File;

    .line 28
    .line 29
    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-nez v3, :cond_0

    .line 37
    .line 38
    const-string v1, "path is not exists "

    .line 39
    .line 40
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_0
    move/from16 v3, p9

    .line 45
    .line 46
    iput-boolean v3, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->oOo0:Z

    .line 47
    .line 48
    iput v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O8o08O8O:F

    .line 49
    .line 50
    new-instance v7, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 51
    .line 52
    move v3, p3

    .line 53
    invoke-direct {v7, p1, p3}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;-><init>(Ljava/lang/String;I)V

    .line 54
    .line 55
    .line 56
    move-object v1, p2

    .line 57
    invoke-virtual {v7, p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setTempSignaturePath(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    move v1, p4

    .line 61
    invoke-virtual {v7, p4}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setStrokeSize(I)V

    .line 62
    .line 63
    .line 64
    new-instance v1, Lcom/intsig/camscanner/signature/SignatureView;

    .line 65
    .line 66
    iget-object v4, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o0:Landroid/content/Context;

    .line 67
    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 69
    .line 70
    .line 71
    move-result-wide v5

    .line 72
    const-string v8, "TYPE_SIGNATURE"

    .line 73
    .line 74
    move-object v3, v1

    .line 75
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/signature/SignatureView;-><init>(Landroid/content/Context;JLcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    iput-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 79
    .line 80
    move-object v3, p5

    .line 81
    move-object v4, p6

    .line 82
    invoke-interface {v1, p0, p5, p6, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->o〇O8〇〇o(Landroid/view/View;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;F)V

    .line 83
    .line 84
    .line 85
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 86
    .line 87
    const/4 v2, 0x1

    .line 88
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->o〇0(Z)V

    .line 89
    .line 90
    .line 91
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 92
    .line 93
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->Oooo8o0〇(Z)V

    .line 94
    .line 95
    .line 96
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 97
    .line 98
    const/4 v2, 0x0

    .line 99
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇O00(Z)V

    .line 100
    .line 101
    .line 102
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 103
    .line 104
    iget-boolean v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o8〇OO0〇0o:Z

    .line 105
    .line 106
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇〇808〇(Z)V

    .line 107
    .line 108
    .line 109
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 110
    .line 111
    iget-boolean v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇8〇oO〇〇8o:Z

    .line 112
    .line 113
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇8o8o〇(Z)V

    .line 114
    .line 115
    .line 116
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 117
    .line 118
    move/from16 v2, p10

    .line 119
    .line 120
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇80〇808〇O(Z)V

    .line 121
    .line 122
    .line 123
    move-object/from16 v1, p7

    .line 124
    .line 125
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇O888o0o(Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 126
    .line 127
    .line 128
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 129
    .line 130
    .line 131
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
.end method

.method public 〇oo〇()Z
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    new-instance v5, Landroid/graphics/Point;

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->getCenter()[F

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    aget v0, v0, v2

    .line 21
    .line 22
    float-to-int v0, v0

    .line 23
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 24
    .line 25
    invoke-interface {v3}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->getCenter()[F

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    const/4 v4, 0x1

    .line 30
    aget v3, v3, v4

    .line 31
    .line 32
    float-to-int v3, v3

    .line 33
    invoke-direct {v5, v0, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 34
    .line 35
    .line 36
    new-instance v6, Lcom/intsig/camscanner/util/ParcelSize;

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 39
    .line 40
    invoke-interface {v0}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->OO0o〇〇()[I

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    aget v0, v0, v2

    .line 45
    .line 46
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 47
    .line 48
    invoke-interface {v3}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->OO0o〇〇()[I

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    aget v3, v3, v4

    .line 53
    .line 54
    invoke-direct {v6, v0, v3}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 58
    .line 59
    invoke-interface {v0}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇〇8O0〇8()I

    .line 60
    .line 61
    .line 62
    move-result v8

    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 64
    .line 65
    invoke-interface {v0}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇O8o08O()I

    .line 66
    .line 67
    .line 68
    move-result v9

    .line 69
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->OO〇00〇8oO:Z

    .line 70
    .line 71
    if-eqz v0, :cond_0

    .line 72
    .line 73
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->OO〇00〇8oO:Z

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 76
    .line 77
    invoke-interface {v0}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇o〇()Landroid/graphics/Bitmap;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    .line 82
    .line 83
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .line 85
    .line 86
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v3

    .line 90
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    const-string v3, "AddSignature/"

    .line 94
    .line 95
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    new-instance v3, Ljava/lang/StringBuilder;

    .line 103
    .line 104
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    .line 106
    .line 107
    const-string v4, "PdfSignature_"

    .line 108
    .line 109
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v4

    .line 116
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    const-string v4, ".png"

    .line 120
    .line 121
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v3

    .line 128
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 129
    .line 130
    const/16 v7, 0x5a

    .line 131
    .line 132
    invoke-static {v0, v7, v2, v3, v4}, Lcom/intsig/utils/ImageUtil;->〇0000OOO(Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;)Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object v4

    .line 136
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 137
    .line 138
    iget v7, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O8o08O8O:F

    .line 139
    .line 140
    invoke-interface/range {v3 .. v9}, Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;->O8o08O8O(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FII)Z

    .line 141
    .line 142
    .line 143
    move-result v0

    .line 144
    goto :goto_0

    .line 145
    :cond_0
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o〇00O:Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;

    .line 146
    .line 147
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 148
    .line 149
    invoke-interface {v0}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->O8()Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object v4

    .line 153
    iget v7, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O8o08O8O:F

    .line 154
    .line 155
    invoke-interface/range {v3 .. v9}, Lcom/intsig/camscanner/pdf/signature/IEditPdfSignature;->O8o08O8O(Ljava/lang/String;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FII)Z

    .line 156
    .line 157
    .line 158
    move-result v0

    .line 159
    :goto_0
    move v2, v0

    .line 160
    if-eqz v2, :cond_2

    .line 161
    .line 162
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 163
    .line 164
    invoke-interface {v0}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->onDelete()V

    .line 165
    .line 166
    .line 167
    const/4 v0, 0x0

    .line 168
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 169
    .line 170
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 171
    .line 172
    .line 173
    goto :goto_1

    .line 174
    :cond_1
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 175
    .line 176
    .line 177
    :cond_2
    :goto_1
    return v2
.end method

.method public 〇〇808〇(Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;FZLcom/intsig/camscanner/util/ParcelSize;)V
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Landroid/graphics/Point;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p9    # Lcom/intsig/camscanner/util/ParcelSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    move-object v0, p0

    .line 2
    move-object v1, p1

    .line 3
    move/from16 v2, p7

    .line 4
    .line 5
    new-instance v3, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v4, "path = "

    .line 11
    .line 12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    const-string v4, "FloatActionView"

    .line 23
    .line 24
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    new-instance v3, Ljava/io/File;

    .line 28
    .line 29
    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-nez v3, :cond_0

    .line 37
    .line 38
    const-string v1, "path is not exists "

    .line 39
    .line 40
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_0
    move/from16 v3, p8

    .line 45
    .line 46
    iput-boolean v3, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->oOo0:Z

    .line 47
    .line 48
    iput v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->O8o08O8O:F

    .line 49
    .line 50
    new-instance v7, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 51
    .line 52
    move v3, p3

    .line 53
    invoke-direct {v7, p1, p3}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;-><init>(Ljava/lang/String;I)V

    .line 54
    .line 55
    .line 56
    move-object v1, p2

    .line 57
    invoke-virtual {v7, p2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setTempSignaturePath(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    move v1, p4

    .line 61
    invoke-virtual {v7, p4}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setStrokeSize(I)V

    .line 62
    .line 63
    .line 64
    new-instance v1, Lcom/intsig/camscanner/signature/SignatureView;

    .line 65
    .line 66
    iget-object v4, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o0:Landroid/content/Context;

    .line 67
    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 69
    .line 70
    .line 71
    move-result-wide v5

    .line 72
    const-string v8, "TYPE_SIGNATURE"

    .line 73
    .line 74
    move-object v3, v1

    .line 75
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/signature/SignatureView;-><init>(Landroid/content/Context;JLcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    iput-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 79
    .line 80
    move-object v3, p5

    .line 81
    move-object v4, p6

    .line 82
    invoke-interface {v1, p0, p5, p6, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->o〇O8〇〇o(Landroid/view/View;Landroid/graphics/Point;Lcom/intsig/camscanner/util/ParcelSize;F)V

    .line 83
    .line 84
    .line 85
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 86
    .line 87
    const/4 v2, 0x1

    .line 88
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->o〇0(Z)V

    .line 89
    .line 90
    .line 91
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 92
    .line 93
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->Oooo8o0〇(Z)V

    .line 94
    .line 95
    .line 96
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 97
    .line 98
    const/4 v2, 0x0

    .line 99
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇O00(Z)V

    .line 100
    .line 101
    .line 102
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 103
    .line 104
    iget-boolean v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->o8〇OO0〇0o:Z

    .line 105
    .line 106
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇〇808〇(Z)V

    .line 107
    .line 108
    .line 109
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 110
    .line 111
    iget-boolean v2, v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇8〇oO〇〇8o:Z

    .line 112
    .line 113
    invoke-interface {v1, v2}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->〇8o8o〇(Z)V

    .line 114
    .line 115
    .line 116
    move-object/from16 v1, p9

    .line 117
    .line 118
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇O888o0o(Lcom/intsig/camscanner/util/ParcelSize;)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 122
    .line 123
    .line 124
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
.end method

.method public 〇〇8O0〇8(F)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureViewInterface;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureActionView;->OO〇00〇8oO:Z

    .line 7
    .line 8
    float-to-int p1, p1

    .line 9
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/signature/SignatureViewInterface;->oo88o8O(I)V

    .line 10
    .line 11
    .line 12
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;
.super Ljava/lang/Object;
.source "PdfEditingPresenter.java"

# interfaces
.implements Lcom/intsig/camscanner/util/PdfEncryptionUtil$PdfEncStatusListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$PdfEditingImageEntity;
    }
.end annotation


# instance fields
.field private O0O:Landroid/net/Uri;

.field private O88O:Ljava/lang/String;

.field private O8o08O8O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;"
        }
    .end annotation
.end field

.field private OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

.field private OO〇00〇8oO:Ljava/lang/String;

.field private final Oo0〇Ooo:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

.field private Oo80:I

.field private Ooo08:Z

.field public O〇08oOOO0:Z

.field private O〇o88o08〇:I

.field private final o0:Landroidx/fragment/app/FragmentActivity;

.field o0OoOOo0:Z

.field private o8o:Z

.field private o8oOOo:Z

.field private o8〇OO:Z

.field public o8〇OO0〇0o:Z

.field private oOO〇〇:Z

.field private oOo0:Ljava/lang/String;

.field private oOo〇8o008:I

.field private oO〇8O8oOo:Landroid/graphics/RectF;

.field private oo8ooo8O:I

.field public ooO:I

.field private ooo0〇〇O:Z

.field private o〇00O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$PdfEditingImageEntity;",
            ">;"
        }
    .end annotation
.end field

.field private o〇oO:I

.field private o〇o〇Oo88:Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

.field private 〇00O0:Landroid/os/Bundle;

.field private 〇080OO8〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private 〇08O〇00〇o:Z

.field private 〇08〇o0O:I

.field private 〇0O:J

.field private 〇0O〇O00O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;"
        }
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:Z

.field private 〇OO8ooO8〇:Z

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

.field private 〇OO〇00〇0O:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

.field private 〇O〇〇O8:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

.field private 〇o0O:I

.field private 〇〇08O:Z

.field private 〇〇o〇:Z

.field private 〇〇〇0o〇〇0:J


# direct methods
.method constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;I)V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O8o08O8O:Ljava/util/List;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇08oOOO0:Z

    .line 13
    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO:Z

    .line 15
    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Ooo08:Z

    .line 17
    .line 18
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO8ooO8〇:Z

    .line 19
    .line 20
    sget-object v1, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;->OTHER:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 21
    .line 22
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO〇00〇0O:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$2;

    .line 25
    .line 26
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$2;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 27
    .line 28
    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo0〇Ooo:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 30
    .line 31
    const-wide/16 v1, -0x1

    .line 32
    .line 33
    iput-wide v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇〇0o〇〇0:J

    .line 34
    .line 35
    new-instance v1, Landroid/graphics/RectF;

    .line 36
    .line 37
    const/high16 v2, 0x41200000    # 10.0f

    .line 38
    .line 39
    const/high16 v3, 0x43480000    # 200.0f

    .line 40
    .line 41
    invoke-direct {v1, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 42
    .line 43
    .line 44
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 45
    .line 46
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0OoOOo0:Z

    .line 47
    .line 48
    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇o〇Oo88:Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 50
    .line 51
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 52
    .line 53
    iput-object p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 54
    .line 55
    iput p3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooO:I

    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O00()V
    .locals 2

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string v1, "saveNoWaterStatus"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O80OO()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    add-int/lit8 v0, v0, 0x1

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o0(I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O00O()V
    .locals 5

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string v1, "tryLoadSharePermissionAndCreator"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 9
    .line 10
    const-wide/16 v2, -0x1

    .line 11
    .line 12
    cmp-long v4, v0, v2

    .line 13
    .line 14
    if-nez v4, :cond_0

    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/Oo8Oo00oo;

    .line 22
    .line 23
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/Oo8Oo00oo;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O08000()V
    .locals 5

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string v1, "checkTryDeduction>>>"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    if-eqz v0, :cond_3

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 20
    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇O〇oO()V

    .line 40
    .line 41
    .line 42
    return-void

    .line 43
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_2

    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0o〇〇Oo()Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-eqz v0, :cond_3

    .line 54
    .line 55
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇OO80oO()I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-lez v0, :cond_3

    .line 60
    .line 61
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 62
    .line 63
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 64
    .line 65
    new-instance v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$12;

    .line 66
    .line 67
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$12;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 68
    .line 69
    .line 70
    const/4 v3, 0x0

    .line 71
    const/4 v4, 0x0

    .line 72
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;Z)V

    .line 73
    .line 74
    .line 75
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇8o8o〇()Ljava/util/concurrent/ExecutorService;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    new-array v2, v4, [Ljava/lang/Void;

    .line 80
    .line 81
    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 82
    .line 83
    .line 84
    :cond_3
    :goto_0
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O0OO8〇0()Z
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;->o0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    return v1

    .line 18
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 19
    .line 20
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    new-instance v1, Ljava/util/ArrayList;

    .line 25
    .line 26
    const/4 v2, 0x1

    .line 27
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 28
    .line 29
    .line 30
    iget-wide v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 31
    .line 32
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    new-instance v3, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 40
    .line 41
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 42
    .line 43
    iget-object v5, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 44
    .line 45
    invoke-direct {v3, v4, v1, v5}, Lcom/intsig/camscanner/share/type/ShareToWord;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 46
    .line 47
    .line 48
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇00O〇0o(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 54
    .line 55
    .line 56
    return v2
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O0o〇〇Oo()Z
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->isPayVersion()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "PdfEditingPresenter"

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const-string v0, "FULL VERSION, NOT SHOW, expect premium in svip gray"

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return v2

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 17
    .line 18
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->O0〇oo()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    const-string v0, "HAS ONCE VIP, NOT SHOW"

    .line 25
    .line 26
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return v2

    .line 30
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇OO80oO()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-lez v0, :cond_2

    .line 35
    .line 36
    new-instance v3, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v4, "has no watermark count = "

    .line 42
    .line 43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    return v2

    .line 57
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->Oooo8o0〇()Z

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    if-eqz v0, :cond_3

    .line 62
    .line 63
    return v2

    .line 64
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->us_share_watermark_free:I

    .line 69
    .line 70
    const/4 v1, 0x1

    .line 71
    if-lt v0, v1, :cond_4

    .line 72
    .line 73
    return v2

    .line 74
    :cond_4
    return v1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O0〇OO8(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$PdfEditingImageEntity;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-lez v1, :cond_0

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    invoke-static {v1, p1}, Lcom/intsig/camscanner/db/dao/ImageDao;->o〇8(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-lez v1, :cond_0

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    if-ge v1, v2, :cond_0

    .line 30
    .line 31
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    check-cast v2, Landroid/util/Pair;

    .line 36
    .line 37
    new-instance v3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$PdfEditingImageEntity;

    .line 38
    .line 39
    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 40
    .line 41
    check-cast v4, Ljava/lang/Long;

    .line 42
    .line 43
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    .line 44
    .line 45
    .line 46
    move-result-wide v4

    .line 47
    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 48
    .line 49
    check-cast v2, Ljava/lang/String;

    .line 50
    .line 51
    invoke-direct {v3, v4, v5, v2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$PdfEditingImageEntity;-><init>(JLjava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    add-int/lit8 v1, v1, 0x1

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    return-object v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooo0〇O88O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O880oOO08()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 6
    .line 7
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->〇oo〇(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0OoOOo0:Z

    .line 18
    .line 19
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 20
    .line 21
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->o〇0(JLjava/lang/String;)Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇o〇Oo88:Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 26
    .line 27
    :cond_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O8ooOoo〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Ooo08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O8〇o(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;[I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇O([I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO8oO0o〇()Lcom/intsig/camscanner/util/PdfEncryptionUtil;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇O〇〇O8:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 10
    .line 11
    iget-wide v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 12
    .line 13
    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-direct {v0, v1, v2, p0}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;-><init>(Landroid/app/Activity;Landroid/net/Uri;Lcom/intsig/camscanner/util/PdfEncryptionUtil$PdfEncStatusListener;)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇O〇〇O8:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 21
    .line 22
    const-string v1, "cs_pdf_view"

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->〇O00(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇O〇〇O8:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 28
    .line 29
    const-string v3, "CSPdfPreview"

    .line 30
    .line 31
    const-string v4, "pdf_password"

    .line 32
    .line 33
    const-string v5, "confirm_pdf_password"

    .line 34
    .line 35
    const-string v6, "cancel_pdf_password"

    .line 36
    .line 37
    const-string v7, "reset_pdf_password"

    .line 38
    .line 39
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇O〇〇O8:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 43
    .line 44
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private OOO()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇080()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eq v0, v1, :cond_1

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "share"

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_1
    :goto_0
    const-string v0, "remove_watermark"

    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic OOO〇O0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO8ooO8〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic OOo8o〇O(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p1, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string p2, "User Operation: go to ocr language setting"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    const/4 p2, 0x1

    .line 11
    const/16 v0, 0x67

    .line 12
    .line 13
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/ocrapi/OcrIntent;->O8(Landroid/app/Activity;II)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic Oo(Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;
    .locals 4

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 7
    .line 8
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOO〇〇:Z

    .line 20
    .line 21
    const/4 v3, 0x0

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    move-object v2, v3

    .line 28
    :goto_0
    invoke-direct {v0, v1, p1, v2}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 29
    .line 30
    .line 31
    new-instance p1, Lcom/intsig/camscanner/pdf/preshare/〇00〇8;

    .line 32
    .line 33
    invoke-direct {p1, p2}, Lcom/intsig/camscanner/pdf/preshare/〇00〇8;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/SharePdf;->O〇〇(Lcom/intsig/camscanner/share/type/SharePdf$CreatePdfListener;)V

    .line 37
    .line 38
    .line 39
    return-object v3
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O880oOO08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic Oo8Oo00oo(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0(Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8o:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static Ooo(I)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_VIEW_PDF:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    const-string p0, "cs_list_view"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_AMERICA_MODE_2_PREVIEW:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-ne p0, v0, :cond_1

    .line 19
    .line 20
    const-string p0, "share_view"

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    const-string p0, "share"

    .line 24
    .line 25
    :goto_0
    return-object p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private Ooo8〇〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Landroid/graphics/RectF;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Oo〇O8o〇8(Landroid/net/Uri;)V
    .locals 12

    .line 1
    new-instance v1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v2, "uri = "

    .line 7
    .line 8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    const-string v7, "PdfEditingPresenter"

    .line 19
    .line 20
    invoke-static {v7, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    if-nez p1, :cond_0

    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    const/4 v8, 0x0

    .line 27
    const/4 v9, 0x1

    .line 28
    const-wide/16 v10, -0x1

    .line 29
    .line 30
    :try_start_0
    const-string v1, "page_orientation"

    .line 31
    .line 32
    const-string v2, "page_margin"

    .line 33
    .line 34
    const-string v3, "page_size"

    .line 35
    .line 36
    const-string v4, "PDF_PAGE_NUM_LOCATION"

    .line 37
    .line 38
    filled-new-array {v1, v2, v3, v4}, [Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const/4 v4, 0x0

    .line 51
    const/4 v5, 0x0

    .line 52
    const/4 v6, 0x0

    .line 53
    move-object v2, p1

    .line 54
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    if-eqz v0, :cond_3

    .line 59
    .line 60
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    if-eqz v1, :cond_2

    .line 65
    .line 66
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08〇o0O:I

    .line 71
    .line 72
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    if-ne v1, v9, :cond_1

    .line 77
    .line 78
    const/4 v1, 0x1

    .line 79
    goto :goto_0

    .line 80
    :cond_1
    const/4 v1, 0x0

    .line 81
    :goto_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇o〇:Z

    .line 82
    .line 83
    const/4 v1, 0x2

    .line 84
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    int-to-long v10, v1

    .line 89
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    if-eqz v1, :cond_2

    .line 94
    .line 95
    const/4 v1, 0x3

    .line 96
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo80:I

    .line 101
    .line 102
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .line 104
    .line 105
    goto :goto_1

    .line 106
    :catch_0
    move-exception v0

    .line 107
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 108
    .line 109
    .line 110
    :cond_3
    :goto_1
    const-wide/16 v1, 0x0

    .line 111
    .line 112
    cmp-long v0, v10, v1

    .line 113
    .line 114
    if-gez v0, :cond_4

    .line 115
    .line 116
    return-void

    .line 117
    :cond_4
    :try_start_1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$PdfSize;->〇080:Landroid/net/Uri;

    .line 118
    .line 119
    invoke-static {v0, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    const-string v0, "pdf_width"

    .line 124
    .line 125
    const-string v1, "pdf_height"

    .line 126
    .line 127
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v3

    .line 131
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 136
    .line 137
    .line 138
    move-result-object v1

    .line 139
    const/4 v4, 0x0

    .line 140
    const/4 v5, 0x0

    .line 141
    const/4 v6, 0x0

    .line 142
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    if-eqz v0, :cond_6

    .line 147
    .line 148
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 149
    .line 150
    .line 151
    move-result v1

    .line 152
    if-eqz v1, :cond_5

    .line 153
    .line 154
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    .line 155
    .line 156
    .line 157
    move-result v1

    .line 158
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇oO:I

    .line 159
    .line 160
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oo8ooo8O:I

    .line 165
    .line 166
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 167
    .line 168
    iget v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇oO:I

    .line 169
    .line 170
    int-to-float v3, v3

    .line 171
    int-to-float v1, v1

    .line 172
    invoke-static {v2, v3, v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingUtil;->〇o00〇〇Oo(Landroid/content/Context;FF)Lcom/intsig/camscanner/util/ParcelSize;

    .line 173
    .line 174
    .line 175
    move-result-object v1

    .line 176
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 177
    .line 178
    .line 179
    move-result v2

    .line 180
    iput v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇oO:I

    .line 181
    .line 182
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oo8ooo8O:I

    .line 187
    .line 188
    :cond_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 189
    .line 190
    .line 191
    goto :goto_2

    .line 192
    :catch_1
    move-exception v0

    .line 193
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 194
    .line 195
    .line 196
    :cond_6
    :goto_2
    return-void
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private synthetic Oo〇o(Ljava/lang/String;JLjava/util/ArrayList;)V
    .locals 13

    .line 1
    move-object v0, p0

    .line 2
    new-instance v12, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 3
    .line 4
    iget-object v2, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 5
    .line 6
    const-string v3, "WORD"

    .line 7
    .line 8
    iget-object v4, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo0:Ljava/lang/String;

    .line 9
    .line 10
    iget-object v5, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO〇00〇8oO:Ljava/lang/String;

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO8oO0o〇()Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->〇〇808〇()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v6

    .line 20
    sget-object v10, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_PREVIEW:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 21
    .line 22
    move-object v1, v12

    .line 23
    move-object v7, p1

    .line 24
    move-wide v8, p2

    .line 25
    move-object/from16 v11, p4

    .line 26
    .line 27
    invoke-direct/range {v1 .. v11}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;Ljava/util/ArrayList;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v12}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;->O8()V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O〇0(Ljava/util/List;Lcom/intsig/camscanner/share/type/SharePdf;Ljava/lang/String;I)Ljava/lang/String;
    .locals 6

    .line 1
    if-eqz p1, :cond_6

    .line 2
    .line 3
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto/16 :goto_0

    .line 10
    .line 11
    :cond_0
    if-ltz p4, :cond_6

    .line 12
    .line 13
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-lt p4, v0, :cond_1

    .line 18
    .line 19
    goto/16 :goto_0

    .line 20
    .line 21
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Ooo8〇〇()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    const-string v1, "PdfEditingPresenter"

    .line 26
    .line 27
    if-eqz v0, :cond_3

    .line 28
    .line 29
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPdfEnhanceImage()Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    if-eqz v0, :cond_3

    .line 40
    .line 41
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    check-cast v0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPdfEnhanceImage()Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;->getEnhanceImagePath()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-eqz v0, :cond_3

    .line 60
    .line 61
    const/4 v0, 0x0

    .line 62
    invoke-static {p3, v0}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    const/4 v3, 0x1

    .line 67
    if-eqz v2, :cond_2

    .line 68
    .line 69
    aget v4, v2, v0

    .line 70
    .line 71
    const/16 v5, 0x1194

    .line 72
    .line 73
    if-gt v4, v5, :cond_2

    .line 74
    .line 75
    aget v4, v2, v3

    .line 76
    .line 77
    if-le v4, v5, :cond_3

    .line 78
    .line 79
    :cond_2
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 80
    .line 81
    .line 82
    move-result-object p3

    .line 83
    check-cast p3, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 84
    .line 85
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPath()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p3

    .line 89
    new-instance v4, Ljava/lang/StringBuilder;

    .line 90
    .line 91
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .line 93
    .line 94
    const-string v5, "enhance image too large w="

    .line 95
    .line 96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    aget v0, v2, v0

    .line 100
    .line 101
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    const-string v0, ",h="

    .line 105
    .line 106
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    aget v0, v2, v3

    .line 110
    .line 111
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    :cond_3
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    check-cast v0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 126
    .line 127
    invoke-direct {p0, p2, p3, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇〇0〇〇0(Lcom/intsig/camscanner/share/type/SharePdf;Ljava/lang/String;Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;)Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object p3

    .line 131
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Ooo8〇〇()Z

    .line 132
    .line 133
    .line 134
    move-result v0

    .line 135
    if-eqz v0, :cond_6

    .line 136
    .line 137
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇00O0O0(Ljava/lang/String;)Z

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    if-eqz v0, :cond_4

    .line 142
    .line 143
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 144
    .line 145
    .line 146
    move-result-object v0

    .line 147
    check-cast v0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 148
    .line 149
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPdfEnhanceImage()Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    if-eqz v0, :cond_5

    .line 154
    .line 155
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    check-cast v0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 160
    .line 161
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPdfEnhanceImage()Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;->getEnhanceImagePath()Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v0

    .line 169
    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 170
    .line 171
    .line 172
    move-result v0

    .line 173
    if-eqz v0, :cond_5

    .line 174
    .line 175
    :cond_4
    const-string p3, "enhance image add mark try again"

    .line 176
    .line 177
    invoke-static {v1, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 181
    .line 182
    .line 183
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 184
    .line 185
    .line 186
    move-result-object p3

    .line 187
    check-cast p3, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 188
    .line 189
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPath()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object p3

    .line 193
    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 194
    .line 195
    .line 196
    move-result-object p1

    .line 197
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 198
    .line 199
    invoke-direct {p0, p2, p3, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇〇0〇〇0(Lcom/intsig/camscanner/share/type/SharePdf;Ljava/lang/String;Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;)Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object p1

    .line 203
    move-object p3, p1

    .line 204
    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    .line 205
    .line 206
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    .line 208
    .line 209
    const-string p2, "enhance image add mark path="

    .line 210
    .line 211
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object p1

    .line 221
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    :cond_6
    :goto_0
    return-object p3
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method private O〇08(Landroid/app/Activity;)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance p1, Landroid/content/Intent;

    .line 8
    .line 9
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇00O0:Landroid/os/Bundle;

    .line 22
    .line 23
    const-string v0, "doc_id"

    .line 24
    .line 25
    const-wide/16 v1, -0x1

    .line 26
    .line 27
    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 28
    .line 29
    .line 30
    move-result-wide v0

    .line 31
    iput-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 32
    .line 33
    const-string v0, "extra_pdf_path"

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO〇00〇8oO:Ljava/lang/String;

    .line 40
    .line 41
    const-string v0, "doc_title"

    .line 42
    .line 43
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo0:Ljava/lang/String;

    .line 48
    .line 49
    const-string v0, "log_agent_is_from_pdf_kit"

    .line 50
    .line 51
    const/4 v1, 0x0

    .line 52
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇8〇oO〇〇8o:Z

    .line 57
    .line 58
    const-string v0, "is_local_doc"

    .line 59
    .line 60
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooo0〇〇O:Z

    .line 65
    .line 66
    const-string v0, "is_from_pdf_kit_share"

    .line 67
    .line 68
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇08O:Z

    .line 73
    .line 74
    const-string v0, "extra_is_from_email"

    .line 75
    .line 76
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8o:Z

    .line 81
    .line 82
    const-string v0, "extra_activity_from"

    .line 83
    .line 84
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    check-cast v0, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 89
    .line 90
    if-eqz v0, :cond_1

    .line 91
    .line 92
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO〇00〇0O:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 93
    .line 94
    goto :goto_1

    .line 95
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8o:Z

    .line 96
    .line 97
    if-eqz v0, :cond_2

    .line 98
    .line 99
    sget-object v0, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;->EMAIL:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 100
    .line 101
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO〇00〇0O:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 102
    .line 103
    :cond_2
    :goto_1
    const-string v0, "extra_is_part_doc_pages"

    .line 104
    .line 105
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOO〇〇:Z

    .line 110
    .line 111
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 112
    .line 113
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo0:Ljava/lang/String;

    .line 114
    .line 115
    iget-wide v4, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 116
    .line 117
    xor-int/lit8 v6, v0, 0x1

    .line 118
    .line 119
    iget-object v7, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO〇00〇0O:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 120
    .line 121
    invoke-interface/range {v2 .. v7}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->Oo0oOo〇0(Ljava/lang/String;JZLcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;)V

    .line 122
    .line 123
    .line 124
    const-string v0, "multi_image_id"

    .line 125
    .line 126
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    new-instance v2, Ljava/util/ArrayList;

    .line 131
    .line 132
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .line 134
    .line 135
    iput-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 136
    .line 137
    if-eqz v0, :cond_3

    .line 138
    .line 139
    array-length v2, v0

    .line 140
    :goto_2
    if-ge v1, v2, :cond_3

    .line 141
    .line 142
    aget-wide v3, v0, v1

    .line 143
    .line 144
    iget-object v5, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 145
    .line 146
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 147
    .line 148
    .line 149
    move-result-object v3

    .line 150
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    .line 152
    .line 153
    add-int/lit8 v1, v1, 0x1

    .line 154
    .line 155
    goto :goto_2

    .line 156
    :cond_3
    const-string v0, "extra_from_where"

    .line 157
    .line 158
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v1

    .line 162
    iput-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O88O:Ljava/lang/String;

    .line 163
    .line 164
    sget-object v1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_SHARE:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 165
    .line 166
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 167
    .line 168
    .line 169
    move-result v1

    .line 170
    const-string v2, "extra_func_entrance"

    .line 171
    .line 172
    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 173
    .line 174
    .line 175
    move-result p1

    .line 176
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O:I

    .line 177
    .line 178
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O00O()V

    .line 179
    .line 180
    .line 181
    new-instance p1, Ljava/lang/StringBuilder;

    .line 182
    .line 183
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .line 185
    .line 186
    const-string v1, "parseIntentData    mDocId = "

    .line 187
    .line 188
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 192
    .line 193
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    const-string v1, "\n"

    .line 197
    .line 198
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O:I

    .line 205
    .line 206
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->isFromViewPdf(I)Z

    .line 207
    .line 208
    .line 209
    move-result v0

    .line 210
    if-eqz v0, :cond_4

    .line 211
    .line 212
    const-string v0, "click view pdf"

    .line 213
    .line 214
    goto :goto_3

    .line 215
    :cond_4
    const-string v0, "click share pdf"

    .line 216
    .line 217
    :goto_3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    const-string v0, "\nmPageFrom = "

    .line 221
    .line 222
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    .line 224
    .line 225
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O88O:Ljava/lang/String;

    .line 226
    .line 227
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    .line 229
    .line 230
    const-string v0, "  mFuncEntrance = "

    .line 231
    .line 232
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O:I

    .line 236
    .line 237
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object p1

    .line 244
    const-string v0, "PdfEditingPresenter"

    .line 245
    .line 246
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    return-void
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private O〇0〇o808〇()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇80()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "from_part"

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O88O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v3, "CSPdfPreview"

    .line 10
    .line 11
    const-string v4, "from"

    .line 12
    .line 13
    invoke-static {v3, v4, v0, v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O〇8O8〇008(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇OO()V
    .locals 5

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0O:Landroid/net/Uri;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    const-class v3, Lcom/intsig/camscanner/DocumentActivity;

    .line 8
    .line 9
    const-string v4, "android.intent.action.VIEW"

    .line 10
    .line 11
    invoke-direct {v0, v4, v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O〇O〇oO()V
    .locals 5

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string v1, "consumeAddWatermarksCount>>>"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 9
    .line 10
    if-eqz v0, :cond_2

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_2

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 30
    .line 31
    if-eqz v0, :cond_2

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 43
    .line 44
    new-instance v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$13;

    .line 45
    .line 46
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$13;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 47
    .line 48
    .line 49
    const/4 v3, 0x0

    .line 50
    const/4 v4, 0x0

    .line 51
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;Z)V

    .line 52
    .line 53
    .line 54
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇8o8o〇()Ljava/util/concurrent/ExecutorService;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    new-array v2, v4, [Ljava/lang/Void;

    .line 59
    .line 60
    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 61
    .line 62
    .line 63
    :cond_2
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o0(Ljava/lang/String;Z)V
    .locals 3

    .line 1
    const-string v0, "from"

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    new-instance p2, Landroid/util/Pair;

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O:I

    .line 8
    .line 9
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Ooo(I)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-direct {p2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    new-instance p2, Landroid/util/Pair;

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇80()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-direct {p2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    :goto_0
    const/4 v0, 0x2

    .line 27
    new-array v0, v0, [Landroid/util/Pair;

    .line 28
    .line 29
    const/4 v1, 0x0

    .line 30
    aput-object p2, v0, v1

    .line 31
    .line 32
    new-instance p2, Landroid/util/Pair;

    .line 33
    .line 34
    const-string v1, "from_part"

    .line 35
    .line 36
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O88O:Ljava/lang/String;

    .line 37
    .line 38
    invoke-direct {p2, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    const/4 v1, 0x1

    .line 42
    aput-object p2, v0, v1

    .line 43
    .line 44
    const-string p2, "CSPdfPreview"

    .line 45
    .line 46
    invoke-static {p2, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic o0ooO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo〇O8o〇8(Landroid/net/Uri;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇o88o08〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o8O0()V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const-string v1, "add_signature"

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0(Ljava/lang/String;Z)V

    .line 7
    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    iget-object v1, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    iget-wide v2, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 22
    .line 23
    const-string v4, "ENTRANCE_PDF_PREVIEW"

    .line 24
    .line 25
    invoke-static {v1, v2, v3, v4}, Lcom/intsig/camscanner/newsign/signtype/SelectSignTypeHelper;->〇80〇808〇O(Landroidx/fragment/app/FragmentActivity;JLjava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_0
    iget-object v5, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 30
    .line 31
    iget-wide v6, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 32
    .line 33
    iget-boolean v8, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08O〇00〇o:Z

    .line 34
    .line 35
    iget-object v9, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O8o08O8O:Ljava/util/List;

    .line 36
    .line 37
    iget v10, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇o88o08〇:I

    .line 38
    .line 39
    iget v1, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O:I

    .line 40
    .line 41
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Ooo(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v11

    .line 45
    iget-object v12, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O88O:Ljava/lang/String;

    .line 46
    .line 47
    iget v1, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooO:I

    .line 48
    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 50
    .line 51
    .line 52
    move-result-object v13

    .line 53
    iget-boolean v14, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇8〇oO〇〇8o:Z

    .line 54
    .line 55
    iget-boolean v15, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooo0〇〇O:Z

    .line 56
    .line 57
    iget-boolean v1, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇08O:Z

    .line 58
    .line 59
    move/from16 v16, v1

    .line 60
    .line 61
    invoke-static/range {v5 .. v16}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->〇80〇808〇O(Landroid/app/Activity;JZLjava/util/List;ILjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ZZZ)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o8oO〇([III)Landroid/graphics/Bitmap;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p1, v0

    .line 3
    .line 4
    const/4 v2, 0x1

    .line 5
    aget p1, p1, v2

    .line 6
    .line 7
    int-to-float v1, v1

    .line 8
    const/high16 v2, 0x3f800000    # 1.0f

    .line 9
    .line 10
    mul-float v1, v1, v2

    .line 11
    .line 12
    int-to-float p2, p2

    .line 13
    div-float/2addr v1, p2

    .line 14
    int-to-float p1, p1

    .line 15
    mul-float p1, p1, v2

    .line 16
    .line 17
    int-to-float p3, p3

    .line 18
    div-float/2addr p1, p3

    .line 19
    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    mul-float p2, p2, p1

    .line 24
    .line 25
    float-to-int p2, p2

    .line 26
    mul-float p1, p1, p3

    .line 27
    .line 28
    float-to-int p1, p1

    .line 29
    :cond_0
    invoke-static {p2, p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇〇808〇(II)Landroid/graphics/Bitmap;

    .line 30
    .line 31
    .line 32
    move-result-object p3

    .line 33
    if-nez p3, :cond_1

    .line 34
    .line 35
    int-to-float p2, p2

    .line 36
    const v1, 0x3f4ccccd    # 0.8f

    .line 37
    .line 38
    .line 39
    mul-float p2, p2, v1

    .line 40
    .line 41
    float-to-int p2, p2

    .line 42
    int-to-float p1, p1

    .line 43
    mul-float p1, p1, v1

    .line 44
    .line 45
    float-to-int p1, p1

    .line 46
    add-int/lit8 v0, v0, 0x1

    .line 47
    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    const-string v2, "pageWidth="

    .line 54
    .line 55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    const-string v2, " pageHeight="

    .line 62
    .line 63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v2, " tryTime="

    .line 70
    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    const-string v2, "PdfEditingPresenter"

    .line 82
    .line 83
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    :cond_1
    if-nez p3, :cond_2

    .line 87
    .line 88
    const/4 v1, 0x3

    .line 89
    if-lt v0, v1, :cond_0

    .line 90
    .line 91
    :cond_2
    return-object p3
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private oO()Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "PdfEditingPresenter"

    .line 5
    .line 6
    if-eqz v0, :cond_3

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-wide v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 16
    .line 17
    const-wide/16 v5, 0x0

    .line 18
    .line 19
    cmp-long v0, v3, v5

    .line 20
    .line 21
    if-gtz v0, :cond_1

    .line 22
    .line 23
    const-string v0, "checkParamsOk mDocId <= 0"

    .line 24
    .line 25
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return v1

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 30
    .line 31
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_2

    .line 36
    .line 37
    const-string v0, "mImageIds.size() == 0"

    .line 38
    .line 39
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return v1

    .line 43
    :cond_2
    const/4 v0, 0x1

    .line 44
    return v0

    .line 45
    :cond_3
    :goto_0
    const-string v0, "checkParamsOk mImageUrls is null or empty"

    .line 46
    .line 47
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    return v1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O08000()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oOo(Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0(Ljava/lang/String;Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO〇()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pdf/preshare/PdfCloseWatchAdDialog;->o880()Lcom/intsig/camscanner/pdf/preshare/PdfCloseWatchAdDialog;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$7;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$7;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/preshare/PdfCloseWatchAdDialog;->〇8〇OOoooo(Lcom/intsig/camscanner/pdf/preshare/PdfCloseWatchAdDialog$ActionCallBack;)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const-string v2, "PdfEditingPresenter"

    .line 24
    .line 25
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static synthetic oo(Lkotlin/jvm/functions/Function1;ZLjava/lang/String;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-interface {p0, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    :cond_0
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic oo88o8O(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇o〇:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private ooO〇00O()V
    .locals 4

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string v1, "purchaseForNoBgWatermark"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SECURITY_MARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_COLLAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 26
    .line 27
    const-string v2, "Add_antiTheft_watermark"

    .line 28
    .line 29
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    const/16 v3, 0x65

    .line 34
    .line 35
    invoke-static {v1, v0, v3, v2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;ILcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic ooo0〇O88O()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 4
    .line 5
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O00(Landroid/content/Context;J)J

    .line 6
    .line 7
    .line 8
    move-result-wide v6

    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 12
    .line 13
    iget-wide v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 14
    .line 15
    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-static {v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oo88o8O(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v5

    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-static {v0, v1}, Lcom/intsig/camscanner/db/dao/ImageDao;->o0O0(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 28
    .line 29
    .line 30
    move-result-object v8

    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 32
    .line 33
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/〇o;

    .line 34
    .line 35
    move-object v3, v1

    .line 36
    move-object v4, p0

    .line 37
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/pdf/preshare/〇o;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Ljava/lang/String;JLjava/util/ArrayList;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private ooo〇8oO()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$1;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 4
    .line 5
    .line 6
    const-string v1, "PdfEditingPresenter"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oooo8o0〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic oo〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;JLjava/lang/String;ZII)Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p6}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08O8o〇0(JLjava/lang/String;ZII)Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OOo8o〇O(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic o〇8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇O([I)V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    aget v1, p1, v0

    .line 3
    .line 4
    const/4 v2, 0x6

    .line 5
    aget v2, p1, v2

    .line 6
    .line 7
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x1

    .line 12
    aget v2, p1, v2

    .line 13
    .line 14
    const/4 v3, 0x3

    .line 15
    aget v3, p1, v3

    .line 16
    .line 17
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    const/4 v3, 0x2

    .line 22
    aget v3, p1, v3

    .line 23
    .line 24
    const/4 v4, 0x4

    .line 25
    aget v4, p1, v4

    .line 26
    .line 27
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    const/4 v4, 0x5

    .line 32
    aget v4, p1, v4

    .line 33
    .line 34
    const/4 v5, 0x7

    .line 35
    aget p1, p1, v5

    .line 36
    .line 37
    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    if-gez v1, :cond_0

    .line 42
    .line 43
    const/4 v1, 0x0

    .line 44
    :cond_0
    if-gez v2, :cond_1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    move v0, v2

    .line 48
    :goto_0
    if-ge v1, v3, :cond_6

    .line 49
    .line 50
    if-ge v0, p1, :cond_6

    .line 51
    .line 52
    sub-int v2, v3, v1

    .line 53
    .line 54
    sub-int v4, p1, v0

    .line 55
    .line 56
    const/16 v5, 0x64

    .line 57
    .line 58
    const/16 v6, 0x12c

    .line 59
    .line 60
    if-ge v2, v5, :cond_2

    .line 61
    .line 62
    add-int/lit8 v3, v1, 0x64

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_2
    if-le v2, v6, :cond_3

    .line 66
    .line 67
    add-int/lit16 v3, v1, 0x12c

    .line 68
    .line 69
    :cond_3
    :goto_1
    if-ge v4, v5, :cond_4

    .line 70
    .line 71
    add-int/lit8 p1, v0, 0x64

    .line 72
    .line 73
    goto :goto_2

    .line 74
    :cond_4
    if-le v4, v6, :cond_5

    .line 75
    .line 76
    add-int/lit16 p1, v0, 0x12c

    .line 77
    .line 78
    :cond_5
    :goto_2
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oO〇8O8oOo:Landroid/graphics/RectF;

    .line 79
    .line 80
    int-to-float v1, v1

    .line 81
    int-to-float v0, v0

    .line 82
    int-to-float v3, v3

    .line 83
    int-to-float p1, p1

    .line 84
    invoke-virtual {v2, v1, v0, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 85
    .line 86
    .line 87
    :cond_6
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic o〇O8〇〇o(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇oO:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o〇o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/〇〇〇0〇〇0;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/〇〇〇0〇〇0;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 6
    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->〇〇o0〇8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic o〇〇0〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Ljava/util/List;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O8o08O8O:Ljava/util/List;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇0(Ljava/lang/String;[ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 4

    .line 1
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    const/high16 v1, 0x3f800000    # 1.0f

    .line 7
    .line 8
    mul-float v0, v0, v1

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    aget v3, p2, v2

    .line 12
    .line 13
    int-to-float v3, v3

    .line 14
    div-float/2addr v0, v3

    .line 15
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 16
    .line 17
    .line 18
    move-result p3

    .line 19
    int-to-float p3, p3

    .line 20
    mul-float p3, p3, v1

    .line 21
    .line 22
    const/4 v1, 0x1

    .line 23
    aget v3, p2, v1

    .line 24
    .line 25
    int-to-float v3, v3

    .line 26
    div-float/2addr p3, v3

    .line 27
    invoke-static {v0, p3}, Ljava/lang/Math;->min(FF)F

    .line 28
    .line 29
    .line 30
    move-result p3

    .line 31
    aget v0, p2, v2

    .line 32
    .line 33
    int-to-float v0, v0

    .line 34
    mul-float v0, v0, p3

    .line 35
    .line 36
    float-to-int v0, v0

    .line 37
    aget p2, p2, v1

    .line 38
    .line 39
    int-to-float p2, p2

    .line 40
    mul-float p3, p3, p2

    .line 41
    .line 42
    float-to-int p2, p3

    .line 43
    :try_start_0
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->O8〇o()Landroid/graphics/Bitmap$Config;

    .line 44
    .line 45
    .line 46
    move-result-object p3

    .line 47
    invoke-static {p1, v0, p2, p3, v1}, Lcom/intsig/utils/ImageUtil;->o〇O8〇〇o(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 48
    .line 49
    .line 50
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    goto :goto_0

    .line 52
    :catch_0
    move-exception p1

    .line 53
    const-string p2, "PdfEditingPresenter"

    .line 54
    .line 55
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    .line 57
    .line 58
    const/4 p1, 0x0

    .line 59
    :goto_0
    return-object p1
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇00(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O8o08O8O:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0000OOO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇o88o08〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇000O0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/〇〇〇0〇〇0;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/〇〇〇0〇〇0;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 6
    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->〇〇o0〇8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇00O0O0(Ljava/lang/String;)Z
    .locals 6

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    new-instance v0, Ljava/io/File;

    .line 10
    .line 11
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-eqz p1, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 21
    .line 22
    .line 23
    move-result-wide v2

    .line 24
    const-wide/16 v4, 0x0

    .line 25
    .line 26
    cmp-long p1, v2, v4

    .line 27
    .line 28
    if-lez p1, :cond_1

    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    :cond_1
    return v1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇00〇8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OOO()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Ljava/util/List;Lcom/intsig/camscanner/share/type/SharePdf;Ljava/lang/String;I)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇0(Ljava/util/List;Lcom/intsig/camscanner/share/type/SharePdf;Ljava/lang/String;I)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private 〇08O8o〇0(JLjava/lang/String;ZII)Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;
    .locals 15

    .line 1
    move-object v0, p0

    .line 2
    invoke-static/range {p3 .. p3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz p4, :cond_1

    .line 15
    .line 16
    iget v3, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇o88o08〇:I

    .line 17
    .line 18
    if-ge v3, v2, :cond_0

    .line 19
    .line 20
    iput v2, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇o88o08〇:I

    .line 21
    .line 22
    :cond_0
    move v4, v1

    .line 23
    move v3, v2

    .line 24
    goto :goto_0

    .line 25
    :cond_1
    move/from16 v3, p5

    .line 26
    .line 27
    move/from16 v4, p6

    .line 28
    .line 29
    :goto_0
    invoke-static/range {p3 .. p3}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    rsub-int v5, v5, 0x168

    .line 34
    .line 35
    const/16 v6, 0x5a

    .line 36
    .line 37
    if-eq v5, v6, :cond_3

    .line 38
    .line 39
    const/16 v6, 0x10e

    .line 40
    .line 41
    if-ne v5, v6, :cond_2

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_2
    move v12, v1

    .line 45
    move v11, v2

    .line 46
    goto :goto_2

    .line 47
    :cond_3
    :goto_1
    move v11, v1

    .line 48
    move v12, v2

    .line 49
    :goto_2
    iget v1, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08〇o0O:I

    .line 50
    .line 51
    if-eqz v1, :cond_6

    .line 52
    .line 53
    if-eqz p4, :cond_4

    .line 54
    .line 55
    goto :goto_3

    .line 56
    :cond_4
    const/4 v2, 0x2

    .line 57
    if-ne v1, v2, :cond_5

    .line 58
    .line 59
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 60
    .line 61
    iget v13, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oo8ooo8O:I

    .line 62
    .line 63
    iget v14, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇oO:I

    .line 64
    .line 65
    move-object v7, v1

    .line 66
    move-wide/from16 v8, p1

    .line 67
    .line 68
    move-object/from16 v10, p3

    .line 69
    .line 70
    invoke-direct/range {v7 .. v14}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;-><init>(JLjava/lang/String;IIII)V

    .line 71
    .line 72
    .line 73
    goto :goto_6

    .line 74
    :cond_5
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 75
    .line 76
    iget v13, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇oO:I

    .line 77
    .line 78
    iget v14, v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oo8ooo8O:I

    .line 79
    .line 80
    move-object v7, v1

    .line 81
    move-wide/from16 v8, p1

    .line 82
    .line 83
    move-object/from16 v10, p3

    .line 84
    .line 85
    invoke-direct/range {v7 .. v14}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;-><init>(JLjava/lang/String;IIII)V

    .line 86
    .line 87
    .line 88
    goto :goto_6

    .line 89
    :cond_6
    :goto_3
    if-le v3, v4, :cond_7

    .line 90
    .line 91
    if-le v12, v11, :cond_8

    .line 92
    .line 93
    goto :goto_4

    .line 94
    :cond_7
    if-le v11, v12, :cond_8

    .line 95
    .line 96
    :goto_4
    move v14, v3

    .line 97
    move v13, v4

    .line 98
    goto :goto_5

    .line 99
    :cond_8
    move v13, v3

    .line 100
    move v14, v4

    .line 101
    :goto_5
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 102
    .line 103
    move-object v7, v1

    .line 104
    move-wide/from16 v8, p1

    .line 105
    .line 106
    move-object/from16 v10, p3

    .line 107
    .line 108
    invoke-direct/range {v7 .. v14}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;-><init>(JLjava/lang/String;IIII)V

    .line 109
    .line 110
    .line 111
    :goto_6
    return-object v1
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇80()Ljava/lang/String;
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O:I

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->isFromViewPdf(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "View"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "Share"

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇80〇808〇O(Lkotlin/jvm/functions/Function1;ZLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oo(Lkotlin/jvm/functions/Function1;ZLjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O888o0o(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oo8ooo8O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Landroidx/fragment/app/FragmentActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Landroid/net/Uri;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0O:Landroid/net/Uri;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O〇80o08O()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "from"

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OOO()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇o(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooO〇00O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇000O0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇oOO8O8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇oo〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo80:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Ljava/lang/String;JLjava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo〇o(Ljava/lang/String;JLjava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇〇0o()V
    .locals 4

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string v1, "checkCsQrCodeFlagForOVip"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO8ooO8〇:Z

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 22
    .line 23
    new-instance v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$10;

    .line 24
    .line 25
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$10;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 26
    .line 27
    .line 28
    const/4 v3, 0x0

    .line 29
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    const/4 v2, 0x0

    .line 37
    new-array v2, v2, [Ljava/lang/Void;

    .line 38
    .line 39
    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 40
    .line 41
    .line 42
    :cond_1
    :goto_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo(Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08O〇00〇o:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇〇0〇〇0(Lcom/intsig/camscanner/share/type/SharePdf;Ljava/lang/String;Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;)Ljava/lang/String;
    .locals 11

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v1, v2

    .line 14
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-nez v1, :cond_6

    .line 19
    .line 20
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-lez v1, :cond_6

    .line 25
    .line 26
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-gtz v1, :cond_1

    .line 31
    .line 32
    goto/16 :goto_5

    .line 33
    .line 34
    :cond_1
    const/4 v1, 0x1

    .line 35
    const/4 v3, 0x0

    .line 36
    const/4 v4, 0x2

    .line 37
    :try_start_0
    invoke-static {p2, v3}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    if-nez v5, :cond_2

    .line 42
    .line 43
    const-string p1, "imageBound == null"

    .line 44
    .line 45
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 46
    .line 47
    .line 48
    new-array p1, v4, [Landroid/graphics/Bitmap;

    .line 49
    .line 50
    aput-object v2, p1, v3

    .line 51
    .line 52
    aput-object v2, p1, v1

    .line 53
    .line 54
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    .line 55
    .line 56
    .line 57
    return-object p2

    .line 58
    :cond_2
    :try_start_1
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 59
    .line 60
    .line 61
    move-result v6

    .line 62
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 63
    .line 64
    .line 65
    move-result v7

    .line 66
    invoke-direct {p0, v5, v6, v7}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8oO〇([III)Landroid/graphics/Bitmap;

    .line 67
    .line 68
    .line 69
    move-result-object v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 70
    if-nez v6, :cond_3

    .line 71
    .line 72
    :try_start_2
    const-string p1, "rootBitmap == null"

    .line 73
    .line 74
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75
    .line 76
    .line 77
    new-array p1, v4, [Landroid/graphics/Bitmap;

    .line 78
    .line 79
    aput-object v6, p1, v3

    .line 80
    .line 81
    aput-object v2, p1, v1

    .line 82
    .line 83
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    .line 84
    .line 85
    .line 86
    return-object p2

    .line 87
    :cond_3
    :try_start_3
    new-instance v7, Landroid/graphics/Canvas;

    .line 88
    .line 89
    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 90
    .line 91
    .line 92
    const/4 v8, -0x1

    .line 93
    invoke-virtual {v7, v8}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 94
    .line 95
    .line 96
    invoke-direct {p0, p2, v5, v6}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0(Ljava/lang/String;[ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 97
    .line 98
    .line 99
    move-result-object v5
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 100
    if-nez v5, :cond_4

    .line 101
    .line 102
    :try_start_4
    const-string p1, "imageBitmap == null"

    .line 103
    .line 104
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 105
    .line 106
    .line 107
    new-array p1, v4, [Landroid/graphics/Bitmap;

    .line 108
    .line 109
    aput-object v6, p1, v3

    .line 110
    .line 111
    aput-object v5, p1, v1

    .line 112
    .line 113
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    .line 114
    .line 115
    .line 116
    return-object p2

    .line 117
    :cond_4
    :try_start_5
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    .line 118
    .line 119
    .line 120
    move-result v8

    .line 121
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    .line 122
    .line 123
    .line 124
    move-result v9

    .line 125
    sub-int/2addr v8, v9

    .line 126
    div-int/2addr v8, v4

    .line 127
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    .line 128
    .line 129
    .line 130
    move-result v9

    .line 131
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    .line 132
    .line 133
    .line 134
    move-result v10

    .line 135
    sub-int/2addr v9, v10

    .line 136
    div-int/2addr v9, v4

    .line 137
    int-to-float v8, v8

    .line 138
    int-to-float v9, v9

    .line 139
    invoke-virtual {v7, v5, v8, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 140
    .line 141
    .line 142
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 143
    .line 144
    iget-object v8, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 145
    .line 146
    invoke-interface {v2, v8, p3}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇00O0(Landroid/content/Context;Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 147
    .line 148
    .line 149
    move-result-object p3

    .line 150
    const/high16 v2, 0x3f800000    # 1.0f

    .line 151
    .line 152
    if-eqz p3, :cond_5

    .line 153
    .line 154
    invoke-virtual {p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 155
    .line 156
    .line 157
    move-result v8

    .line 158
    if-lez v8, :cond_5

    .line 159
    .line 160
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    .line 161
    .line 162
    .line 163
    move-result v8

    .line 164
    int-to-float v8, v8

    .line 165
    mul-float v8, v8, v2

    .line 166
    .line 167
    invoke-virtual {p3}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 168
    .line 169
    .line 170
    move-result p3

    .line 171
    int-to-float p3, p3

    .line 172
    div-float v2, v8, p3

    .line 173
    .line 174
    :cond_5
    iget-object p3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 175
    .line 176
    iget-object v8, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 177
    .line 178
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I

    .line 179
    .line 180
    .line 181
    move-result v9

    .line 182
    invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I

    .line 183
    .line 184
    .line 185
    move-result v10

    .line 186
    invoke-static {p3, v8, v9, v10, v2}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->o〇0(Landroid/content/Context;Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;IIF)Lcom/intsig/camscanner/topic/model/DrawWaterMark;

    .line 187
    .line 188
    .line 189
    move-result-object p3

    .line 190
    invoke-virtual {p3, v7}, Lcom/intsig/camscanner/topic/model/DrawWaterMark;->〇o00〇〇Oo(Landroid/graphics/Canvas;)V

    .line 191
    .line 192
    .line 193
    invoke-virtual {v7}, Landroid/graphics/Canvas;->save()I

    .line 194
    .line 195
    .line 196
    invoke-virtual {p1, v6}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->O8O〇88oO0(Landroid/graphics/Bitmap;)Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 200
    new-array p2, v4, [Landroid/graphics/Bitmap;

    .line 201
    .line 202
    aput-object v6, p2, v3

    .line 203
    .line 204
    aput-object v5, p2, v1

    .line 205
    .line 206
    invoke-static {p2}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    .line 207
    .line 208
    .line 209
    return-object p1

    .line 210
    :catchall_0
    move-exception p1

    .line 211
    goto :goto_1

    .line 212
    :catch_0
    move-exception p1

    .line 213
    goto :goto_2

    .line 214
    :catchall_1
    move-exception p1

    .line 215
    move-object v5, v2

    .line 216
    :goto_1
    move-object v2, v6

    .line 217
    goto :goto_4

    .line 218
    :catch_1
    move-exception p1

    .line 219
    move-object v5, v2

    .line 220
    :goto_2
    move-object v2, v6

    .line 221
    goto :goto_3

    .line 222
    :catchall_2
    move-exception p1

    .line 223
    move-object v5, v2

    .line 224
    goto :goto_4

    .line 225
    :catch_2
    move-exception p1

    .line 226
    move-object v5, v2

    .line 227
    :goto_3
    :try_start_6
    new-instance p3, Ljava/lang/StringBuilder;

    .line 228
    .line 229
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 230
    .line 231
    .line 232
    const-string v6, "getBgWatermarkListener   "

    .line 233
    .line 234
    invoke-virtual {p3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object p1

    .line 244
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 245
    .line 246
    .line 247
    new-array p1, v4, [Landroid/graphics/Bitmap;

    .line 248
    .line 249
    aput-object v2, p1, v3

    .line 250
    .line 251
    aput-object v5, p1, v1

    .line 252
    .line 253
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    .line 254
    .line 255
    .line 256
    return-object p2

    .line 257
    :catchall_3
    move-exception p1

    .line 258
    :goto_4
    new-array p2, v4, [Landroid/graphics/Bitmap;

    .line 259
    .line 260
    aput-object v2, p2, v3

    .line 261
    .line 262
    aput-object v5, p2, v1

    .line 263
    .line 264
    invoke-static {p2}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    .line 265
    .line 266
    .line 267
    throw p1

    .line 268
    :cond_6
    :goto_5
    return-object p2
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method


# virtual methods
.method public O0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08O〇00〇o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O000()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0O:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O0O8OO088(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇08oOOO0:Z

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 6
    .line 7
    const-string v1, "gotoPurchase"

    .line 8
    .line 9
    const-string v2, "PdfEditingPresenter"

    .line 10
    .line 11
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const-string v1, "remove_watermark"

    .line 15
    .line 16
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 20
    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-nez v1, :cond_0

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooO〇00O()V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const-string v1, "FROM_FUN_NO_INK"

    .line 38
    .line 39
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    xor-int/2addr v0, v1

    .line 47
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO8ooO8〇:Z

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 50
    .line 51
    const-string v1, "No_Watermark"

    .line 52
    .line 53
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    const/16 v2, 0x64

    .line 58
    .line 59
    invoke-static {v0, p1, v2, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;ILcom/intsig/camscanner/tsapp/purchase/PurchaseExtraData;)V

    .line 60
    .line 61
    .line 62
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O0o(I)V
    .locals 3

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo〇8o008:I

    .line 2
    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "compressFlag = "

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "PdfEditingPresenter"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x2

    .line 26
    if-eq p1, v0, :cond_1

    .line 27
    .line 28
    const/4 v0, 0x3

    .line 29
    if-eq p1, v0, :cond_0

    .line 30
    .line 31
    const-string p1, "original_size"

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const-string p1, "compression_pay"

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const-string p1, "compression_size"

    .line 38
    .line 39
    :goto_0
    const-string v0, "file_compression"

    .line 40
    .line 41
    const-string v1, "type"

    .line 42
    .line 43
    const-string v2, "CSPdfPreview"

    .line 44
    .line 45
    invoke-static {v2, v0, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O0oO008(ZLcom/intsig/camscanner/share/listener/ShareBackListener;)V
    .locals 3

    .line 1
    new-instance p1, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 7
    .line 8
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-direct {v0, v1, p1, v2}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O8o08O8O:Ljava/util/List;

    .line 25
    .line 26
    invoke-virtual {p0, v0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo0oO〇O〇O(Lcom/intsig/camscanner/share/type/SharePdf;Ljava/util/List;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->O8888(Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 31
    .line 32
    .line 33
    new-instance p2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$5;

    .line 34
    .line 35
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$5;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->OOo88OOo(Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;)V

    .line 39
    .line 40
    .line 41
    iget-boolean p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8o:Z

    .line 42
    .line 43
    if-eqz p2, :cond_0

    .line 44
    .line 45
    sget-object p2, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 46
    .line 47
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/ShareHelper;->o08〇〇0O(Lcom/intsig/camscanner/share/ShareHelper$ShareType;)V

    .line 48
    .line 49
    .line 50
    const-string p2, "send_email"

    .line 51
    .line 52
    const/4 v1, 0x1

    .line 53
    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0(Ljava/lang/String;Z)V

    .line 54
    .line 55
    .line 56
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/ShareHelper;->o88O8()V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public O0oo0o0〇()V
    .locals 1

    .line 1
    const-string v0, "back"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O0o〇O0〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8O〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8O〇88oO0(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_6

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇00O:Ljava/util/ArrayList;

    .line 4
    .line 5
    if-eqz v0, :cond_6

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇00O:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eq v0, v1, :cond_0

    .line 18
    .line 19
    goto/16 :goto_2

    .line 20
    .line 21
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    const-string v1, "size = "

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const-string v1, "PdfEditingPresenter"

    .line 43
    .line 44
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O〇O00O:Ljava/util/ArrayList;

    .line 48
    .line 49
    if-eqz v0, :cond_4

    .line 50
    .line 51
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-lez v0, :cond_4

    .line 56
    .line 57
    new-instance v0, Ljava/util/ArrayList;

    .line 58
    .line 59
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .line 61
    .line 62
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O〇O00O:Ljava/util/ArrayList;

    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    if-eqz v2, :cond_3

    .line 73
    .line 74
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    check-cast v2, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;

    .line 79
    .line 80
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 81
    .line 82
    .line 83
    move-result-object v3

    .line 84
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    if-eqz v4, :cond_1

    .line 89
    .line 90
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    move-result-object v4

    .line 94
    check-cast v4, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;

    .line 95
    .line 96
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇o〇()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v5

    .line 100
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇080()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v6

    .line 104
    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 105
    .line 106
    .line 107
    move-result v5

    .line 108
    if-eqz v5, :cond_2

    .line 109
    .line 110
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇080()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    invoke-virtual {v4, v2}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->O8(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    .line 119
    .line 120
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    goto :goto_0

    .line 124
    :cond_3
    move-object p1, v0

    .line 125
    :cond_4
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O〇O00O:Ljava/util/ArrayList;

    .line 126
    .line 127
    const/4 v0, 0x1

    .line 128
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08O〇00〇o:Z

    .line 129
    .line 130
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0o〇〇Oo()Z

    .line 131
    .line 132
    .line 133
    move-result v0

    .line 134
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 135
    .line 136
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇00O:Ljava/util/ArrayList;

    .line 137
    .line 138
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 139
    .line 140
    .line 141
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 146
    .line 147
    .line 148
    move-result v0

    .line 149
    if-eqz v0, :cond_5

    .line 150
    .line 151
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    check-cast v0, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;

    .line 156
    .line 157
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇00O:Ljava/util/ArrayList;

    .line 158
    .line 159
    new-instance v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$PdfEditingImageEntity;

    .line 160
    .line 161
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇o00〇〇Oo()J

    .line 162
    .line 163
    .line 164
    move-result-wide v3

    .line 165
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇o〇()Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v0

    .line 169
    invoke-direct {v2, v3, v4, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$PdfEditingImageEntity;-><init>(JLjava/lang/String;)V

    .line 170
    .line 171
    .line 172
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    .line 174
    .line 175
    goto :goto_1

    .line 176
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooo〇8oO()V

    .line 177
    .line 178
    .line 179
    :cond_6
    :goto_2
    return-void
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public OOO8o〇〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8oOOo:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OOo0O()V
    .locals 10

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$8;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$8;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/ocrapi/LocalOcrClient;->〇O8o08O()Lcom/intsig/ocrapi/LocalOcrClient;

    .line 7
    .line 8
    .line 9
    move-result-object v9

    .line 10
    invoke-virtual {v9, v0}, Lcom/intsig/ocrapi/LocalOcrClient;->o〇0(Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;)V

    .line 11
    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O8o08O8O:Ljava/util/List;

    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPath()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-static {}, Lcom/intsig/nativelib/OcrLanguage;->getLanguage()J

    .line 29
    .line 30
    .line 31
    move-result-wide v4

    .line 32
    const/4 v6, 0x0

    .line 33
    const/4 v7, 0x1

    .line 34
    const/4 v8, 0x0

    .line 35
    move-object v1, v9

    .line 36
    invoke-virtual/range {v1 .. v8}, Lcom/intsig/ocrapi/LocalOcrClient;->〇00(Landroid/content/Context;Ljava/lang/String;J[Lcom/intsig/nativelib/Polygon;ZLcom/intsig/ocrapi/LocalOcrClient$MultimodalClassifyCallback;)V

    .line 37
    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    invoke-virtual {v1}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    new-instance v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$9;

    .line 46
    .line 47
    invoke-direct {v2, p0, v9, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$9;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Lcom/intsig/ocrapi/LocalOcrClient;Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1, v2}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public OOoo(Z)V
    .locals 3

    .line 1
    const-string v0, "from"

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OOO()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "CSPdfWatermarkVideoAD"

    .line 8
    .line 9
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O00()V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->Oo〇O()V

    .line 18
    .line 19
    .line 20
    new-instance v0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 23
    .line 24
    invoke-direct {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;-><init>(Landroid/content/Context;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇O〇80o08O()Ljava/util/HashMap;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇8o8o〇(Ljava/util/HashMap;)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$6;

    .line 36
    .line 37
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$6;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Z)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇O8o08O(Lcom/intsig/advertisement/listener/OnAdRequestListener;)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇80〇808〇O()Lcom/intsig/advertisement/params/AdRequestOptions;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    sget-object v0, Lcom/intsig/advertisement/adapters/positions/PdfWaterMarkManager;->〇O8o08O:Lcom/intsig/advertisement/adapters/positions/PdfWaterMarkManager$Companion;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/positions/PdfWaterMarkManager$Companion;->〇080()Lcom/intsig/advertisement/adapters/positions/PdfWaterMarkManager;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8〇0〇o〇O(Lcom/intsig/advertisement/params/AdRequestOptions;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OO〇0008O8()V
    .locals 1

    .line 1
    const-string v0, "confirm_security_watermark"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo0oO〇O〇O(Lcom/intsig/camscanner/share/type/SharePdf;Ljava/util/List;)Lcom/intsig/camscanner/share/ShareHelper;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/share/type/SharePdf;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;)",
            "Lcom/intsig/camscanner/share/ShareHelper;"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-nez v0, :cond_1

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0o〇〇Oo()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    goto :goto_1

    .line 15
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 16
    :goto_1
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/SharePdf;->o0oO(Z)V

    .line 17
    .line 18
    .line 19
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O:I

    .line 20
    .line 21
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->isFromViewPdf(I)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/SharePdf;->〇o8oO(Z)V

    .line 26
    .line 27
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v2, "gotoShare     mBgWatermark = "

    .line 34
    .line 35
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v2, "  mCompressFlag = "

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    iget v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo〇8o008:I

    .line 49
    .line 50
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    const-string v2, "PdfEditingPresenter"

    .line 58
    .line 59
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 63
    .line 64
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->o8O0(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 65
    .line 66
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    .line 68
    .line 69
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    .line 71
    .line 72
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 73
    .line 74
    .line 75
    move-result v2

    .line 76
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const-string v2, ""

    .line 80
    .line 81
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    const-string v2, "CSPdfPreview"

    .line 89
    .line 90
    const-string v3, "share"

    .line 91
    .line 92
    const-string v4, "total_page_num"

    .line 93
    .line 94
    invoke-static {v2, v3, v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08O〇00〇o:Z

    .line 98
    .line 99
    if-eqz v0, :cond_2

    .line 100
    .line 101
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O〇O00O:Ljava/util/ArrayList;

    .line 102
    .line 103
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->o80ooO(Ljava/util/ArrayList;)V

    .line 104
    .line 105
    .line 106
    :cond_2
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOO〇〇:Z

    .line 107
    .line 108
    if-eqz v0, :cond_3

    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo0:Ljava/lang/String;

    .line 111
    .line 112
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/SharePdf;->OO〇(Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    :cond_3
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO:Z

    .line 116
    .line 117
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/SharePdf;->o8〇(Z)V

    .line 118
    .line 119
    .line 120
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Ooo08:Z

    .line 121
    .line 122
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/SharePdf;->〇〇O00〇8(Z)V

    .line 123
    .line 124
    .line 125
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo〇8o008:I

    .line 126
    .line 127
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->O0o(I)V

    .line 128
    .line 129
    .line 130
    new-instance v0, Lcom/intsig/camscanner/pdf/preshare/o〇0OOo〇0;

    .line 131
    .line 132
    invoke-direct {v0, p0, p2, p1}, Lcom/intsig/camscanner/pdf/preshare/o〇0OOo〇0;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Ljava/util/List;Lcom/intsig/camscanner/share/type/SharePdf;)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/type/BaseImagePdf;->O00(Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;)V

    .line 136
    .line 137
    .line 138
    new-instance p2, Lcom/intsig/camscanner/pdf/preshare/OOO〇O0;

    .line 139
    .line 140
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pdf/preshare/OOO〇O0;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/type/SharePdf;->〇08〇0〇o〇8(Lcom/intsig/camscanner/share/type/SharePdf$OnTryDeductionListener;)V

    .line 144
    .line 145
    .line 146
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 147
    .line 148
    .line 149
    move-result p2

    .line 150
    xor-int/2addr p2, v1

    .line 151
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/share/type/SharePdf;->〇〇〇0880(Z)V

    .line 152
    .line 153
    .line 154
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 155
    .line 156
    invoke-static {p1}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 157
    .line 158
    .line 159
    move-result-object p1

    .line 160
    return-object p1
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public Oo〇O()V
    .locals 2

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string v1, "handleBgWatermarkDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 24
    .line 25
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->Oo0〇Ooo()V

    .line 26
    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 30
    .line 31
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->o88o0O()V

    .line 32
    .line 33
    .line 34
    :goto_1
    const-string v0, "add_security_watermark"

    .line 35
    .line 36
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O〇Oooo〇〇()V
    .locals 5

    .line 1
    const-string v0, "onClickShare"

    .line 2
    .line 3
    const-string v1, "PdfEditingPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇08〇o0O:I

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string v0, "auto"

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const-string v0, "manual"

    .line 16
    .line 17
    :goto_0
    const-string v2, "CSPdfPreview"

    .line 18
    .line 19
    const-string v3, "pdf_page_direction"

    .line 20
    .line 21
    const-string v4, "type"

    .line 22
    .line 23
    invoke-static {v2, v3, v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 27
    .line 28
    const/4 v2, 0x0

    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-nez v0, :cond_2

    .line 40
    .line 41
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-nez v0, :cond_2

    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 54
    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 56
    .line 57
    new-instance v3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$3;

    .line 58
    .line 59
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$3;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 60
    .line 61
    .line 62
    const/4 v4, 0x0

    .line 63
    invoke-direct {v0, v1, v3, v4}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    new-array v2, v2, [Ljava/lang/Void;

    .line 71
    .line 72
    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 73
    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooO〇00O()V

    .line 77
    .line 78
    .line 79
    :goto_1
    return-void

    .line 80
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇OO80oO()I

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    new-instance v3, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    const-string v4, "watermarkNumFromServer"

    .line 90
    .line 91
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    sget-object v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$14;->〇080:[I

    .line 105
    .line 106
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO〇00〇0O:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 107
    .line 108
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 109
    .line 110
    .line 111
    move-result v1

    .line 112
    aget v0, v0, v1

    .line 113
    .line 114
    const/4 v1, 0x1

    .line 115
    if-eq v0, v1, :cond_3

    .line 116
    .line 117
    const-string v0, "share"

    .line 118
    .line 119
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO:Z

    .line 123
    .line 124
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Ooo08:Z

    .line 125
    .line 126
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O0O8()V

    .line 127
    .line 128
    .line 129
    return-void

    .line 130
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇OO()V

    .line 131
    .line 132
    .line 133
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public clear()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8oOOo:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 5
    .line 6
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇008〇o0〇〇(Z)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 10
    .line 11
    const v1, 0x7f130671

    .line 12
    .line 13
    .line 14
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇〇o〇(I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public dismiss()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o0O0()Landroid/os/Bundle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇00O0:Landroid/os/Bundle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o0O〇8o0O(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o80ooO()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0o〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v1, "restoreInitedCsQrCodeFlag   mHasShowingQrCode = "

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "PdfEditingPresenter"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o88〇OO08〇()J
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇〇0o〇〇0:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmp-long v4, v0, v2

    .line 6
    .line 7
    if-gez v4, :cond_1

    .line 8
    .line 9
    new-instance v0, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 15
    .line 16
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    new-instance v1, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 26
    .line 27
    iget-boolean v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOO〇〇:Z

    .line 28
    .line 29
    if-eqz v3, :cond_0

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const/4 v3, 0x0

    .line 35
    :goto_0
    invoke-direct {v1, v2, v0, v3}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 36
    .line 37
    .line 38
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇o0O:I

    .line 39
    .line 40
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->isFromViewPdf(I)Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/type/SharePdf;->〇o8oO(Z)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/type/BaseShare;->oo88o8O()J

    .line 48
    .line 49
    .line 50
    move-result-wide v0

    .line 51
    iput-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇〇0o〇〇0:J

    .line 52
    .line 53
    :cond_1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇〇0o〇〇0:J

    .line 54
    .line 55
    return-wide v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o8O〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method public oO00OOO()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public ooOO()V
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmp-long v4, v0, v2

    .line 6
    .line 7
    if-ltz v4, :cond_0

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 10
    .line 11
    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0O:Landroid/net/Uri;

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO8oO0o〇()Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->Oooo8o0〇()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8oOOo:Z

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 28
    .line 29
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇008〇o0〇〇(Z)V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public ooo〇〇O〇()V
    .locals 1

    .line 1
    const-string v0, "clear_security_water"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0OOo〇0()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0o〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "curStatus = "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, "     mHasShowingQrCode = "

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const-string v2, "PdfEditingPresenter"

    .line 33
    .line 34
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 38
    .line 39
    if-eq v1, v0, :cond_0

    .line 40
    .line 41
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 44
    .line 45
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->Oo〇o(Z)V

    .line 46
    .line 47
    .line 48
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇〇0o()V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇8oOO88()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OO〇00〇0O:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;->PPT:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView$FROM;

    .line 4
    .line 5
    if-ne v0, v1, :cond_1

    .line 6
    .line 7
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 8
    .line 9
    const-wide/16 v2, 0x0

    .line 10
    .line 11
    cmp-long v4, v0, v2

    .line 12
    .line 13
    if-gez v4, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-static {v0, v1}, Lcom/intsig/camscanner/gallery/pdf/DocFileUtils;->〇080(J)V

    .line 17
    .line 18
    .line 19
    :cond_1
    :goto_0
    return-void
    .line 20
    .line 21
.end method

.method public o〇8〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 6
    .line 7
    .line 8
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->PdfTarBar:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    const v2, 0x7f130b01

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/o〇〇0〇;

    .line 28
    .line 29
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/o〇〇0〇;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function2;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o〇()Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇0〇O0088o()V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇oO()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8oOOo:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 5
    .line 6
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇008〇o0〇〇(Z)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 10
    .line 11
    const v1, 0x7f130672

    .line 12
    .line 13
    .line 14
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇〇o〇(I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇000〇〇08()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0o〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/SwitchControl;->〇o00〇〇Oo()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    new-instance v2, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v3, "queryProductsForBackUserNotice >>> isNeedRequest = "

    .line 15
    .line 16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v3, " isRCN = "

    .line 23
    .line 24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const-string v2, "PdfEditingPresenter"

    .line 35
    .line 36
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    if-eqz v0, :cond_0

    .line 40
    .line 41
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 44
    .line 45
    new-instance v2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$11;

    .line 46
    .line 47
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$11;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 48
    .line 49
    .line 50
    const/4 v3, 0x0

    .line 51
    const/4 v4, 0x0

    .line 52
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;Z)V

    .line 53
    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇8o8o〇()Ljava/util/concurrent/ExecutorService;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    new-array v2, v4, [Ljava/lang/Void;

    .line 60
    .line 61
    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 62
    .line 63
    .line 64
    :cond_0
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇0OO8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇08(Landroid/app/Activity;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0〇OO8(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇00O:Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oO()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O〇0〇o808〇()V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0o〇〇Oo()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8〇OO0〇0o:Z

    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooOO()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->ooo〇8oO()V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇000〇〇08()V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇0O〇Oo()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o〇o〇Oo88:Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->Oo08(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    xor-int/lit8 v0, v0, 0x1

    .line 8
    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8(Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;)Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->OO0o〇〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    iget v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;->is_show_pdf_share:I

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    if-ne v0, v1, :cond_0

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O8o08O8O:Ljava/util/List;

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    iget p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;->show_pdf_pages:I

    .line 27
    .line 28
    if-le v0, p1, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 v1, 0x0

    .line 32
    :goto_0
    return v1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇8o()V
    .locals 1

    .line 1
    const-string v0, "modify_security_water"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8〇0〇o〇O()V
    .locals 5

    .line 1
    const-string v0, "PdfEditingPresenter"

    .line 2
    .line 3
    const-string v1, "compress"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o88〇OO08〇()J

    .line 9
    .line 10
    .line 11
    move-result-wide v0

    .line 12
    long-to-double v0, v0

    .line 13
    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    .line 14
    .line 15
    cmpg-double v4, v0, v2

    .line 16
    .line 17
    if-gez v4, :cond_0

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    const v1, 0x7f130c0c

    .line 22
    .line 23
    .line 24
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o88〇OO08〇()J

    .line 31
    .line 32
    .line 33
    move-result-wide v1

    .line 34
    invoke-interface {v0, v1, v2}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->O8O〇8oo08(J)V

    .line 35
    .line 36
    .line 37
    :goto_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method 〇O()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo〇8o008:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o0O0O8()V
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 7
    .line 8
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇O8o08O()Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇8(Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;)Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    const/4 v3, 0x0

    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    const/4 v2, 0x1

    .line 27
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOO〇〇:Z

    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 30
    .line 31
    iget v4, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;->show_pdf_pages:I

    .line 32
    .line 33
    invoke-virtual {v2, v3, v4}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    new-instance v4, Ljava/util/ArrayList;

    .line 38
    .line 39
    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 40
    .line 41
    .line 42
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O8o08O8O:Ljava/util/List;

    .line 43
    .line 44
    iget v1, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;->show_pdf_pages:I

    .line 45
    .line 46
    invoke-interface {v2, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    new-instance v2, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 51
    .line 52
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 53
    .line 54
    invoke-direct {v2, v3, v0, v4}, Lcom/intsig/camscanner/share/type/SharePdf;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0, v2, v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo0oO〇O〇O(Lcom/intsig/camscanner/share/type/SharePdf;Ljava/util/List;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    new-instance v3, Ljava/lang/StringBuilder;

    .line 62
    .line 63
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    const-string v4, "subPdfImageList\uff1a"

    .line 67
    .line 68
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 72
    .line 73
    .line 74
    move-result v4

    .line 75
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    const-string v4, "PdfEditingPresenter"

    .line 83
    .line 84
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    new-instance v3, Lcom/intsig/camscanner/pdf/preshare/o0ooO;

    .line 88
    .line 89
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/pdf/preshare/o0ooO;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/share/ShareHelper;->O8888(Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 93
    .line 94
    .line 95
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 96
    .line 97
    .line 98
    move-result v1

    .line 99
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEquityMeasurementDialog;->〇8〇80o(I)Lcom/intsig/camscanner/pdf/preshare/PdfEquityMeasurementDialog;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    new-instance v3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$4;

    .line 104
    .line 105
    invoke-direct {v3, p0, v0, v2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter$4;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;Lcom/intsig/camscanner/share/ShareHelper;Lcom/intsig/camscanner/share/type/SharePdf;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/pdf/preshare/PdfEquityMeasurementDialog;->o〇O8OO(Lcom/intsig/camscanner/pdf/preshare/PdfEquityMeasurementDialog$ActionCallBack;)V

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 112
    .line 113
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    invoke-virtual {v1, v0, v4}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    goto :goto_0

    .line 121
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/pdf/preshare/o〇8;

    .line 122
    .line 123
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdf/preshare/o〇8;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {p0, v3, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0oO008(ZLcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 127
    .line 128
    .line 129
    :goto_0
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇o〇8()V
    .locals 1

    .line 1
    const-string v0, "cancel_security_watermark"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇Oo0()V
    .locals 11

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 2
    .line 3
    invoke-static {v0, v1}, Lcom/intsig/camscanner/image2word/Image2WordNavigator;->〇80〇808〇O(J)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    iget-wide v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇0O:J

    .line 12
    .line 13
    const-string v3, "pdf_to_word"

    .line 14
    .line 15
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/camscanner/image2word/Image2WordNavigator;->Oo08(Landroidx/fragment/app/FragmentActivity;JLjava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOO〇〇()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/4 v1, 0x1

    .line 24
    if-ne v0, v1, :cond_2

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->OO〇00〇8oO:Ljava/lang/String;

    .line 27
    .line 28
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    const-string v0, "PdfEditingPresenter"

    .line 35
    .line 36
    const-string v1, "pdfPath should not be null"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/oo〇;

    .line 47
    .line 48
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/oo〇;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 52
    .line 53
    .line 54
    :goto_0
    return-void

    .line 55
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O0OO8〇0()Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-eqz v0, :cond_3

    .line 60
    .line 61
    return-void

    .line 62
    :cond_3
    invoke-static {v1}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->o〇0(I)Z

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    if-eqz v0, :cond_4

    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 69
    .line 70
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/O8〇o;

    .line 71
    .line 72
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/O8〇o;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;)V

    .line 73
    .line 74
    .line 75
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DialogUtils;->O0o〇〇Oo(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_4
    new-array v0, v1, [Landroid/util/Pair;

    .line 80
    .line 81
    new-instance v1, Landroid/util/Pair;

    .line 82
    .line 83
    const-string v2, "from_part"

    .line 84
    .line 85
    const-string v3, "cs_list"

    .line 86
    .line 87
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 88
    .line 89
    .line 90
    const/4 v2, 0x0

    .line 91
    aput-object v1, v0, v2

    .line 92
    .line 93
    const-string v1, "CSPdfPreview"

    .line 94
    .line 95
    const-string v2, "transfer_word"

    .line 96
    .line 97
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 98
    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 101
    .line 102
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 107
    .line 108
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->o8(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 109
    .line 110
    .line 111
    move-result-object v4

    .line 112
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 113
    .line 114
    invoke-static {v4}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->o0ooO(Ljava/util/List;)I

    .line 115
    .line 116
    .line 117
    move-result v1

    .line 118
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->O8〇o(Landroid/content/Context;I)Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-eqz v0, :cond_5

    .line 123
    .line 124
    return-void

    .line 125
    :cond_5
    new-instance v2, Lcom/intsig/camscanner/mode_ocr/OCRClient;

    .line 126
    .line 127
    invoke-direct {v2}, Lcom/intsig/camscanner/mode_ocr/OCRClient;-><init>()V

    .line 128
    .line 129
    .line 130
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 131
    .line 132
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->〇8(Lcom/intsig/camscanner/purchase/entity/Function;)V

    .line 133
    .line 134
    .line 135
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_COLLAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 136
    .line 137
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->O08000(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 138
    .line 139
    .line 140
    const-string v8, "paragraph"

    .line 141
    .line 142
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 143
    .line 144
    iget-object v5, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->Oo0〇Ooo:Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;

    .line 145
    .line 146
    const/4 v6, 0x0

    .line 147
    const/4 v7, 0x0

    .line 148
    const/4 v9, 0x0

    .line 149
    const-string v10, ""

    .line 150
    .line 151
    invoke-virtual/range {v2 .. v10}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->oo〇(Landroid/app/Activity;Ljava/util/List;Lcom/intsig/camscanner/mode_ocr/OCRClient$OCRProgressListener;Lcom/intsig/camscanner/mode_ocr/AbstractOcrInterceptor;ILjava/lang/String;Lcom/intsig/camscanner/mode_ocr/interfaces/OCRProgressDialogCallback;Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    :goto_1
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇〇00OO(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->O88O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "other_app"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 12
    .line 13
    invoke-interface {p1}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇OO〇00〇0O()V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 18
    .line 19
    invoke-interface {v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->O8O〇88oO0()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-nez v0, :cond_6

    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    if-eqz p1, :cond_3

    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇O800oo()Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-nez p1, :cond_3

    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇oO8O0〇〇O()Z

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇080()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    const/4 v2, -0x1

    .line 43
    if-eq v1, v2, :cond_1

    .line 44
    .line 45
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇080()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-eqz v1, :cond_1

    .line 50
    .line 51
    const/4 v1, 0x1

    .line 52
    goto :goto_0

    .line 53
    :cond_1
    const/4 v1, 0x0

    .line 54
    :goto_0
    if-eqz p1, :cond_2

    .line 55
    .line 56
    if-eqz v1, :cond_2

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_2
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇O8(Z)V

    .line 60
    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 63
    .line 64
    invoke-interface {p1}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->o8o()V

    .line 65
    .line 66
    .line 67
    :goto_1
    return-void

    .line 68
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 69
    .line 70
    invoke-interface {p1}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->Oo80()Z

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    if-eqz p1, :cond_4

    .line 75
    .line 76
    return-void

    .line 77
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooO8〇00()Z

    .line 78
    .line 79
    .line 80
    move-result p1

    .line 81
    if-nez p1, :cond_5

    .line 82
    .line 83
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o00ooOO(Z)V

    .line 84
    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 87
    .line 88
    invoke-interface {p1}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->oOo〇8o008()V

    .line 89
    .line 90
    .line 91
    const-string p1, "add_security_guide"

    .line 92
    .line 93
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->oOo(Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    return-void

    .line 97
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 98
    .line 99
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇〇0O8ooO(Z)Z

    .line 100
    .line 101
    .line 102
    move-result p1

    .line 103
    if-eqz p1, :cond_6

    .line 104
    .line 105
    return-void

    .line 106
    :cond_6
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8oOOo:Z

    .line 107
    .line 108
    if-eqz p1, :cond_7

    .line 109
    .line 110
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;

    .line 111
    .line 112
    const v0, 0x7f130672

    .line 113
    .line 114
    .line 115
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/pdf/preshare/IPdfEditingView;->〇〇o〇(I)V

    .line 116
    .line 117
    .line 118
    :cond_7
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇〇o8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o8O0()V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingPresenter;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    const v1, 0x7f131d05

    .line 18
    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 21
    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.class public Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;
.super Lcom/intsig/adapter/BaseRecyclerViewAdapter;
.source "PdfEditingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;,
        Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$onWatchAdListener;,
        Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;,
        Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;,
        Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/adapter/BaseRecyclerViewAdapter<",
        "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
        ">;"
    }
.end annotation


# instance fields
.field private final O0O:I

.field private O88O:[I

.field private final O8o08O8O:Landroid/content/Context;

.field private OO〇00〇8oO:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$onWatchAdListener;

.field private Oo80:I

.field private o8o:I

.field private o8oOOo:Z

.field private o8〇OO0〇0o:Z

.field private oOO〇〇:Landroid/graphics/Matrix;

.field private oOo0:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;

.field private oOo〇8o008:Z

.field private oo8ooo8O:I

.field private ooo0〇〇O:Z

.field private final o〇00O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;"
        }
    .end annotation
.end field

.field private final o〇oO:I

.field private 〇080OO8〇0:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

.field private 〇08〇o0O:Z

.field private 〇0O:Z

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;

.field private 〇O〇〇O8:Z

.field private final 〇o0O:I

.field private final 〇〇08O:Z

.field private 〇〇o〇:Z


# direct methods
.method constructor <init>(ZIZILcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;Ljava/util/List;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZIZI",
            "Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->ooo0〇〇O:Z

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇oO:I

    .line 9
    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇〇o〇:Z

    .line 11
    .line 12
    const/4 v2, -0x1

    .line 13
    iput v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->Oo80:I

    .line 14
    .line 15
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 16
    .line 17
    iput-object p5, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 18
    .line 19
    iput-boolean p3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇〇08O:Z

    .line 20
    .line 21
    iput p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0O:I

    .line 22
    .line 23
    iput p4, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇o0O:I

    .line 24
    .line 25
    iput-object p6, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇00O:Ljava/util/List;

    .line 26
    .line 27
    iput-object p7, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0O8OO088()V

    .line 30
    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇oO8O0〇〇O()Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-eqz p1, :cond_0

    .line 37
    .line 38
    iget p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->Oo80:I

    .line 39
    .line 40
    if-lez p1, :cond_0

    .line 41
    .line 42
    const/4 v0, 0x1

    .line 43
    :cond_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇08〇o0O:Z

    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method private O0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->ooo0〇〇O:Z

    .line 3
    .line 4
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->o〇00O:Landroid/widget/ImageView;

    .line 5
    .line 6
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/OoO8;

    .line 7
    .line 8
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/pdf/preshare/OoO8;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v2, 0x1f4

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O08000(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0O8OO088()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇o00〇〇Oo()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo8ooo8O()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    new-instance v2, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v3, "adVideoTimes "

    .line 15
    .line 16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    const-string v3, "PdfEditingAdapter"

    .line 27
    .line 28
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    new-instance v2, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v4, " pdfWatchAdNoWaterMarkTimes"

    .line 37
    .line 38
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    if-ge v1, v0, :cond_0

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇080()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->Oo80:I

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    const/4 v0, -0x1

    .line 61
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->Oo80:I

    .line 62
    .line 63
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic O0o〇〇Oo(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->OO〇00〇8oO:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$onWatchAdListener;

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    invoke-interface {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$onWatchAdListener;->o8〇OO0〇0o()V

    .line 6
    .line 7
    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;->〇0000OOO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const/16 p2, 0x8

    .line 13
    .line 14
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O8O〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;->o08O()V

    .line 4
    .line 5
    .line 6
    :cond_0
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O8〇o(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0o〇〇Oo(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic OO8oO0o〇(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇OO80oO()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const/4 v0, 0x1

    .line 6
    if-lez p1, :cond_0

    .line 7
    .line 8
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->oOo〇8o008:Z

    .line 9
    .line 10
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 11
    .line 12
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->oo(Z)V

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8oOOo:Z

    .line 18
    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇O〇〇O8:Z

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 22
    .line 23
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇o(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static OOO()I
    .locals 1

    .line 1
    const v0, 0x7f080ad8

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private OOO8o〇〇(Lcom/intsig/camscanner/topic/view/WatermarkView;Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V
    .locals 0

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->Oo08()Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/topic/view/WatermarkView;->setWaterEntity(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/topic/view/WatermarkView;->setWaterEntity(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 12
    .line 13
    .line 14
    :goto_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OOO〇O0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o0O0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic Oo8Oo00oo(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static Ooo()I
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇o00〇〇Oo()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->O8()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x1

    .line 10
    const v3, 0x7f080ac0

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x2

    .line 15
    if-eq v0, v5, :cond_2

    .line 16
    .line 17
    if-eq v0, v4, :cond_2

    .line 18
    .line 19
    if-ne v1, v2, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    if-ne v1, v5, :cond_1

    .line 23
    .line 24
    const v3, 0x7f080ac1

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    if-ne v1, v4, :cond_5

    .line 29
    .line 30
    const v3, 0x7f080abf

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    if-ne v1, v2, :cond_3

    .line 35
    .line 36
    const v3, 0x7f080ac3

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_3
    if-ne v1, v5, :cond_4

    .line 41
    .line 42
    const v3, 0x7f080ac4

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_4
    if-ne v1, v4, :cond_5

    .line 47
    .line 48
    const v3, 0x7f080ac2

    .line 49
    .line 50
    .line 51
    :cond_5
    :goto_0
    return v3
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private Oo〇O(ZZLcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "isShowQrCode = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, " isShowStrongGuide = "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    const-string v0, "PdfEditingAdapter"

    .line 27
    .line 28
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO:Landroid/view/View;

    .line 32
    .line 33
    if-eqz p1, :cond_0

    .line 34
    .line 35
    const/4 p1, 0x0

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/16 p1, 0x8

    .line 38
    .line 39
    :goto_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O〇0(Landroidx/recyclerview/widget/RecyclerView;I)V
    .locals 0

    .line 1
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    if-nez p2, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    instance-of p2, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;

    .line 13
    .line 14
    if-eqz p2, :cond_3

    .line 15
    .line 16
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;

    .line 17
    .line 18
    iget-object p1, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇0O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 19
    .line 20
    if-eqz p1, :cond_3

    .line 21
    .line 22
    iget p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->Oo80:I

    .line 23
    .line 24
    if-lez p2, :cond_2

    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->o〇0()Z

    .line 27
    .line 28
    .line 29
    move-result p2

    .line 30
    if-eqz p2, :cond_1

    .line 31
    .line 32
    const p2, 0x7f120048

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, p2}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    const p2, 0x7f120047

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1, p2}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 43
    .line 44
    .line 45
    :cond_2
    :goto_0
    const/4 p2, 0x0

    .line 46
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 47
    .line 48
    .line 49
    const/high16 p2, 0x3f800000    # 1.0f

    .line 50
    .line 51
    invoke-virtual {p1, p2}, Lcom/airbnb/lottie/LottieAnimationView;->setSpeed(F)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 55
    .line 56
    .line 57
    :cond_3
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static synthetic O〇O〇oO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o0O0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇o(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic o0ooO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8oOOo:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->oOo0:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o88〇OO08〇(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇O〇〇O8:Z

    .line 3
    .line 4
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 5
    .line 6
    .line 7
    const/high16 v0, -0x40800000    # -1.0f

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setSpeed(F)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o8O〇(ZZLcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 6

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    if-eqz p2, :cond_8

    .line 4
    .line 5
    if-eqz p1, :cond_8

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇OO80oO()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    iget p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->Oo80:I

    .line 18
    .line 19
    const/4 v2, 0x2

    .line 20
    const/4 v3, 0x1

    .line 21
    const/4 v4, 0x0

    .line 22
    if-ne p2, v3, :cond_0

    .line 23
    .line 24
    if-gtz p1, :cond_0

    .line 25
    .line 26
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo0:Landroid/widget/ImageView;

    .line 27
    .line 28
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 29
    .line 30
    .line 31
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 32
    .line 33
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    if-ne p2, v2, :cond_1

    .line 38
    .line 39
    if-gtz p1, :cond_1

    .line 40
    .line 41
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo0:Landroid/widget/ImageView;

    .line 42
    .line 43
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 44
    .line 45
    .line 46
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 47
    .line 48
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 52
    .line 53
    const v5, 0x7f080de3

    .line 54
    .line 55
    .line 56
    invoke-static {p2, v5}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 57
    .line 58
    .line 59
    move-result-object p2

    .line 60
    iget-object v5, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 61
    .line 62
    invoke-virtual {v5, p2, v4, v4, v4}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_1
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 67
    .line 68
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 69
    .line 70
    .line 71
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo0:Landroid/widget/ImageView;

    .line 72
    .line 73
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 74
    .line 75
    .line 76
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->o〇0()Z

    .line 77
    .line 78
    .line 79
    move-result p2

    .line 80
    const v5, 0x7f130bf5

    .line 81
    .line 82
    .line 83
    if-eqz p2, :cond_3

    .line 84
    .line 85
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->〇o〇()I

    .line 86
    .line 87
    .line 88
    move-result p1

    .line 89
    if-ne p1, v2, :cond_2

    .line 90
    .line 91
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 92
    .line 93
    const p2, 0x7f131b2a

    .line 94
    .line 95
    .line 96
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 97
    .line 98
    .line 99
    goto/16 :goto_1

    .line 100
    .line 101
    :cond_2
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 102
    .line 103
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 104
    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_3
    if-lez p1, :cond_4

    .line 108
    .line 109
    iget-object p2, p3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 110
    .line 111
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    const v2, 0x7f1309e4

    .line 116
    .line 117
    .line 118
    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object p2

    .line 122
    new-array v2, v3, [Ljava/lang/Object;

    .line 123
    .line 124
    new-instance v3, Ljava/lang/StringBuilder;

    .line 125
    .line 126
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    .line 128
    .line 129
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    const-string p1, ""

    .line 133
    .line 134
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object p1

    .line 141
    aput-object p1, v2, v1

    .line 142
    .line 143
    invoke-static {p2, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object p1

    .line 147
    iget-object p2, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 148
    .line 149
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    .line 151
    .line 152
    goto :goto_1

    .line 153
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇80〇808〇O()Z

    .line 154
    .line 155
    .line 156
    move-result p1

    .line 157
    if-nez p1, :cond_6

    .line 158
    .line 159
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0〇〇00()I

    .line 160
    .line 161
    .line 162
    move-result p1

    .line 163
    const/4 p2, 0x4

    .line 164
    if-ne p1, p2, :cond_5

    .line 165
    .line 166
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 167
    .line 168
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 169
    .line 170
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 171
    .line 172
    .line 173
    move-result-object p2

    .line 174
    const v1, 0x7f130fa0

    .line 175
    .line 176
    .line 177
    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object p2

    .line 181
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    .line 183
    .line 184
    goto :goto_1

    .line 185
    :cond_5
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 186
    .line 187
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 188
    .line 189
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 190
    .line 191
    .line 192
    move-result-object p2

    .line 193
    invoke-virtual {p2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object p2

    .line 197
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    .line 199
    .line 200
    goto :goto_1

    .line 201
    :cond_6
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 202
    .line 203
    iget-object p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 204
    .line 205
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 206
    .line 207
    .line 208
    move-result-object p2

    .line 209
    invoke-virtual {p2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object p2

    .line 213
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    .line 215
    .line 216
    :goto_1
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->o〇0()Z

    .line 217
    .line 218
    .line 219
    move-result p1

    .line 220
    if-eqz p1, :cond_7

    .line 221
    .line 222
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 223
    .line 224
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 225
    .line 226
    .line 227
    goto :goto_2

    .line 228
    :cond_7
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 229
    .line 230
    invoke-virtual {p1, v4, v4, v4, v4}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 231
    .line 232
    .line 233
    goto :goto_2

    .line 234
    :cond_8
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 235
    .line 236
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 237
    .line 238
    .line 239
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 240
    .line 241
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 242
    .line 243
    .line 244
    iget-object p1, p3, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo0:Landroid/widget/ImageView;

    .line 245
    .line 246
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 247
    .line 248
    .line 249
    :goto_2
    return-void
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method static synthetic o8oO〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic oO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO00OOO(IIII)Lcom/intsig/camscanner/util/ParcelSize;
    .locals 9

    .line 1
    int-to-double v0, p2

    .line 2
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 3
    .line 4
    mul-double v0, v0, v2

    .line 5
    .line 6
    int-to-double v4, p1

    .line 7
    div-double/2addr v0, v4

    .line 8
    int-to-double v4, p4

    .line 9
    mul-double v2, v2, v4

    .line 10
    .line 11
    int-to-double v6, p3

    .line 12
    div-double/2addr v2, v6

    .line 13
    cmpl-double v8, v0, v2

    .line 14
    .line 15
    if-ltz v8, :cond_0

    .line 16
    .line 17
    div-double/2addr v4, v0

    .line 18
    double-to-int v0, v4

    .line 19
    move v1, p4

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    mul-double v6, v6, v0

    .line 22
    .line 23
    double-to-int v0, v6

    .line 24
    move v1, v0

    .line 25
    move v0, p3

    .line 26
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v5, "sourceSize.width = "

    .line 32
    .line 33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string p1, "  sourceSize.height = "

    .line 40
    .line 41
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string p1, "  maxWidth = "

    .line 48
    .line 49
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    const-string p1, "  maxHeight = "

    .line 56
    .line 57
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    const-string p1, "  destWidth = "

    .line 64
    .line 65
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    const-string p1, "  destHeight = "

    .line 72
    .line 73
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const-string p1, "  referRatio = "

    .line 80
    .line 81
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    const-string p2, "PdfEditingAdapter"

    .line 92
    .line 93
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    new-instance p1, Lcom/intsig/camscanner/util/ParcelSize;

    .line 97
    .line 98
    invoke-direct {p1, v0, v1}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 99
    .line 100
    .line 101
    return-object p1
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic oo〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇0OOo〇0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇8oOO88(Landroid/content/Context;Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇80()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0O:I

    .line 6
    .line 7
    if-lez v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    mul-int v0, v0, v1

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0O:I

    .line 16
    .line 17
    div-int/2addr v0, v1

    .line 18
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    int-to-double v1, v1

    .line 23
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 24
    .line 25
    .line 26
    move-result p2

    .line 27
    int-to-double v3, p2

    .line 28
    div-double/2addr v1, v3

    .line 29
    int-to-double v3, v0

    .line 30
    mul-double v3, v3, v1

    .line 31
    .line 32
    double-to-int p2, v3

    .line 33
    iget-object p3, p3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 34
    .line 35
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 40
    .line 41
    new-instance p2, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string v1, "itemView height = "

    .line 47
    .line 48
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 52
    .line 53
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v1, ",width = "

    .line 57
    .line 58
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 62
    .line 63
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p2

    .line 70
    const-string v0, "PdfEditingAdapter"

    .line 71
    .line 72
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    iget-boolean p2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇〇08O:Z

    .line 76
    .line 77
    if-eqz p2, :cond_1

    .line 78
    .line 79
    const/high16 p2, 0x41200000    # 10.0f

    .line 80
    .line 81
    invoke-static {p1, p2}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    float-to-int p1, p1

    .line 86
    goto :goto_0

    .line 87
    :cond_1
    const/4 p1, 0x0

    .line 88
    :goto_0
    invoke-virtual {p3, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 89
    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private o〇O(ILcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    if-gtz p1, :cond_1

    .line 6
    .line 7
    iget-object p1, p2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    const/16 v0, 0x8

    .line 14
    .line 15
    if-eq p1, v0, :cond_1

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;

    .line 18
    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object p1, p2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 23
    .line 24
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    new-instance v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$2;

    .line 29
    .line 30
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$2;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Landroid/view/ViewTreeObserver;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 34
    .line 35
    .line 36
    :cond_1
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o〇〇0〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->OO8oO0o〇(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 2

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O88O:[I

    .line 5
    .line 6
    iget-object v1, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->o〇00O:Landroid/widget/ImageView;

    .line 7
    .line 8
    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->o〇00O:Landroid/widget/ImageView;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->oOO〇〇:Landroid/graphics/Matrix;

    .line 18
    .line 19
    iget-object p1, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->o〇00O:Landroid/widget/ImageView;

    .line 20
    .line 21
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    if-eqz p1, :cond_0

    .line 26
    .line 27
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8o:I

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->oo8ooo8O:I

    .line 38
    .line 39
    :cond_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇0000OOO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o88〇OO08〇(Lcom/airbnb/lottie/LottieAnimationView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇000O0(Landroidx/recyclerview/widget/RecyclerView;I)V
    .locals 0

    .line 1
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    if-nez p2, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    instance-of p2, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;

    .line 13
    .line 14
    if-eqz p2, :cond_1

    .line 15
    .line 16
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;

    .line 17
    .line 18
    iget-object p2, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO:Landroid/view/View;

    .line 19
    .line 20
    iget-object p1, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇0O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 21
    .line 22
    if-eqz p2, :cond_1

    .line 23
    .line 24
    if-eqz p1, :cond_1

    .line 25
    .line 26
    new-instance p2, Lcom/intsig/camscanner/pdf/preshare/o800o8O;

    .line 27
    .line 28
    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/pdf/preshare/o800o8O;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/airbnb/lottie/LottieAnimationView;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, p2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 32
    .line 33
    .line 34
    :cond_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic 〇00〇8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇O〇〇O8:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇08O8o〇0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0O〇Oo(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 2

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇8〇oO〇〇8o:Landroid/view/ViewStub;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->ooo0〇〇O:Landroid/view/ViewStub;

    .line 9
    .line 10
    invoke-virtual {p1, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇80()I
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 8
    .line 9
    const/16 v2, 0xa

    .line 10
    .line 11
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    sub-int/2addr v0, v1

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic 〇8〇0〇o〇O(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static 〇O〇80o08O()I
    .locals 1

    .line 1
    const v0, 0x7f080ad6

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇o(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇08〇o0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇0o(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8O〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇〇o8(ZLcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_6

    .line 2
    .line 3
    iget-object p1, p2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->o〇0()Z

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    if-eqz p2, :cond_0

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->OOO()I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 16
    .line 17
    .line 18
    goto :goto_3

    .line 19
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->OO0o〇〇〇〇0()Z

    .line 20
    .line 21
    .line 22
    move-result p2

    .line 23
    if-eqz p2, :cond_1

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇O〇80o08O()I

    .line 26
    .line 27
    .line 28
    move-result p2

    .line 29
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 30
    .line 31
    .line 32
    goto :goto_3

    .line 33
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇80〇808〇O()Z

    .line 34
    .line 35
    .line 36
    move-result p2

    .line 37
    if-nez p2, :cond_5

    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 40
    .line 41
    .line 42
    move-result-object p2

    .line 43
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_4

    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->〇〇888()Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-eqz v0, :cond_2

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0〇〇00()I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    if-nez v0, :cond_3

    .line 61
    .line 62
    const/high16 v0, 0x42f80000    # 124.0f

    .line 63
    .line 64
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    iput v0, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_3
    const/high16 v0, 0x42d40000    # 106.0f

    .line 72
    .line 73
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    iput v0, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 78
    .line 79
    :goto_0
    const/high16 v0, 0x42300000    # 44.0f

    .line 80
    .line 81
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    iput v0, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 88
    .line 89
    invoke-static {v0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingUtil;->〇o〇(Landroid/content/Context;Landroid/widget/ImageView;)V

    .line 90
    .line 91
    .line 92
    goto :goto_2

    .line 93
    :cond_4
    :goto_1
    invoke-static {}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingUtil;->〇080()I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 98
    .line 99
    .line 100
    const/4 v0, -0x2

    .line 101
    iput v0, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 102
    .line 103
    :goto_2
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    .line 105
    .line 106
    goto :goto_3

    .line 107
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->Ooo()I

    .line 108
    .line 109
    .line 110
    move-result p2

    .line 111
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 112
    .line 113
    .line 114
    :cond_6
    :goto_3
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic 〇〇〇0〇〇0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8oOOo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O000(I)Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇08〇o0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    if-lt p1, v0, :cond_0

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇00O:Ljava/util/List;

    .line 9
    .line 10
    sub-int/2addr p1, v0

    .line 11
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 16
    .line 17
    return-object p1

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇00O:Ljava/util/List;

    .line 19
    .line 20
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 25
    .line 26
    return-object p1

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇00O:Ljava/util/List;

    .line 28
    .line 29
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 34
    .line 35
    return-object p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected O8ooOoo〇()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method Oo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OoO8(Landroid/view/View;I)Lcom/intsig/adapter/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I)",
            "Lcom/intsig/adapter/BaseViewHolder<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p2, v0, :cond_0

    .line 3
    .line 4
    new-instance p2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;

    .line 5
    .line 6
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;-><init>(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    return-object p2

    .line 10
    :cond_0
    new-instance p2, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;

    .line 11
    .line 12
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;-><init>(Landroid/view/View;)V

    .line 13
    .line 14
    .line 15
    return-object p2
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Ooo8〇〇(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 4

    .line 1
    :try_start_0
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    move v2, v1

    .line 18
    :goto_0
    if-gt v2, v0, :cond_0

    .line 19
    .line 20
    sub-int v3, v2, v1

    .line 21
    .line 22
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇000O0(Landroidx/recyclerview/widget/RecyclerView;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .line 24
    .line 25
    add-int/lit8 v2, v2, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :catch_0
    move-exception p1

    .line 29
    const-string v0, "PdfEditingAdapter"

    .line 30
    .line 31
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method Oo〇o(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getItemCount()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇00O:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇08〇o0O:Z

    .line 8
    .line 9
    if-eqz v1, :cond_1

    .line 10
    .line 11
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    add-int/lit8 v0, v0, 0x1

    .line 16
    .line 17
    return v0

    .line 18
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇08〇o0O:Z

    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O(I)I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    const p1, 0x7f0d046f

    .line 5
    .line 6
    .line 7
    return p1

    .line 8
    :cond_0
    const p1, 0x7f0d0479

    .line 9
    .line 10
    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p1, Lcom/intsig/adapter/BaseViewHolder;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇oo〇(Lcom/intsig/adapter/BaseViewHolder;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method oo(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->oOo〇8o008:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇OO80oO()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 19
    .line 20
    if-eq v0, p1, :cond_1

    .line 21
    .line 22
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 23
    .line 24
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public ooOO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$onWatchAdListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->OO〇00〇8oO:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$onWatchAdListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public ooo〇8oO(Landroid/content/Context;Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;)Lcom/intsig/camscanner/util/ParcelSize;
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇80()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0O:I

    .line 6
    .line 7
    if-lez v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    mul-int v1, v1, v0

    .line 14
    .line 15
    iget v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0O:I

    .line 16
    .line 17
    div-int/2addr v1, v2

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    move v1, v0

    .line 20
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageHeight()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    int-to-double v2, v2

    .line 25
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPageWidth()I

    .line 26
    .line 27
    .line 28
    move-result v4

    .line 29
    int-to-double v4, v4

    .line 30
    div-double/2addr v2, v4

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const v4, 0x7f07059b

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    int-to-float p1, p1

    .line 43
    int-to-double v4, v1

    .line 44
    mul-double v4, v4, v2

    .line 45
    .line 46
    double-to-int v2, v4

    .line 47
    iget-boolean v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 48
    .line 49
    if-eqz v3, :cond_1

    .line 50
    .line 51
    int-to-float v3, v2

    .line 52
    sub-float/2addr v3, p1

    .line 53
    float-to-int p1, v3

    .line 54
    goto :goto_1

    .line 55
    :cond_1
    move p1, v2

    .line 56
    :goto_1
    iget v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0O:I

    .line 57
    .line 58
    if-lez v3, :cond_2

    .line 59
    .line 60
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getImageWidth()I

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getImageHeight()I

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    invoke-direct {p0, v0, p2, v1, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->oO00OOO(IIII)Lcom/intsig/camscanner/util/ParcelSize;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    return-object p1

    .line 73
    :cond_2
    new-instance p1, Lcom/intsig/camscanner/util/ParcelSize;

    .line 74
    .line 75
    invoke-direct {p1, v0, v2}, Lcom/intsig/camscanner/util/ParcelSize;-><init>(II)V

    .line 76
    .line 77
    .line 78
    return-object p1
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public o〇o(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 4

    .line 1
    :try_start_0
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    move v2, v1

    .line 18
    :goto_0
    if-gt v2, v0, :cond_0

    .line 19
    .line 20
    sub-int v3, v2, v1

    .line 21
    .line 22
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O〇0(Landroidx/recyclerview/widget/RecyclerView;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .line 24
    .line 25
    add-int/lit8 v2, v2, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :catch_0
    move-exception p1

    .line 29
    const-string v0, "PdfEditingAdapter"

    .line 30
    .line 31
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method 〇00O0O0()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8〇OO0〇0o:Z

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0O8OO088()V

    .line 8
    .line 9
    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇08〇o0O:Z

    .line 11
    .line 12
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method 〇O(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnStrongGuideListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o0O0O8(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->oOo0:Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$OnCancelListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇oo〇(Lcom/intsig/adapter/BaseViewHolder;I)V
    .locals 5
    .param p1    # Lcom/intsig/adapter/BaseViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/adapter/BaseViewHolder<",
            "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
            ">;I)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->getItemViewType(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v1, v0, :cond_1

    .line 7
    .line 8
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;

    .line 9
    .line 10
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;->〇0000OOO(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p2

    .line 14
    new-instance v0, Lcom/intsig/camscanner/pdf/preshare/〇O00;

    .line 15
    .line 16
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/pdf/preshare/〇O00;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    if-eqz p2, :cond_0

    .line 27
    .line 28
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;->o〇〇0〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;)Landroid/widget/ImageView;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const p2, 0x7f080f2d

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 36
    .line 37
    .line 38
    goto/16 :goto_3

    .line 39
    .line 40
    :cond_0
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;->o〇〇0〇(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfBannerHolder;)Landroid/widget/ImageView;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    const p2, 0x7f080f2e

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 48
    .line 49
    .line 50
    goto/16 :goto_3

    .line 51
    .line 52
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;

    .line 53
    .line 54
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O000(I)Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 59
    .line 60
    invoke-direct {p0, v2, v0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇8oOO88(Landroid/content/Context;Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->getPath()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O8o08O8O:Landroid/content/Context;

    .line 68
    .line 69
    invoke-static {v2}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    invoke-virtual {v2, v0}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    new-instance v2, Lcom/bumptech/glide/request/RequestOptions;

    .line 78
    .line 79
    invoke-direct {v2}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 80
    .line 81
    .line 82
    sget-object v3, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 83
    .line 84
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    check-cast v2, Lcom/bumptech/glide/request/RequestOptions;

    .line 89
    .line 90
    invoke-virtual {v2, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->O8O〇(Z)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    invoke-virtual {v0, v2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    iget-object v2, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->o〇00O:Landroid/widget/ImageView;

    .line 99
    .line 100
    invoke-virtual {v0, v2}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 101
    .line 102
    .line 103
    if-nez p2, :cond_2

    .line 104
    .line 105
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->ooo0〇〇O:Z

    .line 106
    .line 107
    if-nez v0, :cond_2

    .line 108
    .line 109
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->O0(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 110
    .line 111
    .line 112
    :cond_2
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 113
    .line 114
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8〇OO0〇0o:Z

    .line 115
    .line 116
    invoke-direct {p0, v0, v2, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->Oo〇O(ZZLcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 117
    .line 118
    .line 119
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 120
    .line 121
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇〇o8(ZLcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 122
    .line 123
    .line 124
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O:Z

    .line 125
    .line 126
    const/4 v2, 0x0

    .line 127
    if-nez p2, :cond_3

    .line 128
    .line 129
    const/4 v3, 0x1

    .line 130
    goto :goto_0

    .line 131
    :cond_3
    const/4 v3, 0x0

    .line 132
    :goto_0
    invoke-direct {p0, v0, v3, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o8O〇(ZZLcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 133
    .line 134
    .line 135
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->O8o08O8O:Lcom/intsig/camscanner/topic/view/WatermarkView;

    .line 136
    .line 137
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 138
    .line 139
    invoke-direct {p0, v0, v3}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->OOO8o〇〇(Lcom/intsig/camscanner/topic/view/WatermarkView;Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 140
    .line 141
    .line 142
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO:Landroid/view/View;

    .line 143
    .line 144
    new-instance v3, Lcom/intsig/camscanner/pdf/preshare/〇〇8O0〇8;

    .line 145
    .line 146
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/pdf/preshare/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    .line 151
    .line 152
    invoke-direct {p0, p2, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->o〇O(ILcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 153
    .line 154
    .line 155
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇0O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 156
    .line 157
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->〇O00()V

    .line 158
    .line 159
    .line 160
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇0O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 161
    .line 162
    const/16 v3, 0x8

    .line 163
    .line 164
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 165
    .line 166
    .line 167
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇0O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 168
    .line 169
    new-instance v4, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$1;

    .line 170
    .line 171
    invoke-direct {v4, p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$1;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v0, v4}, Lcom/airbnb/lottie/LottieAnimationView;->O8(Landroid/animation/Animator$AnimatorListener;)V

    .line 175
    .line 176
    .line 177
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇o0O:I

    .line 178
    .line 179
    if-lez v0, :cond_6

    .line 180
    .line 181
    const/4 v4, 0x2

    .line 182
    if-eq v0, v4, :cond_5

    .line 183
    .line 184
    const/4 v4, 0x3

    .line 185
    if-eq v0, v4, :cond_4

    .line 186
    .line 187
    const v0, 0x800003

    .line 188
    .line 189
    .line 190
    goto :goto_1

    .line 191
    :cond_4
    const v0, 0x800005

    .line 192
    .line 193
    .line 194
    goto :goto_1

    .line 195
    :cond_5
    const/4 v0, 0x1

    .line 196
    :goto_1
    iget-object v4, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 197
    .line 198
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 199
    .line 200
    .line 201
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 202
    .line 203
    add-int/lit8 v4, p2, 0x1

    .line 204
    .line 205
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v4

    .line 209
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    .line 211
    .line 212
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 213
    .line 214
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 215
    .line 216
    .line 217
    goto :goto_2

    .line 218
    :cond_6
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 219
    .line 220
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 221
    .line 222
    .line 223
    :goto_2
    iget-object v0, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇〇08O:Landroid/widget/LinearLayout;

    .line 224
    .line 225
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 226
    .line 227
    .line 228
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇0O〇Oo(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;)V

    .line 229
    .line 230
    .line 231
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇08〇o0O:Z

    .line 232
    .line 233
    if-eqz v0, :cond_7

    .line 234
    .line 235
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇〇o〇:Z

    .line 236
    .line 237
    if-nez v0, :cond_7

    .line 238
    .line 239
    if-nez p2, :cond_7

    .line 240
    .line 241
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;->〇〇o〇:Z

    .line 242
    .line 243
    iget-object p1, p1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇0O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 244
    .line 245
    new-instance p2, Lcom/intsig/camscanner/pdf/preshare/〇0〇O0088o;

    .line 246
    .line 247
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pdf/preshare/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;)V

    .line 248
    .line 249
    .line 250
    const-wide/16 v0, 0x1f4

    .line 251
    .line 252
    invoke-virtual {p1, p2, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 253
    .line 254
    .line 255
    :cond_7
    :goto_3
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->o〇0()Z

    .line 256
    .line 257
    .line 258
    move-result p1

    .line 259
    if-eqz p1, :cond_8

    .line 260
    .line 261
    const-string p1, "CSPdfPreview"

    .line 262
    .line 263
    const-string p2, "remove_watermark"

    .line 264
    .line 265
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    .line 267
    .line 268
    :cond_8
    return-void
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.class public Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "ZoomRecyclerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ZoomScrollLayoutManager;,
        Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$GestureListener;,
        Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;
    }
.end annotation


# instance fields
.field private O0O:F

.field private O88O:F

.field private O8o08O8O:F

.field private OO:F

.field private OO〇00〇8oO:Z

.field private o0:Landroid/view/ScaleGestureDetector;

.field private o8o:F

.field private o8oOOo:F

.field private o8〇OO0〇0o:Z

.field private oOO〇〇:F

.field private oOo0:F

.field private oOo〇8o008:F

.field private oo8ooo8O:F

.field private ooo0〇〇O:Z

.field private o〇00O:F

.field private o〇oO:I

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:F

.field private 〇0O:I

.field private 〇8〇oO〇〇8o:Z

.field private 〇OOo8〇0:Landroid/view/GestureDetector;

.field private 〇O〇〇O8:F

.field private 〇o0O:F

.field private 〇〇08O:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0O:I

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO〇00〇8oO:Z

    const/4 v0, 0x1

    .line 4
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8〇OO0〇0o:Z

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->ooo0〇〇O:Z

    .line 6
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇oo〇(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 7
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p3, -0x1

    .line 8
    iput p3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0O:I

    const/4 p3, 0x0

    .line 9
    iput-boolean p3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO〇00〇8oO:Z

    const/4 p3, 0x1

    .line 10
    iput-boolean p3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8〇OO0〇0o:Z

    .line 11
    iput-boolean p3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->ooo0〇〇O:Z

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇oo〇(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O88O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O8ooOoo〇(FF)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇08O:Landroid/animation/ValueAnimator;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇00()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇08O:Landroid/animation/ValueAnimator;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO:F

    .line 18
    .line 19
    mul-float v1, v0, p2

    .line 20
    .line 21
    sub-float/2addr v0, v1

    .line 22
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O〇〇O8:F

    .line 23
    .line 24
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇08O〇00〇o:F

    .line 25
    .line 26
    mul-float v1, v0, p2

    .line 27
    .line 28
    sub-float/2addr v0, v1

    .line 29
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇o0O:F

    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇00O:F

    .line 32
    .line 33
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8o08O8O:F

    .line 34
    .line 35
    sub-float v2, p2, p1

    .line 36
    .line 37
    iget v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O0O:F

    .line 38
    .line 39
    mul-float v3, v3, v2

    .line 40
    .line 41
    sub-float v3, v0, v3

    .line 42
    .line 43
    iget v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8oOOo:F

    .line 44
    .line 45
    mul-float v2, v2, v4

    .line 46
    .line 47
    sub-float v2, v1, v2

    .line 48
    .line 49
    invoke-direct {p0, v3, v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oo88o8O(FF)[F

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    const/4 v3, 0x0

    .line 54
    aget v4, v2, v3

    .line 55
    .line 56
    const/4 v5, 0x1

    .line 57
    aget v2, v2, v5

    .line 58
    .line 59
    const/4 v6, 0x2

    .line 60
    new-array v7, v6, [F

    .line 61
    .line 62
    aput p1, v7, v3

    .line 63
    .line 64
    aput p2, v7, v5

    .line 65
    .line 66
    const-string p1, "scale"

    .line 67
    .line 68
    invoke-static {p1, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    new-array p2, v6, [F

    .line 73
    .line 74
    aput v0, p2, v3

    .line 75
    .line 76
    aput v4, p2, v5

    .line 77
    .line 78
    const-string v0, "tranX"

    .line 79
    .line 80
    invoke-static {v0, p2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    .line 81
    .line 82
    .line 83
    move-result-object p2

    .line 84
    new-array v0, v6, [F

    .line 85
    .line 86
    aput v1, v0, v3

    .line 87
    .line 88
    aput v2, v0, v5

    .line 89
    .line 90
    const-string v1, "tranY"

    .line 91
    .line 92
    invoke-static {v1, v0}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇08O:Landroid/animation/ValueAnimator;

    .line 97
    .line 98
    const/4 v2, 0x3

    .line 99
    new-array v2, v2, [Landroid/animation/PropertyValuesHolder;

    .line 100
    .line 101
    aput-object p1, v2, v3

    .line 102
    .line 103
    aput-object p2, v2, v5

    .line 104
    .line 105
    aput-object v0, v2, v6

    .line 106
    .line 107
    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 108
    .line 109
    .line 110
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇08O:Landroid/animation/ValueAnimator;

    .line 111
    .line 112
    iget p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇oO:I

    .line 113
    .line 114
    int-to-long v0, p2

    .line 115
    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 116
    .line 117
    .line 118
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇08O:Landroid/animation/ValueAnimator;

    .line 119
    .line 120
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    .line 121
    .line 122
    .line 123
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇00O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOO〇〇:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;FF)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O〇8O8〇008(FF)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO〇00〇8oO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇8O8〇008(FF)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇00O:F

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8o08O8O:F

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;FF)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8ooOoo〇(FF)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8oOOo:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oo88o8O(FF)[F
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 2
    .line 3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    const/4 v3, 0x0

    .line 7
    const/4 v4, 0x2

    .line 8
    cmpg-float v0, v0, v1

    .line 9
    .line 10
    if-gtz v0, :cond_0

    .line 11
    .line 12
    new-array v0, v4, [F

    .line 13
    .line 14
    aput p1, v0, v3

    .line 15
    .line 16
    aput p2, v0, v2

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    cmpl-float v1, p1, v0

    .line 21
    .line 22
    if-lez v1, :cond_1

    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    goto :goto_0

    .line 26
    :cond_1
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O〇〇O8:F

    .line 27
    .line 28
    cmpg-float v5, p1, v1

    .line 29
    .line 30
    if-gez v5, :cond_2

    .line 31
    .line 32
    move p1, v1

    .line 33
    :cond_2
    :goto_0
    cmpl-float v1, p2, v0

    .line 34
    .line 35
    if-lez v1, :cond_3

    .line 36
    .line 37
    const/4 p2, 0x0

    .line 38
    goto :goto_1

    .line 39
    :cond_3
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇o0O:F

    .line 40
    .line 41
    cmpg-float v1, p2, v0

    .line 42
    .line 43
    if-gez v1, :cond_4

    .line 44
    .line 45
    move p2, v0

    .line 46
    :cond_4
    :goto_1
    new-array v0, v4, [F

    .line 47
    .line 48
    aput p1, v0, v3

    .line 49
    .line 50
    aput p2, v0, v2

    .line 51
    .line 52
    return-object v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8o:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o〇O8〇〇o(Landroid/animation/ValueAnimator;)V
    .locals 2

    .line 1
    const-string v0, "scale"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Float;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 14
    .line 15
    const-string v0, "tranX"

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Ljava/lang/Float;

    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const-string v1, "tranY"

    .line 28
    .line 29
    invoke-virtual {p1, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    check-cast p1, Ljava/lang/Float;

    .line 34
    .line 35
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O〇8O8〇008(FF)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇00()V
    .locals 2

    .line 1
    new-instance v0, Landroid/animation/ValueAnimator;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇08O:Landroid/animation/ValueAnimator;

    .line 7
    .line 8
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    .line 9
    .line 10
    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇08O:Landroid/animation/ValueAnimator;

    .line 17
    .line 18
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/o〇8oOO88;

    .line 19
    .line 20
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/o〇8oOO88;-><init>(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇08O:Landroid/animation/ValueAnimator;

    .line 27
    .line 28
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$1;

    .line 29
    .line 30
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$1;-><init>(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇O8〇〇o(Landroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8o08O8O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O0O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O888o0o()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇00O:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8o08O8O:F

    .line 4
    .line 5
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oo88o8O(FF)[F

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    aget v1, v0, v1

    .line 11
    .line 12
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇00O:F

    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    aget v0, v0, v1

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8o08O8O:F

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇08O〇00〇o:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇o0O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO〇00〇8oO:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oo〇(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ZoomScrollLayoutManager;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ZoomScrollLayoutManager;-><init>(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Landroid/view/ScaleGestureDetector;

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;-><init>(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;Lcom/intsig/camscanner/pdf/preshare/oO00OOO;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o0:Landroid/view/ScaleGestureDetector;

    .line 21
    .line 22
    new-instance v0, Landroid/view/GestureDetector;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$GestureListener;

    .line 25
    .line 26
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$GestureListener;-><init>(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;Lcom/intsig/camscanner/pdf/preshare/o〇O;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇OOo8〇0:Landroid/view/GestureDetector;

    .line 33
    .line 34
    const/16 v0, 0x12c

    .line 35
    .line 36
    const/high16 v1, 0x3f800000    # 1.0f

    .line 37
    .line 38
    const/high16 v2, 0x40000000    # 2.0f

    .line 39
    .line 40
    const/high16 v3, 0x3f000000    # 0.5f

    .line 41
    .line 42
    const/high16 v4, 0x40800000    # 4.0f

    .line 43
    .line 44
    if-eqz p2, :cond_0

    .line 45
    .line 46
    sget-object v5, Lcom/intsig/camscanner/R$styleable;->ZoomRecyclerView:[I

    .line 47
    .line 48
    const/4 v6, 0x0

    .line 49
    invoke-virtual {p1, p2, v5, v6, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    const/4 p2, 0x3

    .line 54
    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    .line 55
    .line 56
    .line 57
    move-result p2

    .line 58
    iput p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8o:F

    .line 59
    .line 60
    const/4 p2, 0x1

    .line 61
    invoke-virtual {p1, p2, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    .line 62
    .line 63
    .line 64
    move-result p2

    .line 65
    iput p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O88O:F

    .line 66
    .line 67
    const/4 p2, 0x2

    .line 68
    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    .line 69
    .line 70
    .line 71
    move-result p2

    .line 72
    iput p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOO〇〇:F

    .line 73
    .line 74
    invoke-virtual {p1, v6, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    .line 75
    .line 76
    .line 77
    move-result p2

    .line 78
    iput p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oo8ooo8O:F

    .line 79
    .line 80
    const/4 p2, 0x4

    .line 81
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    .line 82
    .line 83
    .line 84
    move-result p2

    .line 85
    iput p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇oO:I

    .line 86
    .line 87
    iget p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oo8ooo8O:F

    .line 88
    .line 89
    iput p2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 90
    .line 91
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_0
    iput v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O88O:F

    .line 96
    .line 97
    iput v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8o:F

    .line 98
    .line 99
    iput v2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOO〇〇:F

    .line 100
    .line 101
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oo8ooo8O:F

    .line 102
    .line 103
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 104
    .line 105
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇oO:I

    .line 106
    .line 107
    :goto_0
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oo8ooo8O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O〇〇O8:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O0O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8oOOo:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇00O:F

    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8o08O8O:F

    .line 7
    .line 8
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 9
    .line 10
    .line 11
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 12
    .line 13
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 14
    .line 15
    .line 16
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :catch_0
    move-exception p1

    .line 24
    const-string v0, "ZoomRecyclerView"

    .line 25
    .line 26
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->ooo0〇〇O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    return p1

    .line 7
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO:F

    .line 7
    .line 8
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    int-to-float v0, v0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇08O〇00〇o:F

    .line 14
    .line 15
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->onMeasure(II)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o0:Landroid/view/ScaleGestureDetector;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇OOo8〇0:Landroid/view/GestureDetector;

    .line 17
    .line 18
    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    const/4 v2, 0x1

    .line 23
    const/4 v3, 0x0

    .line 24
    if-nez v1, :cond_2

    .line 25
    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 v0, 0x0

    .line 30
    goto :goto_1

    .line 31
    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 32
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    if-eqz v1, :cond_9

    .line 37
    .line 38
    if-eq v1, v2, :cond_8

    .line 39
    .line 40
    const/4 v4, 0x2

    .line 41
    if-eq v1, v4, :cond_5

    .line 42
    .line 43
    const/4 v4, 0x3

    .line 44
    if-eq v1, v4, :cond_8

    .line 45
    .line 46
    const/4 v4, 0x6

    .line 47
    if-eq v1, v4, :cond_3

    .line 48
    .line 49
    goto/16 :goto_3

    .line 50
    .line 51
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    iget v5, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0O:I

    .line 60
    .line 61
    if-ne v4, v5, :cond_a

    .line 62
    .line 63
    if-nez v1, :cond_4

    .line 64
    .line 65
    const/4 v1, 0x1

    .line 66
    goto :goto_2

    .line 67
    :cond_4
    const/4 v1, 0x0

    .line 68
    :goto_2
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    iput v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo〇8o008:F

    .line 73
    .line 74
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    .line 75
    .line 76
    .line 77
    move-result v4

    .line 78
    iput v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo0:F

    .line 79
    .line 80
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0O:I

    .line 85
    .line 86
    iput-boolean v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇8〇oO〇〇8o:Z

    .line 87
    .line 88
    goto/16 :goto_3

    .line 89
    .line 90
    :cond_5
    const/high16 v1, 0x3f800000    # 1.0f

    .line 91
    .line 92
    :try_start_0
    iget v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0O:I

    .line 93
    .line 94
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    .line 95
    .line 96
    .line 97
    move-result v4

    .line 98
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    .line 99
    .line 100
    .line 101
    move-result v5

    .line 102
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    .line 103
    .line 104
    .line 105
    move-result v4

    .line 106
    iget-boolean v6, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO〇00〇8oO:Z

    .line 107
    .line 108
    if-nez v6, :cond_6

    .line 109
    .line 110
    iget v6, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 111
    .line 112
    cmpl-float v6, v6, v1

    .line 113
    .line 114
    if-lez v6, :cond_6

    .line 115
    .line 116
    iget v6, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo〇8o008:F

    .line 117
    .line 118
    sub-float v6, v5, v6

    .line 119
    .line 120
    iget v7, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo0:F

    .line 121
    .line 122
    sub-float v7, v4, v7

    .line 123
    .line 124
    iget v8, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇00O:F

    .line 125
    .line 126
    add-float/2addr v8, v6

    .line 127
    iget v6, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8o08O8O:F

    .line 128
    .line 129
    add-float/2addr v6, v7

    .line 130
    invoke-direct {p0, v8, v6}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O〇8O8〇008(FF)V

    .line 131
    .line 132
    .line 133
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O888o0o()V

    .line 134
    .line 135
    .line 136
    :cond_6
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 137
    .line 138
    .line 139
    iput v5, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo〇8o008:F

    .line 140
    .line 141
    iput v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo0:F

    .line 142
    .line 143
    iput-boolean v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇8〇oO〇〇8o:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .line 145
    goto :goto_3

    .line 146
    :catch_0
    nop

    .line 147
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 148
    .line 149
    .line 150
    move-result v4

    .line 151
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 152
    .line 153
    .line 154
    move-result v5

    .line 155
    iget-boolean v6, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO〇00〇8oO:Z

    .line 156
    .line 157
    if-nez v6, :cond_7

    .line 158
    .line 159
    iget v6, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 160
    .line 161
    cmpl-float v1, v6, v1

    .line 162
    .line 163
    if-lez v1, :cond_7

    .line 164
    .line 165
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇8〇oO〇〇8o:Z

    .line 166
    .line 167
    if-nez v1, :cond_7

    .line 168
    .line 169
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo〇8o008:F

    .line 170
    .line 171
    sub-float v1, v4, v1

    .line 172
    .line 173
    iget v6, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo0:F

    .line 174
    .line 175
    sub-float v6, v5, v6

    .line 176
    .line 177
    iget v7, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇00O:F

    .line 178
    .line 179
    add-float/2addr v7, v1

    .line 180
    iget v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8o08O8O:F

    .line 181
    .line 182
    add-float/2addr v1, v6

    .line 183
    invoke-direct {p0, v7, v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O〇8O8〇008(FF)V

    .line 184
    .line 185
    .line 186
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O888o0o()V

    .line 187
    .line 188
    .line 189
    :cond_7
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 190
    .line 191
    .line 192
    iput v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo〇8o008:F

    .line 193
    .line 194
    iput v5, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo0:F

    .line 195
    .line 196
    iput-boolean v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇8〇oO〇〇8o:Z

    .line 197
    .line 198
    goto :goto_3

    .line 199
    :cond_8
    const/4 v1, -0x1

    .line 200
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0O:I

    .line 201
    .line 202
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇8〇oO〇〇8o:Z

    .line 203
    .line 204
    goto :goto_3

    .line 205
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    .line 206
    .line 207
    .line 208
    move-result v1

    .line 209
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    .line 210
    .line 211
    .line 212
    move-result v4

    .line 213
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    .line 214
    .line 215
    .line 216
    move-result v1

    .line 217
    iput v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo〇8o008:F

    .line 218
    .line 219
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oOo0:F

    .line 220
    .line 221
    iput-boolean v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇8〇oO〇〇8o:Z

    .line 222
    .line 223
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    .line 224
    .line 225
    .line 226
    move-result v1

    .line 227
    iput v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0O:I

    .line 228
    .line 229
    :cond_a
    :goto_3
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 230
    .line 231
    .line 232
    move-result p1

    .line 233
    if-nez p1, :cond_c

    .line 234
    .line 235
    if-eqz v0, :cond_b

    .line 236
    .line 237
    goto :goto_4

    .line 238
    :cond_b
    const/4 v2, 0x0

    .line 239
    :cond_c
    :goto_4
    return v2
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public setEnableScale(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o8〇OO0〇0o:Z

    .line 7
    .line 8
    if-nez p1, :cond_1

    .line 9
    .line 10
    iget p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 11
    .line 12
    const/high16 v0, 0x3f800000    # 1.0f

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/utils/CommonUtil;->〇〇8O0〇8(FF)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-nez p1, :cond_1

    .line 19
    .line 20
    iget p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇080OO8〇0:F

    .line 21
    .line 22
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8ooOoo〇(FF)V

    .line 23
    .line 24
    .line 25
    :cond_1
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setEnableTouch(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->ooo0〇〇O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

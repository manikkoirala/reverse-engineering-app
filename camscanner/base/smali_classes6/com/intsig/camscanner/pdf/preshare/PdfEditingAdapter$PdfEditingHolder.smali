.class Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;
.super Lcom/intsig/adapter/BaseViewHolder;
.source "PdfEditingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PdfEditingHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/adapter/BaseViewHolder<",
        "Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;",
        ">;"
    }
.end annotation


# instance fields
.field O8o08O8O:Lcom/intsig/camscanner/topic/view/WatermarkView;

.field OO:Landroid/view/View;

.field OO〇00〇8oO:Landroid/widget/TextView;

.field o8〇OO0〇0o:Landroid/widget/FrameLayout;

.field oOo0:Landroid/widget/ImageView;

.field oOo〇8o008:Landroid/widget/ImageView;

.field ooo0〇〇O:Landroid/view/ViewStub;

.field o〇00O:Landroid/widget/ImageView;

.field 〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

.field 〇08O〇00〇o:Landroid/widget/ImageView;

.field 〇0O:Lcom/airbnb/lottie/LottieAnimationView;

.field 〇8〇oO〇〇8o:Landroid/view/ViewStub;

.field 〇〇08O:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/adapter/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇0000OOO(Landroid/view/View;)V

    .line 5
    .line 6
    .line 7
    const v0, 0x7f0a09da

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    check-cast v0, Landroid/widget/ImageView;

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->o〇00O:Landroid/widget/ImageView;

    .line 17
    .line 18
    const v0, 0x7f0a1ac8

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/intsig/camscanner/topic/view/WatermarkView;

    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->O8o08O8O:Lcom/intsig/camscanner/topic/view/WatermarkView;

    .line 28
    .line 29
    const v0, 0x7f0a0e68

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇0O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 39
    .line 40
    const v0, 0x7f0a0e69

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    check-cast v0, Landroid/widget/TextView;

    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 50
    .line 51
    const v0, 0x7f0a0619

    .line 52
    .line 53
    .line 54
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    check-cast v0, Landroid/widget/FrameLayout;

    .line 59
    .line 60
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->o8〇OO0〇0o:Landroid/widget/FrameLayout;

    .line 61
    .line 62
    const v0, 0x7f0a1aa5

    .line 63
    .line 64
    .line 65
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    check-cast v0, Landroid/view/ViewStub;

    .line 70
    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇8〇oO〇〇8o:Landroid/view/ViewStub;

    .line 72
    .line 73
    const v0, 0x7f0a1aa4

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    check-cast v0, Landroid/view/ViewStub;

    .line 81
    .line 82
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->ooo0〇〇O:Landroid/view/ViewStub;

    .line 83
    .line 84
    const v0, 0x7f0a0c96

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    check-cast p1, Landroid/widget/LinearLayout;

    .line 92
    .line 93
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇〇08O:Landroid/widget/LinearLayout;

    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇0000OOO(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const v0, 0x7f0a1a42

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Landroid/view/ViewStub;

    .line 9
    .line 10
    const v1, 0x7f0a1a41

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Landroid/view/ViewStub;

    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->o〇0()Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const v2, 0x7f0d0471

    .line 24
    .line 25
    .line 26
    if-eqz v1, :cond_2

    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->〇o〇()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    const/4 v1, 0x1

    .line 33
    if-eq v0, v1, :cond_1

    .line 34
    .line 35
    const/4 v1, 0x2

    .line 36
    if-eq v0, v1, :cond_0

    .line 37
    .line 38
    invoke-virtual {p1, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    goto :goto_1

    .line 46
    :cond_0
    const v0, 0x7f0d0476

    .line 47
    .line 48
    .line 49
    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    goto :goto_1

    .line 57
    :cond_1
    const v0, 0x7f0d0475

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    goto :goto_1

    .line 68
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->OO0o〇〇〇〇0()Z

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    if-eqz v1, :cond_3

    .line 73
    .line 74
    invoke-virtual {p1, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    goto :goto_1

    .line 82
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇80〇808〇O()Z

    .line 83
    .line 84
    .line 85
    move-result v1

    .line 86
    if-eqz v1, :cond_5

    .line 87
    .line 88
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇o00〇〇Oo()I

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    const/4 v3, 0x3

    .line 93
    if-eq v1, v3, :cond_4

    .line 94
    .line 95
    invoke-virtual {p1, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    goto :goto_1

    .line 103
    :cond_4
    const p1, 0x7f0d0477

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    goto :goto_1

    .line 114
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0〇〇00()I

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    if-nez v0, :cond_6

    .line 119
    .line 120
    const v0, 0x7f0d0472

    .line 121
    .line 122
    .line 123
    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_6
    const v0, 0x7f0d0473

    .line 128
    .line 129
    .line 130
    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 131
    .line 132
    .line 133
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    :goto_1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->OO:Landroid/view/View;

    .line 138
    .line 139
    const v0, 0x7f0a09d9

    .line 140
    .line 141
    .line 142
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    check-cast v0, Landroid/widget/ImageView;

    .line 147
    .line 148
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 149
    .line 150
    const v0, 0x7f0a04e9

    .line 151
    .line 152
    .line 153
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 154
    .line 155
    .line 156
    move-result-object v0

    .line 157
    check-cast v0, Lcom/intsig/comm/widget/CustomTextView;

    .line 158
    .line 159
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->〇080OO8〇0:Lcom/intsig/comm/widget/CustomTextView;

    .line 160
    .line 161
    const v0, 0x7f0a0aab

    .line 162
    .line 163
    .line 164
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    check-cast v0, Landroid/widget/ImageView;

    .line 169
    .line 170
    iput-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 171
    .line 172
    const v0, 0x7f0a08fe

    .line 173
    .line 174
    .line 175
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    check-cast p1, Landroid/widget/ImageView;

    .line 180
    .line 181
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->oOo0:Landroid/widget/ImageView;

    .line 182
    .line 183
    return-void
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method


# virtual methods
.method public bridge synthetic O〇8O8〇008(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingAdapter$PdfEditingHolder;->o〇〇0〇(Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇〇0〇(Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;I)V
    .locals 0
    .param p1    # Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const-string p1, "PdfEditingAdapter"

    .line 2
    .line 3
    const-string p2, "setData"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

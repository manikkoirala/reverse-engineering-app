.class Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;
.super Ljava/lang/Object;
.source "ZoomRecyclerView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;Lcom/intsig/camscanner/pdf/preshare/oO00OOO;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;-><init>(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 8
    .line 9
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    mul-float v2, v2, v3

    .line 18
    .line 19
    invoke-static {v1, v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0〇O0088o(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 23
    .line 24
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o〇0(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 29
    .line 30
    invoke-static {v3}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 35
    .line 36
    invoke-static {v4}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->O8(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    invoke-static {v1, v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇0〇O0088o(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 49
    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 52
    .line 53
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO0o〇〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 58
    .line 59
    invoke-static {v3}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO0o〇〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 64
    .line 65
    invoke-static {v4}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    mul-float v3, v3, v4

    .line 70
    .line 71
    sub-float/2addr v2, v3

    .line 72
    invoke-static {v1, v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇808〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 73
    .line 74
    .line 75
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 76
    .line 77
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O8o08O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 82
    .line 83
    invoke-static {v3}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O8o08O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 88
    .line 89
    invoke-static {v4}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    mul-float v3, v3, v4

    .line 94
    .line 95
    sub-float/2addr v2, v3

    .line 96
    invoke-static {v1, v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 97
    .line 98
    .line 99
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 100
    .line 101
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    invoke-static {v1, v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O00(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 106
    .line 107
    .line 108
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 109
    .line 110
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    .line 111
    .line 112
    .line 113
    move-result p1

    .line 114
    invoke-static {v1, p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇8O0〇8(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 115
    .line 116
    .line 117
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 118
    .line 119
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇888(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 120
    .line 121
    .line 122
    move-result p1

    .line 123
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 124
    .line 125
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    sub-float v1, v0, v1

    .line 130
    .line 131
    mul-float p1, p1, v1

    .line 132
    .line 133
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 134
    .line 135
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oO80(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 140
    .line 141
    invoke-static {v2}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 142
    .line 143
    .line 144
    move-result v2

    .line 145
    sub-float/2addr v0, v2

    .line 146
    mul-float v1, v1, v0

    .line 147
    .line 148
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 149
    .line 150
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 151
    .line 152
    .line 153
    move-result v2

    .line 154
    add-float/2addr v2, p1

    .line 155
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 156
    .line 157
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇8o8o〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 158
    .line 159
    .line 160
    move-result p1

    .line 161
    add-float/2addr p1, v1

    .line 162
    invoke-static {v0, v2, p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OoO8(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;FF)V

    .line 163
    .line 164
    .line 165
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 166
    .line 167
    const/4 v0, 0x1

    .line 168
    invoke-static {p1, v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->Oooo8o0〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;Z)V

    .line 169
    .line 170
    .line 171
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 172
    .line 173
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 174
    .line 175
    .line 176
    return v0
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇o〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    cmpg-float p1, p1, v0

    .line 14
    .line 15
    if-gtz p1, :cond_2

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 18
    .line 19
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    neg-float v0, v0

    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 25
    .line 26
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    const/high16 v2, 0x3f800000    # 1.0f

    .line 31
    .line 32
    sub-float/2addr v1, v2

    .line 33
    div-float/2addr v0, v1

    .line 34
    invoke-static {p1, v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O00(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 38
    .line 39
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇8o8o〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    neg-float v0, v0

    .line 44
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 45
    .line 46
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    sub-float/2addr v1, v2

    .line 51
    div-float/2addr v0, v1

    .line 52
    invoke-static {p1, v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇8O0〇8(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 56
    .line 57
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇888(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    const/4 v1, 0x0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    .line 68
    const/4 v0, 0x0

    .line 69
    goto :goto_0

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 71
    .line 72
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇888(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    :goto_0
    invoke-static {p1, v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇O00(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 77
    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 80
    .line 81
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oO80(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-eqz v0, :cond_1

    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 93
    .line 94
    invoke-static {v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->oO80(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    :goto_1
    invoke-static {p1, v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇〇8O0〇8(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;F)V

    .line 99
    .line 100
    .line 101
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 102
    .line 103
    invoke-static {p1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇80〇808〇O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 104
    .line 105
    .line 106
    move-result v0

    .line 107
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 108
    .line 109
    invoke-static {v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->〇o〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;)F

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->o800o8O(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;FF)V

    .line 114
    .line 115
    .line 116
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView$ScaleListener;->o0:Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;

    .line 117
    .line 118
    const/4 v0, 0x0

    .line 119
    invoke-static {p1, v0}, Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;->Oooo8o0〇(Lcom/intsig/camscanner/pdf/preshare/ZoomRecyclerView;Z)V

    .line 120
    .line 121
    .line 122
    return-void
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

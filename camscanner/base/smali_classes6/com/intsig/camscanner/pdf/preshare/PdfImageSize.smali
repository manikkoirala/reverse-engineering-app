.class public Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;
.super Ljava/lang/Object;
.source "PdfImageSize.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private imageHeight:I

.field private imageWidth:I

.field public modifiedTime:Ljava/lang/String;

.field private pageHeight:I

.field private pageId:J

.field public pageSyncId:Ljava/lang/String;

.field private pageWidth:I

.field private path:Ljava/lang/String;

.field private pdfEnhanceImage:Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;IIII)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-wide p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageId:J

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->path:Ljava/lang/String;

    .line 5
    iput p4, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->imageWidth:I

    .line 6
    iput p5, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->imageHeight:I

    .line 7
    iput p6, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageWidth:I

    .line 8
    iput p7, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageHeight:I

    return-void
.end method


# virtual methods
.method public getImageHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->imageHeight:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getImageWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->imageWidth:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageHeight:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageId()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageId:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageWidth:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->path:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPdfEnhanceImage()Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pdfEnhanceImage:Lcom/intsig/camscanner/pdf/bean/PdfEnhanceImage;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setImageHeight(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->imageHeight:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setImageWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->imageWidth:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPageHeight(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageHeight:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPageId(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageId:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPageWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->pageWidth:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/preshare/PdfImageSize;->path:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

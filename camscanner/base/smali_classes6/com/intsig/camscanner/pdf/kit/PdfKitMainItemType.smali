.class public final enum Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;
.super Ljava/lang/Enum;
.source "PdfKitMainItemType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum ADD_PAGE_NUMBER:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum COMPRESSION:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum CS_PDF_APP:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum EXTRACT:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum FILL_FROM:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum IMAGE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum LONG_PICTURE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum MERGE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum MOVE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum NO_CS_WATERMARK:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum PASSWORD:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum PDF_TO_EXCEL:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum PDF_TO_PPT:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum PDF_TO_WORD:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum SIGNATURE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

.field public static final enum WATER:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;


# instance fields
.field private excludeEncryptedPdf:Z

.field private logAgentActionId:Ljava/lang/String;

.field private singleSelection:Z

.field private specialSubmitRes:I

.field private titleRes:I

.field private useOnlyOneEnhance:Z


# direct methods
.method static constructor <clinit>()V
    .locals 36

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 2
    .line 3
    const v1, 0x7f130768

    .line 4
    .line 5
    .line 6
    const-string v2, "transfer_word"

    .line 7
    .line 8
    const-string v3, "PDF_TO_WORD"

    .line 9
    .line 10
    const/4 v4, 0x0

    .line 11
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->PDF_TO_WORD:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 15
    .line 16
    new-instance v1, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 17
    .line 18
    const v2, 0x7f1309a8

    .line 19
    .line 20
    .line 21
    const-string v3, "pdf_to_excel"

    .line 22
    .line 23
    const-string v5, "PDF_TO_EXCEL"

    .line 24
    .line 25
    const/4 v6, 0x1

    .line 26
    invoke-direct {v1, v5, v6, v2, v3}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 27
    .line 28
    .line 29
    sput-object v1, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->PDF_TO_EXCEL:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 30
    .line 31
    new-instance v2, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 32
    .line 33
    const v3, 0x7f1309a9

    .line 34
    .line 35
    .line 36
    const-string v5, "pdf_to_ppt"

    .line 37
    .line 38
    const-string v7, "PDF_TO_PPT"

    .line 39
    .line 40
    const/4 v8, 0x2

    .line 41
    invoke-direct {v2, v7, v8, v3, v5}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 42
    .line 43
    .line 44
    sput-object v2, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->PDF_TO_PPT:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 45
    .line 46
    new-instance v3, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 47
    .line 48
    const v5, 0x7f130805

    .line 49
    .line 50
    .line 51
    const-string v7, "signature"

    .line 52
    .line 53
    const-string v9, "SIGNATURE"

    .line 54
    .line 55
    const/4 v10, 0x3

    .line 56
    invoke-direct {v3, v9, v10, v5, v7}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 57
    .line 58
    .line 59
    sput-object v3, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->SIGNATURE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 60
    .line 61
    new-instance v5, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 62
    .line 63
    const v7, 0x7f130808

    .line 64
    .line 65
    .line 66
    const-string v9, "watermark"

    .line 67
    .line 68
    const-string v11, "WATER"

    .line 69
    .line 70
    const/4 v12, 0x4

    .line 71
    invoke-direct {v5, v11, v12, v7, v9}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 72
    .line 73
    .line 74
    sput-object v5, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->WATER:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 75
    .line 76
    new-instance v7, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 77
    .line 78
    const-string v14, "IMAGE"

    .line 79
    .line 80
    const/4 v15, 0x5

    .line 81
    const v16, 0x7f1307ff

    .line 82
    .line 83
    .line 84
    const/16 v17, 0x1

    .line 85
    .line 86
    const/16 v18, 0x1

    .line 87
    .line 88
    const-string v19, "transfer_pic"

    .line 89
    .line 90
    move-object v13, v7

    .line 91
    invoke-direct/range {v13 .. v19}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IIZZLjava/lang/String;)V

    .line 92
    .line 93
    .line 94
    sput-object v7, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->IMAGE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 95
    .line 96
    new-instance v9, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 97
    .line 98
    const v11, 0x7f130800

    .line 99
    .line 100
    .line 101
    const-string v13, "transfer_long_pic"

    .line 102
    .line 103
    const-string v14, "LONG_PICTURE"

    .line 104
    .line 105
    const/4 v15, 0x6

    .line 106
    invoke-direct {v9, v14, v15, v11, v13}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 107
    .line 108
    .line 109
    sput-object v9, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->LONG_PICTURE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 110
    .line 111
    new-instance v11, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 112
    .line 113
    const v13, 0x7f1307fd

    .line 114
    .line 115
    .line 116
    const-string v14, "compress"

    .line 117
    .line 118
    const-string v15, "COMPRESSION"

    .line 119
    .line 120
    const/4 v12, 0x7

    .line 121
    invoke-direct {v11, v15, v12, v13, v14}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 122
    .line 123
    .line 124
    sput-object v11, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->COMPRESSION:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 125
    .line 126
    new-instance v13, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 127
    .line 128
    const-string v19, "MERGE"

    .line 129
    .line 130
    const/16 v20, 0x8

    .line 131
    .line 132
    const v21, 0x7f130801

    .line 133
    .line 134
    .line 135
    const/16 v22, 0x0

    .line 136
    .line 137
    const/16 v23, 0x0

    .line 138
    .line 139
    const/16 v24, 0x0

    .line 140
    .line 141
    const v25, 0x7f1307fa

    .line 142
    .line 143
    .line 144
    const-string v26, "merge"

    .line 145
    .line 146
    move-object/from16 v18, v13

    .line 147
    .line 148
    invoke-direct/range {v18 .. v26}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IIZZZILjava/lang/String;)V

    .line 149
    .line 150
    .line 151
    sput-object v13, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->MERGE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 152
    .line 153
    new-instance v14, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 154
    .line 155
    const v15, 0x7f1307fe

    .line 156
    .line 157
    .line 158
    const-string v12, "extract"

    .line 159
    .line 160
    const-string v10, "EXTRACT"

    .line 161
    .line 162
    const/16 v8, 0x9

    .line 163
    .line 164
    invoke-direct {v14, v10, v8, v15, v12}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 165
    .line 166
    .line 167
    sput-object v14, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->EXTRACT:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 168
    .line 169
    new-instance v10, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 170
    .line 171
    const v12, 0x7f130b2c

    .line 172
    .line 173
    .line 174
    const-string v15, "adjust_page"

    .line 175
    .line 176
    const-string v8, "MOVE"

    .line 177
    .line 178
    const/16 v6, 0xa

    .line 179
    .line 180
    invoke-direct {v10, v8, v6, v12, v15}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 181
    .line 182
    .line 183
    sput-object v10, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->MOVE:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 184
    .line 185
    new-instance v8, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 186
    .line 187
    const v12, 0x7f130b2b

    .line 188
    .line 189
    .line 190
    const-string v15, "fill_from"

    .line 191
    .line 192
    const-string v6, "FILL_FROM"

    .line 193
    .line 194
    const/16 v4, 0xb

    .line 195
    .line 196
    invoke-direct {v8, v6, v4, v12, v15}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 197
    .line 198
    .line 199
    sput-object v8, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->FILL_FROM:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 200
    .line 201
    new-instance v6, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 202
    .line 203
    const v12, 0x7f130b2d

    .line 204
    .line 205
    .line 206
    const-string v15, "no_cs_watermark"

    .line 207
    .line 208
    const-string v4, "NO_CS_WATERMARK"

    .line 209
    .line 210
    move-object/from16 v26, v8

    .line 211
    .line 212
    const/16 v8, 0xc

    .line 213
    .line 214
    invoke-direct {v6, v4, v8, v12, v15}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 215
    .line 216
    .line 217
    sput-object v6, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->NO_CS_WATERMARK:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 218
    .line 219
    new-instance v4, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 220
    .line 221
    const v12, 0x7f1309a3

    .line 222
    .line 223
    .line 224
    const-string v15, "add_page_number"

    .line 225
    .line 226
    const-string v8, "ADD_PAGE_NUMBER"

    .line 227
    .line 228
    move-object/from16 v27, v6

    .line 229
    .line 230
    const/16 v6, 0xd

    .line 231
    .line 232
    invoke-direct {v4, v8, v6, v12, v15}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 233
    .line 234
    .line 235
    sput-object v4, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->ADD_PAGE_NUMBER:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 236
    .line 237
    new-instance v8, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 238
    .line 239
    const-string v29, "PASSWORD"

    .line 240
    .line 241
    const/16 v30, 0xe

    .line 242
    .line 243
    const v31, 0x7f130670

    .line 244
    .line 245
    .line 246
    const/16 v32, 0x1

    .line 247
    .line 248
    const/16 v33, 0x0

    .line 249
    .line 250
    const/16 v34, 0x1

    .line 251
    .line 252
    const-string v35, "encryption"

    .line 253
    .line 254
    move-object/from16 v28, v8

    .line 255
    .line 256
    invoke-direct/range {v28 .. v35}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IIZZZLjava/lang/String;)V

    .line 257
    .line 258
    .line 259
    sput-object v8, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->PASSWORD:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 260
    .line 261
    new-instance v12, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 262
    .line 263
    const v15, 0x7f131148

    .line 264
    .line 265
    .line 266
    const-string v6, ""

    .line 267
    .line 268
    move-object/from16 v29, v8

    .line 269
    .line 270
    const-string v8, "CS_PDF_APP"

    .line 271
    .line 272
    move-object/from16 v30, v4

    .line 273
    .line 274
    const/16 v4, 0xf

    .line 275
    .line 276
    invoke-direct {v12, v8, v4, v15, v6}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 277
    .line 278
    .line 279
    sput-object v12, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->CS_PDF_APP:Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 280
    .line 281
    const/16 v6, 0x10

    .line 282
    .line 283
    new-array v6, v6, [Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 284
    .line 285
    const/4 v8, 0x0

    .line 286
    aput-object v0, v6, v8

    .line 287
    .line 288
    const/4 v0, 0x1

    .line 289
    aput-object v1, v6, v0

    .line 290
    .line 291
    const/4 v0, 0x2

    .line 292
    aput-object v2, v6, v0

    .line 293
    .line 294
    const/4 v0, 0x3

    .line 295
    aput-object v3, v6, v0

    .line 296
    .line 297
    const/4 v0, 0x4

    .line 298
    aput-object v5, v6, v0

    .line 299
    .line 300
    const/4 v0, 0x5

    .line 301
    aput-object v7, v6, v0

    .line 302
    .line 303
    const/4 v0, 0x6

    .line 304
    aput-object v9, v6, v0

    .line 305
    .line 306
    const/4 v0, 0x7

    .line 307
    aput-object v11, v6, v0

    .line 308
    .line 309
    const/16 v0, 0x8

    .line 310
    .line 311
    aput-object v13, v6, v0

    .line 312
    .line 313
    const/16 v0, 0x9

    .line 314
    .line 315
    aput-object v14, v6, v0

    .line 316
    .line 317
    const/16 v0, 0xa

    .line 318
    .line 319
    aput-object v10, v6, v0

    .line 320
    .line 321
    const/16 v0, 0xb

    .line 322
    .line 323
    aput-object v26, v6, v0

    .line 324
    .line 325
    const/16 v0, 0xc

    .line 326
    .line 327
    aput-object v27, v6, v0

    .line 328
    .line 329
    const/16 v0, 0xd

    .line 330
    .line 331
    aput-object v30, v6, v0

    .line 332
    .line 333
    const/16 v0, 0xe

    .line 334
    .line 335
    aput-object v29, v6, v0

    .line 336
    .line 337
    aput-object v12, v6, v4

    .line 338
    .line 339
    sput-object v6, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->$VALUES:[Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 340
    .line 341
    return-void
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v6, p4

    .line 1
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IIZZLjava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZZLjava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v7, p6

    .line 2
    invoke-direct/range {v0 .. v7}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IIZZZLjava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZZZILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZZI",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5
    iput p3, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->titleRes:I

    .line 6
    iput-boolean p4, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->singleSelection:Z

    .line 7
    iput-boolean p5, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->useOnlyOneEnhance:Z

    .line 8
    iput-boolean p6, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->excludeEncryptedPdf:Z

    .line 9
    iput p7, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->specialSubmitRes:I

    .line 10
    iput-object p8, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->logAgentActionId:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZZZLjava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v8, p7

    .line 3
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;-><init>(Ljava/lang/String;IIZZZILjava/lang/String;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->$VALUES:[Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public excludeEncryptedPdf()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->excludeEncryptedPdf:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLogAgentActionId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->logAgentActionId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSpecialSubmitRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->specialSubmitRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTitleRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->titleRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public singleSelection()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->singleSelection:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public useOnlyOneEnhance()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/kit/PdfKitMainItemType;->useOnlyOneEnhance:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

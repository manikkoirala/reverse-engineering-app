.class public final Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;
.super Ljava/lang/Object;
.source "CsPdfRiver.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;,
        Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$WhenMappings;,
        Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇O8o08O:Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private oO80:Z

.field private final o〇0:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8o8o〇:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇888:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O8o08O:Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function2;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroidx/fragment/app/FragmentActivity;",
            "-",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "entry"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o〇:Lkotlin/jvm/functions/Function0;

    .line 19
    .line 20
    iput-object p4, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->O8:Lkotlin/jvm/functions/Function0;

    .line 21
    .line 22
    iput-object p5, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->Oo08:Lkotlin/jvm/functions/Function0;

    .line 23
    .line 24
    iput-object p6, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->o〇0:Lkotlin/jvm/functions/Function2;

    .line 25
    .line 26
    iput-object p7, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇〇888:Ljava/lang/String;

    .line 27
    .line 28
    iput-boolean p8, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->oO80:Z

    .line 29
    .line 30
    const-string p1, "CsPdfRiver"

    .line 31
    .line 32
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 33
    .line 34
    new-instance p1, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$actionAppStoreDefault$1;

    .line 35
    .line 36
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$actionAppStoreDefault$1;-><init>(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function0;

    .line 40
    .line 41
    new-instance p1, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$actionAppDefault$1;

    .line 42
    .line 43
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$actionAppDefault$1;-><init>(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)V

    .line 44
    .line 45
    .line 46
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇8o8o〇:Lkotlin/jvm/functions/Function0;

    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)Lkotlin/jvm/functions/Function0;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO0o〇〇〇〇0()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->oO80:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-virtual {v0, v1, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OoO8(Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;)V
    .locals 3

    .line 1
    iget v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;->open_landpage:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v0, "  river open land page"

    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O888o0o()V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 37
    .line 38
    new-instance v2, Ljava/lang/StringBuilder;

    .line 39
    .line 40
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v1, "  river no land page"

    .line 47
    .line 48
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->o800o8O(Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;)V

    .line 59
    .line 60
    .line 61
    :goto_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final Oooo8o0〇()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-class v0, Lcom/intsig/webview/WebViewActivity;

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const-class v0, Lcom/intsig/webview/TransWebActivity;

    .line 11
    .line 12
    :goto_0
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o800o8O(Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;)V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->O8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_2

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->oo88o8O()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    iget p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;->open_app_no_page:I

    .line 15
    .line 16
    if-ne p1, v1, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 22
    .line 23
    new-instance v1, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v0, "  river key not open "

    .line 32
    .line 33
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o〇:Lkotlin/jvm/functions/Function0;

    .line 44
    .line 45
    if-eqz p1, :cond_4

    .line 46
    .line 47
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇〇888()V

    .line 52
    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/pdf/pdfriver/RiverCacheHelper;->〇080:Lcom/intsig/camscanner/pdf/pdfriver/RiverCacheHelper;

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 58
    .line 59
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pdf/pdfriver/RiverCacheHelper;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    add-int/2addr v2, v1

    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->oo88o8O()Z

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    if-nez v1, :cond_3

    .line 69
    .line 70
    iget p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;->open_app_store_times:I

    .line 71
    .line 72
    if-le p1, v2, :cond_3

    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 75
    .line 76
    new-instance v3, Ljava/lang/StringBuilder;

    .line 77
    .line 78
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .line 80
    .line 81
    const-string v4, "skipTime="

    .line 82
    .line 83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    const-string v2, " cfgTime ="

    .line 90
    .line 91
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 105
    .line 106
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdf/pdfriver/RiverCacheHelper;->〇080(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)V

    .line 107
    .line 108
    .line 109
    iget-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o〇:Lkotlin/jvm/functions/Function0;

    .line 110
    .line 111
    if-eqz p1, :cond_4

    .line 112
    .line 113
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 114
    .line 115
    .line 116
    goto :goto_1

    .line 117
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->oO80()V

    .line 118
    .line 119
    .line 120
    :cond_4
    :goto_1
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final oO80()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->Oo08:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "go to app store"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function0;

    .line 13
    .line 14
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 19
    .line 20
    const-string v1, "go to default action app store"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->Oo08:Lkotlin/jvm/functions/Function0;

    .line 26
    .line 27
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oo88o8O()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->PdfTarBar:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)Lorg/json/JSONObject;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇〇808〇()Lorg/json/JSONObject;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇〇888()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇80〇808〇O()Ljava/lang/String;
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/utils/WebUrlUtils;->〇080:Lcom/intsig/utils/WebUrlUtils;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/utils/WebUrlUtils;->〇00()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-static {}, Lcom/intsig/utils/WebUrlUtils;->〇O00()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v0, "app/diversion"

    .line 27
    .line 28
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->O8()Z

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    if-eqz v1, :cond_1

    .line 48
    .line 49
    const-string v1, "1"

    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_1
    const-string v1, "0"

    .line 53
    .line 54
    :goto_1
    const-string v2, "cs_pdf_install"

    .line 55
    .line 56
    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 65
    .line 66
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇〇888:Ljava/lang/String;

    .line 67
    .line 68
    new-instance v3, Ljava/lang/StringBuilder;

    .line 69
    .line 70
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .line 72
    .line 73
    const-string v4, "land page url:"

    .line 74
    .line 75
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    const-string v4, " title="

    .line 82
    .line 83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    const-string v1, "uri.toString()"

    .line 101
    .line 102
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    return-object v0
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇O00()Z
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 10
    .line 11
    sget-object v2, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->PdfAppEdit:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 12
    .line 13
    if-eq v0, v2, :cond_1

    .line 14
    .line 15
    sget-object v2, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->PdfTarBar:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 16
    .line 17
    if-eq v0, v2, :cond_1

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    :cond_1
    return v1
    .line 21
.end method

.method private final 〇O888o0o()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->Oooo8o0〇()Ljava/lang/Class;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10
    .line 11
    .line 12
    const-string v1, "targeturl"

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 19
    .line 20
    .line 21
    const-string v1, "extra_key_nav_icon_drawable"

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O〇()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 28
    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇〇888:Ljava/lang/String;

    .line 31
    .line 32
    if-eqz v1, :cond_0

    .line 33
    .line 34
    const-string v2, "tagetkfkalabel"

    .line 35
    .line 36
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    const-string v1, "islabelfix"

    .line 40
    .line 41
    const/4 v2, 0x1

    .line 42
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 43
    .line 44
    .line 45
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇〇808〇()Lorg/json/JSONObject;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    if-eqz v1, :cond_1

    .line 50
    .line 51
    const-string v2, "CSImportPdfWeb"

    .line 52
    .line 53
    invoke-static {v2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 54
    .line 55
    .line 56
    :cond_1
    new-instance v1, Lcom/intsig/result/GetActivityResult;

    .line 57
    .line 58
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 59
    .line 60
    invoke-direct {v1, v2}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 61
    .line 62
    .line 63
    const/16 v2, 0xa

    .line 64
    .line 65
    invoke-virtual {v1, v0, v2}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    new-instance v1, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$startRiverWithLandPage$3;

    .line 70
    .line 71
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$startRiverWithLandPage$3;-><init>(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0, v1}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇O〇()I
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/base/ToolbarThemeGet;->〇080:Lcom/intsig/base/ToolbarThemeGet;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/base/ToolbarThemeGet;->〇o00〇〇Oo()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, 0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const v0, 0x7f080b93

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const v0, 0x7f080b94

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const v0, 0x7f0806a9

    .line 25
    .line 26
    .line 27
    :goto_0
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->oO80()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇oo〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$WhenMappings;->〇080:[I

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    aget v0, v1, v0

    .line 10
    .line 11
    packed-switch v0, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    goto :goto_0

    .line 16
    :pswitch_0
    const/4 v0, 0x1

    .line 17
    :goto_0
    return v0

    .line 18
    nop

    .line 19
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->OO0o〇〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇808〇()Lorg/json/JSONObject;
    .locals 5

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->getGpStoreTrackId()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const-string v2, "refer_source"

    .line 13
    .line 14
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 18
    .line 19
    sget-object v2, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$WhenMappings;->〇080:[I

    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    aget v1, v2, v1

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    const-string v3, "cs_main_application"

    .line 29
    .line 30
    const-string v4, ""

    .line 31
    .line 32
    packed-switch v1, :pswitch_data_0

    .line 33
    .line 34
    .line 35
    move-object v3, v4

    .line 36
    move-object v4, v2

    .line 37
    goto :goto_0

    .line 38
    :pswitch_0
    const-string v3, "cs_detail"

    .line 39
    .line 40
    const-string v4, "add_text"

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :pswitch_1
    const-string v3, "cs_mark_pop"

    .line 44
    .line 45
    const-string v4, "mark_up"

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :pswitch_2
    const-string v4, "doc_pic_up"

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :pswitch_3
    const-string v4, "doc_revise"

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :pswitch_4
    const-string v4, "doc_merge"

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :pswitch_5
    const-string v4, "doc_signature"

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :pswitch_6
    const-string v4, "import_doc"

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :pswitch_7
    move-object v3, v4

    .line 64
    goto :goto_0

    .line 65
    :pswitch_8
    const-string v3, "cs_home"

    .line 66
    .line 67
    const-string v4, "pdf_tool"

    .line 68
    .line 69
    :goto_0
    if-nez v4, :cond_0

    .line 70
    .line 71
    return-object v2

    .line 72
    :cond_0
    const-string v1, "from"

    .line 73
    .line 74
    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 75
    .line 76
    .line 77
    const-string v1, "from_part"

    .line 78
    .line 79
    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 80
    .line 81
    .line 82
    return-object v0

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇〇888()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->O8:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "go to app"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇8o8o〇:Lkotlin/jvm/functions/Function0;

    .line 13
    .line 14
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 19
    .line 20
    const-string v1, "go to default action app"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->O8:Lkotlin/jvm/functions/Function0;

    .line 26
    .line 27
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final 〇〇8O0〇8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Z
    .locals 1
    .param p0    # Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O8o08O:Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Z

    .line 4
    .line 5
    .line 6
    move-result p0

    .line 7
    return p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final OO0o〇〇()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->o〇0:Lkotlin/jvm/functions/Function2;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getActivity()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0〇O0088o()V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O8o08O:Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇080(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 14
    .line 15
    new-instance v2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v1, " cfg is null"

    .line 24
    .line 25
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o〇:Lkotlin/jvm/functions/Function0;

    .line 36
    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    :cond_0
    return-void

    .line 43
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 44
    .line 45
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 46
    .line 47
    iget v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;->open_river:I

    .line 48
    .line 49
    iget v4, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;->open_landpage:I

    .line 50
    .line 51
    iget v5, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;->open_app_store_times:I

    .line 52
    .line 53
    new-instance v6, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    const-string v2, " cfg open = "

    .line 62
    .line 63
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v2, ",landpage="

    .line 70
    .line 71
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string v2, " ,times="

    .line 78
    .line 79
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    iget v1, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;->open_river:I

    .line 93
    .line 94
    const/4 v2, 0x1

    .line 95
    if-ne v1, v2, :cond_2

    .line 96
    .line 97
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 98
    .line 99
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 100
    .line 101
    new-instance v3, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    const-string v2, " start river"

    .line 110
    .line 111
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v2

    .line 118
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->OoO8(Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;)V

    .line 122
    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇80〇808〇O:Ljava/lang/String;

    .line 126
    .line 127
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 128
    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    .line 130
    .line 131
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    const-string v1, " not open river"

    .line 138
    .line 139
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o〇:Lkotlin/jvm/functions/Function0;

    .line 150
    .line 151
    if-eqz v0, :cond_3

    .line 152
    .line 153
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    :cond_3
    :goto_0
    return-void
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇8o8o〇()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o〇:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O8o08O()Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

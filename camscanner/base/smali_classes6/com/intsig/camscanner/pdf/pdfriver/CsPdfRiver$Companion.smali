.class public final Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;
.super Ljava/lang/Object;
.source "CsPdfRiver.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion$WhenMappings;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion$WhenMappings;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget p1, v0, p1

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    packed-switch p1, :pswitch_data_0

    .line 11
    .line 12
    .line 13
    goto/16 :goto_0

    .line 14
    .line 15
    :pswitch_0
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    goto/16 :goto_0

    .line 26
    .line 27
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 32
    .line 33
    if-eqz p1, :cond_c

    .line 34
    .line 35
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->cslist_mark_add_text:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 36
    .line 37
    goto/16 :goto_0

    .line 38
    .line 39
    :pswitch_1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    goto/16 :goto_0

    .line 50
    .line 51
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 56
    .line 57
    if-eqz p1, :cond_c

    .line 58
    .line 59
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->cslist_mark:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 60
    .line 61
    goto/16 :goto_0

    .line 62
    .line 63
    :pswitch_2
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    if-eqz p1, :cond_2

    .line 68
    .line 69
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    goto/16 :goto_0

    .line 74
    .line 75
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 80
    .line 81
    if-eqz p1, :cond_c

    .line 82
    .line 83
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->extract_pdf_pages:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 84
    .line 85
    goto/16 :goto_0

    .line 86
    .line 87
    :pswitch_3
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 88
    .line 89
    .line 90
    move-result p1

    .line 91
    if-eqz p1, :cond_3

    .line 92
    .line 93
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    goto/16 :goto_0

    .line 98
    .line 99
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 104
    .line 105
    if-eqz p1, :cond_c

    .line 106
    .line 107
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->reorder_pdf_pages:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 108
    .line 109
    goto/16 :goto_0

    .line 110
    .line 111
    :pswitch_4
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 112
    .line 113
    .line 114
    move-result p1

    .line 115
    if-eqz p1, :cond_4

    .line 116
    .line 117
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    goto/16 :goto_0

    .line 122
    .line 123
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 128
    .line 129
    if-eqz p1, :cond_c

    .line 130
    .line 131
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->merge_pdfs:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 132
    .line 133
    goto/16 :goto_0

    .line 134
    .line 135
    :pswitch_5
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 136
    .line 137
    .line 138
    move-result p1

    .line 139
    if-eqz p1, :cond_5

    .line 140
    .line 141
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    goto/16 :goto_0

    .line 146
    .line 147
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 148
    .line 149
    .line 150
    move-result-object p1

    .line 151
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 152
    .line 153
    if-eqz p1, :cond_c

    .line 154
    .line 155
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->cs_sign:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 156
    .line 157
    goto/16 :goto_0

    .line 158
    .line 159
    :pswitch_6
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 160
    .line 161
    .line 162
    move-result p1

    .line 163
    if-eqz p1, :cond_6

    .line 164
    .line 165
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 166
    .line 167
    .line 168
    move-result-object v0

    .line 169
    goto/16 :goto_0

    .line 170
    .line 171
    :cond_6
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 176
    .line 177
    if-eqz p1, :cond_c

    .line 178
    .line 179
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->import_files:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 180
    .line 181
    goto/16 :goto_0

    .line 182
    .line 183
    :pswitch_7
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 184
    .line 185
    .line 186
    move-result p1

    .line 187
    if-eqz p1, :cond_7

    .line 188
    .line 189
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    goto/16 :goto_0

    .line 194
    .line 195
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 200
    .line 201
    if-eqz p1, :cond_c

    .line 202
    .line 203
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->send_with_pdf:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 204
    .line 205
    goto/16 :goto_0

    .line 206
    .line 207
    :pswitch_8
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 208
    .line 209
    .line 210
    move-result p1

    .line 211
    if-eqz p1, :cond_8

    .line 212
    .line 213
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 214
    .line 215
    .line 216
    move-result-object v0

    .line 217
    goto/16 :goto_0

    .line 218
    .line 219
    :cond_8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 220
    .line 221
    .line 222
    move-result-object p1

    .line 223
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 224
    .line 225
    if-eqz p1, :cond_c

    .line 226
    .line 227
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->share_via_pdf:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 228
    .line 229
    goto/16 :goto_0

    .line 230
    .line 231
    :pswitch_9
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 232
    .line 233
    .line 234
    move-result p1

    .line 235
    if-eqz p1, :cond_9

    .line 236
    .line 237
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 238
    .line 239
    .line 240
    move-result-object v0

    .line 241
    goto :goto_0

    .line 242
    :cond_9
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 243
    .line 244
    .line 245
    move-result-object p1

    .line 246
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 247
    .line 248
    if-eqz p1, :cond_c

    .line 249
    .line 250
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->pdf_tools_redirect:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 251
    .line 252
    goto :goto_0

    .line 253
    :pswitch_a
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 254
    .line 255
    .line 256
    move-result p1

    .line 257
    if-eqz p1, :cond_a

    .line 258
    .line 259
    invoke-direct {p0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 260
    .line 261
    .line 262
    move-result-object v0

    .line 263
    goto :goto_0

    .line 264
    :cond_a
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 265
    .line 266
    .line 267
    move-result-object p1

    .line 268
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 269
    .line 270
    if-eqz p1, :cond_c

    .line 271
    .line 272
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->pdf_app_edit:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 273
    .line 274
    goto :goto_0

    .line 275
    :pswitch_b
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 276
    .line 277
    .line 278
    move-result-object p1

    .line 279
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 280
    .line 281
    if-eqz p1, :cond_c

    .line 282
    .line 283
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->pagelist_bubble:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 284
    .line 285
    goto :goto_0

    .line 286
    :pswitch_c
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 287
    .line 288
    .line 289
    move-result-object p1

    .line 290
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 291
    .line 292
    if-eqz p1, :cond_c

    .line 293
    .line 294
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->pdf_tarbar:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 295
    .line 296
    goto :goto_0

    .line 297
    :pswitch_d
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 298
    .line 299
    .line 300
    move-result-object p1

    .line 301
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 302
    .line 303
    if-eqz p1, :cond_c

    .line 304
    .line 305
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->pdf_to_jpg:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 306
    .line 307
    goto :goto_0

    .line 308
    :pswitch_e
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 309
    .line 310
    .line 311
    move-result p1

    .line 312
    if-eqz p1, :cond_b

    .line 313
    .line 314
    goto :goto_0

    .line 315
    :cond_b
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 316
    .line 317
    .line 318
    move-result-object p1

    .line 319
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 320
    .line 321
    if-eqz p1, :cond_c

    .line 322
    .line 323
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->open_pdf:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 324
    .line 325
    goto :goto_0

    .line 326
    :pswitch_f
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 327
    .line 328
    .line 329
    move-result-object p1

    .line 330
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->pdf_app_v2:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;

    .line 331
    .line 332
    if-eqz p1, :cond_c

    .line 333
    .line 334
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppEnTryV2;->save_to_cs:Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 335
    .line 336
    :cond_c
    :goto_0
    return-object v0

    .line 337
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇o〇()Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "open_river"

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 10
    .line 11
    .line 12
    const-string v1, "open_landpage"

    .line 13
    .line 14
    const/4 v2, 0x1

    .line 15
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 16
    .line 17
    .line 18
    const-string v1, "open_app_no_page"

    .line 19
    .line 20
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 21
    .line 22
    .line 23
    new-instance v1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 24
    .line 25
    invoke-direct {v1, v0}, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;-><init>(Lorg/json/JSONObject;)V

    .line 26
    .line 27
    .line 28
    return-object v1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Z
    .locals 2
    .param p1    # Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "entry"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const/4 v0, 0x0

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    iget p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PdfAppItem;->open_river:I

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    if-ne p1, v1, :cond_0

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    :cond_0
    return v0
    .line 20
.end method

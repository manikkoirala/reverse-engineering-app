.class public final Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;
.super Ljava/lang/Object;
.source "CsPdfRiver.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CsPdfRiverBuilder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private Oo08:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private oO80:Z

.field private o〇0:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroidx/fragment/app/FragmentActivity;",
            "-",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇888:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    sget-object p1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->SaveToCs:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 12
    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "entry"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "title"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇〇888:Ljava/lang/String;

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final getActivity()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0(Z)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->oO80:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇080(Lkotlin/jvm/functions/Function0;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;
    .locals 1
    .param p1    # Lkotlin/jvm/functions/Function0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "actionCs"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o〇:Lkotlin/jvm/functions/Function0;

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o00〇〇Oo(Lkotlin/jvm/functions/Function2;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;
    .locals 1
    .param p1    # Lkotlin/jvm/functions/Function2;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Landroidx/fragment/app/FragmentActivity;",
            "-",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "pdfPath"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->o〇0:Lkotlin/jvm/functions/Function2;

    .line 7
    .line 8
    return-object p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o〇()Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;
    .locals 10
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o00〇〇Oo:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o〇:Lkotlin/jvm/functions/Function0;

    .line 8
    .line 9
    iget-object v4, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->O8:Lkotlin/jvm/functions/Function0;

    .line 10
    .line 11
    iget-object v5, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->Oo08:Lkotlin/jvm/functions/Function0;

    .line 12
    .line 13
    iget-object v6, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->o〇0:Lkotlin/jvm/functions/Function2;

    .line 14
    .line 15
    iget-object v7, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇〇888:Ljava/lang/String;

    .line 16
    .line 17
    iget-boolean v8, p0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->oO80:Z

    .line 18
    .line 19
    move-object v0, v9

    .line 20
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function2;Ljava/lang/String;Z)V

    .line 21
    .line 22
    .line 23
    return-object v9
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.class public Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;
.super Ljava/lang/Object;
.source "AdAppLaunchActivityLifecycleCallback.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field private O8o08O8O:Z

.field private OO:Z

.field private OO〇00〇8oO:Z

.field private o0:J

.field private oOo0:Landroidx/fragment/app/FragmentManager$FragmentLifecycleCallbacks;

.field private oOo〇8o008:Z

.field private o〇00O:Landroid/app/Application;

.field private 〇080OO8〇0:J

.field private 〇08O〇00〇o:I

.field private 〇0O:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private 〇OOo8〇0:Z


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-wide/16 v0, 0x0

    .line 5
    .line 6
    iput-wide v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o0:J

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    iput-boolean v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇OOo8〇0:Z

    .line 10
    .line 11
    iput-boolean v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO:Z

    .line 12
    .line 13
    iput v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇08O〇00〇o:I

    .line 14
    .line 15
    iput-boolean v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->O8o08O8O:Z

    .line 16
    .line 17
    iput-wide v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇080OO8〇0:J

    .line 18
    .line 19
    iput-boolean v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo〇8o008:Z

    .line 20
    .line 21
    iput-boolean v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO〇00〇8oO:Z

    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o〇00O:Landroid/app/Application;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oO80()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO0o〇〇(Landroid/app/Activity;)Z
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "current activity = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const-string v1, " null "

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "AdAppLaunchActivityLifecycleCallback"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    instance-of v0, p1, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 33
    .line 34
    if-nez v0, :cond_2

    .line 35
    .line 36
    instance-of v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryActivity;

    .line 37
    .line 38
    if-nez v0, :cond_2

    .line 39
    .line 40
    instance-of v0, p1, Lcom/intsig/camscanner/pdf/external/PdfToCsBaseActivity;

    .line 41
    .line 42
    if-nez v0, :cond_2

    .line 43
    .line 44
    instance-of v0, p1, Lcom/intsig/camscanner/pdf/kit/PdfKitMainActivity;

    .line 45
    .line 46
    if-nez v0, :cond_2

    .line 47
    .line 48
    instance-of v0, p1, Lcom/intsig/camscanner/purchase/activity/PurchasePointActivity;

    .line 49
    .line 50
    if-nez v0, :cond_2

    .line 51
    .line 52
    instance-of v0, p1, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 53
    .line 54
    if-nez v0, :cond_2

    .line 55
    .line 56
    instance-of v0, p1, Lcom/intsig/camscanner/ImageScannerActivity;

    .line 57
    .line 58
    if-nez v0, :cond_2

    .line 59
    .line 60
    instance-of v0, p1, Lcom/intsig/camscanner/mode_ocr/BatchOCRPrepareActivity;

    .line 61
    .line 62
    if-nez v0, :cond_2

    .line 63
    .line 64
    instance-of v0, p1, Lcom/intsig/camscanner/ImportSingleOcrActivity;

    .line 65
    .line 66
    if-nez v0, :cond_2

    .line 67
    .line 68
    instance-of v0, p1, Lcom/intsig/camscanner/ImageEditActivity;

    .line 69
    .line 70
    if-nez v0, :cond_2

    .line 71
    .line 72
    instance-of v0, p1, Lcom/intsig/camscanner/image_restore/ImageRestoreIntroductionActivity;

    .line 73
    .line 74
    if-nez v0, :cond_2

    .line 75
    .line 76
    instance-of v0, p1, Lcom/intsig/camscanner/image_restore/ImageRestoreResultActivity;

    .line 77
    .line 78
    if-nez v0, :cond_2

    .line 79
    .line 80
    instance-of v0, p1, Lcom/intsig/camscanner/UpgradeDescriptionActivity;

    .line 81
    .line 82
    if-nez v0, :cond_2

    .line 83
    .line 84
    instance-of v0, p1, Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 85
    .line 86
    if-nez v0, :cond_2

    .line 87
    .line 88
    instance-of v0, p1, Lcom/intsig/camscanner/LikeActivity;

    .line 89
    .line 90
    if-nez v0, :cond_2

    .line 91
    .line 92
    instance-of v0, p1, Lcom/intsig/camscanner/BatchModeActivity;

    .line 93
    .line 94
    if-nez v0, :cond_2

    .line 95
    .line 96
    instance-of v0, p1, Lcom/intsig/tsapp/account/LoginMainActivity;

    .line 97
    .line 98
    if-nez v0, :cond_2

    .line 99
    .line 100
    instance-of v0, p1, Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 101
    .line 102
    if-nez v0, :cond_2

    .line 103
    .line 104
    instance-of v0, p1, Lcom/intsig/camscanner/innovationlab/InnovationLabActivity;

    .line 105
    .line 106
    if-nez v0, :cond_2

    .line 107
    .line 108
    instance-of v0, p1, Lcom/intsig/camscanner/shortcut/CaptureWidgetActivity;

    .line 109
    .line 110
    if-nez v0, :cond_2

    .line 111
    .line 112
    instance-of v0, p1, Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 113
    .line 114
    if-nez v0, :cond_2

    .line 115
    .line 116
    instance-of p1, p1, Lcom/intsig/camscanner/guide/CancelAdShowCnGuidePurchaseActivity;

    .line 117
    .line 118
    if-eqz p1, :cond_1

    .line 119
    .line 120
    goto :goto_1

    .line 121
    :cond_1
    const/4 p1, 0x0

    .line 122
    goto :goto_2

    .line 123
    :cond_2
    :goto_1
    const/4 p1, 0x1

    .line 124
    :goto_2
    return p1
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private OO0o〇〇〇〇0(Landroid/app/Activity;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    instance-of v0, p1, Lcom/intsig/camscanner/capture/CaptureActivity;

    .line 6
    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    instance-of v0, p1, Lcom/intsig/camscanner/ImageScannerActivity;

    .line 10
    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    instance-of v0, p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditPreviewActivity;

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    instance-of v0, p1, Lcom/intsig/camscanner/batch/BatchImageReeditActivity;

    .line 18
    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    instance-of v0, p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultActivity;

    .line 22
    .line 23
    if-nez v0, :cond_1

    .line 24
    .line 25
    instance-of v0, p1, Lcom/intsig/camscanner/booksplitter/BookEditActivity;

    .line 26
    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    instance-of v0, p1, Lcom/intsig/camscanner/booksplitter/BookResultActivity;

    .line 30
    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    instance-of v0, p1, Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity;

    .line 34
    .line 35
    if-nez v0, :cond_1

    .line 36
    .line 37
    instance-of v0, p1, Lcom/intsig/camscanner/topic/TopicPreviewActivity;

    .line 38
    .line 39
    if-nez v0, :cond_1

    .line 40
    .line 41
    instance-of p1, p1, Lcom/intsig/camscanner/printer/PrintHomeActivity;

    .line 42
    .line 43
    if-eqz p1, :cond_0

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    const/4 p1, 0x0

    .line 47
    goto :goto_1

    .line 48
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 49
    :goto_1
    return p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;Landroid/app/Activity;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇〇808〇(Landroid/app/Activity;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private OoO8(Ljava/lang/String;J)V
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    long-to-float p2, p2

    .line 7
    const/high16 p3, 0x3f800000    # 1.0f

    .line 8
    .line 9
    mul-float p2, p2, p3

    .line 10
    .line 11
    const/high16 p3, 0x42c80000    # 100.0f

    .line 12
    .line 13
    div-float/2addr p2, p3

    .line 14
    float-to-double p2, p2

    .line 15
    invoke-static {p2, p3}, Ljava/lang/Math;->ceil(D)D

    .line 16
    .line 17
    .line 18
    move-result-wide p2

    .line 19
    double-to-long p2, p2

    .line 20
    const-wide/16 v1, 0x64

    .line 21
    .line 22
    mul-long p2, p2, v1

    .line 23
    .line 24
    :try_start_0
    const-string v1, "dur"

    .line 25
    .line 26
    invoke-virtual {v0, v1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :catch_0
    move-exception p2

    .line 31
    const-string p3, "AdAppLaunchActivityLifecycleCallback"

    .line 32
    .line 33
    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    .line 35
    .line 36
    :goto_0
    const-string p2, "CSScreenPage"

    .line 37
    .line 38
    invoke-static {p2, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->oo88o8O(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private Oooo8o0〇(Landroid/app/Activity;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string p1, ""

    .line 13
    .line 14
    :goto_0
    const-string v0, "com.intsig.camscanner"

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    return p1
.end method

.method private o800o8O()V
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "launch"

    .line 7
    .line 8
    sget-object v2, Lcom/intsig/advertisement/enums/AppLaunchType;->WarmBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 9
    .line 10
    iget-object v2, v2, Lcom/intsig/advertisement/enums/AppLaunchType;->trackName:Ljava/lang/String;

    .line 11
    .line 12
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 13
    .line 14
    .line 15
    const-string v1, "user_status"

    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->o〇0()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .line 23
    .line 24
    :catch_0
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    sget-object v2, Lcom/intsig/advertisement/enums/PositionType;->AppLaunch:Lcom/intsig/advertisement/enums/PositionType;

    .line 29
    .line 30
    invoke-virtual {v1, v2, v0}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇O00(Lcom/intsig/advertisement/enums/PositionType;Lorg/json/JSONObject;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oO80()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/advertisement/control/AdConfigManager;->〇O〇()Ljava/lang/Boolean;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder$CommandMsg;->ClosePage:Lcom/intsig/camscanner/ads_new/view/TranslationBinder$CommandMsg;

    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o〇0(Lcom/intsig/activity/BaseAppCompatActivity;)V
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇080OO8〇0:J

    .line 6
    .line 7
    new-instance v0, LoO80OOO〇/〇o00〇〇Oo;

    .line 8
    .line 9
    invoke-direct {v0, p0, p1}, LoO80OOO〇/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;Lcom/intsig/activity/BaseAppCompatActivity;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1, v0}, Lcom/intsig/activity/BaseAppCompatActivity;->oooO888(Lcom/intsig/activity/BaseAppCompatActivity$OnDispatchTouchEventListener;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080()V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇O00()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇0〇O0088o(Landroid/app/Activity;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback$1;

    .line 7
    .line 8
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback$1;-><init>(Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇O8o08O(Lcom/intsig/advertisement/listener/OnAdRequestListener;)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇80〇808〇O()Lcom/intsig/advertisement/params/AdRequestOptions;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o800o8O()V

    .line 20
    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->o88〇OO08〇(Lcom/intsig/advertisement/params/AdRequestOptions;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇80〇808〇O()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o08o〇0()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    new-instance v0, LoO80OOO〇/〇080;

    .line 13
    .line 14
    invoke-direct {v0}, LoO80OOO〇/〇080;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method private static synthetic 〇O00()V
    .locals 7

    .line 1
    const-string v0, "AdAppLaunchActivityLifecycleCallback"

    .line 2
    .line 3
    :try_start_0
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {}, Lcom/intsig/camscanner/web/UrlUtil;->O8ooOoo〇()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    sget-object v3, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇:Ljava/lang/String;

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->O8()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v4

    .line 17
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇0()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v5

    .line 21
    const/4 v6, 0x0

    .line 22
    invoke-static/range {v1 .. v6}, Lcom/intsig/tianshu/TianShuAPI;->oO8008O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    if-eqz v2, :cond_0

    .line 37
    .line 38
    const-string v1, "checkOccupationLabel result is empty"

    .line 39
    .line 40
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_0
    :try_start_1
    const-class v2, Lcom/intsig/camscanner/occupation_label/model/OccupationLabelResponse;

    .line 45
    .line 46
    invoke-static {v1, v2}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    check-cast v1, Lcom/intsig/camscanner/occupation_label/model/OccupationLabelResponse;

    .line 51
    .line 52
    if-nez v1, :cond_1

    .line 53
    .line 54
    const-string v1, "checkOccupationLabel occupationLabelResponse == null"

    .line 55
    .line 56
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    return-void

    .line 60
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/camscanner/occupation_label/model/OccupationLabelResponse;->getList()Ljava/util/List;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    if-eqz v1, :cond_7

    .line 65
    .line 66
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 67
    .line 68
    .line 69
    move-result v2

    .line 70
    if-nez v2, :cond_2

    .line 71
    .line 72
    goto :goto_2

    .line 73
    :cond_2
    const/4 v2, 0x0

    .line 74
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    check-cast v1, Lcom/intsig/camscanner/occupation_label/model/OccupationLabelList;

    .line 79
    .line 80
    if-nez v1, :cond_3

    .line 81
    .line 82
    const-string v1, "checkOccupationLabel occupationLabel == null"

    .line 83
    .line 84
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    return-void

    .line 88
    :cond_3
    invoke-virtual {v1}, Lcom/intsig/camscanner/occupation_label/model/OccupationLabelList;->getLabels()Ljava/util/List;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    if-eqz v1, :cond_6

    .line 93
    .line 94
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 95
    .line 96
    .line 97
    move-result v3

    .line 98
    if-nez v3, :cond_4

    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_4
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    check-cast v1, Lcom/intsig/camscanner/occupation_label/model/LabelEntity;

    .line 106
    .line 107
    if-nez v1, :cond_5

    .line 108
    .line 109
    const-string v1, "checkOccupationLabel labelEntity == null"

    .line 110
    .line 111
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    return-void

    .line 115
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 116
    .line 117
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    .line 119
    .line 120
    const-string v3, "checkOccupationLabel tagCode="

    .line 121
    .line 122
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v1}, Lcom/intsig/camscanner/occupation_label/model/LabelEntity;->getTag_code()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v3

    .line 129
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object v2

    .line 136
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    invoke-virtual {v1}, Lcom/intsig/camscanner/occupation_label/model/LabelEntity;->getTag_code()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v1

    .line 143
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0〇〇(Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    goto :goto_3

    .line 147
    :cond_6
    :goto_1
    const-string v1, "checkOccupationLabel labelEntityList == null || labelEntityList.size() == 0"

    .line 148
    .line 149
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    return-void

    .line 153
    :cond_7
    :goto_2
    const-string v1, "checkOccupationLabel occupationLabelList == null || occupationLabelList.size() == 0"

    .line 154
    .line 155
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 156
    .line 157
    .line 158
    return-void

    .line 159
    :catch_1
    move-exception v1

    .line 160
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 161
    .line 162
    .line 163
    :goto_3
    return-void
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇O8o08O()Landroidx/fragment/app/FragmentManager$FragmentLifecycleCallbacks;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo0:Landroidx/fragment/app/FragmentManager$FragmentLifecycleCallbacks;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/launch/CSFragmentLifecycleCallbacks;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/camscanner/launch/CSFragmentLifecycleCallbacks;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo0:Landroidx/fragment/app/FragmentManager$FragmentLifecycleCallbacks;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo0:Landroidx/fragment/app/FragmentManager$FragmentLifecycleCallbacks;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic 〇O〇(Lcom/intsig/activity/BaseAppCompatActivity;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const/4 p2, 0x0

    .line 2
    invoke-virtual {p1, p2}, Lcom/intsig/activity/BaseAppCompatActivity;->oooO888(Lcom/intsig/activity/BaseAppCompatActivity$OnDispatchTouchEventListener;)V

    .line 3
    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->O8o08O8O:Z

    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;Lcom/intsig/activity/BaseAppCompatActivity;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇O〇(Lcom/intsig/activity/BaseAppCompatActivity;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;Lcom/intsig/activity/BaseAppCompatActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o〇0(Lcom/intsig/activity/BaseAppCompatActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇〇808〇(Landroid/app/Activity;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z
    .locals 11

    .line 1
    const-string v0, "AdAppLaunchActivityLifecycleCallback"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz p1, :cond_7

    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    if-nez v2, :cond_7

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    goto/16 :goto_0

    .line 19
    .line 20
    :cond_0
    instance-of v2, p2, Lcom/intsig/advertisement/adapters/sources/xiaomi/XiaoMiSplash;

    .line 21
    .line 22
    if-eqz v2, :cond_1

    .line 23
    .line 24
    const-string p1, "xiao splash or for cache"

    .line 25
    .line 26
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return v1

    .line 30
    :cond_1
    iget-boolean v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO:Z

    .line 31
    .line 32
    if-eqz v2, :cond_2

    .line 33
    .line 34
    const-string p1, " not show ad"

    .line 35
    .line 36
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iput-boolean v1, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO:Z

    .line 40
    .line 41
    return v1

    .line 42
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 43
    .line 44
    .line 45
    move-result-wide v2

    .line 46
    iget-wide v4, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇080OO8〇0:J

    .line 47
    .line 48
    sub-long/2addr v2, v4

    .line 49
    const-string v4, "before_show"

    .line 50
    .line 51
    invoke-direct {p0, v4, v2, v3}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OoO8(Ljava/lang/String;J)V

    .line 52
    .line 53
    .line 54
    iget-boolean v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->O8o08O8O:Z

    .line 55
    .line 56
    if-eqz v2, :cond_3

    .line 57
    .line 58
    iput-boolean v1, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->O8o08O8O:Z

    .line 59
    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 61
    .line 62
    .line 63
    move-result-wide v2

    .line 64
    iget-wide v4, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇080OO8〇0:J

    .line 65
    .line 66
    sub-long/2addr v2, v4

    .line 67
    const-string v4, "click"

    .line 68
    .line 69
    invoke-direct {p0, v4, v2, v3}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OoO8(Ljava/lang/String;J)V

    .line 70
    .line 71
    .line 72
    invoke-static {}, Lcom/intsig/advertisement/control/AdConfigManager;->Oooo8o0〇()Z

    .line 73
    .line 74
    .line 75
    move-result v2

    .line 76
    if-eqz v2, :cond_3

    .line 77
    .line 78
    const-string p1, "user has handle  not show ad"

    .line 79
    .line 80
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return v1

    .line 84
    :cond_3
    if-eqz p2, :cond_5

    .line 85
    .line 86
    invoke-virtual {p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    sget-object v3, Lcom/intsig/advertisement/enums/SourceType;->Admob:Lcom/intsig/advertisement/enums/SourceType;

    .line 95
    .line 96
    if-eq v2, v3, :cond_4

    .line 97
    .line 98
    invoke-virtual {p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    sget-object v3, Lcom/intsig/advertisement/enums/SourceType;->Pangle:Lcom/intsig/advertisement/enums/SourceType;

    .line 107
    .line 108
    if-ne v2, v3, :cond_5

    .line 109
    .line 110
    :cond_4
    invoke-virtual {p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇o00〇〇Oo()Lcom/intsig/advertisement/enums/AdType;

    .line 115
    .line 116
    .line 117
    move-result-object v2

    .line 118
    sget-object v3, Lcom/intsig/advertisement/enums/AdType;->Splash:Lcom/intsig/advertisement/enums/AdType;

    .line 119
    .line 120
    if-ne v2, v3, :cond_5

    .line 121
    .line 122
    move-object v4, p2

    .line 123
    check-cast v4, Lcom/intsig/advertisement/interfaces/SplashRequest;

    .line 124
    .line 125
    const/4 v6, 0x0

    .line 126
    const/4 v7, 0x0

    .line 127
    const/4 v8, 0x0

    .line 128
    const/4 v9, 0x0

    .line 129
    const/4 v10, 0x0

    .line 130
    move-object v5, p1

    .line 131
    invoke-virtual/range {v4 .. v10}, Lcom/intsig/advertisement/interfaces/SplashRequest;->showSplashAd(Landroid/app/Activity;Landroid/widget/RelativeLayout;Landroid/widget/TextView;ILandroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/api/sdk/listener/ResetBootListener;)V

    .line 132
    .line 133
    .line 134
    const-string p1, "show admob splash"

    .line 135
    .line 136
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    return v1

    .line 140
    :cond_5
    instance-of v2, p2, Lcom/intsig/advertisement/interfaces/InterstitialRequest;

    .line 141
    .line 142
    if-eqz v2, :cond_6

    .line 143
    .line 144
    const-string v2, "show  interstitial--"

    .line 145
    .line 146
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    check-cast p2, Lcom/intsig/advertisement/interfaces/InterstitialRequest;

    .line 150
    .line 151
    invoke-virtual {p2, p1}, Lcom/intsig/advertisement/interfaces/InterstitialRequest;->showInterstitialAd(Landroid/content/Context;)V

    .line 152
    .line 153
    .line 154
    return v1

    .line 155
    :cond_6
    const/4 p1, 0x1

    .line 156
    return p1

    .line 157
    :cond_7
    :goto_0
    const-string p1, "requestAd activity is finish"

    .line 158
    .line 159
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    return v1
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private 〇〇888(Landroid/app/Activity;)Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/comm/ad/AdUtils;->〇080:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇08O8o〇0()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-boolean v0, Lcom/intsig/advertisement/control/AdConfigManager;->o〇0:Z

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    sget-boolean v0, Lcom/intsig/advertisement/control/AdConfigManager;->〇〇888:Z

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    iget-boolean v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇OOo8〇0:Z

    .line 20
    .line 21
    if-nez v0, :cond_0

    .line 22
    .line 23
    iget-boolean v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo〇8o008:Z

    .line 24
    .line 25
    if-nez v0, :cond_0

    .line 26
    .line 27
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->Oooo8o0〇(Landroid/app/Activity;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/camscanner/app/Verify;->O8()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-nez v0, :cond_0

    .line 38
    .line 39
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO0o〇〇(Landroid/app/Activity;)Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-nez p1, :cond_0

    .line 44
    .line 45
    const/4 p1, 0x1

    .line 46
    goto :goto_0

    .line 47
    :cond_0
    const/4 p1, 0x0

    .line 48
    :goto_0
    return p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇〇8O0〇8(Landroid/app/Activity;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇〇888(Landroid/app/Activity;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 10
    .line 11
    invoke-direct {v0, p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;-><init>(Landroid/content/Context;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->OO0o〇〇〇〇0()Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇80〇808〇O()Lcom/intsig/advertisement/params/AdRequestOptions;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    sget-object v0, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080:Ljava/lang/String;

    .line 23
    .line 24
    const-string v1, "pre load ad----"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->o88〇OO08〇(Lcom/intsig/advertisement/params/AdRequestOptions;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    const/4 v0, 0x1

    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8〇〇8o()Z

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    if-eqz p2, :cond_1

    .line 13
    .line 14
    :cond_0
    iget-boolean p2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO〇00〇8oO:Z

    .line 15
    .line 16
    if-nez p2, :cond_1

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇80〇808〇O()V

    .line 19
    .line 20
    .line 21
    iput-boolean v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO〇00〇8oO:Z

    .line 22
    .line 23
    :cond_1
    instance-of p2, p1, Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    if-eqz p2, :cond_2

    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-static {p2}, Lcom/intsig/camscanner/CustomExceptionHandler;->O8(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    move-object p2, p1

    .line 39
    check-cast p2, Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    invoke-virtual {p2}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇O8o08O()Landroidx/fragment/app/FragmentManager$FragmentLifecycleCallbacks;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v1, v2, v0}, Landroidx/fragment/app/FragmentManager;->registerFragmentLifecycleCallbacks(Landroidx/fragment/app/FragmentManager$FragmentLifecycleCallbacks;Z)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p2}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    new-instance v1, Lcom/intsig/camscanner/receiver/StorageStateLifecycleObserver;

    .line 57
    .line 58
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/receiver/StorageStateLifecycleObserver;-><init>(Landroid/app/Activity;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p2, v1}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 62
    .line 63
    .line 64
    :cond_2
    instance-of p2, p1, Lcom/intsig/router/CSRouterActivity;

    .line 65
    .line 66
    if-eqz p2, :cond_3

    .line 67
    .line 68
    const-string p2, "AdAppLaunchActivityLifecycleCallback"

    .line 69
    .line 70
    const-string v1, "CSRouterActivity onActivityCreated"

    .line 71
    .line 72
    invoke-static {p2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    iput-boolean v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO:Z

    .line 76
    .line 77
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->〇O888o0o(Landroid/app/Activity;)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    const-string v2, "deeplink"

    .line 90
    .line 91
    invoke-static {v2, p2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    :cond_3
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 95
    .line 96
    if-eqz p2, :cond_4

    .line 97
    .line 98
    iput-boolean v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo〇8o008:Z

    .line 99
    .line 100
    :cond_4
    instance-of p2, p1, Lcom/intsig/camscanner/doodle/DoodleActivity;

    .line 101
    .line 102
    if-nez p2, :cond_5

    .line 103
    .line 104
    instance-of p1, p1, Lcom/intsig/camscanner/doodle/DoodleTextActivity;

    .line 105
    .line 106
    if-eqz p1, :cond_6

    .line 107
    .line 108
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/util/DoodleProxy;->〇〇808〇()V

    .line 109
    .line 110
    .line 111
    :cond_6
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    const-class v0, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    const/4 p1, 0x1

    .line 24
    iput-boolean p1, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇OOo8〇0:Z

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 p1, 0x0

    .line 28
    iput-boolean p1, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇OOo8〇0:Z

    .line 29
    .line 30
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇0O:Ljava/lang/ref/WeakReference;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 7

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    instance-of v0, p1, Lcom/intsig/webview/WebViewActivity;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->OO0o〇〇〇〇0(Landroid/app/Activity;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->oO(Z)V

    .line 18
    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/ocrapi/LocalOcrClient;->〇O8o08O()Lcom/intsig/ocrapi/LocalOcrClient;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v1, v0}, Lcom/intsig/ocrapi/LocalOcrClient;->O8ooOoo〇(Z)V

    .line 25
    .line 26
    .line 27
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇08O〇00〇o:I

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    add-int/2addr v0, v1

    .line 31
    iput v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇08O〇00〇o:I

    .line 32
    .line 33
    const/4 v2, 0x0

    .line 34
    if-ne v0, v1, :cond_9

    .line 35
    .line 36
    sget-object v0, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;->Oo08:Lcom/intsig/camscanner/pagelist/reader/DocReaderManager$Companion;

    .line 37
    .line 38
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager$Companion;->〇080(Z)V

    .line 39
    .line 40
    .line 41
    invoke-static {v1}, Lcom/intsig/utils/ForeBackgroundRecord;->〇〇888(Z)V

    .line 42
    .line 43
    .line 44
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/AdRecordHelper;->oO80()V

    .line 49
    .line 50
    .line 51
    iget-boolean v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo〇8o008:Z

    .line 52
    .line 53
    if-nez v0, :cond_1

    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/camscanner/app/Verify;->O8()Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-nez v0, :cond_1

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o〇00O:Landroid/app/Application;

    .line 62
    .line 63
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->OO0o〇〇〇〇0(Landroid/content/Context;)Z

    .line 64
    .line 65
    .line 66
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    const-string v3, " onActivityStarted = 1  "

    .line 72
    .line 73
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    if-nez p1, :cond_2

    .line 77
    .line 78
    const-string v3, ""

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    :goto_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    const-string v3, "AdAppLaunchActivityLifecycleCallback"

    .line 97
    .line 98
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇〇888(Landroid/app/Activity;)Z

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    if-eqz v0, :cond_6

    .line 106
    .line 107
    invoke-virtual {p0}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇8o8o〇()Landroid/app/Activity;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    instance-of v0, v0, Lcom/intsig/camscanner/guide/CancelAdShowCnGuidePurchaseActivity;

    .line 112
    .line 113
    invoke-static {}, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/AfterScanPremiumManager;->O8()Z

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    if-eqz v4, :cond_3

    .line 118
    .line 119
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇0〇O0088o(Landroid/app/Activity;)V

    .line 120
    .line 121
    .line 122
    goto :goto_1

    .line 123
    :cond_3
    if-nez v0, :cond_5

    .line 124
    .line 125
    invoke-static {}, Lcom/intsig/camscanner/guide/dropchannel/DropCnlShowConfiguration;->o〇0()Z

    .line 126
    .line 127
    .line 128
    move-result v0

    .line 129
    if-eqz v0, :cond_5

    .line 130
    .line 131
    sget-object v0, Lcom/intsig/camscanner/guide/dropchannel/DropCnlShowConfiguration;->〇080:Lcom/intsig/camscanner/guide/dropchannel/DropCnlShowConfiguration;

    .line 132
    .line 133
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/dropchannel/DropCnlShowConfiguration;->〇〇888()Z

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    if-eqz v0, :cond_4

    .line 138
    .line 139
    const-string v0, "show drop cnl purchase page"

    .line 140
    .line 141
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    sget-object v0, Lcom/intsig/camscanner/password_identify/PasswordIdentifyManager;->〇080:Lcom/intsig/camscanner/password_identify/PasswordIdentifyManager;

    .line 145
    .line 146
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/password_identify/PasswordIdentifyManager;->O8ooOoo〇(Z)V

    .line 147
    .line 148
    .line 149
    const/4 v0, -0x1

    .line 150
    invoke-static {p1, v0, v2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->o800o8O(Landroid/app/Activity;II)V

    .line 151
    .line 152
    .line 153
    goto :goto_1

    .line 154
    :cond_4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇0〇O0088o(Landroid/app/Activity;)V

    .line 155
    .line 156
    .line 157
    goto :goto_1

    .line 158
    :cond_5
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇0〇O0088o(Landroid/app/Activity;)V

    .line 159
    .line 160
    .line 161
    :cond_6
    :goto_1
    iget-wide v3, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o0:J

    .line 162
    .line 163
    const-wide/16 v5, 0x0

    .line 164
    .line 165
    cmp-long v0, v3, v5

    .line 166
    .line 167
    if-lez v0, :cond_7

    .line 168
    .line 169
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 170
    .line 171
    .line 172
    move-result-wide v3

    .line 173
    iget-wide v5, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o0:J

    .line 174
    .line 175
    sub-long/2addr v3, v5

    .line 176
    const-wide/32 v5, 0x1b7740

    .line 177
    .line 178
    .line 179
    cmp-long v0, v3, v5

    .line 180
    .line 181
    if-lez v0, :cond_7

    .line 182
    .line 183
    invoke-static {}, Lcom/intsig/advertisement/record/SessionRecorder;->getInstance()Lcom/intsig/advertisement/record/SessionRecorder;

    .line 184
    .line 185
    .line 186
    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/SessionRecorder;->createNewSession()V

    .line 188
    .line 189
    .line 190
    const-wide/32 v5, 0x36ee80

    .line 191
    .line 192
    .line 193
    cmp-long v0, v3, v5

    .line 194
    .line 195
    if-lez v0, :cond_7

    .line 196
    .line 197
    invoke-static {}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇8O0〇8()V

    .line 198
    .line 199
    .line 200
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 201
    .line 202
    .line 203
    move-result-wide v3

    .line 204
    iget-wide v5, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o0:J

    .line 205
    .line 206
    sub-long/2addr v3, v5

    .line 207
    const-wide/16 v5, 0x7d0

    .line 208
    .line 209
    cmp-long v0, v3, v5

    .line 210
    .line 211
    if-lez v0, :cond_8

    .line 212
    .line 213
    sput-boolean v2, Lcom/intsig/comm/ad/AdUtils;->〇080:Z

    .line 214
    .line 215
    :cond_8
    sput-boolean v2, Lcom/intsig/advertisement/control/AdConfigManager;->o〇0:Z

    .line 216
    .line 217
    invoke-static {v2}, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->〇〇0o(Z)V

    .line 218
    .line 219
    .line 220
    goto :goto_2

    .line 221
    :cond_9
    invoke-static {v2}, Lcom/intsig/utils/ForeBackgroundRecord;->〇〇888(Z)V

    .line 222
    .line 223
    .line 224
    :goto_2
    iget-boolean v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo〇8o008:Z

    .line 225
    .line 226
    if-nez v0, :cond_a

    .line 227
    .line 228
    iget v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇08O〇00〇o:I

    .line 229
    .line 230
    if-ne v0, v1, :cond_b

    .line 231
    .line 232
    :cond_a
    invoke-static {}, Lcom/intsig/camscanner/launch/LicenceLoginTipHelper;->〇080()Z

    .line 233
    .line 234
    .line 235
    move-result v0

    .line 236
    if-eqz v0, :cond_b

    .line 237
    .line 238
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooo()V

    .line 239
    .line 240
    .line 241
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇080()V

    .line 242
    .line 243
    .line 244
    new-instance v0, Lcom/intsig/tsapp/account/model/LoginMainArgs;

    .line 245
    .line 246
    invoke-direct {v0}, Lcom/intsig/tsapp/account/model/LoginMainArgs;-><init>()V

    .line 247
    .line 248
    .line 249
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/model/LoginMainArgs;->〇8〇0〇o〇O(Z)V

    .line 250
    .line 251
    .line 252
    invoke-static {p1, v0}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->〇80〇808〇O(Landroid/content/Context;Lcom/intsig/tsapp/account/model/LoginMainArgs;)V

    .line 253
    .line 254
    .line 255
    :cond_b
    iput-boolean v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->oOo〇8o008:Z

    .line 256
    .line 257
    return-void
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇08O〇00〇o:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    sub-int/2addr v0, v1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇08O〇00〇o:I

    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    iget v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇08O〇00〇o:I

    .line 13
    .line 14
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v2, ""

    .line 18
    .line 19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v2, "AdAppLaunchActivityLifecycleCallback"

    .line 27
    .line 28
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇08O〇00〇o:I

    .line 32
    .line 33
    if-nez v0, :cond_2

    .line 34
    .line 35
    sget-boolean v0, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇O〇:Z

    .line 36
    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    sget-object v2, Lcom/intsig/advertisement/enums/AppLaunchType;->BackBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->OO8oO0o〇(Lcom/intsig/advertisement/enums/AppLaunchType;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    sget-object v2, Lcom/intsig/advertisement/enums/AppLaunchType;->WarmBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 54
    .line 55
    invoke-virtual {v0, v2}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->OO8oO0o〇(Lcom/intsig/advertisement/enums/AppLaunchType;)V

    .line 56
    .line 57
    .line 58
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 59
    .line 60
    .line 61
    move-result-wide v2

    .line 62
    iget-wide v4, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o0:J

    .line 63
    .line 64
    sub-long/2addr v2, v4

    .line 65
    const-wide/16 v4, 0xbb8

    .line 66
    .line 67
    cmp-long v0, v2, v4

    .line 68
    .line 69
    if-lez v0, :cond_2

    .line 70
    .line 71
    sget-object v0, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;->Oo08:Lcom/intsig/camscanner/pagelist/reader/DocReaderManager$Companion;

    .line 72
    .line 73
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager$Companion;->〇080(Z)V

    .line 74
    .line 75
    .line 76
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 77
    .line 78
    .line 79
    move-result-wide v2

    .line 80
    iput-wide v2, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o0:J

    .line 81
    .line 82
    invoke-static {}, Lcom/intsig/advertisement/control/AdConfigManager;->OO0o〇〇()Z

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    if-eqz v0, :cond_1

    .line 87
    .line 88
    invoke-static {}, Lcom/intsig/camscanner/app/Verify;->O8()Z

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    if-nez v0, :cond_1

    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->o〇00O:Landroid/app/Application;

    .line 95
    .line 96
    const/4 v2, 0x0

    .line 97
    const/4 v3, 0x0

    .line 98
    invoke-static {v0, v2, v3}, Lcom/intsig/advertisement/control/AdConfigManager;->OoO8(Landroid/content/Context;ZLcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 99
    .line 100
    .line 101
    :cond_1
    invoke-static {v1}, Lcom/intsig/camscanner/launch/CsApplication;->o88〇OO08〇(Z)V

    .line 102
    .line 103
    .line 104
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/AdRecordHelper;->o〇0OOo〇0()V

    .line 109
    .line 110
    .line 111
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇〇8O0〇8(Landroid/app/Activity;)V

    .line 112
    .line 113
    .line 114
    :cond_2
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇8o8o〇()Landroid/app/Activity;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;->〇0O:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Landroid/app/Activity;

    .line 12
    .line 13
    :goto_0
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

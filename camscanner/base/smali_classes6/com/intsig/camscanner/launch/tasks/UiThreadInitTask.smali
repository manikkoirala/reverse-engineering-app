.class public final Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;
.super Lcom/effective/android/anchors/task/Task;
.source "UiThreadInitTask.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$MainPageGrayTask;,
        Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$LoginInfoTask;,
        Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    const-string v0, "TASK_MAIN"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {p0, v0, v1}, Lcom/effective/android/anchors/task/Task;-><init>(Ljava/lang/String;Z)V

    .line 5
    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O8ooOoo〇()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->oo88o8O()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eqz v1, :cond_2

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/utils/WindowUtilsSingleton;->〇0〇O0088o()Lcom/intsig/utils/WindowUtilsSingleton;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8o〇8()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    if-eqz v2, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {v1, v2}, Lcom/intsig/utils/WindowUtilsSingleton;->O〇8O8〇008(Landroid/content/Context;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0〇〇o0()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-nez v2, :cond_1

    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇00()Z

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    if-eqz v2, :cond_2

    .line 37
    .line 38
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {v1, v0}, Lcom/intsig/utils/WindowUtilsSingleton;->O8ooOoo〇(Landroid/content/Context;)V

    .line 43
    .line 44
    .line 45
    :cond_2
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O8〇o(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;Ljava/lang/String;J)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "launchActivityName"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->oo〇(Ljava/lang/String;J)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final OOO〇O0(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;)Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 7
    .line 8
    const v0, 0x7f1304ee

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    return-object p0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o0ooO()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/tianshu/ParamsBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "client"

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO8oO0o〇()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 17
    .line 18
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇0(Landroid/content/Context;)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const-string v3, "client_id"

    .line 23
    .line 24
    invoke-virtual {v1, v3, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 29
    .line 30
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    const-string v3, "client_app"

    .line 35
    .line 36
    invoke-virtual {v1, v3, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    sget-object v2, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇O〇()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    const-string v3, "cs_ept_d"

    .line 47
    .line 48
    invoke-virtual {v1, v3, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    const-string v2, "attribute"

    .line 53
    .line 54
    const-string v3, "update_device"

    .line 55
    .line 56
    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    const-string v2, "country"

    .line 61
    .line 62
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->O8()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    const-string v2, "language"

    .line 71
    .line 72
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇〇888()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 77
    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/CsHosts;->oo88o8O()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v1, "/set_user_attribute"

    .line 92
    .line 93
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    :try_start_0
    invoke-static {v1}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    invoke-virtual {v0}, Lcom/intsig/tianshu/ParamsBuilder;->OO0o〇〇()Lcom/intsig/tianshu/ParamsBuilder;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇080()Ljava/util/Map;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    const/4 v2, 0x0

    .line 113
    new-array v2, v2, [Z

    .line 114
    .line 115
    invoke-virtual {v1, v0, v2}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    check-cast v0, Lcom/lzy/okgo/request/GetRequest;

    .line 120
    .line 121
    invoke-virtual {v0}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .line 123
    .line 124
    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    sget-object v1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 127
    .line 128
    invoke-virtual {v1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇8O0〇8()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 133
    .line 134
    .line 135
    :goto_0
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;Ljava/lang/String;J)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->O8〇o(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;Ljava/lang/String;J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oo〇(Ljava/lang/String;J)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-static {}, Lcom/intsig/launch/GlobalAppLaunchManager;->〇〇888()Lcom/intsig/launch/GlobalAppLaunchManager;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    invoke-virtual {v1}, Lcom/intsig/launch/GlobalAppLaunchManager;->〇80〇808〇O()Z

    .line 7
    .line 8
    .line 9
    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    const-string v2, "user_status"

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    :try_start_1
    const-string p1, "CSStart"

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->o〇0()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    const-string p3, "process"

    .line 21
    .line 22
    sget-object v1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇8o8o〇()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-static {p1, v2, p2, p3, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 37
    .line 38
    .line 39
    move-result-wide v3

    .line 40
    sub-long/2addr v3, p2

    .line 41
    new-instance p2, Landroid/util/Pair;

    .line 42
    .line 43
    const-string p3, "time"

    .line 44
    .line 45
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-direct {p2, p3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 50
    .line 51
    .line 52
    new-instance p3, Landroid/util/Pair;

    .line 53
    .line 54
    const-string v1, "project_page"

    .line 55
    .line 56
    invoke-direct {p3, v1, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 57
    .line 58
    .line 59
    new-instance p1, Landroid/util/Pair;

    .line 60
    .line 61
    const-string v1, "is_exit_physics"

    .line 62
    .line 63
    sget-boolean v3, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇O〇:Z

    .line 64
    .line 65
    if-eqz v3, :cond_1

    .line 66
    .line 67
    const-string v3, "1"

    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_1
    const-string v3, "0"

    .line 71
    .line 72
    :goto_0
    invoke-direct {p1, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 73
    .line 74
    .line 75
    new-instance v1, Landroid/util/Pair;

    .line 76
    .line 77
    const-string v3, "label_num"

    .line 78
    .line 79
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 80
    .line 81
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 82
    .line 83
    .line 84
    move-result-object v4

    .line 85
    invoke-static {v4}, Lcom/intsig/camscanner/db/dao/TagDao;->Oo08(Landroid/content/Context;)I

    .line 86
    .line 87
    .line 88
    move-result v4

    .line 89
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v4

    .line 93
    invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 94
    .line 95
    .line 96
    new-instance v3, Landroid/util/Pair;

    .line 97
    .line 98
    invoke-static {}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->o〇0()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v4

    .line 102
    invoke-direct {v3, v2, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 103
    .line 104
    .line 105
    const-string v2, "CSBackground"

    .line 106
    .line 107
    const-string v4, "warm_boot"

    .line 108
    .line 109
    const/4 v5, 0x5

    .line 110
    new-array v5, v5, [Landroid/util/Pair;

    .line 111
    .line 112
    aput-object p2, v5, v0

    .line 113
    .line 114
    const/4 p2, 0x1

    .line 115
    aput-object p3, v5, p2

    .line 116
    .line 117
    const/4 p2, 0x2

    .line 118
    aput-object v3, v5, p2

    .line 119
    .line 120
    const/4 p2, 0x3

    .line 121
    aput-object v1, v5, p2

    .line 122
    .line 123
    const/4 p2, 0x4

    .line 124
    aput-object p1, v5, p2

    .line 125
    .line 126
    invoke-static {v2, v4, v5}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 127
    .line 128
    .line 129
    goto :goto_1

    .line 130
    :catch_0
    move-exception p1

    .line 131
    sget-object p2, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 132
    .line 133
    invoke-virtual {p2}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇8O0〇8()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object p2

    .line 137
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 138
    .line 139
    .line 140
    :goto_1
    sput-boolean v0, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇O〇:Z

    .line 141
    .line 142
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic o〇O8〇〇o(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o0ooO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇〇0〇()V
    .locals 1

    .line 1
    new-instance v0, L〇〇0o〇o8/o〇0;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇〇0o〇o8/o〇0;-><init>(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPIUtils;->〇o00〇〇Oo(Lcom/intsig/tianshu/TianShuAPIProxy;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇00()V
    .locals 10

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇O8o08O()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    if-eqz v3, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    sget-object v3, Lcom/intsig/utils/AESEncUtil$EncType;->SecurityCheck:Lcom/intsig/utils/AESEncUtil$EncType;

    .line 17
    .line 18
    invoke-static {v2, v3}, Lcom/intsig/utils/AESEncUtil;->〇〇888(Ljava/lang/String;Lcom/intsig/utils/AESEncUtil$EncType;)Z

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 23
    .line 24
    .line 25
    move-result-wide v4

    .line 26
    sub-long/2addr v4, v0

    .line 27
    sget-object v6, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 28
    .line 29
    invoke-virtual {v6}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇8O0〇8()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v7

    .line 33
    new-instance v8, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v9, "encryptAccount  diff01 = "

    .line 39
    .line 40
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    invoke-static {v7, v4}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    if-eqz v3, :cond_1

    .line 54
    .line 55
    invoke-virtual {v6}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇8O0〇8()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const-string v1, "is already encrypted"

    .line 60
    .line 61
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return-void

    .line 65
    :cond_1
    invoke-static {v2}, Lcom/intsig/comm/account_data/AccountPreference;->〇o0O0O8(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 69
    .line 70
    .line 71
    move-result-wide v2

    .line 72
    sub-long/2addr v2, v0

    .line 73
    invoke-virtual {v6}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇8O0〇8()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    new-instance v1, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    const-string v4, "encryptAccount  diff02 = "

    .line 83
    .line 84
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0000OOO()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/push/common/PushMsgClient;->〇o〇()Lcom/intsig/camscanner/push/common/PushMsgClient;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/push/common/PushMsgClient;->o〇0(Landroid/content/Context;)Lcom/intsig/camscanner/push/common/PushMsgClient;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO8oO0o〇()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 16
    .line 17
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇0(Landroid/content/Context;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    iget-object v3, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 22
    .line 23
    invoke-static {v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    sget-object v4, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 28
    .line 29
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->O8()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/push/common/PushMsgClient;->〇〇888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/push/common/PushMsgClient;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/push/common/PushMsgClient;->oO80()V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇00〇8(I)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    invoke-static {v0}, Lcom/intsig/tianshu/UserInfo;->switchApis(I)V

    .line 5
    .line 6
    .line 7
    invoke-static {p1}, Lcom/intsig/tianshu/purchase/TianshuPurchaseApi;->O8(I)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    if-ne v0, p1, :cond_1

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-static {v0}, Lcom/intsig/tianshu/UserInfo;->switchApis(I)V

    .line 15
    .line 16
    .line 17
    invoke-static {p1}, Lcom/intsig/tianshu/purchase/TianshuPurchaseApi;->O8(I)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const/4 v0, 0x2

    .line 22
    invoke-static {v0}, Lcom/intsig/tianshu/UserInfo;->switchApis(I)V

    .line 23
    .line 24
    .line 25
    invoke-static {p1}, Lcom/intsig/tianshu/purchase/TianshuPurchaseApi;->O8(I)V

    .line 26
    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇o()V
    .locals 2

    .line 1
    const-string v0, "CSStart"

    .line 2
    .line 3
    const-string v1, "start"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->o800o8O(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇oOO8O8()V
    .locals 4

    .line 1
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2
    .line 3
    const/16 v1, 0x1c

    .line 4
    .line 5
    if-lt v0, v1, :cond_0

    .line 6
    .line 7
    invoke-static {}, Landroid/app/Application;->getProcessName()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 12
    .line 13
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-nez v1, :cond_0

    .line 22
    .line 23
    sget-object v1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇8O0〇8()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    new-instance v2, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v3, "processName = "

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-static {v0}, Lcom/applovin/adview/〇080;->〇080(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    sget-object v1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇8O0〇8()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    .line 62
    .line 63
    :cond_0
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->OOO〇O0(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final O〇8O8〇008()Lcom/intsig/camscanner/launch/CsApplication;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected 〇O00(Ljava/lang/String;)V
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "name"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    sget-object p1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇o00〇〇Oo()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->〇00〇8(I)V

    .line 17
    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v3}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8()Lcom/intsig/camscanner/launch/AdAppLaunchActivityLifecycleCallback;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    invoke-virtual {v2, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 30
    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 33
    .line 34
    invoke-static {v2}, Lcom/intsig/nativelib/PinyinUtil;->init(Landroid/content/Context;)V

    .line 35
    .line 36
    .line 37
    invoke-static {}, Lcom/intsig/camscanner/app/Verify;->O8()Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    const/4 v3, 0x1

    .line 42
    if-nez v2, :cond_1

    .line 43
    .line 44
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 45
    .line 46
    invoke-static {v2}, Lcom/intsig/camscanner/app/AppUtil;->o〇0OOo〇0(Landroid/content/Context;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-static {v2}, Lcom/intsig/camscanner/app/AppActivateUtils;->〇o〇(Landroid/content/Context;)Z

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    if-eqz v2, :cond_0

    .line 58
    .line 59
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇8(Z)V

    .line 60
    .line 61
    .line 62
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇O00:Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;

    .line 63
    .line 64
    invoke-virtual {v2}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;->〇080()Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    invoke-virtual {v2}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇00〇8()V

    .line 69
    .line 70
    .line 71
    :cond_1
    new-instance v2, Lcom/intsig/camscanner/CsEventBusIndex;

    .line 72
    .line 73
    invoke-direct {v2}, Lcom/intsig/camscanner/CsEventBusIndex;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-static {v2}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇080(Ljava/lang/Object;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->O8ooOoo〇()Z

    .line 80
    .line 81
    .line 82
    move-result v2

    .line 83
    invoke-static {v2}, Lcom/intsig/utils/GatedUtil;->O8(Z)V

    .line 84
    .line 85
    .line 86
    invoke-static {v2}, Lcom/intsig/thread/ThreadPoolSingleton;->Oo08(Z)V

    .line 87
    .line 88
    .line 89
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    invoke-static {v2}, Lcom/intsig/developer/shortcutbadger/ShortcutBadger;->oO80(Z)V

    .line 94
    .line 95
    .line 96
    new-instance v2, Lcom/intsig/camscanner/web/WebAction;

    .line 97
    .line 98
    iget-object v4, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 99
    .line 100
    invoke-direct {v2, v4}, Lcom/intsig/camscanner/web/WebAction;-><init>(Landroid/content/Context;)V

    .line 101
    .line 102
    .line 103
    invoke-static {v2}, Lcom/intsig/webview/WebViewUtils;->〇O00(Lcom/intsig/webview/WebViewAppInterface;)V

    .line 104
    .line 105
    .line 106
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 107
    .line 108
    invoke-static {v2}, Lcom/intsig/wechat/WeChatApi;->oO80(Landroid/content/Context;)V

    .line 109
    .line 110
    .line 111
    new-instance v2, Lcom/intsig/camscanner/tsapp/SyncAdapter;

    .line 112
    .line 113
    iget-object v4, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 114
    .line 115
    invoke-direct {v2, v4}, Lcom/intsig/camscanner/tsapp/SyncAdapter;-><init>(Landroid/content/Context;)V

    .line 116
    .line 117
    .line 118
    invoke-static {v2}, Lcom/intsig/tianshu/TianShuAPI;->o〇0o〇〇(Lcom/intsig/tianshu/Adapter;)V

    .line 119
    .line 120
    .line 121
    invoke-static {}, Lcom/intsig/launch/GlobalAppLaunchManager;->〇〇888()Lcom/intsig/launch/GlobalAppLaunchManager;

    .line 122
    .line 123
    .line 124
    move-result-object v2

    .line 125
    iget-object v4, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 126
    .line 127
    new-instance v5, L〇〇0o〇o8/Oo08;

    .line 128
    .line 129
    invoke-direct {v5, p0}, L〇〇0o〇o8/Oo08;-><init>(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {v2, v4, v5}, Lcom/intsig/launch/GlobalAppLaunchManager;->oO80(Landroid/app/Application;Lcom/intsig/launch/GlobalAppLaunchManager$GlobalAppLaunchListener;)V

    .line 133
    .line 134
    .line 135
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->〇00()V

    .line 136
    .line 137
    .line 138
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 139
    .line 140
    invoke-static {v2}, Lcom/intsig/camscanner/app/AppUtil;->OO0o〇〇(Landroid/content/Context;)V

    .line 141
    .line 142
    .line 143
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o〇〇0〇()V

    .line 144
    .line 145
    .line 146
    invoke-static {}, Lcom/intsig/camscanner/util/PermissionRefuseTips;->oO80()V

    .line 147
    .line 148
    .line 149
    invoke-static {}, Lcom/intsig/camscanner/app/Verify;->O8()Z

    .line 150
    .line 151
    .line 152
    move-result v2

    .line 153
    if-nez v2, :cond_6

    .line 154
    .line 155
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->〇0000OOO()V

    .line 156
    .line 157
    .line 158
    iget-object v2, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 159
    .line 160
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->OO0o〇〇〇〇0(Landroid/content/Context;)Z

    .line 161
    .line 162
    .line 163
    invoke-static {}, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    invoke-static {v2}, Lcom/intsig/tianshu/UserInfo;->updateApisByServerInParentThread(Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    const/4 v4, 0x0

    .line 171
    const/4 v5, 0x0

    .line 172
    const/4 v6, 0x0

    .line 173
    const/4 v7, 0x0

    .line 174
    const/4 v8, 0x0

    .line 175
    new-instance v9, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$run$3;

    .line 176
    .line 177
    invoke-direct {v9, p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$run$3;-><init>(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;)V

    .line 178
    .line 179
    .line 180
    const/16 v10, 0x1f

    .line 181
    .line 182
    const/4 v11, 0x0

    .line 183
    invoke-static/range {v4 .. v11}, Lkotlin/concurrent/ThreadsKt;->〇o00〇〇Oo(ZZLjava/lang/ClassLoader;Ljava/lang/String;ILkotlin/jvm/functions/Function0;ILjava/lang/Object;)Ljava/lang/Thread;

    .line 184
    .line 185
    .line 186
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 187
    .line 188
    .line 189
    move-result v4

    .line 190
    const/4 v5, 0x0

    .line 191
    if-eqz v4, :cond_2

    .line 192
    .line 193
    sget-object v2, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$run$4;->o0:Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$run$4;

    .line 194
    .line 195
    invoke-static {v2}, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->〇O〇(Lkotlin/jvm/functions/Function0;)V

    .line 196
    .line 197
    .line 198
    invoke-static {v5, v3, v5}, Lcom/intsig/camscanner/uploadinfo/UploadDeviceInfo;->〇O00(Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 199
    .line 200
    .line 201
    goto :goto_0

    .line 202
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 203
    .line 204
    .line 205
    move-result v2

    .line 206
    xor-int/2addr v2, v3

    .line 207
    if-eqz v2, :cond_3

    .line 208
    .line 209
    invoke-static {v5, v3, v5}, Lcom/intsig/camscanner/uploadinfo/UploadDeviceInfo;->〇O00(Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 210
    .line 211
    .line 212
    invoke-static {}, Lcom/intsig/camscanner/util/BranchResultCallManager;->〇o00〇〇Oo()V

    .line 213
    .line 214
    .line 215
    invoke-static {}, Lcom/intsig/camscanner/util/AppFlyerAttributeInfoManager;->〇o〇()V

    .line 216
    .line 217
    .line 218
    :cond_3
    new-instance v3, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$run$5;

    .line 219
    .line 220
    invoke-direct {v3, v2}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$run$5;-><init>(Z)V

    .line 221
    .line 222
    .line 223
    invoke-static {v3}, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->〇O〇(Lkotlin/jvm/functions/Function0;)V

    .line 224
    .line 225
    .line 226
    :goto_0
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇0〇O0088o()Z

    .line 227
    .line 228
    .line 229
    move-result v2

    .line 230
    if-nez v2, :cond_4

    .line 231
    .line 232
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 233
    .line 234
    .line 235
    move-result v2

    .line 236
    if-eqz v2, :cond_5

    .line 237
    .line 238
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇o8oO()V

    .line 239
    .line 240
    .line 241
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O08O0〇O()V

    .line 242
    .line 243
    .line 244
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->O8ooOoo〇()V

    .line 245
    .line 246
    .line 247
    sget-object v2, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$MainPageGrayTask;->〇080:Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$MainPageGrayTask;

    .line 248
    .line 249
    invoke-virtual {v2}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$MainPageGrayTask;->〇080()V

    .line 250
    .line 251
    .line 252
    sget-object v2, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$LoginInfoTask;->〇080:Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$LoginInfoTask;

    .line 253
    .line 254
    invoke-virtual {v2}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$LoginInfoTask;->〇080()V

    .line 255
    .line 256
    .line 257
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->〇oOO8O8()V

    .line 258
    .line 259
    .line 260
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->oo88o8O()Z

    .line 261
    .line 262
    .line 263
    move-result v2

    .line 264
    if-nez v2, :cond_7

    .line 265
    .line 266
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇oOO8O8()Z

    .line 267
    .line 268
    .line 269
    move-result v2

    .line 270
    if-eqz v2, :cond_8

    .line 271
    .line 272
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/tools/FrameDetectionTool;->〇o〇()Lcom/intsig/camscanner/tools/FrameDetectionTool;

    .line 273
    .line 274
    .line 275
    move-result-object v2

    .line 276
    iget-object v3, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 277
    .line 278
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/tools/FrameDetectionTool;->〇〇888(Landroid/app/Application;)V

    .line 279
    .line 280
    .line 281
    :cond_8
    sget-object v2, Lcom/intsig/camscanner/capture/util/BranchSdkUtils;->INSTANCE:Lcom/intsig/camscanner/capture/util/BranchSdkUtils;

    .line 282
    .line 283
    iget-object v3, p0, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->o8〇OO0〇0o:Lcom/intsig/camscanner/launch/CsApplication;

    .line 284
    .line 285
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇0〇O0088o()Z

    .line 286
    .line 287
    .line 288
    move-result v4

    .line 289
    invoke-virtual {v2, v3, v4}, Lcom/intsig/camscanner/capture/util/BranchSdkUtils;->initSdk(Landroid/content/Context;Z)V

    .line 290
    .line 291
    .line 292
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;->〇o()V

    .line 293
    .line 294
    .line 295
    new-instance v2, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$run$6;

    .line 296
    .line 297
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask$run$6;-><init>(Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;)V

    .line 298
    .line 299
    .line 300
    invoke-static {v2}, Lcom/intsig/wxapi/WXEntryActivity;->o880(Lcom/intsig/wxapi/WXEntryActivity$WeChatReqListener;)V

    .line 301
    .line 302
    .line 303
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇8O0〇8()Ljava/lang/String;

    .line 304
    .line 305
    .line 306
    move-result-object p1

    .line 307
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 308
    .line 309
    .line 310
    move-result-wide v2

    .line 311
    sub-long/2addr v2, v0

    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    .line 313
    .line 314
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 315
    .line 316
    .line 317
    const-string v1, "UiThreadInitTask run costTime:"

    .line 318
    .line 319
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    .line 321
    .line 322
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 323
    .line 324
    .line 325
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 326
    .line 327
    .line 328
    move-result-object v0

    .line 329
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    .line 331
    .line 332
    return-void
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.class public final Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator;
.super Ljava/lang/Object;
.source "CsAppStartTaskCreator.kt"

# interfaces
.implements Lcom/effective/android/anchors/task/TaskCreator;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator;->〇080:Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator;->〇o〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇o〇()Ljava/lang/String;
    .locals 6

    .line 1
    const-string v0, "TASK_FOR_PRE_INIT"

    .line 2
    .line 3
    const-string v1, "TASK_BASE_MAIN"

    .line 4
    .line 5
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const-string v2, "TASK_LOG"

    .line 10
    .line 11
    invoke-static {v2, v0}, Lcom/effective/android/anchors/AnchorsManagerKt;->O8(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    const-string v0, "TASK_HIGH_MAIN"

    .line 15
    .line 16
    const-string v3, "TASK_HIGH_THREAD"

    .line 17
    .line 18
    const-string v4, "TASK_HIGH_THREAD_2"

    .line 19
    .line 20
    filled-new-array {v0, v3, v4}, [Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v5

    .line 24
    invoke-static {v1, v5}, Lcom/effective/android/anchors/AnchorsManagerKt;->O8(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    const-string v1, "TASK_MAIN"

    .line 28
    .line 29
    filled-new-array {v1}, [Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v5

    .line 33
    invoke-static {v0, v5}, Lcom/effective/android/anchors/AnchorsManagerKt;->O8(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    filled-new-array {v1}, [Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-static {v3, v0}, Lcom/effective/android/anchors/AnchorsManagerKt;->O8(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    filled-new-array {v1}, [Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-static {v4, v0}, Lcom/effective/android/anchors/AnchorsManagerKt;->O8(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    const-string v0, "TASK_VERSION_INFO"

    .line 51
    .line 52
    const-string v3, "TASK_NON_BLOCK"

    .line 53
    .line 54
    const-string v4, "TASK_DEBUG"

    .line 55
    .line 56
    filled-new-array {v4, v0, v3}, [Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-static {v1, v0}, Lcom/effective/android/anchors/AnchorsManagerKt;->O8(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    return-object v2
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final O8()V
    .locals 3

    .line 1
    sget-object v0, Lcom/effective/android/anchors/AnchorsManager;->Oo08:Lcom/effective/android/anchors/AnchorsManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    invoke-static {v0, v1, v2, v1}, Lcom/effective/android/anchors/AnchorsManager$Companion;->〇o00〇〇Oo(Lcom/effective/android/anchors/AnchorsManager$Companion;Ljava/util/concurrent/ExecutorService;ILjava/lang/Object;)Lcom/effective/android/anchors/AnchorsManager;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sget-object v1, Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator$start$1;->o0:Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator$start$1;

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/effective/android/anchors/AnchorsManagerKt;->〇o00〇〇Oo(Lcom/effective/android/anchors/AnchorsManager;Lkotlin/jvm/functions/Function0;)Lcom/effective/android/anchors/AnchorsManager;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sget-object v1, Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator$start$2;->o0:Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator$start$2;

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/effective/android/anchors/AnchorsManagerKt;->o〇0(Lcom/effective/android/anchors/AnchorsManager;Lkotlin/jvm/functions/Function0;)Lcom/effective/android/anchors/AnchorsManager;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    sget-object v1, Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator$start$3;->o0:Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator$start$3;

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/effective/android/anchors/AnchorsManagerKt;->〇080(Lcom/effective/android/anchors/AnchorsManager;Lkotlin/jvm/functions/Function0;)Lcom/effective/android/anchors/AnchorsManager;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    sget-object v1, Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator$start$4;->o0:Lcom/intsig/camscanner/launch/tasks/CsAppStartTaskCreator$start$4;

    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/effective/android/anchors/AnchorsManagerKt;->〇o〇(Lcom/effective/android/anchors/AnchorsManager;Lkotlin/jvm/functions/Function0;)Lcom/effective/android/anchors/AnchorsManager;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-static {v0}, Lcom/effective/android/anchors/AnchorsManagerKt;->Oo08(Lcom/effective/android/anchors/AnchorsManager;)Lcom/effective/android/anchors/AnchorsManager;

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇080(Ljava/lang/String;)Lcom/effective/android/anchors/task/Task;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "taskName"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    sparse-switch v0, :sswitch_data_0

    .line 11
    .line 12
    .line 13
    goto/16 :goto_0

    .line 14
    .line 15
    :sswitch_0
    const-string v0, "TASK_NON_BLOCK"

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-nez p1, :cond_0

    .line 22
    .line 23
    goto/16 :goto_0

    .line 24
    .line 25
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/NonBlockTask;

    .line 26
    .line 27
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/NonBlockTask;-><init>()V

    .line 28
    .line 29
    .line 30
    goto/16 :goto_1

    .line 31
    .line 32
    :sswitch_1
    const-string v0, "TASK_LOG"

    .line 33
    .line 34
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    if-eqz p1, :cond_9

    .line 39
    .line 40
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/LogInitTask;

    .line 41
    .line 42
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/LogInitTask;-><init>()V

    .line 43
    .line 44
    .line 45
    goto/16 :goto_1

    .line 46
    .line 47
    :sswitch_2
    const-string v0, "TASK_HIGH_THREAD_2"

    .line 48
    .line 49
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-nez p1, :cond_1

    .line 54
    .line 55
    goto/16 :goto_0

    .line 56
    .line 57
    :cond_1
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/HighThreadTask2;

    .line 58
    .line 59
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/HighThreadTask2;-><init>()V

    .line 60
    .line 61
    .line 62
    goto/16 :goto_1

    .line 63
    .line 64
    :sswitch_3
    const-string v0, "TASK_VERSION_INFO"

    .line 65
    .line 66
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    if-nez p1, :cond_2

    .line 71
    .line 72
    goto/16 :goto_0

    .line 73
    .line 74
    :cond_2
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/TaskVersionInfo;

    .line 75
    .line 76
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/TaskVersionInfo;-><init>()V

    .line 77
    .line 78
    .line 79
    goto/16 :goto_1

    .line 80
    .line 81
    :sswitch_4
    const-string v0, "TASK_HIGH_MAIN"

    .line 82
    .line 83
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    move-result p1

    .line 87
    if-nez p1, :cond_3

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_3
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/HighMainTask;

    .line 91
    .line 92
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/HighMainTask;-><init>()V

    .line 93
    .line 94
    .line 95
    goto :goto_1

    .line 96
    :sswitch_5
    const-string v0, "TASK_HIGH_THREAD"

    .line 97
    .line 98
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    move-result p1

    .line 102
    if-nez p1, :cond_4

    .line 103
    .line 104
    goto :goto_0

    .line 105
    :cond_4
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/HighThreadTask;

    .line 106
    .line 107
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/HighThreadTask;-><init>()V

    .line 108
    .line 109
    .line 110
    goto :goto_1

    .line 111
    :sswitch_6
    const-string v0, "TASK_FOR_PRE_INIT"

    .line 112
    .line 113
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    move-result p1

    .line 117
    if-nez p1, :cond_5

    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_5
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/PreInitTask;

    .line 121
    .line 122
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/PreInitTask;-><init>()V

    .line 123
    .line 124
    .line 125
    goto :goto_1

    .line 126
    :sswitch_7
    const-string v0, "TASK_MAIN"

    .line 127
    .line 128
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    move-result p1

    .line 132
    if-nez p1, :cond_6

    .line 133
    .line 134
    goto :goto_0

    .line 135
    :cond_6
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;

    .line 136
    .line 137
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/UiThreadInitTask;-><init>()V

    .line 138
    .line 139
    .line 140
    goto :goto_1

    .line 141
    :sswitch_8
    const-string v0, "TASK_BASE_MAIN"

    .line 142
    .line 143
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    if-nez p1, :cond_7

    .line 148
    .line 149
    goto :goto_0

    .line 150
    :cond_7
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/BaseMainTask;

    .line 151
    .line 152
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/BaseMainTask;-><init>()V

    .line 153
    .line 154
    .line 155
    goto :goto_1

    .line 156
    :sswitch_9
    const-string v0, "TASK_DEBUG"

    .line 157
    .line 158
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    move-result p1

    .line 162
    if-nez p1, :cond_8

    .line 163
    .line 164
    goto :goto_0

    .line 165
    :cond_8
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/DebugTask;

    .line 166
    .line 167
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/DebugTask;-><init>()V

    .line 168
    .line 169
    .line 170
    goto :goto_1

    .line 171
    :cond_9
    :goto_0
    new-instance p1, Lcom/intsig/camscanner/launch/tasks/DebugTask;

    .line 172
    .line 173
    invoke-direct {p1}, Lcom/intsig/camscanner/launch/tasks/DebugTask;-><init>()V

    .line 174
    .line 175
    .line 176
    :goto_1
    return-object p1

    .line 177
    :sswitch_data_0
    .sparse-switch
        -0x5f16c907 -> :sswitch_9
        -0x5387e7d3 -> :sswitch_8
        -0x4d5fcbcd -> :sswitch_7
        -0x3c329b64 -> :sswitch_6
        -0x20bea473 -> :sswitch_5
        -0x10902404 -> :sswitch_4
        0x14443c4f -> :sswitch_3
        0x1458b800 -> :sswitch_2
        0x1647396a -> :sswitch_1
        0x7043ae21 -> :sswitch_0
    .end sparse-switch
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

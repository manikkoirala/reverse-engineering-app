.class public final Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;
.super Ljava/lang/Object;
.source "ESignSyncApi.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UploadJpgItem;,
        Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UpdateTitleReq;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o00〇〇Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇080:Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;

    .line 7
    .line 8
    const-class v0, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;

    .line 9
    .line 10
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const-string v1, "ESignSyncApi::class.java.simpleName"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇o00〇〇Oo:Ljava/lang/String;

    .line 20
    .line 21
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;[ILcom/intsig/tianshu/TianShuAPI$OnProgressListener;Ljava/lang/String;Z)[I
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/io/OutputStream;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/tianshu/exception/TianShuException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    move-object v0, p0

    .line 2
    move-object v1, p1

    .line 3
    move-object v2, p2

    .line 4
    move-object v3, p3

    .line 5
    const-string v4, "fileName"

    .line 6
    .line 7
    invoke-static {p3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const-string v4, "outputStream"

    .line 11
    .line 12
    move-object v7, p4

    .line 13
    invoke-static {p4, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v4, "outputContentLength"

    .line 17
    .line 18
    move-object/from16 v9, p5

    .line 19
    .line 20
    invoke-static {v9, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v4, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇o00〇〇Oo:Ljava/lang/String;

    .line 24
    .line 25
    new-instance v5, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v6, "downloadJpg imageSyncId == "

    .line 31
    .line 32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v5

    .line 42
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    new-instance v5, Lcom/intsig/tianshu/ParamsBuilder;

    .line 46
    .line 47
    invoke-direct {v5}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 48
    .line 49
    .line 50
    if-eqz v0, :cond_0

    .line 51
    .line 52
    if-eqz v1, :cond_0

    .line 53
    .line 54
    const-string v2, "sid"

    .line 55
    .line 56
    invoke-virtual {v5, v2, p0}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 57
    .line 58
    .line 59
    const-string v0, "encrypt_id"

    .line 60
    .line 61
    invoke-virtual {v5, v0, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-static {v0, p2}, Lcom/intsig/camscanner/app/DBUtil;->〇80(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    const-string v1, "doc_id"

    .line 74
    .line 75
    invoke-virtual {v5, v1, v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 76
    .line 77
    .line 78
    const-string v0, "token"

    .line 79
    .line 80
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    invoke-virtual {v5, v0, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 85
    .line 86
    .line 87
    :goto_0
    const-string v0, "file_name"

    .line 88
    .line 89
    invoke-virtual {v5, v0, p3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 90
    .line 91
    .line 92
    const-string v0, "type"

    .line 93
    .line 94
    move-object/from16 v1, p7

    .line 95
    .line 96
    invoke-virtual {v5, v0, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 97
    .line 98
    .line 99
    if-eqz p8, :cond_1

    .line 100
    .line 101
    const-string v0, "miss"

    .line 102
    .line 103
    const-string v1, "1"

    .line 104
    .line 105
    invoke-virtual {v5, v0, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 106
    .line 107
    .line 108
    :cond_1
    invoke-virtual {v5}, Lcom/intsig/tianshu/ParamsBuilder;->〇080()Ljava/util/Map;

    .line 109
    .line 110
    .line 111
    move-result-object v6

    .line 112
    const-string v5, "/esign/pic/download"

    .line 113
    .line 114
    const/4 v10, 0x1

    .line 115
    move-object v7, p4

    .line 116
    move-object/from16 v8, p6

    .line 117
    .line 118
    move-object/from16 v9, p5

    .line 119
    .line 120
    invoke-static/range {v5 .. v10}, Lcom/intsig/tianshu/TianShuAPI;->〇8(Ljava/lang/String;Ljava/util/Map;Ljava/io/OutputStream;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;[II)[I

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v1

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    .line 129
    .line 130
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .line 132
    .line 133
    const-string v3, "downloadJpg TianShuAPI.downloadCommonFile result:"

    .line 134
    .line 135
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    if-nez v0, :cond_2

    .line 149
    .line 150
    const/4 v0, 0x2

    .line 151
    new-array v0, v0, [I

    .line 152
    .line 153
    fill-array-data v0, :array_0

    .line 154
    .line 155
    .line 156
    :cond_2
    return-object v0

    .line 157
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
.end method

.method public static final Oo08(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/String;[ILcom/intsig/tianshu/TianShuAPI$OnProgressListener;IZ)[I
    .locals 8
    .param p2    # Ljava/io/OutputStream;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/tianshu/exception/TianShuException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "outputStream"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "appVersion"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "outputContentLength"

    .line 12
    .line 13
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    sget-object v1, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇080:Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;

    .line 21
    .line 22
    const-string v2, "downloadResizeJpg"

    .line 23
    .line 24
    invoke-direct {v1, v2, v0}, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 v0, 0x0

    .line 32
    :goto_0
    if-eqz v0, :cond_2

    .line 33
    .line 34
    new-instance v1, Lcom/intsig/tianshu/ParamsBuilder;

    .line 35
    .line 36
    invoke-direct {v1}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v2, "token"

    .line 40
    .line 41
    invoke-virtual {v1, v2, v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 42
    .line 43
    .line 44
    const-string v0, "doc_id"

    .line 45
    .line 46
    invoke-virtual {v1, v0, p0}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 47
    .line 48
    .line 49
    const-string p0, "file_name"

    .line 50
    .line 51
    invoke-virtual {v1, p0, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 52
    .line 53
    .line 54
    const-string p0, "app_version"

    .line 55
    .line 56
    invoke-virtual {v1, p0, p3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 57
    .line 58
    .line 59
    const-string p0, "pixel"

    .line 60
    .line 61
    invoke-virtual {v1, p0, p6}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 62
    .line 63
    .line 64
    if-eqz p7, :cond_1

    .line 65
    .line 66
    const-string p0, "miss"

    .line 67
    .line 68
    const-string p1, "1"

    .line 69
    .line 70
    invoke-virtual {v1, p0, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 71
    .line 72
    .line 73
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇080()Ljava/util/Map;

    .line 74
    .line 75
    .line 76
    move-result-object v3

    .line 77
    const-string v2, "/esign/pic/resize"

    .line 78
    .line 79
    const/4 v7, 0x1

    .line 80
    move-object v4, p2

    .line 81
    move-object v5, p5

    .line 82
    move-object v6, p4

    .line 83
    invoke-static/range {v2 .. v7}, Lcom/intsig/tianshu/TianShuAPI;->〇8(Ljava/lang/String;Ljava/util/Map;Ljava/io/OutputStream;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;[II)[I

    .line 84
    .line 85
    .line 86
    move-result-object p0

    .line 87
    sget-object p1, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇o00〇〇Oo:Ljava/lang/String;

    .line 88
    .line 89
    invoke-static {p0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object p2

    .line 93
    new-instance p3, Ljava/lang/StringBuilder;

    .line 94
    .line 95
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .line 97
    .line 98
    const-string p4, "downloadResizeJpg TianShuAPI.downloadCommonFile result:"

    .line 99
    .line 100
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object p2

    .line 110
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    const-string p1, "result"

    .line 114
    .line 115
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    return-object p0

    .line 119
    :cond_2
    const/4 p0, 0x2

    .line 120
    new-array p0, p0, [I

    .line 121
    .line 122
    fill-array-data p0, :array_0

    .line 123
    .line 124
    .line 125
    return-object p0

    .line 126
    nop

    .line 127
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
.end method

.method public static final 〇8o8o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UploadJpgItem;)Z
    .locals 18
    .param p5    # Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UploadJpgItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/tianshu/exception/TianShuException;
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    const-string v2, "uploadJpgItem"

    .line 6
    .line 7
    move-object/from16 v3, p5

    .line 8
    .line 9
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    sget-object v2, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇o00〇〇Oo:Ljava/lang/String;

    .line 13
    .line 14
    new-instance v4, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v5, "signToken == "

    .line 20
    .line 21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v5, " ,token == "

    .line 28
    .line 29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const/4 v5, 0x0

    .line 43
    if-eqz v1, :cond_1

    .line 44
    .line 45
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    if-nez v6, :cond_0

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const/4 v6, 0x0

    .line 53
    goto :goto_1

    .line 54
    :cond_1
    :goto_0
    const/4 v6, 0x1

    .line 55
    :goto_1
    if-nez v6, :cond_9

    .line 56
    .line 57
    if-eqz v0, :cond_3

    .line 58
    .line 59
    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    .line 60
    .line 61
    .line 62
    move-result v6

    .line 63
    if-nez v6, :cond_2

    .line 64
    .line 65
    goto :goto_2

    .line 66
    :cond_2
    const/4 v6, 0x0

    .line 67
    goto :goto_3

    .line 68
    :cond_3
    :goto_2
    const/4 v6, 0x1

    .line 69
    :goto_3
    if-eqz v6, :cond_4

    .line 70
    .line 71
    goto/16 :goto_7

    .line 72
    .line 73
    :cond_4
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UploadJpgItem;->getPageSyncId()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v6

    .line 77
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UploadJpgItem;->getBigImgPath()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v15

    .line 81
    if-eqz v6, :cond_8

    .line 82
    .line 83
    invoke-static {v15}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 84
    .line 85
    .line 86
    move-result v7

    .line 87
    if-nez v7, :cond_5

    .line 88
    .line 89
    goto/16 :goto_6

    .line 90
    .line 91
    :cond_5
    new-instance v14, Ljava/util/Vector;

    .line 92
    .line 93
    invoke-direct {v14}, Ljava/util/Vector;-><init>()V

    .line 94
    .line 95
    .line 96
    invoke-virtual/range {p5 .. p5}, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UploadJpgItem;->getModifiedTime()Ljava/lang/Long;

    .line 97
    .line 98
    .line 99
    move-result-object v3

    .line 100
    if-eqz v3, :cond_6

    .line 101
    .line 102
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    .line 103
    .line 104
    .line 105
    move-result-wide v7

    .line 106
    goto :goto_4

    .line 107
    :cond_6
    const-wide/16 v7, 0x0

    .line 108
    .line 109
    :goto_4
    move-wide v10, v7

    .line 110
    new-instance v3, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction;

    .line 111
    .line 112
    new-instance v7, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    const-string v8, ".jpg"

    .line 121
    .line 122
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v8

    .line 129
    const/4 v9, 0x0

    .line 130
    const-string v12, "CamScanner_Page"

    .line 131
    .line 132
    const/4 v13, 0x3

    .line 133
    move-object v7, v3

    .line 134
    move-wide/from16 v16, v10

    .line 135
    .line 136
    move-object v10, v12

    .line 137
    move-wide/from16 v11, v16

    .line 138
    .line 139
    move-object v4, v14

    .line 140
    move-object v14, v15

    .line 141
    invoke-direct/range {v7 .. v14}, Lcom/intsig/camscanner/tsapp/sync/PageUploadAction;-><init>(Ljava/lang/String;ILjava/lang/String;JILjava/lang/String;)V

    .line 142
    .line 143
    .line 144
    new-instance v7, Ljava/lang/StringBuilder;

    .line 145
    .line 146
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .line 148
    .line 149
    const-string v8, "uploadTrimmedPaper2Server action = "

    .line 150
    .line 151
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    const-string v6, ".jpg, time = "

    .line 158
    .line 159
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    move-wide/from16 v8, v16

    .line 163
    .line 164
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    const-string v6, ", path = "

    .line 168
    .line 169
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object v6

    .line 179
    invoke-static {v2, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    .line 181
    .line 182
    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 183
    .line 184
    .line 185
    new-instance v3, Lcom/intsig/tianshu/ParamsBuilder;

    .line 186
    .line 187
    invoke-direct {v3}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 188
    .line 189
    .line 190
    const-string v6, "token"

    .line 191
    .line 192
    invoke-virtual {v3, v6, v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 193
    .line 194
    .line 195
    move-result-object v0

    .line 196
    const-string v3, "sign_token"

    .line 197
    .line 198
    invoke-virtual {v0, v3, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 199
    .line 200
    .line 201
    move-result-object v0

    .line 202
    const-string v1, "sid"

    .line 203
    .line 204
    move-object/from16 v3, p2

    .line 205
    .line 206
    invoke-virtual {v0, v1, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    const-string v1, "encrypt_id"

    .line 211
    .line 212
    move-object/from16 v3, p3

    .line 213
    .line 214
    invoke-virtual {v0, v1, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 215
    .line 216
    .line 217
    move-result-object v0

    .line 218
    const-string v1, "doc_id"

    .line 219
    .line 220
    move-object/from16 v3, p4

    .line 221
    .line 222
    invoke-virtual {v0, v1, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 223
    .line 224
    .line 225
    move-result-object v0

    .line 226
    const-string v1, "/esign/pic/upload"

    .line 227
    .line 228
    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/ParamsBuilder;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 229
    .line 230
    .line 231
    move-result-object v0

    .line 232
    const/4 v1, 0x0

    .line 233
    invoke-static {v4, v1, v5, v0}, Lcom/intsig/tianshu/TianShuAPI;->O0〇oO〇o(Ljava/util/Vector;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;ZLjava/lang/String;)Lcom/intsig/tianshu/UploadState;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    invoke-virtual {v0}, Lcom/intsig/tianshu/UploadState;->〇080()I

    .line 238
    .line 239
    .line 240
    move-result v0

    .line 241
    if-gtz v0, :cond_7

    .line 242
    .line 243
    const/4 v4, 0x1

    .line 244
    goto :goto_5

    .line 245
    :cond_7
    const/4 v4, 0x0

    .line 246
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 247
    .line 248
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    .line 250
    .line 251
    const-string v1, "pic upload result == "

    .line 252
    .line 253
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 260
    .line 261
    .line 262
    move-result-object v0

    .line 263
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    .line 265
    .line 266
    return v4

    .line 267
    :cond_8
    :goto_6
    const-string v0, "pageSyncId == null or file not exist"

    .line 268
    .line 269
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    .line 271
    .line 272
    :cond_9
    :goto_7
    return v5
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final 〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    if-eqz p2, :cond_1

    .line 4
    .line 5
    invoke-static {p2}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    if-eqz p2, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p2, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 p2, 0x1

    .line 15
    :goto_1
    if-eqz p2, :cond_2

    .line 16
    .line 17
    sget-object p2, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇o00〇〇Oo:Ljava/lang/String;

    .line 18
    .line 19
    new-instance v1, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string p1, " checkToken token is empty"

    .line 28
    .line 29
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    goto :goto_2

    .line 40
    :cond_2
    const/4 v0, 0x1

    .line 41
    :goto_2
    return v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public final OO0o〇〇〇〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Response;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UpdateTitleReq;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi$UpdateTitleReq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    sget-object p2, Lokhttp3/MediaType;->〇〇888:Lokhttp3/MediaType$Companion;

    .line 11
    .line 12
    const-string p3, "application/json; charset=utf-8"

    .line 13
    .line 14
    invoke-virtual {p2, p3}, Lokhttp3/MediaType$Companion;->〇o00〇〇Oo(Ljava/lang/String;)Lokhttp3/MediaType;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    sget-object p3, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 19
    .line 20
    const-string v0, "jsonString"

    .line 21
    .line 22
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p3, p1, p2}, Lokhttp3/RequestBody$Companion;->〇o00〇〇Oo(Ljava/lang/String;Lokhttp3/MediaType;)Lokhttp3/RequestBody;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 30
    .line 31
    .line 32
    move-result-object p2

    .line 33
    const/4 p3, 0x1

    .line 34
    invoke-virtual {p2, p3}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    new-instance p3, Ljava/lang/StringBuilder;

    .line 39
    .line 40
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string p2, "/update_doc"

    .line 47
    .line 48
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    invoke-static {p2}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    invoke-virtual {p2, p1}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 64
    .line 65
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    const-string p2, "post<Any>(TianShuAPI.get\u2026y)\n            .execute()"

    .line 70
    .line 71
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    return-object p1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final oO80(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Response;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/tianshu/ParamsBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p3, :cond_0

    .line 7
    .line 8
    if-eqz p4, :cond_0

    .line 9
    .line 10
    const-string p5, "sid"

    .line 11
    .line 12
    invoke-virtual {v0, p5, p3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 13
    .line 14
    .line 15
    const-string p3, "encrypt_id"

    .line 16
    .line 17
    invoke-virtual {v0, p3, p4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-string p3, "doc_id"

    .line 22
    .line 23
    invoke-virtual {v0, p3, p5}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 24
    .line 25
    .line 26
    :goto_0
    const-string p3, "token"

    .line 27
    .line 28
    invoke-virtual {v0, p3, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 29
    .line 30
    .line 31
    const-string p1, "sign_token"

    .line 32
    .line 33
    invoke-virtual {v0, p1, p2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇080()Ljava/util/Map;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 41
    .line 42
    .line 43
    move-result-object p2

    .line 44
    const/4 p3, 0x1

    .line 45
    invoke-virtual {p2, p3}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    new-instance p3, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    const-string p2, "/esign/file/release"

    .line 58
    .line 59
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p2

    .line 66
    invoke-static {p2}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 67
    .line 68
    .line 69
    move-result-object p2

    .line 70
    const/4 p3, 0x0

    .line 71
    new-array p3, p3, [Z

    .line 72
    .line 73
    invoke-virtual {p2, p1, p3}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    check-cast p1, Lcom/lzy/okgo/request/GetRequest;

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    const-string p2, "get<Any>(\n            Ti\u2026s)\n            .execute()"

    .line 84
    .line 85
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    return-object p1
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public final o〇0(Ljava/lang/String;I)Lokhttp3/Response;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/tianshu/ParamsBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "token"

    .line 7
    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 9
    .line 10
    .line 11
    const-string p1, "order"

    .line 12
    .line 13
    invoke-virtual {v0, p1, p2}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇080()Ljava/util/Map;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    const/4 v0, 0x1

    .line 25
    invoke-virtual {p2, v0}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    const-string p2, "/esign/query"

    .line 38
    .line 39
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p2

    .line 46
    invoke-static {p2}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 47
    .line 48
    .line 49
    move-result-object p2

    .line 50
    const/4 v0, 0x0

    .line 51
    new-array v0, v0, [Z

    .line 52
    .line 53
    invoke-virtual {p2, p1, v0}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    check-cast p1, Lcom/lzy/okgo/request/GetRequest;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    const-string p2, "get<ESignQueryRes>(\n    \u2026s)\n            .execute()"

    .line 64
    .line 65
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    return-object p1
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final 〇080(Ljava/lang/String;Ljava/lang/String;IILcom/intsig/tianshu/TianShuAPI$OnUpdateListener;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/intsig/tianshu/TianShuAPI$OnUpdateListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/tianshu/exception/TianShuException;
        }
    .end annotation

    .line 1
    const-string v0, "encrypt_id"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v1, "folderName"

    .line 7
    .line 8
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v1, "onUpdateListener"

    .line 12
    .line 13
    invoke-static {p5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance v1, Lcom/intsig/tianshu/ParamsBuilder;

    .line 17
    .line 18
    invoke-direct {v1}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v2, "token"

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    invoke-virtual {v1, v2, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 28
    .line 29
    .line 30
    const-string v2, "folder_name"

    .line 31
    .line 32
    invoke-virtual {v1, v2, p2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v0, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 36
    .line 37
    .line 38
    const-string p1, "client_revision"

    .line 39
    .line 40
    invoke-virtual {v1, p1, p3}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 41
    .line 42
    .line 43
    if-lez p4, :cond_0

    .line 44
    .line 45
    const-string p1, "target_revision"

    .line 46
    .line 47
    invoke-virtual {v1, p1, p4}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 48
    .line 49
    .line 50
    :cond_0
    const-string p1, "/esign/file/batch_query"

    .line 51
    .line 52
    invoke-virtual {v1, p1}, Lcom/intsig/tianshu/ParamsBuilder;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    const/4 p2, 0x0

    .line 57
    invoke-static {p3, p1, p5, p2}, Lcom/intsig/tianshu/TianShuAPI;->〇〇8O0〇8(ILjava/lang/String;Lcom/intsig/tianshu/TianShuAPI$OnUpdateListener;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;)I

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    sget-object p2, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇o00〇〇Oo:Ljava/lang/String;

    .line 62
    .line 63
    new-instance p3, Ljava/lang/StringBuilder;

    .line 64
    .line 65
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .line 67
    .line 68
    const-string p4, "batchUpdate lastVersion:"

    .line 69
    .line 70
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public final 〇80〇808〇O(Lcom/intsig/camscanner/newsign/data/ESignLinkReq;Ljava/lang/String;)Lokhttp3/Response;
    .locals 4
    .param p1    # Lcom/intsig/camscanner/newsign/data/ESignLinkReq;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "eSignLinkReq"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/tianshu/ParamsBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "token"

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 18
    .line 19
    .line 20
    const-string v1, "platform"

    .line 21
    .line 22
    const-string v2, "android"

    .line 23
    .line 24
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 25
    .line 26
    .line 27
    const-string v1, "cs_ept_d"

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->o〇0()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 34
    .line 35
    .line 36
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getDocId()J

    .line 43
    .line 44
    .line 45
    move-result-wide v2

    .line 46
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O888o0o(Landroid/content/Context;J)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const-string v2, "doc_id"

    .line 51
    .line 52
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getShare_channel()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    const-string v2, "share_channel"

    .line 64
    .line 65
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getContact_count()I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    const-string v2, "contact_count"

    .line 77
    .line 78
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getExpire_tm()Ljava/lang/Long;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    if-eqz v1, :cond_0

    .line 86
    .line 87
    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    goto :goto_0

    .line 92
    :cond_0
    const/4 v1, 0x0

    .line 93
    :goto_0
    const-string v2, "expire_tm"

    .line 94
    .line 95
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getShare_with_me()I

    .line 99
    .line 100
    .line 101
    move-result v1

    .line 102
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    const-string v2, "share_with_me"

    .line 107
    .line 108
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 109
    .line 110
    .line 111
    const-string v1, "contact_ids"

    .line 112
    .line 113
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getContact_ids()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-virtual {v0, v1, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 118
    .line 119
    .line 120
    const-string p1, "title"

    .line 121
    .line 122
    invoke-virtual {v0, p1, p2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇080()Ljava/util/Map;

    .line 126
    .line 127
    .line 128
    move-result-object p1

    .line 129
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 130
    .line 131
    .line 132
    move-result-object p2

    .line 133
    const/4 v0, 0x1

    .line 134
    invoke-virtual {p2, v0}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object p2

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    .line 139
    .line 140
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 141
    .line 142
    .line 143
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    .line 145
    .line 146
    const-string p2, "/esign/link/update"

    .line 147
    .line 148
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object p2

    .line 155
    invoke-static {p2}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 156
    .line 157
    .line 158
    move-result-object p2

    .line 159
    const/4 v0, 0x0

    .line 160
    new-array v0, v0, [Z

    .line 161
    .line 162
    invoke-virtual {p2, p1, v0}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 163
    .line 164
    .line 165
    move-result-object p1

    .line 166
    check-cast p1, Lcom/lzy/okgo/request/GetRequest;

    .line 167
    .line 168
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 169
    .line 170
    .line 171
    move-result-object p1

    .line 172
    const-string p2, "get<Any>(\n            Ti\u2026s)\n            .execute()"

    .line 173
    .line 174
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    return-object p1
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public final 〇o〇(Lcom/intsig/camscanner/newsign/data/ESignLinkReq;)Lokhttp3/Response;
    .locals 4
    .param p1    # Lcom/intsig/camscanner/newsign/data/ESignLinkReq;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "eSignLinkReq"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/tianshu/ParamsBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "token"

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 18
    .line 19
    .line 20
    const-string v1, "platform"

    .line 21
    .line 22
    const-string v2, "android"

    .line 23
    .line 24
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 25
    .line 26
    .line 27
    const-string v1, "cs_ept_d"

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->o〇0()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 34
    .line 35
    .line 36
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getDocId()J

    .line 43
    .line 44
    .line 45
    move-result-wide v2

    .line 46
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O888o0o(Landroid/content/Context;J)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const-string v2, "doc_id"

    .line 51
    .line 52
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getShare_channel()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    const-string v2, "share_channel"

    .line 64
    .line 65
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getContact_count()I

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    const-string v2, "contact_count"

    .line 77
    .line 78
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 79
    .line 80
    .line 81
    const-string v1, "contact_ids"

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getContact_ids()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-virtual {v0, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getExpire_tm()Ljava/lang/Long;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    if-eqz v1, :cond_0

    .line 95
    .line 96
    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    goto :goto_0

    .line 101
    :cond_0
    const/4 v1, 0x0

    .line 102
    :goto_0
    const-string v2, "expire_tm"

    .line 103
    .line 104
    invoke-virtual {v0, v2, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/data/ESignLinkReq;->getShare_with_me()I

    .line 108
    .line 109
    .line 110
    move-result p1

    .line 111
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    const-string v1, "share_with_me"

    .line 116
    .line 117
    invoke-virtual {v0, v1, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 118
    .line 119
    .line 120
    const-string p1, "self_finished"

    .line 121
    .line 122
    const-string v1, "1"

    .line 123
    .line 124
    invoke-virtual {v0, p1, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇080()Ljava/util/Map;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    const/4 v1, 0x1

    .line 136
    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    new-instance v1, Ljava/lang/StringBuilder;

    .line 141
    .line 142
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .line 144
    .line 145
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    const-string v0, "/esign/link/add"

    .line 149
    .line 150
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object v0

    .line 157
    invoke-static {v0}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    const/4 v1, 0x0

    .line 162
    new-array v1, v1, [Z

    .line 163
    .line 164
    invoke-virtual {v0, p1, v1}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 165
    .line 166
    .line 167
    move-result-object p1

    .line 168
    check-cast p1, Lcom/lzy/okgo/request/GetRequest;

    .line 169
    .line 170
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    const-string v0, "get<Any>(\n            Ti\u2026s)\n            .execute()"

    .line 175
    .line 176
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    return-object p1
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇〇888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Response;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/tianshu/exception/TianShuException;
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/tianshu/ParamsBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    const-string p3, "sid"

    .line 11
    .line 12
    invoke-virtual {v0, p3, p2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 13
    .line 14
    .line 15
    const-string p2, "encrypt_id"

    .line 16
    .line 17
    invoke-virtual {v0, p2, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 18
    .line 19
    .line 20
    const-string p1, "doc_upload_time"

    .line 21
    .line 22
    invoke-virtual {v0, p1, p4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const-string p1, "doc_id"

    .line 27
    .line 28
    invoke-virtual {v0, p1, p3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 29
    .line 30
    .line 31
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-static {p1, p3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->OOO〇O0(Landroid/content/Context;Ljava/lang/String;)I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    sget-object p2, Lcom/intsig/camscanner/newsign/sync/ESignSyncApi;->〇o00〇〇Oo:Ljava/lang/String;

    .line 42
    .line 43
    new-instance p3, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string p4, "docVersion == "

    .line 49
    .line 50
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p3

    .line 60
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    const-string p2, "doc_revision"

    .line 64
    .line 65
    invoke-virtual {v0, p2, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 66
    .line 67
    .line 68
    :goto_0
    const-string p1, "token"

    .line 69
    .line 70
    invoke-virtual {v0, p1, p5}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0}, Lcom/intsig/tianshu/ParamsBuilder;->〇080()Ljava/util/Map;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    const/4 p3, 0x1

    .line 82
    invoke-virtual {p2, p3}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object p2

    .line 86
    new-instance p3, Ljava/lang/StringBuilder;

    .line 87
    .line 88
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string p2, "/esign/file/commit"

    .line 95
    .line 96
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p2

    .line 103
    invoke-static {p2}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    const/4 p3, 0x0

    .line 108
    new-array p3, p3, [Z

    .line 109
    .line 110
    invoke-virtual {p2, p1, p3}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    check-cast p1, Lcom/lzy/okgo/request/GetRequest;

    .line 115
    .line 116
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 117
    .line 118
    .line 119
    move-result-object p1

    .line 120
    const-string p2, "get<Any>(\n            Ti\u2026s)\n            .execute()"

    .line 121
    .line 122
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    return-object p1
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

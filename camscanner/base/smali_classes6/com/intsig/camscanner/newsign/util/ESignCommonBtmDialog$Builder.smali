.class public final Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;
.super Ljava/lang/Object;
.source "ESignBaseBtmDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇080:Landroid/content/Context;

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;

    .line 12
    .line 13
    const-string v0, ""

    .line 14
    .line 15
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇80〇808〇O()Ljava/util/List;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-direct {p1, v0, v1}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 20
    .line 21
    .line 22
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final getContext()Landroid/content/Context;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇080()Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇080:Landroid/content/Context;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog;-><init>(Landroid/content/Context;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 7
    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;->〇o00〇〇Oo()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog;->〇〇888(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;->〇080()Ljava/util/List;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog;->o〇0(Ljava/util/List;)V

    .line 25
    .line 26
    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;->O8(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o〇(Ljava/util/List;)Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;",
            ">;)",
            "Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "menuItemList"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder$DialogParams;->〇o〇(Ljava/util/List;)V

    .line 9
    .line 10
    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

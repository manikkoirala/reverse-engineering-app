.class public final Lcom/intsig/camscanner/newsign/util/ESignShareConfig;
.super Ljava/lang/Object;
.source "ESignShareConfig.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/newsign/util/ESignShareConfig;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/util/ESignShareConfig;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/newsign/util/ESignShareConfig;->〇080:Lcom/intsig/camscanner/newsign/util/ESignShareConfig;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇080(Landroidx/fragment/app/FragmentActivity;)Ljava/util/ArrayList;
    .locals 11
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "mContext"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const-string v2, "mContext.getString(R.string.cs_519b_more)"

    .line 16
    .line 17
    const v3, 0x7f130848

    .line 18
    .line 19
    .line 20
    const v4, 0x7f080c2e

    .line 21
    .line 22
    .line 23
    const/4 v5, 0x7

    .line 24
    const-string v6, ""

    .line 25
    .line 26
    if-eqz v1, :cond_0

    .line 27
    .line 28
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 29
    .line 30
    const v7, 0x7f1305ed

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v7

    .line 37
    const-string v8, "mContext.getString(R.string.cs_35_weixin)"

    .line 38
    .line 39
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const-string v8, "com.tencent.mm"

    .line 43
    .line 44
    const/4 v9, 0x4

    .line 45
    const v10, 0x7f0809f2

    .line 46
    .line 47
    .line 48
    invoke-direct {v1, v9, v10, v7, v8}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 55
    .line 56
    const v7, 0x7f1305e9

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v7

    .line 63
    const-string v8, "mContext.getString(R.string.cs_35_qq)"

    .line 64
    .line 65
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    const-string v8, "com.tencent.mobileqq"

    .line 69
    .line 70
    const/4 v9, 0x5

    .line 71
    const v10, 0x7f0809f1

    .line 72
    .line 73
    .line 74
    invoke-direct {v1, v9, v10, v7, v8}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 81
    .line 82
    const v7, 0x7f131f9c

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v7

    .line 89
    const-string v8, "mContext.getString(R.str\u2026b_a_label_menu_copy_link)"

    .line 90
    .line 91
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    const/4 v8, 0x6

    .line 95
    const v9, 0x7f080e43

    .line 96
    .line 97
    .line 98
    invoke-direct {v1, v8, v9, v7, v6}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    .line 103
    .line 104
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 105
    .line 106
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    invoke-direct {v1, v5, v4, p1, v6}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 121
    .line 122
    const v7, 0x7f1307eb

    .line 123
    .line 124
    .line 125
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v7

    .line 129
    const-string v8, "mContext.getString(R.string.cs_518a_whatsapp)"

    .line 130
    .line 131
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    const-string v8, "com.whatsapp"

    .line 135
    .line 136
    const/4 v9, 0x0

    .line 137
    const v10, 0x7f080c57

    .line 138
    .line 139
    .line 140
    invoke-direct {v1, v9, v10, v7, v8}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 147
    .line 148
    const v7, 0x7f130072

    .line 149
    .line 150
    .line 151
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v7

    .line 155
    const-string v8, "mContext.getString(R.str\u2026.a_global_label_facebook)"

    .line 156
    .line 157
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    const-string v8, "com.facebook.katana"

    .line 161
    .line 162
    const/4 v9, 0x1

    .line 163
    const v10, 0x7f080c19

    .line 164
    .line 165
    .line 166
    invoke-direct {v1, v9, v10, v7, v8}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    .line 171
    .line 172
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 173
    .line 174
    const v7, 0x7f13007b

    .line 175
    .line 176
    .line 177
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v7

    .line 181
    const-string v8, "mContext.getString(R.str\u2026g.a_global_label_twitter)"

    .line 182
    .line 183
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    const-string v8, "com.twitter.android"

    .line 187
    .line 188
    const/4 v9, 0x2

    .line 189
    const v10, 0x7f080c4a

    .line 190
    .line 191
    .line 192
    invoke-direct {v1, v9, v10, v7, v8}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    .line 197
    .line 198
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 199
    .line 200
    const v7, 0x7f130824

    .line 201
    .line 202
    .line 203
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object v7

    .line 207
    const-string v8, "mContext.getString(R.string.cs_519a_line_title)"

    .line 208
    .line 209
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    const-string v8, "jp.naver.line.android"

    .line 213
    .line 214
    const/4 v9, 0x3

    .line 215
    const v10, 0x7f080951

    .line 216
    .line 217
    .line 218
    invoke-direct {v1, v9, v10, v7, v8}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    .line 223
    .line 224
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;

    .line 225
    .line 226
    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    .line 232
    .line 233
    invoke-direct {v1, v5, v4, p1, v6}, Lcom/intsig/camscanner/newsign/util/ESignShareConfig$ShareItem;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    .line 234
    .line 235
    .line 236
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    .line 238
    .line 239
    :goto_0
    return-object v0
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

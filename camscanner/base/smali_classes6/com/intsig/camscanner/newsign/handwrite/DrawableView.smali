.class public final Lcom/intsig/camscanner/newsign/handwrite/DrawableView;
.super Landroid/view/View;
.source "DrawableView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:I

.field private OO〇00〇8oO:I

.field private o0:Landroid/content/Context;

.field private o8〇OO0〇0o:Landroid/graphics/Canvas;

.field private final oOo0:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooo0〇〇O:Lcom/intsig/camscanner/newsign/handwrite/OnStepChangeListener;

.field private o〇00O:Z

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:I

.field private 〇0O:F

.field private 〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

.field private 〇OOo8〇0:F

.field private 〇〇08O:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 p2, 0x41200000    # 10.0f

    .line 2
    iput p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇OOo8〇0:F

    const/high16 p2, -0x1000000

    .line 3
    iput p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO:I

    const/4 p2, -0x1

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o〇00O:Z

    .line 6
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 7
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo〇8o008:Landroid/graphics/Path;

    .line 8
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 9
    iput p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o〇0(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 p2, 0x41200000    # 10.0f

    .line 12
    iput p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇OOo8〇0:F

    const/high16 p2, -0x1000000

    .line 13
    iput p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO:I

    const/4 p2, -0x1

    .line 14
    iput p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    const/4 p3, 0x1

    .line 15
    iput-boolean p3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o〇00O:Z

    .line 16
    new-instance p3, Landroid/graphics/Paint;

    invoke-direct {p3}, Landroid/graphics/Paint;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 17
    new-instance p3, Landroid/graphics/Path;

    invoke-direct {p3}, Landroid/graphics/Path;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo〇8o008:Landroid/graphics/Path;

    .line 18
    new-instance p3, Ljava/util/LinkedList;

    invoke-direct {p3}, Ljava/util/LinkedList;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 19
    iput p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 20
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o〇0(Landroid/content/Context;)V

    return-void
.end method

.method private final Oo08(IILandroid/graphics/Bitmap;Lkotlin/ranges/IntProgression;)I
    .locals 15

    .line 1
    move/from16 v0, p1

    .line 2
    .line 3
    move/from16 v9, p2

    .line 4
    .line 5
    new-array v10, v9, [I

    .line 6
    .line 7
    invoke-virtual/range {p4 .. p4}, Lkotlin/ranges/IntProgression;->〇080()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual/range {p4 .. p4}, Lkotlin/ranges/IntProgression;->〇o〇()I

    .line 12
    .line 13
    .line 14
    move-result v11

    .line 15
    invoke-virtual/range {p4 .. p4}, Lkotlin/ranges/IntProgression;->O8()I

    .line 16
    .line 17
    .line 18
    move-result v12

    .line 19
    const/4 v13, 0x0

    .line 20
    if-lez v12, :cond_0

    .line 21
    .line 22
    if-le v1, v11, :cond_1

    .line 23
    .line 24
    :cond_0
    if-gez v12, :cond_6

    .line 25
    .line 26
    if-gt v11, v1, :cond_6

    .line 27
    .line 28
    :cond_1
    move v14, v1

    .line 29
    :goto_0
    if-eqz v0, :cond_3

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    if-eq v0, v1, :cond_2

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_2
    const/4 v3, 0x0

    .line 36
    const/4 v4, 0x1

    .line 37
    const/4 v6, 0x0

    .line 38
    const/4 v7, 0x1

    .line 39
    move-object/from16 v1, p3

    .line 40
    .line 41
    move-object v2, v10

    .line 42
    move v5, v14

    .line 43
    move/from16 v8, p2

    .line 44
    .line 45
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 46
    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_3
    const/4 v3, 0x0

    .line 50
    const/4 v5, 0x0

    .line 51
    const/4 v8, 0x1

    .line 52
    move-object/from16 v1, p3

    .line 53
    .line 54
    move-object v2, v10

    .line 55
    move/from16 v4, p2

    .line 56
    .line 57
    move v6, v14

    .line 58
    move/from16 v7, p2

    .line 59
    .line 60
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 61
    .line 62
    .line 63
    :goto_1
    const/4 v1, 0x0

    .line 64
    :goto_2
    if-ge v1, v9, :cond_5

    .line 65
    .line 66
    aget v2, v10, v1

    .line 67
    .line 68
    move-object v3, p0

    .line 69
    iget v4, v3, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    .line 70
    .line 71
    if-eq v2, v4, :cond_4

    .line 72
    .line 73
    return v14

    .line 74
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 75
    .line 76
    goto :goto_2

    .line 77
    :cond_5
    move-object v3, p0

    .line 78
    if-eq v14, v11, :cond_7

    .line 79
    .line 80
    add-int/2addr v14, v12

    .line 81
    goto :goto_0

    .line 82
    :cond_6
    move-object v3, p0

    .line 83
    :cond_7
    return v13
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final o〇0(Landroid/content/Context;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o0:Landroid/content/Context;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 10
    .line 11
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 17
    .line 18
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇OOo8〇0:F

    .line 19
    .line 20
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 24
    .line 25
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO:I

    .line 26
    .line 27
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇o〇(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-static {v2, v1}, Lkotlin/ranges/RangesKt;->OO0o〇〇(II)Lkotlin/ranges/IntRange;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-direct {p0, v2, v0, p1, v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->Oo08(IILandroid/graphics/Bitmap;Lkotlin/ranges/IntProgression;)I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    const/4 v4, 0x1

    .line 27
    sub-int/2addr v3, v4

    .line 28
    invoke-static {v3, v2}, Lkotlin/ranges/RangesKt;->OO0o〇〇〇〇0(II)Lkotlin/ranges/IntProgression;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    invoke-direct {p0, v2, v1, p1, v3}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->Oo08(IILandroid/graphics/Bitmap;Lkotlin/ranges/IntProgression;)I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    invoke-static {v2, v5}, Lkotlin/ranges/RangesKt;->OO0o〇〇(II)Lkotlin/ranges/IntRange;

    .line 45
    .line 46
    .line 47
    move-result-object v5

    .line 48
    invoke-direct {p0, v4, v3, p1, v5}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->Oo08(IILandroid/graphics/Bitmap;Lkotlin/ranges/IntProgression;)I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 53
    .line 54
    .line 55
    move-result v5

    .line 56
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 57
    .line 58
    .line 59
    move-result v6

    .line 60
    sub-int/2addr v6, v4

    .line 61
    invoke-static {v6, v2}, Lkotlin/ranges/RangesKt;->OO0o〇〇〇〇0(II)Lkotlin/ranges/IntProgression;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    invoke-direct {p0, v4, v5, p1, v2}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->Oo08(IILandroid/graphics/Bitmap;Lkotlin/ranges/IntProgression;)I

    .line 66
    .line 67
    .line 68
    move-result v2

    .line 69
    if-nez v3, :cond_0

    .line 70
    .line 71
    if-nez v0, :cond_0

    .line 72
    .line 73
    if-nez v2, :cond_0

    .line 74
    .line 75
    if-nez v1, :cond_0

    .line 76
    .line 77
    const/16 v1, 0x177

    .line 78
    .line 79
    const/16 v2, 0x177

    .line 80
    .line 81
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 82
    .line 83
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .line 85
    .line 86
    const-string v5, "left == "

    .line 87
    .line 88
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string v5, " ,right == "

    .line 95
    .line 96
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    const-string v5, " , top == "

    .line 103
    .line 104
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    const-string v5, " ,bottom == "

    .line 111
    .line 112
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v4

    .line 122
    const-string v5, "DrawableView"

    .line 123
    .line 124
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    sub-int/2addr v2, v3

    .line 128
    sub-int/2addr v1, v0

    .line 129
    invoke-static {p1, v3, v0, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    const-string v0, "createBitmap(bitmap, lef\u2026ght - left, bottom - top)"

    .line 134
    .line 135
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    return-object p1
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public O8()V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 4
    .line 5
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-ge v0, v1, :cond_4

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-gtz v0, :cond_0

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMPaintColor(I)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 30
    .line 31
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMPaintWidth(F)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇〇08O:Landroid/widget/TextView;

    .line 39
    .line 40
    if-nez v0, :cond_1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    const/16 v1, 0x8

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 46
    .line 47
    .line 48
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 51
    .line 52
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    add-int/lit8 v1, v1, -0x1

    .line 57
    .line 58
    if-ge v0, v1, :cond_2

    .line 59
    .line 60
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 61
    .line 62
    add-int/lit8 v0, v0, 0x1

    .line 63
    .line 64
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 65
    .line 66
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 67
    .line 68
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 69
    .line 70
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    const-string v1, "pathList[index]"

    .line 75
    .line 76
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    check-cast v0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;

    .line 80
    .line 81
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 82
    .line 83
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getColor()I

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    .line 89
    .line 90
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 91
    .line 92
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getSize()F

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 97
    .line 98
    .line 99
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 100
    .line 101
    if-nez v1, :cond_3

    .line 102
    .line 103
    const-string v1, "mCanvas"

    .line 104
    .line 105
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    const/4 v1, 0x0

    .line 109
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getPath()Landroid/graphics/Path;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 114
    .line 115
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 116
    .line 117
    .line 118
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 119
    .line 120
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO:I

    .line 121
    .line 122
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 126
    .line 127
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇OOo8〇0:F

    .line 128
    .line 129
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 133
    .line 134
    .line 135
    :cond_4
    :goto_1
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final getHistoryPathListSize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMBackgroundColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMOnStepChangeListener()Lcom/intsig/camscanner/newsign/handwrite/OnStepChangeListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->ooo0〇〇O:Lcom/intsig/camscanner/newsign/handwrite/OnStepChangeListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMPaintColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMPaintWidth()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇OOo8〇0:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const-string v0, "mBitmap"

    .line 15
    .line 16
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    move-object v0, v1

    .line 20
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    invoke-virtual {p1, v0, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 27
    .line 28
    if-nez p1, :cond_1

    .line 29
    .line 30
    const-string p1, "mCanvas"

    .line 31
    .line 32
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    move-object v1, p1

    .line 37
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo〇8o008:Landroid/graphics/Path;

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 40
    .line 41
    invoke-virtual {v1, p1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    sget-object p3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 13
    .line 14
    invoke-static {p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const-string p2, "createBitmap(width, heig\u2026 Bitmap.Config.ARGB_8888)"

    .line 19
    .line 20
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 24
    .line 25
    new-instance p1, Landroid/graphics/Canvas;

    .line 26
    .line 27
    iget-object p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 28
    .line 29
    if-nez p2, :cond_0

    .line 30
    .line 31
    const-string p2, "mBitmap"

    .line 32
    .line 33
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    const/4 p2, 0x0

    .line 37
    :cond_0
    invoke-direct {p1, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 38
    .line 39
    .line 40
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 41
    .line 42
    iget p2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    .line 43
    .line 44
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    const-string v0, "event"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x1

    .line 11
    if-eqz v0, :cond_7

    .line 12
    .line 13
    if-eq v0, v1, :cond_3

    .line 14
    .line 15
    const/4 v2, 0x2

    .line 16
    if-eq v0, v2, :cond_0

    .line 17
    .line 18
    goto/16 :goto_3

    .line 19
    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇〇08O:Landroid/widget/TextView;

    .line 21
    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/16 v3, 0x8

    .line 26
    .line 27
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 28
    .line 29
    .line 30
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇080OO8〇0:F

    .line 31
    .line 32
    iget v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇0O:F

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    sub-float/2addr v4, v0

    .line 39
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    .line 40
    .line 41
    .line 42
    move-result v4

    .line 43
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 44
    .line 45
    .line 46
    move-result v5

    .line 47
    sub-float/2addr v5, v3

    .line 48
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    .line 49
    .line 50
    .line 51
    move-result v5

    .line 52
    const/high16 v6, 0x40400000    # 3.0f

    .line 53
    .line 54
    cmpl-float v4, v4, v6

    .line 55
    .line 56
    if-gez v4, :cond_2

    .line 57
    .line 58
    cmpl-float v4, v5, v6

    .line 59
    .line 60
    if-ltz v4, :cond_8

    .line 61
    .line 62
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 63
    .line 64
    .line 65
    move-result v4

    .line 66
    add-float/2addr v4, v0

    .line 67
    int-to-float v2, v2

    .line 68
    div-float/2addr v4, v2

    .line 69
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 70
    .line 71
    .line 72
    move-result v5

    .line 73
    add-float/2addr v5, v3

    .line 74
    div-float/2addr v5, v2

    .line 75
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo〇8o008:Landroid/graphics/Path;

    .line 76
    .line 77
    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇080OO8〇0:F

    .line 85
    .line 86
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 87
    .line 88
    .line 89
    move-result p1

    .line 90
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇0O:F

    .line 91
    .line 92
    goto/16 :goto_3

    .line 93
    .line 94
    :cond_3
    iget p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 95
    .line 96
    const/4 v0, -0x1

    .line 97
    if-ne p1, v0, :cond_4

    .line 98
    .line 99
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 100
    .line 101
    invoke-virtual {p1}, Ljava/util/LinkedList;->clear()V

    .line 102
    .line 103
    .line 104
    goto :goto_2

    .line 105
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 106
    .line 107
    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    .line 108
    .line 109
    .line 110
    move-result p1

    .line 111
    sub-int/2addr p1, v1

    .line 112
    :goto_1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 113
    .line 114
    add-int/2addr v0, v1

    .line 115
    if-lt p1, v0, :cond_5

    .line 116
    .line 117
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 118
    .line 119
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 120
    .line 121
    .line 122
    add-int/lit8 p1, p1, -0x1

    .line 123
    .line 124
    goto :goto_1

    .line 125
    :cond_5
    :goto_2
    iget p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 126
    .line 127
    add-int/2addr p1, v1

    .line 128
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 129
    .line 130
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 131
    .line 132
    new-instance v0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;

    .line 133
    .line 134
    new-instance v2, Landroid/graphics/Path;

    .line 135
    .line 136
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo〇8o008:Landroid/graphics/Path;

    .line 137
    .line 138
    invoke-direct {v2, v3}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .line 139
    .line 140
    .line 141
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 142
    .line 143
    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    .line 144
    .line 145
    .line 146
    move-result v3

    .line 147
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 148
    .line 149
    invoke-virtual {v4}, Landroid/graphics/Paint;->getStrokeWidth()F

    .line 150
    .line 151
    .line 152
    move-result v4

    .line 153
    invoke-direct {v0, v2, v3, v4}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;-><init>(Landroid/graphics/Path;IF)V

    .line 154
    .line 155
    .line 156
    invoke-virtual {p1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 157
    .line 158
    .line 159
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 160
    .line 161
    if-nez p1, :cond_6

    .line 162
    .line 163
    const-string p1, "mCanvas"

    .line 164
    .line 165
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    const/4 p1, 0x0

    .line 169
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo〇8o008:Landroid/graphics/Path;

    .line 170
    .line 171
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 172
    .line 173
    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 174
    .line 175
    .line 176
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo〇8o008:Landroid/graphics/Path;

    .line 177
    .line 178
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 179
    .line 180
    .line 181
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->ooo0〇〇O:Lcom/intsig/camscanner/newsign/handwrite/OnStepChangeListener;

    .line 182
    .line 183
    if-eqz p1, :cond_8

    .line 184
    .line 185
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 186
    .line 187
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 188
    .line 189
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    .line 190
    .line 191
    .line 192
    move-result v2

    .line 193
    invoke-interface {p1, v0, v2}, Lcom/intsig/camscanner/newsign/handwrite/OnStepChangeListener;->〇080(II)V

    .line 194
    .line 195
    .line 196
    goto :goto_3

    .line 197
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 198
    .line 199
    .line 200
    move-result v0

    .line 201
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇080OO8〇0:F

    .line 202
    .line 203
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 204
    .line 205
    .line 206
    move-result p1

    .line 207
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇0O:F

    .line 208
    .line 209
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo〇8o008:Landroid/graphics/Path;

    .line 210
    .line 211
    iget v2, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇080OO8〇0:F

    .line 212
    .line 213
    invoke-virtual {v0, v2, p1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 214
    .line 215
    .line 216
    :cond_8
    :goto_3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 217
    .line 218
    .line 219
    return v1
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final setClearBlank(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o〇00O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setMBackgroundColor(I)V
    .locals 5

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    const-string v2, "mCanvas"

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    move-object v0, v1

    .line 14
    :cond_0
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    .line 15
    .line 16
    invoke-virtual {v0, p1, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 20
    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    move-object v0, v1

    .line 27
    :cond_1
    invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 28
    .line 29
    .line 30
    const/4 p1, 0x0

    .line 31
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 32
    .line 33
    if-gt p1, v0, :cond_3

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    const-string v3, "pathList[tmp]"

    .line 42
    .line 43
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    check-cast v0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;

    .line 47
    .line 48
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getColor()I

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    .line 56
    .line 57
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getSize()F

    .line 60
    .line 61
    .line 62
    move-result v4

    .line 63
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 64
    .line 65
    .line 66
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 67
    .line 68
    if-nez v3, :cond_2

    .line 69
    .line 70
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    move-object v3, v1

    .line 74
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getPath()Landroid/graphics/Path;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 79
    .line 80
    invoke-virtual {v3, v0, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 81
    .line 82
    .line 83
    add-int/lit8 p1, p1, 0x1

    .line 84
    .line 85
    goto :goto_0

    .line 86
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final setMOnStepChangeListener(Lcom/intsig/camscanner/newsign/handwrite/OnStepChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->ooo0〇〇O:Lcom/intsig/camscanner/newsign/handwrite/OnStepChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setMPaintColor(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setMPaintWidth(F)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇OOo8〇0:F

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setTipsView(Landroid/widget/TextView;)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "tipsView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇〇08O:Landroid/widget/TextView;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080()V
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 2
    .line 3
    if-ltz v0, :cond_7

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-gtz v0, :cond_0

    .line 12
    .line 13
    goto/16 :goto_2

    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    const-string v2, "mCanvas"

    .line 19
    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    move-object v0, v1

    .line 26
    :cond_1
    iget v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    .line 27
    .line 28
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    .line 29
    .line 30
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 34
    .line 35
    if-nez v0, :cond_2

    .line 36
    .line 37
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    move-object v0, v1

    .line 41
    :cond_2
    iget v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    .line 42
    .line 43
    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 44
    .line 45
    .line 46
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 47
    .line 48
    const/4 v3, 0x0

    .line 49
    if-nez v0, :cond_4

    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇〇08O:Landroid/widget/TextView;

    .line 52
    .line 53
    if-nez v0, :cond_3

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_3
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 57
    .line 58
    .line 59
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 60
    .line 61
    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMPaintColor(I)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 69
    .line 70
    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMPaintWidth(F)V

    .line 75
    .line 76
    .line 77
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 78
    .line 79
    add-int/lit8 v0, v0, -0x1

    .line 80
    .line 81
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 82
    .line 83
    if-ltz v0, :cond_6

    .line 84
    .line 85
    :goto_1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 86
    .line 87
    if-gt v3, v0, :cond_6

    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 90
    .line 91
    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    const-string v4, "pathList[tmp]"

    .line 96
    .line 97
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    check-cast v0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;

    .line 101
    .line 102
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 103
    .line 104
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getColor()I

    .line 105
    .line 106
    .line 107
    move-result v5

    .line 108
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 109
    .line 110
    .line 111
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 112
    .line 113
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getSize()F

    .line 114
    .line 115
    .line 116
    move-result v5

    .line 117
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 118
    .line 119
    .line 120
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 121
    .line 122
    if-nez v4, :cond_5

    .line 123
    .line 124
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    move-object v4, v1

    .line 128
    :cond_5
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView$PathItem;->getPath()Landroid/graphics/Path;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    iget-object v5, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 133
    .line 134
    invoke-virtual {v4, v0, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 135
    .line 136
    .line 137
    add-int/lit8 v3, v3, 0x1

    .line 138
    .line 139
    goto :goto_1

    .line 140
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 141
    .line 142
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO:I

    .line 143
    .line 144
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 145
    .line 146
    .line 147
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 148
    .line 149
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇OOo8〇0:F

    .line 150
    .line 151
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 152
    .line 153
    .line 154
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 155
    .line 156
    .line 157
    :cond_7
    :goto_2
    return-void
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇o00〇〇Oo()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇〇08O:Landroid/widget/TextView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    .line 9
    .line 10
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    const-string v2, "mCanvas"

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    move-object v0, v1

    .line 21
    :cond_1
    iget v3, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    .line 22
    .line 23
    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    .line 24
    .line 25
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o8〇OO0〇0o:Landroid/graphics/Canvas;

    .line 29
    .line 30
    if-nez v0, :cond_2

    .line 31
    .line 32
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_2
    move-object v1, v0

    .line 37
    :goto_1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇08O〇00〇o:I

    .line 38
    .line 39
    invoke-virtual {v1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 43
    .line 44
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO:I

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8o08O8O:Landroid/graphics/Paint;

    .line 50
    .line 51
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇OOo8〇0:F

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->oOo0:Ljava/util/LinkedList;

    .line 57
    .line 58
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 59
    .line 60
    .line 61
    const/4 v0, -0x1

    .line 62
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->OO〇00〇8oO:I

    .line 63
    .line 64
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
.end method

.method public 〇〇888(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const-string v0, "path"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->o〇00O:Z

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    const-string v2, "mBitmap"

    .line 10
    .line 11
    const/4 v3, 0x1

    .line 12
    if-ne v0, v3, :cond_1

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    move-object v1, v0

    .line 23
    :goto_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇o〇(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    goto :goto_1

    .line 28
    :cond_1
    if-nez v0, :cond_4

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 31
    .line 32
    if-nez v0, :cond_2

    .line 33
    .line 34
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_2
    move-object v1, v0

    .line 39
    :goto_1
    const/4 v0, 0x0

    .line 40
    invoke-static {v1, v0, v3}, Lcom/intsig/camscanner/doodle/util/DoodleImageUtils;->〇o〇(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    const-string v1, "rotate(bitmap, 0, true)"

    .line 45
    .line 46
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    .line 50
    .line 51
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 52
    .line 53
    .line 54
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    .line 55
    .line 56
    const/16 v3, 0x5a

    .line 57
    .line 58
    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    const-string v1, "bos.toByteArray()"

    .line 66
    .line 67
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    new-instance v1, Ljava/io/File;

    .line 71
    .line 72
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    if-eqz p1, :cond_3

    .line 80
    .line 81
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 82
    .line 83
    .line 84
    :cond_3
    new-instance p1, Ljava/io/FileOutputStream;

    .line 85
    .line 86
    invoke-direct {p1, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 93
    .line 94
    .line 95
    return-void

    .line 96
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 97
    .line 98
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 99
    .line 100
    .line 101
    throw p1
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

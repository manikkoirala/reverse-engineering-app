.class public Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;
.super Landroid/view/View;
.source "WriteLineView.java"


# instance fields
.field private O8o08O8O:F

.field private OO:F

.field private o0:F

.field private oOo0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private oOo〇8o008:F

.field private o〇00O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:Landroid/graphics/Paint;

.field private 〇0O:Landroid/graphics/Path;

.field private 〇OOo8〇0:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const p1, 0x3eb851ec    # 0.36f

    .line 5
    .line 6
    .line 7
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->oOo〇8o008:F

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇o00〇〇Oo()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O8(Landroid/graphics/Canvas;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->oOo0:Ljava/util/ArrayList;

    .line 4
    .line 5
    if-eqz v1, :cond_4

    .line 6
    .line 7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    goto/16 :goto_2

    .line 14
    .line 15
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    iget-object v2, v0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->oOo0:Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 23
    .line 24
    .line 25
    new-instance v2, Landroid/graphics/Path;

    .line 26
    .line 27
    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v2, v0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇0O:Landroid/graphics/Path;

    .line 31
    .line 32
    const/4 v3, 0x0

    .line 33
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    check-cast v4, Landroid/graphics/PointF;

    .line 38
    .line 39
    iget v4, v4, Landroid/graphics/PointF;->x:F

    .line 40
    .line 41
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    check-cast v3, Landroid/graphics/PointF;

    .line 46
    .line 47
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 48
    .line 49
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 50
    .line 51
    .line 52
    const/4 v2, 0x0

    .line 53
    const/4 v3, 0x1

    .line 54
    const/4 v3, 0x0

    .line 55
    const/4 v4, 0x1

    .line 56
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 57
    .line 58
    .line 59
    move-result v5

    .line 60
    if-ge v4, v5, :cond_3

    .line 61
    .line 62
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    check-cast v5, Landroid/graphics/PointF;

    .line 67
    .line 68
    add-int/lit8 v6, v4, -0x1

    .line 69
    .line 70
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object v6

    .line 74
    check-cast v6, Landroid/graphics/PointF;

    .line 75
    .line 76
    iget v7, v6, Landroid/graphics/PointF;->x:F

    .line 77
    .line 78
    add-float v9, v7, v2

    .line 79
    .line 80
    iget v2, v6, Landroid/graphics/PointF;->y:F

    .line 81
    .line 82
    add-float v10, v2, v3

    .line 83
    .line 84
    add-int/lit8 v3, v4, 0x1

    .line 85
    .line 86
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 87
    .line 88
    .line 89
    move-result v2

    .line 90
    if-ge v3, v2, :cond_1

    .line 91
    .line 92
    move v4, v3

    .line 93
    :cond_1
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    check-cast v2, Landroid/graphics/PointF;

    .line 98
    .line 99
    iget v4, v2, Landroid/graphics/PointF;->x:F

    .line 100
    .line 101
    iget v7, v6, Landroid/graphics/PointF;->x:F

    .line 102
    .line 103
    sub-float/2addr v4, v7

    .line 104
    const/high16 v7, 0x40000000    # 2.0f

    .line 105
    .line 106
    div-float/2addr v4, v7

    .line 107
    iget v8, v0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->oOo〇8o008:F

    .line 108
    .line 109
    mul-float v4, v4, v8

    .line 110
    .line 111
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 112
    .line 113
    iget v6, v6, Landroid/graphics/PointF;->y:F

    .line 114
    .line 115
    sub-float/2addr v2, v6

    .line 116
    div-float/2addr v2, v7

    .line 117
    mul-float v2, v2, v8

    .line 118
    .line 119
    iget v13, v5, Landroid/graphics/PointF;->x:F

    .line 120
    .line 121
    sub-float v11, v13, v4

    .line 122
    .line 123
    iget v14, v5, Landroid/graphics/PointF;->y:F

    .line 124
    .line 125
    sub-float v5, v14, v2

    .line 126
    .line 127
    cmpl-float v6, v10, v14

    .line 128
    .line 129
    if-nez v6, :cond_2

    .line 130
    .line 131
    move v12, v10

    .line 132
    goto :goto_1

    .line 133
    :cond_2
    move v12, v5

    .line 134
    :goto_1
    iget-object v8, v0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇0O:Landroid/graphics/Path;

    .line 135
    .line 136
    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 137
    .line 138
    .line 139
    move v15, v3

    .line 140
    move v3, v2

    .line 141
    move v2, v4

    .line 142
    move v4, v15

    .line 143
    goto :goto_0

    .line 144
    :cond_3
    iget-object v1, v0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇0O:Landroid/graphics/Path;

    .line 145
    .line 146
    iget-object v2, v0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 147
    .line 148
    move-object/from16 v3, p1

    .line 149
    .line 150
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 151
    .line 152
    .line 153
    :cond_4
    :goto_2
    return-void
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private Oo08()V
    .locals 7

    .line 1
    const/high16 v0, 0x41500000    # 13.0f

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-instance v1, Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->oOo0:Ljava/util/ArrayList;

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    :goto_0
    const/4 v2, 0x6

    .line 16
    if-ge v1, v2, :cond_0

    .line 17
    .line 18
    iget v2, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇080OO8〇0:F

    .line 19
    .line 20
    mul-int/lit8 v3, v1, 0x4

    .line 21
    .line 22
    int-to-float v3, v3

    .line 23
    mul-float v3, v3, v2

    .line 24
    .line 25
    add-float/2addr v2, v3

    .line 26
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 27
    .line 28
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    check-cast v3, Ljava/lang/Integer;

    .line 33
    .line 34
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    int-to-float v3, v3

    .line 39
    iget v4, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o0:F

    .line 40
    .line 41
    iget v5, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->O8o08O8O:F

    .line 42
    .line 43
    mul-float v5, v5, v3

    .line 44
    .line 45
    sub-float/2addr v4, v5

    .line 46
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->oOo0:Ljava/util/ArrayList;

    .line 47
    .line 48
    new-instance v5, Landroid/graphics/PointF;

    .line 49
    .line 50
    int-to-float v6, v0

    .line 51
    add-float/2addr v2, v6

    .line 52
    invoke-direct {v5, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    add-int/lit8 v1, v1, 0x1

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇o00〇〇Oo()V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 5
    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 8
    .line 9
    const-string v1, "#000000"

    .line 10
    .line 11
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 19
    .line 20
    const/high16 v1, 0x40000000    # 2.0f

    .line 21
    .line 22
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    int-to-float v1, v1

    .line 27
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 31
    .line 32
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 38
    .line 39
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇o〇()V
    .locals 3

    .line 1
    const/high16 v0, 0x42c80000    # 100.0f

    .line 2
    .line 3
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    const/4 v1, 0x6

    .line 7
    if-ge v0, v1, :cond_1

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    check-cast v2, Ljava/lang/Integer;

    .line 18
    .line 19
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    int-to-float v2, v2

    .line 24
    cmpg-float v1, v1, v2

    .line 25
    .line 26
    if-gtz v1, :cond_0

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 29
    .line 30
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    check-cast v1, Ljava/lang/Integer;

    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    int-to-float v1, v1

    .line 41
    iput v1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 42
    .line 43
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 47
    .line 48
    const/high16 v1, 0x41a00000    # 20.0f

    .line 49
    .line 50
    cmpg-float v0, v0, v1

    .line 51
    .line 52
    if-gez v0, :cond_2

    .line 53
    .line 54
    iput v1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 55
    .line 56
    :cond_2
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o0:F

    .line 57
    .line 58
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 59
    .line 60
    div-float/2addr v0, v1

    .line 61
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->O8o08O8O:F

    .line 62
    .line 63
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇OOo8〇0:F

    .line 64
    .line 65
    const/high16 v1, 0x41e00000    # 28.0f

    .line 66
    .line 67
    div-float/2addr v0, v1

    .line 68
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇080OO8〇0:F

    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public oO80()V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 7
    .line 8
    const/16 v1, 0x14

    .line 9
    .line 10
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 18
    .line 19
    const/16 v2, 0x3c

    .line 20
    .line 21
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 34
    .line 35
    const/16 v1, 0x32

    .line 36
    .line 37
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 45
    .line 46
    const/16 v2, 0x1e

    .line 47
    .line 48
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 56
    .line 57
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o〇00O:Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇080(Ljava/util/List;)F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇o〇()V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->Oo08()V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->O8(Landroid/graphics/Canvas;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    int-to-float p1, p1

    .line 9
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->o0:F

    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    int-to-float p1, p1

    .line 16
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇OOo8〇0:F

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method o〇0(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 2
    .line 3
    int-to-float p1, p1

    .line 4
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080(Ljava/util/List;)F
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)F"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    :goto_0
    const/4 v1, 0x6

    .line 6
    if-ge v0, v1, :cond_1

    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 9
    .line 10
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    check-cast v2, Ljava/lang/Integer;

    .line 15
    .line 16
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    int-to-float v2, v2

    .line 21
    cmpg-float v1, v1, v2

    .line 22
    .line 23
    if-gez v1, :cond_0

    .line 24
    .line 25
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    check-cast v1, Ljava/lang/Integer;

    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    int-to-float v1, v1

    .line 36
    iput v1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 37
    .line 38
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    iget p1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 42
    .line 43
    const/high16 v0, 0x41200000    # 10.0f

    .line 44
    .line 45
    cmpg-float p1, p1, v0

    .line 46
    .line 47
    if-gtz p1, :cond_2

    .line 48
    .line 49
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 50
    .line 51
    :cond_2
    iget p1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 52
    .line 53
    float-to-double v0, p1

    .line 54
    const-wide v2, 0x3ff3333333333333L    # 1.2

    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    mul-double v0, v0, v2

    .line 60
    .line 61
    double-to-float p1, v0

    .line 62
    iput p1, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->OO:F

    .line 63
    .line 64
    return p1
    .line 65
    .line 66
    .line 67
.end method

.method 〇〇888(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇08O〇00〇o:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public final Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "HandWriteSignActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8o:Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oo8ooo8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0O:I

.field private O88O:Landroid/view/View;

.field private final o8oOOo:I

.field private final oOO〇〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooo0〇〇O:Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;

.field private final 〇O〇〇O8:I

.field private 〇o0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇08O:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o8o:Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "HandWriteSignActivity::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->oo8ooo8O:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const/high16 v0, -0x1000000

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇〇08O:I

    .line 7
    .line 8
    const-string v0, "#19BC9C"

    .line 9
    .line 10
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O0O:I

    .line 15
    .line 16
    const-string v0, "#FF9312"

    .line 17
    .line 18
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o8oOOo:I

    .line 23
    .line 24
    const-string v0, "#FF6161"

    .line 25
    .line 26
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    iput v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇O〇〇O8:I

    .line 31
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇o0O:Ljava/util/ArrayList;

    .line 38
    .line 39
    new-instance v0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$loadingDialog$2;

    .line 40
    .line 41
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$loadingDialog$2;-><init>(Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;)V

    .line 42
    .line 43
    .line 44
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->oOO〇〇:Lkotlin/Lazy;

    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O0〇(Landroid/widget/CheckBox;Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇ooO〇000(Landroid/widget/CheckBox;Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final O880O〇(Lcom/intsig/camscanner/newsign/handwrite/DrawableView;)V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$saveSignature$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, p1, v4}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$saveSignature$1;-><init>(Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;Lcom/intsig/camscanner/newsign/handwrite/DrawableView;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method private final O8O()V
    .locals 5

    .line 1
    const-string v0, "signature"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignCompliance;->〇o〇(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const v1, 0x7f0d0259

    .line 11
    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const v1, 0x7f0a0315

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const-string v2, "null cannot be cast to non-null type android.widget.CheckBox"

    .line 26
    .line 27
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    check-cast v1, Landroid/widget/CheckBox;

    .line 31
    .line 32
    const v2, 0x7f130014

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 36
    .line 37
    .line 38
    const/4 v2, 0x0

    .line 39
    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 40
    .line 41
    .line 42
    const v2, 0x7f0a1960

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    const-string v3, "null cannot be cast to non-null type android.widget.TextView"

    .line 50
    .line 51
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    check-cast v2, Landroid/widget/TextView;

    .line 55
    .line 56
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    const v4, 0x7f131e8a

    .line 61
    .line 62
    .line 63
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    .line 69
    .line 70
    sget-object v3, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->〇080:Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;

    .line 71
    .line 72
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->O8()Z

    .line 73
    .line 74
    .line 75
    move-result v3

    .line 76
    if-nez v3, :cond_0

    .line 77
    .line 78
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    invoke-virtual {v3}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    .line 83
    .line 84
    .line 85
    move-result v3

    .line 86
    if-eqz v3, :cond_1

    .line 87
    .line 88
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 89
    .line 90
    .line 91
    move-result-object v3

    .line 92
    const v4, 0x7f131234

    .line 93
    .line 94
    .line 95
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v3

    .line 99
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    .line 101
    .line 102
    :cond_1
    new-instance v2, Lcom/intsig/app/AlertDialog$Builder;

    .line 103
    .line 104
    invoke-direct {v2, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    const v4, 0x7f1300a9

    .line 112
    .line 113
    .line 114
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v3

    .line 118
    invoke-virtual {v2, v3}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 119
    .line 120
    .line 121
    move-result-object v2

    .line 122
    invoke-virtual {v2, v0}, Lcom/intsig/app/AlertDialog$Builder;->oO(Landroid/view/View;)Lcom/intsig/app/AlertDialog$Builder;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 127
    .line 128
    .line 129
    move-result-object v2

    .line 130
    const v3, 0x7f130019

    .line 131
    .line 132
    .line 133
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    new-instance v3, Lo〇0〇o/〇080;

    .line 138
    .line 139
    invoke-direct {v3, v1, p0}, Lo〇0〇o/〇080;-><init>(Landroid/widget/CheckBox;Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v0, v2, v3}, Lcom/intsig/app/AlertDialog$Builder;->oo〇(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 151
    .line 152
    .line 153
    return-void
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final OO0O(Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "extra_path"

    .line 7
    .line 8
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 9
    .line 10
    .line 11
    const/4 p1, -0x1

    .line 12
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method private final OO〇〇o0oO(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O88O:Landroid/view/View;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 8
    .line 9
    .line 10
    :goto_0
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O88O:Landroid/view/View;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OooO〇(II)V
    .locals 2

    .line 1
    const v0, 0x7f060206

    .line 2
    .line 3
    .line 4
    if-lez p2, :cond_0

    .line 5
    .line 6
    add-int/lit8 p2, p2, -0x1

    .line 7
    .line 8
    if-ge p1, p2, :cond_0

    .line 9
    .line 10
    const v0, 0x7f060208

    .line 11
    .line 12
    .line 13
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const p2, 0x7f08080d

    .line 18
    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-static {p1, p2, v1}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    if-eqz p1, :cond_1

    .line 26
    .line 27
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    if-eqz p1, :cond_1

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    if-eqz p1, :cond_1

    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 46
    .line 47
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 52
    .line 53
    .line 54
    move-result p2

    .line 55
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    .line 56
    .line 57
    invoke-virtual {p1, p2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 58
    .line 59
    .line 60
    iget-object p2, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;

    .line 61
    .line 62
    if-eqz p2, :cond_1

    .line 63
    .line 64
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 65
    .line 66
    if-eqz p2, :cond_1

    .line 67
    .line 68
    invoke-virtual {p2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    .line 70
    .line 71
    :cond_1
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic O〇080〇o0(Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o〇08oO80o()Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OO0O(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O〇〇O80o8(Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o0Oo()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->oo8ooo8O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o808o8o08(Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OooO〇(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇08oO80o()Lcom/intsig/app/BaseProgressDialog;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->oOO〇〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-loadingDialog>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Lcom/intsig/app/BaseProgressDialog;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇o08〇(II)V
    .locals 2

    .line 1
    const v0, 0x7f060206

    .line 2
    .line 3
    .line 4
    if-lez p2, :cond_0

    .line 5
    .line 6
    const/4 p2, -0x1

    .line 7
    if-le p1, p2, :cond_0

    .line 8
    .line 9
    const v0, 0x7f060208

    .line 10
    .line 11
    .line 12
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const p2, 0x7f080545

    .line 17
    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-static {p1, p2, v1}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    if-eqz p1, :cond_1

    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    if-eqz p1, :cond_1

    .line 31
    .line 32
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    if-eqz p1, :cond_1

    .line 37
    .line 38
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    if-eqz p1, :cond_1

    .line 43
    .line 44
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 45
    .line 46
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 47
    .line 48
    .line 49
    move-result-object p2

    .line 50
    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 51
    .line 52
    .line 53
    move-result p2

    .line 54
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    .line 55
    .line 56
    invoke-virtual {p1, p2, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 57
    .line 58
    .line 59
    iget-object p2, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;

    .line 60
    .line 61
    if-eqz p2, :cond_1

    .line 62
    .line 63
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 64
    .line 65
    if-eqz p2, :cond_1

    .line 66
    .line 67
    invoke-virtual {p2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 68
    .line 69
    .line 70
    :cond_1
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o〇o08〇(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇ooO8Ooo〇(Landroid/view/View;I)V
    .locals 3

    .line 1
    const/4 v0, -0x1

    .line 2
    if-ne p2, v0, :cond_0

    .line 3
    .line 4
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 5
    .line 6
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const v2, 0x7f080209

    .line 11
    .line 12
    .line 13
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const v2, 0x7f080208

    .line 25
    .line 26
    .line 27
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 32
    .line 33
    .line 34
    if-eq p2, v0, :cond_3

    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    if-eqz p1, :cond_1

    .line 41
    .line 42
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    goto :goto_1

    .line 47
    :cond_1
    const/4 p1, 0x0

    .line 48
    :goto_1
    if-nez p1, :cond_2

    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_2
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    .line 52
    .line 53
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    .line 54
    .line 55
    invoke-direct {v0, p2, v1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 59
    .line 60
    .line 61
    :cond_3
    :goto_2
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇ooO〇000(Landroid/widget/CheckBox;Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p2, "$checkBox"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 12
    .line 13
    .line 14
    move-result p0

    .line 15
    const-string p2, "signature"

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-static {p2}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignCompliance;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 p0, 0x0

    .line 23
    invoke-static {p0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0oOO0(Z)V

    .line 24
    .line 25
    .line 26
    :cond_0
    iget-object p0, p1, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;

    .line 27
    .line 28
    if-nez p0, :cond_1

    .line 29
    .line 30
    return-void

    .line 31
    :cond_1
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 32
    .line 33
    const-string p3, "vb.drawView"

    .line 34
    .line 35
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O880O〇(Lcom/intsig/camscanner/newsign/handwrite/DrawableView;)V

    .line 39
    .line 40
    .line 41
    invoke-static {p2}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignCompliance;->〇080(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method


# virtual methods
.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-static {p1, v0}, Lcom/intsig/utils/SystemUiUtil;->o〇0(Landroid/view/Window;Z)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o〇00O:Landroid/view/View;

    .line 10
    .line 11
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;

    .line 16
    .line 17
    if-nez p1, :cond_0

    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 21
    .line 22
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 23
    .line 24
    const-string v2, "vb.tvTips"

    .line 25
    .line 26
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setTipsView(Landroid/widget/TextView;)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 33
    .line 34
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->oOo〇8o008:Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->oO80()V

    .line 37
    .line 38
    .line 39
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 40
    .line 41
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇0O:Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 42
    .line 43
    const/16 v1, 0x64

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 49
    .line 50
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇0O:Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 51
    .line 52
    new-instance v1, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$initialize$1;

    .line 53
    .line 54
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$initialize$1;-><init>(Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 61
    .line 62
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇0O:Landroidx/appcompat/widget/AppCompatSeekBar;

    .line 63
    .line 64
    const/16 v1, 0x14

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇o0O:Ljava/util/ArrayList;

    .line 70
    .line 71
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 72
    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇o0O:Ljava/util/ArrayList;

    .line 75
    .line 76
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 77
    .line 78
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->OO:Landroid/view/View;

    .line 79
    .line 80
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇o0O:Ljava/util/ArrayList;

    .line 84
    .line 85
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 86
    .line 87
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇o0O:Ljava/util/ArrayList;

    .line 93
    .line 94
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 95
    .line 96
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->o〇00O:Landroid/view/View;

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇o0O:Ljava/util/ArrayList;

    .line 102
    .line 103
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 104
    .line 105
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->O8o08O8O:Landroid/view/View;

    .line 106
    .line 107
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    .line 109
    .line 110
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 111
    .line 112
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->OO:Landroid/view/View;

    .line 113
    .line 114
    const-string v1, "vb.layoutTools.color1"

    .line 115
    .line 116
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    iget v2, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇〇08O:I

    .line 120
    .line 121
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇ooO8Ooo〇(Landroid/view/View;I)V

    .line 122
    .line 123
    .line 124
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 125
    .line 126
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 127
    .line 128
    const-string v2, "vb.layoutTools.color2"

    .line 129
    .line 130
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    iget v2, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O0O:I

    .line 134
    .line 135
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇ooO8Ooo〇(Landroid/view/View;I)V

    .line 136
    .line 137
    .line 138
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 139
    .line 140
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->o〇00O:Landroid/view/View;

    .line 141
    .line 142
    const-string v2, "vb.layoutTools.color3"

    .line 143
    .line 144
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    iget v2, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o8oOOo:I

    .line 148
    .line 149
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇ooO8Ooo〇(Landroid/view/View;I)V

    .line 150
    .line 151
    .line 152
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 153
    .line 154
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->O8o08O8O:Landroid/view/View;

    .line 155
    .line 156
    const-string v2, "vb.layoutTools.color4"

    .line 157
    .line 158
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    iget v2, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇O〇〇O8:I

    .line 162
    .line 163
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇ooO8Ooo〇(Landroid/view/View;I)V

    .line 164
    .line 165
    .line 166
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 167
    .line 168
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->OO:Landroid/view/View;

    .line 169
    .line 170
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    .line 172
    .line 173
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OO〇〇o0oO(Landroid/view/View;)V

    .line 174
    .line 175
    .line 176
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 177
    .line 178
    new-instance v0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$initialize$2;

    .line 179
    .line 180
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity$initialize$2;-><init>(Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;)V

    .line 181
    .line 182
    .line 183
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMOnStepChangeListener(Lcom/intsig/camscanner/newsign/handwrite/OnStepChangeListener;)V

    .line 184
    .line 185
    .line 186
    return-void
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public o00〇88〇08()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/16 v1, 0xc

    .line 7
    .line 8
    new-array v1, v1, [Landroid/view/View;

    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 12
    .line 13
    aput-object v3, v1, v2

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 17
    .line 18
    aput-object v3, v1, v2

    .line 19
    .line 20
    const/4 v2, 0x2

    .line 21
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 22
    .line 23
    aput-object v3, v1, v2

    .line 24
    .line 25
    const/4 v2, 0x3

    .line 26
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 27
    .line 28
    aput-object v3, v1, v2

    .line 29
    .line 30
    const/4 v2, 0x4

    .line 31
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 32
    .line 33
    aput-object v3, v1, v2

    .line 34
    .line 35
    const/4 v2, 0x5

    .line 36
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 37
    .line 38
    aput-object v3, v1, v2

    .line 39
    .line 40
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 41
    .line 42
    iget-object v3, v2, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇OOo8〇0:Lcom/github/happlebubble/BubbleLayout;

    .line 43
    .line 44
    const/4 v4, 0x6

    .line 45
    aput-object v3, v1, v4

    .line 46
    .line 47
    const/4 v3, 0x7

    .line 48
    iget-object v4, v2, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->OO:Landroid/view/View;

    .line 49
    .line 50
    aput-object v4, v1, v3

    .line 51
    .line 52
    const/16 v3, 0x8

    .line 53
    .line 54
    iget-object v4, v2, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 55
    .line 56
    aput-object v4, v1, v3

    .line 57
    .line 58
    const/16 v3, 0x9

    .line 59
    .line 60
    iget-object v4, v2, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->o〇00O:Landroid/view/View;

    .line 61
    .line 62
    aput-object v4, v1, v3

    .line 63
    .line 64
    const/16 v3, 0xa

    .line 65
    .line 66
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->O8o08O8O:Landroid/view/View;

    .line 67
    .line 68
    aput-object v2, v1, v3

    .line 69
    .line 70
    const/16 v2, 0xb

    .line 71
    .line 72
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 73
    .line 74
    aput-object v0, v1, v2

    .line 75
    .line 76
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇O8〇8O0oO([Landroid/view/View;)V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    if-eqz p1, :cond_1

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    goto :goto_0

    .line 17
    :cond_1
    const/4 p1, 0x0

    .line 18
    :goto_0
    if-nez p1, :cond_2

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    const v2, 0x7f0a12be

    .line 26
    .line 27
    .line 28
    if-ne v1, v2, :cond_3

    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇00()V

    .line 31
    .line 32
    .line 33
    goto/16 :goto_c

    .line 34
    .line 35
    :cond_3
    :goto_1
    if-nez p1, :cond_4

    .line 36
    .line 37
    goto :goto_2

    .line 38
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    const v2, 0x7f0a01da

    .line 43
    .line 44
    .line 45
    if-eq v1, v2, :cond_1b

    .line 46
    .line 47
    :goto_2
    if-nez p1, :cond_5

    .line 48
    .line 49
    goto :goto_3

    .line 50
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    const v2, 0x7f0a1369

    .line 55
    .line 56
    .line 57
    if-ne v1, v2, :cond_6

    .line 58
    .line 59
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 60
    .line 61
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇o00〇〇Oo()V

    .line 62
    .line 63
    .line 64
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getIndex()I

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 71
    .line 72
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getHistoryPathListSize()I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OooO〇(II)V

    .line 77
    .line 78
    .line 79
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getIndex()I

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getHistoryPathListSize()I

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o〇o08〇(II)V

    .line 92
    .line 93
    .line 94
    goto/16 :goto_c

    .line 95
    .line 96
    :cond_6
    :goto_3
    if-nez p1, :cond_7

    .line 97
    .line 98
    goto :goto_4

    .line 99
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    const v2, 0x7f0a18db

    .line 104
    .line 105
    .line 106
    if-ne v1, v2, :cond_8

    .line 107
    .line 108
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->O8()V

    .line 111
    .line 112
    .line 113
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 114
    .line 115
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getIndex()I

    .line 116
    .line 117
    .line 118
    move-result p1

    .line 119
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 120
    .line 121
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getHistoryPathListSize()I

    .line 122
    .line 123
    .line 124
    move-result v1

    .line 125
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OooO〇(II)V

    .line 126
    .line 127
    .line 128
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 129
    .line 130
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getIndex()I

    .line 131
    .line 132
    .line 133
    move-result p1

    .line 134
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 135
    .line 136
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getHistoryPathListSize()I

    .line 137
    .line 138
    .line 139
    move-result v0

    .line 140
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o〇o08〇(II)V

    .line 141
    .line 142
    .line 143
    goto/16 :goto_c

    .line 144
    .line 145
    :cond_8
    :goto_4
    if-nez p1, :cond_9

    .line 146
    .line 147
    goto :goto_5

    .line 148
    :cond_9
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 149
    .line 150
    .line 151
    move-result v1

    .line 152
    const v2, 0x7f0a18a1

    .line 153
    .line 154
    .line 155
    if-ne v1, v2, :cond_a

    .line 156
    .line 157
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 158
    .line 159
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->〇080()V

    .line 160
    .line 161
    .line 162
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 163
    .line 164
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getIndex()I

    .line 165
    .line 166
    .line 167
    move-result p1

    .line 168
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 169
    .line 170
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getHistoryPathListSize()I

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OooO〇(II)V

    .line 175
    .line 176
    .line 177
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 178
    .line 179
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getIndex()I

    .line 180
    .line 181
    .line 182
    move-result p1

    .line 183
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 184
    .line 185
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getHistoryPathListSize()I

    .line 186
    .line 187
    .line 188
    move-result v0

    .line 189
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o〇o08〇(II)V

    .line 190
    .line 191
    .line 192
    goto/16 :goto_c

    .line 193
    .line 194
    :cond_a
    :goto_5
    const/4 v1, 0x0

    .line 195
    const-string v2, "vb.layoutTools.blContent"

    .line 196
    .line 197
    if-nez p1, :cond_b

    .line 198
    .line 199
    goto :goto_6

    .line 200
    :cond_b
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 201
    .line 202
    .line 203
    move-result v3

    .line 204
    const v4, 0x7f0a1317

    .line 205
    .line 206
    .line 207
    if-ne v3, v4, :cond_d

    .line 208
    .line 209
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 210
    .line 211
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇OOo8〇0:Lcom/github/happlebubble/BubbleLayout;

    .line 212
    .line 213
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 217
    .line 218
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇OOo8〇0:Lcom/github/happlebubble/BubbleLayout;

    .line 219
    .line 220
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 221
    .line 222
    .line 223
    move-result v0

    .line 224
    if-eqz v0, :cond_c

    .line 225
    .line 226
    const/4 v1, 0x1

    .line 227
    :cond_c
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 228
    .line 229
    .line 230
    goto/16 :goto_c

    .line 231
    .line 232
    :cond_d
    :goto_6
    if-nez p1, :cond_e

    .line 233
    .line 234
    goto :goto_7

    .line 235
    :cond_e
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 236
    .line 237
    .line 238
    move-result v3

    .line 239
    const v4, 0x7f0a0b20

    .line 240
    .line 241
    .line 242
    if-ne v3, v4, :cond_f

    .line 243
    .line 244
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 245
    .line 246
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇OOo8〇0:Lcom/github/happlebubble/BubbleLayout;

    .line 247
    .line 248
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    .line 250
    .line 251
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 252
    .line 253
    .line 254
    goto/16 :goto_c

    .line 255
    .line 256
    :cond_f
    :goto_7
    if-nez p1, :cond_10

    .line 257
    .line 258
    goto :goto_8

    .line 259
    :cond_10
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 260
    .line 261
    .line 262
    move-result v1

    .line 263
    const v2, 0x7f0a047b

    .line 264
    .line 265
    .line 266
    if-ne v1, v2, :cond_11

    .line 267
    .line 268
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 269
    .line 270
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->OO:Landroid/view/View;

    .line 271
    .line 272
    const-string v1, "vb.layoutTools.color1"

    .line 273
    .line 274
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    .line 276
    .line 277
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OO〇〇o0oO(Landroid/view/View;)V

    .line 278
    .line 279
    .line 280
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 281
    .line 282
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇〇08O:I

    .line 283
    .line 284
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMPaintColor(I)V

    .line 285
    .line 286
    .line 287
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 288
    .line 289
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->oOo〇8o008:Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;

    .line 290
    .line 291
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇〇08O:I

    .line 292
    .line 293
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇〇888(I)V

    .line 294
    .line 295
    .line 296
    goto/16 :goto_c

    .line 297
    .line 298
    :cond_11
    :goto_8
    if-nez p1, :cond_12

    .line 299
    .line 300
    goto :goto_9

    .line 301
    :cond_12
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 302
    .line 303
    .line 304
    move-result v1

    .line 305
    const v2, 0x7f0a047c

    .line 306
    .line 307
    .line 308
    if-ne v1, v2, :cond_13

    .line 309
    .line 310
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 311
    .line 312
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 313
    .line 314
    const-string v1, "vb.layoutTools.color2"

    .line 315
    .line 316
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    .line 318
    .line 319
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OO〇〇o0oO(Landroid/view/View;)V

    .line 320
    .line 321
    .line 322
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 323
    .line 324
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O0O:I

    .line 325
    .line 326
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMPaintColor(I)V

    .line 327
    .line 328
    .line 329
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 330
    .line 331
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->oOo〇8o008:Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;

    .line 332
    .line 333
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O0O:I

    .line 334
    .line 335
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇〇888(I)V

    .line 336
    .line 337
    .line 338
    goto/16 :goto_c

    .line 339
    .line 340
    :cond_13
    :goto_9
    if-nez p1, :cond_14

    .line 341
    .line 342
    goto :goto_a

    .line 343
    :cond_14
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 344
    .line 345
    .line 346
    move-result v1

    .line 347
    const v2, 0x7f0a047d

    .line 348
    .line 349
    .line 350
    if-ne v1, v2, :cond_15

    .line 351
    .line 352
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 353
    .line 354
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->o〇00O:Landroid/view/View;

    .line 355
    .line 356
    const-string v1, "vb.layoutTools.color3"

    .line 357
    .line 358
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 359
    .line 360
    .line 361
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OO〇〇o0oO(Landroid/view/View;)V

    .line 362
    .line 363
    .line 364
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 365
    .line 366
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o8oOOo:I

    .line 367
    .line 368
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMPaintColor(I)V

    .line 369
    .line 370
    .line 371
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 372
    .line 373
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->oOo〇8o008:Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;

    .line 374
    .line 375
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->o8oOOo:I

    .line 376
    .line 377
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇〇888(I)V

    .line 378
    .line 379
    .line 380
    goto :goto_c

    .line 381
    :cond_15
    :goto_a
    if-nez p1, :cond_16

    .line 382
    .line 383
    goto :goto_b

    .line 384
    :cond_16
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 385
    .line 386
    .line 387
    move-result v1

    .line 388
    const v2, 0x7f0a047e

    .line 389
    .line 390
    .line 391
    if-ne v1, v2, :cond_17

    .line 392
    .line 393
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 394
    .line 395
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->O8o08O8O:Landroid/view/View;

    .line 396
    .line 397
    const-string v1, "vb.layoutTools.color4"

    .line 398
    .line 399
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 400
    .line 401
    .line 402
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->OO〇〇o0oO(Landroid/view/View;)V

    .line 403
    .line 404
    .line 405
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 406
    .line 407
    iget v1, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇O〇〇O8:I

    .line 408
    .line 409
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->setMPaintColor(I)V

    .line 410
    .line 411
    .line 412
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;

    .line 413
    .line 414
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/PopSignToolConfigBinding;->oOo〇8o008:Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;

    .line 415
    .line 416
    iget v0, p0, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->〇O〇〇O8:I

    .line 417
    .line 418
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/newsign/handwrite/WriteLineView;->〇〇888(I)V

    .line 419
    .line 420
    .line 421
    goto :goto_c

    .line 422
    :cond_17
    :goto_b
    if-nez p1, :cond_18

    .line 423
    .line 424
    goto :goto_c

    .line 425
    :cond_18
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 426
    .line 427
    .line 428
    move-result p1

    .line 429
    const v1, 0x7f0a1313

    .line 430
    .line 431
    .line 432
    if-ne p1, v1, :cond_1b

    .line 433
    .line 434
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 435
    .line 436
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/handwrite/DrawableView;->getIndex()I

    .line 437
    .line 438
    .line 439
    move-result p1

    .line 440
    if-gez p1, :cond_19

    .line 441
    .line 442
    sget-object p1, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->oo8ooo8O:Ljava/lang/String;

    .line 443
    .line 444
    const-string v0, "draw nothing, can\'t save"

    .line 445
    .line 446
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    .line 448
    .line 449
    return-void

    .line 450
    :cond_19
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o88()Z

    .line 451
    .line 452
    .line 453
    move-result p1

    .line 454
    if-eqz p1, :cond_1a

    .line 455
    .line 456
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O8O()V

    .line 457
    .line 458
    .line 459
    goto :goto_c

    .line 460
    :cond_1a
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityHandWriteSignBinding;->〇OOo8〇0:Lcom/intsig/camscanner/newsign/handwrite/DrawableView;

    .line 461
    .line 462
    const-string v0, "vb.drawView"

    .line 463
    .line 464
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 465
    .line 466
    .line 467
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;->O880O〇(Lcom/intsig/camscanner/newsign/handwrite/DrawableView;)V

    .line 468
    .line 469
    .line 470
    :cond_1b
    :goto_c
    return-void
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d007f

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

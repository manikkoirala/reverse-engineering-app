.class public final Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "SignAdjustFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$EditSignEvent;,
        Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final OO〇00〇8oO:Ljava/text/DecimalFormat;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic oOo0:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final oOo〇8o008:Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:F

.field private OO:Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:I

.field private 〇080OO8〇0:Lcom/intsig/camscanner/util/ParcelSize;

.field private 〇08O〇00〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->oOo0:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->oOo〇8o008:Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$Companion;

    .line 31
    .line 32
    new-instance v0, Ljava/text/DecimalFormat;

    .line 33
    .line 34
    const-string v1, "#.#"

    .line 35
    .line 36
    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->OO〇00〇8oO:Ljava/text/DecimalFormat;

    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    const-string v0, ""

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 21
    .line 22
    const/high16 v1, -0x1000000

    .line 23
    .line 24
    iput v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 25
    .line 26
    const/high16 v1, 0x3f800000    # 1.0f

    .line 27
    .line 28
    iput v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0O:Ljava/lang/String;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O0〇0(F)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->OO〇00〇8oO:Ljava/text/DecimalFormat;

    .line 2
    .line 3
    float-to-double v1, p1

    .line 4
    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    iget v2, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 9
    .line 10
    float-to-double v2, v2

    .line 11
    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    const-string v1, "it\'s same stroke size = "

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    const-string v0, "SignAdjustFragment"

    .line 39
    .line 40
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_0
    iput p1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 47
    .line 48
    iget v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 49
    .line 50
    invoke-direct {p0, v0, v1, p1}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->oOoO8OO〇(Ljava/lang/String;IF)V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇OOo8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;F)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O0〇0(F)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oOoO8OO〇(Ljava/lang/String;IF)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_4

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-gtz v0, :cond_0

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇080OO8〇0:Lcom/intsig/camscanner/util/ParcelSize;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 29
    .line 30
    invoke-static {v0, p1, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    const/4 v1, 0x0

    .line 39
    if-eqz v0, :cond_1

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 42
    .line 43
    invoke-static {v0, p1, v1, v1}, Lcom/intsig/nativelib/DraftEngine;->CleanImageKeepColor(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    goto :goto_0

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 49
    .line 50
    invoke-static {v0, p1, v1, v1}, Lcom/intsig/nativelib/DraftEngine;->CleanImage(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    :goto_0
    const/4 v1, -0x1

    .line 55
    if-le v0, v1, :cond_2

    .line 56
    .line 57
    iget v2, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 58
    .line 59
    if-eqz v2, :cond_2

    .line 60
    .line 61
    invoke-static {v0, p1, p2}, Lcom/intsig/nativelib/DraftEngine;->StrokeColor(ILandroid/graphics/Bitmap;I)V

    .line 62
    .line 63
    .line 64
    invoke-static {v0, p1, p3}, Lcom/intsig/nativelib/DraftEngine;->StrokeSize(ILandroid/graphics/Bitmap;F)V

    .line 65
    .line 66
    .line 67
    :cond_2
    if-le v0, v1, :cond_3

    .line 68
    .line 69
    invoke-static {v0}, Lcom/intsig/nativelib/DraftEngine;->FreeContext(I)V

    .line 70
    .line 71
    .line 72
    :cond_3
    const-string p2, "bitmap"

    .line 73
    .line 74
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0ooOOo(Landroid/graphics/Bitmap;)V

    .line 78
    .line 79
    .line 80
    return-void

    .line 81
    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 82
    .line 83
    .line 84
    move-result p2

    .line 85
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 86
    .line 87
    .line 88
    move-result p1

    .line 89
    new-instance p3, Ljava/lang/StringBuilder;

    .line 90
    .line 91
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .line 93
    .line 94
    const-string v0, "bmWidth="

    .line 95
    .line 96
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    const-string p2, " bmHeight="

    .line 103
    .line 104
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    const-string p2, "SignAdjustFragment"

    .line 115
    .line 116
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;II)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇8〇80o(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;Landroidx/appcompat/app/AppCompatActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0〇0(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;Landroidx/appcompat/app/AppCompatActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final o〇0〇o()Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->oOo0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final o〇O8OO(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 7
    .line 8
    .line 9
    const-string p1, "SignAdjustFragment"

    .line 10
    .line 11
    const-string p2, "click cancel"

    .line 12
    .line 13
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const/4 p1, 0x1

    .line 17
    new-array p1, p1, [Landroid/util/Pair;

    .line 18
    .line 19
    new-instance p2, Landroid/util/Pair;

    .line 20
    .line 21
    const-string v0, "from_part"

    .line 22
    .line 23
    iget-object p0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0O:Ljava/lang/String;

    .line 24
    .line 25
    invoke-direct {p2, v0, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 26
    .line 27
    .line 28
    const/4 p0, 0x0

    .line 29
    aput-object p2, p1, p0

    .line 30
    .line 31
    const-string p0, "CSSignatureEdit"

    .line 32
    .line 33
    const-string p2, "unsave_pop_cancel"

    .line 34
    .line 35
    invoke-static {p0, p2, p1}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇088O()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇0〇o()Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/IncludeEsignEditSignPanelBinding;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeEsignEditSignPanelBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 12
    .line 13
    const-string v2, "tvSave"

    .line 14
    .line 15
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const/16 v2, 0x8

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/IncludeEsignEditSignPanelBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 24
    .line 25
    const-string v1, "tvApplyToAll"

    .line 26
    .line 27
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    :cond_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0ooOOo(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 1
    const-string v0, "SignAdjustFragment"

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇0〇o()Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    :try_start_0
    invoke-virtual {v1, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 14
    .line 15
    .line 16
    const-string p1, "loadBitmapIntoTarget"

    .line 17
    .line 18
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :catch_0
    move-exception p1

    .line 23
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    :goto_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final 〇0〇0(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;Landroidx/appcompat/app/AppCompatActivity;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$it"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "SignAdjustFragment"

    .line 12
    .line 13
    const-string v0, "click ok"

    .line 14
    .line 15
    invoke-static {p3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    .line 19
    .line 20
    .line 21
    const/4 p2, 0x1

    .line 22
    new-array p2, p2, [Landroid/util/Pair;

    .line 23
    .line 24
    new-instance p3, Landroid/util/Pair;

    .line 25
    .line 26
    const-string v0, "from_part"

    .line 27
    .line 28
    iget-object p0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0O:Ljava/lang/String;

    .line 29
    .line 30
    invoke-direct {p3, v0, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    const/4 p0, 0x0

    .line 34
    aput-object p3, p2, p0

    .line 35
    .line 36
    const-string p0, "CSSignatureEdit"

    .line 37
    .line 38
    const-string p3, "unsave_pop_unsave"

    .line 39
    .line 40
    invoke-static {p0, p3, p2}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final 〇8〇80o(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;II)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    iput-boolean p1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇OOo8〇0:Z

    .line 8
    .line 9
    new-instance p1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v0, "click color picker color = "

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const-string v0, "SignAdjustFragment"

    .line 27
    .line 28
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇〇o0〇8(I)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8〇OOoooo()V
    .locals 9

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v1, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v2, Landroid/util/Pair;

    .line 5
    .line 6
    const-string v3, "from_part"

    .line 7
    .line 8
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0O:Ljava/lang/String;

    .line 9
    .line 10
    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    aput-object v2, v1, v3

    .line 15
    .line 16
    const-string v2, "CSSignatureEdit"

    .line 17
    .line 18
    const-string v4, "save"

    .line 19
    .line 20
    invoke-static {v2, v4, v1}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->OO:Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;

    .line 24
    .line 25
    const-string v2, "SignAdjustFragment"

    .line 26
    .line 27
    if-eqz v1, :cond_7

    .line 28
    .line 29
    const-string v4, "do save"

    .line 30
    .line 31
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->getType()I

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    if-nez v4, :cond_0

    .line 39
    .line 40
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇8o8o〇()Ljava/util/ArrayList;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->getType()I

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    invoke-static {v4}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O8o08O(I)Ljava/util/ArrayList;

    .line 50
    .line 51
    .line 52
    move-result-object v4

    .line 53
    :goto_0
    const-string v5, "when (it.type) {\n       \u2026hs(it.type)\n            }"

    .line 54
    .line 55
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 59
    .line 60
    .line 61
    move-result-object v5

    .line 62
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 63
    .line 64
    .line 65
    move-result v6

    .line 66
    if-eqz v6, :cond_4

    .line 67
    .line 68
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v6

    .line 72
    check-cast v6, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 73
    .line 74
    invoke-virtual {v6}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v7

    .line 78
    if-eqz v7, :cond_3

    .line 79
    .line 80
    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    .line 81
    .line 82
    .line 83
    move-result v7

    .line 84
    if-nez v7, :cond_2

    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_2
    const/4 v7, 0x0

    .line 88
    goto :goto_2

    .line 89
    :cond_3
    :goto_1
    const/4 v7, 0x1

    .line 90
    :goto_2
    if-nez v7, :cond_1

    .line 91
    .line 92
    invoke-virtual {v6}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v7

    .line 96
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->〇o〇()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v8

    .line 100
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 101
    .line 102
    .line 103
    move-result v7

    .line 104
    if-eqz v7, :cond_1

    .line 105
    .line 106
    iget v3, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 107
    .line 108
    invoke-virtual {v6, v3}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setColor(I)V

    .line 109
    .line 110
    .line 111
    iget v3, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 112
    .line 113
    float-to-int v3, v3

    .line 114
    invoke-virtual {v6, v3}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setStrokeSize(I)V

    .line 115
    .line 116
    .line 117
    goto :goto_3

    .line 118
    :cond_4
    const/4 v0, 0x0

    .line 119
    :goto_3
    if-eqz v0, :cond_6

    .line 120
    .line 121
    const-string v0, "save data to sp file"

    .line 122
    .line 123
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->getType()I

    .line 127
    .line 128
    .line 129
    move-result v0

    .line 130
    if-nez v0, :cond_5

    .line 131
    .line 132
    invoke-static {v4}, Lcom/intsig/camscanner/signature/SignatureUtil;->OoO8(Ljava/util/List;)V

    .line 133
    .line 134
    .line 135
    goto :goto_4

    .line 136
    :cond_5
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->getType()I

    .line 137
    .line 138
    .line 139
    move-result v0

    .line 140
    invoke-static {v0, v4}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇0〇O0088o(ILjava/util/List;)V

    .line 141
    .line 142
    .line 143
    :goto_4
    new-instance v0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$EditSignEvent;

    .line 144
    .line 145
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->getType()I

    .line 146
    .line 147
    .line 148
    move-result v1

    .line 149
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$EditSignEvent;-><init>(I)V

    .line 150
    .line 151
    .line 152
    invoke-static {v0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 153
    .line 154
    .line 155
    goto :goto_5

    .line 156
    :cond_6
    const-string v0, "not compat, error occur"

    .line 157
    .line 158
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    :goto_5
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 162
    .line 163
    goto :goto_6

    .line 164
    :cond_7
    const/4 v0, 0x0

    .line 165
    :goto_6
    if-nez v0, :cond_8

    .line 166
    .line 167
    const-string v0, "mSignaturePath null"

    .line 168
    .line 169
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .line 171
    .line 172
    :cond_8
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇O8OO(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇o〇88〇8()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇0〇o()Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/IncludeEsignEditSignPanelBinding;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/IncludeEsignEditSignPanelBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setSignatureColors(Z)V

    .line 20
    .line 21
    .line 22
    iget v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 23
    .line 24
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setCurrentSelect(Ljava/lang/Integer;)V

    .line 29
    .line 30
    .line 31
    new-instance v1, L〇0〇0/O8;

    .line 32
    .line 33
    invoke-direct {v1, p0}, L〇0〇0/O8;-><init>(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setOnColorSelectedListener(Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;)V

    .line 37
    .line 38
    .line 39
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇0〇o()Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    .line 45
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentSignAdjustBinding;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/IncludeEsignEditSignPanelBinding;

    .line 46
    .line 47
    if-eqz v0, :cond_1

    .line 48
    .line 49
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/IncludeEsignEditSignPanelBinding;->o〇00O:Landroid/widget/SeekBar;

    .line 50
    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    iget v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 54
    .line 55
    float-to-int v1, v1

    .line 56
    mul-int/lit8 v1, v1, 0x2

    .line 57
    .line 58
    add-int/lit8 v1, v1, 0x4

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 61
    .line 62
    .line 63
    new-instance v1, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$initSignatureAdjustView$2$1;

    .line 64
    .line 65
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment$initSignatureAdjustView$2$1;-><init>(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 69
    .line 70
    .line 71
    :cond_1
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇〇o0〇8(I)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    const-string p1, "SignAdjustFragment"

    .line 6
    .line 7
    const-string v0, "it\'s same color"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iput p1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 16
    .line 17
    iget v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 18
    .line 19
    invoke-direct {p0, v0, p1, v1}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->oOoO8OO〇(Ljava/lang/String;IF)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final Ooo8o()V
    .locals 2

    .line 1
    const-string v0, "SignAdjustFragment"

    .line 2
    .line 3
    const-string v1, "click save"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇8〇OOoooo()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v1, -0x1

    .line 16
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public getIntentData(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->getIntentData(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    const-string v1, "key_signature_path"

    .line 8
    .line 9
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object v1, v0

    .line 17
    :goto_0
    iput-object v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->OO:Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->〇o〇()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    move-object v1, v0

    .line 27
    :goto_1
    const-string v2, ""

    .line 28
    .line 29
    if-nez v1, :cond_2

    .line 30
    .line 31
    move-object v1, v2

    .line 32
    :cond_2
    iput-object v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->OO:Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;

    .line 35
    .line 36
    if-eqz v1, :cond_3

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->〇080()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    goto :goto_2

    .line 43
    :cond_3
    const/high16 v1, -0x1000000

    .line 44
    .line 45
    :goto_2
    iput v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 46
    .line 47
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->OO:Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;

    .line 48
    .line 49
    if-eqz v1, :cond_4

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/signadjust/TempSignaturePath;->Oo08()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    int-to-float v1, v1

    .line 56
    goto :goto_3

    .line 57
    :cond_4
    const/high16 v1, 0x3f800000    # 1.0f

    .line 58
    .line 59
    :goto_3
    iput v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 60
    .line 61
    if-eqz p1, :cond_5

    .line 62
    .line 63
    const-string v0, "key_from_part"

    .line 64
    .line 65
    invoke-virtual {p1, v0, v2}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    :cond_5
    if-nez v0, :cond_6

    .line 70
    .line 71
    goto :goto_4

    .line 72
    :cond_6
    move-object v2, v0

    .line 73
    :goto_4
    iput-object v2, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0O:Ljava/lang/String;

    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 2
    .line 3
    iget v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 6
    .line 7
    new-instance v2, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v3, "initialize mImagePath="

    .line 13
    .line 14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string p1, "\nmColor="

    .line 21
    .line 22
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string p1, "\nmStrokeSize="

    .line 29
    .line 30
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    const-string v0, "SignAdjustFragment"

    .line 41
    .line 42
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇088O()V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 49
    .line 50
    iget v0, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->o〇00O:I

    .line 51
    .line 52
    iget v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->O8o08O8O:F

    .line 53
    .line 54
    invoke-direct {p0, p1, v0, v1}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->oOoO8OO〇(Ljava/lang/String;IF)V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇o〇88〇8()V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public interceptBackPressed()Z
    .locals 8

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v1, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v2, Landroid/util/Pair;

    .line 5
    .line 6
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0O:Ljava/lang/String;

    .line 7
    .line 8
    const-string v4, "from_part"

    .line 9
    .line 10
    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    const/4 v3, 0x0

    .line 14
    aput-object v2, v1, v3

    .line 15
    .line 16
    const-string v2, "CSSignatureEdit"

    .line 17
    .line 18
    const-string v5, "back"

    .line 19
    .line 20
    invoke-static {v2, v5, v1}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 21
    .line 22
    .line 23
    iget-boolean v1, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇OOo8〇0:Z

    .line 24
    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    new-array v5, v0, [Landroid/util/Pair;

    .line 32
    .line 33
    new-instance v6, Landroid/util/Pair;

    .line 34
    .line 35
    iget-object v7, p0, Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;->〇0O:Ljava/lang/String;

    .line 36
    .line 37
    invoke-direct {v6, v4, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    aput-object v6, v5, v3

    .line 41
    .line 42
    const-string v3, "unsave_pop_show"

    .line 43
    .line 44
    invoke-static {v2, v3, v5}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 45
    .line 46
    .line 47
    new-instance v2, Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    invoke-direct {v2, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 50
    .line 51
    .line 52
    const v3, 0x7f1309ad

    .line 53
    .line 54
    .line 55
    invoke-virtual {v2, v3}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    const v3, 0x7f13063e

    .line 60
    .line 61
    .line 62
    invoke-virtual {v2, v3}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    new-instance v3, L〇0〇0/〇o00〇〇Oo;

    .line 67
    .line 68
    invoke-direct {v3, p0}, L〇0〇0/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;)V

    .line 69
    .line 70
    .line 71
    const v4, 0x7f13057e

    .line 72
    .line 73
    .line 74
    invoke-virtual {v2, v4, v3}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    new-instance v3, L〇0〇0/〇o〇;

    .line 79
    .line 80
    invoke-direct {v3, p0, v1}, L〇0〇0/〇o〇;-><init>(Lcom/intsig/camscanner/newsign/signadjust/SignAdjustFragment;Landroidx/appcompat/app/AppCompatActivity;)V

    .line 81
    .line 82
    .line 83
    const v1, 0x7f130617

    .line 84
    .line 85
    .line 86
    invoke-virtual {v2, v1, v3}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    invoke-virtual {v1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 99
    .line 100
    if-eqz v1, :cond_1

    .line 101
    .line 102
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 103
    .line 104
    .line 105
    :cond_1
    :goto_0
    return v0
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0343

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

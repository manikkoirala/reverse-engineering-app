.class final Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;
.super Ljava/lang/Object;
.source "GlideSignatureBitmapTransformation.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BitmapTransformCallable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o0:Landroid/graphics/Bitmap;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "toTransform"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "signaturePath"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->o0:Landroid/graphics/Bitmap;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public call()Landroid/graphics/Bitmap;
    .locals 7
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    invoke-virtual {v2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    move-result-object v2

    .line 4
    invoke-virtual {v2}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    move-result v3

    const-string v4, "GlideSignatureBitmapTransformation"

    if-lez v3, :cond_4

    invoke-virtual {v2}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    move-result v3

    if-gtz v3, :cond_0

    goto :goto_1

    .line 5
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    move-result v2

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 6
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    move-result v3

    const/4 v5, 0x0

    if-eqz v3, :cond_1

    .line 7
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    invoke-virtual {v3}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v5, v5}, Lcom/intsig/nativelib/DraftEngine;->CleanImageKeepColor(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    move-result v3

    goto :goto_0

    .line 8
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    invoke-virtual {v3}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v5, v5}, Lcom/intsig/nativelib/DraftEngine;->CleanImage(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    move-result v3

    :goto_0
    const/4 v5, -0x1

    if-le v3, v5, :cond_2

    .line 9
    iget-object v6, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    invoke-virtual {v6}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    move-result v6

    if-eqz v6, :cond_2

    .line 10
    iget-object v6, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    invoke-virtual {v6}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    move-result v6

    invoke-static {v3, v2, v6}, Lcom/intsig/nativelib/DraftEngine;->StrokeColor(ILandroid/graphics/Bitmap;I)V

    .line 11
    iget-object v6, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    invoke-virtual {v6}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    move-result v6

    int-to-float v6, v6

    invoke-static {v3, v2, v6}, Lcom/intsig/nativelib/DraftEngine;->StrokeSize(ILandroid/graphics/Bitmap;F)V

    :cond_2
    if-le v3, v5, :cond_3

    .line 12
    invoke-static {v3}, Lcom/intsig/nativelib/DraftEngine;->FreeContext(I)V

    .line 13
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "transform costTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "newBitmap"

    .line 14
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v2

    .line 15
    :cond_4
    :goto_1
    invoke-virtual {v2}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bmWidth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " bmHeight="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->o0:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/signadjust/GlideSignatureBitmapTransformation$BitmapTransformCallable;->call()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

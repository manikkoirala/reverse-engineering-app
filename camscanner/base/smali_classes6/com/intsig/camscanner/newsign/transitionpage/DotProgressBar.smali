.class public final Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;
.super Landroid/view/View;
.source "DotProgressBar.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$ProgressListener;,
        Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8〇OO0〇0o:Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇oO〇〇8o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:J

.field private OO:I

.field private OO〇00〇8oO:Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$ProgressListener;

.field private o0:I

.field private oOo0:J

.field private oOo〇8o008:I

.field private o〇00O:I

.field private final 〇080OO8〇0:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:I

.field private 〇0O:Lkotlinx/coroutines/Job;

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o8〇OO0〇0o:Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "DotProgressBar::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x5

    .line 10
    iput p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o0:I

    .line 11
    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const p2, 0x7f060205

    .line 17
    .line 18
    .line 19
    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    iput p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇OOo8〇0:I

    .line 24
    .line 25
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    const p2, 0x7f060207

    .line 30
    .line 31
    .line 32
    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    iput p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->OO:I

    .line 37
    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    const/4 p2, 0x3

    .line 43
    invoke-static {p1, p2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    iput p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇08O〇00〇o:I

    .line 48
    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    const/4 p2, 0x4

    .line 54
    invoke-static {p1, p2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    iput p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o〇00O:I

    .line 59
    .line 60
    const-wide/16 p1, 0x12c

    .line 61
    .line 62
    iput-wide p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->O8o08O8O:J

    .line 63
    .line 64
    new-instance p1, Landroid/graphics/Paint;

    .line 65
    .line 66
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 67
    .line 68
    .line 69
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;)Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$ProgressListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->OO〇00〇8oO:Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$ProgressListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic Oo08()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->oOo〇8o008:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->oOo〇8o008:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->O8o08O8O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final oO80()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇0O:Lkotlinx/coroutines/Job;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    const/4 v2, 0x1

    .line 7
    invoke-static {v0, v1, v2, v1}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    iput-object v1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->OO〇00〇8oO:Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$ProgressListener;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    int-to-float v0, v0

    .line 9
    const/high16 v1, 0x40000000    # 2.0f

    .line 10
    .line 11
    div-float/2addr v0, v1

    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    int-to-float v2, v2

    .line 17
    div-float/2addr v2, v1

    .line 18
    iget v3, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o0:I

    .line 19
    .line 20
    add-int/lit8 v4, v3, -0x1

    .line 21
    .line 22
    iget v5, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o〇00O:I

    .line 23
    .line 24
    mul-int v4, v4, v5

    .line 25
    .line 26
    iget v5, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇08O〇00〇o:I

    .line 27
    .line 28
    mul-int/lit8 v6, v5, 0x2

    .line 29
    .line 30
    mul-int v6, v6, v3

    .line 31
    .line 32
    add-int/2addr v4, v6

    .line 33
    int-to-float v4, v4

    .line 34
    div-float/2addr v4, v1

    .line 35
    sub-float/2addr v0, v4

    .line 36
    int-to-float v1, v5

    .line 37
    add-float/2addr v0, v1

    .line 38
    const/4 v1, 0x0

    .line 39
    :goto_0
    if-ge v1, v3, :cond_2

    .line 40
    .line 41
    iget v4, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->oOo〇8o008:I

    .line 42
    .line 43
    if-ne v1, v4, :cond_0

    .line 44
    .line 45
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 46
    .line 47
    iget v5, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->OO:I

    .line 48
    .line 49
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 50
    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_0
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 54
    .line 55
    iget v5, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇OOo8〇0:I

    .line 56
    .line 57
    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    .line 59
    .line 60
    :goto_1
    if-eqz p1, :cond_1

    .line 61
    .line 62
    iget v4, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇08O〇00〇o:I

    .line 63
    .line 64
    int-to-float v4, v4

    .line 65
    iget-object v5, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 66
    .line 67
    invoke-virtual {p1, v0, v2, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 68
    .line 69
    .line 70
    :cond_1
    iget v4, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇08O〇00〇o:I

    .line 71
    .line 72
    mul-int/lit8 v4, v4, 0x2

    .line 73
    .line 74
    iget v5, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o〇00O:I

    .line 75
    .line 76
    add-int/2addr v4, v5

    .line 77
    int-to-float v4, v4

    .line 78
    add-float/2addr v0, v4

    .line 79
    add-int/lit8 v1, v1, 0x1

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_2
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method protected onMeasure(II)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 2
    .line 3
    .line 4
    iget p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o0:I

    .line 5
    .line 6
    add-int/lit8 p2, p1, -0x1

    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o〇00O:I

    .line 9
    .line 10
    mul-int p2, p2, v0

    .line 11
    .line 12
    iget v0, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇08O〇00〇o:I

    .line 13
    .line 14
    mul-int/lit8 v1, v0, 0x2

    .line 15
    .line 16
    mul-int v1, v1, p1

    .line 17
    .line 18
    add-int/2addr p2, v1

    .line 19
    mul-int/lit8 v0, v0, 0x2

    .line 20
    .line 21
    invoke-virtual {p0, p2, v0}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final setDuration(J)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "setDuration == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iput-wide p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->oOo0:J

    .line 24
    .line 25
    const-wide/16 v0, 0x0

    .line 26
    .line 27
    cmp-long v2, p1, v0

    .line 28
    .line 29
    if-lez v2, :cond_0

    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->o0:I

    .line 32
    .line 33
    add-int/lit8 v0, v0, -0x1

    .line 34
    .line 35
    int-to-long v0, v0

    .line 36
    div-long/2addr p1, v0

    .line 37
    iput-wide p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->O8o08O8O:J

    .line 38
    .line 39
    :cond_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final setProgressListener(Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$ProgressListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$ProgressListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "listener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->OO〇00〇8oO:Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$ProgressListener;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇888(Lkotlinx/coroutines/CoroutineScope;)V
    .locals 7
    .param p1    # Lkotlinx/coroutines/CoroutineScope;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "scope"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x0

    .line 8
    new-instance v4, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$startAnimation$1;

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar$startAnimation$1;-><init>(Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;Lkotlin/coroutines/Continuation;)V

    .line 12
    .line 13
    .line 14
    const/4 v5, 0x3

    .line 15
    const/4 v6, 0x0

    .line 16
    move-object v1, p1

    .line 17
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/transitionpage/DotProgressBar;->〇0O:Lkotlinx/coroutines/Job;

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

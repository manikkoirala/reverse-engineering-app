.class public final Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;
.super Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$FileSaver;
.source "ESignSaveFileDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PdfSaver"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mData"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$FileSaver;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final getContext()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;

    .line 4
    .line 5
    if-eqz v1, :cond_2

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;->〇080()Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 16
    .line 17
    .line 18
    move-result-wide v1

    .line 19
    sget-object v0, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;

    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;

    .line 22
    .line 23
    check-cast v3, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;

    .line 24
    .line 25
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;->〇o00〇〇Oo()Lcom/intsig/camscanner/newsign/data/ESignInfo;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    if-eqz v3, :cond_0

    .line 30
    .line 31
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getUser_role()Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    const/4 v3, 0x0

    .line 37
    :goto_0
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;->〇0000OOO(Ljava/lang/Integer;)V

    .line 38
    .line 39
    .line 40
    const/4 v3, 0x0

    .line 41
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 42
    .line 43
    const/4 v5, 0x0

    .line 44
    const/4 v6, 0x5

    .line 45
    const/4 v7, 0x0

    .line 46
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_1
    return-void

    .line 51
    :cond_2
    instance-of v0, v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$DownloadData;

    .line 52
    .line 53
    if-eqz v0, :cond_3

    .line 54
    .line 55
    sget-object v0, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;

    .line 56
    .line 57
    const/4 v1, 0x2

    .line 58
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;->o〇〇0〇(Ljava/lang/Integer;)V

    .line 63
    .line 64
    .line 65
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 66
    .line 67
    const/4 v1, 0x1

    .line 68
    new-array v1, v1, [Ljava/lang/Object;

    .line 69
    .line 70
    const v2, 0x7f130b28

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    new-instance v3, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    const-string v2, "/CamScanner"

    .line 86
    .line 87
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    const/4 v3, 0x0

    .line 95
    aput-object v2, v1, v3

    .line 96
    .line 97
    const v2, 0x7f130621

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->〇80〇808〇O(Landroid/content/Context;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

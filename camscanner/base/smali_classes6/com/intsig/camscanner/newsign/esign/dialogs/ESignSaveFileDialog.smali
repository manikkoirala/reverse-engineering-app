.class public final Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;
.super Ljava/lang/Object;
.source "ESignSaveFileDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;,
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$FileSaver;,
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;,
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$Companion;,
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$DownloadData;,
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$ImageSaver;,
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$PdfSaver;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo08:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->O8:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "ESignSaveFileDialog::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->Oo08:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mContext"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mData"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;

    .line 17
    .line 18
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 19
    .line 20
    new-instance p2, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$loadingDialog$2;

    .line 21
    .line 22
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$loadingDialog$2;-><init>(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;)V

    .line 23
    .line 24
    .line 25
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇o〇:Lkotlin/Lazy;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;)Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final OO0o〇〇〇〇0(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    sget-object p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "it dismiss"

    .line 4
    .line 5
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$FileSaver;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->oO80(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$FileSaver;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oO80(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$FileSaver;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x0

    .line 9
    new-instance v4, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$save$1;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    invoke-direct {v4, p0, p1, v0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$save$1;-><init>(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$FileSaver;Lkotlin/coroutines/Continuation;)V

    .line 13
    .line 14
    .line 15
    const/4 v5, 0x3

    .line 16
    const/4 v6, 0x0

    .line 17
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method private final o〇0()Lcom/intsig/app/BaseProgressDialog;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇o〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-loadingDialog>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Lcom/intsig/app/BaseProgressDialog;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇8o8o〇(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇8o8o〇(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->Oo08:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "it show"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;

    .line 14
    .line 15
    instance-of p1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;

    .line 16
    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    check-cast p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$LocalDbData;->〇o00〇〇Oo()Lcom/intsig/camscanner/newsign/data/ESignInfo;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    if-eqz p0, :cond_0

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getUser_role()Ljava/lang/Integer;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 p0, 0x0

    .line 33
    goto :goto_0

    .line 34
    :cond_1
    const/4 p0, 0x2

    .line 35
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;

    .line 40
    .line 41
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSAddSignature;->〇oOO8O8(Ljava/lang/Integer;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇o00〇〇Oo(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->OO0o〇〇〇〇0(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->o〇0()Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final 〇80〇808〇O()V
    .locals 6

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$IData;

    .line 7
    .line 8
    instance-of v1, v1, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$DownloadData;

    .line 9
    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;

    .line 13
    .line 14
    new-instance v2, Lcom/intsig/menu/MenuItem;

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    const v4, 0x7f130c65

    .line 19
    .line 20
    .line 21
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    const v4, 0x7f080ab3

    .line 26
    .line 27
    .line 28
    const/4 v5, 0x1

    .line 29
    invoke-direct {v2, v5, v3, v4}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 30
    .line 31
    .line 32
    new-instance v3, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$show$1;

    .line 33
    .line 34
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$show$1;-><init>(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;-><init>(Lcom/intsig/menu/MenuItem;Lkotlin/jvm/functions/Function0;)V

    .line 38
    .line 39
    .line 40
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;

    .line 44
    .line 45
    new-instance v2, Lcom/intsig/menu/MenuItem;

    .line 46
    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 48
    .line 49
    const v4, 0x7f1316b8

    .line 50
    .line 51
    .line 52
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    const v4, 0x7f080902

    .line 57
    .line 58
    .line 59
    const/4 v5, 0x2

    .line 60
    invoke-direct {v2, v5, v3, v4}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 61
    .line 62
    .line 63
    new-instance v3, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$show$2;

    .line 64
    .line 65
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog$show$2;-><init>(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;)V

    .line 66
    .line 67
    .line 68
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;-><init>(Lcom/intsig/menu/MenuItem;Lkotlin/jvm/functions/Function0;)V

    .line 69
    .line 70
    .line 71
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;

    .line 75
    .line 76
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 77
    .line 78
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 79
    .line 80
    .line 81
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 82
    .line 83
    const v3, 0x7f1312ff

    .line 84
    .line 85
    .line 86
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o〇(Ljava/util/List;)Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇080()Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    new-instance v1, Lcom/intsig/camscanner/newsign/esign/dialogs/〇o〇;

    .line 103
    .line 104
    invoke-direct {v1}, Lcom/intsig/camscanner/newsign/esign/dialogs/〇o〇;-><init>()V

    .line 105
    .line 106
    .line 107
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 108
    .line 109
    .line 110
    new-instance v1, Lcom/intsig/camscanner/newsign/esign/dialogs/O8;

    .line 111
    .line 112
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/O8;-><init>(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;)V

    .line 113
    .line 114
    .line 115
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 119
    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇〇888()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignSaveFileDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

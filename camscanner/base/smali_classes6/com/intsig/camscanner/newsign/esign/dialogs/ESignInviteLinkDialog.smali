.class public final Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;
.super Ljava/lang/Object;
.source "ESignInviteLinkDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;,
        Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo08:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->O8:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "ESignInviteLinkDialog::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->Oo08:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mContext"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mWechatShareParam"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;

    .line 17
    .line 18
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 19
    .line 20
    new-instance p2, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$loadingDialog$2;

    .line 21
    .line 22
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$loadingDialog$2;-><init>(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;)V

    .line 23
    .line 24
    .line 25
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇o〇:Lkotlin/Lazy;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method private final O8(I)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "clickMenuItem menuId == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;->〇080()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    new-instance v2, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v3, "link == "

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    const/4 v0, 0x1

    .line 50
    if-eqz v1, :cond_1

    .line 51
    .line 52
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-nez v2, :cond_0

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    const/4 v2, 0x0

    .line 60
    goto :goto_1

    .line 61
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 62
    :goto_1
    if-eqz v2, :cond_2

    .line 63
    .line 64
    return-void

    .line 65
    :cond_2
    if-eq p1, v0, :cond_4

    .line 66
    .line 67
    const/4 v0, 0x2

    .line 68
    if-eq p1, v0, :cond_3

    .line 69
    .line 70
    goto :goto_2

    .line 71
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;

    .line 72
    .line 73
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;->〇o〇()V

    .line 74
    .line 75
    .line 76
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 77
    .line 78
    const-string v0, ""

    .line 79
    .line 80
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/app/AppUtil;->〇O00(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)Z

    .line 81
    .line 82
    .line 83
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 84
    .line 85
    const v0, 0x7f13025a

    .line 86
    .line 87
    .line 88
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 89
    .line 90
    .line 91
    goto :goto_2

    .line 92
    :cond_4
    sget-object p1, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;

    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;->O8()V

    .line 95
    .line 96
    .line 97
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->o〇0()V

    .line 98
    .line 99
    .line 100
    :goto_2
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final oO80(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    sget-object p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "it dismiss"

    .line 4
    .line 5
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇0()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "shareWechat"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/newsign/esign/dialogs/ShareESignToWxMin;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇o00〇〇Oo:Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;

    .line 13
    .line 14
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/newsign/esign/dialogs/ShareESignToWxMin;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$WeChatShareParam;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ShareESignToWxMin;->OO0o〇〇〇〇0()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public static synthetic 〇080(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇80〇808〇O(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇80〇808〇O(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    sget-object p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "it show"

    .line 4
    .line 5
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object p0, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CSSignatureDetail;->〇〇888()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o00〇〇Oo(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->oO80(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->O8(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final Oo08()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇888()V
    .locals 6

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;

    .line 7
    .line 8
    new-instance v2, Lcom/intsig/menu/MenuItem;

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    const v4, 0x7f13143d

    .line 13
    .line 14
    .line 15
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    const v4, 0x7f080952

    .line 20
    .line 21
    .line 22
    const/4 v5, 0x1

    .line 23
    invoke-direct {v2, v5, v3, v4}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 24
    .line 25
    .line 26
    new-instance v3, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$show$1;

    .line 27
    .line 28
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$show$1;-><init>(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;-><init>(Lcom/intsig/menu/MenuItem;Lkotlin/jvm/functions/Function0;)V

    .line 32
    .line 33
    .line 34
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    new-instance v1, Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;

    .line 38
    .line 39
    new-instance v2, Lcom/intsig/menu/MenuItem;

    .line 40
    .line 41
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 42
    .line 43
    const v4, 0x7f131c4c

    .line 44
    .line 45
    .line 46
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    const v4, 0x7f080c26

    .line 51
    .line 52
    .line 53
    const/4 v5, 0x2

    .line 54
    invoke-direct {v2, v5, v3, v4}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 55
    .line 56
    .line 57
    new-instance v3, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$show$2;

    .line 58
    .line 59
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog$show$2;-><init>(Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;)V

    .line 60
    .line 61
    .line 62
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/newsign/util/MenuItemAndFun;-><init>(Lcom/intsig/menu/MenuItem;Lkotlin/jvm/functions/Function0;)V

    .line 63
    .line 64
    .line 65
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    new-instance v1, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;

    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 71
    .line 72
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 73
    .line 74
    .line 75
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/esign/dialogs/ESignInviteLinkDialog;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 76
    .line 77
    const v3, 0x7f1318a1

    .line 78
    .line 79
    .line 80
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇o〇(Ljava/util/List;)Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog$Builder;->〇080()Lcom/intsig/camscanner/newsign/util/ESignCommonBtmDialog;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    new-instance v1, Lcom/intsig/camscanner/newsign/esign/dialogs/〇080;

    .line 97
    .line 98
    invoke-direct {v1}, Lcom/intsig/camscanner/newsign/esign/dialogs/〇080;-><init>()V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 102
    .line 103
    .line 104
    new-instance v1, Lcom/intsig/camscanner/newsign/esign/dialogs/〇o00〇〇Oo;

    .line 105
    .line 106
    invoke-direct {v1}, Lcom/intsig/camscanner/newsign/esign/dialogs/〇o00〇〇Oo;-><init>()V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 113
    .line 114
    .line 115
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

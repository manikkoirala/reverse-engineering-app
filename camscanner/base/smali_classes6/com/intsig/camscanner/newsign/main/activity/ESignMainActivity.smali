.class public final Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "ESignMainActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$StartESignActivityEvent;,
        Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O0O:Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic o8oOOo:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final 〇O〇〇O8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇08O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "_mBinding"

    .line 7
    .line 8
    const-string v3, "get_mBinding()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->o8oOOo:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->O0O:Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;

    .line 31
    .line 32
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "ESignMainActivity::class.java.simpleName"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 7
    .line 8
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$special$$inlined$viewModels$default$1;

    .line 14
    .line 15
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$special$$inlined$viewModels$default$1;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Landroidx/lifecycle/ViewModelLazy;

    .line 19
    .line 20
    const-class v2, Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;

    .line 21
    .line 22
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    new-instance v3, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$special$$inlined$viewModels$default$2;

    .line 27
    .line 28
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$special$$inlined$viewModels$default$2;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 29
    .line 30
    .line 31
    new-instance v4, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$special$$inlined$viewModels$default$3;

    .line 32
    .line 33
    const/4 v5, 0x0

    .line 34
    invoke-direct {v4, v5, p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$special$$inlined$viewModels$default$3;-><init>(Lkotlin/jvm/functions/Function0;Landroidx/activity/ComponentActivity;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {v1, v2, v3, v0, v4}, Landroidx/lifecycle/ViewModelLazy;-><init>(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 38
    .line 39
    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇〇08O:Lkotlin/Lazy;

    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O00OoO〇(Landroidx/core/view/WindowInsetsControllerCompat;)V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->o8〇OO0〇0o:Landroidx/viewpager2/widget/ViewPager2;

    .line 6
    .line 7
    const-string v1, "mBinding.viewpager2"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x2

    .line 13
    new-array v1, v1, [Lkotlin/Pair;

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 20
    .line 21
    new-instance v3, Lcom/intsig/camscanner/newsign/main/home/SignHomeFragment;

    .line 22
    .line 23
    invoke-direct {v3}, Lcom/intsig/camscanner/newsign/main/home/SignHomeFragment;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-static {v2, v3}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const/4 v3, 0x0

    .line 31
    aput-object v2, v1, v3

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 38
    .line 39
    sget-object v4, Lcom/intsig/camscanner/newsign/main/me/SignMeFragment;->〇〇08O:Lcom/intsig/camscanner/newsign/main/me/SignMeFragment$Companion;

    .line 40
    .line 41
    const/4 v5, 0x1

    .line 42
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/newsign/main/me/SignMeFragment$Companion;->〇080(I)Lcom/intsig/camscanner/newsign/main/me/SignMeFragment;

    .line 43
    .line 44
    .line 45
    move-result-object v4

    .line 46
    invoke-static {v2, v4}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    aput-object v2, v1, v5

    .line 51
    .line 52
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    move-object v2, v1

    .line 57
    check-cast v2, Ljava/lang/Iterable;

    .line 58
    .line 59
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    const/4 v4, 0x0

    .line 64
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 65
    .line 66
    .line 67
    move-result v5

    .line 68
    if-eqz v5, :cond_1

    .line 69
    .line 70
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    add-int/lit8 v6, v4, 0x1

    .line 75
    .line 76
    if-gez v4, :cond_0

    .line 77
    .line 78
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 79
    .line 80
    .line 81
    :cond_0
    check-cast v5, Lkotlin/Pair;

    .line 82
    .line 83
    invoke-virtual {v5}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v5

    .line 87
    check-cast v5, Lcom/intsig/view/ImageTextButton;

    .line 88
    .line 89
    new-instance v7, L〇088O/〇o〇;

    .line 90
    .line 91
    invoke-direct {v7, v0, v4}, L〇088O/〇o〇;-><init>(Landroidx/viewpager2/widget/ViewPager2;I)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v5, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    .line 96
    .line 97
    move v4, v6

    .line 98
    goto :goto_0

    .line 99
    :cond_1
    invoke-virtual {v0, v3}, Landroidx/viewpager2/widget/ViewPager2;->setUserInputEnabled(Z)V

    .line 100
    .line 101
    .line 102
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 103
    .line 104
    new-instance v3, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$initViewPager$2$1;

    .line 105
    .line 106
    invoke-direct {v3, v1, p0, v2}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$initViewPager$2$1;-><init>(Ljava/util/List;Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v0, v3}, Landroidx/viewpager2/widget/ViewPager2;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 110
    .line 111
    .line 112
    new-instance v2, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$initViewPager$2$2;

    .line 113
    .line 114
    invoke-direct {v2, p1, p0, v1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$initViewPager$2$2;-><init>(Landroidx/core/view/WindowInsetsControllerCompat;Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Ljava/util/List;)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v0, v2}, Landroidx/viewpager2/widget/ViewPager2;->registerOnPageChangeCallback(Landroidx/viewpager2/widget/ViewPager2$OnPageChangeCallback;)V

    .line 118
    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic O0〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->O8O(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O88()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 8
    .line 9
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const-string v2, "mActivity.window.decorView"

    .line 18
    .line 19
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-static {v0, v2}, Lcom/intsig/utils/SystemUiUtil;->〇O8o08O(Landroid/view/Window;Z)V

    .line 24
    .line 25
    .line 26
    new-instance v3, Landroidx/core/view/WindowInsetsControllerCompat;

    .line 27
    .line 28
    invoke-direct {v3, v0, v1}, Landroidx/core/view/WindowInsetsControllerCompat;-><init>(Landroid/view/Window;Landroid/view/View;)V

    .line 29
    .line 30
    .line 31
    invoke-static {}, Landroidx/core/view/WindowInsetsCompat$Type;->statusBars()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    invoke-virtual {v3, v1}, Landroidx/core/view/WindowInsetsControllerCompat;->show(I)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v3, v2}, Landroidx/core/view/WindowInsetsControllerCompat;->setAppearanceLightStatusBars(Z)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 42
    .line 43
    .line 44
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->O00OoO〇(Landroidx/core/view/WindowInsetsControllerCompat;)V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 52
    .line 53
    new-instance v1, L〇088O/〇080;

    .line 54
    .line 55
    invoke-direct {v1, p0}, L〇088O/〇080;-><init>(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 66
    .line 67
    new-instance v1, L〇088O/〇o00〇〇Oo;

    .line 68
    .line 69
    invoke-direct {v1, p0}, L〇088O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O880O〇(ILjava/util/List;)Landroidx/fragment/app/Fragment;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Lcom/intsig/view/ImageTextButton;",
            "Lcom/intsig/mvp/fragment/BaseChangeFragment;",
            ">;>;)",
            "Landroidx/fragment/app/Fragment;"
        }
    .end annotation

    .line 1
    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Lkotlin/Pair;

    .line 6
    .line 7
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Landroidx/fragment/app/Fragment;

    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O8O(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    sget-object p1, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "cancel"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OO0O(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->OoO〇OOo8o()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO〇〇o0oO()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f131d10

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const v1, 0x7f13176b

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, L〇088O/O8;

    .line 21
    .line 22
    invoke-direct {v1}, L〇088O/O8;-><init>()V

    .line 23
    .line 24
    .line 25
    const v2, 0x7f131cfc

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    new-instance v1, L〇088O/Oo08;

    .line 33
    .line 34
    invoke-direct {v1, p0}, L〇088O/Oo08;-><init>(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;)V

    .line 35
    .line 36
    .line 37
    const v2, 0x7f13054e

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final OoO〇OOo8o()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v1, "EXTRA_KEY_OPEN_ESIGN_HOME_FROM"

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final OooO〇(Z)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    const-string v1, "mBinding.groupBtmBar"

    .line 3
    .line 4
    const/16 v2, 0x8

    .line 5
    .line 6
    const-string v3, "mBinding.groupDelete"

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    sget-object p1, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 11
    .line 12
    const-string v4, "change2EditMode"

    .line 13
    .line 14
    invoke-static {p1, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->OO:Landroidx/constraintlayout/widget/Group;

    .line 22
    .line 23
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/Group;

    .line 34
    .line 35
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 43
    .line 44
    const-string v4, "change2NormalMode"

    .line 45
    .line 46
    invoke-static {p1, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->OO:Landroidx/constraintlayout/widget/Group;

    .line 54
    .line 55
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/Group;

    .line 66
    .line 67
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 71
    .line 72
    .line 73
    :goto_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇0o88Oo〇(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇ooO〇000(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇oOO80o(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O〇〇o8O(Landroidx/viewpager2/widget/ViewPager2;ILandroid/view/View;)V
    .locals 0

    .line 1
    const-string p2, "$viewpager2"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, p1}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(I)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o0O0O〇〇〇0()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->o8oOOo:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final o0OO(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->O0O:Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0, p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;->〇080(Ljava/lang/Long;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o0Oo(Landroidx/viewpager2/widget/ViewPager2;ILandroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->O〇〇o8O(Landroidx/viewpager2/widget/ViewPager2;ILandroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic o808o8o08(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;ILjava/util/List;)Landroidx/fragment/app/Fragment;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->O880O〇(ILjava/util/List;)Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic o〇08oO80o(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;)Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇OoO0o0()Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇OoO0()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$subscribeUi$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$subscribeUi$1;-><init>(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o〇o08〇()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final startActivity(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->O0O:Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0, p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$Companion;->startActivity(Landroid/app/Activity;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0o88Oo〇(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->OO〇〇o0oO()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇OoO0o0()Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇〇08O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->OooO〇(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇oOO80o(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent;

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->OoO〇OOo8o()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/newsign/ESignLogAgent;->O8ooOoo〇(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇OoO0o0()Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    sget-object p1, Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel$SendAction$ClickFabAction;->〇080:Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel$SendAction$ClickFabAction;

    .line 20
    .line 21
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;->〇O00(Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel$SendAction;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇ooO8Ooo〇()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->o0O0O〇〇〇0()Lcom/intsig/camscanner/databinding/ActivityNewSignBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇ooO〇000(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p2, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "confirm"

    .line 9
    .line 10
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->〇OoO0o0()Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    sget-object p2, Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel$SendAction$DeleteSignAction;->〇080:Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel$SendAction$DeleteSignAction;

    .line 18
    .line 19
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel;->〇O00(Lcom/intsig/camscanner/newsign/main/activity/ESignMainViewModel$SendAction;)V

    .line 20
    .line 21
    .line 22
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public final OO0o(ILjava/util/List;)V
    .locals 4
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Lcom/intsig/view/ImageTextButton;",
            "Lcom/intsig/mvp/fragment/BaseChangeFragment;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "itbFragmentList"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    check-cast p2, Ljava/lang/Iterable;

    .line 7
    .line 8
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object p2

    .line 12
    const/4 v0, 0x0

    .line 13
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_2

    .line 18
    .line 19
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    add-int/lit8 v2, v0, 0x1

    .line 24
    .line 25
    if-gez v0, :cond_0

    .line 26
    .line 27
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 28
    .line 29
    .line 30
    :cond_0
    check-cast v1, Lkotlin/Pair;

    .line 31
    .line 32
    invoke-virtual {v1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    check-cast v1, Lcom/intsig/view/ImageTextButton;

    .line 37
    .line 38
    if-ne v0, p1, :cond_1

    .line 39
    .line 40
    const-string v0, "#107672"

    .line 41
    .line 42
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    invoke-virtual {v1, v3}, Lcom/intsig/view/ImageTextButton;->setTextColor(I)V

    .line 47
    .line 48
    .line 49
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-virtual {v1, v0}, Lcom/intsig/view/ImageTextButton;->setImagIconColor(I)V

    .line 54
    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 58
    .line 59
    const v3, 0x7f060206

    .line 60
    .line 61
    .line 62
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    invoke-virtual {v1, v0}, Lcom/intsig/view/ImageTextButton;->setTextColor(I)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 70
    .line 71
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    invoke-virtual {v1, v0}, Lcom/intsig/view/ImageTextButton;->setImagIconColor(I)V

    .line 76
    .line 77
    .line 78
    :goto_1
    move v0, v2

    .line 79
    goto :goto_0

    .line 80
    :cond_2
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->O88()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;->o〇OoO0()V

    .line 5
    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->O8(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onDestroy()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onDestroy()V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->o〇0(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final showGpRedeem(Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$StartESignActivityEvent;)V
    .locals 4
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$StartESignActivityEvent;->〇080()Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$StartESignActivityEvent;->〇o00〇〇Oo()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    if-nez p1, :cond_0

    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 21
    .line 22
    const-string v3, "mActivity"

    .line 23
    .line 24
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-static {v2, v0, v1, p1}, Lcom/intsig/camscanner/newsign/signtype/SelectSignTypeHelper;->〇80〇808〇O(Landroidx/fragment/app/FragmentActivity;JLjava/lang/String;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

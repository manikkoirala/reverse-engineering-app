.class public final Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "ESignDocItemProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;,
        Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/newsign/main/home/adapter/ESignMultiType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final oOo0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final oOo〇8o008:Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:I

.field private final o〇00O:I

.field private final 〇080OO8〇0:Lcom/intsig/camscanner/newsign/main/home/adapter/SignHomeAdapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Ljava/text/SimpleDateFormat;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->oOo〇8o008:Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "ESignDocItemProvider::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->oOo0:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(IILcom/intsig/camscanner/newsign/main/home/adapter/SignHomeAdapter;)V
    .locals 1
    .param p3    # Lcom/intsig/camscanner/newsign/main/home/adapter/SignHomeAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "mAdapter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->o〇00O:I

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->O8o08O8O:I

    .line 5
    iput-object p3, p0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->〇080OO8〇0:Lcom/intsig/camscanner/newsign/main/home/adapter/SignHomeAdapter;

    .line 6
    new-instance p1, Ljava/text/SimpleDateFormat;

    const-string p2, "yyyy-MM-dd HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->〇0O:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public synthetic constructor <init>(IILcom/intsig/camscanner/newsign/main/home/adapter/SignHomeAdapter;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    const p2, 0x7f0d045a

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;-><init>(IILcom/intsig/camscanner/newsign/main/home/adapter/SignHomeAdapter;)V

    return-void
.end method

.method private final oo88o8O(Ljava/lang/Long;)Ljava/lang/String;
    .locals 6

    .line 1
    const-string v0, "- -"

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const-wide/16 v1, 0x0

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 8
    .line 9
    .line 10
    move-result-wide v3

    .line 11
    cmp-long v5, v3, v1

    .line 12
    .line 13
    if-eqz v5, :cond_0

    .line 14
    .line 15
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->〇0O:Ljava/text/SimpleDateFormat;

    .line 16
    .line 17
    invoke-virtual {v1, p1}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const-string v1, "{\n                sfm.fo\u2026ionSeconds)\n            }"

    .line 22
    .line 23
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .line 25
    .line 26
    move-object v0, p1

    .line 27
    goto :goto_0

    .line 28
    :catch_0
    sget-object p1, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->oOo0:Ljava/lang/String;

    .line 29
    .line 30
    const-string v1, "parse time error"

    .line 31
    .line 32
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_0
    :goto_0
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o〇O8〇〇o()Landroid/graphics/drawable/Drawable;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/util/DarkModeUtils;->〇080(Landroid/content/Context;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string v0, "#494742"

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const-string v0, "#FFFBF4"

    .line 15
    .line 16
    :goto_0
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 21
    .line 22
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    const/16 v2, 0x8

    .line 36
    .line 37
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    int-to-float v1, v1

    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const-string v1, "Builder()\n              \u2026\n                .build()"

    .line 51
    .line 52
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇00(Landroid/widget/TextView;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3
    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f06020b

    .line 10
    .line 11
    .line 12
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 17
    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇oo〇()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const v2, 0x7f0601e0

    .line 11
    .line 12
    .line 13
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const/16 v2, 0x8

    .line 28
    .line 29
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    int-to-float v1, v1

    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    new-instance p2, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;

    .line 11
    .line 12
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string v0, "baseViewHolder.itemView"

    .line 15
    .line 16
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;-><init>(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object p2
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/newsign/main/home/adapter/ESignMultiType;)V
    .locals 18
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/newsign/main/home/adapter/ESignMultiType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v2, p2

    .line 6
    .line 7
    const-string v3, "helper"

    .line 8
    .line 9
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-string v3, "item"

    .line 13
    .line 14
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    instance-of v3, v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 18
    .line 19
    if-nez v3, :cond_0

    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    instance-of v3, v1, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;

    .line 23
    .line 24
    if-nez v3, :cond_1

    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 28
    .line 29
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇0000OOO()Lcom/intsig/camscanner/newsign/data/ESignInfo;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    if-nez v3, :cond_2

    .line 34
    .line 35
    return-void

    .line 36
    :cond_2
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getFile_status()Ljava/lang/Integer;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    const/4 v5, 0x0

    .line 41
    if-eqz v4, :cond_3

    .line 42
    .line 43
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    goto :goto_0

    .line 48
    :cond_3
    const/4 v4, 0x0

    .line 49
    :goto_0
    move-object v6, v1

    .line 50
    check-cast v6, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;

    .line 51
    .line 52
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇0000OOO()Landroid/widget/TextView;

    .line 53
    .line 54
    .line 55
    move-result-object v7

    .line 56
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v8

    .line 60
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    .line 62
    .line 63
    const-string v7, "format(format, *args)"

    .line 64
    .line 65
    const/16 v8, 0x8

    .line 66
    .line 67
    const/4 v9, 0x1

    .line 68
    if-nez v4, :cond_4

    .line 69
    .line 70
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->O〇8O8〇008()Landroid/widget/TextView;

    .line 71
    .line 72
    .line 73
    move-result-object v10

    .line 74
    invoke-virtual {v10, v8}, Landroid/view/View;->setVisibility(I)V

    .line 75
    .line 76
    .line 77
    goto/16 :goto_3

    .line 78
    .line 79
    :cond_4
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->O〇8O8〇008()Landroid/widget/TextView;

    .line 80
    .line 81
    .line 82
    move-result-object v10

    .line 83
    invoke-virtual {v10, v5}, Landroid/view/View;->setVisibility(I)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getManager_nickname()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v10

    .line 90
    if-eqz v10, :cond_6

    .line 91
    .line 92
    invoke-static {v10}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 93
    .line 94
    .line 95
    move-result v11

    .line 96
    if-eqz v11, :cond_5

    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_5
    const/4 v11, 0x0

    .line 100
    goto :goto_2

    .line 101
    :cond_6
    :goto_1
    const/4 v11, 0x1

    .line 102
    :goto_2
    if-eqz v11, :cond_7

    .line 103
    .line 104
    const-string v10, "- -"

    .line 105
    .line 106
    :cond_7
    sget-object v11, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 107
    .line 108
    const v11, 0x7f131425

    .line 109
    .line 110
    .line 111
    invoke-static {v11}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v11

    .line 115
    new-array v12, v9, [Ljava/lang/Object;

    .line 116
    .line 117
    aput-object v10, v12, v5

    .line 118
    .line 119
    invoke-static {v12, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 120
    .line 121
    .line 122
    move-result-object v12

    .line 123
    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v11

    .line 127
    invoke-static {v11, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    new-instance v15, Landroid/text/SpannableStringBuilder;

    .line 131
    .line 132
    invoke-direct {v15, v11}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 133
    .line 134
    .line 135
    const/4 v13, 0x0

    .line 136
    const/4 v14, 0x0

    .line 137
    const/16 v16, 0x6

    .line 138
    .line 139
    const/16 v17, 0x0

    .line 140
    .line 141
    move-object v12, v10

    .line 142
    move-object v8, v15

    .line 143
    move/from16 v15, v16

    .line 144
    .line 145
    move-object/from16 v16, v17

    .line 146
    .line 147
    invoke-static/range {v11 .. v16}, Lkotlin/text/StringsKt;->oO00OOO(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    .line 148
    .line 149
    .line 150
    move-result v11

    .line 151
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    .line 152
    .line 153
    .line 154
    move-result v10

    .line 155
    add-int/2addr v10, v11

    .line 156
    new-instance v12, Landroid/text/style/StyleSpan;

    .line 157
    .line 158
    invoke-direct {v12, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 159
    .line 160
    .line 161
    const/16 v13, 0x21

    .line 162
    .line 163
    invoke-virtual {v8, v12, v11, v10, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 164
    .line 165
    .line 166
    new-instance v12, Landroid/text/style/ForegroundColorSpan;

    .line 167
    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 169
    .line 170
    .line 171
    move-result-object v14

    .line 172
    const v15, 0x7f060207

    .line 173
    .line 174
    .line 175
    invoke-static {v14, v15}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 176
    .line 177
    .line 178
    move-result v14

    .line 179
    invoke-direct {v12, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 180
    .line 181
    .line 182
    invoke-virtual {v8, v12, v11, v10, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->O〇8O8〇008()Landroid/widget/TextView;

    .line 186
    .line 187
    .line 188
    move-result-object v10

    .line 189
    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    .line 191
    .line 192
    :goto_3
    const/4 v8, 0x0

    .line 193
    if-eqz v4, :cond_c

    .line 194
    .line 195
    const/16 v10, 0x3e8

    .line 196
    .line 197
    if-eq v4, v9, :cond_9

    .line 198
    .line 199
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇oOO8O8()Landroid/widget/TextView;

    .line 200
    .line 201
    .line 202
    move-result-object v11

    .line 203
    invoke-virtual {v11, v5}, Landroid/view/View;->setVisibility(I)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getExpire_time()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v11

    .line 210
    if-eqz v11, :cond_8

    .line 211
    .line 212
    invoke-static {v11}, Lkotlin/text/StringsKt;->Oooo8o0〇(Ljava/lang/String;)Ljava/lang/Long;

    .line 213
    .line 214
    .line 215
    move-result-object v11

    .line 216
    if-eqz v11, :cond_8

    .line 217
    .line 218
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    .line 219
    .line 220
    .line 221
    move-result-wide v11

    .line 222
    int-to-long v13, v10

    .line 223
    mul-long v11, v11, v13

    .line 224
    .line 225
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 226
    .line 227
    .line 228
    move-result-object v10

    .line 229
    goto :goto_4

    .line 230
    :cond_8
    move-object v10, v8

    .line 231
    :goto_4
    invoke-direct {v0, v10}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->oo88o8O(Ljava/lang/Long;)Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v10

    .line 235
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇oOO8O8()Landroid/widget/TextView;

    .line 236
    .line 237
    .line 238
    move-result-object v11

    .line 239
    sget-object v12, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 240
    .line 241
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 242
    .line 243
    .line 244
    move-result-object v12

    .line 245
    const v13, 0x7f131895

    .line 246
    .line 247
    .line 248
    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 249
    .line 250
    .line 251
    move-result-object v12

    .line 252
    const-string v13, "context.getString(R.string.cs_646_sign_ddl)"

    .line 253
    .line 254
    invoke-static {v12, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    .line 256
    .line 257
    new-array v13, v9, [Ljava/lang/Object;

    .line 258
    .line 259
    aput-object v10, v13, v5

    .line 260
    .line 261
    invoke-static {v13, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 262
    .line 263
    .line 264
    move-result-object v10

    .line 265
    invoke-static {v12, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 266
    .line 267
    .line 268
    move-result-object v10

    .line 269
    invoke-static {v10, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    .line 271
    .line 272
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    .line 274
    .line 275
    goto/16 :goto_6

    .line 276
    .line 277
    :cond_9
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇oOO8O8()Landroid/widget/TextView;

    .line 278
    .line 279
    .line 280
    move-result-object v7

    .line 281
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 282
    .line 283
    .line 284
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getFinish_tm()Ljava/lang/String;

    .line 285
    .line 286
    .line 287
    move-result-object v7

    .line 288
    if-eqz v7, :cond_a

    .line 289
    .line 290
    invoke-static {v7}, Lkotlin/text/StringsKt;->Oooo8o0〇(Ljava/lang/String;)Ljava/lang/Long;

    .line 291
    .line 292
    .line 293
    move-result-object v7

    .line 294
    if-eqz v7, :cond_a

    .line 295
    .line 296
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    .line 297
    .line 298
    .line 299
    move-result-wide v11

    .line 300
    int-to-long v13, v10

    .line 301
    mul-long v11, v11, v13

    .line 302
    .line 303
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 304
    .line 305
    .line 306
    move-result-object v7

    .line 307
    goto :goto_5

    .line 308
    :cond_a
    move-object v7, v8

    .line 309
    :goto_5
    if-nez v7, :cond_b

    .line 310
    .line 311
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00〇8()J

    .line 312
    .line 313
    .line 314
    move-result-wide v10

    .line 315
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 316
    .line 317
    .line 318
    move-result-object v7

    .line 319
    :cond_b
    invoke-direct {v0, v7}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->oo88o8O(Ljava/lang/Long;)Ljava/lang/String;

    .line 320
    .line 321
    .line 322
    move-result-object v7

    .line 323
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇oOO8O8()Landroid/widget/TextView;

    .line 324
    .line 325
    .line 326
    move-result-object v10

    .line 327
    sget-object v11, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 328
    .line 329
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 330
    .line 331
    .line 332
    move-result-object v11

    .line 333
    sget-object v12, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 334
    .line 335
    invoke-virtual {v12}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 336
    .line 337
    .line 338
    move-result-object v12

    .line 339
    const v13, 0x7f1318a9

    .line 340
    .line 341
    .line 342
    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 343
    .line 344
    .line 345
    move-result-object v12

    .line 346
    const-string v13, "ApplicationHelper.sConte\u2026_646_sign_signature_time)"

    .line 347
    .line 348
    invoke-static {v12, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 349
    .line 350
    .line 351
    new-array v13, v9, [Ljava/lang/Object;

    .line 352
    .line 353
    aput-object v7, v13, v5

    .line 354
    .line 355
    invoke-static {v13, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 356
    .line 357
    .line 358
    move-result-object v7

    .line 359
    invoke-static {v11, v12, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 360
    .line 361
    .line 362
    move-result-object v7

    .line 363
    const-string v11, "format(locale, format, *args)"

    .line 364
    .line 365
    invoke-static {v7, v11}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    .line 367
    .line 368
    invoke-virtual {v10, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    .line 370
    .line 371
    goto :goto_6

    .line 372
    :cond_c
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇oOO8O8()Landroid/widget/TextView;

    .line 373
    .line 374
    .line 375
    move-result-object v7

    .line 376
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 377
    .line 378
    .line 379
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇O00()J

    .line 380
    .line 381
    .line 382
    move-result-wide v10

    .line 383
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 384
    .line 385
    .line 386
    move-result-object v7

    .line 387
    invoke-direct {v0, v7}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->oo88o8O(Ljava/lang/Long;)Ljava/lang/String;

    .line 388
    .line 389
    .line 390
    move-result-object v7

    .line 391
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇oOO8O8()Landroid/widget/TextView;

    .line 392
    .line 393
    .line 394
    move-result-object v10

    .line 395
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 396
    .line 397
    .line 398
    move-result-object v11

    .line 399
    const v12, 0x7f131346

    .line 400
    .line 401
    .line 402
    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 403
    .line 404
    .line 405
    move-result-object v11

    .line 406
    new-instance v12, Ljava/lang/StringBuilder;

    .line 407
    .line 408
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 409
    .line 410
    .line 411
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    .line 413
    .line 414
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    .line 416
    .line 417
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 418
    .line 419
    .line 420
    move-result-object v7

    .line 421
    invoke-virtual {v10, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    .line 423
    .line 424
    :goto_6
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇00()Landroid/widget/ImageView;

    .line 425
    .line 426
    .line 427
    move-result-object v7

    .line 428
    const/16 v10, 0x8

    .line 429
    .line 430
    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 431
    .line 432
    .line 433
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->O8ooOoo〇()Landroid/widget/TextView;

    .line 434
    .line 435
    .line 436
    move-result-object v7

    .line 437
    if-eqz v4, :cond_1a

    .line 438
    .line 439
    if-eq v4, v9, :cond_19

    .line 440
    .line 441
    const/4 v11, 0x2

    .line 442
    if-eq v4, v11, :cond_e

    .line 443
    .line 444
    const/4 v3, 0x3

    .line 445
    if-eq v4, v3, :cond_d

    .line 446
    .line 447
    goto/16 :goto_f

    .line 448
    .line 449
    :cond_d
    invoke-direct {v0, v7}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->〇00(Landroid/widget/TextView;)V

    .line 450
    .line 451
    .line 452
    sget-object v3, Lcom/intsig/camscanner/newsign/ESignHelper;->〇080:Lcom/intsig/camscanner/newsign/ESignHelper;

    .line 453
    .line 454
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 455
    .line 456
    .line 457
    move-result-object v5

    .line 458
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 459
    .line 460
    .line 461
    move-result-object v4

    .line 462
    invoke-virtual {v3, v5, v4}, Lcom/intsig/camscanner/newsign/ESignHelper;->OO0o〇〇(Landroid/content/Context;Ljava/lang/Integer;)Ljava/lang/String;

    .line 463
    .line 464
    .line 465
    move-result-object v3

    .line 466
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 467
    .line 468
    .line 469
    goto/16 :goto_f

    .line 470
    .line 471
    :cond_e
    sget-object v11, Lcom/intsig/camscanner/newsign/ESignHelper;->〇080:Lcom/intsig/camscanner/newsign/ESignHelper;

    .line 472
    .line 473
    invoke-virtual {v11, v3}, Lcom/intsig/camscanner/newsign/ESignHelper;->Oooo8o0〇(Lcom/intsig/camscanner/newsign/data/ESignInfo;)Z

    .line 474
    .line 475
    .line 476
    move-result v12

    .line 477
    if-eqz v12, :cond_f

    .line 478
    .line 479
    invoke-direct {v0, v7}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->〇00(Landroid/widget/TextView;)V

    .line 480
    .line 481
    .line 482
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 483
    .line 484
    .line 485
    move-result-object v3

    .line 486
    const v4, 0x7f1313ea    # 1.9549992E38f

    .line 487
    .line 488
    .line 489
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 490
    .line 491
    .line 492
    move-result-object v3

    .line 493
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    .line 495
    .line 496
    goto/16 :goto_f

    .line 497
    .line 498
    :cond_f
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 499
    .line 500
    .line 501
    const-string v12, "#386FD9"

    .line 502
    .line 503
    invoke-static {v12}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 504
    .line 505
    .line 506
    move-result v12

    .line 507
    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 508
    .line 509
    .line 510
    invoke-virtual {v7, v8}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 511
    .line 512
    .line 513
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getUser_role()Ljava/lang/Integer;

    .line 514
    .line 515
    .line 516
    move-result-object v8

    .line 517
    if-nez v8, :cond_10

    .line 518
    .line 519
    goto :goto_a

    .line 520
    :cond_10
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    .line 521
    .line 522
    .line 523
    move-result v8

    .line 524
    if-ne v8, v9, :cond_15

    .line 525
    .line 526
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇00()Landroid/widget/ImageView;

    .line 527
    .line 528
    .line 529
    move-result-object v6

    .line 530
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getSign_status()Ljava/lang/Integer;

    .line 531
    .line 532
    .line 533
    move-result-object v8

    .line 534
    if-nez v8, :cond_11

    .line 535
    .line 536
    goto :goto_7

    .line 537
    :cond_11
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    .line 538
    .line 539
    .line 540
    move-result v8

    .line 541
    if-nez v8, :cond_13

    .line 542
    .line 543
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getShare_with_me()Ljava/lang/Integer;

    .line 544
    .line 545
    .line 546
    move-result-object v3

    .line 547
    if-nez v3, :cond_12

    .line 548
    .line 549
    goto :goto_7

    .line 550
    :cond_12
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 551
    .line 552
    .line 553
    move-result v3

    .line 554
    if-ne v3, v9, :cond_13

    .line 555
    .line 556
    goto :goto_8

    .line 557
    :cond_13
    :goto_7
    const/4 v9, 0x0

    .line 558
    :goto_8
    if-eqz v9, :cond_14

    .line 559
    .line 560
    goto :goto_9

    .line 561
    :cond_14
    const/16 v5, 0x8

    .line 562
    .line 563
    :goto_9
    invoke-virtual {v6, v5}, Landroid/view/View;->setVisibility(I)V

    .line 564
    .line 565
    .line 566
    goto :goto_e

    .line 567
    :cond_15
    :goto_a
    invoke-virtual {v6}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider$ItemViewHolder;->〇00()Landroid/widget/ImageView;

    .line 568
    .line 569
    .line 570
    move-result-object v6

    .line 571
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/data/ESignInfo;->getSign_status()Ljava/lang/Integer;

    .line 572
    .line 573
    .line 574
    move-result-object v3

    .line 575
    if-nez v3, :cond_16

    .line 576
    .line 577
    goto :goto_b

    .line 578
    :cond_16
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 579
    .line 580
    .line 581
    move-result v3

    .line 582
    if-nez v3, :cond_17

    .line 583
    .line 584
    goto :goto_c

    .line 585
    :cond_17
    :goto_b
    const/4 v9, 0x0

    .line 586
    :goto_c
    if-eqz v9, :cond_18

    .line 587
    .line 588
    goto :goto_d

    .line 589
    :cond_18
    const/16 v5, 0x8

    .line 590
    .line 591
    :goto_d
    invoke-virtual {v6, v5}, Landroid/view/View;->setVisibility(I)V

    .line 592
    .line 593
    .line 594
    :goto_e
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 595
    .line 596
    .line 597
    move-result-object v3

    .line 598
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 599
    .line 600
    .line 601
    move-result-object v4

    .line 602
    invoke-virtual {v11, v3, v4}, Lcom/intsig/camscanner/newsign/ESignHelper;->OO0o〇〇(Landroid/content/Context;Ljava/lang/Integer;)Ljava/lang/String;

    .line 603
    .line 604
    .line 605
    move-result-object v3

    .line 606
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 607
    .line 608
    .line 609
    goto :goto_f

    .line 610
    :cond_19
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 611
    .line 612
    .line 613
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 614
    .line 615
    .line 616
    move-result-object v3

    .line 617
    const v5, 0x7f0601ee

    .line 618
    .line 619
    .line 620
    invoke-static {v3, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 621
    .line 622
    .line 623
    move-result v3

    .line 624
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 625
    .line 626
    .line 627
    invoke-virtual {v7, v8}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 628
    .line 629
    .line 630
    sget-object v3, Lcom/intsig/camscanner/newsign/ESignHelper;->〇080:Lcom/intsig/camscanner/newsign/ESignHelper;

    .line 631
    .line 632
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 633
    .line 634
    .line 635
    move-result-object v5

    .line 636
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 637
    .line 638
    .line 639
    move-result-object v4

    .line 640
    invoke-virtual {v3, v5, v4}, Lcom/intsig/camscanner/newsign/ESignHelper;->OO0o〇〇(Landroid/content/Context;Ljava/lang/Integer;)Ljava/lang/String;

    .line 641
    .line 642
    .line 643
    move-result-object v3

    .line 644
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 645
    .line 646
    .line 647
    goto :goto_f

    .line 648
    :cond_1a
    invoke-virtual {v7, v5}, Landroid/view/View;->setVisibility(I)V

    .line 649
    .line 650
    .line 651
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 652
    .line 653
    .line 654
    move-result-object v3

    .line 655
    const v5, 0x7f060205

    .line 656
    .line 657
    .line 658
    invoke-static {v3, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 659
    .line 660
    .line 661
    move-result v3

    .line 662
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 663
    .line 664
    .line 665
    invoke-virtual {v7, v8}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 666
    .line 667
    .line 668
    sget-object v3, Lcom/intsig/camscanner/newsign/ESignHelper;->〇080:Lcom/intsig/camscanner/newsign/ESignHelper;

    .line 669
    .line 670
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 671
    .line 672
    .line 673
    move-result-object v5

    .line 674
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 675
    .line 676
    .line 677
    move-result-object v4

    .line 678
    invoke-virtual {v3, v5, v4}, Lcom/intsig/camscanner/newsign/ESignHelper;->OO0o〇〇(Landroid/content/Context;Ljava/lang/Integer;)Ljava/lang/String;

    .line 679
    .line 680
    .line 681
    move-result-object v3

    .line 682
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 683
    .line 684
    .line 685
    :goto_f
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 686
    .line 687
    .line 688
    move-result-wide v2

    .line 689
    iget-object v4, v0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->〇080OO8〇0:Lcom/intsig/camscanner/newsign/main/home/adapter/SignHomeAdapter;

    .line 690
    .line 691
    invoke-virtual {v4}, Lcom/intsig/camscanner/newsign/main/home/adapter/SignHomeAdapter;->o8O0()J

    .line 692
    .line 693
    .line 694
    move-result-wide v4

    .line 695
    cmp-long v6, v2, v4

    .line 696
    .line 697
    if-nez v6, :cond_1b

    .line 698
    .line 699
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 700
    .line 701
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->o〇O8〇〇o()Landroid/graphics/drawable/Drawable;

    .line 702
    .line 703
    .line 704
    move-result-object v2

    .line 705
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 706
    .line 707
    .line 708
    goto :goto_10

    .line 709
    :cond_1b
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 710
    .line 711
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->〇oo〇()Landroid/graphics/drawable/GradientDrawable;

    .line 712
    .line 713
    .line 714
    move-result-object v2

    .line 715
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 716
    .line 717
    .line 718
    :goto_10
    return-void
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignMultiType;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/newsign/main/home/adapter/ESignMultiType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/newsign/main/home/adapter/ESignDocItemProvider;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

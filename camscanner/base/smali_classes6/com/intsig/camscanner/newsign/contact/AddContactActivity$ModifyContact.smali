.class public final Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;
.super Ljava/lang/Object;
.source "AddContactActivity.kt"

# interfaces
.implements Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ContactEditStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/newsign/contact/AddContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ModifyContact"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o8o:Lcom/intsig/camscanner/newsign/contact/AddContactActivity$Companion;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$Companion;->〇080()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const-string v0, "ModifyContact Strategy"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method private static final O8(Landroidx/appcompat/widget/AppCompatEditText;)V
    .locals 1

    .line 1
    const-string v0, "$etName"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Lcom/intsig/utils/KeyboardUtils;->〇8o8o〇(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o〇(Landroidx/appcompat/widget/AppCompatEditText;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->O8(Landroidx/appcompat/widget/AppCompatEditText;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 2
    .line 3
    const v1, 0x7f131d1e

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "getString(R.string.esign_fake23)"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080()V
    .locals 15

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O88(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 11
    .line 12
    invoke-static {v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->〇oOO80o(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/newsign/data/ESignContact;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-nez v1, :cond_1

    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getContact_id()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    if-nez v2, :cond_2

    .line 24
    .line 25
    return-void

    .line 26
    :cond_2
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->OoO8()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatEditText;

    .line 31
    .line 32
    invoke-virtual {v4}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getNickname()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v5

    .line 44
    const/4 v6, 0x2

    .line 45
    const/4 v7, 0x0

    .line 46
    const/4 v8, 0x0

    .line 47
    invoke-static {v5, v4, v7, v6, v8}, Lkotlin/text/StringsKt;->OoO8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    if-eqz v5, :cond_3

    .line 52
    .line 53
    move-object v14, v8

    .line 54
    goto :goto_0

    .line 55
    :cond_3
    move-object v14, v4

    .line 56
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->enableModifyNameAndPhoneEmail()Z

    .line 57
    .line 58
    .line 59
    move-result v4

    .line 60
    if-eqz v4, :cond_d

    .line 61
    .line 62
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->〇8〇oO〇〇8o:Landroid/widget/LinearLayout;

    .line 63
    .line 64
    const-string v4, "binding.llPhone"

    .line 65
    .line 66
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    const/4 v4, 0x1

    .line 74
    if-nez v1, :cond_4

    .line 75
    .line 76
    const/4 v1, 0x1

    .line 77
    goto :goto_1

    .line 78
    :cond_4
    const/4 v1, 0x0

    .line 79
    :goto_1
    const-string v5, "getString(R.string.cs_631_sign_myinfo)"

    .line 80
    .line 81
    const v6, 0x7f131406

    .line 82
    .line 83
    .line 84
    if-eqz v1, :cond_a

    .line 85
    .line 86
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 87
    .line 88
    invoke-static {v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O00OoO〇(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;->〇oo〇()Landroidx/lifecycle/MutableLiveData;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    invoke-virtual {v1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    check-cast v1, Ljava/lang/String;

    .line 101
    .line 102
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatEditText;

    .line 103
    .line 104
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 113
    .line 114
    .line 115
    move-result v8

    .line 116
    if-eqz v8, :cond_5

    .line 117
    .line 118
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 119
    .line 120
    .line 121
    move-result v3

    .line 122
    if-nez v3, :cond_5

    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 125
    .line 126
    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v1

    .line 130
    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    invoke-static {v0, v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->OO0o(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    return-void

    .line 137
    :cond_5
    invoke-static {v0}, Lcom/intsig/comm/util/StringUtilDelegate;->O8(Ljava/lang/String;)Z

    .line 138
    .line 139
    .line 140
    move-result v3

    .line 141
    if-nez v3, :cond_6

    .line 142
    .line 143
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 144
    .line 145
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o0O0O〇〇〇0(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    const v1, 0x7f130566

    .line 150
    .line 151
    .line 152
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 153
    .line 154
    .line 155
    return-void

    .line 156
    :cond_6
    if-eqz v1, :cond_7

    .line 157
    .line 158
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 159
    .line 160
    .line 161
    move-result v3

    .line 162
    if-eqz v3, :cond_8

    .line 163
    .line 164
    :cond_7
    const/4 v7, 0x1

    .line 165
    :cond_8
    if-eqz v7, :cond_9

    .line 166
    .line 167
    goto :goto_2

    .line 168
    :cond_9
    new-instance v3, Ljava/lang/StringBuilder;

    .line 169
    .line 170
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .line 172
    .line 173
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    const-string v1, "-"

    .line 177
    .line 178
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    :goto_2
    move-object v13, v0

    .line 189
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 190
    .line 191
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O00OoO〇(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;

    .line 192
    .line 193
    .line 194
    move-result-object v9

    .line 195
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v10

    .line 199
    invoke-static {v2}, Lkotlin/text/StringsKt;->Oooo8o0〇(Ljava/lang/String;)Ljava/lang/Long;

    .line 200
    .line 201
    .line 202
    move-result-object v11

    .line 203
    const-string v12, ""

    .line 204
    .line 205
    invoke-virtual/range {v9 .. v14}, Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;->O8ooOoo〇(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .line 207
    .line 208
    goto :goto_3

    .line 209
    :cond_a
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->OO:Landroidx/appcompat/widget/AppCompatEditText;

    .line 210
    .line 211
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 212
    .line 213
    .line 214
    move-result-object v0

    .line 215
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v12

    .line 219
    invoke-static {v12, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 220
    .line 221
    .line 222
    move-result v0

    .line 223
    if-eqz v0, :cond_b

    .line 224
    .line 225
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 226
    .line 227
    .line 228
    move-result v0

    .line 229
    if-nez v0, :cond_b

    .line 230
    .line 231
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 232
    .line 233
    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object v1

    .line 237
    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    .line 239
    .line 240
    invoke-static {v0, v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->OO0o(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;Ljava/lang/String;)V

    .line 241
    .line 242
    .line 243
    return-void

    .line 244
    :cond_b
    invoke-static {v12}, Lcom/intsig/tsapp/account/util/AccountUtils;->O8〇o(Ljava/lang/String;)Z

    .line 245
    .line 246
    .line 247
    move-result v0

    .line 248
    if-nez v0, :cond_c

    .line 249
    .line 250
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 251
    .line 252
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o0O0O〇〇〇0(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    const v1, 0x7f131d16

    .line 257
    .line 258
    .line 259
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 260
    .line 261
    .line 262
    return-void

    .line 263
    :cond_c
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 264
    .line 265
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O00OoO〇(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;

    .line 266
    .line 267
    .line 268
    move-result-object v9

    .line 269
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object v10

    .line 273
    invoke-static {v2}, Lkotlin/text/StringsKt;->Oooo8o0〇(Ljava/lang/String;)Ljava/lang/Long;

    .line 274
    .line 275
    .line 276
    move-result-object v11

    .line 277
    const-string v13, ""

    .line 278
    .line 279
    invoke-virtual/range {v9 .. v14}, Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;->O8ooOoo〇(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    goto :goto_3

    .line 283
    :cond_d
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->enableModifyName()Z

    .line 284
    .line 285
    .line 286
    move-result v0

    .line 287
    if-eqz v0, :cond_e

    .line 288
    .line 289
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 290
    .line 291
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O00OoO〇(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;

    .line 292
    .line 293
    .line 294
    move-result-object v9

    .line 295
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 296
    .line 297
    .line 298
    move-result-object v10

    .line 299
    invoke-static {v2}, Lkotlin/text/StringsKt;->Oooo8o0〇(Ljava/lang/String;)Ljava/lang/Long;

    .line 300
    .line 301
    .line 302
    move-result-object v11

    .line 303
    const/4 v12, 0x0

    .line 304
    const/4 v13, 0x0

    .line 305
    invoke-virtual/range {v9 .. v14}, Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;->O8ooOoo〇(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    .line 307
    .line 308
    :cond_e
    :goto_3
    return-void
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public 〇o00〇〇Oo()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O88(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 11
    .line 12
    invoke-static {v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->〇oOO80o(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/newsign/data/ESignContact;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-nez v1, :cond_1

    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->enableModifyName()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->enableModifyNameAndPhoneEmail()Z

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    sget-object v4, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o8o:Lcom/intsig/camscanner/newsign/contact/AddContactActivity$Companion;

    .line 28
    .line 29
    invoke-virtual {v4}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$Companion;->〇080()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    new-instance v5, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v6, "enableModifyName == "

    .line 39
    .line 40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v6, " ,enableModifyNameAndPhone == "

    .line 47
    .line 48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v5

    .line 58
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatEditText;

    .line 62
    .line 63
    const-string v5, "binding.etName"

    .line 64
    .line 65
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    if-eqz v2, :cond_2

    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 71
    .line 72
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getNickname()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v5

    .line 76
    invoke-virtual {v2, v4, v5}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O08〇oO8〇(Landroid/widget/EditText;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    new-instance v2, LoooO888/〇O00;

    .line 80
    .line 81
    invoke-direct {v2, v4}, LoooO888/〇O00;-><init>(Landroidx/appcompat/widget/AppCompatEditText;)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v4, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getNickname()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    .line 94
    .line 95
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 96
    .line 97
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O〇00O(Landroid/widget/EditText;)V

    .line 98
    .line 99
    .line 100
    :goto_0
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatEditText;

    .line 101
    .line 102
    const-string v4, "binding.etPhone"

    .line 103
    .line 104
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->OO:Landroidx/appcompat/widget/AppCompatEditText;

    .line 108
    .line 109
    const-string v5, "binding.etEmail"

    .line 110
    .line 111
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getPhone()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v5

    .line 118
    const/4 v6, 0x1

    .line 119
    const/4 v7, 0x0

    .line 120
    if-eqz v5, :cond_4

    .line 121
    .line 122
    invoke-static {v5}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 123
    .line 124
    .line 125
    move-result v5

    .line 126
    if-eqz v5, :cond_3

    .line 127
    .line 128
    goto :goto_1

    .line 129
    :cond_3
    const/4 v5, 0x0

    .line 130
    goto :goto_2

    .line 131
    :cond_4
    :goto_1
    const/4 v5, 0x1

    .line 132
    :goto_2
    xor-int/2addr v5, v6

    .line 133
    iget-object v6, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 134
    .line 135
    invoke-static {v6, v5}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->oOO8oo0(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;Z)V

    .line 136
    .line 137
    .line 138
    if-eqz v3, :cond_6

    .line 139
    .line 140
    if-eqz v5, :cond_5

    .line 141
    .line 142
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 143
    .line 144
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getPhone()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v1

    .line 148
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O08〇oO8〇(Landroid/widget/EditText;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    goto :goto_4

    .line 152
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 153
    .line 154
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getEmail()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    invoke-virtual {v0, v4, v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O08〇oO8〇(Landroid/widget/EditText;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    goto :goto_4

    .line 162
    :cond_6
    if-eqz v5, :cond_7

    .line 163
    .line 164
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getPhone()Ljava/lang/String;

    .line 165
    .line 166
    .line 167
    move-result-object v1

    .line 168
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    .line 170
    .line 171
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 172
    .line 173
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O〇00O(Landroid/widget/EditText;)V

    .line 174
    .line 175
    .line 176
    goto :goto_3

    .line 177
    :cond_7
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/data/ESignContact;->getEmail()Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v1

    .line 181
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    .line 183
    .line 184
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ModifyContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 185
    .line 186
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O〇00O(Landroid/widget/EditText;)V

    .line 187
    .line 188
    .line 189
    :goto_3
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 190
    .line 191
    invoke-virtual {v1, v7}, Landroid/view/View;->setClickable(Z)V

    .line 192
    .line 193
    .line 194
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 195
    .line 196
    invoke-virtual {v0, v7}, Landroid/view/View;->setClickable(Z)V

    .line 197
    .line 198
    .line 199
    :goto_4
    return-void
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

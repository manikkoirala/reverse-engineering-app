.class public final Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;
.super Ljava/lang/Object;
.source "AddContactActivity.kt"

# interfaces
.implements Lcom/intsig/camscanner/newsign/contact/AddContactActivity$ContactEditStrategy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/newsign/contact/AddContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "AddContact"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o8o:Lcom/intsig/camscanner/newsign/contact/AddContactActivity$Companion;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$Companion;->〇080()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const-string v0, "AddContact Strategy"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method private final O8()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O88(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->OO:Landroidx/appcompat/widget/AppCompatEditText;

    .line 11
    .line 12
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatEditText;

    .line 21
    .line 22
    invoke-virtual {v2}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 31
    .line 32
    invoke-static {v3}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o0O0O〇〇〇0(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-static {v3}, Lcom/intsig/advertisement/util/NetworkUtil;->〇080(Landroid/content/Context;)Z

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    if-nez v3, :cond_1

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 43
    .line 44
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o0O0O〇〇〇0(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 49
    .line 50
    const v2, 0x7f13008d

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->OoO8()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    if-nez v4, :cond_a

    .line 70
    .line 71
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    move-result v4

    .line 75
    if-eqz v4, :cond_2

    .line 76
    .line 77
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 78
    .line 79
    .line 80
    move-result v3

    .line 81
    if-nez v3, :cond_2

    .line 82
    .line 83
    goto/16 :goto_4

    .line 84
    .line 85
    :cond_2
    iget-object v3, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 86
    .line 87
    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    invoke-static {v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 92
    .line 93
    .line 94
    move-result v3

    .line 95
    const/4 v4, 0x1

    .line 96
    if-nez v3, :cond_3

    .line 97
    .line 98
    sget-object v0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o8o:Lcom/intsig/camscanner/newsign/contact/AddContactActivity$Companion;

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$Companion;->〇080()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    const-string v1, "share dir link need login"

    .line 105
    .line 106
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 110
    .line 111
    invoke-static {v0, v4}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->〇ooO〇000(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;I)Z

    .line 112
    .line 113
    .line 114
    return-void

    .line 115
    :cond_3
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->OO〇00〇8oO:Landroid/widget/LinearLayout;

    .line 116
    .line 117
    const-string v5, "binding.llEmail"

    .line 118
    .line 119
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    .line 123
    .line 124
    .line 125
    move-result v3

    .line 126
    const/4 v6, 0x0

    .line 127
    if-nez v3, :cond_4

    .line 128
    .line 129
    const/4 v3, 0x1

    .line 130
    goto :goto_0

    .line 131
    :cond_4
    const/4 v3, 0x0

    .line 132
    :goto_0
    if-eqz v3, :cond_5

    .line 133
    .line 134
    invoke-static {v1}, Lcom/intsig/tsapp/account/util/AccountUtils;->O8〇o(Ljava/lang/String;)Z

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    if-nez v1, :cond_5

    .line 139
    .line 140
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 141
    .line 142
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o0O0O〇〇〇0(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    const v1, 0x7f131d16

    .line 147
    .line 148
    .line 149
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 150
    .line 151
    .line 152
    return-void

    .line 153
    :cond_5
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->〇8〇oO〇〇8o:Landroid/widget/LinearLayout;

    .line 154
    .line 155
    const-string v3, "binding.llPhone"

    .line 156
    .line 157
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    .line 161
    .line 162
    .line 163
    move-result v1

    .line 164
    if-nez v1, :cond_6

    .line 165
    .line 166
    const/4 v1, 0x1

    .line 167
    goto :goto_1

    .line 168
    :cond_6
    const/4 v1, 0x0

    .line 169
    :goto_1
    if-eqz v1, :cond_7

    .line 170
    .line 171
    invoke-static {v2}, Lcom/intsig/comm/util/StringUtilDelegate;->O8(Ljava/lang/String;)Z

    .line 172
    .line 173
    .line 174
    move-result v1

    .line 175
    if-nez v1, :cond_7

    .line 176
    .line 177
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 178
    .line 179
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->o0O0O〇〇〇0(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 180
    .line 181
    .line 182
    move-result-object v0

    .line 183
    const v1, 0x7f130566

    .line 184
    .line 185
    .line 186
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 187
    .line 188
    .line 189
    return-void

    .line 190
    :cond_7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->OO〇00〇8oO:Landroid/widget/LinearLayout;

    .line 191
    .line 192
    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    .line 196
    .line 197
    .line 198
    move-result v1

    .line 199
    if-nez v1, :cond_8

    .line 200
    .line 201
    goto :goto_2

    .line 202
    :cond_8
    const/4 v4, 0x0

    .line 203
    :goto_2
    if-eqz v4, :cond_9

    .line 204
    .line 205
    const-string v1, "email"

    .line 206
    .line 207
    goto :goto_3

    .line 208
    :cond_9
    const-string v1, "phone"

    .line 209
    .line 210
    :goto_3
    sget-object v2, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent;

    .line 211
    .line 212
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇o〇(Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 216
    .line 217
    invoke-static {v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O00OoO〇(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;

    .line 218
    .line 219
    .line 220
    move-result-object v2

    .line 221
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object v3

    .line 225
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->OO:Landroidx/appcompat/widget/AppCompatEditText;

    .line 226
    .line 227
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 228
    .line 229
    .line 230
    move-result-object v1

    .line 231
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v4

    .line 235
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatEditText;

    .line 236
    .line 237
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 238
    .line 239
    .line 240
    move-result-object v1

    .line 241
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 242
    .line 243
    .line 244
    move-result-object v5

    .line 245
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatEditText;

    .line 246
    .line 247
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 248
    .line 249
    .line 250
    move-result-object v0

    .line 251
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 252
    .line 253
    .line 254
    move-result-object v6

    .line 255
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 256
    .line 257
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O00OoO〇(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;

    .line 258
    .line 259
    .line 260
    move-result-object v0

    .line 261
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;->〇oo〇()Landroidx/lifecycle/MutableLiveData;

    .line 262
    .line 263
    .line 264
    move-result-object v0

    .line 265
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 266
    .line 267
    .line 268
    move-result-object v0

    .line 269
    move-object v7, v0

    .line 270
    check-cast v7, Ljava/lang/String;

    .line 271
    .line 272
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/camscanner/newsign/contact/AddContactViewModel;->〇O00(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    .line 274
    .line 275
    return-void

    .line 276
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 277
    .line 278
    const v1, 0x7f131406

    .line 279
    .line 280
    .line 281
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 282
    .line 283
    .line 284
    move-result-object v1

    .line 285
    const-string v2, "getString(R.string.cs_631_sign_myinfo)"

    .line 286
    .line 287
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    .line 289
    .line 290
    invoke-static {v0, v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->OO0o(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;Ljava/lang/String;)V

    .line 291
    .line 292
    .line 293
    return-void
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private static final Oo08(Landroidx/appcompat/widget/AppCompatEditText;)V
    .locals 1

    .line 1
    const-string v0, "$it"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Lcom/intsig/utils/KeyboardUtils;->〇8o8o〇(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o〇(Landroidx/appcompat/widget/AppCompatEditText;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->Oo08(Landroidx/appcompat/widget/AppCompatEditText;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 2
    .line 3
    const v1, 0x7f1313cd

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "getString(R.string.cs_631_sign_add_contact)"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v0, v1}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->〇0o0oO〇〇0(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/contact/AddContactActivity$AddContact;->〇080:Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;->O88(Lcom/intsig/camscanner/newsign/contact/AddContactActivity;)Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityAddSignerBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatEditText;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    new-instance v1, LoooO888/〇〇808〇;

    .line 20
    .line 21
    invoke-direct {v1, v0}, LoooO888/〇〇808〇;-><init>(Landroidx/appcompat/widget/AppCompatEditText;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 25
    .line 26
    .line 27
    :cond_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

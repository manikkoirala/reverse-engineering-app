.class public final Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "ShareByAppActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$PageFinishEvent;,
        Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O〇08oOOO0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final O〇o88o08〇:Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇00O0:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O0O:J

.field private O88O:Ljava/lang/String;

.field private final Oo80:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8o:Lcom/intsig/camscanner/newsign/util/CsStartLoginHelperAct;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8oOOo:I

.field private final oOO〇〇:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oo8ooo8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇oO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08〇o0O:Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$imageAdapter$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O〇〇O8:I

.field private 〇o0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/newsign/data/ESignContact;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇08O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇o〇:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇00O0:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇o88o08〇:Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$Companion;

    .line 31
    .line 32
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "ShareByAppActivity::class.java.simpleName"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 7
    .line 8
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$special$$inlined$viewModels$default$1;

    .line 14
    .line 15
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$special$$inlined$viewModels$default$1;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Landroidx/lifecycle/ViewModelLazy;

    .line 19
    .line 20
    const-class v2, Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel;

    .line 21
    .line 22
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    new-instance v3, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$special$$inlined$viewModels$default$2;

    .line 27
    .line 28
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$special$$inlined$viewModels$default$2;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 29
    .line 30
    .line 31
    new-instance v4, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$special$$inlined$viewModels$default$3;

    .line 32
    .line 33
    const/4 v5, 0x0

    .line 34
    invoke-direct {v4, v5, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$special$$inlined$viewModels$default$3;-><init>(Lkotlin/jvm/functions/Function0;Landroidx/activity/ComponentActivity;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {v1, v2, v3, v0, v4}, Landroidx/lifecycle/ViewModelLazy;-><init>(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 38
    .line 39
    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇〇08O:Lkotlin/Lazy;

    .line 41
    .line 42
    const/4 v0, 0x2

    .line 43
    iput v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8oOOo:I

    .line 44
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    .line 46
    .line 47
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .line 49
    .line 50
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o0O:Ljava/util/ArrayList;

    .line 51
    .line 52
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 53
    .line 54
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 55
    .line 56
    .line 57
    new-instance v1, Lo〇O8OO/〇〇888;

    .line 58
    .line 59
    invoke-direct {v1, p0}, Lo〇O8OO/〇〇888;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    const-string v1, "registerForActivityResul\u2026Enabled()\n        }\n    }"

    .line 67
    .line 68
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oOO〇〇:Landroidx/activity/result/ActivityResultLauncher;

    .line 72
    .line 73
    new-instance v0, Lcom/intsig/camscanner/newsign/util/CsStartLoginHelperAct;

    .line 74
    .line 75
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/newsign/util/CsStartLoginHelperAct;-><init>(Landroidx/appcompat/app/AppCompatActivity;)V

    .line 76
    .line 77
    .line 78
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o:Lcom/intsig/camscanner/newsign/util/CsStartLoginHelperAct;

    .line 79
    .line 80
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 81
    .line 82
    new-instance v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$loadingDialog$2;

    .line 83
    .line 84
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$loadingDialog$2;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 85
    .line 86
    .line 87
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    iput-object v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oo8ooo8O:Lkotlin/Lazy;

    .line 92
    .line 93
    new-instance v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$simpleDateFormat$2;

    .line 94
    .line 95
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$simpleDateFormat$2;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 96
    .line 97
    .line 98
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o〇oO:Lkotlin/Lazy;

    .line 103
    .line 104
    new-instance v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$imageAdapter$1;

    .line 105
    .line 106
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$imageAdapter$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 107
    .line 108
    .line 109
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇08〇o0O:Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$imageAdapter$1;

    .line 110
    .line 111
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 112
    .line 113
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 114
    .line 115
    .line 116
    new-instance v1, Lo〇O8OO/oO80;

    .line 117
    .line 118
    invoke-direct {v1, p0}, Lo〇O8OO/oO80;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    const-string v1, "registerForActivityResul\u2026Contact()\n        }\n    }"

    .line 126
    .line 127
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇〇o〇:Landroidx/activity/result/ActivityResultLauncher;

    .line 131
    .line 132
    new-instance v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$mContactsAdapter$2;

    .line 133
    .line 134
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$mContactsAdapter$2;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 135
    .line 136
    .line 137
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    iput-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->Oo80:Lkotlin/Lazy;

    .line 142
    .line 143
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O00OoO〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->OO8〇O8()Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final O088O(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/util/Calendar;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p2, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent;

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80〇()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇00〇8(Z)V

    .line 13
    .line 14
    .line 15
    sget-object p2, Lcom/intsig/camscanner/newsign/share/ESignDatePickerDialog;->o〇00O:Lcom/intsig/camscanner/newsign/share/ESignDatePickerDialog$Companion;

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 18
    .line 19
    const-string v1, "mActivity"

    .line 20
    .line 21
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const-string v1, "calendar"

    .line 25
    .line 26
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    new-instance v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initTimePickerView$1$1;

    .line 30
    .line 31
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initTimePickerView$1$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p2, v0, p1, v1}, Lcom/intsig/camscanner/newsign/share/ESignDatePickerDialog$Companion;->〇080(Landroid/content/Context;Ljava/util/Calendar;Lcom/intsig/camscanner/newsign/share/ESignDatePickerDialog$OnTimeSelectDoneListener;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final O08〇oO8〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 7
    .line 8
    add-int/lit8 p1, p1, -0x1

    .line 9
    .line 10
    iput p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oo8()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O0o0(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/util/Calendar;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8〇0O〇(Ljava/util/Calendar;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->OO〇000(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O80OO(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O88O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O88(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O0O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O880O〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/Long;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o88o88(Ljava/lang/Long;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O8O(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8〇〇8o()Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇8O0O80〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OO0O(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8oo〇〇oO(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OO0o(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8oo0oO0(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final OO8〇O8()Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇〇08O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final OOo00(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o:Lcom/intsig/camscanner/newsign/util/CsStartLoginHelperAct;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    const-string v1, "mActivity"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initInviteView$9$1;

    .line 16
    .line 17
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initInviteView$9$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/newsign/util/CsStartLoginHelperAct;->〇o〇(Landroid/app/Activity;Lkotlin/jvm/functions/Function0;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final OO〇000(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getResultCode()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, -0x1

    .line 11
    if-ne v0, v1, :cond_2

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 14
    .line 15
    const-string v1, "login success"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    const-string v0, "EXTRA_SELECTED_CONTACTS"

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 p1, 0x0

    .line 34
    :goto_0
    if-nez p1, :cond_1

    .line 35
    .line 36
    new-instance p1, Ljava/util/ArrayList;

    .line 37
    .line 38
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .line 40
    .line 41
    :cond_1
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o0O:Ljava/util/ArrayList;

    .line 42
    .line 43
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇0888(Ljava/util/ArrayList;)V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->ooooo0O()V

    .line 47
    .line 48
    .line 49
    :cond_2
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic OO〇〇o0oO(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$imageAdapter$1;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇08〇o0O:Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$imageAdapter$1;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OoO〇OOo8o(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OooO〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8O〇008()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O〇00O(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->ooooo0O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oo88(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->OOo00(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O〇0o8o8〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇0888(Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇0o8〇()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->O88O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 9
    .line 10
    const-string v2, "binding.tvInviteByApp"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇8O0〇8()Z

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    if-eqz v2, :cond_1

    .line 20
    .line 21
    const v2, 0x7f13143d

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const v2, 0x7f1307eb

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    .line 38
    .line 39
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 40
    .line 41
    new-instance v2, Lo〇O8OO/〇80〇808〇O;

    .line 42
    .line 43
    invoke-direct {v2, p0}, Lo〇O8OO/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    .line 48
    .line 49
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 50
    .line 51
    new-instance v2, Lo〇O8OO/OO0o〇〇〇〇0;

    .line 52
    .line 53
    invoke-direct {v2, p0}, Lo〇O8OO/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    .line 58
    .line 59
    iget v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8oOOo:I

    .line 60
    .line 61
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o8〇8(I)V

    .line 62
    .line 63
    .line 64
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->oOo0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 65
    .line 66
    const-string v2, "initInviteView$lambda$5"

    .line 67
    .line 68
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    const/16 v2, 0xa

    .line 72
    .line 73
    invoke-static {v1, v2, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->Oo08(Landroid/view/View;II)V

    .line 74
    .line 75
    .line 76
    new-instance v3, Lo〇O8OO/〇8o8o〇;

    .line 77
    .line 78
    invoke-direct {v3, p0}, Lo〇O8OO/〇8o8o〇;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    .line 83
    .line 84
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 85
    .line 86
    const-string v3, "initInviteView$lambda$7"

    .line 87
    .line 88
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-static {v1, v2, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->Oo08(Landroid/view/View;II)V

    .line 92
    .line 93
    .line 94
    new-instance v2, Lo〇O8OO/〇O8o08O;

    .line 95
    .line 96
    invoke-direct {v2, p0}, Lo〇O8OO/〇O8o08O;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    .line 101
    .line 102
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oo8()V

    .line 103
    .line 104
    .line 105
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->ooo0〇〇O:Landroid/widget/LinearLayout;

    .line 106
    .line 107
    new-instance v2, Lo〇O8OO/OO0o〇〇;

    .line 108
    .line 109
    invoke-direct {v2, p0}, Lo〇O8OO/OO0o〇〇;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    .line 114
    .line 115
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->o8oOOo:Landroidx/recyclerview/widget/RecyclerView;

    .line 116
    .line 117
    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 118
    .line 119
    iget-object v3, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 120
    .line 121
    invoke-direct {v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 125
    .line 126
    .line 127
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oO〇O0O()Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$mContactsAdapter$2$1;

    .line 128
    .line 129
    .line 130
    move-result-object v2

    .line 131
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 132
    .line 133
    .line 134
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->ooooo0O()V

    .line 135
    .line 136
    .line 137
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 138
    .line 139
    new-instance v1, Lo〇O8OO/Oooo8o0〇;

    .line 140
    .line 141
    invoke-direct {v1, p0}, Lo〇O8OO/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    .line 146
    .line 147
    return-void
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O〇8()V
    .locals 9

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "clickConfirm"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8o0o0()Ljava/lang/Long;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-eqz v1, :cond_2

    .line 13
    .line 14
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 15
    .line 16
    .line 17
    move-result-wide v1

    .line 18
    sget-object v3, Lcom/intsig/camscanner/newsign/ESignHelper;->〇080:Lcom/intsig/camscanner/newsign/ESignHelper;

    .line 19
    .line 20
    invoke-virtual {v3}, Lcom/intsig/camscanner/newsign/ESignHelper;->OO0o〇〇〇〇0()J

    .line 21
    .line 22
    .line 23
    move-result-wide v3

    .line 24
    iget-wide v5, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O0O:J

    .line 25
    .line 26
    const/16 v7, 0x3e8

    .line 27
    .line 28
    int-to-long v7, v7

    .line 29
    mul-long v5, v5, v7

    .line 30
    .line 31
    invoke-static {v3, v4, v5, v6}, Lcom/intsig/utils/DateTimeUtil;->〇〇888(JJ)I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    iget v4, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8oOOo:I

    .line 36
    .line 37
    const/4 v5, 0x2

    .line 38
    if-ne v4, v5, :cond_0

    .line 39
    .line 40
    const-string v4, "clickConfirm share by app"

    .line 41
    .line 42
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    sget-object v0, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent;

    .line 46
    .line 47
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80〇()Z

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    iget v6, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 56
    .line 57
    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇0000OOO(ILjava/lang/Integer;ZI)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    const/4 v5, 0x1

    .line 62
    if-ne v4, v5, :cond_1

    .line 63
    .line 64
    const-string v4, "clickConfirm share by phone/email"

    .line 65
    .line 66
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    sget-object v0, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent;

    .line 70
    .line 71
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 72
    .line 73
    .line 74
    move-result-object v4

    .line 75
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80〇()Z

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    invoke-virtual {v0, v3, v4, v5}, Lcom/intsig/camscanner/newsign/ESignLogAgent;->o〇〇0〇(ILjava/lang/Integer;Z)V

    .line 80
    .line 81
    .line 82
    :cond_1
    :goto_0
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇〇〇OOO〇〇(Ljava/lang/Long;)V

    .line 87
    .line 88
    .line 89
    :cond_2
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O〇8O0O80〇(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const v2, 0x7f130420

    .line 5
    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    new-instance v6, Lo〇O8OO/〇O00;

    .line 9
    .line 10
    invoke-direct {v6, p0, p1}, Lo〇O8OO/〇O00;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v7, 0x0

    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8o0o0()Ljava/lang/Long;

    .line 15
    .line 16
    .line 17
    move-result-object v4

    .line 18
    if-eqz v4, :cond_0

    .line 19
    .line 20
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const-wide/16 v4, -0x1

    .line 26
    .line 27
    :goto_0
    move-wide v8, v4

    .line 28
    move-object v4, p1

    .line 29
    move-object v5, p2

    .line 30
    invoke-static/range {v0 .. v9}, Lcom/intsig/camscanner/app/DialogUtils;->o88〇OO08〇(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;Lcom/intsig/camscanner/app/DialogUtils$OnTemplateSettingsListener;J)V

    .line 31
    .line 32
    .line 33
    return-void
.end method

.method private final O〇O800oo()V
    .locals 10

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8o0o0()Ljava/lang/Long;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 8
    .line 9
    .line 10
    move-result-wide v3

    .line 11
    sget-object v0, Lcom/intsig/camscanner/newsign/ESignHelper;->〇080:Lcom/intsig/camscanner/newsign/ESignHelper;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/ESignHelper;->OO0o〇〇〇〇0()J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    iget-wide v5, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O0O:J

    .line 18
    .line 19
    const/16 v2, 0x3e8

    .line 20
    .line 21
    int-to-long v7, v2

    .line 22
    mul-long v5, v5, v7

    .line 23
    .line 24
    invoke-static {v0, v1, v5, v6}, Lcom/intsig/utils/DateTimeUtil;->〇〇888(JJ)I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    sget-object v1, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent;

    .line 29
    .line 30
    const/4 v2, 0x1

    .line 31
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80〇()Z

    .line 36
    .line 37
    .line 38
    move-result v5

    .line 39
    invoke-virtual {v1, v0, v2, v5}, Lcom/intsig/camscanner/newsign/ESignLogAgent;->o〇〇0〇(ILjava/lang/Integer;Z)V

    .line 40
    .line 41
    .line 42
    sget-object v1, Lcom/intsig/camscanner/newsign/contact/ESignContactListActivity;->ooo0〇〇O:Lcom/intsig/camscanner/newsign/contact/ESignContactListActivity$Companion;

    .line 43
    .line 44
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 45
    .line 46
    const-string v0, "mActivity"

    .line 47
    .line 48
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80〇()Z

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    iget-wide v6, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O0O:J

    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇o8()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v8

    .line 61
    iget-object v9, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O88O:Ljava/lang/String;

    .line 62
    .line 63
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/camscanner/newsign/contact/ESignContactListActivity$Companion;->〇080(Landroid/app/Activity;JZJLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    const-string v1, "EXTRA_PRE_SELECTED_CONTACTS"

    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o0O:Ljava/util/ArrayList;

    .line 70
    .line 71
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 72
    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oOO〇〇:Landroidx/activity/result/ActivityResultLauncher;

    .line 75
    .line 76
    invoke-virtual {v1, v0}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 77
    .line 78
    .line 79
    :cond_0
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O〇o8()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "EXTRA_KEY_ENTRANCE"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O〇oo8O80()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$subscribeUi$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$subscribeUi$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O08〇oO8〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O〇〇o8O()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final o088O8800(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8oOOo:I

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o8〇8(I)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O88O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o0OO(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o0o8(Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic o0Oo(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o0〇〇00〇o(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o0〇〇00〇o(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o:Lcom/intsig/camscanner/newsign/util/CsStartLoginHelperAct;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    const-string v1, "mActivity"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initInviteView$7$1;

    .line 16
    .line 17
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initInviteView$7$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/newsign/util/CsStartLoginHelperAct;->〇o〇(Landroid/app/Activity;Lkotlin/jvm/functions/Function0;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o088O8800(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o88o88(Ljava/lang/Long;)V
    .locals 9

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "checkDocSync docId == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 26
    .line 27
    .line 28
    new-instance v0, Lcom/intsig/camscanner/scenariodir/DocManualOperations$CheckDocSyncHelper;

    .line 29
    .line 30
    iget-object v3, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 31
    .line 32
    const-string v1, "mActivity"

    .line 33
    .line 34
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 38
    .line 39
    .line 40
    move-result-wide v4

    .line 41
    new-instance v6, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$checkDocSync$1;

    .line 42
    .line 43
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$checkDocSync$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 44
    .line 45
    .line 46
    new-instance v7, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$checkDocSync$2;

    .line 47
    .line 48
    invoke-direct {v7, p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$checkDocSync$2;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/Long;)V

    .line 49
    .line 50
    .line 51
    const/4 v8, 0x1

    .line 52
    move-object v2, v0

    .line 53
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/scenariodir/DocManualOperations$CheckDocSyncHelper;-><init>(Landroidx/fragment/app/FragmentActivity;JLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Z)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/DocManualOperations$CheckDocSyncHelper;->Oooo8o0〇()V

    .line 57
    .line 58
    .line 59
    :cond_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o88oo〇O()V
    .locals 17

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 4
    .line 5
    .line 6
    move-result-object v8

    .line 7
    if-nez v8, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8o0o0()Ljava/lang/Long;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 17
    .line 18
    .line 19
    move-result-wide v9

    .line 20
    iget-object v1, v8, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 21
    .line 22
    const-string v0, "binding.tvDocName"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v3, v8, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 28
    .line 29
    const-string v0, "binding.ivModifyName"

    .line 30
    .line 31
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-static/range {p0 .. p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 35
    .line 36
    .line 37
    move-result-object v11

    .line 38
    const/4 v12, 0x0

    .line 39
    const/4 v13, 0x0

    .line 40
    new-instance v14, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initDocInfo$1;

    .line 41
    .line 42
    const/4 v6, 0x0

    .line 43
    move-object v0, v14

    .line 44
    move-object/from16 v2, p0

    .line 45
    .line 46
    move-wide v4, v9

    .line 47
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initDocInfo$1;-><init>(Landroidx/appcompat/widget/AppCompatTextView;Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroidx/appcompat/widget/AppCompatImageView;JLkotlin/coroutines/Continuation;)V

    .line 48
    .line 49
    .line 50
    const/4 v15, 0x3

    .line 51
    const/16 v16, 0x0

    .line 52
    .line 53
    invoke-static/range {v11 .. v16}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 54
    .line 55
    .line 56
    iget-object v0, v8, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->O0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 57
    .line 58
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 59
    .line 60
    iget-object v2, v7, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 61
    .line 62
    const/4 v3, 0x0

    .line 63
    invoke-direct {v1, v2, v3, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 67
    .line 68
    .line 69
    iget-object v1, v7, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇08〇o0O:Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$imageAdapter$1;

    .line 70
    .line 71
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 72
    .line 73
    .line 74
    invoke-static/range {p0 .. p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 75
    .line 76
    .line 77
    move-result-object v11

    .line 78
    new-instance v14, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initDocInfo$3;

    .line 79
    .line 80
    const/4 v0, 0x0

    .line 81
    invoke-direct {v14, v7, v9, v10, v0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$initDocInfo$3;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;JLkotlin/coroutines/Continuation;)V

    .line 82
    .line 83
    .line 84
    invoke-static/range {v11 .. v16}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 85
    .line 86
    .line 87
    :cond_1
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o8O〇008()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "click add signer"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Landroid/content/Intent;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 11
    .line 12
    const-class v2, Lcom/intsig/camscanner/newsign/contact/AddContactActivity;

    .line 13
    .line 14
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇〇o〇:Landroidx/activity/result/ActivityResultLauncher;

    .line 18
    .line 19
    invoke-virtual {v1, v0}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o8o0(Landroid/widget/ImageView;Z)V
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 4
    .line 5
    const v0, 0x7f060207

    .line 6
    .line 7
    .line 8
    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 13
    .line 14
    .line 15
    move-result-object p2

    .line 16
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object p2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 21
    .line 22
    const v0, 0x7f060205

    .line 23
    .line 24
    .line 25
    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 26
    .line 27
    .line 28
    move-result p2

    .line 29
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 30
    .line 31
    .line 32
    move-result-object p2

    .line 33
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 34
    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final o8o0o8(Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x3

    .line 8
    const/4 v6, 0x3

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x2

    .line 11
    const/4 v6, 0x2

    .line 12
    :goto_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 15
    .line 16
    .line 17
    sget-object v1, Lcom/intsig/camscanner/newsign/done/SignDoneActivity;->O0O:Lcom/intsig/camscanner/newsign/done/SignDoneActivity$Companion;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 20
    .line 21
    const-string v0, "mActivity"

    .line 22
    .line 23
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v4, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O88O:Ljava/lang/String;

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇o8()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v7

    .line 32
    move-object v3, p1

    .line 33
    move-object v5, p2

    .line 34
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/camscanner/newsign/done/SignDoneActivity$Companion;->startActivity(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;ILjava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final oO8(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x2

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8oOOo:I

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o8〇8(I)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oOO8oo0(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇O800oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oO〇O0O()Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$mContactsAdapter$2$1;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->Oo80:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$mContactsAdapter$2$1;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oo8()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->oOo0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 9
    .line 10
    const-string v2, "binding.ivNumMinus"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 16
    .line 17
    const-string v3, "binding.ivNumPlus"

    .line 18
    .line 19
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget v3, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 23
    .line 24
    const/4 v4, 0x0

    .line 25
    const/4 v5, 0x1

    .line 26
    if-gt v3, v5, :cond_1

    .line 27
    .line 28
    iput v5, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 29
    .line 30
    sget-object v3, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 31
    .line 32
    const-string v6, "select reach min 1"

    .line 33
    .line 34
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0, v1, v4}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o0(Landroid/widget/ImageView;Z)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0, v2, v5}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o0(Landroid/widget/ImageView;Z)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    const/4 v6, 0x5

    .line 45
    if-lt v3, v6, :cond_2

    .line 46
    .line 47
    iput v6, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 48
    .line 49
    sget-object v3, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 50
    .line 51
    const-string v6, "select reach limit 5"

    .line 52
    .line 53
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    iget-object v3, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 57
    .line 58
    const v6, 0x7f1313fa

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v6

    .line 65
    invoke-static {v3, v6}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0, v2, v4}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o0(Landroid/widget/ImageView;Z)V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0, v1, v5}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o0(Landroid/widget/ImageView;Z)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    invoke-direct {p0, v2, v5}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o0(Landroid/widget/ImageView;Z)V

    .line 76
    .line 77
    .line 78
    invoke-direct {p0, v1, v5}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8o0(Landroid/widget/ImageView;Z)V

    .line 79
    .line 80
    .line 81
    :goto_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->o8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 82
    .line 83
    iget v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 84
    .line 85
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final oo88(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    const-string v5, "esign"

    .line 12
    .line 13
    new-instance v6, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$showRenameDlg$1$1;

    .line 14
    .line 15
    invoke-direct {v6, p0, p2}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$showRenameDlg$1$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    new-instance v7, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$showRenameDlg$1$2;

    .line 19
    .line 20
    invoke-direct {v7, p2, p1, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$showRenameDlg$1$2;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 21
    .line 22
    .line 23
    move-object v4, p2

    .line 24
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/mainmenu/SensitiveWordsChecker;->〇080(Ljava/lang/Boolean;Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oo8〇〇(Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel$Action$GenerateLinkAction;)V
    .locals 9

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel$Action$GenerateLinkAction;->〇o00〇〇Oo()Lcom/intsig/utils/CsResult;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    new-instance v2, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v3, "onCreateShareLinkAction result == "

    .line 13
    .line 14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel$Action$GenerateLinkAction;->〇o00〇〇Oo()Lcom/intsig/utils/CsResult;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    const/4 v3, 0x0

    .line 32
    new-instance v4, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$onCreateShareLinkAction$1;

    .line 33
    .line 34
    invoke-direct {v4, p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$onCreateShareLinkAction$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel$Action$GenerateLinkAction;)V

    .line 35
    .line 36
    .line 37
    new-instance v5, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$onCreateShareLinkAction$2;

    .line 38
    .line 39
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$onCreateShareLinkAction$2;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 40
    .line 41
    .line 42
    new-instance v6, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$onCreateShareLinkAction$3;

    .line 43
    .line 44
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$onCreateShareLinkAction$3;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V

    .line 45
    .line 46
    .line 47
    const/4 v7, 0x1

    .line 48
    const/4 v8, 0x0

    .line 49
    invoke-static/range {v2 .. v8}, Lcom/intsig/utils/CsResultKt;->〇o00〇〇Oo(Lcom/intsig/utils/CsResult;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final oooO8〇00()V
    .locals 9

    .line 1
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 9
    .line 10
    const-string v2, "selectOrAppendContact"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/newsign/contact/repo/ESignContactRepository;

    .line 16
    .line 17
    invoke-direct {v1}, Lcom/intsig/camscanner/newsign/contact/repo/ESignContactRepository;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8〇〇8o()Lcom/intsig/app/BaseProgressDialog;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v2}, Lcom/intsig/app/AlertDialog;->show()V

    .line 25
    .line 26
    .line 27
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    const/4 v4, 0x0

    .line 32
    const/4 v5, 0x0

    .line 33
    new-instance v6, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$selectOrAppendContact$1;

    .line 34
    .line 35
    const/4 v2, 0x0

    .line 36
    invoke-direct {v6, v1, v0, p0, v2}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$selectOrAppendContact$1;-><init>(Lcom/intsig/camscanner/newsign/contact/repo/ESignContactRepository;Ljava/lang/String;Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Lkotlin/coroutines/Continuation;)V

    .line 37
    .line 38
    .line 39
    const/4 v7, 0x3

    .line 40
    const/4 v8, 0x0

    .line 41
    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final ooooo0O()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8oOOo:I

    .line 9
    .line 10
    const/4 v2, 0x2

    .line 11
    const/4 v3, 0x0

    .line 12
    const/4 v4, 0x1

    .line 13
    if-eq v1, v2, :cond_2

    .line 14
    .line 15
    if-ne v1, v4, :cond_1

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o0O:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    xor-int/2addr v1, v4

    .line 24
    if-eqz v1, :cond_1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    const/4 v1, 0x0

    .line 28
    goto :goto_1

    .line 29
    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 30
    :goto_1
    if-eqz v1, :cond_3

    .line 31
    .line 32
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 33
    .line 34
    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 35
    .line 36
    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 38
    .line 39
    const/high16 v1, 0x3f800000    # 1.0f

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 42
    .line 43
    .line 44
    goto :goto_2

    .line 45
    :cond_3
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 46
    .line 47
    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 48
    .line 49
    .line 50
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 51
    .line 52
    const v1, 0x3e99999a    # 0.3f

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 56
    .line 57
    .line 58
    :goto_2
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o〇08oO80o(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oO8(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇OoO0(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel$Action$GenerateLinkAction;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oo8〇〇(Lcom/intsig/camscanner/newsign/share/ShareByAppViewModel$Action$GenerateLinkAction;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇o08〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8oo8888(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇o0oOO8()V
    .locals 1

    .line 1
    const v0, 0x7f131434

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o88oo〇O()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇00o〇O8()V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇0o8〇()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method private final 〇00o〇O8()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    sget-object v2, Lcom/intsig/camscanner/newsign/ESignHelper;->〇080:Lcom/intsig/camscanner/newsign/ESignHelper;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/camscanner/newsign/ESignHelper;->OO0o〇〇〇〇0()J

    .line 15
    .line 16
    .line 17
    move-result-wide v2

    .line 18
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 19
    .line 20
    .line 21
    const/4 v2, 0x2

    .line 22
    const/4 v3, 0x3

    .line 23
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 24
    .line 25
    .line 26
    const/16 v2, 0xb

    .line 27
    .line 28
    const/16 v3, 0x17

    .line 29
    .line 30
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 31
    .line 32
    .line 33
    const/16 v2, 0xc

    .line 34
    .line 35
    const/16 v3, 0x3b

    .line 36
    .line 37
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 38
    .line 39
    .line 40
    const/16 v2, 0xd

    .line 41
    .line 42
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 43
    .line 44
    .line 45
    const/16 v2, 0xe

    .line 46
    .line 47
    const/16 v3, 0x3e7

    .line 48
    .line 49
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 50
    .line 51
    .line 52
    const-string v2, "calendar"

    .line 53
    .line 54
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8〇0O〇(Ljava/util/Calendar;)V

    .line 58
    .line 59
    .line 60
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 61
    .line 62
    const-string v3, "binding.tvDate"

    .line 63
    .line 64
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 68
    .line 69
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    const/16 v4, 0xf

    .line 74
    .line 75
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    const/4 v4, 0x0

    .line 80
    invoke-static {v2, v4, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->Oo08(Landroid/view/View;II)V

    .line 81
    .line 82
    .line 83
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 84
    .line 85
    new-instance v2, Lo〇O8OO/〇〇808〇;

    .line 86
    .line 87
    invoke-direct {v2, p0, v1}, Lo〇O8OO/〇〇808〇;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/util/Calendar;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    .line 92
    .line 93
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0888(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/newsign/data/ESignContact;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    new-instance v2, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v3, "showContactsListView size == "

    .line 13
    .line 14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oO〇O0O()Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$mContactsAdapter$2$1;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇0O8Oo()Ljava/text/SimpleDateFormat;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o〇oO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oooO8〇00()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o8oOOo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇80O(Ljava/lang/String;Ljava/lang/Long;I)V
    .locals 10

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    if-nez v0, :cond_3

    .line 14
    .line 15
    if-nez p2, :cond_2

    .line 16
    .line 17
    goto :goto_2

    .line 18
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication;->oo〇()Lkotlinx/coroutines/CoroutineScope;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$saveDocTitleToDbAsync$1;

    .line 32
    .line 33
    const/4 v9, 0x0

    .line 34
    move-object v4, v0

    .line 35
    move-object v5, p2

    .line 36
    move-object v6, p1

    .line 37
    move-object v7, p0

    .line 38
    move v8, p3

    .line 39
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$saveDocTitleToDbAsync$1;-><init>(Ljava/lang/Long;Ljava/lang/String;Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;ILkotlin/coroutines/Continuation;)V

    .line 40
    .line 41
    .line 42
    const/4 v5, 0x2

    .line 43
    const/4 v6, 0x0

    .line 44
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 45
    .line 46
    .line 47
    :cond_3
    :goto_2
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇80〇()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const-string v2, "EXTRA_KEY_INCLUDE_SELF"

    .line 9
    .line 10
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    :cond_0
    return v1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇8o0o0()Ljava/lang/Long;
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v1, "EXTRA_KEY_DOC_ID"

    .line 8
    .line 9
    const-wide/16 v2, -0x1

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    return-object v0
.end method

.method private final 〇8oo0oO0(Ljava/lang/String;I)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/newsign/ESignLogAgent$CsSignatureInviteLogAgent;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent$CsSignatureInviteLogAgent;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80〇()Z

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/newsign/ESignLogAgent$CsSignatureInviteLogAgent;->〇o00〇〇Oo(Z)V

    .line 15
    .line 16
    .line 17
    invoke-static {p1}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const/4 v1, 0x1

    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    const/4 v2, 0x0

    .line 32
    goto :goto_1

    .line 33
    :cond_2
    :goto_0
    const/4 v2, 0x1

    .line 34
    :goto_1
    if-nez v2, :cond_3

    .line 35
    .line 36
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 37
    .line 38
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    .line 40
    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O88O:Ljava/lang/String;

    .line 42
    .line 43
    sget-object v0, Lcom/intsig/camscanner/newsign/data/dao/ESignDbDao;->〇080:Lcom/intsig/camscanner/newsign/data/dao/ESignDbDao;

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8o0o0()Ljava/lang/Long;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/newsign/data/dao/ESignDbDao;->Oooo8o0〇(Ljava/lang/Long;)Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    xor-int/2addr v0, v1

    .line 54
    sget-object v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 55
    .line 56
    new-instance v2, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    const-string v3, "canSaveTitleToDb == "

    .line 62
    .line 63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    if-eqz v0, :cond_3

    .line 77
    .line 78
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8o0o0()Ljava/lang/Long;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    invoke-direct {p0, p1, v0, p2}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80O(Ljava/lang/String;Ljava/lang/Long;I)V

    .line 83
    .line 84
    .line 85
    :cond_3
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇8oo8888(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 7
    .line 8
    add-int/lit8 p1, p1, 0x1

    .line 9
    .line 10
    iput p1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oo8()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇8oo〇〇oO(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getResultCode()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    const/4 v0, -0x1

    .line 11
    if-ne p1, v0, :cond_0

    .line 12
    .line 13
    sget-object p1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 14
    .line 15
    const-string v0, "add contact success"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇O800oo()V

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8〇0O〇(Ljava/util/Calendar;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    .line 13
    .line 14
    .line 15
    move-result-wide v2

    .line 16
    const/16 p1, 0x3e8

    .line 17
    .line 18
    int-to-long v4, p1

    .line 19
    div-long/2addr v2, v4

    .line 20
    iput-wide v2, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O0O:J

    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇0O8Oo()Ljava/text/SimpleDateFormat;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {p1, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 31
    .line 32
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    .line 34
    .line 35
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 36
    .line 37
    iget-wide v1, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O0O:J

    .line 38
    .line 39
    new-instance v3, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v4, "mExpireTimeSeconds == "

    .line 45
    .line 46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    const-string v1, " , timeString == "

    .line 53
    .line 54
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    return-void
.end method

.method private final 〇8〇〇8o()Lcom/intsig/app/BaseProgressDialog;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->oo8ooo8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/app/BaseProgressDialog;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇OoO0o0(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)Ljava/lang/Long;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8o0o0()Ljava/lang/Long;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇o8〇8(I)V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v1, 0x2

    .line 9
    const-string v2, "binding.llAddSigner"

    .line 10
    .line 11
    const/16 v3, 0x8

    .line 12
    .line 13
    const-string v4, "binding.groupInvite"

    .line 14
    .line 15
    const/4 v5, 0x1

    .line 16
    const/4 v6, 0x0

    .line 17
    if-ne p1, v1, :cond_1

    .line 18
    .line 19
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->O8o08O8O:Landroidx/constraintlayout/widget/Group;

    .line 20
    .line 21
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->O88O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 28
    .line 29
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 30
    .line 31
    .line 32
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 33
    .line 34
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 35
    .line 36
    .line 37
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->oOO〇〇:Landroidx/appcompat/widget/AppCompatTextView;

    .line 38
    .line 39
    invoke-virtual {p1, v6}, Landroid/view/View;->setSelected(Z)V

    .line 40
    .line 41
    .line 42
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 43
    .line 44
    invoke-virtual {p1, v6}, Landroid/view/View;->setSelected(Z)V

    .line 45
    .line 46
    .line 47
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->ooo0〇〇O:Landroid/widget/LinearLayout;

    .line 48
    .line 49
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->O8o08O8O:Landroidx/constraintlayout/widget/Group;

    .line 57
    .line 58
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 62
    .line 63
    .line 64
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->O88O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 65
    .line 66
    invoke-virtual {p1, v6}, Landroid/view/View;->setSelected(Z)V

    .line 67
    .line 68
    .line 69
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 70
    .line 71
    invoke-virtual {p1, v6}, Landroid/view/View;->setSelected(Z)V

    .line 72
    .line 73
    .line 74
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->oOO〇〇:Landroidx/appcompat/widget/AppCompatTextView;

    .line 75
    .line 76
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 77
    .line 78
    .line 79
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 80
    .line 81
    invoke-virtual {p1, v5}, Landroid/view/View;->setSelected(Z)V

    .line 82
    .line 83
    .line 84
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;->ooo0〇〇O:Landroid/widget/LinearLayout;

    .line 85
    .line 86
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 90
    .line 91
    .line 92
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->ooooo0O()V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/util/Calendar;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O088O(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/util/Calendar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇oOO80o(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇80〇()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇O〇〇O8:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇ooO〇000(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇o〇88()Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇00O0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityShareSignToOtherBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇OO80oO(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇〇OOO〇〇(Ljava/lang/Long;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    const-wide/16 v2, 0x0

    .line 8
    .line 9
    cmp-long v4, v0, v2

    .line 10
    .line 11
    if-gtz v4, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/advertisement/util/NetworkUtil;->〇080(Landroid/content/Context;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    sget-object p1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 23
    .line 24
    const-string v0, "checkCanCreateLink no network"

    .line 25
    .line 26
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 30
    .line 31
    const v0, 0x7f13008d

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-void

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 43
    .line 44
    const-string v1, "mActivity"

    .line 45
    .line 46
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    new-instance v1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$checkCanCreateLink$1;

    .line 50
    .line 51
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$checkCanCreateLink$1;-><init>(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;Ljava/lang/Long;)V

    .line 52
    .line 53
    .line 54
    const/4 p1, 0x1

    .line 55
    const-string v2, "other"

    .line 56
    .line 57
    invoke-static {v0, v1, p1, v2, v2}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/ipo/IPOCheckCallback;ZLjava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_2
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 62
    .line 63
    new-instance v1, Ljava/lang/StringBuilder;

    .line 64
    .line 65
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .line 67
    .line 68
    const-string v2, "checkCanCreateLink docId illegal: "

    .line 69
    .line 70
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->〇8o0o0()Ljava/lang/Long;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_1

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    const-wide/16 v2, 0x0

    .line 12
    .line 13
    cmp-long v4, v0, v2

    .line 14
    .line 15
    if-gtz v4, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->Oo08(Landroidx/lifecycle/LifecycleOwner;)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->o〇o0oOO8()V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇oo8O80()V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_1
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 29
    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v2, "illegal docId == "

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final onReceiveFinishCurrentActivityEvent(Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$PageFinishEvent;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$PageFinishEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "event"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "onReceiveFinishCurrentActivityEvent"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onResume()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const-string v1, "logAgentFromPart"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const-string v1, ""

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    move-object v0, v1

    .line 19
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    const-string v3, "logAgentType"

    .line 24
    .line 25
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    if-nez v2, :cond_1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    move-object v1, v2

    .line 33
    :goto_0
    sget-object v2, Lcom/intsig/camscanner/newsign/ESignLogAgent;->〇080:Lcom/intsig/camscanner/newsign/ESignLogAgent;

    .line 34
    .line 35
    invoke-virtual {v2, v0, v1}, Lcom/intsig/camscanner/newsign/ESignLogAgent;->O〇O〇oO(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;
.super Landroid/view/View;
.source "RiseTextView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$QuintOutInterpolator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8o08O8O:Landroid/animation/ValueAnimator;

.field private final OO:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Landroid/view/animation/Interpolator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:I

.field private o8〇OO0〇0o:I

.field private oOo0:J

.field private oOo〇8o008:Ljava/lang/CharSequence;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:Landroid/graphics/Rect;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:I

.field private 〇8〇oO〇〇8o:I

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 3
    new-instance v1, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    invoke-direct {v1}, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇08O〇00〇o:Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    .line 4
    new-instance v2, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    invoke-direct {v2, v0, v1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;-><init>(Landroid/graphics/Paint;Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;)V

    iput-object v2, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    const/4 v0, 0x1

    new-array v0, v0, [F

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 5
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->O8o08O8O:Landroid/animation/ValueAnimator;

    .line 6
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    const v0, 0x800005

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇0O:I

    const-string v0, ""

    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo〇8o008:Ljava/lang/CharSequence;

    const-wide/16 v0, 0x2ee

    .line 9
    iput-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo0:J

    .line 10
    new-instance v0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$QuintOutInterpolator;

    invoke-direct {v0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$QuintOutInterpolator;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO〇00〇8oO:Landroid/view/animation/Interpolator;

    const/high16 v0, -0x1000000

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8〇oO〇〇8o:I

    const/4 v0, 0x0

    .line 12
    invoke-direct {p0, p1, v0, v2, v2}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oO80(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 15
    new-instance v1, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    invoke-direct {v1}, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇08O〇00〇o:Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    .line 16
    new-instance v2, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    invoke-direct {v2, v0, v1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;-><init>(Landroid/graphics/Paint;Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;)V

    iput-object v2, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    const/4 v0, 0x1

    new-array v0, v0, [F

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 17
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->O8o08O8O:Landroid/animation/ValueAnimator;

    .line 18
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    const v0, 0x800005

    .line 19
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇0O:I

    const-string v0, ""

    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo〇8o008:Ljava/lang/CharSequence;

    const-wide/16 v0, 0x2ee

    .line 21
    iput-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo0:J

    .line 22
    new-instance v0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$QuintOutInterpolator;

    invoke-direct {v0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$QuintOutInterpolator;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO〇00〇8oO:Landroid/view/animation/Interpolator;

    const/high16 v0, -0x1000000

    .line 23
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8〇oO〇〇8o:I

    .line 24
    invoke-direct {p0, p1, p2, v2, v2}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oO80(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 27
    new-instance v1, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    invoke-direct {v1}, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇08O〇00〇o:Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    .line 28
    new-instance v2, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    invoke-direct {v2, v0, v1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;-><init>(Landroid/graphics/Paint;Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;)V

    iput-object v2, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    const/4 v0, 0x1

    new-array v0, v0, [F

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 29
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->O8o08O8O:Landroid/animation/ValueAnimator;

    .line 30
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    const v0, 0x800005

    .line 31
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇0O:I

    const-string v0, ""

    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo〇8o008:Ljava/lang/CharSequence;

    const-wide/16 v0, 0x2ee

    .line 33
    iput-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo0:J

    .line 34
    new-instance v0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$QuintOutInterpolator;

    invoke-direct {v0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$QuintOutInterpolator;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO〇00〇8oO:Landroid/view/animation/Interpolator;

    const/high16 v0, -0x1000000

    .line 35
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8〇oO〇〇8o:I

    .line 36
    invoke-direct {p0, p1, p2, p3, v2}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oO80(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private static final OO0o〇〇〇〇0(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 12
    .line 13
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->〇O8o08O(F)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->Oo08()Z

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Oo08()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oO80(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 17

    .line 1
    move-object/from16 v8, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    new-instance v9, Lkotlin/jvm/internal/Ref$IntRef;

    .line 6
    .line 7
    invoke-direct {v9}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    .line 8
    .line 9
    .line 10
    new-instance v10, Lkotlin/jvm/internal/Ref$FloatRef;

    .line 11
    .line 12
    invoke-direct {v10}, Lkotlin/jvm/internal/Ref$FloatRef;-><init>()V

    .line 13
    .line 14
    .line 15
    new-instance v11, Lkotlin/jvm/internal/Ref$FloatRef;

    .line 16
    .line 17
    invoke-direct {v11}, Lkotlin/jvm/internal/Ref$FloatRef;-><init>()V

    .line 18
    .line 19
    .line 20
    new-instance v12, Lkotlin/jvm/internal/Ref$FloatRef;

    .line 21
    .line 22
    invoke-direct {v12}, Lkotlin/jvm/internal/Ref$FloatRef;-><init>()V

    .line 23
    .line 24
    .line 25
    new-instance v13, Lkotlin/jvm/internal/Ref$ObjectRef;

    .line 26
    .line 27
    invoke-direct {v13}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v1, ""

    .line 31
    .line 32
    iput-object v1, v13, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 33
    .line 34
    new-instance v14, Lkotlin/jvm/internal/Ref$FloatRef;

    .line 35
    .line 36
    invoke-direct {v14}, Lkotlin/jvm/internal/Ref$FloatRef;-><init>()V

    .line 37
    .line 38
    .line 39
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    const/4 v2, 0x2

    .line 48
    const/high16 v3, 0x41400000    # 12.0f

    .line 49
    .line 50
    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    iput v1, v14, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 55
    .line 56
    sget-object v1, Lcom/intsig/camscanner/R$styleable;->RiseTextView:[I

    .line 57
    .line 58
    move-object/from16 v2, p2

    .line 59
    .line 60
    move/from16 v3, p3

    .line 61
    .line 62
    move/from16 v4, p4

    .line 63
    .line 64
    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    .line 65
    .line 66
    .line 67
    move-result-object v15

    .line 68
    const-string v2, "context.obtainStyledAttr\u2026efStyleAttr, defStyleRes)"

    .line 69
    .line 70
    invoke-static {v15, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    const/4 v7, 0x0

    .line 74
    const/4 v2, -0x1

    .line 75
    invoke-virtual {v15, v7, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    if-eq v3, v2, :cond_0

    .line 80
    .line 81
    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    .line 82
    .line 83
    .line 84
    move-result-object v6

    .line 85
    const-string v0, "context.obtainStyledAttr\u2026R.styleable.RiseTextView)"

    .line 86
    .line 87
    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    move-object/from16 v0, p0

    .line 91
    .line 92
    move-object v1, v9

    .line 93
    move-object v2, v10

    .line 94
    move-object v3, v11

    .line 95
    move-object v4, v12

    .line 96
    move-object v5, v13

    .line 97
    move-object/from16 v16, v6

    .line 98
    .line 99
    move-object v6, v14

    .line 100
    move-object/from16 v7, v16

    .line 101
    .line 102
    invoke-static/range {v0 .. v7}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇80〇808〇O(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$FloatRef;Landroid/content/res/TypedArray;)V

    .line 103
    .line 104
    .line 105
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/TypedArray;->recycle()V

    .line 106
    .line 107
    .line 108
    :cond_0
    move-object/from16 v0, p0

    .line 109
    .line 110
    move-object v1, v9

    .line 111
    move-object v2, v10

    .line 112
    move-object v3, v11

    .line 113
    move-object v4, v12

    .line 114
    move-object v5, v13

    .line 115
    move-object v6, v14

    .line 116
    move-object v7, v15

    .line 117
    invoke-static/range {v0 .. v7}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇80〇808〇O(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$FloatRef;Landroid/content/res/TypedArray;)V

    .line 118
    .line 119
    .line 120
    iget-wide v0, v8, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo0:J

    .line 121
    .line 122
    long-to-int v1, v0

    .line 123
    const/16 v0, 0xa

    .line 124
    .line 125
    invoke-virtual {v15, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    .line 126
    .line 127
    .line 128
    move-result v0

    .line 129
    int-to-long v0, v0

    .line 130
    iput-wide v0, v8, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo0:J

    .line 131
    .line 132
    iget-object v0, v8, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 133
    .line 134
    const/4 v1, 0x1

    .line 135
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 136
    .line 137
    .line 138
    iget v0, v9, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 139
    .line 140
    if-eqz v0, :cond_1

    .line 141
    .line 142
    iget-object v1, v8, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 143
    .line 144
    iget v2, v12, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 145
    .line 146
    iget v3, v10, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 147
    .line 148
    iget v4, v11, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 149
    .line 150
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 151
    .line 152
    .line 153
    :cond_1
    iget v0, v8, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o8〇OO0〇0o:I

    .line 154
    .line 155
    if-eqz v0, :cond_2

    .line 156
    .line 157
    iget-object v0, v8, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 158
    .line 159
    invoke-virtual {v0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    invoke-virtual {v8, v0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 164
    .line 165
    .line 166
    :cond_2
    iget v0, v14, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 167
    .line 168
    const/4 v1, 0x0

    .line 169
    invoke-virtual {v8, v1, v0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->Oooo8o0〇(IF)V

    .line 170
    .line 171
    .line 172
    iget-object v0, v13, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 173
    .line 174
    check-cast v0, Ljava/lang/CharSequence;

    .line 175
    .line 176
    invoke-virtual {v8, v0, v1}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO0o〇〇(Ljava/lang/CharSequence;Z)V

    .line 177
    .line 178
    .line 179
    invoke-virtual {v15}, Landroid/content/res/TypedArray;->recycle()V

    .line 180
    .line 181
    .line 182
    iget-object v0, v8, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->O8o08O8O:Landroid/animation/ValueAnimator;

    .line 183
    .line 184
    new-instance v1, L〇o〇OO80oO/〇o〇;

    .line 185
    .line 186
    invoke-direct {v1, v8}, L〇o〇OO80oO/〇o〇;-><init>(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;)V

    .line 187
    .line 188
    .line 189
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 190
    .line 191
    .line 192
    iget-object v0, v8, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->O8o08O8O:Landroid/animation/ValueAnimator;

    .line 193
    .line 194
    new-instance v1, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$init$2;

    .line 195
    .line 196
    invoke-direct {v1, v8}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$init$2;-><init>(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 200
    .line 201
    .line 202
    return-void
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method private final o〇0()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->〇〇888()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    float-to-int v0, v0

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    add-int/2addr v0, v1

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    add-int/2addr v0, v1

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method private static final setText$lambda$2$lambda$1(Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->start()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080(Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->setText$lambda$2$lambda$1(Landroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇80〇808〇O(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$FloatRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$FloatRef;Landroid/content/res/TypedArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;",
            "Lkotlin/jvm/internal/Ref$IntRef;",
            "Lkotlin/jvm/internal/Ref$FloatRef;",
            "Lkotlin/jvm/internal/Ref$FloatRef;",
            "Lkotlin/jvm/internal/Ref$FloatRef;",
            "Lkotlin/jvm/internal/Ref$ObjectRef<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/internal/Ref$FloatRef;",
            "Landroid/content/res/TypedArray;",
            ")V"
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇0O:I

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    invoke-virtual {p7, v1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇0O:I

    .line 9
    .line 10
    iget v0, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 11
    .line 12
    const/4 v1, 0x6

    .line 13
    invoke-virtual {p7, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iput v0, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 18
    .line 19
    iget p1, p2, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 20
    .line 21
    const/4 v0, 0x7

    .line 22
    invoke-virtual {p7, v0, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    iput p1, p2, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 27
    .line 28
    iget p1, p3, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 29
    .line 30
    const/16 p2, 0x8

    .line 31
    .line 32
    invoke-virtual {p7, p2, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    iput p1, p3, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 37
    .line 38
    iget p1, p4, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 39
    .line 40
    const/16 p2, 0x9

    .line 41
    .line 42
    invoke-virtual {p7, p2, p1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    iput p1, p4, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 47
    .line 48
    const/4 p1, 0x5

    .line 49
    invoke-virtual {p7, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    if-nez p1, :cond_0

    .line 54
    .line 55
    const-string p1, ""

    .line 56
    .line 57
    :cond_0
    iput-object p1, p5, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 58
    .line 59
    const/4 p1, 0x3

    .line 60
    iget p2, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8〇oO〇〇8o:I

    .line 61
    .line 62
    invoke-virtual {p7, p1, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->setTextColor(I)V

    .line 67
    .line 68
    .line 69
    iget p1, p6, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 70
    .line 71
    const/4 p2, 0x1

    .line 72
    invoke-virtual {p7, p2, p1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    iput p1, p6, Lkotlin/jvm/internal/Ref$FloatRef;->element:F

    .line 77
    .line 78
    const/4 p1, 0x2

    .line 79
    iget p2, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o8〇OO0〇0o:I

    .line 80
    .line 81
    invoke-virtual {p7, p1, p2}, Landroid/content/res/TypedArray;->getInt(II)I

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->setTextStyle(I)V

    .line 86
    .line 87
    .line 88
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
.end method

.method private final 〇8o8o〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->OO0o〇〇()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->Oo08()Z

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇O8o08O(Landroid/graphics/Canvas;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->O8()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->〇〇888()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 14
    .line 15
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 20
    .line 21
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    iget v4, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇0O:I

    .line 26
    .line 27
    and-int/lit8 v5, v4, 0x10

    .line 28
    .line 29
    const/high16 v6, 0x40000000    # 2.0f

    .line 30
    .line 31
    const/4 v7, 0x0

    .line 32
    const/16 v8, 0x10

    .line 33
    .line 34
    if-ne v5, v8, :cond_0

    .line 35
    .line 36
    iget-object v5, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 37
    .line 38
    iget v5, v5, Landroid/graphics/Rect;->top:I

    .line 39
    .line 40
    int-to-float v5, v5

    .line 41
    int-to-float v8, v3

    .line 42
    sub-float/2addr v8, v1

    .line 43
    div-float/2addr v8, v6

    .line 44
    add-float/2addr v5, v8

    .line 45
    goto :goto_0

    .line 46
    :cond_0
    const/4 v5, 0x0

    .line 47
    :goto_0
    and-int/lit8 v8, v4, 0x1

    .line 48
    .line 49
    const/4 v9, 0x1

    .line 50
    if-ne v8, v9, :cond_1

    .line 51
    .line 52
    iget-object v8, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 53
    .line 54
    iget v8, v8, Landroid/graphics/Rect;->left:I

    .line 55
    .line 56
    int-to-float v8, v8

    .line 57
    int-to-float v9, v2

    .line 58
    sub-float/2addr v9, v0

    .line 59
    div-float/2addr v9, v6

    .line 60
    add-float/2addr v8, v9

    .line 61
    goto :goto_1

    .line 62
    :cond_1
    const/4 v8, 0x0

    .line 63
    :goto_1
    and-int/lit8 v6, v4, 0x30

    .line 64
    .line 65
    const/16 v9, 0x30

    .line 66
    .line 67
    if-ne v6, v9, :cond_2

    .line 68
    .line 69
    const/4 v5, 0x0

    .line 70
    :cond_2
    and-int/lit8 v6, v4, 0x50

    .line 71
    .line 72
    const/16 v9, 0x50

    .line 73
    .line 74
    if-ne v6, v9, :cond_3

    .line 75
    .line 76
    iget-object v5, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 77
    .line 78
    iget v5, v5, Landroid/graphics/Rect;->top:I

    .line 79
    .line 80
    int-to-float v5, v5

    .line 81
    int-to-float v3, v3

    .line 82
    sub-float/2addr v3, v1

    .line 83
    add-float/2addr v5, v3

    .line 84
    :cond_3
    const v3, 0x800003

    .line 85
    .line 86
    .line 87
    and-int v6, v4, v3

    .line 88
    .line 89
    if-ne v6, v3, :cond_4

    .line 90
    .line 91
    const/4 v8, 0x0

    .line 92
    :cond_4
    const v3, 0x800005

    .line 93
    .line 94
    .line 95
    and-int/2addr v4, v3

    .line 96
    if-ne v4, v3, :cond_5

    .line 97
    .line 98
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 99
    .line 100
    iget v3, v3, Landroid/graphics/Rect;->left:I

    .line 101
    .line 102
    int-to-float v3, v3

    .line 103
    int-to-float v2, v2

    .line 104
    sub-float/2addr v2, v0

    .line 105
    add-float v8, v3, v2

    .line 106
    .line 107
    :cond_5
    invoke-virtual {p1, v8, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1, v7, v7, v0, v1}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 111
    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;Landroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;)Lcom/intsig/camscanner/pic2word/view/rise/TextManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇888()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->O8()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    float-to-int v0, v0

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    add-int/2addr v0, v1

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    add-int/2addr v0, v1

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final O8(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "orderList"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇08O〇00〇o:Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    .line 7
    .line 8
    invoke-static {p1}, Lkotlin/text/StringsKt;->o〇8〇(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;->〇080(Ljava/lang/Iterable;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final OO0o〇〇(Ljava/lang/CharSequence;Z)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "text"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo〇8o008:Ljava/lang/CharSequence;

    .line 7
    .line 8
    if-eqz p2, :cond_1

    .line 9
    .line 10
    iget-object p2, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 11
    .line 12
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->OO0o〇〇〇〇0(Ljava/lang/CharSequence;)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->O8o08O8O:Landroid/animation/ValueAnimator;

    .line 16
    .line 17
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->isRunning()Z

    .line 18
    .line 19
    .line 20
    move-result p2

    .line 21
    if-eqz p2, :cond_0

    .line 22
    .line 23
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 24
    .line 25
    .line 26
    :cond_0
    iget-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo0:J

    .line 27
    .line 28
    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 29
    .line 30
    .line 31
    iget-object p2, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO〇00〇8oO:Landroid/view/animation/Interpolator;

    .line 32
    .line 33
    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 34
    .line 35
    .line 36
    new-instance p2, L〇o〇OO80oO/〇o00〇〇Oo;

    .line 37
    .line 38
    invoke-direct {p2, p1}, L〇o〇OO80oO/〇o00〇〇Oo;-><init>(Landroid/animation/ValueAnimator;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0, p2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->getCharStrategy()Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    invoke-static {}, Lcom/intsig/camscanner/pic2word/view/rise/strategy/Strategy;->〇080()Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->setCharStrategy(Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 57
    .line 58
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->OO0o〇〇〇〇0(Ljava/lang/CharSequence;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->setCharStrategy(Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->oO80()V

    .line 67
    .line 68
    .line 69
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->Oo08()Z

    .line 70
    .line 71
    .line 72
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 73
    .line 74
    .line 75
    :goto_0
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final Oooo8o0〇(IF)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    if-nez v0, :cond_1

    .line 14
    .line 15
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "getSystem()"

    .line 20
    .line 21
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 25
    .line 26
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-static {p1, p2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8o8o〇()V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final getAnimationDuration()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo0:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getAnimationInterpolator()Landroid/view/animation/Interpolator;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO〇00〇8oO:Landroid/view/animation/Interpolator;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBaseline()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->〇〇888()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x2

    .line 14
    int-to-float v2, v2

    .line 15
    div-float/2addr v1, v2

    .line 16
    iget v3, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 17
    .line 18
    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 19
    .line 20
    sub-float v0, v3, v0

    .line 21
    .line 22
    div-float/2addr v0, v2

    .line 23
    sub-float/2addr v0, v3

    .line 24
    add-float/2addr v1, v0

    .line 25
    float-to-int v0, v1

    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final getCharStrategy()Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇08O〇00〇o:Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;->Oo08()Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getCurrentText()[C
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->〇o〇()[C

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getLetterSpacingExtra()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->Oo08()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getText()Ljava/lang/CharSequence;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo〇8o008:Ljava/lang/CharSequence;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTextColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTextSize()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTextStyle()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o8〇OO0〇0o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTypeface()Landroid/graphics/Typeface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇O8o08O(Landroid/graphics/Canvas;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->o〇0()F

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    const/4 v1, 0x0

    .line 22
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->〇o00〇〇Oo(Landroid/graphics/Canvas;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇〇888()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o0:I

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇0()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇OOo8〇0:I

    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o0:I

    .line 14
    .line 15
    invoke-static {v0, p1}, Landroid/view/View;->resolveSize(II)I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇OOo8〇0:I

    .line 20
    .line 21
    invoke-static {v0, p2}, Landroid/view/View;->resolveSize(II)I

    .line 22
    .line 23
    .line 24
    move-result p2

    .line 25
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 2
    .line 3
    .line 4
    iget-object p3, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇080OO8〇0:Landroid/graphics/Rect;

    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    .line 7
    .line 8
    .line 9
    move-result p4

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    sub-int/2addr p1, v1

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    sub-int/2addr p2, v1

    .line 24
    invoke-virtual {p3, p4, v0, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public final setAnimationDuration(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo0:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setAnimationInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 1
    .param p1    # Landroid/view/animation/Interpolator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO〇00〇8oO:Landroid/view/animation/Interpolator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setCharStrategy(Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "value"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇08O〇00〇o:Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/CharOrderManager;->〇〇888(Lcom/intsig/camscanner/pic2word/view/rise/strategy/CharOrderStrategy;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setLetterSpacingExtra(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o〇00O:Lcom/intsig/camscanner/pic2word/view/rise/TextManager;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/TextManager;->〇80〇808〇O(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "text"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->oOo〇8o008:Ljava/lang/CharSequence;

    .line 7
    .line 8
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    xor-int/lit8 v0, v0, 0x1

    .line 13
    .line 14
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO0o〇〇(Ljava/lang/CharSequence;Z)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public final setTextColor(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    if-eq v0, p1, :cond_0

    .line 4
    .line 5
    iput p1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8〇oO〇〇8o:I

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setTextSize(F)V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-virtual {p0, v0, p1}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->Oooo8o0〇(IF)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setTextStyle(I)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eq p1, v0, :cond_0

    .line 3
    .line 4
    const/4 v0, 0x2

    .line 5
    if-eq p1, v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x3

    .line 8
    if-eq p1, v0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    iput v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o8〇OO0〇0o:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setTypeface(Landroid/graphics/Typeface;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->OO:Landroid/graphics/Paint;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->o8〇OO0〇0o:I

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    if-eq v1, v2, :cond_2

    .line 7
    .line 8
    const/4 v2, 0x2

    .line 9
    if-eq v1, v2, :cond_1

    .line 10
    .line 11
    const/4 v2, 0x3

    .line 12
    if-eq v1, v2, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-static {p1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    goto :goto_0

    .line 20
    :cond_1
    invoke-static {p1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    goto :goto_0

    .line 25
    :cond_2
    invoke-static {p1, v2}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    :goto_0
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;->〇8o8o〇()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

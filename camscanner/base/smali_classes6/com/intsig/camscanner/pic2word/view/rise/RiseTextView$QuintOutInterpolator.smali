.class public final Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView$QuintOutInterpolator;
.super Ljava/lang/Object;
.source "RiseTextView.kt"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pic2word/view/rise/RiseTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QuintOutInterpolator"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3

    .line 1
    const/high16 v0, 0x40000000    # 2.0f

    .line 2
    .line 3
    mul-float v0, v0, p1

    .line 4
    .line 5
    const/high16 v1, 0x3f000000    # 0.5f

    .line 6
    .line 7
    cmpg-float v2, p1, v1

    .line 8
    .line 9
    if-gez v2, :cond_0

    .line 10
    .line 11
    mul-float v1, v1, v0

    .line 12
    .line 13
    mul-float v1, v1, v0

    .line 14
    .line 15
    mul-float v1, v1, v0

    .line 16
    .line 17
    mul-float v1, v1, v0

    .line 18
    .line 19
    mul-float v1, v1, v0

    .line 20
    .line 21
    return v1

    .line 22
    :cond_0
    sub-float/2addr p1, v1

    .line 23
    const/4 v0, 0x2

    .line 24
    int-to-float v0, v0

    .line 25
    mul-float p1, p1, v0

    .line 26
    .line 27
    const/4 v0, 0x1

    .line 28
    int-to-float v0, v0

    .line 29
    sub-float/2addr p1, v0

    .line 30
    mul-float v1, v1, p1

    .line 31
    .line 32
    mul-float v1, v1, p1

    .line 33
    .line 34
    mul-float v1, v1, p1

    .line 35
    .line 36
    mul-float v1, v1, p1

    .line 37
    .line 38
    mul-float v1, v1, p1

    .line 39
    .line 40
    add-float/2addr v1, v0

    .line 41
    return v1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

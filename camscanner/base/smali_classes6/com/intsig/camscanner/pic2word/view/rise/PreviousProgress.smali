.class public final Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;
.super Ljava/lang/Object;
.source "TextManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:C

.field private final Oo08:F

.field private final 〇080:I

.field private final 〇o00〇〇Oo:D

.field private final 〇o〇:D


# direct methods
.method public constructor <init>(IDDCF)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇080:I

    .line 3
    iput-wide p2, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o00〇〇Oo:D

    .line 4
    iput-wide p4, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o〇:D

    .line 5
    iput-char p6, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->O8:C

    .line 6
    iput p7, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->Oo08:F

    return-void
.end method

.method public synthetic constructor <init>(IDDCFILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v7, 0x0

    goto :goto_0

    :cond_0
    move v7, p6

    :goto_0
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    const/4 v8, 0x0

    goto :goto_1

    :cond_1
    move/from16 v8, p7

    :goto_1
    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    .line 7
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;-><init>(IDDCF)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    return v2

    .line 11
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇080:I

    .line 14
    .line 15
    iget v3, p1, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇080:I

    .line 16
    .line 17
    if-eq v1, v3, :cond_2

    .line 18
    .line 19
    return v2

    .line 20
    :cond_2
    iget-wide v3, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o00〇〇Oo:D

    .line 21
    .line 22
    iget-wide v5, p1, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o00〇〇Oo:D

    .line 23
    .line 24
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_3

    .line 29
    .line 30
    return v2

    .line 31
    :cond_3
    iget-wide v3, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o〇:D

    .line 32
    .line 33
    iget-wide v5, p1, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o〇:D

    .line 34
    .line 35
    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_4

    .line 40
    .line 41
    return v2

    .line 42
    :cond_4
    iget-char v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->O8:C

    .line 43
    .line 44
    iget-char v3, p1, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->O8:C

    .line 45
    .line 46
    if-eq v1, v3, :cond_5

    .line 47
    .line 48
    return v2

    .line 49
    :cond_5
    iget v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->Oo08:F

    .line 50
    .line 51
    iget p1, p1, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->Oo08:F

    .line 52
    .line 53
    invoke-static {v1, p1}, Ljava/lang/Float;->compare(FF)I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    if-eqz p1, :cond_6

    .line 58
    .line 59
    return v2

    .line 60
    :cond_6
    return v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇080:I

    .line 2
    .line 3
    mul-int/lit8 v0, v0, 0x1f

    .line 4
    .line 5
    iget-wide v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o00〇〇Oo:D

    .line 6
    .line 7
    invoke-static {v1, v2}, L〇o〇OO80oO/〇080;->〇080(D)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    add-int/2addr v0, v1

    .line 12
    mul-int/lit8 v0, v0, 0x1f

    .line 13
    .line 14
    iget-wide v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o〇:D

    .line 15
    .line 16
    invoke-static {v1, v2}, L〇o〇OO80oO/〇080;->〇080(D)I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    add-int/2addr v0, v1

    .line 21
    mul-int/lit8 v0, v0, 0x1f

    .line 22
    .line 23
    iget-char v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->O8:C

    .line 24
    .line 25
    add-int/2addr v0, v1

    .line 26
    mul-int/lit8 v0, v0, 0x1f

    .line 27
    .line 28
    iget v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->Oo08:F

    .line 29
    .line 30
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    add-int/2addr v0, v1

    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public toString()Ljava/lang/String;
    .locals 9
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇080:I

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o00〇〇Oo:D

    .line 4
    .line 5
    iget-wide v3, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o〇:D

    .line 6
    .line 7
    iget-char v5, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->O8:C

    .line 8
    .line 9
    iget v6, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->Oo08:F

    .line 10
    .line 11
    new-instance v7, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v8, "PreviousProgress(currentIndex="

    .line 17
    .line 18
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v0, ", offsetPercentage="

    .line 25
    .line 26
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v0, ", progress="

    .line 33
    .line 34
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v7, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    const-string v0, ", currentChar="

    .line 41
    .line 42
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string v0, ", currentWidth="

    .line 49
    .line 50
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v0, ")"

    .line 57
    .line 58
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇080()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/rise/PreviousProgress;->〇o〇:D

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/pic2word/view/ParticleLayer;
.super Ljava/lang/Object;
.source "ParticleLayer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;,
        Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇80〇808〇O:Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:J

.field private oO80:Z

.field private final o〇0:Lkotlin/random/Random;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080:I

.field private 〇o00〇〇Oo:I

.field private final 〇o〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    new-instance v0, Ljava/util/ArrayList;

    .line 10
    .line 11
    const/16 v1, 0xc8

    .line 12
    .line 13
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇o〇:Ljava/util/ArrayList;

    .line 17
    .line 18
    new-instance v0, Landroid/graphics/Paint;

    .line 19
    .line 20
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 21
    .line 22
    .line 23
    const/4 v2, 0x1

    .line 24
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 25
    .line 26
    .line 27
    const/high16 v2, -0x10000

    .line 28
    .line 29
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 30
    .line 31
    .line 32
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    .line 46
    .line 47
    const v2, 0x3f99999a    # 1.2f

    .line 48
    .line 49
    .line 50
    mul-float p1, p1, v2

    .line 51
    .line 52
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 53
    .line 54
    .line 55
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->O8:Landroid/graphics/Paint;

    .line 56
    .line 57
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 58
    .line 59
    .line 60
    move-result-wide v2

    .line 61
    invoke-static {v2, v3}, Lkotlin/random/RandomKt;->〇080(J)Lkotlin/random/Random;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->o〇0:Lkotlin/random/Random;

    .line 66
    .line 67
    iput v1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇〇888:I

    .line 68
    .line 69
    const/4 p1, 0x0

    .line 70
    :goto_0
    if-ge p1, v1, :cond_0

    .line 71
    .line 72
    new-instance v0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;

    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->O8:Landroid/graphics/Paint;

    .line 75
    .line 76
    invoke-direct {v0, v2}, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;-><init>(Landroid/graphics/Paint;)V

    .line 77
    .line 78
    .line 79
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇o〇:Ljava/util/ArrayList;

    .line 80
    .line 81
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    .line 83
    .line 84
    add-int/lit8 p1, p1, 0x1

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_0
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇o〇()V
    .locals 12

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->oO80:Z

    .line 3
    .line 4
    iget v1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇o00〇〇Oo:I

    .line 5
    .line 6
    const/16 v2, 0x64

    .line 7
    .line 8
    if-le v1, v2, :cond_1

    .line 9
    .line 10
    iget v3, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇080:I

    .line 11
    .line 12
    if-gtz v3, :cond_0

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_0
    const/4 v3, 0x1

    .line 16
    iput-boolean v3, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->oO80:Z

    .line 17
    .line 18
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇o〇:Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    if-eqz v4, :cond_1

    .line 29
    .line 30
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    check-cast v4, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;

    .line 35
    .line 36
    iget-object v5, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->o〇0:Lkotlin/random/Random;

    .line 37
    .line 38
    iget v6, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇080:I

    .line 39
    .line 40
    invoke-virtual {v5, v0, v6}, Lkotlin/random/Random;->nextInt(II)I

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    int-to-float v6, v5

    .line 45
    iget-object v5, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->o〇0:Lkotlin/random/Random;

    .line 46
    .line 47
    invoke-virtual {v5, v2, v1}, Lkotlin/random/Random;->nextInt(II)I

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    int-to-float v7, v5

    .line 52
    iget-object v5, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->o〇0:Lkotlin/random/Random;

    .line 53
    .line 54
    const-wide/16 v8, 0x7d0

    .line 55
    .line 56
    const-wide/16 v10, 0xfa0

    .line 57
    .line 58
    invoke-virtual {v5, v8, v9, v10, v11}, Lkotlin/random/Random;->nextLong(JJ)J

    .line 59
    .line 60
    .line 61
    move-result-wide v10

    .line 62
    iget-wide v8, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->Oo08:J

    .line 63
    .line 64
    move-object v5, v4

    .line 65
    invoke-virtual/range {v5 .. v11}, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇o00〇〇Oo(FFJJ)V

    .line 66
    .line 67
    .line 68
    iget-object v5, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->o〇0:Lkotlin/random/Random;

    .line 69
    .line 70
    const-wide v6, 0x3fa47ae147ae147bL    # 0.04

    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    const-wide v8, 0x3fbeb851eb851eb8L    # 0.12

    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    invoke-virtual {v5, v6, v7, v8, v9}, Lkotlin/random/Random;->nextDouble(DD)D

    .line 81
    .line 82
    .line 83
    move-result-wide v5

    .line 84
    double-to-float v5, v5

    .line 85
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇o〇(F)V

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_1
    :goto_1
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public final O8(Landroid/graphics/Shader;)V
    .locals 1
    .param p1    # Landroid/graphics/Shader;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "shader"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->O8:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo08(II)V
    .locals 0

    .line 1
    add-int/lit8 p1, p1, -0x14

    .line 2
    .line 3
    iput p1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇080:I

    .line 4
    .line 5
    iput p2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇o00〇〇Oo:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇0()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇〇888()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇o〇()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final update()V
    .locals 5

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇〇888:I

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    :goto_0
    if-ge v3, v2, :cond_0

    .line 9
    .line 10
    iget-object v4, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇o〇:Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v4

    .line 16
    check-cast v4, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;

    .line 17
    .line 18
    invoke-virtual {v4, v0, v1}, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->update(J)V

    .line 19
    .line 20
    .line 21
    add-int/lit8 v3, v3, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇080()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "c"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->oO80:Z

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇〇888:I

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    :goto_0
    if-ge v1, v0, :cond_1

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->〇o〇:Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;

    .line 23
    .line 24
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇080(Landroid/graphics/Canvas;)V

    .line 25
    .line 26
    .line 27
    add-int/lit8 v1, v1, 0x1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇〇888()V
    .locals 2

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer;->Oo08:J

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

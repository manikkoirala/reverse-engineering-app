.class final Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;
.super Ljava/lang/Object;
.source "ParticleLayer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pic2word/view/ParticleLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Particle"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:F

.field private Oo08:F

.field private oO80:J

.field private o〇0:F

.field private final 〇080:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇80〇808〇O:J

.field private 〇o00〇〇Oo:F

.field private 〇o〇:F

.field private 〇〇888:F


# direct methods
.method public constructor <init>(Landroid/graphics/Paint;)V
    .locals 2
    .param p1    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "paint"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇080:Landroid/graphics/Paint;

    .line 10
    .line 11
    const-wide/16 v0, 0xbb8

    .line 12
    .line 13
    iput-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇80〇808〇O:J

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final update(J)V
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->oO80:J

    .line 2
    .line 3
    sub-long v0, p1, v0

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->o〇0:F

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇o00〇〇Oo:F

    .line 8
    .line 9
    long-to-float v4, v0

    .line 10
    mul-float v3, v3, v4

    .line 11
    .line 12
    add-float/2addr v2, v3

    .line 13
    iput v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->O8:F

    .line 14
    .line 15
    iget v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇〇888:F

    .line 16
    .line 17
    iget v3, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇o〇:F

    .line 18
    .line 19
    mul-float v3, v3, v4

    .line 20
    .line 21
    const/4 v4, -0x1

    .line 22
    int-to-float v4, v4

    .line 23
    mul-float v3, v3, v4

    .line 24
    .line 25
    add-float/2addr v2, v3

    .line 26
    iput v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->Oo08:F

    .line 27
    .line 28
    const/4 v3, 0x0

    .line 29
    cmpg-float v2, v2, v3

    .line 30
    .line 31
    if-lez v2, :cond_0

    .line 32
    .line 33
    iget-wide v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇80〇808〇O:J

    .line 34
    .line 35
    cmp-long v4, v0, v2

    .line 36
    .line 37
    if-lez v4, :cond_1

    .line 38
    .line 39
    :cond_0
    iput-wide p1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->oO80:J

    .line 40
    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇080(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "c"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->O8:F

    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->Oo08:F

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇080:Landroid/graphics/Paint;

    .line 11
    .line 12
    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o00〇〇Oo(FFJJ)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->o〇0:F

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇〇888:F

    .line 4
    .line 5
    iput-wide p3, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->oO80:J

    .line 6
    .line 7
    iput-wide p5, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇80〇808〇O:J

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public final 〇o〇(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/view/ParticleLayer$Particle;->〇o〇:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class final Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;
.super Ljava/lang/Object;
.source "ZoomGesture.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ScrollValues"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:F

.field private 〇080:F

.field private 〇o00〇〇Oo:F

.field private 〇o〇:F


# direct methods
.method public constructor <init>()V
    .locals 7

    .line 1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;-><init>(FFFFILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080:F

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo:F

    .line 5
    iput p3, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇:F

    .line 6
    iput p4, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8:F

    return-void
.end method

.method public synthetic constructor <init>(FFFFILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    const/4 p3, 0x0

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    const/4 p4, 0x0

    .line 7
    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;-><init>(FFFF)V

    return-void
.end method


# virtual methods
.method public final O8()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo08(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    return v2

    .line 11
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080:F

    .line 14
    .line 15
    iget v3, p1, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080:F

    .line 16
    .line 17
    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_2

    .line 22
    .line 23
    return v2

    .line 24
    :cond_2
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo:F

    .line 25
    .line 26
    iget v3, p1, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo:F

    .line 27
    .line 28
    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-eqz v1, :cond_3

    .line 33
    .line 34
    return v2

    .line 35
    :cond_3
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇:F

    .line 36
    .line 37
    iget v3, p1, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇:F

    .line 38
    .line 39
    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-eqz v1, :cond_4

    .line 44
    .line 45
    return v2

    .line 46
    :cond_4
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8:F

    .line 47
    .line 48
    iget p1, p1, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8:F

    .line 49
    .line 50
    invoke-static {v1, p1}, Ljava/lang/Float;->compare(FF)I

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    if-eqz p1, :cond_5

    .line 55
    .line 56
    return v2

    .line 57
    :cond_5
    return v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080:F

    .line 2
    .line 3
    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    mul-int/lit8 v0, v0, 0x1f

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo:F

    .line 10
    .line 11
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    mul-int/lit8 v0, v0, 0x1f

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇:F

    .line 19
    .line 20
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    add-int/2addr v0, v1

    .line 25
    mul-int/lit8 v0, v0, 0x1f

    .line 26
    .line 27
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8:F

    .line 28
    .line 29
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    add-int/2addr v0, v1

    .line 34
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final oO80(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o〇0(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public toString()Ljava/lang/String;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇:F

    .line 6
    .line 7
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8:F

    .line 8
    .line 9
    new-instance v4, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v5, "ScrollValues(minX="

    .line 15
    .line 16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v0, ", maxX="

    .line 23
    .line 24
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v0, ", minY="

    .line 31
    .line 32
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v0, ", maxY="

    .line 39
    .line 40
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v0, ")"

    .line 47
    .line 48
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇080()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇888(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

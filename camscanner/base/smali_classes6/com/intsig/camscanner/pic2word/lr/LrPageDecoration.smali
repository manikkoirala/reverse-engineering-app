.class public final Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;
.super Ljava/lang/Object;
.source "LrPageDecoration.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oooo8o0〇:Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static 〇〇808〇:Landroid/graphics/Bitmap;


# instance fields
.field private final O8:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇:F

.field private final OO0o〇〇〇〇0:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO80:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇0:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8o8o〇:Z

.field private 〇O8o08O:Z

.field private 〇o00〇〇Oo:F

.field private 〇o〇:F

.field private final 〇〇888:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->Oooo8o0〇:Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;FFLandroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇080:Landroid/content/Context;

    .line 10
    .line 11
    iput p2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 12
    .line 13
    iput p3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o〇:F

    .line 14
    .line 15
    if-nez p4, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const p2, 0x7f080e1e

    .line 22
    .line 23
    .line 24
    invoke-static {p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 25
    .line 26
    .line 27
    move-result-object p4

    .line 28
    :cond_0
    sput-object p4, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇〇808〇:Landroid/graphics/Bitmap;

    .line 29
    .line 30
    new-instance p1, Landroid/graphics/Paint;

    .line 31
    .line 32
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 33
    .line 34
    .line 35
    const/4 p2, 0x1

    .line 36
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 37
    .line 38
    .line 39
    const/4 p3, -0x1

    .line 40
    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    .line 42
    .line 43
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->O8:Landroid/graphics/Paint;

    .line 44
    .line 45
    new-instance p1, Landroid/graphics/Paint;

    .line 46
    .line 47
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 51
    .line 52
    .line 53
    const p3, -0x333334

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    .line 58
    .line 59
    sget-object p3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 60
    .line 61
    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 62
    .line 63
    .line 64
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->Oo08:Landroid/graphics/Paint;

    .line 65
    .line 66
    new-instance p1, Landroid/graphics/Paint;

    .line 67
    .line 68
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 72
    .line 73
    .line 74
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->o〇0:Landroid/graphics/Paint;

    .line 75
    .line 76
    new-instance p1, Landroid/graphics/RectF;

    .line 77
    .line 78
    iget p2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 79
    .line 80
    iget p3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o〇:F

    .line 81
    .line 82
    const/4 p4, 0x0

    .line 83
    invoke-direct {p1, p4, p4, p2, p3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 84
    .line 85
    .line 86
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇〇888:Landroid/graphics/RectF;

    .line 87
    .line 88
    new-instance p1, Landroid/graphics/Path;

    .line 89
    .line 90
    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    .line 91
    .line 92
    .line 93
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 94
    .line 95
    new-instance p1, Landroid/graphics/Matrix;

    .line 96
    .line 97
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 98
    .line 99
    .line 100
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 101
    .line 102
    new-instance p1, Landroid/graphics/RectF;

    .line 103
    .line 104
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 105
    .line 106
    .line 107
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->OO0o〇〇〇〇0:Landroid/graphics/RectF;

    .line 108
    .line 109
    const/high16 p1, 0x3f800000    # 1.0f

    .line 110
    .line 111
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->OO0o〇〇:F

    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇o00〇〇Oo()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    xor-int/lit8 v0, v0, 0x1

    .line 6
    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇888()V
    .locals 5

    .line 1
    const/16 v0, 0xc

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->OO0o〇〇:F

    .line 8
    .line 9
    div-float/2addr v0, v1

    .line 10
    const/16 v1, 0x8

    .line 11
    .line 12
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    iget v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->OO0o〇〇:F

    .line 17
    .line 18
    div-float/2addr v1, v2

    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 20
    .line 21
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 22
    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 25
    .line 26
    add-float/2addr v0, v1

    .line 27
    invoke-virtual {v2, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 31
    .line 32
    invoke-virtual {v2, v0, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 33
    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 36
    .line 37
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 38
    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 41
    .line 42
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 43
    .line 44
    sub-float/2addr v3, v1

    .line 45
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 46
    .line 47
    .line 48
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 49
    .line 50
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 51
    .line 52
    sub-float/2addr v3, v0

    .line 53
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 54
    .line 55
    .line 56
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 57
    .line 58
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 59
    .line 60
    sub-float/2addr v3, v0

    .line 61
    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 62
    .line 63
    .line 64
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 65
    .line 66
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o〇:F

    .line 67
    .line 68
    sub-float/2addr v3, v0

    .line 69
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 70
    .line 71
    .line 72
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 73
    .line 74
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o〇:F

    .line 75
    .line 76
    sub-float/2addr v3, v0

    .line 77
    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 78
    .line 79
    .line 80
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 81
    .line 82
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o〇:F

    .line 83
    .line 84
    sub-float/2addr v3, v1

    .line 85
    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 86
    .line 87
    .line 88
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 89
    .line 90
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 91
    .line 92
    sub-float/2addr v3, v1

    .line 93
    iget v4, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o〇:F

    .line 94
    .line 95
    sub-float/2addr v4, v0

    .line 96
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 97
    .line 98
    .line 99
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 100
    .line 101
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 102
    .line 103
    sub-float/2addr v3, v0

    .line 104
    iget v4, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o〇:F

    .line 105
    .line 106
    sub-float/2addr v4, v0

    .line 107
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 108
    .line 109
    .line 110
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 111
    .line 112
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 113
    .line 114
    sub-float/2addr v3, v0

    .line 115
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o〇:F

    .line 116
    .line 117
    sub-float/2addr v0, v1

    .line 118
    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 119
    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public final O8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇O8o08O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo08(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇8o8o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0(F)V
    .locals 4

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->OO0o〇〇:F

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->Oo08:Landroid/graphics/Paint;

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    div-float/2addr v2, p1

    .line 11
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇〇888()V

    .line 15
    .line 16
    .line 17
    int-to-float v0, v1

    .line 18
    div-float/2addr v0, p1

    .line 19
    sget-object p1, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇〇808〇:Landroid/graphics/Bitmap;

    .line 20
    .line 21
    if-eqz p1, :cond_0

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo()Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v1, :cond_0

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 30
    .line 31
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 32
    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 35
    .line 36
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    int-to-float v1, v1

    .line 44
    mul-float v1, v1, v0

    .line 45
    .line 46
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    int-to-float p1, p1

    .line 51
    mul-float p1, p1, v0

    .line 52
    .line 53
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 54
    .line 55
    sub-float/2addr v0, v1

    .line 56
    const/4 v1, 0x4

    .line 57
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    iget v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->OO0o〇〇:F

    .line 62
    .line 63
    div-float/2addr v1, v2

    .line 64
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->OO0o〇〇〇〇0:Landroid/graphics/RectF;

    .line 65
    .line 66
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo:F

    .line 67
    .line 68
    add-float/2addr p1, v1

    .line 69
    invoke-virtual {v2, v0, v1, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 73
    .line 74
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 75
    .line 76
    .line 77
    :cond_0
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇080(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇o00〇〇Oo()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇〇808〇:Landroid/graphics/Bitmap;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇80〇808〇O:Landroid/graphics/Matrix;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->o〇0:Landroid/graphics/Paint;

    .line 19
    .line 20
    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇o〇(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇〇888:Landroid/graphics/RectF;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    iget-boolean v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇8o8o〇:Z

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    iget-boolean v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇O8o08O:Z

    .line 19
    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇〇888:Landroid/graphics/RectF;

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->O8:Landroid/graphics/Paint;

    .line 25
    .line 26
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 27
    .line 28
    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 30
    .line 31
    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_2

    .line 36
    .line 37
    iget-boolean v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->〇8o8o〇:Z

    .line 38
    .line 39
    if-nez v0, :cond_2

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->oO80:Landroid/graphics/Path;

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrPageDecoration;->Oo08:Landroid/graphics/Paint;

    .line 44
    .line 45
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 46
    .line 47
    .line 48
    :cond_2
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

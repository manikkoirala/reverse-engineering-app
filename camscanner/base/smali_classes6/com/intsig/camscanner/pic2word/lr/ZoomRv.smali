.class public final Lcom/intsig/camscanner/pic2word/lr/ZoomRv;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "ZoomRV.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pic2word/lr/ZoomRv$LM;,
        Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final ooo0〇〇O:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Z

.field private final OO:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO〇00〇8oO:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO0〇0o:Landroid/view/ScaleGestureDetector;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Landroid/view/GestureDetector;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mGestureListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:F

.field private final 〇080OO8〇0:Landroid/view/animation/Interpolator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:[F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Landroid/widget/OverScroller;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:I

.field private final 〇OOo8〇0:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->ooo0〇〇O:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 3
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 4
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->OO:Landroid/graphics/Matrix;

    const/16 p1, 0x9

    new-array p1, p1, [F

    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇08O〇00〇o:[F

    const/high16 p1, 0x3f800000    # 1.0f

    .line 6
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o〇00O:F

    .line 7
    new-instance p1, LO8〇o0〇〇8/〇〇888;

    invoke-direct {p1}, LO8〇o0〇〇8/〇〇888;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇080OO8〇0:Landroid/view/animation/Interpolator;

    .line 8
    new-instance p2, Landroid/widget/OverScroller;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇0O:Landroid/widget/OverScroller;

    .line 9
    new-instance p1, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mGestureListener$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mGestureListener$1;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->oOo〇8o008:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mGestureListener$1;

    .line 10
    new-instance p2, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->oOo0:Landroid/view/GestureDetector;

    .line 11
    new-instance p1, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->OO〇00〇8oO:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;

    .line 12
    new-instance p2, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o8〇OO0〇0o:Landroid/view/ScaleGestureDetector;

    .line 13
    new-instance p1, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$LM;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$LM;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 16
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 17
    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->OO:Landroid/graphics/Matrix;

    const/16 p1, 0x9

    new-array p1, p1, [F

    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇08O〇00〇o:[F

    const/high16 p1, 0x3f800000    # 1.0f

    .line 19
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o〇00O:F

    .line 20
    new-instance p1, LO8〇o0〇〇8/〇〇888;

    invoke-direct {p1}, LO8〇o0〇〇8/〇〇888;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇080OO8〇0:Landroid/view/animation/Interpolator;

    .line 21
    new-instance p2, Landroid/widget/OverScroller;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇0O:Landroid/widget/OverScroller;

    .line 22
    new-instance p1, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mGestureListener$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mGestureListener$1;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->oOo〇8o008:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mGestureListener$1;

    .line 23
    new-instance p2, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->oOo0:Landroid/view/GestureDetector;

    .line 24
    new-instance p1, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)V

    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->OO〇00〇8oO:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;

    .line 25
    new-instance p2, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o8〇OO0〇0o:Landroid/view/ScaleGestureDetector;

    .line 26
    new-instance p1, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$LM;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$LM;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    return-void
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o〇00O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO0o〇〇〇〇0()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇08O〇00〇o:[F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇08O〇00〇o:[F

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    aget v2, v0, v1

    .line 12
    .line 13
    const/4 v3, 0x4

    .line 14
    const/high16 v4, 0x3f800000    # 1.0f

    .line 15
    .line 16
    cmpg-float v5, v2, v4

    .line 17
    .line 18
    if-gez v5, :cond_0

    .line 19
    .line 20
    aput v4, v0, v1

    .line 21
    .line 22
    aput v4, v0, v3

    .line 23
    .line 24
    const/high16 v2, 0x3f800000    # 1.0f

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/high16 v4, 0x40800000    # 4.0f

    .line 28
    .line 29
    cmpl-float v5, v2, v4

    .line 30
    .line 31
    if-lez v5, :cond_1

    .line 32
    .line 33
    aput v4, v0, v1

    .line 34
    .line 35
    aput v4, v0, v3

    .line 36
    .line 37
    const/high16 v2, 0x40800000    # 4.0f

    .line 38
    .line 39
    :cond_1
    :goto_0
    const/4 v3, 0x2

    .line 40
    aget v4, v0, v3

    .line 41
    .line 42
    const/4 v5, 0x1

    .line 43
    const/4 v6, 0x0

    .line 44
    cmpl-float v7, v4, v6

    .line 45
    .line 46
    if-lez v7, :cond_2

    .line 47
    .line 48
    aput v6, v0, v3

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    int-to-float v0, v0

    .line 56
    int-to-float v7, v5

    .line 57
    sub-float v7, v2, v7

    .line 58
    .line 59
    mul-float v0, v0, v7

    .line 60
    .line 61
    neg-float v0, v0

    .line 62
    cmpg-float v4, v4, v0

    .line 63
    .line 64
    if-gez v4, :cond_3

    .line 65
    .line 66
    iget-object v4, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇08O〇00〇o:[F

    .line 67
    .line 68
    aput v0, v4, v3

    .line 69
    .line 70
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇08O〇00〇o:[F

    .line 71
    .line 72
    const/4 v3, 0x5

    .line 73
    aget v4, v0, v3

    .line 74
    .line 75
    cmpg-float v4, v4, v6

    .line 76
    .line 77
    if-nez v4, :cond_4

    .line 78
    .line 79
    const/4 v1, 0x1

    .line 80
    :cond_4
    if-nez v1, :cond_5

    .line 81
    .line 82
    aput v6, v0, v3

    .line 83
    .line 84
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 85
    .line 86
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->setValues([F)V

    .line 87
    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 90
    .line 91
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 92
    .line 93
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-eqz v0, :cond_6

    .line 98
    .line 99
    return-void

    .line 100
    :cond_6
    sget-object v0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->ooo0〇〇O:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;

    .line 101
    .line 102
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 103
    .line 104
    new-instance v3, Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .line 108
    .line 109
    const-string v4, "update matrix... "

    .line 110
    .line 111
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v1

    .line 121
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;->〇080(Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    iput v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o〇00O:F

    .line 125
    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    .line 127
    .line 128
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .line 130
    .line 131
    const-string v3, "scale: "

    .line 132
    .line 133
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v1

    .line 143
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;->〇080(Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 147
    .line 148
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 149
    .line 150
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 151
    .line 152
    .line 153
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 154
    .line 155
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->OO:Landroid/graphics/Matrix;

    .line 156
    .line 157
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 158
    .line 159
    .line 160
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 161
    .line 162
    .line 163
    return-void
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->O8o08O8O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oO80(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)Landroid/graphics/Matrix;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080(F)F
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇8o8o〇(F)F

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->O8o08O8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇8o8o〇(F)F
    .locals 2

    .line 1
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2
    .line 3
    sub-float/2addr p0, v0

    .line 4
    mul-float v1, p0, p0

    .line 5
    .line 6
    mul-float v1, v1, p0

    .line 7
    .line 8
    mul-float v1, v1, p0

    .line 9
    .line 10
    mul-float v1, v1, p0

    .line 11
    .line 12
    add-float/2addr v1, v0

    .line 13
    return v1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)[F
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇08O〇00〇o:[F

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)Landroid/widget/OverScroller;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇0O:Landroid/widget/OverScroller;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public computeScroll()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇0O:Landroid/widget/OverScroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->ooo0〇〇O:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇0O:Landroid/widget/OverScroller;

    .line 13
    .line 14
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrX()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    new-instance v2, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v3, "computeScroll cx: "

    .line 24
    .line 25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;->〇080(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇0O:Landroid/widget/OverScroller;

    .line 39
    .line 40
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o0:Landroid/graphics/Matrix;

    .line 45
    .line 46
    iget v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇8〇oO〇〇8o:I

    .line 47
    .line 48
    sub-int v2, v0, v2

    .line 49
    .line 50
    int-to-float v2, v2

    .line 51
    const/4 v3, 0x0

    .line 52
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 53
    .line 54
    .line 55
    iput v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇8〇oO〇〇8o:I

    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->OO0o〇〇〇〇0()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0}, Landroid/view/View;->postInvalidateOnAnimation()V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public computeVerticalScrollRange()I
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->computeVerticalScrollRange()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o〇00O:F

    .line 6
    .line 7
    int-to-float v0, v0

    .line 8
    mul-float v1, v1, v0

    .line 9
    .line 10
    const/high16 v0, 0x3f000000    # 0.5f

    .line 11
    .line 12
    add-float/2addr v1, v0

    .line 13
    float-to-int v0, v1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 7
    .line 8
    .line 9
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->OO:Landroid/graphics/Matrix;

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final getDrawMatrix()Landroid/graphics/Matrix;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMatrixScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o〇00O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz p1, :cond_2

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView;->isLayoutSuppressed()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->ooo0〇〇O:Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;

    .line 15
    .line 16
    new-instance v2, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v3, "onInterceptTouchEvent: "

    .line 22
    .line 23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$Companion;->〇080(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    const/4 p1, 0x1

    .line 39
    return p1

    .line 40
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-nez v1, :cond_2

    .line 45
    .line 46
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 51
    .line 52
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 53
    .line 54
    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->oOo0:Landroid/view/GestureDetector;

    .line 56
    .line 57
    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    .line 61
    .line 62
    .line 63
    :cond_2
    :goto_0
    return v0
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->O8o08O8O:Z

    .line 10
    .line 11
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇OOo8〇0:Landroid/graphics/Matrix;

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 18
    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o8〇OO0〇0o:Landroid/view/ScaleGestureDetector;

    .line 21
    .line 22
    invoke-virtual {v2, v1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    or-int/2addr v0, v2

    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->oOo0:Landroid/view/GestureDetector;

    .line 28
    .line 29
    invoke-virtual {v2, v1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    or-int/2addr v0, v2

    .line 34
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    const/4 v2, 0x2

    .line 42
    if-ne v1, v2, :cond_1

    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->OO0o〇〇〇〇0()V

    .line 45
    .line 46
    .line 47
    :cond_1
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    or-int/2addr p1, v0

    .line 52
    return p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

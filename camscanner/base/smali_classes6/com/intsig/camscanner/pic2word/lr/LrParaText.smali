.class public final Lcom/intsig/camscanner/pic2word/lr/LrParaText;
.super Lcom/intsig/camscanner/pic2word/lr/LrText;
.source "LrParaText.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pic2word/lr/LrParaText$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O000:Lcom/intsig/camscanner/pic2word/lr/LrParaText$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final oO00OOO:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrParaText$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->O000:Lcom/intsig/camscanner/pic2word/lr/LrParaText$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;Lcom/intsig/camscanner/pic2word/lr/LrView;)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pic2word/lr/LrView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "segment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "parentView"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/pic2word/lr/LrText;-><init>(Lcom/intsig/camscanner/pic2word/lr/LrView;)V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 15
    .line 16
    const/4 p2, 0x1

    .line 17
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->o8(Z)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->setLrElement(Lcom/intsig/camscanner/pic2word/lr/LrElement;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->〇O00()V

    .line 24
    .line 25
    .line 26
    new-instance p1, Landroid/graphics/Paint;

    .line 27
    .line 28
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 32
    .line 33
    .line 34
    const/16 p2, 0xbc

    .line 35
    .line 36
    const/16 v0, 0xaa

    .line 37
    .line 38
    const/16 v1, 0xff

    .line 39
    .line 40
    const/16 v2, 0x19

    .line 41
    .line 42
    invoke-static {v1, v2, p2, v0}, Landroid/graphics/Color;->argb(IIII)I

    .line 43
    .line 44
    .line 45
    move-result p2

    .line 46
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    .line 48
    .line 49
    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 50
    .line 51
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 52
    .line 53
    .line 54
    const/high16 p2, 0x3f800000    # 1.0f

    .line 55
    .line 56
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 57
    .line 58
    .line 59
    new-instance p2, Landroid/graphics/DashPathEffect;

    .line 60
    .line 61
    const/4 v0, 0x2

    .line 62
    new-array v0, v0, [F

    .line 63
    .line 64
    fill-array-data v0, :array_0

    .line 65
    .line 66
    .line 67
    const/4 v1, 0x0

    .line 68
    invoke-direct {p2, v0, v1}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 72
    .line 73
    .line 74
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->oO00OOO:Landroid/graphics/Paint;

    .line 75
    .line 76
    return-void

    .line 77
    :array_0
    .array-data 4
        0x40000000    # 2.0f
        0x40000000    # 2.0f
    .end array-data
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final Oo0oO〇O〇O(Landroid/graphics/Canvas;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getNeedDrawDashRect()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->oO00OOO:Landroid/graphics/Paint;

    .line 16
    .line 17
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
.end method

.method private final o0O〇8o0O(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/pic2word/lr/LrView;->oO〇8O8oOo:Lcom/intsig/camscanner/pic2word/lr/LrView$Companion;

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMIsImageEdit()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrView$Companion;->〇080(Z)F

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMIsImageEdit()Z

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView$Companion;->〇080(Z)F

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    if-eqz v0, :cond_5

    .line 40
    .line 41
    move-object v1, v0

    .line 42
    check-cast v1, Ljava/util/Collection;

    .line 43
    .line 44
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    const/4 v2, 0x1

    .line 49
    xor-int/2addr v1, v2

    .line 50
    const/4 v3, 0x0

    .line 51
    if-eqz v1, :cond_0

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    move-object v0, v3

    .line 55
    :goto_0
    if-eqz v0, :cond_5

    .line 56
    .line 57
    check-cast v0, Ljava/lang/Iterable;

    .line 58
    .line 59
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    if-eqz v1, :cond_5

    .line 68
    .line 69
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    check-cast v1, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;

    .line 74
    .line 75
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getSlices()Ljava/util/List;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    if-eqz v1, :cond_1

    .line 80
    .line 81
    check-cast v1, Ljava/lang/Iterable;

    .line 82
    .line 83
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    if-eqz v4, :cond_1

    .line 92
    .line 93
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 94
    .line 95
    .line 96
    move-result-object v4

    .line 97
    check-cast v4, Lcom/intsig/camscanner/pic2word/lr/LrSliceBean;

    .line 98
    .line 99
    new-instance v5, Landroid/graphics/Path;

    .line 100
    .line 101
    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 102
    .line 103
    .line 104
    invoke-virtual {v4}, Lcom/intsig/camscanner/pic2word/lr/LrSliceBean;->getPosition()Ljava/util/List;

    .line 105
    .line 106
    .line 107
    move-result-object v4

    .line 108
    if-eqz v4, :cond_2

    .line 109
    .line 110
    invoke-interface {v4}, Ljava/util/List;->size()I

    .line 111
    .line 112
    .line 113
    move-result v6

    .line 114
    const/16 v7, 0x8

    .line 115
    .line 116
    const/4 v8, 0x0

    .line 117
    if-ne v6, v7, :cond_3

    .line 118
    .line 119
    const/4 v6, 0x1

    .line 120
    goto :goto_2

    .line 121
    :cond_3
    const/4 v6, 0x0

    .line 122
    :goto_2
    if-eqz v6, :cond_4

    .line 123
    .line 124
    goto :goto_3

    .line 125
    :cond_4
    move-object v4, v3

    .line 126
    :goto_3
    if-eqz v4, :cond_2

    .line 127
    .line 128
    invoke-virtual {v5}, Landroid/graphics/Path;->reset()V

    .line 129
    .line 130
    .line 131
    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    move-result-object v6

    .line 135
    check-cast v6, Ljava/lang/Number;

    .line 136
    .line 137
    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    .line 138
    .line 139
    .line 140
    move-result v6

    .line 141
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 142
    .line 143
    .line 144
    move-result-object v7

    .line 145
    check-cast v7, Ljava/lang/Number;

    .line 146
    .line 147
    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    .line 148
    .line 149
    .line 150
    move-result v7

    .line 151
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 152
    .line 153
    .line 154
    const/4 v6, 0x2

    .line 155
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 156
    .line 157
    .line 158
    move-result-object v6

    .line 159
    check-cast v6, Ljava/lang/Number;

    .line 160
    .line 161
    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    .line 162
    .line 163
    .line 164
    move-result v6

    .line 165
    const/4 v7, 0x3

    .line 166
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 167
    .line 168
    .line 169
    move-result-object v7

    .line 170
    check-cast v7, Ljava/lang/Number;

    .line 171
    .line 172
    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    .line 173
    .line 174
    .line 175
    move-result v7

    .line 176
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 177
    .line 178
    .line 179
    const/4 v6, 0x4

    .line 180
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 181
    .line 182
    .line 183
    move-result-object v6

    .line 184
    check-cast v6, Ljava/lang/Number;

    .line 185
    .line 186
    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    .line 187
    .line 188
    .line 189
    move-result v6

    .line 190
    const/4 v7, 0x5

    .line 191
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 192
    .line 193
    .line 194
    move-result-object v7

    .line 195
    check-cast v7, Ljava/lang/Number;

    .line 196
    .line 197
    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    .line 198
    .line 199
    .line 200
    move-result v7

    .line 201
    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    .line 203
    .line 204
    const/4 v6, 0x6

    .line 205
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 206
    .line 207
    .line 208
    move-result-object v6

    .line 209
    check-cast v6, Ljava/lang/Number;

    .line 210
    .line 211
    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    .line 212
    .line 213
    .line 214
    move-result v6

    .line 215
    const/4 v7, 0x7

    .line 216
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 217
    .line 218
    .line 219
    move-result-object v4

    .line 220
    check-cast v4, Ljava/lang/Number;

    .line 221
    .line 222
    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    .line 223
    .line 224
    .line 225
    move-result v4

    .line 226
    invoke-virtual {v5, v6, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 227
    .line 228
    .line 229
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 230
    .line 231
    .line 232
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->ooOO()Landroid/graphics/Paint;

    .line 233
    .line 234
    .line 235
    move-result-object v4

    .line 236
    invoke-virtual {p1, v5, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 237
    .line 238
    .line 239
    goto/16 :goto_1

    .line 240
    .line 241
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 242
    .line 243
    .line 244
    return-void
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final o〇8〇()V
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMIsImageEdit()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->Oooo8o0〇()F

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, 0x1

    .line 17
    const/4 v2, 0x0

    .line 18
    const/4 v3, 0x0

    .line 19
    cmpg-float v0, v0, v3

    .line 20
    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/4 v0, 0x0

    .line 26
    :goto_0
    if-eqz v0, :cond_3

    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇〇808〇()F

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    cmpg-float v0, v0, v3

    .line 33
    .line 34
    if-nez v0, :cond_2

    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    goto :goto_1

    .line 38
    :cond_2
    const/4 v0, 0x0

    .line 39
    :goto_1
    if-eqz v0, :cond_3

    .line 40
    .line 41
    return-void

    .line 42
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->Ooo()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-nez v0, :cond_4

    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v0, p0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o0ooO(Lcom/intsig/camscanner/pic2word/lr/LrEditable;Z)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const-string v4, "drag"

    .line 60
    .line 61
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pic2word/lr/LrView;->oO(Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O880oOO08(Z)V

    .line 65
    .line 66
    .line 67
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 72
    .line 73
    .line 74
    move-result-object v4

    .line 75
    invoke-virtual {v4}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getScale()F

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    cmpg-float v4, v4, v3

    .line 80
    .line 81
    if-nez v4, :cond_5

    .line 82
    .line 83
    goto :goto_2

    .line 84
    :cond_5
    const/4 v1, 0x0

    .line 85
    :goto_2
    if-eqz v1, :cond_6

    .line 86
    .line 87
    const/high16 v1, 0x3f800000    # 1.0f

    .line 88
    .line 89
    goto :goto_3

    .line 90
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 91
    .line 92
    .line 93
    move-result-object v1

    .line 94
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getScale()F

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    :goto_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->Oooo8o0〇()F

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    div-float/2addr v2, v1

    .line 103
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇〇808〇()F

    .line 104
    .line 105
    .line 106
    move-result v4

    .line 107
    div-float/2addr v4, v1

    .line 108
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 109
    .line 110
    .line 111
    move-result-object v5

    .line 112
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    .line 113
    .line 114
    .line 115
    move-result v5

    .line 116
    int-to-float v5, v5

    .line 117
    div-float/2addr v5, v1

    .line 118
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 119
    .line 120
    .line 121
    move-result-object v6

    .line 122
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    .line 123
    .line 124
    .line 125
    move-result v6

    .line 126
    int-to-float v6, v6

    .line 127
    div-float/2addr v6, v1

    .line 128
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 129
    .line 130
    .line 131
    move-result-object v7

    .line 132
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    .line 133
    .line 134
    .line 135
    move-result v7

    .line 136
    int-to-float v7, v7

    .line 137
    div-float/2addr v7, v1

    .line 138
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 139
    .line 140
    .line 141
    move-result-object v8

    .line 142
    invoke-virtual {v8}, Landroid/view/View;->getBottom()I

    .line 143
    .line 144
    .line 145
    move-result v8

    .line 146
    int-to-float v8, v8

    .line 147
    div-float/2addr v8, v1

    .line 148
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 149
    .line 150
    add-float v9, v1, v2

    .line 151
    .line 152
    cmpg-float v9, v9, v5

    .line 153
    .line 154
    if-gez v9, :cond_7

    .line 155
    .line 156
    sub-float v2, v5, v1

    .line 157
    .line 158
    :cond_7
    iget v1, v0, Landroid/graphics/RectF;->top:F

    .line 159
    .line 160
    add-float v5, v1, v4

    .line 161
    .line 162
    cmpg-float v5, v5, v7

    .line 163
    .line 164
    if-gez v5, :cond_8

    .line 165
    .line 166
    sub-float v4, v7, v1

    .line 167
    .line 168
    :cond_8
    iget v1, v0, Landroid/graphics/RectF;->right:F

    .line 169
    .line 170
    add-float v5, v1, v2

    .line 171
    .line 172
    cmpl-float v5, v5, v6

    .line 173
    .line 174
    if-lez v5, :cond_9

    .line 175
    .line 176
    sub-float v2, v6, v1

    .line 177
    .line 178
    :cond_9
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 179
    .line 180
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇〇808〇()F

    .line 181
    .line 182
    .line 183
    move-result v5

    .line 184
    add-float/2addr v1, v5

    .line 185
    cmpl-float v1, v1, v8

    .line 186
    .line 187
    if-lez v1, :cond_a

    .line 188
    .line 189
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 190
    .line 191
    sub-float v4, v8, v1

    .line 192
    .line 193
    :cond_a
    iget v1, v0, Landroid/graphics/RectF;->left:F

    .line 194
    .line 195
    add-float/2addr v1, v2

    .line 196
    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 197
    .line 198
    iget v1, v0, Landroid/graphics/RectF;->right:F

    .line 199
    .line 200
    add-float/2addr v1, v2

    .line 201
    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 202
    .line 203
    iget v1, v0, Landroid/graphics/RectF;->top:F

    .line 204
    .line 205
    add-float/2addr v1, v4

    .line 206
    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 207
    .line 208
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 209
    .line 210
    add-float/2addr v1, v4

    .line 211
    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 212
    .line 213
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇o(F)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->o0ooO(F)V

    .line 217
    .line 218
    .line 219
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->Ooo()Z

    .line 220
    .line 221
    .line 222
    move-result v1

    .line 223
    if-eqz v1, :cond_b

    .line 224
    .line 225
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->o88〇OO08〇()Ljava/lang/Integer;

    .line 226
    .line 227
    .line 228
    move-result-object v1

    .line 229
    if-eqz v1, :cond_b

    .line 230
    .line 231
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 232
    .line 233
    .line 234
    move-result-object v1

    .line 235
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMLrUndoManagerNew()Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

    .line 236
    .line 237
    .line 238
    move-result-object v1

    .line 239
    if-eqz v1, :cond_b

    .line 240
    .line 241
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 242
    .line 243
    .line 244
    move-result-object v2

    .line 245
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getPageIndex()I

    .line 246
    .line 247
    .line 248
    move-result v2

    .line 249
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->o88〇OO08〇()Ljava/lang/Integer;

    .line 250
    .line 251
    .line 252
    move-result-object v3

    .line 253
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 257
    .line 258
    .line 259
    move-result v3

    .line 260
    invoke-virtual {v1, v0, v2, v3}, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;->〇O〇(Landroid/graphics/RectF;II)V

    .line 261
    .line 262
    .line 263
    :cond_b
    return-void
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final 〇〇00OO(Landroid/text/Layout;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMIsImageEdit()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    int-to-float v0, v0

    .line 17
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    cmpl-float v0, v0, v1

    .line 26
    .line 27
    if-lez v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    iget v1, v1, Landroid/graphics/RectF;->top:F

    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    int-to-float p1, p1

    .line 44
    add-float/2addr v1, p1

    .line 45
    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->ooo〇8oO()Landroid/graphics/RectF;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    if-eqz v0, :cond_3

    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    int-to-float v2, v2

    .line 67
    cmpl-float v1, v1, v2

    .line 68
    .line 69
    if-lez v1, :cond_3

    .line 70
    .line 71
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    int-to-float v1, v1

    .line 76
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    cmpg-float v1, v1, v2

    .line 81
    .line 82
    if-gez v1, :cond_2

    .line 83
    .line 84
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    goto :goto_0

    .line 93
    :cond_2
    invoke-virtual {p1}, Landroid/text/Layout;->getHeight()I

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    iget v1, v1, Landroid/graphics/RectF;->top:F

    .line 110
    .line 111
    invoke-virtual {p1}, Ljava/lang/Number;->floatValue()F

    .line 112
    .line 113
    .line 114
    move-result p1

    .line 115
    add-float/2addr v1, p1

    .line 116
    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 117
    .line 118
    :cond_3
    :goto_1
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public final O0oO008()Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇O8〇〇o(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O000()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O〇80o08O()Landroid/text/Layout;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->OoO8()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->〇〇00OO(Landroid/text/Layout;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 27
    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇OoOO〇()Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_2

    .line 34
    .line 35
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o0O〇8o0O(Landroid/graphics/Canvas;)V

    .line 36
    .line 37
    .line 38
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇8〇()V

    .line 39
    .line 40
    .line 41
    sget-object v2, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    iget-object v4, p0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 48
    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 50
    .line 51
    .line 52
    move-result-object v5

    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0o〇〇Oo()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v6

    .line 57
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->ooo〇8oO()Landroid/graphics/RectF;

    .line 58
    .line 59
    .line 60
    move-result-object v7

    .line 61
    const/4 v8, 0x0

    .line 62
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;->〇080(Landroid/text/Editable;Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;Landroid/graphics/RectF;Ljava/lang/String;Landroid/graphics/RectF;Z)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMIsImageEdit()Z

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    if-eqz v1, :cond_3

    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->o88〇OO08〇()Ljava/lang/Integer;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    if-eqz v1, :cond_3

    .line 80
    .line 81
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->Ooo8〇〇()Z

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    if-eqz v1, :cond_3

    .line 86
    .line 87
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->Oo0oO〇O〇O(Landroid/graphics/Canvas;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getOnlyDrawParaBg()Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-nez v0, :cond_6

    .line 99
    .line 100
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->o88〇OO08〇()Ljava/lang/Integer;

    .line 105
    .line 106
    .line 107
    move-result-object v2

    .line 108
    const/4 v3, 0x0

    .line 109
    const/4 v4, 0x1

    .line 110
    const/4 v5, 0x0

    .line 111
    const/4 v6, 0x0

    .line 112
    const/16 v7, 0x18

    .line 113
    .line 114
    const/4 v8, 0x0

    .line 115
    invoke-static/range {v1 .. v8}, Lcom/intsig/camscanner/pic2word/lr/LrView;->〇8o8o〇(Lcom/intsig/camscanner/pic2word/lr/LrView;Ljava/lang/Integer;ZZLjava/lang/Float;Ljava/lang/Float;ILjava/lang/Object;)V

    .line 116
    .line 117
    .line 118
    goto :goto_0

    .line 119
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMIsImageEdit()Z

    .line 124
    .line 125
    .line 126
    move-result v1

    .line 127
    if-eqz v1, :cond_4

    .line 128
    .line 129
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->Oo0oO〇O〇O(Landroid/graphics/Canvas;)V

    .line 130
    .line 131
    .line 132
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMIsImageEdit()Z

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    if-eqz v1, :cond_5

    .line 141
    .line 142
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getOnlyDrawParaBg()Z

    .line 147
    .line 148
    .line 149
    move-result v1

    .line 150
    if-nez v1, :cond_5

    .line 151
    .line 152
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 153
    .line 154
    .line 155
    move-result-object v2

    .line 156
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->o88〇OO08〇()Ljava/lang/Integer;

    .line 157
    .line 158
    .line 159
    move-result-object v3

    .line 160
    const/4 v4, 0x1

    .line 161
    const/4 v5, 0x1

    .line 162
    const/4 v6, 0x0

    .line 163
    const/4 v7, 0x0

    .line 164
    const/16 v8, 0x18

    .line 165
    .line 166
    const/4 v9, 0x0

    .line 167
    invoke-static/range {v2 .. v9}, Lcom/intsig/camscanner/pic2word/lr/LrView;->〇8o8o〇(Lcom/intsig/camscanner/pic2word/lr/LrView;Ljava/lang/Integer;ZZLjava/lang/Float;Ljava/lang/Float;ILjava/lang/Object;)V

    .line 168
    .line 169
    .line 170
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 171
    .line 172
    .line 173
    move-result-object v1

    .line 174
    iget v1, v1, Landroid/graphics/RectF;->left:F

    .line 175
    .line 176
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 177
    .line 178
    .line 179
    move-result-object v2

    .line 180
    iget v2, v2, Landroid/graphics/RectF;->top:F

    .line 181
    .line 182
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 183
    .line 184
    .line 185
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->OO8oO0o〇()Landroid/graphics/Path;

    .line 186
    .line 187
    .line 188
    move-result-object v1

    .line 189
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->o0O0()Landroid/graphics/Paint;

    .line 190
    .line 191
    .line 192
    move-result-object v2

    .line 193
    const/4 v3, 0x0

    .line 194
    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 195
    .line 196
    .line 197
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->o〇O(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    .line 199
    .line 200
    :cond_6
    :goto_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->oO00OOO(Landroid/graphics/Canvas;)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 204
    .line 205
    .line 206
    return-void

    .line 207
    :catchall_0
    move-exception p1

    .line 208
    goto :goto_1

    .line 209
    :catch_0
    move-exception v0

    .line 210
    :try_start_1
    const-string v1, "LrParaText"

    .line 211
    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    .line 213
    .line 214
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 215
    .line 216
    .line 217
    const-string v3, "textLayout draw isCrash:"

    .line 218
    .line 219
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 223
    .line 224
    .line 225
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v0

    .line 229
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    .line 231
    .line 232
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 233
    .line 234
    .line 235
    return-void

    .line 236
    :goto_1
    throw p1
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public o〇o()V
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    float-to-int v0, v0

    .line 10
    if-gtz v0, :cond_2

    .line 11
    .line 12
    new-instance v1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v2, "layout width = "

    .line 18
    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v0, ", is error!"

    .line 26
    .line 27
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const-string v2, "LrParaText"

    .line 35
    .line 36
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇O〇()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    if-eqz v1, :cond_0

    .line 44
    .line 45
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const/4 v1, 0x0

    .line 51
    :goto_0
    if-gtz v1, :cond_1

    .line 52
    .line 53
    const/16 v1, 0x64

    .line 54
    .line 55
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v4, "set new layout width = "

    .line 61
    .line 62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    move v6, v1

    .line 79
    goto :goto_1

    .line 80
    :cond_2
    move v6, v0

    .line 81
    :goto_1
    new-instance v0, Landroid/text/DynamicLayout;

    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 84
    .line 85
    .line 86
    move-result-object v4

    .line 87
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 88
    .line 89
    .line 90
    move-result-object v5

    .line 91
    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 92
    .line 93
    const/high16 v8, 0x3f800000    # 1.0f

    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇0()F

    .line 96
    .line 97
    .line 98
    move-result v9

    .line 99
    const/4 v10, 0x1

    .line 100
    move-object v3, v0

    .line 101
    invoke-direct/range {v3 .. v10}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->OOo0O(Landroid/text/Layout;)V

    .line 105
    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇O00()V
    .locals 21

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {v1}, Landroid/text/Editable;->clear()V

    .line 8
    .line 9
    .line 10
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-interface {v1}, Landroid/text/Editable;->clearSpans()V

    .line 15
    .line 16
    .line 17
    sget-object v2, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;

    .line 18
    .line 19
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    iget-object v4, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 24
    .line 25
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    const-string v5, "null cannot be cast to non-null type android.text.SpannableStringBuilder"

    .line 30
    .line 31
    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    move-object v5, v1

    .line 35
    check-cast v5, Landroid/text/SpannableStringBuilder;

    .line 36
    .line 37
    const/4 v6, 0x1

    .line 38
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMImageJsonParam()Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 43
    .line 44
    .line 45
    move-result-object v7

    .line 46
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;->OO0o〇〇(Landroid/text/TextPaint;Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;Landroid/text/SpannableStringBuilder;ZLcom/intsig/camscanner/pagelist/model/ImageJsonParam;)Landroid/text/SpannableStringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O〇0(Landroid/text/Spannable;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v1}, Landroid/graphics/Paint;->getTextSize()F

    .line 61
    .line 62
    .line 63
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->bottom:F

    .line 72
    .line 73
    float-to-int v1, v1

    .line 74
    iget-object v2, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 75
    .line 76
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    check-cast v2, Ljava/util/Collection;

    .line 81
    .line 82
    const/4 v3, 0x1

    .line 83
    const/4 v4, 0x0

    .line 84
    if-eqz v2, :cond_1

    .line 85
    .line 86
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    .line 87
    .line 88
    .line 89
    move-result v2

    .line 90
    if-eqz v2, :cond_0

    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_0
    const/4 v2, 0x0

    .line 94
    goto :goto_1

    .line 95
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 96
    :goto_1
    if-nez v2, :cond_c

    .line 97
    .line 98
    iget-object v2, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 99
    .line 100
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 105
    .line 106
    .line 107
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 108
    .line 109
    .line 110
    move-result-object v2

    .line 111
    check-cast v2, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;

    .line 112
    .line 113
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getEditParam()Lcom/intsig/camscanner/pic2word/lr/EditParam;

    .line 114
    .line 115
    .line 116
    move-result-object v5

    .line 117
    const/4 v6, 0x0

    .line 118
    if-eqz v5, :cond_2

    .line 119
    .line 120
    invoke-virtual {v5}, Lcom/intsig/camscanner/pic2word/lr/EditParam;->〇080()Landroid/text/Editable;

    .line 121
    .line 122
    .line 123
    move-result-object v5

    .line 124
    goto :goto_2

    .line 125
    :cond_2
    move-object v5, v6

    .line 126
    :goto_2
    if-eqz v5, :cond_3

    .line 127
    .line 128
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getEditParam()Lcom/intsig/camscanner/pic2word/lr/EditParam;

    .line 129
    .line 130
    .line 131
    move-result-object v5

    .line 132
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {v5}, Lcom/intsig/camscanner/pic2word/lr/EditParam;->〇080()Landroid/text/Editable;

    .line 136
    .line 137
    .line 138
    move-result-object v5

    .line 139
    const-string v7, "null cannot be cast to non-null type android.text.Editable"

    .line 140
    .line 141
    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇000〇〇08(Landroid/text/Editable;)V

    .line 145
    .line 146
    .line 147
    :cond_3
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getParaIndex()I

    .line 148
    .line 149
    .line 150
    move-result v5

    .line 151
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 152
    .line 153
    .line 154
    move-result-object v5

    .line 155
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0OO8〇0(Ljava/lang/Integer;)V

    .line 156
    .line 157
    .line 158
    iget-object v5, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 159
    .line 160
    invoke-virtual {v5}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_left()Ljava/lang/Number;

    .line 161
    .line 162
    .line 163
    move-result-object v5

    .line 164
    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    .line 165
    .line 166
    .line 167
    move-result v5

    .line 168
    iget-object v7, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 169
    .line 170
    invoke-virtual {v7}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_right()Ljava/lang/Number;

    .line 171
    .line 172
    .line 173
    move-result-object v7

    .line 174
    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    .line 175
    .line 176
    .line 177
    move-result v7

    .line 178
    sub-float/2addr v5, v7

    .line 179
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    .line 180
    .line 181
    .line 182
    move-result v5

    .line 183
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 184
    .line 185
    .line 186
    move-result-object v7

    .line 187
    invoke-virtual {v7}, Landroid/graphics/Paint;->getTextSize()F

    .line 188
    .line 189
    .line 190
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 191
    .line 192
    .line 193
    move-result-object v7

    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 195
    .line 196
    .line 197
    move-result-object v8

    .line 198
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v8

    .line 202
    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 203
    .line 204
    .line 205
    move-result v7

    .line 206
    iget-object v8, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 207
    .line 208
    invoke-virtual {v8}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_top()Ljava/lang/Number;

    .line 209
    .line 210
    .line 211
    move-result-object v8

    .line 212
    invoke-virtual {v8}, Ljava/lang/Number;->floatValue()F

    .line 213
    .line 214
    .line 215
    move-result v8

    .line 216
    iget-object v9, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 217
    .line 218
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_bottom()Ljava/lang/Number;

    .line 219
    .line 220
    .line 221
    move-result-object v9

    .line 222
    invoke-virtual {v9}, Ljava/lang/Number;->floatValue()F

    .line 223
    .line 224
    .line 225
    move-result v9

    .line 226
    sub-float/2addr v8, v9

    .line 227
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    .line 228
    .line 229
    .line 230
    move-result v8

    .line 231
    const/4 v9, 0x0

    .line 232
    cmpl-float v10, v5, v9

    .line 233
    .line 234
    if-lez v10, :cond_4

    .line 235
    .line 236
    div-float/2addr v7, v5

    .line 237
    float-to-int v7, v7

    .line 238
    goto :goto_3

    .line 239
    :cond_4
    const/4 v7, 0x0

    .line 240
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 241
    .line 242
    .line 243
    move-result-object v10

    .line 244
    const/4 v11, 0x2

    .line 245
    const-string v12, "\n"

    .line 246
    .line 247
    invoke-static {v10, v12, v4, v11, v6}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 248
    .line 249
    .line 250
    move-result v6

    .line 251
    if-nez v6, :cond_5

    .line 252
    .line 253
    const/4 v6, 0x1

    .line 254
    goto :goto_4

    .line 255
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 256
    .line 257
    .line 258
    move-result-object v13

    .line 259
    filled-new-array {v12}, [Ljava/lang/String;

    .line 260
    .line 261
    .line 262
    move-result-object v14

    .line 263
    const/4 v15, 0x0

    .line 264
    const/16 v16, 0x0

    .line 265
    .line 266
    const/16 v17, 0x6

    .line 267
    .line 268
    const/16 v18, 0x0

    .line 269
    .line 270
    invoke-static/range {v13 .. v18}, Lkotlin/text/StringsKt;->Oo(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    .line 271
    .line 272
    .line 273
    move-result-object v6

    .line 274
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 275
    .line 276
    .line 277
    move-result v6

    .line 278
    :goto_4
    if-ge v7, v6, :cond_6

    .line 279
    .line 280
    move v7, v6

    .line 281
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 282
    .line 283
    .line 284
    move-result-object v6

    .line 285
    invoke-virtual {v6}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 286
    .line 287
    .line 288
    move-result-object v6

    .line 289
    iget v6, v6, Landroid/graphics/Paint$FontMetrics;->leading:F

    .line 290
    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 292
    .line 293
    .line 294
    move-result-object v10

    .line 295
    invoke-virtual {v10}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 296
    .line 297
    .line 298
    move-result-object v10

    .line 299
    iget v10, v10, Landroid/graphics/Paint$FontMetrics;->descent:F

    .line 300
    .line 301
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 302
    .line 303
    .line 304
    move-result-object v11

    .line 305
    invoke-virtual {v11}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    .line 306
    .line 307
    .line 308
    move-result-object v11

    .line 309
    iget v11, v11, Landroid/graphics/Paint$FontMetrics;->ascent:F

    .line 310
    .line 311
    sub-float/2addr v10, v11

    .line 312
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getLine_gap()F

    .line 313
    .line 314
    .line 315
    move-result v11

    .line 316
    sub-float/2addr v11, v10

    .line 317
    sub-float/2addr v11, v6

    .line 318
    new-instance v20, Landroid/text/DynamicLayout;

    .line 319
    .line 320
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 321
    .line 322
    .line 323
    move-result-object v13

    .line 324
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0O8OO088()Landroid/text/TextPaint;

    .line 325
    .line 326
    .line 327
    move-result-object v14

    .line 328
    float-to-int v15, v5

    .line 329
    sget-object v16, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 330
    .line 331
    const/high16 v17, 0x3f800000    # 1.0f

    .line 332
    .line 333
    const/16 v18, 0x0

    .line 334
    .line 335
    const/16 v19, 0x1

    .line 336
    .line 337
    move-object/from16 v12, v20

    .line 338
    .line 339
    invoke-direct/range {v12 .. v19}, Landroid/text/DynamicLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 340
    .line 341
    .line 342
    invoke-virtual/range {v20 .. v20}, Landroid/text/Layout;->getHeight()I

    .line 343
    .line 344
    .line 345
    move-result v5

    .line 346
    int-to-float v5, v5

    .line 347
    cmpg-float v5, v5, v8

    .line 348
    .line 349
    if-gez v5, :cond_8

    .line 350
    .line 351
    invoke-virtual/range {v20 .. v20}, Landroid/text/Layout;->getHeight()I

    .line 352
    .line 353
    .line 354
    move-result v5

    .line 355
    int-to-float v5, v5

    .line 356
    sub-float v5, v8, v5

    .line 357
    .line 358
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getParagraph_gap()F

    .line 359
    .line 360
    .line 361
    move-result v2

    .line 362
    sub-float/2addr v5, v2

    .line 363
    int-to-float v2, v1

    .line 364
    sub-float/2addr v5, v2

    .line 365
    if-lez v7, :cond_7

    .line 366
    .line 367
    int-to-float v2, v7

    .line 368
    const/high16 v12, 0x3f000000    # 0.5f

    .line 369
    .line 370
    add-float/2addr v2, v12

    .line 371
    goto :goto_5

    .line 372
    :cond_7
    const/high16 v2, 0x3fc00000    # 1.5f

    .line 373
    .line 374
    :goto_5
    div-float/2addr v5, v2

    .line 375
    invoke-static {v11, v5}, Lkotlin/ranges/RangesKt;->〇o00〇〇Oo(FF)F

    .line 376
    .line 377
    .line 378
    move-result v11

    .line 379
    :cond_8
    invoke-virtual/range {v20 .. v20}, Landroid/text/Layout;->getHeight()I

    .line 380
    .line 381
    .line 382
    move-result v2

    .line 383
    int-to-float v2, v2

    .line 384
    cmpl-float v2, v2, v8

    .line 385
    .line 386
    if-gtz v2, :cond_9

    .line 387
    .line 388
    cmpg-float v2, v11, v9

    .line 389
    .line 390
    if-gez v2, :cond_a

    .line 391
    .line 392
    :cond_9
    const/4 v11, 0x0

    .line 393
    :cond_a
    cmpl-float v2, v11, v9

    .line 394
    .line 395
    if-lez v2, :cond_b

    .line 396
    .line 397
    move v9, v11

    .line 398
    :cond_b
    invoke-virtual {v0, v9}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O〇08(F)V

    .line 399
    .line 400
    .line 401
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇0()F

    .line 402
    .line 403
    .line 404
    move-result v2

    .line 405
    new-instance v5, Ljava/lang/StringBuilder;

    .line 406
    .line 407
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 408
    .line 409
    .line 410
    const-string v8, "mLineExtra:"

    .line 411
    .line 412
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    .line 414
    .line 415
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 416
    .line 417
    .line 418
    const-string v2, ", lineNum:"

    .line 419
    .line 420
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    .line 422
    .line 423
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 424
    .line 425
    .line 426
    const-string v2, " ,lineGap:"

    .line 427
    .line 428
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    .line 430
    .line 431
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 432
    .line 433
    .line 434
    const-string v2, " ,txtH\uff1a"

    .line 435
    .line 436
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    .line 438
    .line 439
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 440
    .line 441
    .line 442
    const-string v2, ", txtLeading:"

    .line 443
    .line 444
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .line 446
    .line 447
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 448
    .line 449
    .line 450
    const-string v2, " "

    .line 451
    .line 452
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    .line 454
    .line 455
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 456
    .line 457
    .line 458
    move-result-object v2

    .line 459
    const-string v5, "LrParaText"

    .line 460
    .line 461
    invoke-static {v5, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    .line 463
    .line 464
    :cond_c
    sget-object v2, Lcom/intsig/camscanner/pic2word/lr/LrView;->oO〇8O8oOo:Lcom/intsig/camscanner/pic2word/lr/LrView$Companion;

    .line 465
    .line 466
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 467
    .line 468
    .line 469
    move-result-object v5

    .line 470
    invoke-virtual {v5}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMIsImageEdit()Z

    .line 471
    .line 472
    .line 473
    move-result v5

    .line 474
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/pic2word/lr/LrView$Companion;->〇080(Z)F

    .line 475
    .line 476
    .line 477
    move-result v2

    .line 478
    new-instance v5, Landroid/graphics/RectF;

    .line 479
    .line 480
    iget-object v6, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 481
    .line 482
    invoke-virtual {v6}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_left()Ljava/lang/Number;

    .line 483
    .line 484
    .line 485
    move-result-object v6

    .line 486
    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    .line 487
    .line 488
    .line 489
    move-result v6

    .line 490
    add-float/2addr v6, v2

    .line 491
    iget-object v7, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 492
    .line 493
    invoke-virtual {v7}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_top()Ljava/lang/Number;

    .line 494
    .line 495
    .line 496
    move-result-object v7

    .line 497
    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    .line 498
    .line 499
    .line 500
    move-result v7

    .line 501
    add-float/2addr v7, v2

    .line 502
    iget-object v8, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 503
    .line 504
    invoke-virtual {v8}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_right()Ljava/lang/Number;

    .line 505
    .line 506
    .line 507
    move-result-object v8

    .line 508
    invoke-virtual {v8}, Ljava/lang/Number;->floatValue()F

    .line 509
    .line 510
    .line 511
    move-result v8

    .line 512
    add-float/2addr v8, v2

    .line 513
    iget-object v9, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 514
    .line 515
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_bottom()Ljava/lang/Number;

    .line 516
    .line 517
    .line 518
    move-result-object v9

    .line 519
    invoke-virtual {v9}, Ljava/lang/Number;->floatValue()F

    .line 520
    .line 521
    .line 522
    move-result v9

    .line 523
    int-to-float v1, v1

    .line 524
    add-float/2addr v9, v1

    .line 525
    add-float/2addr v9, v2

    .line 526
    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 527
    .line 528
    .line 529
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 530
    .line 531
    .line 532
    move-result-object v1

    .line 533
    invoke-virtual {v1, v5}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 534
    .line 535
    .line 536
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇o0O0O8()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 537
    .line 538
    .line 539
    move-result-object v1

    .line 540
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMLrUndoManagerNew()Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

    .line 541
    .line 542
    .line 543
    move-result-object v1

    .line 544
    if-eqz v1, :cond_11

    .line 545
    .line 546
    iget-object v1, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 547
    .line 548
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 549
    .line 550
    .line 551
    move-result-object v1

    .line 552
    check-cast v1, Ljava/util/Collection;

    .line 553
    .line 554
    if-eqz v1, :cond_e

    .line 555
    .line 556
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 557
    .line 558
    .line 559
    move-result v1

    .line 560
    if-eqz v1, :cond_d

    .line 561
    .line 562
    goto :goto_6

    .line 563
    :cond_d
    const/4 v3, 0x0

    .line 564
    :cond_e
    :goto_6
    if-nez v3, :cond_11

    .line 565
    .line 566
    iget-object v1, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 567
    .line 568
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 569
    .line 570
    .line 571
    move-result-object v1

    .line 572
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 573
    .line 574
    .line 575
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 576
    .line 577
    .line 578
    move-result-object v1

    .line 579
    check-cast v1, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;

    .line 580
    .line 581
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getEditParam()Lcom/intsig/camscanner/pic2word/lr/EditParam;

    .line 582
    .line 583
    .line 584
    move-result-object v1

    .line 585
    if-eqz v1, :cond_f

    .line 586
    .line 587
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/EditParam;->〇o00〇〇Oo()Landroid/graphics/RectF;

    .line 588
    .line 589
    .line 590
    move-result-object v1

    .line 591
    if-eqz v1, :cond_f

    .line 592
    .line 593
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 594
    .line 595
    .line 596
    move-result-object v2

    .line 597
    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 598
    .line 599
    .line 600
    :cond_f
    iget-object v1, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 601
    .line 602
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 603
    .line 604
    .line 605
    move-result-object v1

    .line 606
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 607
    .line 608
    .line 609
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 610
    .line 611
    .line 612
    move-result-object v1

    .line 613
    check-cast v1, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;

    .line 614
    .line 615
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getDefaultParam()Lcom/intsig/camscanner/pic2word/lr/DefaultParam;

    .line 616
    .line 617
    .line 618
    move-result-object v1

    .line 619
    if-eqz v1, :cond_10

    .line 620
    .line 621
    iget-object v1, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 622
    .line 623
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 624
    .line 625
    .line 626
    move-result-object v1

    .line 627
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 628
    .line 629
    .line 630
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 631
    .line 632
    .line 633
    move-result-object v1

    .line 634
    check-cast v1, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;

    .line 635
    .line 636
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getDefaultParam()Lcom/intsig/camscanner/pic2word/lr/DefaultParam;

    .line 637
    .line 638
    .line 639
    move-result-object v1

    .line 640
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 641
    .line 642
    .line 643
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/DefaultParam;->getText()Ljava/lang/String;

    .line 644
    .line 645
    .line 646
    move-result-object v1

    .line 647
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O〇OO(Ljava/lang/String;)V

    .line 648
    .line 649
    .line 650
    iget-object v1, v0, Lcom/intsig/camscanner/pic2word/lr/LrParaText;->o〇O:Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 651
    .line 652
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 653
    .line 654
    .line 655
    move-result-object v1

    .line 656
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 657
    .line 658
    .line 659
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 660
    .line 661
    .line 662
    move-result-object v1

    .line 663
    check-cast v1, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;

    .line 664
    .line 665
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getDefaultParam()Lcom/intsig/camscanner/pic2word/lr/DefaultParam;

    .line 666
    .line 667
    .line 668
    move-result-object v1

    .line 669
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 670
    .line 671
    .line 672
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/DefaultParam;->getBound()Landroid/graphics/RectF;

    .line 673
    .line 674
    .line 675
    move-result-object v1

    .line 676
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O〇Oooo〇〇(Landroid/graphics/RectF;)V

    .line 677
    .line 678
    .line 679
    goto :goto_7

    .line 680
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O()Landroid/text/Editable;

    .line 681
    .line 682
    .line 683
    move-result-object v1

    .line 684
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 685
    .line 686
    .line 687
    move-result-object v1

    .line 688
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O〇OO(Ljava/lang/String;)V

    .line 689
    .line 690
    .line 691
    new-instance v1, Landroid/graphics/RectF;

    .line 692
    .line 693
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 694
    .line 695
    .line 696
    iget v2, v5, Landroid/graphics/RectF;->top:F

    .line 697
    .line 698
    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 699
    .line 700
    iget v2, v5, Landroid/graphics/RectF;->bottom:F

    .line 701
    .line 702
    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 703
    .line 704
    iget v2, v5, Landroid/graphics/RectF;->left:F

    .line 705
    .line 706
    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 707
    .line 708
    iget v2, v5, Landroid/graphics/RectF;->right:F

    .line 709
    .line 710
    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 711
    .line 712
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O〇Oooo〇〇(Landroid/graphics/RectF;)V

    .line 713
    .line 714
    .line 715
    :cond_11
    :goto_7
    return-void
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
.end method

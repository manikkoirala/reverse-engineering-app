.class public final enum Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;
.super Ljava/lang/Enum;
.source "LrSegmentUtils.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ParaScene"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

.field public static final enum PAPER:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

.field public static final enum TABPLACE:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;


# instance fields
.field private final value:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;
    .locals 3

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->PAPER:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->TABPLACE:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "paper"

    .line 5
    .line 6
    const-string v3, "PAPER"

    .line 7
    .line 8
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sput-object v0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->PAPER:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    const-string v2, "tabplace"

    .line 17
    .line 18
    const-string v3, "TABPLACE"

    .line 19
    .line 20
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sput-object v0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->TABPLACE:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->$values()[Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    sput-object v0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->$VALUES:[Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->value:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->$VALUES:[Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils$ParaScene;->value:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;
.super Ljava/lang/Object;
.source "LrUndoManagerNew.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "EditHistory"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:F

.field final synthetic OO0o〇〇:Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

.field private OO0o〇〇〇〇0:I

.field private Oo08:F

.field private final oO80:Landroid/text/SpannableStringBuilder;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇0:I

.field private final 〇080:Landroid/text/Editable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇80〇808〇O:I

.field private 〇8o8o〇:Ljava/lang/Float;

.field private 〇O8o08O:Ljava/lang/Float;

.field private 〇o00〇〇Oo:F

.field private 〇o〇:F

.field private final 〇〇888:I


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;Landroid/text/Editable;FFFFII)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/Editable;",
            "FFFFII)V"
        }
    .end annotation

    .line 1
    const-string v0, "src"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->OO0o〇〇:Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

    .line 7
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇080:Landroid/text/Editable;

    .line 12
    .line 13
    iput p3, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇o00〇〇Oo:F

    .line 14
    .line 15
    iput p4, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇o〇:F

    .line 16
    .line 17
    iput p5, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->O8:F

    .line 18
    .line 19
    iput p6, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->Oo08:F

    .line 20
    .line 21
    iput p7, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->o〇0:I

    .line 22
    .line 23
    iput p8, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇〇888:I

    .line 24
    .line 25
    new-instance p1, Landroid/text/SpannableStringBuilder;

    .line 26
    .line 27
    invoke-direct {p1, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 28
    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->oO80:Landroid/text/SpannableStringBuilder;

    .line 31
    .line 32
    invoke-static {p2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->OO0o〇〇〇〇0:I

    .line 37
    .line 38
    if-gez p1, :cond_0

    .line 39
    .line 40
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->OO0o〇〇〇〇0:I

    .line 45
    .line 46
    :cond_0
    invoke-static {p2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇80〇808〇O:I

    .line 51
    .line 52
    if-gez p1, :cond_1

    .line 53
    .line 54
    iget p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->OO0o〇〇〇〇0:I

    .line 55
    .line 56
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇80〇808〇O:I

    .line 57
    .line 58
    :cond_1
    iget p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇o〇:F

    .line 59
    .line 60
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇8o8o〇:Ljava/lang/Float;

    .line 65
    .line 66
    iget p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇o00〇〇Oo:F

    .line 67
    .line 68
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇O8o08O:Ljava/lang/Float;

    .line 73
    .line 74
    iget-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇8o8o〇:Ljava/lang/Float;

    .line 75
    .line 76
    new-instance p3, Ljava/lang/StringBuilder;

    .line 77
    .line 78
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .line 80
    .line 81
    const-string p4, "oldTop:"

    .line 82
    .line 83
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    const-string p2, ", oldLeft:"

    .line 90
    .line 91
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    const-string p2, "LrUndoManagerNew"

    .line 102
    .line 103
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇8o8o〇(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method


# virtual methods
.method public final O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇〇888:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO0o〇〇〇〇0(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->Oo08:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo08()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->O8:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇8o8o〇(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->Oo08:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇080()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇o00〇〇Oo:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇808〇O(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->O8:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇8o8o〇(Z)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇080:Landroid/text/Editable;

    .line 4
    .line 5
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    iget-object v3, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->oO80:Landroid/text/SpannableStringBuilder;

    .line 10
    .line 11
    const/4 v4, 0x0

    .line 12
    invoke-interface {v1, v4, v2, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 13
    .line 14
    .line 15
    iget-object v1, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇080:Landroid/text/Editable;

    .line 16
    .line 17
    iget v2, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->OO0o〇〇〇〇0:I

    .line 18
    .line 19
    invoke-static {v1, v2, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 20
    .line 21
    .line 22
    iget-object v1, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->OO0o〇〇:Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

    .line 23
    .line 24
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;->〇o00〇〇Oo(Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;)Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    if-eqz v1, :cond_b

    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    iget v3, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->o〇0:I

    .line 39
    .line 40
    if-le v2, v3, :cond_b

    .line 41
    .line 42
    if-ltz v3, :cond_b

    .line 43
    .line 44
    iget v2, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇〇888:I

    .line 45
    .line 46
    if-ltz v2, :cond_b

    .line 47
    .line 48
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    iget v3, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->o〇0:I

    .line 53
    .line 54
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    instance-of v2, v2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 59
    .line 60
    if-eqz v2, :cond_b

    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    iget v3, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->o〇0:I

    .line 67
    .line 68
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    const-string v3, "null cannot be cast to non-null type com.intsig.camscanner.pagelist.adapter.word.data.WordListImageItem"

    .line 73
    .line 74
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    check-cast v2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 78
    .line 79
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;->getPageImage()Lcom/intsig/camscanner/loadimage/PageImage;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    const/4 v3, 0x0

    .line 88
    if-eqz v2, :cond_0

    .line 89
    .line 90
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrImageJson;->getPages()Ljava/util/List;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    goto :goto_0

    .line 95
    :cond_0
    move-object v2, v3

    .line 96
    :goto_0
    move-object v5, v2

    .line 97
    check-cast v5, Ljava/util/Collection;

    .line 98
    .line 99
    const/4 v6, 0x1

    .line 100
    if-eqz v5, :cond_2

    .line 101
    .line 102
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    .line 103
    .line 104
    .line 105
    move-result v5

    .line 106
    if-eqz v5, :cond_1

    .line 107
    .line 108
    goto :goto_1

    .line 109
    :cond_1
    const/4 v5, 0x0

    .line 110
    goto :goto_2

    .line 111
    :cond_2
    :goto_1
    const/4 v5, 0x1

    .line 112
    :goto_2
    if-eqz v5, :cond_3

    .line 113
    .line 114
    return-void

    .line 115
    :cond_3
    sget-object v5, Lcom/intsig/camscanner/pic2word/lr/WordDataUtils;->〇080:Lcom/intsig/camscanner/pic2word/lr/WordDataUtils;

    .line 116
    .line 117
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    check-cast v2, Lcom/intsig/camscanner/pic2word/lr/LrPageBean;

    .line 122
    .line 123
    const/4 v7, 0x2

    .line 124
    invoke-static {v5, v2, v4, v7, v3}, Lcom/intsig/camscanner/pic2word/lr/WordDataUtils;->O8(Lcom/intsig/camscanner/pic2word/lr/WordDataUtils;Lcom/intsig/camscanner/pic2word/lr/LrPageBean;ZILjava/lang/Object;)Ljava/util/List;

    .line 125
    .line 126
    .line 127
    move-result-object v2

    .line 128
    check-cast v2, Ljava/lang/Iterable;

    .line 129
    .line 130
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 131
    .line 132
    .line 133
    move-result-object v2

    .line 134
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 135
    .line 136
    .line 137
    move-result v3

    .line 138
    if-eqz v3, :cond_b

    .line 139
    .line 140
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 141
    .line 142
    .line 143
    move-result-object v3

    .line 144
    move-object v9, v3

    .line 145
    check-cast v9, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;

    .line 146
    .line 147
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getType()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v3

    .line 151
    const-string v5, "PARA"

    .line 152
    .line 153
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 154
    .line 155
    .line 156
    move-result v3

    .line 157
    if-eqz v3, :cond_4

    .line 158
    .line 159
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 160
    .line 161
    .line 162
    move-result-object v3

    .line 163
    check-cast v3, Ljava/util/Collection;

    .line 164
    .line 165
    if-eqz v3, :cond_6

    .line 166
    .line 167
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 168
    .line 169
    .line 170
    move-result v3

    .line 171
    if-eqz v3, :cond_5

    .line 172
    .line 173
    goto :goto_3

    .line 174
    :cond_5
    const/4 v3, 0x0

    .line 175
    goto :goto_4

    .line 176
    :cond_6
    :goto_3
    const/4 v3, 0x1

    .line 177
    :goto_4
    if-nez v3, :cond_4

    .line 178
    .line 179
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getParas()Ljava/util/List;

    .line 180
    .line 181
    .line 182
    move-result-object v3

    .line 183
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 184
    .line 185
    .line 186
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 187
    .line 188
    .line 189
    move-result-object v3

    .line 190
    check-cast v3, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;

    .line 191
    .line 192
    sget-object v5, Lcom/intsig/camscanner/pic2word/lr/LrView;->oO〇8O8oOo:Lcom/intsig/camscanner/pic2word/lr/LrView$Companion;

    .line 193
    .line 194
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/pic2word/lr/LrView$Companion;->〇080(Z)F

    .line 195
    .line 196
    .line 197
    move-result v5

    .line 198
    invoke-virtual {v3}, Lcom/intsig/camscanner/pic2word/lr/LrParaBean;->getParaIndex()I

    .line 199
    .line 200
    .line 201
    move-result v3

    .line 202
    iget v7, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇〇888:I

    .line 203
    .line 204
    if-ne v3, v7, :cond_4

    .line 205
    .line 206
    const-string v2, "LrUndoManagerNew"

    .line 207
    .line 208
    const-string v3, "undo refresh"

    .line 209
    .line 210
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    iget-object v2, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇O8o08O:Ljava/lang/Float;

    .line 214
    .line 215
    if-eqz v2, :cond_a

    .line 216
    .line 217
    iget-object v3, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇8o8o〇:Ljava/lang/Float;

    .line 218
    .line 219
    if-nez v3, :cond_7

    .line 220
    .line 221
    goto :goto_6

    .line 222
    :cond_7
    if-eqz p1, :cond_8

    .line 223
    .line 224
    iget v2, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->O8:F

    .line 225
    .line 226
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 227
    .line 228
    .line 229
    move-result-object v2

    .line 230
    :cond_8
    if-eqz p1, :cond_9

    .line 231
    .line 232
    iget v3, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->Oo08:F

    .line 233
    .line 234
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 235
    .line 236
    .line 237
    move-result-object v3

    .line 238
    goto :goto_5

    .line 239
    :cond_9
    iget-object v3, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇8o8o〇:Ljava/lang/Float;

    .line 240
    .line 241
    :goto_5
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 242
    .line 243
    .line 244
    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    .line 245
    .line 246
    .line 247
    move-result v4

    .line 248
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_left()Ljava/lang/Number;

    .line 249
    .line 250
    .line 251
    move-result-object v6

    .line 252
    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    .line 253
    .line 254
    .line 255
    move-result v6

    .line 256
    sub-float/2addr v4, v6

    .line 257
    sub-float/2addr v4, v5

    .line 258
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 259
    .line 260
    .line 261
    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    .line 262
    .line 263
    .line 264
    move-result v6

    .line 265
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_top()Ljava/lang/Number;

    .line 266
    .line 267
    .line 268
    move-result-object v7

    .line 269
    invoke-virtual {v7}, Ljava/lang/Number;->floatValue()F

    .line 270
    .line 271
    .line 272
    move-result v7

    .line 273
    sub-float/2addr v6, v7

    .line 274
    sub-float/2addr v6, v5

    .line 275
    new-instance v10, Landroid/graphics/RectF;

    .line 276
    .line 277
    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    .line 278
    .line 279
    .line 280
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 281
    .line 282
    .line 283
    move-result v2

    .line 284
    iput v2, v10, Landroid/graphics/RectF;->left:F

    .line 285
    .line 286
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 287
    .line 288
    .line 289
    move-result v2

    .line 290
    iput v2, v10, Landroid/graphics/RectF;->top:F

    .line 291
    .line 292
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_right()Ljava/lang/Number;

    .line 293
    .line 294
    .line 295
    move-result-object v2

    .line 296
    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    .line 297
    .line 298
    .line 299
    move-result v2

    .line 300
    add-float/2addr v2, v4

    .line 301
    iput v2, v10, Landroid/graphics/RectF;->right:F

    .line 302
    .line 303
    invoke-virtual {v9}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;->getBox_bottom()Ljava/lang/Number;

    .line 304
    .line 305
    .line 306
    move-result-object v2

    .line 307
    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    .line 308
    .line 309
    .line 310
    move-result v2

    .line 311
    add-float/2addr v2, v6

    .line 312
    iput v2, v10, Landroid/graphics/RectF;->bottom:F

    .line 313
    .line 314
    sget-object v7, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;

    .line 315
    .line 316
    iget-object v8, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->oO80:Landroid/text/SpannableStringBuilder;

    .line 317
    .line 318
    const/4 v11, 0x0

    .line 319
    const/4 v12, 0x0

    .line 320
    const/4 v13, 0x0

    .line 321
    const/16 v14, 0x38

    .line 322
    .line 323
    const/4 v15, 0x0

    .line 324
    invoke-static/range {v7 .. v15}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;->〇o00〇〇Oo(Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;Landroid/text/Editable;Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;Landroid/graphics/RectF;Ljava/lang/String;Landroid/graphics/RectF;ZILjava/lang/Object;)V

    .line 325
    .line 326
    .line 327
    goto :goto_7

    .line 328
    :cond_a
    :goto_6
    sget-object v7, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;

    .line 329
    .line 330
    iget-object v8, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->oO80:Landroid/text/SpannableStringBuilder;

    .line 331
    .line 332
    const/4 v10, 0x0

    .line 333
    const/4 v11, 0x0

    .line 334
    const/4 v12, 0x0

    .line 335
    const/4 v13, 0x0

    .line 336
    const/16 v14, 0x38

    .line 337
    .line 338
    const/4 v15, 0x0

    .line 339
    invoke-static/range {v7 .. v15}, Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;->〇o00〇〇Oo(Lcom/intsig/camscanner/pic2word/lr/LrSegmentUtils;Landroid/text/Editable;Lcom/intsig/camscanner/pic2word/lr/LrSegmentBean;Landroid/graphics/RectF;Ljava/lang/String;Landroid/graphics/RectF;ZILjava/lang/Object;)V

    .line 340
    .line 341
    .line 342
    :goto_7
    iget v2, v0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->o〇0:I

    .line 343
    .line 344
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 345
    .line 346
    .line 347
    :cond_b
    return-void
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇o00〇〇Oo()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇o〇:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->o〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇888()Landroid/text/Editable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew$EditHistory;->〇080:Landroid/text/Editable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

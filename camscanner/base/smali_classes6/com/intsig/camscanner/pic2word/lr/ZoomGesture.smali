.class public final Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;
.super Ljava/lang/Object;
.source "ZoomGesture.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;,
        Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇00:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇:F

.field private final OO0o〇〇〇〇0:[F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OoO8:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$scaleGestureListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oooo8o0〇:Landroid/animation/Animator;

.field private final o800o8O:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$gestureListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO80:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oo88o8O:Landroid/view/ScaleGestureDetector;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇0:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇O8〇〇o:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0〇O0088o:[F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:[F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8o8o〇:Landroid/widget/OverScroller;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O00:F

.field private final 〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O8o08O:F

.field private 〇O〇:F

.field private final 〇o00〇〇Oo:F

.field private final 〇oo〇:Landroid/view/GestureDetector;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:F

.field private 〇〇808〇:F

.field private final 〇〇888:Landroid/graphics/Matrix;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇8O0〇8:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇00:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/pic2word/lr/LrView;)V
    .locals 10
    .param p1    # Lcom/intsig/camscanner/pic2word/lr/LrView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 10
    .line 11
    const/high16 v0, 0x3f800000    # 1.0f

    .line 12
    .line 13
    iput v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o00〇〇Oo:F

    .line 14
    .line 15
    const/high16 v1, 0x40800000    # 4.0f

    .line 16
    .line 17
    iput v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o〇:F

    .line 18
    .line 19
    new-instance v1, Landroid/graphics/Matrix;

    .line 20
    .line 21
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8:Landroid/graphics/Matrix;

    .line 25
    .line 26
    new-instance v1, Landroid/graphics/Matrix;

    .line 27
    .line 28
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 29
    .line 30
    .line 31
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oo08:Landroid/graphics/Matrix;

    .line 32
    .line 33
    new-instance v1, Landroid/graphics/Matrix;

    .line 34
    .line 35
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 36
    .line 37
    .line 38
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇0:Landroid/graphics/Matrix;

    .line 39
    .line 40
    new-instance v1, Landroid/graphics/Matrix;

    .line 41
    .line 42
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 43
    .line 44
    .line 45
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 46
    .line 47
    new-instance v1, Landroid/graphics/Matrix;

    .line 48
    .line 49
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 50
    .line 51
    .line 52
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->oO80:Landroid/graphics/Matrix;

    .line 53
    .line 54
    const/16 v1, 0x9

    .line 55
    .line 56
    new-array v2, v1, [F

    .line 57
    .line 58
    iput-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 59
    .line 60
    new-array v1, v1, [F

    .line 61
    .line 62
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->OO0o〇〇〇〇0:[F

    .line 63
    .line 64
    new-instance v1, Landroid/widget/OverScroller;

    .line 65
    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 71
    .line 72
    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 73
    .line 74
    .line 75
    invoke-direct {v1, v2, v3}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 76
    .line 77
    .line 78
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇8o8o〇:Landroid/widget/OverScroller;

    .line 79
    .line 80
    iput v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O00:F

    .line 81
    .line 82
    const/4 v0, 0x2

    .line 83
    new-array v0, v0, [F

    .line 84
    .line 85
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇0〇O0088o:[F

    .line 86
    .line 87
    new-instance v0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$scaleGestureListener$1;

    .line 88
    .line 89
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$scaleGestureListener$1;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)V

    .line 90
    .line 91
    .line 92
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->OoO8:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$scaleGestureListener$1;

    .line 93
    .line 94
    new-instance v1, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$gestureListener$1;

    .line 95
    .line 96
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$gestureListener$1;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;Lcom/intsig/camscanner/pic2word/lr/LrView;)V

    .line 97
    .line 98
    .line 99
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o800o8O:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$gestureListener$1;

    .line 100
    .line 101
    new-instance v9, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 102
    .line 103
    const/4 v3, 0x0

    .line 104
    const/4 v4, 0x0

    .line 105
    const/4 v5, 0x0

    .line 106
    const/4 v6, 0x0

    .line 107
    const/16 v7, 0xf

    .line 108
    .line 109
    const/4 v8, 0x0

    .line 110
    move-object v2, v9

    .line 111
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;-><init>(FFFFILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 112
    .line 113
    .line 114
    iput-object v9, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 115
    .line 116
    new-instance v2, Landroid/view/ScaleGestureDetector;

    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 119
    .line 120
    .line 121
    move-result-object v3

    .line 122
    invoke-direct {v2, v3, v0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 123
    .line 124
    .line 125
    iput-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->oo88o8O:Landroid/view/ScaleGestureDetector;

    .line 126
    .line 127
    new-instance v0, Landroid/view/GestureDetector;

    .line 128
    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 134
    .line 135
    .line 136
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇oo〇:Landroid/view/GestureDetector;

    .line 137
    .line 138
    new-instance p1, Landroid/graphics/Matrix;

    .line 139
    .line 140
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 141
    .line 142
    .line 143
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇O8〇〇o:Landroid/graphics/Matrix;

    .line 144
    .line 145
    return-void
    .line 146
    .line 147
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O8o08O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O8ooOoo〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)F
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇00()F

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)Landroid/graphics/Matrix;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->OO0o〇〇:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O8o08O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o0ooO([F)[F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->oO80:Landroid/graphics/Matrix;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->oO80:Landroid/graphics/Matrix;

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 11
    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o800o8O(F)Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMPageWidth()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o00〇〇Oo:F

    .line 10
    .line 11
    mul-float v1, v1, v2

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMPageWidth()F

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    mul-float v2, v2, p1

    .line 20
    .line 21
    sub-float/2addr v1, v2

    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇〇888(F)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 26
    .line 27
    const/4 v1, 0x0

    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->Oo08(F)V

    .line 29
    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇808〇:F

    .line 32
    .line 33
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O00:F

    .line 34
    .line 35
    div-float/2addr v0, v1

    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 37
    .line 38
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 39
    .line 40
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMPageHeight()F

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o00〇〇Oo:F

    .line 45
    .line 46
    mul-float v2, v2, v3

    .line 47
    .line 48
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 49
    .line 50
    invoke-virtual {v3}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMPageHeight()F

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    mul-float v3, v3, p1

    .line 55
    .line 56
    sub-float/2addr v2, v3

    .line 57
    sub-float/2addr v2, v0

    .line 58
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->oO80(F)V

    .line 59
    .line 60
    .line 61
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 62
    .line 63
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->o〇0(F)V

    .line 64
    .line 65
    .line 66
    iget p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇808〇:F

    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 69
    .line 70
    new-instance v2, Ljava/lang/StringBuilder;

    .line 71
    .line 72
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .line 74
    .line 75
    const-string v3, "mKeyBoardHeight: "

    .line 76
    .line 77
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    const-string p1, ", maxY: "

    .line 84
    .line 85
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string p1, ", computeScrollXY: "

    .line 92
    .line 93
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    const-string v0, "ZoomGesture"

    .line 104
    .line 105
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 109
    .line 110
    return-object p1
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic oO80(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o00〇〇Oo:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)[F
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇0〇O0088o:[F

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇00()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇0:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    aget v0, v0, v1

    .line 12
    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇00〇8(FFF)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oooo8o0〇:Landroid/animation/Animator;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇00()F

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x2

    .line 13
    new-array v1, v1, [F

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    aput v0, v1, v2

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    aput p1, v1, v0

    .line 20
    .line 21
    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    const-wide/16 v0, 0x118

    .line 26
    .line 27
    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 32
    .line 33
    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oooo8o0〇:Landroid/animation/Animator;

    .line 40
    .line 41
    new-instance v0, LO8〇o0〇〇8/o〇0;

    .line 42
    .line 43
    invoke-direct {v0, p0, p2, p3}, LO8〇o0〇〇8/o〇0;-><init>(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;FF)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 47
    .line 48
    .line 49
    new-instance p2, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$startZoomAnim$2;

    .line 50
    .line 51
    invoke-direct {p2}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$startZoomAnim$2;-><init>()V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p1, p2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;FFLandroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;FFLandroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇0〇O0088o()Z
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    aget v2, v0, v1

    .line 12
    .line 13
    const/4 v3, 0x2

    .line 14
    aget v4, v0, v3

    .line 15
    .line 16
    const/4 v5, 0x5

    .line 17
    aget v6, v0, v5

    .line 18
    .line 19
    iget v7, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o00〇〇Oo:F

    .line 20
    .line 21
    const/4 v8, 0x4

    .line 22
    const/4 v9, 0x1

    .line 23
    cmpg-float v10, v2, v7

    .line 24
    .line 25
    if-gez v10, :cond_0

    .line 26
    .line 27
    aput v7, v0, v1

    .line 28
    .line 29
    aput v7, v0, v8

    .line 30
    .line 31
    const/4 v7, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v7, 0x0

    .line 34
    :goto_0
    iget v10, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o〇:F

    .line 35
    .line 36
    cmpl-float v11, v2, v10

    .line 37
    .line 38
    if-lez v11, :cond_1

    .line 39
    .line 40
    aput v10, v0, v1

    .line 41
    .line 42
    aput v10, v0, v8

    .line 43
    .line 44
    const/4 v7, 0x1

    .line 45
    :cond_1
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o800o8O(F)Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080()F

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇()F

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    cmpl-float v8, v4, v1

    .line 58
    .line 59
    if-lez v8, :cond_2

    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 62
    .line 63
    aput v1, v2, v3

    .line 64
    .line 65
    :goto_1
    const/4 v7, 0x1

    .line 66
    goto :goto_2

    .line 67
    :cond_2
    cmpg-float v1, v4, v2

    .line 68
    .line 69
    if-gez v1, :cond_3

    .line 70
    .line 71
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 72
    .line 73
    aput v2, v1, v3

    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_3
    :goto_2
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo()F

    .line 77
    .line 78
    .line 79
    move-result v1

    .line 80
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8()F

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    cmpl-float v2, v6, v1

    .line 85
    .line 86
    if-lez v2, :cond_4

    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 89
    .line 90
    aput v1, v0, v5

    .line 91
    .line 92
    :goto_3
    const/4 v7, 0x1

    .line 93
    goto :goto_4

    .line 94
    :cond_4
    cmpg-float v1, v6, v0

    .line 95
    .line 96
    if-gez v1, :cond_5

    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 99
    .line 100
    aput v0, v1, v5

    .line 101
    .line 102
    goto :goto_3

    .line 103
    :cond_5
    :goto_4
    if-eqz v7, :cond_6

    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 108
    .line 109
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    .line 110
    .line 111
    .line 112
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇0:Landroid/graphics/Matrix;

    .line 113
    .line 114
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 115
    .line 116
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    move-result v0

    .line 120
    xor-int/2addr v0, v9

    .line 121
    if-eqz v0, :cond_7

    .line 122
    .line 123
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇0:Landroid/graphics/Matrix;

    .line 124
    .line 125
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 126
    .line 127
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 128
    .line 129
    .line 130
    :cond_7
    return v0
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)Landroid/widget/OverScroller;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇8o8o〇:Landroid/widget/OverScroller;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)[F
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O00(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;[F)[F
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o0ooO([F)[F

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)Lcom/intsig/camscanner/pic2word/lr/LrView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O〇(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;FFF)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇00〇8(FFF)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final 〇o(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;FFLandroid/animation/ValueAnimator;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p3}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p3

    .line 15
    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    .line 16
    .line 17
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    check-cast p3, Ljava/lang/Float;

    .line 21
    .line 22
    invoke-virtual {p3}, Ljava/lang/Float;->floatValue()F

    .line 23
    .line 24
    .line 25
    move-result p3

    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇00()F

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    div-float/2addr p3, v0

    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 32
    .line 33
    invoke-virtual {v0, p3, p3, p1, p2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 34
    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇0〇O0088o()Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-eqz p1, :cond_0

    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8ooOoo〇()V

    .line 43
    .line 44
    .line 45
    :cond_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;F)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇8O0〇8(F)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;F)Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o800o8O(F)Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->OO0o〇〇:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o〇:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇8O0〇8(F)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇80〇808〇O:[F

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    aget v0, v0, v1

    .line 12
    .line 13
    iget v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o〇:F

    .line 14
    .line 15
    const/high16 v3, 0x3f800000    # 1.0f

    .line 16
    .line 17
    cmpl-float v2, v0, v2

    .line 18
    .line 19
    if-ltz v2, :cond_0

    .line 20
    .line 21
    cmpl-float v2, p1, v3

    .line 22
    .line 23
    if-lez v2, :cond_0

    .line 24
    .line 25
    return v1

    .line 26
    :cond_0
    iget v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o00〇〇Oo:F

    .line 27
    .line 28
    cmpg-float v0, v0, v2

    .line 29
    .line 30
    if-gtz v0, :cond_1

    .line 31
    .line 32
    cmpg-float p1, p1, v3

    .line 33
    .line 34
    if-gez p1, :cond_1

    .line 35
    .line 36
    return v1

    .line 37
    :cond_1
    const/4 p1, 0x1

    .line 38
    return p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final O8〇o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇8O0〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final OOO〇O0(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "event"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇8O0〇8:Z

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->oo88o8O:Landroid/view/ScaleGestureDetector;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 13
    .line 14
    .line 15
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMStartX()F

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    const/4 v3, 0x0

    .line 30
    const/4 v4, 0x0

    .line 31
    const/4 v5, 0x1

    .line 32
    cmpg-float v2, v2, v3

    .line 33
    .line 34
    if-nez v2, :cond_1

    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    goto :goto_0

    .line 38
    :cond_1
    const/4 v2, 0x0

    .line 39
    :goto_0
    if-eqz v2, :cond_3

    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMStartY()F

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    cmpg-float v2, v2, v3

    .line 48
    .line 49
    if-nez v2, :cond_2

    .line 50
    .line 51
    const/4 v2, 0x1

    .line 52
    goto :goto_1

    .line 53
    :cond_2
    const/4 v2, 0x0

    .line 54
    :goto_1
    if-eqz v2, :cond_3

    .line 55
    .line 56
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 57
    .line 58
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMStartX(F)V

    .line 59
    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 62
    .line 63
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMStartY(F)V

    .line 64
    .line 65
    .line 66
    :cond_3
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 67
    .line 68
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getLongPressMode()Z

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    if-eqz v2, :cond_8

    .line 73
    .line 74
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 75
    .line 76
    .line 77
    move-result v2

    .line 78
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 79
    .line 80
    invoke-virtual {v3}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇O8〇〇o()Z

    .line 81
    .line 82
    .line 83
    move-result v3

    .line 84
    new-instance v6, Ljava/lang/StringBuilder;

    .line 85
    .line 86
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .line 88
    .line 89
    const-string v7, "onLongTouch,  pointerCount: "

    .line 90
    .line 91
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    const-string v2, ", isTouchSelect:"

    .line 98
    .line 99
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v2

    .line 109
    const-string v3, "ZoomGesture"

    .line 110
    .line 111
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 115
    .line 116
    .line 117
    move-result p1

    .line 118
    if-ne p1, v5, :cond_7

    .line 119
    .line 120
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇O8〇〇o()Z

    .line 123
    .line 124
    .line 125
    move-result p1

    .line 126
    if-eqz p1, :cond_7

    .line 127
    .line 128
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 129
    .line 130
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getElement()Lcom/intsig/camscanner/pic2word/lr/LrElement;

    .line 131
    .line 132
    .line 133
    move-result-object p1

    .line 134
    if-eqz p1, :cond_7

    .line 135
    .line 136
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 137
    .line 138
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMStartX()F

    .line 139
    .line 140
    .line 141
    move-result p1

    .line 142
    sub-float p1, v0, p1

    .line 143
    .line 144
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 145
    .line 146
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getMStartY()F

    .line 147
    .line 148
    .line 149
    move-result v2

    .line 150
    sub-float v2, v1, v2

    .line 151
    .line 152
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    .line 153
    .line 154
    .line 155
    move-result v6

    .line 156
    const/high16 v7, 0x3fc00000    # 1.5f

    .line 157
    .line 158
    cmpl-float v6, v6, v7

    .line 159
    .line 160
    if-gtz v6, :cond_4

    .line 161
    .line 162
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    .line 163
    .line 164
    .line 165
    move-result v6

    .line 166
    cmpl-float v6, v6, v7

    .line 167
    .line 168
    if-lez v6, :cond_5

    .line 169
    .line 170
    :cond_4
    const/4 v4, 0x1

    .line 171
    :cond_5
    if-eqz v4, :cond_7

    .line 172
    .line 173
    const-string v4, "onMove"

    .line 174
    .line 175
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    iget-object v4, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 179
    .line 180
    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMStartX(F)V

    .line 181
    .line 182
    .line 183
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 184
    .line 185
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMStartY(F)V

    .line 186
    .line 187
    .line 188
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 189
    .line 190
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->getElement()Lcom/intsig/camscanner/pic2word/lr/LrElement;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    if-eqz v0, :cond_6

    .line 195
    .line 196
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->Oooo8o0〇()F

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    add-float/2addr v1, p1

    .line 201
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇o(F)V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇〇808〇()F

    .line 205
    .line 206
    .line 207
    move-result p1

    .line 208
    add-float/2addr p1, v2

    .line 209
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->o0ooO(F)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->Oooo8o0〇()F

    .line 213
    .line 214
    .line 215
    move-result p1

    .line 216
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇〇808〇()F

    .line 217
    .line 218
    .line 219
    move-result v0

    .line 220
    new-instance v1, Ljava/lang/StringBuilder;

    .line 221
    .line 222
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 223
    .line 224
    .line 225
    const-string v2, "onMove offsetX:"

    .line 226
    .line 227
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    .line 229
    .line 230
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 231
    .line 232
    .line 233
    const-string p1, ", offsetY:"

    .line 234
    .line 235
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 239
    .line 240
    .line 241
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 242
    .line 243
    .line 244
    move-result-object p1

    .line 245
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    .line 247
    .line 248
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8ooOoo〇()V

    .line 249
    .line 250
    .line 251
    :cond_7
    return v5

    .line 252
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇oo〇:Landroid/view/GestureDetector;

    .line 253
    .line 254
    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 255
    .line 256
    .line 257
    iget-boolean p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇8O0〇8:Z

    .line 258
    .line 259
    if-eqz p1, :cond_9

    .line 260
    .line 261
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇0〇O0088o()Z

    .line 262
    .line 263
    .line 264
    move-result p1

    .line 265
    if-eqz p1, :cond_9

    .line 266
    .line 267
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8ooOoo〇()V

    .line 268
    .line 269
    .line 270
    :cond_9
    return v5
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final OoO8()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇8o8o〇:Landroid/widget/OverScroller;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇8o8o〇:Landroid/widget/OverScroller;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇8o8o〇:Landroid/widget/OverScroller;

    .line 17
    .line 18
    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrY()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O888o0o:Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o〇()F

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    float-to-int v3, v3

    .line 29
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇080()F

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    float-to-int v4, v4

    .line 34
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->O8()F

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    float-to-int v5, v5

    .line 39
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture$ScrollValues;->〇o00〇〇Oo()F

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    float-to-int v2, v2

    .line 44
    const/4 v6, 0x1

    .line 45
    const/4 v7, 0x0

    .line 46
    if-gt v3, v0, :cond_1

    .line 47
    .line 48
    if-gt v0, v4, :cond_1

    .line 49
    .line 50
    const/4 v3, 0x1

    .line 51
    goto :goto_0

    .line 52
    :cond_1
    const/4 v3, 0x0

    .line 53
    :goto_0
    if-eqz v3, :cond_2

    .line 54
    .line 55
    goto :goto_2

    .line 56
    :cond_2
    if-gt v5, v1, :cond_3

    .line 57
    .line 58
    if-gt v1, v2, :cond_3

    .line 59
    .line 60
    const/4 v2, 0x1

    .line 61
    goto :goto_1

    .line 62
    :cond_3
    const/4 v2, 0x0

    .line 63
    :goto_1
    if-eqz v2, :cond_4

    .line 64
    .line 65
    goto :goto_2

    .line 66
    :cond_4
    const/4 v6, 0x0

    .line 67
    :goto_2
    if-eqz v6, :cond_5

    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇888:Landroid/graphics/Matrix;

    .line 70
    .line 71
    int-to-float v0, v0

    .line 72
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O8o08O:F

    .line 73
    .line 74
    sub-float v3, v0, v3

    .line 75
    .line 76
    int-to-float v1, v1

    .line 77
    iget v4, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->OO0o〇〇:F

    .line 78
    .line 79
    sub-float v4, v1, v4

    .line 80
    .line 81
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 82
    .line 83
    .line 84
    iput v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O8o08O:F

    .line 85
    .line 86
    iput v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->OO0o〇〇:F

    .line 87
    .line 88
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇0〇O0088o()Z

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    if-eqz v0, :cond_5

    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇080:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 95
    .line 96
    invoke-static {v0}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 97
    .line 98
    .line 99
    :cond_5
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final O〇8O8〇008(FFF)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O00:F

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8:Landroid/graphics/Matrix;

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8:Landroid/graphics/Matrix;

    .line 9
    .line 10
    invoke-virtual {v0, p1, p1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8:Landroid/graphics/Matrix;

    .line 14
    .line 15
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final oo88o8O()Landroid/graphics/Matrix;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->oO80:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oo08:Landroid/graphics/Matrix;

    .line 4
    .line 5
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo〇(FF)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇808〇:F

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O〇:F

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇0〇O0088o()Z

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8ooOoo〇()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final o〇O8〇〇o()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O00:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "canvas"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oo08:Landroid/graphics/Matrix;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇0:Landroid/graphics/Matrix;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇O8〇〇o:Landroid/graphics/Matrix;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->O8:Landroid/graphics/Matrix;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 18
    .line 19
    .line 20
    iget v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O〇:F

    .line 21
    .line 22
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    const/high16 v1, 0x3f800000    # 1.0f

    .line 27
    .line 28
    cmpl-float v0, v0, v1

    .line 29
    .line 30
    if-lez v0, :cond_0

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇O8〇〇o:Landroid/graphics/Matrix;

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    iget v2, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇O〇:F

    .line 36
    .line 37
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 38
    .line 39
    .line 40
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oo08:Landroid/graphics/Matrix;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->o〇O8〇〇o:Landroid/graphics/Matrix;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oo08:Landroid/graphics/Matrix;

    .line 48
    .line 49
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇0000OOO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇〇8O0〇8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O888o0o()F
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oo08:Landroid/graphics/Matrix;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->OO0o〇〇〇〇0:[F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->OO0o〇〇〇〇0:[F

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    aget v0, v0, v1

    .line 12
    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oOO8O8()Z
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇00()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->〇o00〇〇Oo:F

    .line 6
    .line 7
    sub-float/2addr v0, v1

    .line 8
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const v1, 0x358637bd    # 1.0E-6f

    .line 13
    .line 14
    .line 15
    cmpl-float v0, v0, v1

    .line 16
    .line 17
    if-lez v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇oo〇()Landroid/graphics/Matrix;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomGesture;->Oo08:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

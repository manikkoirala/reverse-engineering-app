.class public final Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "ZoomRV.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;->o0:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 2
    .line 3
    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5
    .param p1    # Landroid/view/ScaleGestureDetector;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "detector"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;->o0:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 11
    .line 12
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->O8(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)F

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    const/high16 v2, 0x40800000    # 4.0f

    .line 17
    .line 18
    const/high16 v3, 0x3f800000    # 1.0f

    .line 19
    .line 20
    const/4 v4, 0x1

    .line 21
    cmpl-float v1, v1, v2

    .line 22
    .line 23
    if-ltz v1, :cond_0

    .line 24
    .line 25
    cmpl-float v1, v0, v3

    .line 26
    .line 27
    if-lez v1, :cond_0

    .line 28
    .line 29
    return v4

    .line 30
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;->o0:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 31
    .line 32
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->O8(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)F

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    cmpg-float v1, v1, v3

    .line 37
    .line 38
    if-gtz v1, :cond_1

    .line 39
    .line 40
    cmpg-float v1, v0, v3

    .line 41
    .line 42
    if-gez v1, :cond_1

    .line 43
    .line 44
    return v4

    .line 45
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;->o0:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 46
    .line 47
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->o〇0(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;)Landroid/graphics/Matrix;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    invoke-virtual {v1, v0, v0, v2, p1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 60
    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv$mScaleListener$1;->o0:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 63
    .line 64
    invoke-static {p1, v4}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->〇80〇808〇O(Lcom/intsig/camscanner/pic2word/lr/ZoomRv;Z)V

    .line 65
    .line 66
    .line 67
    return v4
.end method

.class final Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;
.super Ljava/lang/Object;
.source "LrUndoManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pic2word/lr/LrUndoManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Operation"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:I

.field private final 〇080:Landroid/text/Editable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Landroid/text/SpannableStringBuilder;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:I


# direct methods
.method public constructor <init>(Landroid/text/Editable;)V
    .locals 1
    .param p1    # Landroid/text/Editable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "src"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇080:Landroid/text/Editable;

    .line 10
    .line 11
    new-instance v0, Landroid/text/SpannableStringBuilder;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇o00〇〇Oo:Landroid/text/SpannableStringBuilder;

    .line 17
    .line 18
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iput v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->O8:I

    .line 23
    .line 24
    if-gez v0, :cond_0

    .line 25
    .line 26
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    iput v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->O8:I

    .line 31
    .line 32
    :cond_0
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇o〇:I

    .line 37
    .line 38
    if-gez p1, :cond_1

    .line 39
    .line 40
    iget p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->O8:I

    .line 41
    .line 42
    iput p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇o〇:I

    .line 43
    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final 〇080()Landroid/text/Editable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇080:Landroid/text/Editable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇o〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇080:Landroid/text/Editable;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇o00〇〇Oo:Landroid/text/SpannableStringBuilder;

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->〇080:Landroid/text/Editable;

    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrUndoManager$Operation;->O8:I

    .line 16
    .line 17
    invoke-static {v0, v1, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

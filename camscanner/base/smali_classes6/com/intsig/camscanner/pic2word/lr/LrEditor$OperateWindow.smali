.class public final Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;
.super Ljava/lang/Object;
.source "LrEditor.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pic2word/lr/LrEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "OperateWindow"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:I

.field private final o0:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO0〇0o:[F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:I

.field private final oOo〇8o008:[I
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Landroid/widget/PopupWindow;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field final synthetic 〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/pic2word/lr/LrView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/pic2word/lr/LrEditor;Landroid/content/Context;Lcom/intsig/camscanner/pic2word/lr/LrView;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/pic2word/lr/LrEditor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/intsig/camscanner/pic2word/lr/LrView;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "parentView"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 12
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o0:Landroid/content/Context;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 19
    .line 20
    const/4 p1, 0x2

    .line 21
    new-array p3, p1, [I

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->oOo〇8o008:[I

    .line 24
    .line 25
    new-array p1, p1, [F

    .line 26
    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o8〇OO0〇0o:[F

    .line 28
    .line 29
    const p1, 0x7f1309f6

    .line 30
    .line 31
    .line 32
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;I)Landroid/widget/TextView;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇08O〇00〇o:Landroid/view/View;

    .line 37
    .line 38
    const p1, 0x7f130684

    .line 39
    .line 40
    .line 41
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;I)Landroid/widget/TextView;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o〇00O:Landroid/view/View;

    .line 46
    .line 47
    const p1, 0x7f1309fc

    .line 48
    .line 49
    .line 50
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;I)Landroid/widget/TextView;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->O8o08O8O:Landroid/view/View;

    .line 55
    .line 56
    const p3, 0x7f1301b4

    .line 57
    .line 58
    .line 59
    invoke-static {p0, p3}, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;I)Landroid/widget/TextView;

    .line 60
    .line 61
    .line 62
    move-result-object p3

    .line 63
    iput-object p3, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080OO8〇0:Landroid/view/View;

    .line 64
    .line 65
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    .line 66
    .line 67
    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 68
    .line 69
    .line 70
    const/4 v1, 0x3

    .line 71
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 72
    .line 73
    .line 74
    move-result v1

    .line 75
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 76
    .line 77
    .line 78
    const/4 v1, -0x1

    .line 79
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 80
    .line 81
    .line 82
    new-instance v1, Landroid/widget/LinearLayout;

    .line 83
    .line 84
    invoke-direct {v1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 85
    .line 86
    .line 87
    const/4 p2, 0x0

    .line 88
    invoke-virtual {v1, p2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v1, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 98
    .line 99
    .line 100
    iput-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->OO:Landroid/view/View;

    .line 101
    .line 102
    new-instance p1, Landroid/widget/PopupWindow;

    .line 103
    .line 104
    const/4 p3, -0x2

    .line 105
    invoke-direct {p1, v1, p3, p3, p2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    .line 106
    .line 107
    .line 108
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇0O:Landroid/widget/PopupWindow;

    .line 109
    .line 110
    invoke-virtual {p1, p2}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 111
    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final O8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->OO:Landroid/view/View;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 5
    .line 6
    .line 7
    move-result v2

    .line 8
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->OO:Landroid/view/View;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iput v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->oOo0:I

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->OO:Landroid/view/View;

    .line 24
    .line 25
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    iput v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->OO〇00〇8oO:I

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final Oo08()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇08O8o〇0()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇08O〇00〇o:Landroid/view/View;

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 17
    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 20
    .line 21
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇〇0o()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o〇00O:Landroid/view/View;

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 34
    .line 35
    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 37
    .line 38
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->oO()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->O8o08O8O:Landroid/view/View;

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 51
    .line 52
    .line 53
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 54
    .line 55
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇8()Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-eqz v0, :cond_3

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080OO8〇0:Landroid/view/View;

    .line 66
    .line 67
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 68
    .line 69
    .line 70
    :cond_3
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final 〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;I)Landroid/widget/TextView;
    .locals 3

    .line 1
    new-instance v0, Landroid/widget/TextView;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o0:Landroid/content/Context;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const/high16 v1, 0x41800000    # 16.0f

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 11
    .line 12
    .line 13
    const v1, -0xcccccd

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 20
    .line 21
    .line 22
    const/16 p1, 0x10

    .line 23
    .line 24
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    float-to-int p1, p1

    .line 29
    const/16 v1, 0xc

    .line 30
    .line 31
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    float-to-int v1, v1

    .line 36
    invoke-virtual {v0, p1, v1, p1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 37
    .line 38
    .line 39
    const/16 p1, 0x8

    .line 40
    .line 41
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    .line 46
    .line 47
    new-instance p0, Landroid/graphics/drawable/RippleDrawable;

    .line 48
    .line 49
    const/high16 p1, 0x1a000000

    .line 50
    .line 51
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 56
    .line 57
    const/4 v2, -0x1

    .line 58
    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 59
    .line 60
    .line 61
    const/4 v2, 0x0

    .line 62
    invoke-direct {p0, p1, v2, v1}, Landroid/graphics/drawable/RippleDrawable;-><init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, p0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 66
    .line 67
    .line 68
    return-object v0
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇08O〇00〇o:Landroid/view/View;

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x1

    .line 9
    const/4 v3, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-static {v0, v3, v2, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->o8oO〇(Lcom/intsig/camscanner/pic2word/lr/LrText;ZILjava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o〇00O:Landroid/view/View;

    .line 23
    .line 24
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 31
    .line 32
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-static {v0, v3, v2, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇8〇0〇o〇O(Lcom/intsig/camscanner/pic2word/lr/LrText;ZILjava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->O8o08O8O:Landroid/view/View;

    .line 41
    .line 42
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 49
    .line 50
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-static {v0, v3, v2, v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->Oo(Lcom/intsig/camscanner/pic2word/lr/LrText;ZILjava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080OO8〇0:Landroid/view/View;

    .line 59
    .line 60
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    if-eqz v0, :cond_3

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 67
    .line 68
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->ooo0〇O88O()V

    .line 73
    .line 74
    .line 75
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080OO8〇0:Landroid/view/View;

    .line 76
    .line 77
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    move-result p1

    .line 81
    if-nez p1, :cond_4

    .line 82
    .line 83
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 84
    .line 85
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/pic2word/lr/LrText;->ooO〇00O(Z)V

    .line 90
    .line 91
    .line 92
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇o00〇〇Oo()V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 96
    .line 97
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇O〇()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    if-eqz p1, :cond_5

    .line 106
    .line 107
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->OOO〇O0()V

    .line 108
    .line 109
    .line 110
    :cond_5
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final o〇0()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->oOo〇8o008:[I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrText;->〇O〇80o08O()Landroid/text/Layout;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 22
    .line 23
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrText;->O0()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 32
    .line 33
    invoke-static {v2}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrText;->Oo〇O()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-le v1, v2, :cond_1

    .line 42
    .line 43
    move v1, v2

    .line 44
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pic2word/lr/LrEditor;

    .line 45
    .line 46
    invoke-static {v2}, Lcom/intsig/camscanner/pic2word/lr/LrEditor;->〇080(Lcom/intsig/camscanner/pic2word/lr/LrEditor;)Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->〇8o8o〇()Landroid/graphics/RectF;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->Oo08()V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->O8()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    iget v4, v2, Landroid/graphics/RectF;->left:F

    .line 65
    .line 66
    add-float/2addr v3, v4

    .line 67
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    int-to-float v0, v0

    .line 76
    iget v1, v2, Landroid/graphics/RectF;->top:F

    .line 77
    .line 78
    add-float/2addr v0, v1

    .line 79
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o8〇OO0〇0o:[F

    .line 80
    .line 81
    const/4 v2, 0x0

    .line 82
    aput v3, v1, v2

    .line 83
    .line 84
    const/4 v3, 0x1

    .line 85
    aput v0, v1, v3

    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o800o8O([F)V

    .line 90
    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o8〇OO0〇0o:[F

    .line 93
    .line 94
    aget v1, v0, v2

    .line 95
    .line 96
    float-to-int v1, v1

    .line 97
    aget v0, v0, v3

    .line 98
    .line 99
    iget-object v4, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->oOo〇8o008:[I

    .line 100
    .line 101
    aget v3, v4, v3

    .line 102
    .line 103
    int-to-float v3, v3

    .line 104
    add-float/2addr v0, v3

    .line 105
    iget v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->OO〇00〇8oO:I

    .line 106
    .line 107
    int-to-float v3, v3

    .line 108
    sub-float/2addr v0, v3

    .line 109
    const/16 v3, 0x8

    .line 110
    .line 111
    invoke-static {v3}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 112
    .line 113
    .line 114
    move-result v3

    .line 115
    sub-float/2addr v0, v3

    .line 116
    float-to-int v0, v0

    .line 117
    const/16 v3, 0x10

    .line 118
    .line 119
    if-gtz v1, :cond_2

    .line 120
    .line 121
    const/16 v1, 0x10

    .line 122
    .line 123
    :cond_2
    if-gez v0, :cond_3

    .line 124
    .line 125
    const/16 v0, 0x10

    .line 126
    .line 127
    :cond_3
    iget-object v4, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o0:Landroid/content/Context;

    .line 128
    .line 129
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 130
    .line 131
    .line 132
    move-result-object v4

    .line 133
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 134
    .line 135
    .line 136
    move-result-object v4

    .line 137
    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 138
    .line 139
    iget v5, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->oOo0:I

    .line 140
    .line 141
    add-int v6, v1, v5

    .line 142
    .line 143
    if-le v6, v4, :cond_4

    .line 144
    .line 145
    sub-int/2addr v4, v5

    .line 146
    add-int/lit8 v1, v4, -0x10

    .line 147
    .line 148
    :cond_4
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇0O:Landroid/widget/PopupWindow;

    .line 149
    .line 150
    const/high16 v4, 0x41000000    # 8.0f

    .line 151
    .line 152
    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setElevation(F)V

    .line 153
    .line 154
    .line 155
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇0O:Landroid/widget/PopupWindow;

    .line 156
    .line 157
    iget-object v4, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 158
    .line 159
    invoke-virtual {v3, v4, v2, v1, v0}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 160
    .line 161
    .line 162
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇o00〇〇Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇0O:Landroid/widget/PopupWindow;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇08O〇00〇o:Landroid/view/View;

    .line 7
    .line 8
    const/16 v1, 0x8

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->o〇00O:Landroid/view/View;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->O8o08O8O:Landroid/view/View;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇080OO8〇0:Landroid/view/View;

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇o〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/lr/LrEditor$OperateWindow;->〇0O:Landroid/widget/PopupWindow;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

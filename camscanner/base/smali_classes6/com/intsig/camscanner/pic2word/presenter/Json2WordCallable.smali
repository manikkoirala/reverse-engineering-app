.class public final Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;
.super Ljava/lang/Object;
.source "Json2WordCallable.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$JsonBody;,
        Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$NewJsonBody;,
        Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$ToWordScene;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;->〇080:Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O8(Ljava/lang/String;Ljava/util/List;Ljava/io/File;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")Z"
        }
    .end annotation

    .line 1
    const-string v0, "Json2WordCallable"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    :try_start_0
    new-instance v2, Lcom/intsig/tianshu/ParamsBuilder;

    .line 5
    .line 6
    invoke-direct {v2}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v3, "cs_ept_d"

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇888()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v4

    .line 15
    invoke-virtual {v2, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const-string v3, "platform"

    .line 20
    .line 21
    const-string v4, "android"

    .line 22
    .line 23
    invoke-virtual {v2, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    const-string v3, "app_version"

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPIUtils;->〇080()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    invoke-virtual {v2, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    const-string v3, "token"

    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    invoke-virtual {v2, v3, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    const-string v3, "doc_id"

    .line 48
    .line 49
    invoke-virtual {v2, v3, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    const-string v2, "premium"

    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 56
    .line 57
    .line 58
    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    const-string v4, "0"

    .line 60
    .line 61
    if-eqz v3, :cond_0

    .line 62
    .line 63
    :try_start_1
    const-string v3, "1"

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_0
    move-object v3, v4

    .line 67
    :goto_0
    invoke-virtual {p1, v2, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    const-string v2, "slice_draw"

    .line 72
    .line 73
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 74
    .line 75
    .line 76
    move-result-object v3

    .line 77
    iget v3, v3, Lcom/intsig/tsapp/sync/AppConfigJson;->slice_draw:I

    .line 78
    .line 79
    invoke-virtual {p1, v2, v3}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    const-string v2, "timestamp"

    .line 84
    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 86
    .line 87
    .line 88
    move-result-wide v5

    .line 89
    invoke-virtual {p1, v2, v5, v6}, Lcom/intsig/tianshu/ParamsBuilder;->OO0o〇〇〇〇0(Ljava/lang/String;J)Lcom/intsig/tianshu/ParamsBuilder;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    const/16 v3, 0x14

    .line 98
    .line 99
    invoke-virtual {v2, v3}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    new-instance v3, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v2, "/json2pdf"

    .line 112
    .line 113
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    invoke-virtual {p1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v2

    .line 128
    const-string v3, "gen()"

    .line 129
    .line 130
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    const-string v3, "this as java.lang.String).toLowerCase()"

    .line 138
    .line 139
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    new-instance v3, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$JsonBody;

    .line 143
    .line 144
    invoke-direct {v3, p2}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$JsonBody;-><init>(Ljava/util/List;)V

    .line 145
    .line 146
    .line 147
    invoke-static {p1}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 148
    .line 149
    .line 150
    move-result-object p1

    .line 151
    const-string p2, "x_request_id"

    .line 152
    .line 153
    invoke-virtual {p1, p2, v2}, Lcom/lzy/okgo/request/base/Request;->headers(Ljava/lang/String;Ljava/lang/String;)Lcom/lzy/okgo/request/base/Request;

    .line 154
    .line 155
    .line 156
    move-result-object p1

    .line 157
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 158
    .line 159
    invoke-virtual {p1, v3}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 160
    .line 161
    .line 162
    move-result-object p1

    .line 163
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 164
    .line 165
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 166
    .line 167
    .line 168
    move-result-object p1

    .line 169
    invoke-virtual {p1}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 170
    .line 171
    .line 172
    move-result-object p2

    .line 173
    invoke-virtual {p1}, Lokhttp3/Response;->〇oo〇()Z

    .line 174
    .line 175
    .line 176
    move-result v2

    .line 177
    if-eqz v2, :cond_4

    .line 178
    .line 179
    if-nez p2, :cond_1

    .line 180
    .line 181
    goto :goto_1

    .line 182
    :cond_1
    const-string v2, "X-IS-Error-Code"

    .line 183
    .line 184
    invoke-virtual {p1, v2, v4}, Lokhttp3/Response;->〇O00(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object p1

    .line 188
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 189
    .line 190
    .line 191
    move-result p1

    .line 192
    if-nez p1, :cond_2

    .line 193
    .line 194
    goto :goto_2

    .line 195
    :cond_2
    invoke-virtual {p3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    if-eqz p1, :cond_3

    .line 200
    .line 201
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    .line 202
    .line 203
    .line 204
    :cond_3
    invoke-virtual {p2}, Lokhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    .line 205
    .line 206
    .line 207
    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 208
    :try_start_2
    new-instance p2, Ljava/io/FileOutputStream;

    .line 209
    .line 210
    invoke-direct {p2, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 211
    .line 212
    .line 213
    const/4 p3, 0x2

    .line 214
    const/4 v2, 0x0

    .line 215
    :try_start_3
    invoke-static {p1, p2, v1, p3, v2}, Lkotlin/io/ByteStreamsKt;->〇o00〇〇Oo(Ljava/io/InputStream;Ljava/io/OutputStream;IILjava/lang/Object;)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 216
    .line 217
    .line 218
    :try_start_4
    invoke-static {p2, v2}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 219
    .line 220
    .line 221
    :try_start_5
    invoke-static {p1, v2}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 222
    .line 223
    .line 224
    const/4 v1, 0x1

    .line 225
    goto :goto_2

    .line 226
    :catchall_0
    move-exception p3

    .line 227
    :try_start_6
    throw p3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 228
    :catchall_1
    move-exception v2

    .line 229
    :try_start_7
    invoke-static {p2, p3}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 230
    .line 231
    .line 232
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 233
    :catchall_2
    move-exception p2

    .line 234
    :try_start_8
    throw p2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 235
    :catchall_3
    move-exception p3

    .line 236
    :try_start_9
    invoke-static {p1, p2}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 237
    .line 238
    .line 239
    throw p3

    .line 240
    :cond_4
    :goto_1
    const-string p1, "response fail"

    .line 241
    .line 242
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    .line 243
    .line 244
    .line 245
    :goto_2
    return v1

    .line 246
    :catch_0
    move-exception p1

    .line 247
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 248
    .line 249
    .line 250
    return v1
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;ILjava/lang/Object;)Lcom/intsig/camscanner/pic2word/bean/WordResponse;
    .locals 13

    .line 1
    move/from16 v0, p10

    .line 2
    .line 3
    and-int/lit8 v1, v0, 0x10

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    move-object v8, v2

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    move-object/from16 v8, p5

    .line 11
    .line 12
    :goto_0
    and-int/lit8 v1, v0, 0x20

    .line 13
    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    move-object v9, v2

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    move-object/from16 v9, p6

    .line 19
    .line 20
    :goto_1
    and-int/lit8 v1, v0, 0x40

    .line 21
    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    const/4 v10, 0x1

    .line 26
    goto :goto_2

    .line 27
    :cond_2
    move/from16 v10, p7

    .line 28
    .line 29
    :goto_2
    and-int/lit16 v1, v0, 0x80

    .line 30
    .line 31
    if-eqz v1, :cond_3

    .line 32
    .line 33
    const/4 v1, 0x0

    .line 34
    const/4 v11, 0x0

    .line 35
    goto :goto_3

    .line 36
    :cond_3
    move/from16 v11, p8

    .line 37
    .line 38
    :goto_3
    and-int/lit16 v0, v0, 0x100

    .line 39
    .line 40
    if-eqz v0, :cond_4

    .line 41
    .line 42
    move-object v12, v2

    .line 43
    goto :goto_4

    .line 44
    :cond_4
    move-object/from16 v12, p9

    .line 45
    .line 46
    :goto_4
    move-object v3, p0

    .line 47
    move-object v4, p1

    .line 48
    move-object v5, p2

    .line 49
    move-object/from16 v6, p3

    .line 50
    .line 51
    move-object/from16 v7, p4

    .line 52
    .line 53
    invoke-virtual/range {v3 .. v12}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;->〇〇888(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)Lcom/intsig/camscanner/pic2word/bean/WordResponse;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    return-object v0
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;Ljava/lang/String;Ljava/util/List;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;Ljava/util/ArrayList;Lkotlin/coroutines/Continuation;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 13

    move/from16 v0, p11

    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_0

    const-string v1, "classify"

    move-object v7, v1

    goto :goto_0

    :cond_0
    move-object/from16 v7, p5

    :goto_0
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    move/from16 v8, p6

    :goto_1
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    const/4 v9, 0x0

    goto :goto_2

    :cond_2
    move/from16 v9, p7

    :goto_2
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    move-object v10, v0

    goto :goto_3

    :cond_3
    move-object/from16 v10, p8

    :goto_3
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    .line 1
    invoke-virtual/range {v2 .. v12}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;->Oo08(Ljava/lang/String;Ljava/util/List;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;Ljava/util/ArrayList;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;Ljava/lang/String;Ljava/util/List;Ljava/io/File;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;->O8(Ljava/lang/String;Ljava/util/List;Ljava/io/File;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇80〇808〇O(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/loadimage/PageImage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Ljava/io/File;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pic2word/lr/LrImageJson;",
            ">;)V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p4

    .line 2
    .line 3
    move-object/from16 v10, p5

    .line 4
    .line 5
    if-eqz p1, :cond_6

    .line 6
    .line 7
    if-nez p3, :cond_0

    .line 8
    .line 9
    goto/16 :goto_4

    .line 10
    .line 11
    :cond_0
    if-eqz p2, :cond_6

    .line 12
    .line 13
    move-object/from16 v1, p2

    .line 14
    .line 15
    check-cast v1, Ljava/lang/Iterable;

    .line 16
    .line 17
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object v11

    .line 21
    const/4 v12, 0x0

    .line 22
    const/4 v1, 0x0

    .line 23
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_6

    .line 28
    .line 29
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    add-int/lit8 v13, v1, 0x1

    .line 34
    .line 35
    if-gez v1, :cond_1

    .line 36
    .line 37
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 38
    .line 39
    .line 40
    :cond_1
    check-cast v2, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 41
    .line 42
    move-object v3, v0

    .line 43
    check-cast v3, Ljava/util/Collection;

    .line 44
    .line 45
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    const/4 v4, 0x1

    .line 50
    xor-int/2addr v3, v4

    .line 51
    const/4 v5, 0x0

    .line 52
    if-eqz v3, :cond_2

    .line 53
    .line 54
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    if-ge v1, v3, :cond_2

    .line 59
    .line 60
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    check-cast v3, Ljava/io/File;

    .line 65
    .line 66
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    move-object v9, v3

    .line 71
    goto :goto_1

    .line 72
    :cond_2
    move-object v9, v5

    .line 73
    :goto_1
    if-eqz v10, :cond_4

    .line 74
    .line 75
    invoke-interface/range {p5 .. p5}, Ljava/util/Collection;->isEmpty()Z

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    if-eqz v3, :cond_3

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_3
    const/4 v4, 0x0

    .line 83
    :cond_4
    :goto_2
    if-nez v4, :cond_5

    .line 84
    .line 85
    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    if-ge v1, v3, :cond_5

    .line 90
    .line 91
    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    move-result-object v1

    .line 95
    check-cast v1, Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 96
    .line 97
    move-object v8, v1

    .line 98
    goto :goto_3

    .line 99
    :cond_5
    move-object v8, v5

    .line 100
    :goto_3
    sget-object v1, Lcom/intsig/camscanner/pagelist/ImageJsonBadCaseUtil;->〇080:Lcom/intsig/camscanner/pagelist/ImageJsonBadCaseUtil;

    .line 101
    .line 102
    const-string v3, "cs_img2word_feedback_upload"

    .line 103
    .line 104
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v4

    .line 108
    const-string v5, "it.pageSyncId"

    .line 109
    .line 110
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v6

    .line 117
    const-string v2, "it.path()"

    .line 118
    .line 119
    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    const-string v7, "json2word"

    .line 123
    .line 124
    move-object v2, v3

    .line 125
    move-object v3, p1

    .line 126
    move-object/from16 v5, p3

    .line 127
    .line 128
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/camscanner/pagelist/ImageJsonBadCaseUtil;->o〇0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    move v1, v13

    .line 132
    goto :goto_0

    .line 133
    :cond_6
    :goto_4
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final 〇o00〇〇Oo(Lokhttp3/Response;)I
    .locals 2

    .line 1
    const-string v0, "X-IS-Error-Code"

    .line 2
    .line 3
    const-string v1, "0"

    .line 4
    .line 5
    invoke-virtual {p1, v0, v1}, Lokhttp3/Response;->〇O00(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    if-eqz p1, :cond_1

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 21
    :goto_1
    if-eqz v0, :cond_2

    .line 22
    .line 23
    const/16 p1, -0x6f

    .line 24
    .line 25
    goto :goto_2

    .line 26
    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    :goto_2
    return p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final Oo08(Ljava/lang/String;Ljava/util/List;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;Ljava/util/ArrayList;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 13
    .param p3    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pic2word/lr/LrImageJson;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/loadimage/PageImage;",
            ">;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/intsig/camscanner/pic2word/bean/WordResponse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v12, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$json2Word$2;

    .line 6
    .line 7
    const/4 v11, 0x0

    .line 8
    move-object v1, v12

    .line 9
    move-object v2, p2

    .line 10
    move-object/from16 v3, p8

    .line 11
    .line 12
    move-object v4, p1

    .line 13
    move-object/from16 v5, p9

    .line 14
    .line 15
    move-object/from16 v6, p3

    .line 16
    .line 17
    move-object/from16 v7, p4

    .line 18
    .line 19
    move-object/from16 v8, p5

    .line 20
    .line 21
    move/from16 v9, p6

    .line 22
    .line 23
    move/from16 v10, p7

    .line 24
    .line 25
    invoke-direct/range {v1 .. v11}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$json2Word$2;-><init>(Ljava/util/List;Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/ArrayList;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILkotlin/coroutines/Continuation;)V

    .line 26
    .line 27
    .line 28
    move-object/from16 v1, p10

    .line 29
    .line 30
    invoke-static {v0, v12, v1}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
.end method

.method public final 〇o〇(Ljava/lang/String;Ljava/util/List;Ljava/io/File;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$json2Pdf$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p2, p1, p3, v2}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$json2Pdf$2;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/io/File;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p4}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public final 〇〇888(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)Lcom/intsig/camscanner/pic2word/bean/WordResponse;
    .locals 21
    .param p4    # Ljava/io/File;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/loadimage/PageImage;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pic2word/lr/LrImageJson;",
            ">;)",
            "Lcom/intsig/camscanner/pic2word/bean/WordResponse;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v8, p3

    .line 4
    .line 5
    move-object/from16 v0, p4

    .line 6
    .line 7
    move-object/from16 v1, p5

    .line 8
    .line 9
    move-object/from16 v2, p6

    .line 10
    .line 11
    move-object/from16 v9, p9

    .line 12
    .line 13
    const-string v3, "/json2word"

    .line 14
    .line 15
    const-string v4, "1"

    .line 16
    .line 17
    const-string v10, "Json2WordCallable"

    .line 18
    .line 19
    const-string v5, "dst"

    .line 20
    .line 21
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const-string v5, ""

    .line 25
    .line 26
    :try_start_0
    new-instance v6, Lcom/intsig/tianshu/ParamsBuilder;

    .line 27
    .line 28
    invoke-direct {v6}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v11, "cs_ept_d"

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇888()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v12

    .line 37
    invoke-virtual {v6, v11, v12}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 38
    .line 39
    .line 40
    move-result-object v6

    .line 41
    const-string v11, "platform"

    .line 42
    .line 43
    const-string v12, "android"

    .line 44
    .line 45
    invoke-virtual {v6, v11, v12}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 46
    .line 47
    .line 48
    move-result-object v6

    .line 49
    const-string v11, "app_version"

    .line 50
    .line 51
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPIUtils;->〇080()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v12

    .line 55
    invoke-virtual {v6, v11, v12}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 56
    .line 57
    .line 58
    move-result-object v6

    .line 59
    const-string v11, "token"

    .line 60
    .line 61
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v12

    .line 65
    invoke-virtual {v6, v11, v12}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 66
    .line 67
    .line 68
    move-result-object v6

    .line 69
    const-string v11, "doc_id"

    .line 70
    .line 71
    move-object/from16 v12, p1

    .line 72
    .line 73
    invoke-virtual {v6, v11, v12}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 74
    .line 75
    .line 76
    move-result-object v6

    .line 77
    const-string v11, "image_fs_url"

    .line 78
    .line 79
    invoke-virtual {v6, v11, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 80
    .line 81
    .line 82
    move-result-object v6

    .line 83
    const-string v11, "premium"

    .line 84
    .line 85
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 86
    .line 87
    .line 88
    move-result v13

    .line 89
    if-eqz v13, :cond_0

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_0
    const-string v4, "0"

    .line 93
    .line 94
    :goto_0
    invoke-virtual {v6, v11, v4}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 95
    .line 96
    .line 97
    move-result-object v4

    .line 98
    const-string v6, "timestamp"

    .line 99
    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 101
    .line 102
    .line 103
    move-result-wide v13

    .line 104
    invoke-virtual {v4, v6, v13, v14}, Lcom/intsig/tianshu/ParamsBuilder;->OO0o〇〇〇〇0(Ljava/lang/String;J)Lcom/intsig/tianshu/ParamsBuilder;

    .line 105
    .line 106
    .line 107
    move-result-object v4

    .line 108
    const-string v6, "slice_draw"

    .line 109
    .line 110
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 111
    .line 112
    .line 113
    move-result-object v11

    .line 114
    iget v11, v11, Lcom/intsig/tsapp/sync/AppConfigJson;->slice_draw:I

    .line 115
    .line 116
    invoke-virtual {v4, v6, v11}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 117
    .line 118
    .line 119
    move-result-object v4

    .line 120
    const-string v6, "handwritten_render"

    .line 121
    .line 122
    move/from16 v11, p7

    .line 123
    .line 124
    invoke-virtual {v4, v6, v11}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 125
    .line 126
    .line 127
    move-result-object v4

    .line 128
    const-string v6, "stamp_render"

    .line 129
    .line 130
    move/from16 v11, p8

    .line 131
    .line 132
    invoke-virtual {v4, v6, v11}, Lcom/intsig/tianshu/ParamsBuilder;->〇80〇808〇O(Ljava/lang/String;I)Lcom/intsig/tianshu/ParamsBuilder;

    .line 133
    .line 134
    .line 135
    move-result-object v4

    .line 136
    if-eqz v1, :cond_1

    .line 137
    .line 138
    const-string v6, "scene"

    .line 139
    .line 140
    invoke-virtual {v4, v6, v1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 141
    .line 142
    .line 143
    :cond_1
    if-eqz v2, :cond_2

    .line 144
    .line 145
    const-string v1, "ttype"

    .line 146
    .line 147
    invoke-virtual {v4, v1, v2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 148
    .line 149
    .line 150
    :cond_2
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    const/16 v6, 0x14

    .line 155
    .line 156
    invoke-virtual {v1, v6}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v1

    .line 160
    new-instance v6, Ljava/lang/StringBuilder;

    .line 161
    .line 162
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object v1

    .line 175
    invoke-virtual {v4, v1}, Lcom/intsig/tianshu/ParamsBuilder;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object v1

    .line 179
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 180
    .line 181
    .line 182
    move-result-object v6

    .line 183
    const-string v11, "gen()"

    .line 184
    .line 185
    invoke-static {v6, v11}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    .line 187
    .line 188
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 189
    .line 190
    .line 191
    move-result-object v6

    .line 192
    const-string v11, "this as java.lang.String).toLowerCase()"

    .line 193
    .line 194
    invoke-static {v6, v11}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    .line 196
    .line 197
    if-eqz v9, :cond_4

    .line 198
    .line 199
    invoke-interface/range {p9 .. p9}, Ljava/util/Collection;->isEmpty()Z

    .line 200
    .line 201
    .line 202
    move-result v14

    .line 203
    if-eqz v14, :cond_3

    .line 204
    .line 205
    goto :goto_1

    .line 206
    :cond_3
    const/4 v14, 0x0

    .line 207
    goto :goto_2

    .line 208
    :cond_4
    :goto_1
    const/4 v14, 0x1

    .line 209
    :goto_2
    if-eqz v14, :cond_8

    .line 210
    .line 211
    move-object v14, v8

    .line 212
    check-cast v14, Ljava/util/Collection;

    .line 213
    .line 214
    if-eqz v14, :cond_6

    .line 215
    .line 216
    invoke-interface {v14}, Ljava/util/Collection;->isEmpty()Z

    .line 217
    .line 218
    .line 219
    move-result v14

    .line 220
    if-eqz v14, :cond_5

    .line 221
    .line 222
    goto :goto_3

    .line 223
    :cond_5
    const/4 v14, 0x0

    .line 224
    goto :goto_4

    .line 225
    :cond_6
    :goto_3
    const/4 v14, 0x1

    .line 226
    :goto_4
    if-eqz v14, :cond_7

    .line 227
    .line 228
    new-instance v0, Lcom/intsig/camscanner/pic2word/bean/WordResponse;

    .line 229
    .line 230
    const/16 v16, -0x6f

    .line 231
    .line 232
    const/16 v17, 0x0

    .line 233
    .line 234
    const/16 v18, 0x0

    .line 235
    .line 236
    const/16 v19, 0x6

    .line 237
    .line 238
    const/16 v20, 0x0

    .line 239
    .line 240
    move-object v15, v0

    .line 241
    invoke-direct/range {v15 .. v20}, Lcom/intsig/camscanner/pic2word/bean/WordResponse;-><init>(ILjava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 242
    .line 243
    .line 244
    return-object v0

    .line 245
    :cond_7
    new-instance v14, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$JsonBody;

    .line 246
    .line 247
    invoke-direct {v14, v8}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$JsonBody;-><init>(Ljava/util/List;)V

    .line 248
    .line 249
    .line 250
    goto :goto_5

    .line 251
    :cond_8
    new-instance v14, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$NewJsonBody;

    .line 252
    .line 253
    invoke-direct {v14, v9}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable$NewJsonBody;-><init>(Ljava/util/ArrayList;)V

    .line 254
    .line 255
    .line 256
    :goto_5
    invoke-virtual {v14}, Lokhttp3/RequestBody;->contentLength()J

    .line 257
    .line 258
    .line 259
    move-result-wide v11

    .line 260
    new-instance v15, Ljava/lang/StringBuilder;

    .line 261
    .line 262
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    .line 264
    .line 265
    const-string v13, "json2WordInternal jsonBody size = "

    .line 266
    .line 267
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    invoke-virtual {v15, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 271
    .line 272
    .line 273
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 274
    .line 275
    .line 276
    move-result-object v11

    .line 277
    invoke-static {v10, v11}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    .line 279
    .line 280
    invoke-static {v1}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 281
    .line 282
    .line 283
    move-result-object v1

    .line 284
    const-string v11, "x_request_id"

    .line 285
    .line 286
    invoke-virtual {v1, v11, v6}, Lcom/lzy/okgo/request/base/Request;->headers(Ljava/lang/String;Ljava/lang/String;)Lcom/lzy/okgo/request/base/Request;

    .line 287
    .line 288
    .line 289
    move-result-object v1

    .line 290
    check-cast v1, Lcom/lzy/okgo/request/PostRequest;

    .line 291
    .line 292
    invoke-virtual {v1, v14}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 293
    .line 294
    .line 295
    move-result-object v1

    .line 296
    check-cast v1, Lcom/lzy/okgo/request/PostRequest;

    .line 297
    .line 298
    invoke-virtual {v1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 299
    .line 300
    .line 301
    move-result-object v1

    .line 302
    invoke-virtual {v1}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 303
    .line 304
    .line 305
    move-result-object v6

    .line 306
    if-eqz v2, :cond_a

    .line 307
    .line 308
    invoke-interface/range {p6 .. p6}, Ljava/lang/CharSequence;->length()I

    .line 309
    .line 310
    .line 311
    move-result v2

    .line 312
    if-nez v2, :cond_9

    .line 313
    .line 314
    goto :goto_6

    .line 315
    :cond_9
    const/4 v11, 0x0

    .line 316
    goto :goto_7

    .line 317
    :cond_a
    :goto_6
    const/4 v11, 0x1

    .line 318
    :goto_7
    const/4 v2, 0x0

    .line 319
    if-eqz v11, :cond_b

    .line 320
    .line 321
    move-object v11, v2

    .line 322
    goto :goto_8

    .line 323
    :cond_b
    invoke-virtual {v1}, Lokhttp3/Response;->o800o8O()Lokhttp3/Headers;

    .line 324
    .line 325
    .line 326
    move-result-object v11

    .line 327
    const-string v12, "X-Is-Word-Url"

    .line 328
    .line 329
    invoke-virtual {v11, v12}, Lokhttp3/Headers;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 330
    .line 331
    .line 332
    move-result-object v11

    .line 333
    :goto_8
    invoke-virtual {v4, v3}, Lcom/intsig/tianshu/ParamsBuilder;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 334
    .line 335
    .line 336
    move-result-object v12

    .line 337
    const-string v3, "builder.buildWithPath(TianShuAPI.JSON_TO_WORD)"

    .line 338
    .line 339
    invoke-static {v12, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 340
    .line 341
    .line 342
    :try_start_1
    invoke-virtual {v1}, Lokhttp3/Response;->〇oo〇()Z

    .line 343
    .line 344
    .line 345
    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 346
    const-string v4, "response"

    .line 347
    .line 348
    if-eqz v3, :cond_f

    .line 349
    .line 350
    if-nez v6, :cond_c

    .line 351
    .line 352
    goto/16 :goto_a

    .line 353
    .line 354
    :cond_c
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    .line 355
    .line 356
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 357
    .line 358
    .line 359
    const-string v5, "wordUrl:"

    .line 360
    .line 361
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    .line 363
    .line 364
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    .line 366
    .line 367
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 368
    .line 369
    .line 370
    move-result-object v3

    .line 371
    invoke-static {v10, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    .line 373
    .line 374
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 375
    .line 376
    .line 377
    invoke-direct {v7, v1}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;->〇o00〇〇Oo(Lokhttp3/Response;)I

    .line 378
    .line 379
    .line 380
    move-result v13

    .line 381
    if-nez v13, :cond_e

    .line 382
    .line 383
    invoke-virtual/range {p4 .. p4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    .line 384
    .line 385
    .line 386
    move-result-object v1

    .line 387
    if-eqz v1, :cond_d

    .line 388
    .line 389
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 390
    .line 391
    .line 392
    :cond_d
    invoke-virtual {v6}, Lokhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    .line 393
    .line 394
    .line 395
    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 396
    :try_start_3
    new-instance v3, Ljava/io/FileOutputStream;

    .line 397
    .line 398
    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 399
    .line 400
    .line 401
    const/4 v4, 0x2

    .line 402
    const/4 v5, 0x0

    .line 403
    :try_start_4
    invoke-static {v1, v3, v5, v4, v2}, Lkotlin/io/ByteStreamsKt;->〇o00〇〇Oo(Ljava/io/InputStream;Ljava/io/OutputStream;IILjava/lang/Object;)J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 404
    .line 405
    .line 406
    :try_start_5
    invoke-static {v3, v2}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 407
    .line 408
    .line 409
    :try_start_6
    invoke-static {v1, v2}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 410
    .line 411
    .line 412
    goto :goto_9

    .line 413
    :catchall_0
    move-exception v0

    .line 414
    move-object v2, v0

    .line 415
    :try_start_7
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 416
    :catchall_1
    move-exception v0

    .line 417
    move-object v4, v0

    .line 418
    :try_start_8
    invoke-static {v3, v2}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 419
    .line 420
    .line 421
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 422
    :catchall_2
    move-exception v0

    .line 423
    move-object v2, v0

    .line 424
    :try_start_9
    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 425
    :catchall_3
    move-exception v0

    .line 426
    move-object v3, v0

    .line 427
    :try_start_a
    invoke-static {v1, v2}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 428
    .line 429
    .line 430
    throw v3

    .line 431
    :cond_e
    invoke-static/range {p3 .. p3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 432
    .line 433
    .line 434
    move-object/from16 v1, p0

    .line 435
    .line 436
    move-object/from16 v2, p1

    .line 437
    .line 438
    move-object/from16 v3, p2

    .line 439
    .line 440
    move-object v4, v12

    .line 441
    move-object/from16 v5, p3

    .line 442
    .line 443
    move-object/from16 v6, p9

    .line 444
    .line 445
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;->〇80〇808〇O(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/ArrayList;)V

    .line 446
    .line 447
    .line 448
    :goto_9
    new-instance v1, Lcom/intsig/camscanner/pic2word/bean/WordResponse;

    .line 449
    .line 450
    invoke-virtual/range {p4 .. p4}, Ljava/io/File;->getPath()Ljava/lang/String;

    .line 451
    .line 452
    .line 453
    move-result-object v0

    .line 454
    invoke-direct {v1, v13, v11, v0}, Lcom/intsig/camscanner/pic2word/bean/WordResponse;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    goto :goto_b

    .line 458
    :cond_f
    :goto_a
    const-string v0, "response fail"

    .line 459
    .line 460
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    .line 462
    .line 463
    new-instance v0, Lcom/intsig/camscanner/pic2word/bean/WordResponse;

    .line 464
    .line 465
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 466
    .line 467
    .line 468
    invoke-direct {v7, v1}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;->〇o00〇〇Oo(Lokhttp3/Response;)I

    .line 469
    .line 470
    .line 471
    move-result v14

    .line 472
    const/4 v15, 0x0

    .line 473
    const/16 v16, 0x0

    .line 474
    .line 475
    const/16 v17, 0x6

    .line 476
    .line 477
    const/16 v18, 0x0

    .line 478
    .line 479
    move-object v13, v0

    .line 480
    invoke-direct/range {v13 .. v18}, Lcom/intsig/camscanner/pic2word/bean/WordResponse;-><init>(ILjava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    .line 481
    .line 482
    .line 483
    move-object v1, v0

    .line 484
    :goto_b
    return-object v1

    .line 485
    :catch_0
    move-exception v0

    .line 486
    move-object v4, v12

    .line 487
    goto :goto_c

    .line 488
    :catch_1
    move-exception v0

    .line 489
    move-object v4, v5

    .line 490
    :goto_c
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 491
    .line 492
    .line 493
    invoke-static/range {p3 .. p3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 494
    .line 495
    .line 496
    move-object/from16 v1, p0

    .line 497
    .line 498
    move-object/from16 v2, p1

    .line 499
    .line 500
    move-object/from16 v3, p2

    .line 501
    .line 502
    move-object/from16 v5, p3

    .line 503
    .line 504
    move-object/from16 v6, p9

    .line 505
    .line 506
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/pic2word/presenter/Json2WordCallable;->〇80〇808〇O(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/ArrayList;)V

    .line 507
    .line 508
    .line 509
    new-instance v0, Lcom/intsig/camscanner/pic2word/bean/WordResponse;

    .line 510
    .line 511
    const/16 v1, -0x6f

    .line 512
    .line 513
    const/4 v2, 0x0

    .line 514
    const/4 v3, 0x0

    .line 515
    const/4 v4, 0x6

    .line 516
    const/4 v5, 0x0

    .line 517
    move-object/from16 p1, v0

    .line 518
    .line 519
    move/from16 p2, v1

    .line 520
    .line 521
    move-object/from16 p3, v2

    .line 522
    .line 523
    move-object/from16 p4, v3

    .line 524
    .line 525
    move/from16 p5, v4

    .line 526
    .line 527
    move-object/from16 p6, v5

    .line 528
    .line 529
    invoke-direct/range {p1 .. p6}, Lcom/intsig/camscanner/pic2word/bean/WordResponse;-><init>(ILjava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 530
    .line 531
    .line 532
    return-object v0
.end method

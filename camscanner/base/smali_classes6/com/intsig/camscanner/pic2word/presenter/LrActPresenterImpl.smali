.class public final Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;
.super Ljava/lang/Object;
.source "LrActPresenterImpl.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;,
        Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8o8o〇:Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:J

.field private OO0o〇〇〇〇0:J

.field private Oo08:Z

.field private oO80:Lcom/intsig/camscanner/loadimage/PageImage;

.field private o〇0:Z

.field private final 〇080:Lcom/intsig/camscanner/pic2word/LrActView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:Landroid/animation/Animator$AnimatorListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/capture/certificatephoto/util/SimpleCustomAsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/capture/certificatephoto/util/SimpleCustomAsyncTask<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Lcom/intsig/camscanner/pic2word/lr/LrImageJson;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o〇:Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;

.field private 〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇8o8o〇:Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/pic2word/LrActView;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pic2word/LrActView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇080:Lcom/intsig/camscanner/pic2word/LrActView;

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$mAnimListener$1;

    .line 12
    .line 13
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$mAnimListener$1;-><init>(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;)V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇80〇808〇O:Landroid/animation/Animator$AnimatorListener;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO0o〇〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇080:Lcom/intsig/camscanner/pic2word/LrActView;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pic2word/LrActView;->O0oo0o0〇()Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇80〇808〇O:Landroid/animation/Animator$AnimatorListener;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->addAnimListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->Oo08:Z

    .line 14
    .line 15
    iput-boolean v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->o〇0:Z

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->oO80:Lcom/intsig/camscanner/loadimage/PageImage;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;Lcom/intsig/camscanner/loadimage/PageImage;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->oO80:Lcom/intsig/camscanner/loadimage/PageImage;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oO80()V
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->o〇0:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->oO80:Lcom/intsig/camscanner/loadimage/PageImage;

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    const/4 v2, 0x0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 13
    .line 14
    if-eqz v3, :cond_1

    .line 15
    .line 16
    const-string v3, "pic_2_office"

    .line 17
    .line 18
    invoke-static {v3}, Lcom/intsig/camscanner/pic2word/util/LrUtil;->〇O8o08O(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/util/LrUtil;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    iget-object v5, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 27
    .line 28
    invoke-virtual {v3, v4, v5}, Lcom/intsig/camscanner/pic2word/util/LrUtil;->〇080(Ljava/lang/String;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)Z

    .line 29
    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇080:Lcom/intsig/camscanner/pic2word/LrActView;

    .line 32
    .line 33
    iget-wide v4, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->O8:J

    .line 34
    .line 35
    invoke-interface {v3, v1, v4, v5}, Lcom/intsig/camscanner/pic2word/LrActView;->〇〇o〇(ZJ)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇080:Lcom/intsig/camscanner/pic2word/LrActView;

    .line 40
    .line 41
    iget-wide v4, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->O8:J

    .line 42
    .line 43
    invoke-interface {v3, v2, v4, v5}, Lcom/intsig/camscanner/pic2word/LrActView;->〇〇o〇(ZJ)V

    .line 44
    .line 45
    .line 46
    const/4 v2, 0x2

    .line 47
    :goto_0
    if-nez v0, :cond_2

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_2
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/loadimage/PageImage;->o8(I)V

    .line 51
    .line 52
    .line 53
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇080:Lcom/intsig/camscanner/pic2word/LrActView;

    .line 54
    .line 55
    iget-object v2, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 56
    .line 57
    invoke-interface {v0, v2}, Lcom/intsig/camscanner/pic2word/LrActView;->〇0O〇O00O(Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇o〇:Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;

    .line 61
    .line 62
    if-eqz v0, :cond_3

    .line 63
    .line 64
    invoke-interface {v0}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;->finish()V

    .line 65
    .line 66
    .line 67
    :cond_3
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->OO0o〇〇〇〇0(Z)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->O8:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->oO80()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇80〇808〇O()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v0, "LrTempHighLight.png"

    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->OO0o〇〇〇〇0(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;)Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇o〇:Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->OO0o〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final OO0o〇〇〇〇0(Z)V
    .locals 12

    .line 1
    const-string v0, "LrActPresenterImpl"

    .line 2
    .line 3
    const-string v1, "hideGalaxy"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇080:Lcom/intsig/camscanner/pic2word/LrActView;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pic2word/LrActView;->O0oo0o0〇()Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_6

    .line 15
    .line 16
    iget-wide v1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->OO0o〇〇〇〇0:J

    .line 17
    .line 18
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    .line 23
    .line 24
    .line 25
    move-result-wide v2

    .line 26
    const/4 v4, 0x1

    .line 27
    const/4 v5, 0x0

    .line 28
    const-wide/16 v6, 0x0

    .line 29
    .line 30
    cmp-long v8, v2, v6

    .line 31
    .line 32
    if-lez v8, :cond_0

    .line 33
    .line 34
    const/4 v2, 0x1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    const/4 v2, 0x0

    .line 37
    :goto_0
    const/4 v3, 0x0

    .line 38
    if-eqz v2, :cond_1

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_1
    move-object v1, v3

    .line 42
    :goto_1
    if-eqz v1, :cond_5

    .line 43
    .line 44
    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    .line 45
    .line 46
    .line 47
    move-result-wide v1

    .line 48
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 49
    .line 50
    .line 51
    move-result-wide v8

    .line 52
    sub-long/2addr v8, v1

    .line 53
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    .line 58
    .line 59
    .line 60
    move-result-wide v8

    .line 61
    cmp-long v2, v8, v6

    .line 62
    .line 63
    if-lez v2, :cond_2

    .line 64
    .line 65
    const/4 v2, 0x1

    .line 66
    goto :goto_2

    .line 67
    :cond_2
    const/4 v2, 0x0

    .line 68
    :goto_2
    if-eqz v2, :cond_3

    .line 69
    .line 70
    goto :goto_3

    .line 71
    :cond_3
    move-object v1, v3

    .line 72
    :goto_3
    if-eqz v1, :cond_4

    .line 73
    .line 74
    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    .line 75
    .line 76
    .line 77
    move-result-wide v1

    .line 78
    const/4 v8, 0x2

    .line 79
    new-array v8, v8, [Landroid/util/Pair;

    .line 80
    .line 81
    new-instance v9, Landroid/util/Pair;

    .line 82
    .line 83
    const-string v10, "from"

    .line 84
    .line 85
    const-string v11, "cs_detail_to_word"

    .line 86
    .line 87
    invoke-direct {v9, v10, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 88
    .line 89
    .line 90
    aput-object v9, v8, v5

    .line 91
    .line 92
    new-instance v5, Landroid/util/Pair;

    .line 93
    .line 94
    const-string v9, "time"

    .line 95
    .line 96
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    invoke-direct {v5, v9, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 101
    .line 102
    .line 103
    aput-object v5, v8, v4

    .line 104
    .line 105
    const-string v1, "CSWaiting"

    .line 106
    .line 107
    const-string v2, "show"

    .line 108
    .line 109
    invoke-static {v1, v2, v8}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 110
    .line 111
    .line 112
    :cond_4
    iput-wide v6, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->OO0o〇〇〇〇0:J

    .line 113
    .line 114
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇80〇808〇O:Landroid/animation/Animator$AnimatorListener;

    .line 115
    .line 116
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->removeAnimListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 117
    .line 118
    .line 119
    const/16 v1, 0x8

    .line 120
    .line 121
    invoke-virtual {v0, v1, v3, v3, p1}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setVisibility(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    .line 122
    .line 123
    .line 124
    :cond_6
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final Oooo8o0〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->Oo08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇O8o08O(Lcom/intsig/camscanner/loadimage/PageImage;Ljava/lang/String;Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Lcom/intsig/camscanner/loadimage/PageImage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "pageImage"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    new-instance v0, Ljava/io/File;

    .line 14
    .line 15
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 16
    .line 17
    .line 18
    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    return-void

    .line 28
    :cond_1
    iput-object p3, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇o〇:Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;

    .line 29
    .line 30
    if-eqz p3, :cond_2

    .line 31
    .line 32
    invoke-interface {p3}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$RequestStatusListener;->〇080()Z

    .line 33
    .line 34
    .line 35
    move-result p3

    .line 36
    if-nez p3, :cond_2

    .line 37
    .line 38
    return-void

    .line 39
    :cond_2
    new-instance p3, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$requestJson$task$1;

    .line 40
    .line 41
    move-object v0, p3

    .line 42
    move-object v1, p0

    .line 43
    move-object v2, p2

    .line 44
    move-object v3, p1

    .line 45
    move-object v4, p4

    .line 46
    move v5, p5

    .line 47
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl$requestJson$task$1;-><init>(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;Ljava/lang/String;Lcom/intsig/camscanner/loadimage/PageImage;Ljava/lang/String;Z)V

    .line 48
    .line 49
    .line 50
    iput-object p3, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/certificatephoto/util/SimpleCustomAsyncTask;

    .line 51
    .line 52
    const-string p1, "LrActPresenterImpl"

    .line 53
    .line 54
    invoke-virtual {p3, p1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oooo8o0〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇〇888()V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public final 〇O〇(Ljava/lang/String;)Z
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇080:Lcom/intsig/camscanner/pic2word/LrActView;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pic2word/LrActView;->O0oo0o0〇()Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_5

    .line 9
    .line 10
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    goto/16 :goto_2

    .line 17
    .line 18
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    .line 19
    .line 20
    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 21
    .line 22
    .line 23
    const/4 v2, 0x1

    .line 24
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 25
    .line 26
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 27
    .line 28
    .line 29
    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 30
    .line 31
    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 32
    .line 33
    new-instance v5, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v6, "showGalaxy ori bitmapWidth="

    .line 39
    .line 40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v6, " bitmapHeight="

    .line 47
    .line 48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v5

    .line 58
    const-string v7, "LrActPresenterImpl"

    .line 59
    .line 60
    invoke-static {v7, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    const/4 v5, 0x2

    .line 64
    if-lez v3, :cond_2

    .line 65
    .line 66
    if-lez v4, :cond_2

    .line 67
    .line 68
    invoke-static {}, Lcom/intsig/utils/DisplayUtil;->〇8o8o〇()[I

    .line 69
    .line 70
    .line 71
    move-result-object v8

    .line 72
    aget v9, v8, v1

    .line 73
    .line 74
    if-lez v9, :cond_3

    .line 75
    .line 76
    aget v8, v8, v2

    .line 77
    .line 78
    if-lez v8, :cond_3

    .line 79
    .line 80
    int-to-float v9, v9

    .line 81
    const/high16 v10, 0x3f800000    # 1.0f

    .line 82
    .line 83
    mul-float v9, v9, v10

    .line 84
    .line 85
    int-to-float v3, v3

    .line 86
    div-float/2addr v9, v3

    .line 87
    int-to-float v3, v8

    .line 88
    mul-float v3, v3, v10

    .line 89
    .line 90
    int-to-float v4, v4

    .line 91
    div-float/2addr v3, v4

    .line 92
    invoke-static {v9, v3}, Ljava/lang/Math;->min(FF)F

    .line 93
    .line 94
    .line 95
    move-result v3

    .line 96
    int-to-float v4, v2

    .line 97
    div-float/2addr v4, v3

    .line 98
    float-to-int v3, v4

    .line 99
    if-nez v3, :cond_1

    .line 100
    .line 101
    const/4 v3, 0x1

    .line 102
    :cond_1
    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 103
    .line 104
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 105
    .line 106
    :try_start_0
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 107
    .line 108
    .line 109
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    goto :goto_1

    .line 111
    :catch_0
    mul-int/lit8 v3, v3, 0x2

    .line 112
    .line 113
    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 114
    .line 115
    :try_start_1
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 116
    .line 117
    .line 118
    move-result-object v0
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 119
    goto :goto_1

    .line 120
    :catch_1
    move-exception v0

    .line 121
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 122
    .line 123
    .line 124
    goto :goto_0

    .line 125
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 126
    .line 127
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    .line 129
    .line 130
    const-string v8, "error bitmapWidth="

    .line 131
    .line 132
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    :cond_3
    :goto_0
    const/4 v0, 0x0

    .line 152
    :goto_1
    if-nez v0, :cond_4

    .line 153
    .line 154
    return v1

    .line 155
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 156
    .line 157
    .line 158
    move-result-wide v3

    .line 159
    iput-wide v3, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->OO0o〇〇〇〇0:J

    .line 160
    .line 161
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 162
    .line 163
    .line 164
    move-result v3

    .line 165
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 166
    .line 167
    .line 168
    move-result v4

    .line 169
    new-instance v8, Ljava/lang/StringBuilder;

    .line 170
    .line 171
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    .line 173
    .line 174
    const-string v9, "showGalaxy fin bitmapWidth="

    .line 175
    .line 176
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 186
    .line 187
    .line 188
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 189
    .line 190
    .line 191
    move-result-object v3

    .line 192
    invoke-static {v7, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    new-array v3, v5, [I

    .line 196
    .line 197
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 198
    .line 199
    .line 200
    move-result v4

    .line 201
    aput v4, v3, v1

    .line 202
    .line 203
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 204
    .line 205
    .line 206
    move-result v4

    .line 207
    aput v4, v3, v2

    .line 208
    .line 209
    invoke-direct {p0}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇80〇808〇O()Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object v4

    .line 213
    invoke-static {p1, v4, v3}, Lcom/intsig/nativelib/HighLightLineHelper;->getLrEdgeFile(Ljava/lang/String;Ljava/lang/String;[I)I

    .line 214
    .line 215
    .line 216
    move-result p1

    .line 217
    if-ltz p1, :cond_5

    .line 218
    .line 219
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 220
    .line 221
    .line 222
    move-result-object p1

    .line 223
    if-eqz p1, :cond_5

    .line 224
    .line 225
    iget-object v3, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇080:Lcom/intsig/camscanner/pic2word/LrActView;

    .line 226
    .line 227
    invoke-interface {v3, v1, v0, p1}, Lcom/intsig/camscanner/pic2word/LrActView;->Oo〇o(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 228
    .line 229
    .line 230
    return v2

    .line 231
    :cond_5
    :goto_2
    return v1
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇〇808〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇888()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/capture/certificatephoto/util/SimpleCustomAsyncTask;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->〇o〇()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    const-string v1, "LrActPresenterImpl"

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 16
    const/4 v1, 0x0

    .line 17
    const/4 v2, 0x0

    .line 18
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;->〇8o8o〇(Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;ZILjava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    return-void
.end method

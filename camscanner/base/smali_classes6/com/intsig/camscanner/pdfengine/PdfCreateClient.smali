.class public Lcom/intsig/camscanner/pdfengine/PdfCreateClient;
.super Ljava/lang/Object;
.source "PdfCreateClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String; = "PdfCreateClient"


# instance fields
.field private final applicationContext:Landroid/content/Context;

.field private onPdfCreateListener:Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;

.field private pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->applicationContext:Landroid/content/Context;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private addNote(Lcom/intsig/pdfengine/PDF_Engine;Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;Ljava/lang/String;)V
    .locals 7

    .line 1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    const-string v0, "UTF-8"

    .line 8
    .line 9
    invoke-static {p3, v0}, Lcom/intsig/camscanner/app/AppUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    move-object v2, v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v2, p3

    .line 18
    :goto_0
    iget p3, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 19
    .line 20
    const/high16 v0, 0x42200000    # 40.0f

    .line 21
    .line 22
    sub-float v3, p3, v0

    .line 23
    .line 24
    iget p2, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 25
    .line 26
    sub-float v4, p2, v0

    .line 27
    .line 28
    const/high16 v0, 0x41a00000    # 20.0f

    .line 29
    .line 30
    sub-float v5, p3, v0

    .line 31
    .line 32
    sub-float v6, p2, v0

    .line 33
    .line 34
    move-object v1, p1

    .line 35
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/pdfengine/PDF_Engine;->addNote(Ljava/lang/String;FFFF)V

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private getQrcodeImagePath()Ljava/lang/String;
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇〇808〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_2

    .line 7
    .line 8
    new-instance v0, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getPicAddress()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    new-instance v2, Ljava/io/File;

    .line 32
    .line 33
    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    if-nez v3, :cond_0

    .line 41
    .line 42
    :try_start_0
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->applicationContext:Landroid/content/Context;

    .line 43
    .line 44
    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getPicAddress()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    invoke-static {v3, v0}, Lcom/intsig/utils/FileUtil;->〇〇888(Ljava/io/InputStream;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :catch_0
    move-exception v3

    .line 61
    sget-object v4, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->TAG:Ljava/lang/String;

    .line 62
    .line 63
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 64
    .line 65
    .line 66
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    .line 67
    .line 68
    .line 69
    move-result v2

    .line 70
    if-nez v2, :cond_1

    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_1
    move-object v1, v0

    .line 74
    :cond_2
    :goto_1
    return-object v1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private loadPdfPageProperty(Ljava/lang/String;Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;)V
    .locals 6

    .line 1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 8
    .line 9
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 10
    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->getPdfSizes()[I

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 19
    .line 20
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdfengine/PdfData;->isAutoAdjust()Z

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    const/4 v4, 0x0

    .line 25
    if-eqz v3, :cond_0

    .line 26
    .line 27
    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 28
    .line 29
    aput v3, v2, v4

    .line 30
    .line 31
    iget v3, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 32
    .line 33
    aput v3, v2, v1

    .line 34
    .line 35
    :cond_0
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 36
    .line 37
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdfengine/PdfData;->getOrientation()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    const/4 v5, 0x2

    .line 42
    if-ne v3, v5, :cond_1

    .line 43
    .line 44
    aget v1, v2, v1

    .line 45
    .line 46
    int-to-float v1, v1

    .line 47
    iput v1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 48
    .line 49
    aget v1, v2, v4

    .line 50
    .line 51
    int-to-float v1, v1

    .line 52
    iput v1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    aget v3, v2, v4

    .line 56
    .line 57
    int-to-float v3, v3

    .line 58
    iput v3, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 59
    .line 60
    aget v1, v2, v1

    .line 61
    .line 62
    int-to-float v1, v1

    .line 63
    iput v1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 64
    .line 65
    :goto_0
    invoke-static {p1}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 66
    .line 67
    .line 68
    move-result p1

    .line 69
    rsub-int p1, p1, 0x168

    .line 70
    .line 71
    iput p1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->rotation:I

    .line 72
    .line 73
    const/16 v1, 0x5a

    .line 74
    .line 75
    if-eq p1, v1, :cond_3

    .line 76
    .line 77
    const/16 v1, 0x10e

    .line 78
    .line 79
    if-ne p1, v1, :cond_2

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_2
    iget p1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 83
    .line 84
    int-to-float p1, p1

    .line 85
    iput p1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageWidth:F

    .line 86
    .line 87
    iget p1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 88
    .line 89
    int-to-float p1, p1

    .line 90
    iput p1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageHeight:F

    .line 91
    .line 92
    goto :goto_2

    .line 93
    :cond_3
    :goto_1
    iget p1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 94
    .line 95
    int-to-float p1, p1

    .line 96
    iput p1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageWidth:F

    .line 97
    .line 98
    iget p1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 99
    .line 100
    int-to-float p1, p1

    .line 101
    iput p1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageHeight:F

    .line 102
    .line 103
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/PdfData;->getOrientation()I

    .line 106
    .line 107
    .line 108
    move-result p1

    .line 109
    if-eqz p1, :cond_4

    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 112
    .line 113
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/PdfData;->isAutoAdjust()Z

    .line 114
    .line 115
    .line 116
    move-result p1

    .line 117
    if-eqz p1, :cond_6

    .line 118
    .line 119
    :cond_4
    iget p1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 120
    .line 121
    iget v0, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 122
    .line 123
    cmpl-float v1, p1, v0

    .line 124
    .line 125
    if-lez v1, :cond_5

    .line 126
    .line 127
    iget v1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageHeight:F

    .line 128
    .line 129
    iget v2, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageWidth:F

    .line 130
    .line 131
    cmpl-float v1, v1, v2

    .line 132
    .line 133
    if-lez v1, :cond_6

    .line 134
    .line 135
    iput v0, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 136
    .line 137
    iput p1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 138
    .line 139
    goto :goto_3

    .line 140
    :cond_5
    iget v1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageWidth:F

    .line 141
    .line 142
    iget v2, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageHeight:F

    .line 143
    .line 144
    cmpl-float v1, v1, v2

    .line 145
    .line 146
    if-lez v1, :cond_6

    .line 147
    .line 148
    iput v0, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 149
    .line 150
    iput p1, p2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 151
    .line 152
    :cond_6
    :goto_3
    return-void
    .line 153
    .line 154
.end method

.method private setPdfPassword(Lcom/intsig/pdfengine/PDF_Engine;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PdfData;->getPassword()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    :try_start_0
    const-string v1, "000000000000000"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/crypto/CryptoUtil;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    goto :goto_0

    .line 20
    :catch_0
    move-exception v1

    .line 21
    sget-object v2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->TAG:Ljava/lang/String;

    .line 22
    .line 23
    new-instance v3, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v4, "PDF password:"

    .line 29
    .line 30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    invoke-static {v2, v3, v1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 41
    .line 42
    .line 43
    :goto_0
    invoke-virtual {p1, v0, v0}, Lcom/intsig/pdfengine/PDF_Engine;->setPassword(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    .line 45
    .line 46
    :cond_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public createPdf()Z
    .locals 29

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 4
    .line 5
    if-eqz v1, :cond_10

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/PdfData;->checkParameter()V

    .line 8
    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/pdfengine/PDF_Engine;->getInstance()Lcom/intsig/pdfengine/PDF_Engine;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const/4 v9, 0x0

    .line 15
    const/4 v10, 0x0

    .line 16
    if-nez v1, :cond_1

    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->onPdfCreateListener:Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;

    .line 19
    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    sget v2, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_LOAD_ENGINE:I

    .line 23
    .line 24
    invoke-interface {v1, v2, v9}, Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->TAG:Ljava/lang/String;

    .line 28
    .line 29
    const-string v2, "engine == null"

    .line 30
    .line 31
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return v10

    .line 35
    :cond_1
    const/4 v11, 0x2

    .line 36
    invoke-virtual {v1, v11}, Lcom/intsig/pdfengine/PDF_Engine;->setLogLevel(I)V

    .line 37
    .line 38
    .line 39
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 40
    .line 41
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->getSavePdfFile()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    const/4 v12, 0x0

    .line 46
    invoke-virtual {v1, v2, v12, v12}, Lcom/intsig/pdfengine/PDF_Engine;->createCanvas(Ljava/lang/String;FF)I

    .line 47
    .line 48
    .line 49
    new-instance v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;

    .line 50
    .line 51
    invoke-direct {v13, v9}, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;-><init>(Lcom/intsig/camscanner/pdfengine/o〇0;)V

    .line 52
    .line 53
    .line 54
    const/4 v2, 0x5

    .line 55
    new-array v8, v2, [F

    .line 56
    .line 57
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 58
    .line 59
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->isNeedWaterMark()Z

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    if-eqz v2, :cond_3

    .line 64
    .line 65
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->getQrcodeImagePath()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    if-eqz v3, :cond_2

    .line 74
    .line 75
    const/high16 v3, 0x42340000    # 45.0f

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_2
    const/high16 v3, 0x41c80000    # 25.0f

    .line 79
    .line 80
    :goto_0
    move-object/from16 v22, v2

    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_3
    move-object/from16 v22, v9

    .line 84
    .line 85
    const/4 v3, 0x0

    .line 86
    :goto_1
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 87
    .line 88
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->isNoPadding()Z

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    if-eqz v2, :cond_4

    .line 93
    .line 94
    const/16 v23, 0x0

    .line 95
    .line 96
    goto :goto_2

    .line 97
    :cond_4
    const/high16 v2, 0x41a00000    # 20.0f

    .line 98
    .line 99
    const/high16 v23, 0x41a00000    # 20.0f

    .line 100
    .line 101
    :goto_2
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 102
    .line 103
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->getSharePagePropertyList()Ljava/util/List;

    .line 104
    .line 105
    .line 106
    move-result-object v24

    .line 107
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 108
    .line 109
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->getPdfSizes()[I

    .line 110
    .line 111
    .line 112
    move-result-object v25

    .line 113
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 114
    .line 115
    .line 116
    move-result-object v26

    .line 117
    const/16 v27, 0x1

    .line 118
    .line 119
    move/from16 v19, v3

    .line 120
    .line 121
    const/4 v7, 0x0

    .line 122
    const/4 v14, 0x1

    .line 123
    :goto_3
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    .line 124
    .line 125
    .line 126
    move-result v2

    .line 127
    if-eqz v2, :cond_a

    .line 128
    .line 129
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 130
    .line 131
    .line 132
    move-result-object v2

    .line 133
    move-object v6, v2

    .line 134
    check-cast v6, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;

    .line 135
    .line 136
    invoke-static {v13}, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->〇080(Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;)V

    .line 137
    .line 138
    .line 139
    iget-object v2, v6, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o0:Ljava/lang/String;

    .line 140
    .line 141
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 142
    .line 143
    .line 144
    move-result v2

    .line 145
    if-eqz v2, :cond_5

    .line 146
    .line 147
    const/16 v28, 0x0

    .line 148
    .line 149
    goto :goto_4

    .line 150
    :cond_5
    move/from16 v28, v14

    .line 151
    .line 152
    :goto_4
    iget-object v2, v6, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o0:Ljava/lang/String;

    .line 153
    .line 154
    invoke-direct {v0, v2, v13}, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->loadPdfPageProperty(Ljava/lang/String;Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;)V

    .line 155
    .line 156
    .line 157
    iget v2, v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 158
    .line 159
    iget v3, v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 160
    .line 161
    invoke-virtual {v1, v2, v3}, Lcom/intsig/pdfengine/PDF_Engine;->newPage(FF)V

    .line 162
    .line 163
    .line 164
    const/high16 v2, -0x1000000

    .line 165
    .line 166
    invoke-virtual {v1, v2}, Lcom/intsig/pdfengine/PDF_Engine;->setStrokeColor(I)V

    .line 167
    .line 168
    .line 169
    aget v2, v25, v10

    .line 170
    .line 171
    int-to-float v2, v2

    .line 172
    aget v3, v25, v27

    .line 173
    .line 174
    int-to-float v3, v3

    .line 175
    invoke-static {v2, v3}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getFontSize(FF)F

    .line 176
    .line 177
    .line 178
    move-result v2

    .line 179
    const-string v3, "freescan"

    .line 180
    .line 181
    invoke-virtual {v1, v3, v2}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    .line 182
    .line 183
    .line 184
    iget v14, v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 185
    .line 186
    iget v15, v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 187
    .line 188
    iget v2, v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageWidth:F

    .line 189
    .line 190
    iget v3, v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageHeight:F

    .line 191
    .line 192
    const/16 v21, 0x1

    .line 193
    .line 194
    move/from16 v16, v23

    .line 195
    .line 196
    move/from16 v17, v2

    .line 197
    .line 198
    move/from16 v18, v3

    .line 199
    .line 200
    move-object/from16 v20, v8

    .line 201
    .line 202
    invoke-static/range {v14 .. v21}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->parapdfImageInfo(FFFFFF[FZ)V

    .line 203
    .line 204
    .line 205
    aget v14, v8, v10

    .line 206
    .line 207
    aget v15, v8, v27

    .line 208
    .line 209
    aget v16, v8, v11

    .line 210
    .line 211
    const/4 v2, 0x3

    .line 212
    aget v17, v8, v2

    .line 213
    .line 214
    const/4 v2, 0x4

    .line 215
    aget v19, v8, v2

    .line 216
    .line 217
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o8oO〇()Z

    .line 218
    .line 219
    .line 220
    move-result v2

    .line 221
    if-nez v2, :cond_8

    .line 222
    .line 223
    cmpl-float v2, v19, v12

    .line 224
    .line 225
    if-lez v2, :cond_8

    .line 226
    .line 227
    invoke-virtual {v1, v10}, Lcom/intsig/pdfengine/PDF_Engine;->setTextRenderingMode(I)V

    .line 228
    .line 229
    .line 230
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->applicationContext:Landroid/content/Context;

    .line 231
    .line 232
    const v3, 0x7f13066a

    .line 233
    .line 234
    .line 235
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 236
    .line 237
    .line 238
    move-result-object v3

    .line 239
    sget-object v2, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->TAG:Ljava/lang/String;

    .line 240
    .line 241
    new-instance v4, Ljava/lang/StringBuilder;

    .line 242
    .line 243
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    .line 245
    .line 246
    const-string v5, "pdfWaterMark="

    .line 247
    .line 248
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    .line 250
    .line 251
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    .line 253
    .line 254
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 255
    .line 256
    .line 257
    move-result-object v4

    .line 258
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    .line 260
    .line 261
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 262
    .line 263
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->isNoPadding()Z

    .line 264
    .line 265
    .line 266
    move-result v2

    .line 267
    const/high16 v4, 0x41200000    # 10.0f

    .line 268
    .line 269
    if-eqz v2, :cond_6

    .line 270
    .line 271
    const/high16 v5, 0x41200000    # 10.0f

    .line 272
    .line 273
    goto :goto_5

    .line 274
    :cond_6
    sub-float v2, v23, v4

    .line 275
    .line 276
    move v5, v2

    .line 277
    :goto_5
    iget v2, v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 278
    .line 279
    sub-float/2addr v2, v4

    .line 280
    const/high16 v4, 0x432a0000    # 170.0f

    .line 281
    .line 282
    sub-float v18, v2, v4

    .line 283
    .line 284
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 285
    .line 286
    .line 287
    move-result v2

    .line 288
    if-eqz v2, :cond_7

    .line 289
    .line 290
    aget v2, v25, v10

    .line 291
    .line 292
    int-to-float v2, v2

    .line 293
    aget v4, v25, v27

    .line 294
    .line 295
    int-to-float v4, v4

    .line 296
    invoke-static {v2, v4}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getFontSize(FF)F

    .line 297
    .line 298
    .line 299
    move-result v2

    .line 300
    const-string v4, "Times-Roman"

    .line 301
    .line 302
    invoke-virtual {v1, v4, v2}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    .line 303
    .line 304
    .line 305
    const/16 v20, 0x0

    .line 306
    .line 307
    const/16 v21, 0x0

    .line 308
    .line 309
    move-object v2, v1

    .line 310
    move/from16 v4, v18

    .line 311
    .line 312
    move-object v10, v6

    .line 313
    move/from16 v6, v20

    .line 314
    .line 315
    move v11, v7

    .line 316
    move/from16 v7, v21

    .line 317
    .line 318
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/pdfengine/PDF_Engine;->drawText(Ljava/lang/String;FFFI)V

    .line 319
    .line 320
    .line 321
    goto :goto_6

    .line 322
    :cond_7
    move-object v10, v6

    .line 323
    move v11, v7

    .line 324
    const/high16 v2, 0x40a00000    # 5.0f

    .line 325
    .line 326
    sub-float v21, v19, v2

    .line 327
    .line 328
    const/high16 v2, 0x40000000    # 2.0f

    .line 329
    .line 330
    div-float v5, v21, v2

    .line 331
    .line 332
    const/4 v6, 0x0

    .line 333
    const/4 v7, 0x0

    .line 334
    move-object v2, v1

    .line 335
    move/from16 v4, v18

    .line 336
    .line 337
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/pdfengine/PDF_Engine;->drawText(Ljava/lang/String;FFFI)V

    .line 338
    .line 339
    .line 340
    sub-float v4, v18, v19

    .line 341
    .line 342
    const/high16 v5, 0x40a00000    # 5.0f

    .line 343
    .line 344
    const/16 v18, 0x0

    .line 345
    .line 346
    move-object/from16 v3, v22

    .line 347
    .line 348
    move/from16 v6, v21

    .line 349
    .line 350
    move/from16 v7, v21

    .line 351
    .line 352
    move-object/from16 v21, v8

    .line 353
    .line 354
    move/from16 v8, v18

    .line 355
    .line 356
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/pdfengine/PDF_Engine;->drawImage(Ljava/lang/String;FFFFF)V

    .line 357
    .line 358
    .line 359
    goto :goto_7

    .line 360
    :cond_8
    move-object v10, v6

    .line 361
    move v11, v7

    .line 362
    :goto_6
    move-object/from16 v21, v8

    .line 363
    .line 364
    :goto_7
    iget-object v3, v10, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->o0:Ljava/lang/String;

    .line 365
    .line 366
    iget v2, v13, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->rotation:I

    .line 367
    .line 368
    int-to-float v8, v2

    .line 369
    move-object v2, v1

    .line 370
    move v4, v14

    .line 371
    move v5, v15

    .line 372
    move/from16 v6, v16

    .line 373
    .line 374
    move/from16 v7, v17

    .line 375
    .line 376
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/pdfengine/PDF_Engine;->drawImage(Ljava/lang/String;FFFFF)V

    .line 377
    .line 378
    .line 379
    iget-object v2, v10, Lcom/intsig/camscanner/share/data_mode/SharePageProperty;->〇08O〇00〇o:Ljava/lang/String;

    .line 380
    .line 381
    invoke-direct {v0, v1, v13, v2}, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->addNote(Lcom/intsig/pdfengine/PDF_Engine;Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;Ljava/lang/String;)V

    .line 382
    .line 383
    .line 384
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->onPdfCreateListener:Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;

    .line 385
    .line 386
    if-eqz v2, :cond_9

    .line 387
    .line 388
    invoke-interface {v2, v11}, Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;->onProgress(I)V

    .line 389
    .line 390
    .line 391
    :cond_9
    add-int/lit8 v7, v11, 0x1

    .line 392
    .line 393
    move-object/from16 v8, v21

    .line 394
    .line 395
    move/from16 v14, v28

    .line 396
    .line 397
    const/4 v10, 0x0

    .line 398
    const/4 v11, 0x2

    .line 399
    goto/16 :goto_3

    .line 400
    .line 401
    :cond_a
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->setPdfPassword(Lcom/intsig/pdfengine/PDF_Engine;)V

    .line 402
    .line 403
    .line 404
    const-string v3, "intsig.com pdf producer"

    .line 405
    .line 406
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 407
    .line 408
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->getTitleStr()Ljava/lang/String;

    .line 409
    .line 410
    .line 411
    move-result-object v4

    .line 412
    const-string v5, "CamScanner"

    .line 413
    .line 414
    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 415
    .line 416
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/PdfData;->getTitleStr()Ljava/lang/String;

    .line 417
    .line 418
    .line 419
    move-result-object v6

    .line 420
    const/4 v7, 0x0

    .line 421
    const/4 v8, 0x0

    .line 422
    move-object v2, v1

    .line 423
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/pdfengine/PDF_Engine;->setProperties(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    .line 425
    .line 426
    invoke-virtual {v1}, Lcom/intsig/pdfengine/PDF_Engine;->save2PdfFile()I

    .line 427
    .line 428
    .line 429
    move-result v2

    .line 430
    iget-object v3, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->onPdfCreateListener:Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;

    .line 431
    .line 432
    if-eqz v3, :cond_b

    .line 433
    .line 434
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    .line 435
    .line 436
    .line 437
    move-result v4

    .line 438
    invoke-interface {v3, v4}, Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;->onProgress(I)V

    .line 439
    .line 440
    .line 441
    :cond_b
    invoke-virtual {v1}, Lcom/intsig/pdfengine/PDF_Engine;->releaseCanvas()V

    .line 442
    .line 443
    .line 444
    if-nez v2, :cond_d

    .line 445
    .line 446
    if-eqz v14, :cond_c

    .line 447
    .line 448
    goto :goto_8

    .line 449
    :cond_c
    const/4 v10, 0x1

    .line 450
    goto :goto_a

    .line 451
    :cond_d
    :goto_8
    sget-object v1, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->TAG:Ljava/lang/String;

    .line 452
    .line 453
    new-instance v3, Ljava/lang/StringBuilder;

    .line 454
    .line 455
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 456
    .line 457
    .line 458
    const-string v4, "engine.save2PdfFile() failed , error code="

    .line 459
    .line 460
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 461
    .line 462
    .line 463
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 464
    .line 465
    .line 466
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 467
    .line 468
    .line 469
    move-result-object v2

    .line 470
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    .line 472
    .line 473
    iget-object v1, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 474
    .line 475
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/PdfData;->getSavePdfFile()Ljava/lang/String;

    .line 476
    .line 477
    .line 478
    move-result-object v1

    .line 479
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 480
    .line 481
    .line 482
    iget-object v1, v0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->onPdfCreateListener:Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;

    .line 483
    .line 484
    if-eqz v1, :cond_f

    .line 485
    .line 486
    if-eqz v14, :cond_e

    .line 487
    .line 488
    sget v2, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_ALL_PAGE_MISS:I

    .line 489
    .line 490
    invoke-interface {v1, v2, v9}, Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    .line 491
    .line 492
    .line 493
    goto :goto_9

    .line 494
    :cond_e
    sget v2, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_SAVE_PDF_FILE:I

    .line 495
    .line 496
    invoke-interface {v1, v2, v9}, Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    .line 497
    .line 498
    .line 499
    :cond_f
    :goto_9
    const/4 v10, 0x0

    .line 500
    :goto_a
    return v10

    .line 501
    :cond_10
    new-instance v1, Ljava/lang/NullPointerException;

    .line 502
    .line 503
    const-string v2, "pdfData == null"

    .line 504
    .line 505
    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 506
    .line 507
    .line 508
    throw v1
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public getPdfData()Lcom/intsig/camscanner/pdfengine/PdfData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setPdfData(Lcom/intsig/camscanner/pdfengine/PdfData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->pdfData:Lcom/intsig/camscanner/pdfengine/PdfData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setoPdfCreateListener(Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;)Lcom/intsig/camscanner/pdfengine/PdfCreateClient;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient;->onPdfCreateListener:Lcom/intsig/camscanner/pdfengine/OnPdfCreateListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public Lcom/intsig/camscanner/pdfengine/utils/MathUtils;
.super Ljava/lang/Object;
.source "MathUtils.java"


# static fields
.field private static final BIG_ENOUGH_CEIL:D = 16384.999999999996

.field private static final BIG_ENOUGH_FLOOR:D = 16384.0

.field private static final BIG_ENOUGH_INT:I = 0x4000


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static ceil(F)I
    .locals 4

    .line 1
    float-to-double v0, p0

    .line 2
    const-wide v2, 0x40d0003fffffffffL    # 16384.999999999996

    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    add-double/2addr v0, v2

    .line 8
    double-to-int p0, v0

    .line 9
    add-int/lit16 p0, p0, -0x4000

    .line 10
    .line 11
    return p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static floor(F)I
    .locals 4

    .line 1
    float-to-double v0, p0

    .line 2
    const-wide/high16 v2, 0x40d0000000000000L    # 16384.0

    .line 3
    .line 4
    add-double/2addr v0, v2

    .line 5
    double-to-int p0, v0

    .line 6
    add-int/lit16 p0, p0, -0x4000

    .line 7
    .line 8
    return p0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static limit(FFF)F
    .locals 1

    .line 1
    cmpg-float v0, p0, p1

    if-gtz v0, :cond_0

    return p1

    :cond_0
    cmpl-float p1, p0, p2

    if-ltz p1, :cond_1

    return p2

    :cond_1
    return p0
.end method

.method public static limit(III)I
    .locals 0

    .line 2
    if-gt p0, p1, :cond_0

    return p1

    :cond_0
    if-lt p0, p2, :cond_1

    return p2

    :cond_1
    return p0
.end method

.method public static max(FF)F
    .locals 1

    .line 1
    cmpl-float v0, p0, p1

    if-lez v0, :cond_0

    return p1

    :cond_0
    return p0
.end method

.method public static max(II)I
    .locals 0

    .line 2
    if-le p0, p1, :cond_0

    return p1

    :cond_0
    return p0
.end method

.method public static min(FF)F
    .locals 1

    .line 1
    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    return p1

    :cond_0
    return p0
.end method

.method public static min(II)I
    .locals 0

    .line 2
    if-ge p0, p1, :cond_0

    return p1

    :cond_0
    return p0
.end method

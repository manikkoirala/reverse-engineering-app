.class public Lcom/intsig/camscanner/pdfengine/entity/PdfFile;
.super Ljava/lang/Object;
.source "PdfFile.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PdfFile"

.field private static final lock:Ljava/lang/Object;


# instance fields
.field private autoSpacing:Z

.field private documentLength:F

.field private isVertical:Z

.field private maxHeightPageSize:Lcom/shockwave/pdfium/util/SizeF;

.field private maxWidthPageSize:Lcom/shockwave/pdfium/util/SizeF;

.field private openedPages:Landroid/util/SparseBooleanArray;

.field private originalMaxHeightPageSize:Lcom/shockwave/pdfium/util/Size;

.field private originalMaxWidthPageSize:Lcom/shockwave/pdfium/util/Size;

.field private originalPageSizes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/shockwave/pdfium/util/Size;",
            ">;"
        }
    .end annotation
.end field

.field private originalUserPages:[I

.field private final pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

.field private pageOffsets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private pageSizes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/shockwave/pdfium/util/SizeF;",
            ">;"
        }
    .end annotation
.end field

.field private pageSpacing:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private pagesCount:I

.field private pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

.field private pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

.field private spacingPx:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/Object;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->lock:Ljava/lang/Object;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/shockwave/pdfium/PdfiumCore;Lcom/shockwave/pdfium/PdfDocument;Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;Lcom/shockwave/pdfium/util/Size;[IZIZ)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pagesCount:I

    .line 6
    .line 7
    new-instance v1, Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalPageSizes:Ljava/util/List;

    .line 13
    .line 14
    new-instance v1, Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSizes:Ljava/util/List;

    .line 20
    .line 21
    new-instance v1, Landroid/util/SparseBooleanArray;

    .line 22
    .line 23
    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 24
    .line 25
    .line 26
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->openedPages:Landroid/util/SparseBooleanArray;

    .line 27
    .line 28
    new-instance v1, Lcom/shockwave/pdfium/util/Size;

    .line 29
    .line 30
    invoke-direct {v1, v0, v0}, Lcom/shockwave/pdfium/util/Size;-><init>(II)V

    .line 31
    .line 32
    .line 33
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalMaxWidthPageSize:Lcom/shockwave/pdfium/util/Size;

    .line 34
    .line 35
    new-instance v1, Lcom/shockwave/pdfium/util/Size;

    .line 36
    .line 37
    invoke-direct {v1, v0, v0}, Lcom/shockwave/pdfium/util/Size;-><init>(II)V

    .line 38
    .line 39
    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalMaxHeightPageSize:Lcom/shockwave/pdfium/util/Size;

    .line 41
    .line 42
    new-instance v1, Lcom/shockwave/pdfium/util/SizeF;

    .line 43
    .line 44
    const/4 v2, 0x0

    .line 45
    invoke-direct {v1, v2, v2}, Lcom/shockwave/pdfium/util/SizeF;-><init>(FF)V

    .line 46
    .line 47
    .line 48
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->maxHeightPageSize:Lcom/shockwave/pdfium/util/SizeF;

    .line 49
    .line 50
    new-instance v1, Lcom/shockwave/pdfium/util/SizeF;

    .line 51
    .line 52
    invoke-direct {v1, v2, v2}, Lcom/shockwave/pdfium/util/SizeF;-><init>(FF)V

    .line 53
    .line 54
    .line 55
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->maxWidthPageSize:Lcom/shockwave/pdfium/util/SizeF;

    .line 56
    .line 57
    const/4 v1, 0x1

    .line 58
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->isVertical:Z

    .line 59
    .line 60
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->spacingPx:I

    .line 61
    .line 62
    new-instance v0, Ljava/util/ArrayList;

    .line 63
    .line 64
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .line 66
    .line 67
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageOffsets:Ljava/util/List;

    .line 68
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    .line 70
    .line 71
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 72
    .line 73
    .line 74
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSpacing:Ljava/util/List;

    .line 75
    .line 76
    iput v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentLength:F

    .line 77
    .line 78
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 79
    .line 80
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 81
    .line 82
    iput-object p3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 83
    .line 84
    iput-object p5, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalUserPages:[I

    .line 85
    .line 86
    iput-boolean p6, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->isVertical:Z

    .line 87
    .line 88
    iput p7, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->spacingPx:I

    .line 89
    .line 90
    iput-boolean p8, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->autoSpacing:Z

    .line 91
    .line 92
    invoke-direct {p0, p4}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->setup(Lcom/shockwave/pdfium/util/Size;)V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
.end method

.method private closeNativePage(Ljava/lang/Long;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchMethodException;,
            Ljava/lang/reflect/InvocationTargetException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    new-array v2, v1, [Ljava/lang/Class;

    .line 9
    .line 10
    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    .line 11
    .line 12
    const/4 v4, 0x0

    .line 13
    aput-object v3, v2, v4

    .line 14
    .line 15
    const-string v3, "nativeClosePage"

    .line 16
    .line 17
    invoke-virtual {v0, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 22
    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 25
    .line 26
    new-array v1, v1, [Ljava/lang/Object;

    .line 27
    .line 28
    aput-object p1, v1, v4

    .line 29
    .line 30
    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private prepareAutoSpacing(Lcom/shockwave/pdfium/util/Size;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSpacing:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-ge v0, v1, :cond_2

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSizes:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/shockwave/pdfium/util/SizeF;

    .line 20
    .line 21
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->isVertical:Z

    .line 22
    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/shockwave/pdfium/util/Size;->〇080()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    int-to-float v2, v2

    .line 30
    invoke-virtual {v1}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    goto :goto_1

    .line 35
    :cond_0
    invoke-virtual {p1}, Lcom/shockwave/pdfium/util/Size;->〇o00〇〇Oo()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    int-to-float v2, v2

    .line 40
    invoke-virtual {v1}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    :goto_1
    sub-float/2addr v2, v1

    .line 45
    const/4 v1, 0x0

    .line 46
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    add-int/lit8 v2, v2, -0x1

    .line 55
    .line 56
    if-ge v0, v2, :cond_1

    .line 57
    .line 58
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->spacingPx:I

    .line 59
    .line 60
    int-to-float v2, v2

    .line 61
    add-float/2addr v1, v2

    .line 62
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSpacing:Ljava/util/List;

    .line 63
    .line 64
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    add-int/lit8 v0, v0, 0x1

    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_2
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private prepareDocLen()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 4
    .line 5
    .line 6
    move-result v2

    .line 7
    if-ge v1, v2, :cond_3

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSizes:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    check-cast v2, Lcom/shockwave/pdfium/util/SizeF;

    .line 16
    .line 17
    iget-boolean v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->isVertical:Z

    .line 18
    .line 19
    if-eqz v3, :cond_0

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    goto :goto_1

    .line 26
    :cond_0
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    :goto_1
    add-float/2addr v0, v2

    .line 31
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->autoSpacing:Z

    .line 32
    .line 33
    if-eqz v2, :cond_1

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSpacing:Ljava/util/List;

    .line 36
    .line 37
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    check-cast v2, Ljava/lang/Float;

    .line 42
    .line 43
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    :goto_2
    add-float/2addr v0, v2

    .line 48
    goto :goto_3

    .line 49
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    add-int/lit8 v2, v2, -0x1

    .line 54
    .line 55
    if-ge v1, v2, :cond_2

    .line 56
    .line 57
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->spacingPx:I

    .line 58
    .line 59
    int-to-float v2, v2

    .line 60
    goto :goto_2

    .line 61
    :cond_2
    :goto_3
    add-int/lit8 v1, v1, 0x1

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_3
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentLength:F

    .line 65
    .line 66
    return-void
    .line 67
    .line 68
.end method

.method private preparePagesOffset()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageOffsets:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-ge v1, v2, :cond_4

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSizes:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Lcom/shockwave/pdfium/util/SizeF;

    .line 21
    .line 22
    iget-boolean v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->isVertical:Z

    .line 23
    .line 24
    if-eqz v3, :cond_0

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    goto :goto_1

    .line 31
    :cond_0
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    :goto_1
    iget-boolean v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->autoSpacing:Z

    .line 36
    .line 37
    if-eqz v3, :cond_3

    .line 38
    .line 39
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSpacing:Ljava/util/List;

    .line 40
    .line 41
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    check-cast v3, Ljava/lang/Float;

    .line 46
    .line 47
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    const/high16 v4, 0x40000000    # 2.0f

    .line 52
    .line 53
    div-float/2addr v3, v4

    .line 54
    add-float/2addr v0, v3

    .line 55
    if-nez v1, :cond_1

    .line 56
    .line 57
    iget v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->spacingPx:I

    .line 58
    .line 59
    int-to-float v3, v3

    .line 60
    div-float/2addr v3, v4

    .line 61
    sub-float/2addr v0, v3

    .line 62
    goto :goto_2

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 64
    .line 65
    .line 66
    move-result v3

    .line 67
    add-int/lit8 v3, v3, -0x1

    .line 68
    .line 69
    if-ne v1, v3, :cond_2

    .line 70
    .line 71
    iget v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->spacingPx:I

    .line 72
    .line 73
    int-to-float v3, v3

    .line 74
    div-float/2addr v3, v4

    .line 75
    add-float/2addr v0, v3

    .line 76
    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageOffsets:Ljava/util/List;

    .line 77
    .line 78
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 79
    .line 80
    .line 81
    move-result-object v5

    .line 82
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSpacing:Ljava/util/List;

    .line 86
    .line 87
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    check-cast v3, Ljava/lang/Float;

    .line 92
    .line 93
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    div-float/2addr v3, v4

    .line 98
    goto :goto_3

    .line 99
    :cond_3
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageOffsets:Ljava/util/List;

    .line 100
    .line 101
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 102
    .line 103
    .line 104
    move-result-object v4

    .line 105
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    iget v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->spacingPx:I

    .line 109
    .line 110
    int-to-float v3, v3

    .line 111
    :goto_3
    add-float/2addr v2, v3

    .line 112
    add-float/2addr v0, v2

    .line 113
    add-int/lit8 v1, v1, 0x1

    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_4
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private setup(Lcom/shockwave/pdfium/util/Size;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalUserPages:[I

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    array-length v0, v0

    .line 6
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pagesCount:I

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/shockwave/pdfium/PdfiumCore;->O8(Lcom/shockwave/pdfium/PdfDocument;)I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pagesCount:I

    .line 18
    .line 19
    :goto_0
    const/4 v0, 0x0

    .line 20
    :goto_1
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pagesCount:I

    .line 21
    .line 22
    if-ge v0, v1, :cond_3

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 27
    .line 28
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    invoke-virtual {v1, v2, v3}, Lcom/shockwave/pdfium/PdfiumCore;->o〇0(Lcom/shockwave/pdfium/PdfDocument;I)Lcom/shockwave/pdfium/util/Size;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/shockwave/pdfium/util/Size;->〇o00〇〇Oo()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalMaxWidthPageSize:Lcom/shockwave/pdfium/util/Size;

    .line 41
    .line 42
    invoke-virtual {v3}, Lcom/shockwave/pdfium/util/Size;->〇o00〇〇Oo()I

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    if-le v2, v3, :cond_1

    .line 47
    .line 48
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalMaxWidthPageSize:Lcom/shockwave/pdfium/util/Size;

    .line 49
    .line 50
    :cond_1
    invoke-virtual {v1}, Lcom/shockwave/pdfium/util/Size;->〇080()I

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalMaxHeightPageSize:Lcom/shockwave/pdfium/util/Size;

    .line 55
    .line 56
    invoke-virtual {v3}, Lcom/shockwave/pdfium/util/Size;->〇080()I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    if-le v2, v3, :cond_2

    .line 61
    .line 62
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalMaxHeightPageSize:Lcom/shockwave/pdfium/util/Size;

    .line 63
    .line 64
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalPageSizes:Ljava/util/List;

    .line 65
    .line 66
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    .line 68
    .line 69
    add-int/lit8 v0, v0, 0x1

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->recalculatePageSizes(Lcom/shockwave/pdfium/util/Size;)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public closePage(I)V
    .locals 6

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-gez p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->lock:Ljava/lang/Object;

    .line 9
    .line 10
    monitor-enter v0

    .line 11
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 12
    .line 13
    if-nez v1, :cond_1

    .line 14
    .line 15
    const-string p1, "PdfFile"

    .line 16
    .line 17
    const-string v1, "closePage pdfDocument == null"

    .line 18
    .line 19
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 23
    return-void

    .line 24
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    array-length v2, v1

    .line 33
    const/4 v3, 0x0

    .line 34
    :goto_0
    if-ge v3, v2, :cond_4

    .line 35
    .line 36
    aget-object v4, v1, v3

    .line 37
    .line 38
    const/4 v5, 0x1

    .line 39
    invoke-virtual {v4, v5}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 40
    .line 41
    .line 42
    iget-object v5, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 43
    .line 44
    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    instance-of v5, v4, Ljava/util/Map;

    .line 49
    .line 50
    if-eqz v5, :cond_3

    .line 51
    .line 52
    check-cast v4, Landroidx/collection/ArrayMap;

    .line 53
    .line 54
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    check-cast v1, Ljava/lang/Long;

    .line 63
    .line 64
    if-eqz v1, :cond_2

    .line 65
    .line 66
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->closeNativePage(Ljava/lang/Long;)V

    .line 67
    .line 68
    .line 69
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-interface {v4, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->openedPages:Landroid/util/SparseBooleanArray;

    .line 77
    .line 78
    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_2
    const-string p1, "PdfFile"

    .line 83
    .line 84
    const-string v1, "pagePtr = null"

    .line 85
    .line 86
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_3
    add-int/lit8 v3, v3, 0x1

    .line 91
    .line 92
    goto :goto_0

    .line 93
    :catchall_0
    move-exception p1

    .line 94
    :try_start_2
    const-string v1, "PdfFile"

    .line 95
    .line 96
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 97
    .line 98
    .line 99
    :cond_4
    :goto_1
    monitor-exit v0

    .line 100
    return-void

    .line 101
    :catchall_1
    move-exception p1

    .line 102
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 103
    throw p1
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public determineValidPageNumberFrom(I)I
    .locals 2

    .line 1
    if-gtz p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return p1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalUserPages:[I

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    array-length v1, v0

    .line 10
    if-lt p1, v1, :cond_2

    .line 11
    .line 12
    array-length p1, v0

    .line 13
    :goto_0
    add-int/lit8 p1, p1, -0x1

    .line 14
    .line 15
    return p1

    .line 16
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-lt p1, v0, :cond_2

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    goto :goto_0

    .line 27
    :cond_2
    return p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public dispose()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 7
    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    sget-object v0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->lock:Ljava/lang/Object;

    .line 11
    .line 12
    monitor-enter v0

    .line 13
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 14
    .line 15
    if-eqz v2, :cond_0

    .line 16
    .line 17
    :try_start_1
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 18
    .line 19
    invoke-virtual {v3, v2}, Lcom/shockwave/pdfium/PdfiumCore;->〇080(Lcom/shockwave/pdfium/PdfDocument;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :catchall_0
    move-exception v2

    .line 24
    :try_start_2
    const-string v3, "PdfFile"

    .line 25
    .line 26
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 30
    .line 31
    :cond_0
    monitor-exit v0

    .line 32
    goto :goto_1

    .line 33
    :catchall_1
    move-exception v1

    .line 34
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 35
    throw v1

    .line 36
    :cond_1
    :goto_1
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 37
    .line 38
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalUserPages:[I

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public documentPage(I)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalUserPages:[I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-eqz v0, :cond_2

    .line 5
    .line 6
    if-ltz p1, :cond_1

    .line 7
    .line 8
    array-length v2, v0

    .line 9
    if-lt p1, v2, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    aget v0, v0, p1

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_1
    :goto_0
    return v1

    .line 16
    :cond_2
    move v0, p1

    .line 17
    :goto_1
    if-ltz v0, :cond_4

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-lt p1, v2, :cond_3

    .line 24
    .line 25
    goto :goto_2

    .line 26
    :cond_3
    return v0

    .line 27
    :cond_4
    :goto_2
    return v1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public getBookmarks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/shockwave/pdfium/PdfDocument$Bookmark;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    return-object v0

    .line 11
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Lcom/shockwave/pdfium/PdfiumCore;->〇〇888(Lcom/shockwave/pdfium/PdfDocument;)Ljava/util/List;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocLen(F)F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentLength:F

    .line 2
    .line 3
    mul-float v0, v0, p1

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getMaxPageHeight()F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageSize()Lcom/shockwave/pdfium/util/SizeF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMaxPageSize()Lcom/shockwave/pdfium/util/SizeF;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->isVertical:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->maxWidthPageSize:Lcom/shockwave/pdfium/util/SizeF;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->maxHeightPageSize:Lcom/shockwave/pdfium/util/SizeF;

    .line 9
    .line 10
    :goto_0
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMaxPageWidth()F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageSize()Lcom/shockwave/pdfium/util/SizeF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMetaData()Lcom/shockwave/pdfium/PdfDocument$Meta;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Lcom/shockwave/pdfium/PdfiumCore;->〇o00〇〇Oo(Lcom/shockwave/pdfium/PdfDocument;)Lcom/shockwave/pdfium/PdfDocument$Meta;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageAtOffset(FF)I
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x0

    .line 4
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 5
    .line 6
    .line 7
    move-result v3

    .line 8
    if-ge v1, v3, :cond_1

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageOffsets:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    check-cast v3, Ljava/lang/Float;

    .line 17
    .line 18
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    mul-float v3, v3, p2

    .line 23
    .line 24
    invoke-virtual {p0, v1, p2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSpacing(IF)F

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    const/high16 v5, 0x40000000    # 2.0f

    .line 29
    .line 30
    div-float/2addr v4, v5

    .line 31
    sub-float/2addr v3, v4

    .line 32
    cmpl-float v3, v3, p1

    .line 33
    .line 34
    if-ltz v3, :cond_0

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 38
    .line 39
    add-int/lit8 v1, v1, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, -0x1

    .line 43
    .line 44
    if-ltz v2, :cond_2

    .line 45
    .line 46
    move v0, v2

    .line 47
    :cond_2
    return v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public getPageLength(IF)F
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->isVertical:Z

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {p1}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    :goto_0
    mul-float p1, p1, p2

    .line 19
    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getPageLinks(I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/shockwave/pdfium/PdfDocument$Link;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 8
    .line 9
    invoke-virtual {v0, v1, p1}, Lcom/shockwave/pdfium/PdfiumCore;->Oo08(Lcom/shockwave/pdfium/PdfDocument;I)Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getPageOffset(IF)F
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-gez v0, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return p1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageOffsets:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p1, Ljava/lang/Float;

    .line 16
    .line 17
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    mul-float p1, p1, p2

    .line 22
    .line 23
    return p1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-gez v0, :cond_0

    .line 6
    .line 7
    new-instance p1, Lcom/shockwave/pdfium/util/SizeF;

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-direct {p1, v0, v0}, Lcom/shockwave/pdfium/util/SizeF;-><init>(FF)V

    .line 11
    .line 12
    .line 13
    return-object p1

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSizes:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    check-cast p1, Lcom/shockwave/pdfium/util/SizeF;

    .line 21
    .line 22
    return-object p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public getPageSpacing(IF)F
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->autoSpacing:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSpacing:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Ljava/lang/Float;

    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->spacingPx:I

    .line 19
    .line 20
    int-to-float p1, p1

    .line 21
    :goto_0
    mul-float p1, p1, p2

    .line 22
    .line 23
    return p1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getPagesCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pagesCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getScaledPageSize(IF)Lcom/shockwave/pdfium/util/SizeF;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/shockwave/pdfium/util/SizeF;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    mul-float v1, v1, p2

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    mul-float p1, p1, p2

    .line 18
    .line 19
    invoke-direct {v0, v1, p1}, Lcom/shockwave/pdfium/util/SizeF;-><init>(FF)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getSecondaryPageOffset(IF)F
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->isVertical:Z

    .line 6
    .line 7
    const/high16 v1, 0x40000000    # 2.0f

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageWidth()F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-virtual {p1}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    :goto_0
    sub-float/2addr v0, p1

    .line 20
    mul-float p2, p2, v0

    .line 21
    .line 22
    div-float/2addr p2, v1

    .line 23
    return p2

    .line 24
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageHeight()F

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    invoke-virtual {p1}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    goto :goto_0
    .line 33
.end method

.method public mapRectToDevice(IIIIILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 9

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    .line 2
    .line 3
    .line 4
    move-result v2

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 8
    .line 9
    const/4 v7, 0x0

    .line 10
    move v3, p2

    .line 11
    move v4, p3

    .line 12
    move v5, p4

    .line 13
    move v6, p5

    .line 14
    move-object v8, p6

    .line 15
    invoke-virtual/range {v0 .. v8}, Lcom/shockwave/pdfium/PdfiumCore;->〇80〇808〇O(Lcom/shockwave/pdfium/PdfDocument;IIIIIILandroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    return-object p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public openPage(IZ)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-gez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->lock:Ljava/lang/Object;

    .line 10
    .line 11
    monitor-enter v2

    .line 12
    :try_start_0
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 13
    .line 14
    if-nez v3, :cond_1

    .line 15
    .line 16
    monitor-exit v2

    .line 17
    return v1

    .line 18
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->openedPages:Landroid/util/SparseBooleanArray;

    .line 19
    .line 20
    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->indexOfKey(I)I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    if-ltz v3, :cond_3

    .line 25
    .line 26
    if-eqz p2, :cond_2

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    return v1

    .line 31
    :cond_3
    :goto_0
    :try_start_1
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 32
    .line 33
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    .line 34
    .line 35
    invoke-virtual {p2, v3, v0}, Lcom/shockwave/pdfium/PdfiumCore;->〇O8o08O(Lcom/shockwave/pdfium/PdfDocument;I)J

    .line 36
    .line 37
    .line 38
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->openedPages:Landroid/util/SparseBooleanArray;

    .line 39
    .line 40
    const/4 v3, 0x1

    .line 41
    invoke-virtual {p2, v0, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    .line 43
    .line 44
    :try_start_2
    monitor-exit v2

    .line 45
    return v3

    .line 46
    :catch_0
    move-exception p2

    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->openedPages:Landroid/util/SparseBooleanArray;

    .line 48
    .line 49
    invoke-virtual {v3, v0, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 50
    .line 51
    .line 52
    new-instance v0, Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException;

    .line 53
    .line 54
    invoke-direct {v0, p1, p2}, Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException;-><init>(ILjava/lang/Throwable;)V

    .line 55
    .line 56
    .line 57
    throw v0

    .line 58
    :catchall_0
    move-exception p1

    .line 59
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 60
    throw p1
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public pageHasError(I)Z
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->openedPages:Landroid/util/SparseBooleanArray;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    xor-int/lit8 p1, p1, 0x1

    .line 13
    .line 14
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public recalculatePageSizes(Lcom/shockwave/pdfium/util/Size;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSizes:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/pdfengine/entity/PageSizeCalculator;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalMaxWidthPageSize:Lcom/shockwave/pdfium/util/Size;

    .line 11
    .line 12
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalMaxHeightPageSize:Lcom/shockwave/pdfium/util/Size;

    .line 13
    .line 14
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/intsig/camscanner/pdfengine/entity/PageSizeCalculator;-><init>(Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;Lcom/shockwave/pdfium/util/Size;Lcom/shockwave/pdfium/util/Size;Lcom/shockwave/pdfium/util/Size;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PageSizeCalculator;->getOptimalMaxWidthPageSize()Lcom/shockwave/pdfium/util/SizeF;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->maxWidthPageSize:Lcom/shockwave/pdfium/util/SizeF;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PageSizeCalculator;->getOptimalMaxHeightPageSize()Lcom/shockwave/pdfium/util/SizeF;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->maxHeightPageSize:Lcom/shockwave/pdfium/util/SizeF;

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->originalPageSizes:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    if-eqz v2, :cond_0

    .line 40
    .line 41
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    check-cast v2, Lcom/shockwave/pdfium/util/Size;

    .line 46
    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pageSizes:Ljava/util/List;

    .line 48
    .line 49
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pdfengine/entity/PageSizeCalculator;->calculate(Lcom/shockwave/pdfium/util/Size;)Lcom/shockwave/pdfium/util/SizeF;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->autoSpacing:Z

    .line 58
    .line 59
    if-eqz v0, :cond_1

    .line 60
    .line 61
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->prepareAutoSpacing(Lcom/shockwave/pdfium/util/Size;)V

    .line 62
    .line 63
    .line 64
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->prepareDocLen()V

    .line 65
    .line 66
    .line 67
    invoke-direct {p0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->preparePagesOffset()V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public renderPageBitmap(Landroid/graphics/Bitmap;IIIIIZ)V
    .locals 10

    move-object v0, p0

    move v1, p2

    .line 5
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    move-result v4

    .line 6
    iget-object v1, v0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    iget-object v2, v0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    move-object v3, p1

    move v5, p3

    move v6, p4

    move v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-virtual/range {v1 .. v9}, Lcom/shockwave/pdfium/PdfiumCore;->Oooo8o0〇(Lcom/shockwave/pdfium/PdfDocument;Landroid/graphics/Bitmap;IIIIIZ)V

    return-void
.end method

.method public renderPageBitmap(Landroid/graphics/Bitmap;ILandroid/graphics/Rect;Z)V
    .locals 9

    .line 1
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->documentPage(I)I

    move-result v3

    .line 2
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->pdfDocument:Lcom/shockwave/pdfium/PdfDocument;

    iget v4, p3, Landroid/graphics/Rect;->left:I

    iget v5, p3, Landroid/graphics/Rect;->top:I

    .line 3
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v7

    move-object v2, p1

    move v8, p4

    .line 4
    invoke-virtual/range {v0 .. v8}, Lcom/shockwave/pdfium/PdfiumCore;->Oooo8o0〇(Lcom/shockwave/pdfium/PdfDocument;Landroid/graphics/Bitmap;IIIIIZ)V

    return-void
.end method

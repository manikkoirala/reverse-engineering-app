.class Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;
.super Ljava/lang/Object;
.source "PdfCreateClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdfengine/PdfCreateClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PdfPageProperty"
.end annotation


# instance fields
.field oriImageHeight:F

.field oriImageWidth:F

.field pageHeight:F

.field pageWidth:F

.field rotation:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/pdfengine/o〇0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;-><init>()V

    return-void
.end method

.method private reset()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageWidth:F

    .line 3
    .line 4
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->pageHeight:F

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageWidth:F

    .line 7
    .line 8
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->oriImageHeight:F

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->rotation:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdfengine/PdfCreateClient$PdfPageProperty;->reset()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

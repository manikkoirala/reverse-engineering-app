.class public Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;
.super Ljava/lang/Object;
.source "PdfImportParentEntity.java"


# instance fields
.field private mIsImportOriginPdf:Z

.field private mIsOfflineFolder:Z

.field private mIsSupportImportOffice:Z

.field private mParentSyncId:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private tagId:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsSupportImportOffice:Z

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsImportOriginPdf:Z

    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mParentSyncId:Ljava/lang/String;

    .line 5
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsOfflineFolder:Z

    .line 6
    invoke-static {}, Lcom/intsig/base/ToolbarThemeGet;->Oo08()Z

    move-result p1

    if-eqz p1, :cond_0

    const-wide/16 p1, -0x2

    .line 7
    iput-wide p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->tagId:J

    goto :goto_0

    .line 8
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08O0〇O()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->tagId:J

    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZJ)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsSupportImportOffice:Z

    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsImportOriginPdf:Z

    .line 12
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mParentSyncId:Ljava/lang/String;

    .line 13
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsOfflineFolder:Z

    .line 14
    iput-wide p3, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->tagId:J

    return-void
.end method


# virtual methods
.method public getParentSyncId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mParentSyncId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTagId()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->tagId:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isImportOriginPdf()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsImportOriginPdf:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isIsOfflineFolder()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsOfflineFolder:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isSupportImportOffice()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsSupportImportOffice:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setImportOriginPdf(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsImportOriginPdf:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setIsOfflineFolder(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsOfflineFolder:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setParentSyncId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mParentSyncId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setSupportImportOffice(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->mIsSupportImportOffice:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setTagId(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->tagId:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

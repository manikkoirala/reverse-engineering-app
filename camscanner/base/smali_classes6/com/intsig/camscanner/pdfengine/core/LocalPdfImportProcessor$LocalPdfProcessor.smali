.class Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;
.super Landroid/os/AsyncTask;
.source "LocalPdfImportProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalPdfProcessor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;",
        "Ljava/lang/Integer;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field private mPdfFree:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

.field private pdfProcessListener:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$PdfProcessListener;

.field final synthetic this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 2
    .line 3
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private createPdfFile(Lcom/intsig/camscanner/pdfengine/source/FileSource;Lcom/shockwave/pdfium/util/Size;)Lcom/intsig/camscanner/pdfengine/entity/PdfFile;
    .locals 9

    .line 1
    new-instance v1, Lcom/shockwave/pdfium/PdfiumCore;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 4
    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-direct {v1, v0}, Lcom/shockwave/pdfium/PdfiumCore;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->mPdfFree:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getPwd()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/camscanner/pdfengine/source/FileSource;->createDocument(Landroid/content/Context;Lcom/shockwave/pdfium/PdfiumCore;Ljava/lang/String;)Lcom/shockwave/pdfium/PdfDocument;

    .line 25
    .line 26
    .line 27
    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    new-instance p1, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 29
    .line 30
    sget-object v3, Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;->BOTH:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 31
    .line 32
    const/4 v5, 0x0

    .line 33
    const/4 v6, 0x1

    .line 34
    const/4 v7, 0x0

    .line 35
    const/4 v8, 0x1

    .line 36
    move-object v0, p1

    .line 37
    move-object v4, p2

    .line 38
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;-><init>(Lcom/shockwave/pdfium/PdfiumCore;Lcom/shockwave/pdfium/PdfDocument;Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;Lcom/shockwave/pdfium/util/Size;[IZIZ)V

    .line 39
    .line 40
    .line 41
    return-object p1

    .line 42
    :catch_0
    move-exception p1

    .line 43
    const-string p2, "LocalPdfImportProcessor"

    .line 44
    .line 45
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    .line 47
    .line 48
    const/4 p1, 0x0

    .line 49
    return-object p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private recycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p1

    .line 15
    const-string v0, "LocalPdfImportProcessor"

    .line 16
    .line 17
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18
    .line 19
    .line 20
    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;)Landroid/net/Uri;
    .locals 26

    move-object/from16 v1, p0

    const-string v2, ".jpg"

    const/4 v3, 0x0

    .line 2
    aget-object v0, p1, v3

    iput-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->mPdfFree:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    const-string v4, "LocalPdfImportProcessor"

    const/4 v5, 0x0

    if-nez v0, :cond_0

    const-string v0, "LocalPdfProcessor:mPdfFree is not complete"

    .line 3
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    .line 4
    :cond_0
    new-instance v6, Ljava/io/File;

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 7
    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->mPdfFree:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "LocalPdfProcessor:mPdfFree.path is empty"

    .line 8
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    .line 9
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v9, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->mPdfFree:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    invoke-virtual {v9}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 10
    new-instance v9, Lcom/intsig/camscanner/pdfengine/source/FileSource;

    invoke-direct {v9, v0}, Lcom/intsig/camscanner/pdfengine/source/FileSource;-><init>(Ljava/io/File;)V

    .line 11
    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇0〇O0088o(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/shockwave/pdfium/util/Size;

    move-result-object v10

    .line 12
    invoke-direct {v1, v9, v10}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->createPdfFile(Lcom/intsig/camscanner/pdfengine/source/FileSource;Lcom/shockwave/pdfium/util/Size;)Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    move-result-object v0

    if-nez v0, :cond_2

    return-object v5

    .line 13
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    move-result v11

    .line 14
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 15
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "start import, estimated number = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v4, v12}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_0
    if-ge v12, v11, :cond_8

    const/4 v14, 0x1

    add-int/2addr v13, v14

    if-nez v0, :cond_3

    .line 16
    invoke-direct {v1, v9, v10}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->createPdfFile(Lcom/intsig/camscanner/pdfengine/source/FileSource;Lcom/shockwave/pdfium/util/Size;)Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "pdfFile == null"

    .line 17
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    :cond_3
    move-object v5, v0

    .line 18
    :try_start_0
    invoke-virtual {v5, v12, v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->openPage(IZ)Z

    .line 19
    invoke-virtual {v5, v12}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    move-result v14

    float-to-int v14, v14

    .line 21
    invoke-virtual {v0}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    move-result v0

    float-to-int v0, v0

    .line 22
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v14, v0, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3
    :try_end_0
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_0 .. :try_end_0} :catch_17
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_16
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_15
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object/from16 v24, v9

    .line 23
    :try_start_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_1 .. :try_end_1} :catch_14
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_13
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_12
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v25, v10

    :try_start_2
    const-string v10, "create Bitmap, width = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v10, " height = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v23, 0x1

    move-object/from16 v16, v5

    move-object/from16 v17, v3

    move/from16 v18, v12

    move/from16 v21, v14

    move/from16 v22, v0

    .line 24
    invoke-virtual/range {v16 .. v23}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->renderPageBitmap(Landroid/graphics/Bitmap;IIIIIZ)V

    .line 25
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v0

    .line 26
    new-instance v9, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v6, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 27
    new-instance v10, Ljava/io/BufferedOutputStream;

    new-instance v14, Ljava/io/FileOutputStream;

    invoke-direct {v14, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v10, v14}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_10
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v3, :cond_4

    .line 28
    :try_start_3
    sget-object v14, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;
    :try_end_3
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object/from16 v16, v6

    const/16 v6, 0x55

    :try_start_4
    invoke-virtual {v3, v14, v6, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 29
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->flush()V

    .line 30
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V

    const/4 v10, 0x0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    :goto_1
    move-object/from16 v16, v6

    goto/16 :goto_4

    :cond_4
    move-object/from16 v16, v6

    .line 31
    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O000()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 32
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14, v6}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14
    :try_end_4
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_4 .. :try_end_4} :catch_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_d
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_c
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move/from16 v17, v11

    .line 33
    :try_start_5
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_5
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-wide/from16 v18, v7

    :try_start_6
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O0o〇〇Oo()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 34
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    const-string v9, "save image FileUtil.copyPath = %s,copyPicDir = %b,thumbPath = %s,copyThumbPath = %b"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    .line 35
    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    const/16 v21, 0x0

    aput-object v20, v11, v21

    const/16 v20, 0x1

    aput-object v6, v11, v20

    const/4 v6, 0x2

    aput-object v7, v11, v6

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x3

    aput-object v6, v11, v7

    invoke-static {v9, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v14, :cond_5

    if-eqz v8, :cond_5

    .line 36
    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    :cond_5
    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇O8o08O(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)I

    move-result v0

    if-lez v0, :cond_6

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/Integer;

    .line 38
    iget-object v7, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v7}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)I

    move-result v8

    add-int/2addr v8, v0

    invoke-static {v7, v8}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->OO0o〇〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;I)V

    mul-int/lit8 v8, v8, 0x64

    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇O8o08O(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)I

    move-result v0

    div-int/2addr v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_6
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    const/4 v7, 0x0

    :try_start_7
    aput-object v0, v6, v7
    :try_end_7
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    invoke-virtual {v1, v6}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    const/4 v7, 0x0

    goto/16 :goto_d

    :cond_6
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/Integer;

    .line 39
    iget-object v7, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v7}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)I

    move-result v8

    add-int/2addr v8, v0

    invoke-static {v7, v8}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->OO0o〇〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;I)V

    mul-int/lit8 v8, v8, 0x64

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_8
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_8 .. :try_end_8} :catch_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    const/4 v7, 0x0

    :try_start_9
    aput-object v0, v6, v7

    invoke-virtual {v1, v6}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_9
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto/16 :goto_d

    :catch_3
    move-exception v0

    goto/16 :goto_c

    :catch_4
    move-exception v0

    goto/16 :goto_c

    :catch_5
    move-exception v0

    goto/16 :goto_c

    :catch_6
    move-exception v0

    goto :goto_5

    :catch_7
    move-exception v0

    goto :goto_5

    :catch_8
    move-exception v0

    goto :goto_5

    :catch_9
    move-exception v0

    goto :goto_3

    :catch_a
    move-exception v0

    goto :goto_3

    :catch_b
    move-exception v0

    :goto_3
    move-wide/from16 v18, v7

    goto :goto_5

    :catch_c
    move-exception v0

    goto :goto_4

    :catch_d
    move-exception v0

    goto :goto_4

    :catch_e
    move-exception v0

    :goto_4
    move-wide/from16 v18, v7

    move/from16 v17, v11

    :goto_5
    const/4 v7, 0x0

    goto :goto_c

    :catch_f
    move-exception v0

    goto :goto_6

    :catch_10
    move-exception v0

    goto :goto_6

    :catch_11
    move-exception v0

    :goto_6
    move-object/from16 v16, v6

    move-wide/from16 v18, v7

    goto :goto_8

    :catchall_0
    move-exception v0

    move-object v5, v3

    goto :goto_9

    :catch_12
    move-exception v0

    goto :goto_7

    :catch_13
    move-exception v0

    goto :goto_7

    :catch_14
    move-exception v0

    :goto_7
    move-object/from16 v16, v6

    move-wide/from16 v18, v7

    move-object/from16 v25, v10

    :goto_8
    move/from16 v17, v11

    const/4 v7, 0x0

    goto :goto_b

    :catchall_1
    move-exception v0

    const/4 v5, 0x0

    :goto_9
    const/4 v10, 0x0

    goto :goto_f

    :catch_15
    move-exception v0

    goto :goto_a

    :catch_16
    move-exception v0

    goto :goto_a

    :catch_17
    move-exception v0

    :goto_a
    move-object/from16 v16, v6

    move-wide/from16 v18, v7

    move-object/from16 v24, v9

    move-object/from16 v25, v10

    move/from16 v17, v11

    const/4 v7, 0x0

    const/4 v3, 0x0

    :goto_b
    const/4 v10, 0x0

    .line 40
    :goto_c
    :try_start_a
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 41
    :goto_d
    invoke-direct {v1, v3}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 42
    invoke-static {v10}, Lcom/intsig/utils/FileUtil;->〇o〇(Ljava/io/Closeable;)V

    .line 43
    invoke-virtual {v5, v12}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->closePage(I)V

    const/16 v0, 0x1e

    if-lt v13, v0, :cond_7

    .line 44
    invoke-virtual {v5}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->dispose()V

    const/4 v0, 0x0

    const/4 v13, 0x0

    goto :goto_e

    :cond_7
    move-object v0, v5

    :goto_e
    add-int/lit8 v12, v12, 0x1

    move-object/from16 v6, v16

    move/from16 v11, v17

    move-wide/from16 v7, v18

    move-object/from16 v9, v24

    move-object/from16 v10, v25

    const/4 v3, 0x0

    const/4 v5, 0x0

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    move-object v5, v3

    .line 45
    :goto_f
    invoke-direct {v1, v5}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->recycleBitmap(Landroid/graphics/Bitmap;)V

    .line 46
    invoke-static {v10}, Lcom/intsig/utils/FileUtil;->〇o〇(Ljava/io/Closeable;)V

    .line 47
    throw v0

    :cond_8
    move-wide/from16 v18, v7

    if-eqz v0, :cond_9

    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->dispose()V

    .line 49
    :cond_9
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_a

    const/4 v2, 0x0

    return-object v2

    .line 50
    :cond_a
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08O0〇O()J

    move-result-wide v2

    .line 51
    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->oO80(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 52
    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->oO80(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->getTagId()J

    move-result-wide v2

    :cond_b
    move-wide/from16 v16, v2

    .line 53
    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇〇888(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 54
    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇〇888(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->〇〇888(Ljava/lang/String;)V

    .line 55
    :cond_c
    iget-object v12, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v12}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    move-result-object v13

    iget-object v0, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    iget-object v2, v1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->mPdfFree:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇〇8O0〇8(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {v12 .. v17}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->createDoc(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;J)Landroid/net/Uri;

    move-result-object v0

    .line 56
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "finish import, consume = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v5, v5, v18

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->doInBackground([Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;)Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/net/Uri;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "result docUri= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LocalPdfImportProcessor"

    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "import_pdf"

    .line 3
    invoke-static {v0}, Lcom/intsig/appsflyer/AppsFlyerHelper;->Oo08(Ljava/lang/String;)V

    const-string v0, "pdfimport_ok"

    const/4 v1, 0x0

    const-string v2, "CSPdfimportPop"

    .line 4
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->oo88o8O(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    .line 5
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->pdfProcessListener:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$PdfProcessListener;

    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->mPdfFree:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    invoke-interface {v1, p1, v0, v2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$PdfProcessListener;->finishOne(Landroid/net/Uri;ILcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->onPostExecute(Landroid/net/Uri;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 17
    .line 18
    invoke-static {v2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0, v1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇〇808〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 29
    .line 30
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 35
    .line 36
    invoke-static {v1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    const v2, 0x7f131ec6

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 51
    .line 52
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    const/4 v1, 0x0

    .line 57
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->setProgress(I)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 61
    .line 62
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 70
    .line 71
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->show()V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 80
    .line 81
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-nez v0, :cond_1

    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    .line 92
    .line 93
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->show()V

    .line 98
    .line 99
    .line 100
    :cond_1
    :goto_0
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->setProgress(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public setPdfProcessListener(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$PdfProcessListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$LocalPdfProcessor;->pdfProcessListener:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$PdfProcessListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

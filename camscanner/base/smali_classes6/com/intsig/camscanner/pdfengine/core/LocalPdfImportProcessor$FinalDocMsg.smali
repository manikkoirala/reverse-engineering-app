.class public Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;
.super Ljava/lang/Object;
.source "LocalPdfImportProcessor.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FinalDocMsg"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private docId:J

.field private mDocSyncId:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final mIsCsDoc:Z

.field private final mOriginalFileSize:J

.field private final mOriginalPath:Ljava/lang/String;

.field private final mPageCount:I

.field private final mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/net/Uri;JLjava/lang/String;IZ)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 2
    iput-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->docId:J

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mUri:Landroid/net/Uri;

    .line 4
    iput-wide p2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mOriginalFileSize:J

    .line 5
    iput-object p4, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mOriginalPath:Ljava/lang/String;

    .line 6
    iput p5, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mPageCount:I

    .line 7
    iput-boolean p6, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mIsCsDoc:Z

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 9
    iput-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->docId:J

    .line 10
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mUri:Landroid/net/Uri;

    .line 11
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mOriginalFileSize:J

    .line 12
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mOriginalPath:Ljava/lang/String;

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mPageCount:I

    .line 14
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mIsCsDoc:Z

    .line 15
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->docId:J

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mDocSyncId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocId()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->docId:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocSyncId()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mDocSyncId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOriginalFileSize()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mOriginalFileSize:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOriginalPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mOriginalPath:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mPageCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mUri:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isCsDoc()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mIsCsDoc:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDocId(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->docId:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDocSyncId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mDocSyncId:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mUri:Landroid/net/Uri;

    .line 2
    .line 3
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 4
    .line 5
    .line 6
    iget-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mOriginalFileSize:J

    .line 7
    .line 8
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 9
    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mOriginalPath:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget p2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mPageCount:I

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    .line 20
    .line 21
    iget-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mIsCsDoc:Z

    .line 22
    .line 23
    int-to-byte p2, p2

    .line 24
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    .line 25
    .line 26
    .line 27
    iget-wide v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->docId:J

    .line 28
    .line 29
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 30
    .line 31
    .line 32
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->mDocSyncId:Ljava/lang/String;

    .line 33
    .line 34
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

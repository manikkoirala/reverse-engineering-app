.class Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;
.super Landroid/os/AsyncTask;
.source "LocalPdfImportProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckSizeAsy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List<",
        "Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mPdfPathList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;",
            ">;"
        }
    .end annotation
.end field

.field private mPdfUriList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdfengine/entity/PdfUriImportEntity;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;Lcom/intsig/camscanner/pdfengine/core/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;-><init>(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->mPdfUriList:Ljava/util/List;

    if-eqz p1, :cond_0

    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0, p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇O888o0o(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->mPdfPathList:Ljava/util/List;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_1

    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->mPdfPathList:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->o800o8O(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;",
            ">;)V"
        }
    .end annotation

    const v0, 0x7f1302bd

    const v1, 0x7f130378

    if-eqz p1, :cond_3

    .line 2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 4
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    invoke-virtual {v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->isFailData()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 7
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v0, p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇O00(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;Ljava/util/List;)V

    goto :goto_1

    .line 9
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    move-result-object p1

    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/app/DialogUtils;->o〇〇0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 10
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    move-result-object p1

    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->this$0:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;

    invoke-static {v2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/app/DialogUtils;->o〇〇0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public setPdfPathList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->mPdfPathList:Ljava/util/List;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPdfUri(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Landroid/net/Uri;

    .line 21
    .line 22
    if-eqz v1, :cond_0

    .line 23
    .line 24
    new-instance v2, Lcom/intsig/camscanner/pdfengine/entity/PdfUriImportEntity;

    .line 25
    .line 26
    invoke-direct {v2, v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfUriImportEntity;-><init>(Landroid/net/Uri;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$CheckSizeAsy;->mPdfUriList:Ljava/util/List;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

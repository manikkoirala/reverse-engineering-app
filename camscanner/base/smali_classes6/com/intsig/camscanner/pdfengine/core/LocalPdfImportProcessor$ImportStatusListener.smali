.class public interface abstract Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;
.super Ljava/lang/Object;
.source "LocalPdfImportProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ImportStatusListener"
.end annotation


# virtual methods
.method public abstract onCancel()V
.end method

.method public abstract onExcludeEncrypted()V
.end method

.method public abstract onFinish(Ljava/util/List;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onFinishOnePdf(Lcom/intsig/camscanner/pdfengine/core/Pdf2GalleryEntity;Ljava/lang/String;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

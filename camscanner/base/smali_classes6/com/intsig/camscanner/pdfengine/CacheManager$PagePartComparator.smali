.class Lcom/intsig/camscanner/pdfengine/CacheManager$PagePartComparator;
.super Ljava/lang/Object;
.source "CacheManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdfengine/CacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PagePartComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/intsig/camscanner/pdfengine/model/PagePart;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/intsig/camscanner/pdfengine/CacheManager;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/pdfengine/CacheManager;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/CacheManager$PagePartComparator;->this$0:Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public compare(Lcom/intsig/camscanner/pdfengine/model/PagePart;Lcom/intsig/camscanner/pdfengine/model/PagePart;)I
    .locals 2

    .line 2
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getCacheOrder()I

    move-result v0

    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getCacheOrder()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 3
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getCacheOrder()I

    move-result p1

    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getCacheOrder()I

    move-result p2

    if-le p1, p2, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pdfengine/model/PagePart;

    check-cast p2, Lcom/intsig/camscanner/pdfengine/model/PagePart;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pdfengine/CacheManager$PagePartComparator;->compare(Lcom/intsig/camscanner/pdfengine/model/PagePart;Lcom/intsig/camscanner/pdfengine/model/PagePart;)I

    move-result p1

    return p1
.end method

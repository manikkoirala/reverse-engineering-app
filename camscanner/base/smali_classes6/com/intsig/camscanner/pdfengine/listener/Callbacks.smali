.class public Lcom/intsig/camscanner/pdfengine/listener/Callbacks;
.super Ljava/lang/Object;
.source "Callbacks.java"


# instance fields
.field private linkHandler:Lcom/intsig/camscanner/pdfengine/link/LinkHandler;

.field private onDrawAllListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

.field private onDrawListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

.field private onErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;

.field private onLoadCompleteListener:Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;

.field private onLongPressListener:Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;

.field private onPageChangeListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;

.field private onPageErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;

.field private onPageScrollListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;

.field private onRenderListener:Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;

.field private onTapListener:Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public callLinkHandler(Lcom/intsig/camscanner/pdfengine/model/LinkTapEvent;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->linkHandler:Lcom/intsig/camscanner/pdfengine/link/LinkHandler;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pdfengine/link/LinkHandler;->handleLinkEvent(Lcom/intsig/camscanner/pdfengine/model/LinkTapEvent;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public callOnLoadComplete(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onLoadCompleteListener:Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;->loadComplete(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public callOnLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onLongPressListener:Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public callOnPageChange(II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onPageChangeListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1, p2}, Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;->onPageChanged(II)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public callOnPageError(ILjava/lang/Throwable;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onPageErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1, p2}, Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;->onPageError(ILjava/lang/Throwable;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x1

    .line 9
    return p1

    .line 10
    :cond_0
    const/4 p1, 0x0

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public callOnPageScroll(IF)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onPageScrollListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1, p2}, Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;->onPageScrolled(IF)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public callOnRender(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onRenderListener:Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;->onInitiallyRendered(I)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public callOnTap(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onTapListener:Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;->onTap(Landroid/view/MotionEvent;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    :goto_0
    return p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getOnDraw()Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onDrawListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOnDrawAll()Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onDrawAllListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getOnError()Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setLinkHandler(Lcom/intsig/camscanner/pdfengine/link/LinkHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->linkHandler:Lcom/intsig/camscanner/pdfengine/link/LinkHandler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnDraw(Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onDrawListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnDrawAll(Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onDrawAllListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnError(Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnLoadComplete(Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onLoadCompleteListener:Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnLongPress(Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onLongPressListener:Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnPageChange(Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onPageChangeListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnPageError(Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onPageErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnPageScroll(Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onPageScrollListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnRender(Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onRenderListener:Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnTap(Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->onTapListener:Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public Lcom/intsig/camscanner/pdfengine/PDFView;
.super Landroid/widget/RelativeLayout;
.source "PDFView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;,
        Lcom/intsig/camscanner/pdfengine/PDFView$State;,
        Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAX_SCALE:F = 3.0f

.field public static final DEFAULT_MID_SCALE:F = 1.75f

.field public static final DEFAULT_MIN_SCALE:F = 1.0f

.field private static final TAG:Ljava/lang/String; = "PDFView"


# instance fields
.field private animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

.field private annotationRendering:Z

.field private antialiasFilter:Landroid/graphics/PaintFlagsDrawFilter;

.field private autoSpacing:Z

.field private bestQuality:Z

.field cacheManager:Lcom/intsig/camscanner/pdfengine/CacheManager;

.field callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

.field private currentPage:I

.field private currentXOffset:F

.field private currentYOffset:F

.field private debugPaint:Landroid/graphics/Paint;

.field private decodingAsyncTask:Lcom/intsig/camscanner/pdfengine/DecodingAsyncTask;

.field private defaultPage:I

.field private doubletapEnabled:Z

.field private dragPinchManager:Lcom/intsig/camscanner/pdfengine/DragPinchManager;

.field private enableAntialiasing:Z

.field private enableSwipe:Z

.field private hasSize:Z

.field private isScrollHandleInit:Z

.field private maxZoom:F

.field private midZoom:F

.field private minZoom:F

.field private nightMode:Z

.field private onDrawPagesNums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

.field private pageFling:Z

.field private pageSnap:Z

.field private pagesLoader:Lcom/intsig/camscanner/pdfengine/PagesLoader;

.field private paint:Landroid/graphics/Paint;

.field pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

.field private pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

.field private recycled:Z

.field private renderDuringScale:Z

.field renderingHandler:Lcom/intsig/camscanner/pdfengine/RenderingHandler;

.field private final renderingHandlerThread:Landroid/os/HandlerThread;

.field private scrollDir:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

.field private scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

.field private spacingPx:I

.field private state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

.field private swipeVertical:Z

.field private waitingDocumentConfigurator:Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

.field private zoom:F


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    const/high16 p2, 0x3f800000    # 1.0f

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->minZoom:F

    .line 7
    .line 8
    const/high16 v0, 0x3fe00000    # 1.75f

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->midZoom:F

    .line 11
    .line 12
    const/high16 v0, 0x40400000    # 3.0f

    .line 13
    .line 14
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->maxZoom:F

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;->NONE:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollDir:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 24
    .line 25
    iput p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 26
    .line 27
    const/4 p2, 0x1

    .line 28
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->recycled:Z

    .line 29
    .line 30
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$State;->DEFAULT:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 33
    .line 34
    new-instance v0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 35
    .line 36
    invoke-direct {v0}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 40
    .line 41
    sget-object v0, Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;->WIDTH:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 44
    .line 45
    const/4 v0, 0x0

    .line 46
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->defaultPage:I

    .line 47
    .line 48
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 49
    .line 50
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->enableSwipe:Z

    .line 51
    .line 52
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->doubletapEnabled:Z

    .line 53
    .line 54
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->nightMode:Z

    .line 55
    .line 56
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageSnap:Z

    .line 57
    .line 58
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->isScrollHandleInit:Z

    .line 59
    .line 60
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->bestQuality:Z

    .line 61
    .line 62
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->annotationRendering:Z

    .line 63
    .line 64
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderDuringScale:Z

    .line 65
    .line 66
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->enableAntialiasing:Z

    .line 67
    .line 68
    new-instance v1, Landroid/graphics/PaintFlagsDrawFilter;

    .line 69
    .line 70
    const/4 v2, 0x3

    .line 71
    invoke-direct {v1, v0, v2}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    .line 72
    .line 73
    .line 74
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->antialiasFilter:Landroid/graphics/PaintFlagsDrawFilter;

    .line 75
    .line 76
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->spacingPx:I

    .line 77
    .line 78
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->autoSpacing:Z

    .line 79
    .line 80
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageFling:Z

    .line 81
    .line 82
    new-instance p2, Ljava/util/ArrayList;

    .line 83
    .line 84
    const/16 v1, 0xa

    .line 85
    .line 86
    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 87
    .line 88
    .line 89
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->onDrawPagesNums:Ljava/util/List;

    .line 90
    .line 91
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->hasSize:Z

    .line 92
    .line 93
    new-instance p2, Landroid/os/HandlerThread;

    .line 94
    .line 95
    const-string v1, "PDF renderer"

    .line 96
    .line 97
    invoke-direct {p2, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandlerThread:Landroid/os/HandlerThread;

    .line 101
    .line 102
    invoke-virtual {p0}, Landroid/view/View;->isInEditMode()Z

    .line 103
    .line 104
    .line 105
    move-result p2

    .line 106
    if-eqz p2, :cond_0

    .line 107
    .line 108
    return-void

    .line 109
    :cond_0
    new-instance p2, Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 110
    .line 111
    invoke-direct {p2}, Lcom/intsig/camscanner/pdfengine/CacheManager;-><init>()V

    .line 112
    .line 113
    .line 114
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->cacheManager:Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 115
    .line 116
    new-instance p2, Lcom/intsig/camscanner/pdfengine/AnimationManager;

    .line 117
    .line 118
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pdfengine/AnimationManager;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;)V

    .line 119
    .line 120
    .line 121
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    .line 122
    .line 123
    new-instance v1, Lcom/intsig/camscanner/pdfengine/DragPinchManager;

    .line 124
    .line 125
    invoke-direct {v1, p0, p2}, Lcom/intsig/camscanner/pdfengine/DragPinchManager;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/AnimationManager;)V

    .line 126
    .line 127
    .line 128
    iput-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->dragPinchManager:Lcom/intsig/camscanner/pdfengine/DragPinchManager;

    .line 129
    .line 130
    new-instance p2, Lcom/intsig/camscanner/pdfengine/PagesLoader;

    .line 131
    .line 132
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pdfengine/PagesLoader;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;)V

    .line 133
    .line 134
    .line 135
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pagesLoader:Lcom/intsig/camscanner/pdfengine/PagesLoader;

    .line 136
    .line 137
    new-instance p2, Landroid/graphics/Paint;

    .line 138
    .line 139
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 140
    .line 141
    .line 142
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->paint:Landroid/graphics/Paint;

    .line 143
    .line 144
    new-instance p2, Landroid/graphics/Paint;

    .line 145
    .line 146
    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    .line 147
    .line 148
    .line 149
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->debugPaint:Landroid/graphics/Paint;

    .line 150
    .line 151
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 152
    .line 153
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 154
    .line 155
    .line 156
    new-instance p2, Lcom/shockwave/pdfium/PdfiumCore;

    .line 157
    .line 158
    invoke-direct {p2, p1}, Lcom/shockwave/pdfium/PdfiumCore;-><init>(Landroid/content/Context;)V

    .line 159
    .line 160
    .line 161
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    .line 162
    .line 163
    invoke-virtual {p0, v0}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 164
    .line 165
    .line 166
    return-void
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;[I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pdfengine/PDFView;->load(Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;[I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pdfengine/PDFView;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setSwipeVertical(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/pdfengine/PDFView;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setAutoSpacing(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private drawPart(Landroid/graphics/Canvas;Lcom/intsig/camscanner/pdfengine/model/PagePart;)V
    .locals 11

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getPageRelativeBounds()Landroid/graphics/RectF;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getRenderedBitmap()Landroid/graphics/Bitmap;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-eqz v2, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 17
    .line 18
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getPage()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    iget-boolean v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 27
    .line 28
    const/high16 v4, 0x40000000    # 2.0f

    .line 29
    .line 30
    if-eqz v3, :cond_1

    .line 31
    .line 32
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 33
    .line 34
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getPage()I

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    iget v6, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 39
    .line 40
    invoke-virtual {v3, v5, v6}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    iget-object v5, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 45
    .line 46
    invoke-virtual {v5}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageWidth()F

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 51
    .line 52
    .line 53
    move-result v6

    .line 54
    sub-float/2addr v5, v6

    .line 55
    invoke-virtual {p0, v5}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 56
    .line 57
    .line 58
    move-result v5

    .line 59
    div-float/2addr v5, v4

    .line 60
    goto :goto_0

    .line 61
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 62
    .line 63
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getPage()I

    .line 64
    .line 65
    .line 66
    move-result v5

    .line 67
    iget v6, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 68
    .line 69
    invoke-virtual {v3, v5, v6}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 70
    .line 71
    .line 72
    move-result v5

    .line 73
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 74
    .line 75
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageHeight()F

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 80
    .line 81
    .line 82
    move-result v6

    .line 83
    sub-float/2addr v3, v6

    .line 84
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 85
    .line 86
    .line 87
    move-result v3

    .line 88
    div-float/2addr v3, v4

    .line 89
    :goto_0
    invoke-virtual {p1, v5, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 90
    .line 91
    .line 92
    new-instance v4, Landroid/graphics/Rect;

    .line 93
    .line 94
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 95
    .line 96
    .line 97
    move-result v6

    .line 98
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 99
    .line 100
    .line 101
    move-result v7

    .line 102
    const/4 v8, 0x0

    .line 103
    invoke-direct {v4, v8, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 104
    .line 105
    .line 106
    iget v6, v0, Landroid/graphics/RectF;->left:F

    .line 107
    .line 108
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 109
    .line 110
    .line 111
    move-result v7

    .line 112
    mul-float v6, v6, v7

    .line 113
    .line 114
    invoke-virtual {p0, v6}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 115
    .line 116
    .line 117
    move-result v6

    .line 118
    iget v7, v0, Landroid/graphics/RectF;->top:F

    .line 119
    .line 120
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 121
    .line 122
    .line 123
    move-result v8

    .line 124
    mul-float v7, v7, v8

    .line 125
    .line 126
    invoke-virtual {p0, v7}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 127
    .line 128
    .line 129
    move-result v7

    .line 130
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 131
    .line 132
    .line 133
    move-result v8

    .line 134
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 135
    .line 136
    .line 137
    move-result v9

    .line 138
    mul-float v8, v8, v9

    .line 139
    .line 140
    invoke-virtual {p0, v8}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 141
    .line 142
    .line 143
    move-result v8

    .line 144
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 145
    .line 146
    .line 147
    move-result v0

    .line 148
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 149
    .line 150
    .line 151
    move-result v2

    .line 152
    mul-float v0, v0, v2

    .line 153
    .line 154
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 155
    .line 156
    .line 157
    move-result v0

    .line 158
    new-instance v2, Landroid/graphics/RectF;

    .line 159
    .line 160
    float-to-int v9, v6

    .line 161
    int-to-float v9, v9

    .line 162
    float-to-int v10, v7

    .line 163
    int-to-float v10, v10

    .line 164
    add-float/2addr v6, v8

    .line 165
    float-to-int v6, v6

    .line 166
    int-to-float v6, v6

    .line 167
    add-float/2addr v7, v0

    .line 168
    float-to-int v0, v7

    .line 169
    int-to-float v0, v0

    .line 170
    invoke-direct {v2, v9, v10, v6, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 171
    .line 172
    .line 173
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 174
    .line 175
    add-float/2addr v0, v5

    .line 176
    iget v6, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 177
    .line 178
    add-float/2addr v6, v3

    .line 179
    iget v7, v2, Landroid/graphics/RectF;->left:F

    .line 180
    .line 181
    add-float/2addr v7, v0

    .line 182
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 183
    .line 184
    .line 185
    move-result v8

    .line 186
    int-to-float v8, v8

    .line 187
    cmpl-float v7, v7, v8

    .line 188
    .line 189
    if-gez v7, :cond_5

    .line 190
    .line 191
    iget v7, v2, Landroid/graphics/RectF;->right:F

    .line 192
    .line 193
    add-float/2addr v0, v7

    .line 194
    const/4 v7, 0x0

    .line 195
    cmpg-float v0, v0, v7

    .line 196
    .line 197
    if-lez v0, :cond_5

    .line 198
    .line 199
    iget v0, v2, Landroid/graphics/RectF;->top:F

    .line 200
    .line 201
    add-float/2addr v0, v6

    .line 202
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 203
    .line 204
    .line 205
    move-result v8

    .line 206
    int-to-float v8, v8

    .line 207
    cmpl-float v0, v0, v8

    .line 208
    .line 209
    if-gez v0, :cond_5

    .line 210
    .line 211
    iget v0, v2, Landroid/graphics/RectF;->bottom:F

    .line 212
    .line 213
    add-float/2addr v6, v0

    .line 214
    cmpg-float v0, v6, v7

    .line 215
    .line 216
    if-gtz v0, :cond_2

    .line 217
    .line 218
    goto :goto_2

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->paint:Landroid/graphics/Paint;

    .line 220
    .line 221
    invoke-virtual {p1, v1, v4, v2, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 222
    .line 223
    .line 224
    sget-boolean v0, Lcom/intsig/camscanner/pdfengine/utils/Constants;->DEBUG_MODE:Z

    .line 225
    .line 226
    if-eqz v0, :cond_4

    .line 227
    .line 228
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->debugPaint:Landroid/graphics/Paint;

    .line 229
    .line 230
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getPage()I

    .line 231
    .line 232
    .line 233
    move-result p2

    .line 234
    rem-int/lit8 p2, p2, 0x2

    .line 235
    .line 236
    if-nez p2, :cond_3

    .line 237
    .line 238
    const/high16 p2, -0x10000

    .line 239
    .line 240
    goto :goto_1

    .line 241
    :cond_3
    const p2, -0xffff01

    .line 242
    .line 243
    .line 244
    :goto_1
    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 245
    .line 246
    .line 247
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->debugPaint:Landroid/graphics/Paint;

    .line 248
    .line 249
    invoke-virtual {p1, v2, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 250
    .line 251
    .line 252
    :cond_4
    neg-float p2, v5

    .line 253
    neg-float v0, v3

    .line 254
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 255
    .line 256
    .line 257
    return-void

    .line 258
    :cond_5
    :goto_2
    neg-float p2, v5

    .line 259
    neg-float v0, v3

    .line 260
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 261
    .line 262
    .line 263
    return-void
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private drawWithListener(Landroid/graphics/Canvas;ILcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)V
    .locals 4

    .line 1
    if-eqz p3, :cond_1

    .line 2
    .line 3
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 9
    .line 10
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 11
    .line 12
    invoke-virtual {v0, p2, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 18
    .line 19
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 20
    .line 21
    invoke-virtual {v0, p2, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    move v1, v0

    .line 26
    const/4 v0, 0x0

    .line 27
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 31
    .line 32
    invoke-virtual {v2, p2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    invoke-virtual {v2}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-interface {p3, p1, v3, v2, p2}, Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;->onLayerDrawn(Landroid/graphics/Canvas;FFI)V

    .line 53
    .line 54
    .line 55
    neg-float p2, v1

    .line 56
    neg-float p3, v0

    .line 57
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 58
    .line 59
    .line 60
    :cond_1
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private load(Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->load(Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;[I)V

    return-void
.end method

.method private load(Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;[I)V
    .locals 8

    .line 2
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->recycled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->recycled:Z

    .line 4
    new-instance v7, Lcom/intsig/camscanner/pdfengine/DecodingAsyncTask;

    iget-object v6, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfiumCore:Lcom/shockwave/pdfium/PdfiumCore;

    move-object v1, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p0

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/pdfengine/DecodingAsyncTask;-><init>(Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;[ILcom/intsig/camscanner/pdfengine/PDFView;Lcom/shockwave/pdfium/PdfiumCore;)V

    iput-object v7, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->decodingAsyncTask:Lcom/intsig/camscanner/pdfengine/DecodingAsyncTask;

    .line 5
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array p2, v0, [Ljava/lang/Void;

    invoke-virtual {v7, p1, p2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Don\'t call load on a PDF View without recycling it first."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setScrollHandle(Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/pdfengine/PDFView;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setDefaultPage(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private setAutoSpacing(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->autoSpacing:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private setDefaultPage(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->defaultPage:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private setPageFitPolicy(Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private setScrollHandle(Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private setSpacing(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0, p1}, Lcom/intsig/camscanner/pdfengine/utils/Util;->getDP(Landroid/content/Context;I)I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->spacingPx:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private setSwipeVertical(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/pdfengine/PDFView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->hasSize:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pdfengine/PDFView;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setSpacing(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->waitingDocumentConfigurator:Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdfengine/PDFView;->load(Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setPageFitPolicy(Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public canScrollHorizontally(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    if-eqz v2, :cond_2

    .line 11
    .line 12
    if-gez p1, :cond_1

    .line 13
    .line 14
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 15
    .line 16
    cmpg-float v2, v2, v3

    .line 17
    .line 18
    if-gez v2, :cond_1

    .line 19
    .line 20
    return v1

    .line 21
    :cond_1
    if-lez p1, :cond_4

    .line 22
    .line 23
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageWidth()F

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    add-float/2addr p1, v0

    .line 34
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    int-to-float v0, v0

    .line 39
    cmpl-float p1, p1, v0

    .line 40
    .line 41
    if-lez p1, :cond_4

    .line 42
    .line 43
    return v1

    .line 44
    :cond_2
    if-gez p1, :cond_3

    .line 45
    .line 46
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 47
    .line 48
    cmpg-float v2, v2, v3

    .line 49
    .line 50
    if-gez v2, :cond_3

    .line 51
    .line 52
    return v1

    .line 53
    :cond_3
    if-lez p1, :cond_4

    .line 54
    .line 55
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 56
    .line 57
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 58
    .line 59
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    add-float/2addr p1, v0

    .line 64
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    int-to-float v0, v0

    .line 69
    cmpl-float p1, p1, v0

    .line 70
    .line 71
    if-lez p1, :cond_4

    .line 72
    .line 73
    return v1

    .line 74
    :cond_4
    const/4 p1, 0x0

    .line 75
    return p1
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public canScrollVertically(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    if-eqz v2, :cond_2

    .line 11
    .line 12
    if-gez p1, :cond_1

    .line 13
    .line 14
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 15
    .line 16
    cmpg-float v2, v2, v3

    .line 17
    .line 18
    if-gez v2, :cond_1

    .line 19
    .line 20
    return v1

    .line 21
    :cond_1
    if-lez p1, :cond_4

    .line 22
    .line 23
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 24
    .line 25
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 26
    .line 27
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    add-float/2addr p1, v0

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    int-to-float v0, v0

    .line 37
    cmpl-float p1, p1, v0

    .line 38
    .line 39
    if-lez p1, :cond_4

    .line 40
    .line 41
    return v1

    .line 42
    :cond_2
    if-gez p1, :cond_3

    .line 43
    .line 44
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 45
    .line 46
    cmpg-float v2, v2, v3

    .line 47
    .line 48
    if-gez v2, :cond_3

    .line 49
    .line 50
    return v1

    .line 51
    :cond_3
    if-lez p1, :cond_4

    .line 52
    .line 53
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageHeight()F

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    add-float/2addr p1, v0

    .line 64
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    int-to-float v0, v0

    .line 69
    cmpl-float p1, p1, v0

    .line 70
    .line 71
    if-lez p1, :cond_4

    .line 72
    .line 73
    return v1

    .line 74
    :cond_4
    const/4 p1, 0x0

    .line 75
    return p1
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public computeScroll()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/RelativeLayout;->computeScroll()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->isInEditMode()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->computeFling()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public doAutoSpacing()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->autoSpacing:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public doPageFling()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageFling:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public doPageSnap()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageSnap:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public doRenderDuringScale()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderDuringScale:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public documentFitsView()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    const/4 v3, 0x0

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    int-to-float v1, v1

    .line 20
    cmpg-float v0, v0, v1

    .line 21
    .line 22
    if-gez v0, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v2, 0x0

    .line 26
    :goto_0
    return v2

    .line 27
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    int-to-float v1, v1

    .line 32
    cmpg-float v0, v0, v1

    .line 33
    .line 34
    if-gez v0, :cond_2

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_2
    const/4 v2, 0x0

    .line 38
    :goto_1
    return v2
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public enableAnnotationRendering(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->annotationRendering:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public enableAntialiasing(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->enableAntialiasing:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method enableDoubletap(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->doubletapEnabled:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public enableRenderDuringScale(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderDuringScale:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method findFocusPage(FF)I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move p1, p2

    .line 6
    :cond_0
    if-eqz v0, :cond_1

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 9
    .line 10
    .line 11
    move-result p2

    .line 12
    goto :goto_0

    .line 13
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 14
    .line 15
    .line 16
    move-result p2

    .line 17
    :goto_0
    int-to-float p2, p2

    .line 18
    const/high16 v0, -0x40800000    # -1.0f

    .line 19
    .line 20
    cmpl-float v0, p1, v0

    .line 21
    .line 22
    if-lez v0, :cond_2

    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    return p1

    .line 26
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 27
    .line 28
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    neg-float v0, v0

    .line 35
    add-float/2addr v0, p2

    .line 36
    const/high16 v1, 0x3f800000    # 1.0f

    .line 37
    .line 38
    add-float/2addr v0, v1

    .line 39
    cmpg-float v0, p1, v0

    .line 40
    .line 41
    if-gez v0, :cond_3

    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    add-int/lit8 p1, p1, -0x1

    .line 50
    .line 51
    return p1

    .line 52
    :cond_3
    const/high16 v0, 0x40000000    # 2.0f

    .line 53
    .line 54
    div-float/2addr p2, v0

    .line 55
    sub-float/2addr p1, p2

    .line 56
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 57
    .line 58
    neg-float p1, p1

    .line 59
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 60
    .line 61
    invoke-virtual {p2, p1, v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageAtOffset(FF)I

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    return p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method findSnapEdge(I)Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageSnap:Z

    .line 2
    .line 3
    if-eqz v0, :cond_6

    .line 4
    .line 5
    if-gez p1, :cond_0

    .line 6
    .line 7
    goto :goto_2

    .line 8
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 16
    .line 17
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 18
    .line 19
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 20
    .line 21
    invoke-virtual {v1, p1, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    neg-float v1, v1

    .line 26
    iget-boolean v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 27
    .line 28
    if-eqz v2, :cond_2

    .line 29
    .line 30
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    goto :goto_1

    .line 35
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    :goto_1
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 40
    .line 41
    iget v4, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 42
    .line 43
    invoke-virtual {v3, p1, v4}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageLength(IF)F

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    int-to-float v2, v2

    .line 48
    cmpl-float v3, v2, p1

    .line 49
    .line 50
    if-ltz v3, :cond_3

    .line 51
    .line 52
    sget-object p1, Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;->CENTER:Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 53
    .line 54
    return-object p1

    .line 55
    :cond_3
    cmpl-float v3, v0, v1

    .line 56
    .line 57
    if-ltz v3, :cond_4

    .line 58
    .line 59
    sget-object p1, Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;->START:Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 60
    .line 61
    return-object p1

    .line 62
    :cond_4
    sub-float/2addr v1, p1

    .line 63
    sub-float/2addr v0, v2

    .line 64
    cmpl-float p1, v1, v0

    .line 65
    .line 66
    if-lez p1, :cond_5

    .line 67
    .line 68
    sget-object p1, Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;->END:Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 69
    .line 70
    return-object p1

    .line 71
    :cond_5
    sget-object p1, Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;->NONE:Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 72
    .line 73
    return-object p1

    .line 74
    :cond_6
    :goto_2
    sget-object p1, Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;->NONE:Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 75
    .line 76
    return-object p1
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public fitToWidth(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/pdfengine/PDFView$State;->SHOWN:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 4
    .line 5
    if-eq v0, v1, :cond_0

    .line 6
    .line 7
    sget-object p1, Lcom/intsig/camscanner/pdfengine/PDFView;->TAG:Ljava/lang/String;

    .line 8
    .line 9
    const-string v0, "Cannot fit, document not rendered yet"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    int-to-float v0, v0

    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 21
    .line 22
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    div-float/2addr v0, v1

    .line 31
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->zoomTo(F)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->jumpTo(I)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public fromAsset(Ljava/lang/String;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pdfengine/source/AssetSource;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/pdfengine/source/AssetSource;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Lcom/intsig/camscanner/pdfengine/〇080;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public fromBytes([B)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pdfengine/source/ByteArraySource;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/pdfengine/source/ByteArraySource;-><init>([B)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Lcom/intsig/camscanner/pdfengine/〇080;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public fromFile(Ljava/io/File;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pdfengine/source/FileSource;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/pdfengine/source/FileSource;-><init>(Ljava/io/File;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Lcom/intsig/camscanner/pdfengine/〇080;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public fromSource(Lcom/intsig/camscanner/pdfengine/source/DocumentSource;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, p1, v1}, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Lcom/intsig/camscanner/pdfengine/〇080;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public fromStream(Ljava/io/InputStream;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pdfengine/source/InputStreamSource;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/pdfengine/source/InputStreamSource;-><init>(Ljava/io/InputStream;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Lcom/intsig/camscanner/pdfengine/〇080;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public fromUri(Landroid/net/Uri;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pdfengine/source/UriSource;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/pdfengine/source/UriSource;-><init>(Landroid/net/Uri;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Lcom/intsig/camscanner/pdfengine/〇080;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getCurrentPage()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCurrentXOffset()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCurrentYOffset()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDocumentMeta()Lcom/shockwave/pdfium/PdfDocument$Meta;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMetaData()Lcom/shockwave/pdfium/PdfDocument$Meta;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getLinks(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/shockwave/pdfium/PdfDocument$Link;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    return-object p1

    .line 10
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageLinks(I)Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getMaxZoom()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->maxZoom:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMidZoom()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->midZoom:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMinZoom()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->minZoom:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageAtPositionOffset(F)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    mul-float v1, v1, p1

    .line 10
    .line 11
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 12
    .line 13
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageAtOffset(FF)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
.end method

.method public getPageCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageFitPolicy()Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance p1, Lcom/shockwave/pdfium/util/SizeF;

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-direct {p1, v0, v0}, Lcom/shockwave/pdfium/util/SizeF;-><init>(FF)V

    .line 9
    .line 10
    .line 11
    return-object p1

    .line 12
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    return-object p1
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getPositionOffset()F
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 6
    .line 7
    neg-float v0, v0

    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 9
    .line 10
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 22
    .line 23
    neg-float v0, v0

    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 25
    .line 26
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 27
    .line 28
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    :goto_0
    int-to-float v2, v2

    .line 37
    sub-float/2addr v1, v2

    .line 38
    div-float/2addr v0, v1

    .line 39
    const/4 v1, 0x0

    .line 40
    const/high16 v2, 0x3f800000    # 1.0f

    .line 41
    .line 42
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/pdfengine/utils/MathUtils;->limit(FFF)F

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    return v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method getScrollHandle()Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSpacingPx()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->spacingPx:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTableOfContents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/shockwave/pdfium/PdfDocument$Bookmark;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0

    .line 10
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getBookmarks()Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getZoom()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isAnnotationRendering()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->annotationRendering:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isAntialiasing()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->enableAntialiasing:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isBestQuality()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->bestQuality:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method isDoubletapEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->doubletapEnabled:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isRecycled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->recycled:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isSwipeEnabled()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->enableSwipe:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isSwipeVertical()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isZooming()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->minZoom:F

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/utils/CommonUtil;->〇〇8O0〇8(FF)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    xor-int/lit8 v0, v0, 0x1

    .line 10
    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public jumpTo(I)V
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->jumpTo(IZ)V

    return-void
.end method

.method public jumpTo(IZ)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->determineValidPageNumberFrom(I)I

    move-result p1

    if-nez p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    move-result v0

    neg-float v0, v0

    .line 4
    :goto_0
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    if-eqz v1, :cond_3

    if-eqz p2, :cond_2

    .line 5
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    invoke-virtual {p2, v1, v0}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->startYAnimation(FF)V

    goto :goto_1

    .line 6
    :cond_2
    iget p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    invoke-virtual {p0, p2, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FF)V

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    .line 7
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    invoke-virtual {p2, v1, v0}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->startXAnimation(FF)V

    goto :goto_1

    .line 8
    :cond_4
    iget p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    invoke-virtual {p0, v0, p2}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FF)V

    .line 9
    :goto_1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->showPage(I)V

    return-void
.end method

.method loadComplete(Lcom/intsig/camscanner/pdfengine/entity/PdfFile;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$State;->LOADED:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 4
    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandlerThread:Landroid/os/HandlerThread;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandlerThread:Landroid/os/HandlerThread;

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 18
    .line 19
    .line 20
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/pdfengine/RenderingHandler;

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandlerThread:Landroid/os/HandlerThread;

    .line 23
    .line 24
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-direct {v0, v1, p0}, Lcom/intsig/camscanner/pdfengine/RenderingHandler;-><init>(Landroid/os/Looper;Lcom/intsig/camscanner/pdfengine/PDFView;)V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandler:Lcom/intsig/camscanner/pdfengine/RenderingHandler;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/RenderingHandler;->start()V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 37
    .line 38
    if-eqz v0, :cond_1

    .line 39
    .line 40
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;->setupLayout(Lcom/intsig/camscanner/pdfengine/PDFView;)V

    .line 41
    .line 42
    .line 43
    const/4 v0, 0x1

    .line 44
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->isScrollHandleInit:Z

    .line 45
    .line 46
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->dragPinchManager:Lcom/intsig/camscanner/pdfengine/DragPinchManager;

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/DragPinchManager;->enable()V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->callOnLoadComplete(I)V

    .line 58
    .line 59
    .line 60
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->defaultPage:I

    .line 61
    .line 62
    const/4 v0, 0x0

    .line 63
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->jumpTo(IZ)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
.end method

.method loadError(Ljava/lang/Throwable;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$State;->ERROR:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->getOnError()Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->recycle()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 15
    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;->onError(Ljava/lang/Throwable;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const-string v0, "PDFView"

    .line 24
    .line 25
    const-string v1, "load pdf error"

    .line 26
    .line 27
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    .line 29
    .line 30
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method loadPageByOffset()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 11
    .line 12
    const/high16 v1, 0x40000000    # 2.0f

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    goto :goto_0

    .line 23
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 24
    .line 25
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    :goto_0
    int-to-float v2, v2

    .line 30
    div-float/2addr v2, v1

    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 32
    .line 33
    sub-float/2addr v0, v2

    .line 34
    neg-float v0, v0

    .line 35
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 36
    .line 37
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageAtOffset(FF)I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-ltz v0, :cond_2

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 44
    .line 45
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    add-int/lit8 v1, v1, -0x1

    .line 50
    .line 51
    if-gt v0, v1, :cond_2

    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->getCurrentPage()I

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    if-eq v0, v1, :cond_2

    .line 58
    .line 59
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->showPage(I)V

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->loadPages()V

    .line 64
    .line 65
    .line 66
    :goto_1
    return-void
    .line 67
    .line 68
.end method

.method public loadPages()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandler:Lcom/intsig/camscanner/pdfengine/RenderingHandler;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v1, 0x1

    .line 11
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->cacheManager:Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/CacheManager;->makeANewSet()V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pagesLoader:Lcom/intsig/camscanner/pdfengine/PagesLoader;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PagesLoader;->loadPages()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->redraw()V

    .line 25
    .line 26
    .line 27
    :cond_1
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public moveRelativeTo(FF)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 2
    .line 3
    add-float/2addr v0, p1

    .line 4
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 5
    .line 6
    add-float/2addr p1, p2

    .line 7
    invoke-virtual {p0, v0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FF)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public moveTo(FF)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FFZ)V

    return-void
.end method

.method public moveTo(FFZ)V
    .locals 5

    .line 2
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    const/high16 v1, 0x40000000    # 2.0f

    const/4 v2, 0x0

    if-eqz v0, :cond_8

    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageWidth()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    move-result v0

    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_0

    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, v1

    div-float/2addr v0, v1

    :goto_0
    sub-float/2addr p1, v0

    goto :goto_1

    :cond_0
    cmpl-float v3, p1, v2

    if-lez v3, :cond_1

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    add-float v3, p1, v0

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    int-to-float p1, p1

    goto :goto_0

    .line 8
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    iget v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    move-result v0

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_3

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p2

    int-to-float p2, p2

    sub-float/2addr p2, v0

    div-float/2addr p2, v1

    goto :goto_2

    :cond_3
    cmpl-float v1, p2, v2

    if-lez v1, :cond_4

    const/4 p2, 0x0

    goto :goto_2

    :cond_4
    add-float v1, p2, v0

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_5

    neg-float p2, v0

    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr p2, v0

    .line 13
    :cond_5
    :goto_2
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    cmpg-float v1, p2, v0

    if-gez v1, :cond_6

    .line 14
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;->END:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollDir:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    goto/16 :goto_6

    :cond_6
    cmpl-float v0, p2, v0

    if-lez v0, :cond_7

    .line 15
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;->START:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollDir:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    goto/16 :goto_6

    .line 16
    :cond_7
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;->NONE:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollDir:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    goto/16 :goto_6

    .line 17
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getMaxPageHeight()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->toCurrentScale(F)F

    move-result v0

    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_9

    .line 19
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr p2, v1

    div-float/2addr v0, v1

    :goto_3
    sub-float/2addr p2, v0

    goto :goto_4

    :cond_9
    cmpl-float v3, p2, v2

    if-lez v3, :cond_a

    const/4 p2, 0x0

    goto :goto_4

    :cond_a
    add-float v3, p2, v0

    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_b

    .line 21
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p2

    int-to-float p2, p2

    goto :goto_3

    .line 22
    :cond_b
    :goto_4
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    iget v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    move-result v0

    .line 23
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v0, v3

    if-gez v3, :cond_c

    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p1

    int-to-float p1, p1

    sub-float/2addr p1, v0

    div-float/2addr p1, v1

    goto :goto_5

    :cond_c
    cmpl-float v1, p1, v2

    if-lez v1, :cond_d

    const/4 p1, 0x0

    goto :goto_5

    :cond_d
    add-float v1, p1, v0

    .line 25
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_e

    neg-float p1, v0

    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr p1, v0

    .line 27
    :cond_e
    :goto_5
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    cmpg-float v1, p1, v0

    if-gez v1, :cond_f

    .line 28
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;->END:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollDir:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    goto :goto_6

    :cond_f
    cmpl-float v0, p1, v0

    if-lez v0, :cond_10

    .line 29
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;->START:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollDir:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    goto :goto_6

    .line 30
    :cond_10
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;->NONE:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollDir:Lcom/intsig/camscanner/pdfengine/PDFView$ScrollDir;

    .line 31
    :goto_6
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 32
    iput p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->getPositionOffset()F

    move-result p1

    if-eqz p3, :cond_11

    .line 34
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    if-eqz p2, :cond_11

    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->documentFitsView()Z

    move-result p2

    if-nez p2, :cond_11

    .line 35
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    invoke-interface {p2, p1}, Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;->setScroll(F)V

    .line 36
    :cond_11
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->getCurrentPage()I

    move-result p3

    invoke-virtual {p2, p3, p1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->callOnPageScroll(IF)V

    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->redraw()V

    return-void
.end method

.method public onBitmapRendered(Lcom/intsig/camscanner/pdfengine/model/PagePart;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/pdfengine/PDFView$State;->LOADED:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$State;->SHOWN:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->callOnRender(I)V

    .line 20
    .line 21
    .line 22
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->isThumbnail()Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->cacheManager:Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 29
    .line 30
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/CacheManager;->cacheThumbnail(Lcom/intsig/camscanner/pdfengine/model/PagePart;)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->cacheManager:Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 35
    .line 36
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/CacheManager;->cachePart(Lcom/intsig/camscanner/pdfengine/model/PagePart;)V

    .line 37
    .line 38
    .line 39
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->redraw()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->recycle()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->isInEditMode()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->enableAntialiasing:Z

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->antialiasFilter:Landroid/graphics/PaintFlagsDrawFilter;

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 15
    .line 16
    .line 17
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-nez v0, :cond_3

    .line 22
    .line 23
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->nightMode:Z

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    const/high16 v0, -0x1000000

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_2
    const/4 v0, -0x1

    .line 31
    :goto_0
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_3
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 36
    .line 37
    .line 38
    :goto_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->recycled:Z

    .line 39
    .line 40
    if-eqz v0, :cond_4

    .line 41
    .line 42
    return-void

    .line 43
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 44
    .line 45
    sget-object v1, Lcom/intsig/camscanner/pdfengine/PDFView$State;->SHOWN:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 46
    .line 47
    if-eq v0, v1, :cond_5

    .line 48
    .line 49
    return-void

    .line 50
    :cond_5
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 51
    .line 52
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 53
    .line 54
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 55
    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->cacheManager:Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 58
    .line 59
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/CacheManager;->getThumbnails()Ljava/util/List;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    if-eqz v3, :cond_6

    .line 72
    .line 73
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v3

    .line 77
    check-cast v3, Lcom/intsig/camscanner/pdfengine/model/PagePart;

    .line 78
    .line 79
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/pdfengine/PDFView;->drawPart(Landroid/graphics/Canvas;Lcom/intsig/camscanner/pdfengine/model/PagePart;)V

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_6
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->cacheManager:Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 84
    .line 85
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/CacheManager;->getPageParts()Ljava/util/List;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    :cond_7
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    if-eqz v3, :cond_8

    .line 98
    .line 99
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    move-result-object v3

    .line 103
    check-cast v3, Lcom/intsig/camscanner/pdfengine/model/PagePart;

    .line 104
    .line 105
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/pdfengine/PDFView;->drawPart(Landroid/graphics/Canvas;Lcom/intsig/camscanner/pdfengine/model/PagePart;)V

    .line 106
    .line 107
    .line 108
    iget-object v4, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 109
    .line 110
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->getOnDrawAll()Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 111
    .line 112
    .line 113
    move-result-object v4

    .line 114
    if-eqz v4, :cond_7

    .line 115
    .line 116
    iget-object v4, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->onDrawPagesNums:Ljava/util/List;

    .line 117
    .line 118
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getPage()I

    .line 119
    .line 120
    .line 121
    move-result v5

    .line 122
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 123
    .line 124
    .line 125
    move-result-object v5

    .line 126
    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 127
    .line 128
    .line 129
    move-result v4

    .line 130
    if-nez v4, :cond_7

    .line 131
    .line 132
    iget-object v4, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->onDrawPagesNums:Ljava/util/List;

    .line 133
    .line 134
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdfengine/model/PagePart;->getPage()I

    .line 135
    .line 136
    .line 137
    move-result v3

    .line 138
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 139
    .line 140
    .line 141
    move-result-object v3

    .line 142
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    .line 144
    .line 145
    goto :goto_3

    .line 146
    :cond_8
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->onDrawPagesNums:Ljava/util/List;

    .line 147
    .line 148
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 149
    .line 150
    .line 151
    move-result-object v2

    .line 152
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 153
    .line 154
    .line 155
    move-result v3

    .line 156
    if-eqz v3, :cond_9

    .line 157
    .line 158
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 159
    .line 160
    .line 161
    move-result-object v3

    .line 162
    check-cast v3, Ljava/lang/Integer;

    .line 163
    .line 164
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 165
    .line 166
    .line 167
    move-result v3

    .line 168
    iget-object v4, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 169
    .line 170
    invoke-virtual {v4}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->getOnDrawAll()Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 171
    .line 172
    .line 173
    move-result-object v4

    .line 174
    invoke-direct {p0, p1, v3, v4}, Lcom/intsig/camscanner/pdfengine/PDFView;->drawWithListener(Landroid/graphics/Canvas;ILcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)V

    .line 175
    .line 176
    .line 177
    goto :goto_4

    .line 178
    :cond_9
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->onDrawPagesNums:Ljava/util/List;

    .line 179
    .line 180
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 181
    .line 182
    .line 183
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 184
    .line 185
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 186
    .line 187
    invoke-virtual {v3}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->getOnDraw()Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 188
    .line 189
    .line 190
    move-result-object v3

    .line 191
    invoke-direct {p0, p1, v2, v3}, Lcom/intsig/camscanner/pdfengine/PDFView;->drawWithListener(Landroid/graphics/Canvas;ILcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)V

    .line 192
    .line 193
    .line 194
    neg-float v0, v0

    .line 195
    neg-float v1, v1

    .line 196
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 197
    .line 198
    .line 199
    return-void
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method onPageError(Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException;->getPage()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->callOnPageError(ILjava/lang/Throwable;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView;->TAG:Ljava/lang/String;

    .line 18
    .line 19
    new-instance v1, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v2, "Cannot open page "

    .line 25
    .line 26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException;->getPage()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 45
    .line 46
    .line 47
    :cond_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onScrollEndListener(Lcom/intsig/camscanner/pdfengine/DragPinchManager$ScrollEndListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->dragPinchManager:Lcom/intsig/camscanner/pdfengine/DragPinchManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/DragPinchManager;->setScrollEndListener(Lcom/intsig/camscanner/pdfengine/DragPinchManager$ScrollEndListener;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 1
    const/4 p3, 0x1

    .line 2
    iput-boolean p3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->hasSize:Z

    .line 3
    .line 4
    iget-object p3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->waitingDocumentConfigurator:Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 5
    .line 6
    if-eqz p3, :cond_0

    .line 7
    .line 8
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->load()V

    .line 9
    .line 10
    .line 11
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->isInEditMode()Z

    .line 12
    .line 13
    .line 14
    move-result p3

    .line 15
    if-nez p3, :cond_3

    .line 16
    .line 17
    iget-object p3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 18
    .line 19
    sget-object p4, Lcom/intsig/camscanner/pdfengine/PDFView$State;->SHOWN:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 20
    .line 21
    if-eq p3, p4, :cond_1

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    iget-object p3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    .line 25
    .line 26
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->stopAll()V

    .line 27
    .line 28
    .line 29
    iget-object p3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 30
    .line 31
    new-instance p4, Lcom/shockwave/pdfium/util/Size;

    .line 32
    .line 33
    invoke-direct {p4, p1, p2}, Lcom/shockwave/pdfium/util/Size;-><init>(II)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->recalculatePageSizes(Lcom/shockwave/pdfium/util/Size;)V

    .line 37
    .line 38
    .line 39
    iget-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 40
    .line 41
    if-eqz p1, :cond_2

    .line 42
    .line 43
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 44
    .line 45
    iget-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 46
    .line 47
    iget p3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 48
    .line 49
    iget p4, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 50
    .line 51
    invoke-virtual {p2, p3, p4}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 52
    .line 53
    .line 54
    move-result p2

    .line 55
    neg-float p2, p2

    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FF)V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 61
    .line 62
    iget p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 63
    .line 64
    iget p3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 65
    .line 66
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    neg-float p1, p1

    .line 71
    iget p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 72
    .line 73
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FF)V

    .line 74
    .line 75
    .line 76
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->loadPageByOffset()V

    .line 77
    .line 78
    .line 79
    :cond_3
    :goto_1
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public pageFillsScreen()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    neg-float v0, v0

    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 13
    .line 14
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 15
    .line 16
    iget v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 17
    .line 18
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageLength(IF)F

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    sub-float v1, v0, v1

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->isSwipeVertical()Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    const/4 v3, 0x1

    .line 29
    const/4 v4, 0x0

    .line 30
    if-eqz v2, :cond_1

    .line 31
    .line 32
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 33
    .line 34
    cmpl-float v0, v0, v2

    .line 35
    .line 36
    if-lez v0, :cond_0

    .line 37
    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    int-to-float v0, v0

    .line 43
    sub-float/2addr v2, v0

    .line 44
    cmpg-float v0, v1, v2

    .line 45
    .line 46
    if-gez v0, :cond_0

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/4 v3, 0x0

    .line 50
    :goto_0
    return v3

    .line 51
    :cond_1
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 52
    .line 53
    cmpl-float v0, v0, v2

    .line 54
    .line 55
    if-lez v0, :cond_2

    .line 56
    .line 57
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    int-to-float v0, v0

    .line 62
    sub-float/2addr v2, v0

    .line 63
    cmpg-float v0, v1, v2

    .line 64
    .line 65
    if-gez v0, :cond_2

    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_2
    const/4 v3, 0x0

    .line 69
    :goto_1
    return v3
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public performPageSnap()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageSnap:Z

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 6
    .line 7
    if-eqz v0, :cond_3

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 19
    .line 20
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->findFocusPage(FF)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->findSnapEdge(I)Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    sget-object v2, Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;->NONE:Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 29
    .line 30
    if-ne v1, v2, :cond_1

    .line 31
    .line 32
    return-void

    .line 33
    :cond_1
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->snapOffsetForPage(ILcom/intsig/camscanner/pdfengine/utils/SnapEdge;)F

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 38
    .line 39
    if-eqz v1, :cond_2

    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    .line 42
    .line 43
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 44
    .line 45
    neg-float v0, v0

    .line 46
    invoke-virtual {v1, v2, v0}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->startYAnimation(FF)V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    .line 51
    .line 52
    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 53
    .line 54
    neg-float v0, v0

    .line 55
    invoke-virtual {v1, v2, v0}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->startXAnimation(FF)V

    .line 56
    .line 57
    .line 58
    :cond_3
    :goto_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public recycle()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->waitingDocumentConfigurator:Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    .line 5
    .line 6
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->stopAll()V

    .line 7
    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->dragPinchManager:Lcom/intsig/camscanner/pdfengine/DragPinchManager;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/DragPinchManager;->disable()V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandler:Lcom/intsig/camscanner/pdfengine/RenderingHandler;

    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/RenderingHandler;->stop()V

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandler:Lcom/intsig/camscanner/pdfengine/RenderingHandler;

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 25
    .line 26
    .line 27
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->decodingAsyncTask:Lcom/intsig/camscanner/pdfengine/DecodingAsyncTask;

    .line 28
    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 32
    .line 33
    .line 34
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->cacheManager:Lcom/intsig/camscanner/pdfengine/CacheManager;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/CacheManager;->recycle()V

    .line 37
    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 40
    .line 41
    if-eqz v1, :cond_2

    .line 42
    .line 43
    iget-boolean v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->isScrollHandleInit:Z

    .line 44
    .line 45
    if-eqz v3, :cond_2

    .line 46
    .line 47
    invoke-interface {v1}, Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;->destroyLayout()V

    .line 48
    .line 49
    .line 50
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 51
    .line 52
    if-eqz v1, :cond_3

    .line 53
    .line 54
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->dispose()V

    .line 55
    .line 56
    .line 57
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 58
    .line 59
    :cond_3
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->renderingHandler:Lcom/intsig/camscanner/pdfengine/RenderingHandler;

    .line 60
    .line 61
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 62
    .line 63
    const/4 v0, 0x0

    .line 64
    iput-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->isScrollHandleInit:Z

    .line 65
    .line 66
    const/4 v0, 0x0

    .line 67
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 68
    .line 69
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 70
    .line 71
    const/high16 v0, 0x3f800000    # 1.0f

    .line 72
    .line 73
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 74
    .line 75
    iput-boolean v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->recycled:Z

    .line 76
    .line 77
    new-instance v0, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 78
    .line 79
    invoke-direct {v0}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;-><init>()V

    .line 80
    .line 81
    .line 82
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 83
    .line 84
    sget-object v0, Lcom/intsig/camscanner/pdfengine/PDFView$State;->DEFAULT:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 85
    .line 86
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->state:Lcom/intsig/camscanner/pdfengine/PDFView$State;

    .line 87
    .line 88
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method redraw()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public resetZoom()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->minZoom:F

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->zoomTo(F)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public resetZoomWithAnimation()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->minZoom:F

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->zoomWithAnimation(F)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setMaxZoom(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->maxZoom:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setMidZoom(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->midZoom:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setMinZoom(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->minZoom:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setNightMode(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->nightMode:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    new-instance p1, Landroid/graphics/ColorMatrix;

    .line 6
    .line 7
    const/16 v0, 0x14

    .line 8
    .line 9
    new-array v0, v0, [F

    .line 10
    .line 11
    fill-array-data v0, :array_0

    .line 12
    .line 13
    .line 14
    invoke-direct {p1, v0}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 15
    .line 16
    .line 17
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    .line 18
    .line 19
    invoke-direct {v0, p1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->paint:Landroid/graphics/Paint;

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->paint:Landroid/graphics/Paint;

    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 32
    .line 33
    .line 34
    :goto_0
    return-void

    .line 35
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x437f0000    # 255.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setPageFling(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageFling:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPageSnap(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pageSnap:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPositionOffset(F)V
    .locals 1

    const/4 v0, 0x1

    .line 5
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->setPositionOffset(FZ)V

    return-void
.end method

.method public setPositionOffset(FZ)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    if-eqz v0, :cond_0

    .line 2
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    mul-float v1, v1, p1

    invoke-virtual {p0, v0, v1, p2}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FFZ)V

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getDocLen(F)F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    mul-float v0, v0, p1

    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    invoke-virtual {p0, v0, p1, p2}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FFZ)V

    .line 4
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->loadPageByOffset()V

    return-void
.end method

.method public setSwipeEnabled(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->enableSwipe:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method showPage(I)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->recycled:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->determineValidPageNumberFrom(I)I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->loadPages()V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 18
    .line 19
    if-eqz p1, :cond_1

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->documentFitsView()Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    if-nez p1, :cond_1

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 28
    .line 29
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 30
    .line 31
    add-int/lit8 v0, v0, 0x1

    .line 32
    .line 33
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;->setPageNum(I)V

    .line 34
    .line 35
    .line 36
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 37
    .line 38
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentPage:I

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 41
    .line 42
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->callOnPageChange(II)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method snapOffsetForPage(ILcom/intsig/camscanner/pdfengine/utils/SnapEdge;)F
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 4
    .line 5
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageOffset(IF)F

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->swipeVertical:Z

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    :goto_0
    int-to-float v1, v1

    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->pdfFile:Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 24
    .line 25
    iget v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 26
    .line 27
    invoke-virtual {v2, p1, v3}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageLength(IF)F

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    sget-object v2, Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;->CENTER:Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 32
    .line 33
    if-ne p2, v2, :cond_1

    .line 34
    .line 35
    const/high16 p2, 0x40000000    # 2.0f

    .line 36
    .line 37
    div-float/2addr v1, p2

    .line 38
    sub-float/2addr v0, v1

    .line 39
    div-float/2addr p1, p2

    .line 40
    :goto_1
    add-float/2addr v0, p1

    .line 41
    goto :goto_2

    .line 42
    :cond_1
    sget-object v2, Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;->END:Lcom/intsig/camscanner/pdfengine/utils/SnapEdge;

    .line 43
    .line 44
    if-ne p2, v2, :cond_2

    .line 45
    .line 46
    sub-float/2addr v0, v1

    .line 47
    goto :goto_1

    .line 48
    :cond_2
    :goto_2
    return v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public stopFling()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->stopFling()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toCurrentScale(F)F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 2
    .line 3
    mul-float p1, p1, v0

    .line 4
    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public toRealScale(F)F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 2
    .line 3
    div-float/2addr p1, v0

    .line 4
    return p1
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public useBestQuality(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->bestQuality:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public zoomCenteredRelativeTo(FLandroid/graphics/PointF;)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 2
    .line 3
    mul-float v0, v0, p1

    .line 4
    .line 5
    invoke-virtual {p0, v0, p2}, Lcom/intsig/camscanner/pdfengine/PDFView;->zoomCenteredTo(FLandroid/graphics/PointF;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public zoomCenteredTo(FLandroid/graphics/PointF;)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 2
    .line 3
    div-float v0, p1, v0

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->zoomTo(F)V

    .line 6
    .line 7
    .line 8
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentXOffset:F

    .line 9
    .line 10
    mul-float p1, p1, v0

    .line 11
    .line 12
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->currentYOffset:F

    .line 13
    .line 14
    mul-float v1, v1, v0

    .line 15
    .line 16
    iget v2, p2, Landroid/graphics/PointF;->x:F

    .line 17
    .line 18
    mul-float v3, v2, v0

    .line 19
    .line 20
    sub-float/2addr v2, v3

    .line 21
    add-float/2addr p1, v2

    .line 22
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 23
    .line 24
    mul-float v0, v0, p2

    .line 25
    .line 26
    sub-float/2addr p2, v0

    .line 27
    add-float/2addr v1, p2

    .line 28
    invoke-virtual {p0, p1, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->moveTo(FF)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method public zoomTo(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public zoomWithAnimation(F)V
    .locals 4

    .line 2
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v2

    iget v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    invoke-virtual {v0, v1, v3, v2, p1}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->startZoomAnimation(FFFF)V

    return-void
.end method

.method public zoomWithAnimation(FFF)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->animationManager:Lcom/intsig/camscanner/pdfengine/AnimationManager;

    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView;->zoom:F

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/intsig/camscanner/pdfengine/AnimationManager;->startZoomAnimation(FFFF)V

    return-void
.end method

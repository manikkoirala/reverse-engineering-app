.class public Lcom/intsig/camscanner/pdfengine/PDF_Util;
.super Ljava/lang/Object;
.source "PDF_Util.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;,
        Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;,
        Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;
    }
.end annotation


# static fields
.field public static ERROR_ALL_PAGE_MISS:I = -0x3

.field public static ERROR_EMPTY_DOC:I = -0x1

.field public static ERROR_INDEX_OUT_OF_BOUND:I = -0x2

.field public static ERROR_LOAD_ENGINE:I = -0x5

.field public static ERROR_SAVE_PDF_FILE:I = -0x4

.field private static final MAX_FILE_BYTES_LENGTH:I = 0xb4

.field public static NO_WATER_INTCEPTOR:Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor; = null

.field public static final QRCODE_MARK_HEIGHT:I = 0x2d

.field public static SUCCESS_CREATE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "PDF_Util"

.field public static final TEXT_MARK_HEIGHT:I = 0x19


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/PDF_Util$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/pdfengine/PDF_Util$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->NO_WATER_INTCEPTOR:Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static checkPdfSizeIdExist(JLandroid/content/Context;)[I
    .locals 9
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    fill-array-data v1, :array_0

    .line 5
    .line 6
    .line 7
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$PdfSize;->〇080:Landroid/net/Uri;

    .line 8
    .line 9
    invoke-static {v2, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 10
    .line 11
    .line 12
    move-result-object v4

    .line 13
    const-string p0, "pdf_width"

    .line 14
    .line 15
    const-string p1, "pdf_height"

    .line 16
    .line 17
    const-string v2, "_id"

    .line 18
    .line 19
    const-string v3, "name"

    .line 20
    .line 21
    filled-new-array {v2, v3, p0, p1}, [Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v5

    .line 25
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    const/4 v6, 0x0

    .line 30
    const/4 v7, 0x0

    .line 31
    const/4 v8, 0x0

    .line 32
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 33
    .line 34
    .line 35
    move-result-object p0

    .line 36
    if-eqz p0, :cond_1

    .line 37
    .line 38
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    if-eqz p1, :cond_0

    .line 43
    .line 44
    const/4 p1, 0x0

    .line 45
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    aput p2, v1, p1

    .line 50
    .line 51
    const/4 p1, 0x3

    .line 52
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    const/4 p2, 0x1

    .line 57
    aput p1, v1, p2

    .line 58
    .line 59
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 60
    .line 61
    .line 62
    :cond_1
    return-object v1

    .line 63
    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static clearNormalPdf()V
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O08000()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "clearNormalPdf SDStorageManager.getDocDir():"

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "PDF_Util"

    .line 23
    .line 24
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_0

    .line 32
    .line 33
    return-void

    .line 34
    :cond_0
    new-instance v1, Ljava/io/File;

    .line 35
    .line 36
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_3

    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    if-nez v0, :cond_1

    .line 50
    .line 51
    const-string v0, "files == null"

    .line 52
    .line 53
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_1
    array-length v1, v0

    .line 58
    const/4 v2, 0x0

    .line 59
    :goto_0
    if-ge v2, v1, :cond_4

    .line 60
    .line 61
    aget-object v3, v0, v2

    .line 62
    .line 63
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v4

    .line 67
    const-string v5, ".pdf"

    .line 68
    .line 69
    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    if-eqz v4, :cond_2

    .line 74
    .line 75
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 76
    .line 77
    .line 78
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_3
    const-string v0, "csFile not exists"

    .line 82
    .line 83
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    :cond_4
    :goto_1
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static clearNormalPdfInThread()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdfengine/〇o00〇〇Oo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/pdfengine/〇o00〇〇Oo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static clearPdfForHuaWeiPay()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/pdfengine/〇o〇;

    .line 6
    .line 7
    invoke-direct {v1}, Lcom/intsig/camscanner/pdfengine/〇o〇;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static computeLayout(Ljava/lang/String;ZFFIZF)[F
    .locals 17

    .line 1
    move/from16 v0, p4

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    move/from16 v3, p2

    .line 7
    .line 8
    move/from16 v2, p3

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move/from16 v2, p2

    .line 12
    .line 13
    move/from16 v3, p3

    .line 14
    .line 15
    :goto_0
    if-eqz p5, :cond_1

    .line 16
    .line 17
    const/high16 v4, 0x41a00000    # 20.0f

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    const/4 v4, 0x0

    .line 21
    :goto_1
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    .line 22
    .line 23
    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 24
    .line 25
    .line 26
    const/4 v13, 0x1

    .line 27
    iput-boolean v13, v5, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 28
    .line 29
    move-object/from16 v6, p0

    .line 30
    .line 31
    invoke-static {v6, v5}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 32
    .line 33
    .line 34
    if-eqz p1, :cond_2

    .line 35
    .line 36
    iget v2, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 37
    .line 38
    int-to-float v2, v2

    .line 39
    iget v3, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 40
    .line 41
    int-to-float v3, v3

    .line 42
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 43
    .line 44
    .line 45
    move-result v6

    .line 46
    rsub-int v14, v6, 0x168

    .line 47
    .line 48
    const/16 v6, 0x5a

    .line 49
    .line 50
    if-eq v14, v6, :cond_4

    .line 51
    .line 52
    const/16 v6, 0x10e

    .line 53
    .line 54
    if-ne v14, v6, :cond_3

    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_3
    iget v6, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 58
    .line 59
    int-to-float v6, v6

    .line 60
    iget v5, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 61
    .line 62
    goto :goto_3

    .line 63
    :cond_4
    :goto_2
    iget v6, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 64
    .line 65
    int-to-float v6, v6

    .line 66
    iget v5, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 67
    .line 68
    :goto_3
    int-to-float v5, v5

    .line 69
    move v9, v5

    .line 70
    move v8, v6

    .line 71
    if-eqz v0, :cond_5

    .line 72
    .line 73
    if-eqz p1, :cond_7

    .line 74
    .line 75
    :cond_5
    cmpl-float v0, v2, v3

    .line 76
    .line 77
    if-lez v0, :cond_6

    .line 78
    .line 79
    cmpl-float v0, v9, v8

    .line 80
    .line 81
    if-lez v0, :cond_7

    .line 82
    .line 83
    goto :goto_4

    .line 84
    :cond_6
    cmpl-float v0, v8, v9

    .line 85
    .line 86
    if-lez v0, :cond_7

    .line 87
    .line 88
    :goto_4
    move/from16 v16, v3

    .line 89
    .line 90
    move v3, v2

    .line 91
    move/from16 v2, v16

    .line 92
    .line 93
    :cond_7
    const/4 v0, 0x5

    .line 94
    new-array v15, v0, [F

    .line 95
    .line 96
    const/4 v12, 0x1

    .line 97
    move v5, v2

    .line 98
    move v6, v3

    .line 99
    move v7, v4

    .line 100
    move/from16 v10, p6

    .line 101
    .line 102
    move-object v11, v15

    .line 103
    invoke-static/range {v5 .. v12}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->parapdfImageInfo(FFFFFF[FZ)V

    .line 104
    .line 105
    .line 106
    const/4 v5, 0x0

    .line 107
    aget v6, v15, v5

    .line 108
    .line 109
    aget v7, v15, v13

    .line 110
    .line 111
    aget v8, v15, v1

    .line 112
    .line 113
    const/4 v9, 0x3

    .line 114
    aget v10, v15, v9

    .line 115
    .line 116
    const/4 v11, 0x4

    .line 117
    aget v12, v15, v11

    .line 118
    .line 119
    const/16 v15, 0xa

    .line 120
    .line 121
    new-array v15, v15, [F

    .line 122
    .line 123
    aput v2, v15, v5

    .line 124
    .line 125
    aput v3, v15, v13

    .line 126
    .line 127
    aput v8, v15, v1

    .line 128
    .line 129
    aput v10, v15, v9

    .line 130
    .line 131
    aput v6, v15, v11

    .line 132
    .line 133
    aput v7, v15, v0

    .line 134
    .line 135
    const/4 v0, 0x6

    .line 136
    aput v4, v15, v0

    .line 137
    .line 138
    const/4 v0, 0x7

    .line 139
    aput v4, v15, v0

    .line 140
    .line 141
    const/16 v0, 0x8

    .line 142
    .line 143
    aput v12, v15, v0

    .line 144
    .line 145
    const/16 v0, 0x9

    .line 146
    .line 147
    int-to-float v1, v14

    .line 148
    aput v1, v15, v0

    .line 149
    .line 150
    return-object v15
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method public static createDocPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "/"

    invoke-virtual {p3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 4
    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_2

    .line 5
    :cond_1
    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    const/16 v1, 0xb4

    if-le v0, v1, :cond_4

    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 7
    :goto_0
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_3

    add-int/lit8 v4, v2, 0x1

    .line 8
    invoke-virtual {p4, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 9
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    array-length v5, v5

    add-int/2addr v3, v5

    if-le v3, v1, :cond_2

    goto :goto_1

    .line 10
    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v4

    goto :goto_0

    .line 11
    :cond_3
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    move-object v0, p4

    .line 12
    :goto_2
    invoke-static {p0, p4}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->isNeedAddPrefix(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    const-string p4, ""

    const-string v1, "_"

    if-eqz p0, :cond_6

    if-ltz p5, :cond_5

    .line 13
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 14
    :cond_5
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->Ooo8〇〇(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/intsig/utils/FileNameUtils;->〇o〇(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_3

    .line 15
    :cond_6
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->Ooo8〇〇(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 16
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    if-ltz p5, :cond_7

    .line 17
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 18
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_3

    .line 19
    :cond_8
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/intsig/utils/FileNameUtils;->〇o〇(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 20
    :goto_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "creat doc path = "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "PDF_Util"

    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public static createDocPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v5, -0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    .line 1
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createDocPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createPdf(JLjava/lang/String;[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 50
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CsMethodParameterSizeDetector"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "[I",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;",
            "Z",
            "Lcom/intsig/utils/ImageScaleListener;",
            "Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;",
            "Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    move-wide/from16 v7, p0

    move-object/from16 v0, p3

    move-object/from16 v9, p4

    move/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p9

    move-object/from16 v13, p11

    .line 7
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v14, "PDF_Util"

    if-nez v1, :cond_0

    if-nez v10, :cond_0

    .line 8
    new-instance v1, Ljava/io/File;

    move-object/from16 v2, p5

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 9
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "delete old file"

    .line 10
    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v15, "title"

    const-string v16, "pages"

    const-string v17, "page_size"

    const-string v18, "page_orientation"

    const-string v19, "page_margin"

    const-string v20, "password_pdf"

    const-string v21, "modified"

    const-string v22, "team_token"

    const-string v23, "PDF_PAGE_NUM_LOCATION"

    const-string v24, "type"

    .line 11
    filled-new-array/range {v15 .. v24}, [Ljava/lang/String;

    move-result-object v3

    .line 12
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    invoke-static {v2, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v15, 0x6

    const-wide/16 v16, 0x2

    const/4 v6, 0x4

    const-string v18, ""

    const/4 v5, 0x5

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_4

    .line 13
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 14
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    goto :goto_0

    :cond_1
    move-object/from16 v20, p2

    .line 15
    :goto_0
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 16
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 17
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 18
    invoke-interface {v1, v15}, Landroid/database/Cursor;->getLong(I)J

    const/4 v6, 0x7

    .line 19
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 20
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 21
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    int-to-long v5, v6

    const/4 v15, 0x3

    .line 22
    invoke-interface {v1, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    goto :goto_1

    :cond_2
    const/4 v15, 0x3

    .line 23
    invoke-static {v9, v6}, Lcom/intsig/camscanner/app/DBUtil;->ooo0〇O88O(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v27

    .line 24
    invoke-static {v9, v6}, Lcom/intsig/camscanner/app/DBUtil;->Oo〇o(Landroid/content/Context;Ljava/lang/String;)I

    move-result v19

    move-wide/from16 v5, v27

    :goto_1
    const/16 v2, 0x8

    .line 25
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/16 v15, 0x9

    .line 26
    invoke-interface {v1, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    invoke-static {v15}, Lcom/intsig/camscanner/certificate_package/util/CertificateDBUtil;->OO0o〇〇〇〇0(I)Z

    move-result v15

    goto :goto_2

    :cond_3
    move-wide/from16 v5, v16

    move-object/from16 v20, v18

    const/4 v2, 0x0

    const/4 v15, 0x0

    const/16 v19, 0x1

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    .line 27
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object/from16 v1, v20

    move/from16 v3, v21

    move-object/from16 p2, v23

    move/from16 v49, v15

    move v15, v2

    move/from16 v2, v19

    move/from16 v19, v49

    goto :goto_3

    :cond_4
    const-string v1, "doc == null"

    .line 28
    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    move-wide/from16 v5, v16

    move-object/from16 v1, v18

    const/16 p2, 0x0

    const/4 v2, 0x1

    const/4 v15, 0x0

    const/16 v19, 0x0

    const/16 v22, 0x0

    :goto_3
    if-nez v22, :cond_5

    const/16 v21, 0x1

    goto :goto_4

    :cond_5
    const/16 v21, 0x0

    :goto_4
    if-nez v0, :cond_9

    if-gtz v3, :cond_7

    if-eqz v11, :cond_6

    .line 29
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_EMPTY_DOC:I

    const/4 v1, 0x0

    invoke-interface {v11, v0, v1}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    :cond_6
    const-string v0, "pages == null , page_num <= 0"

    .line 30
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v18

    .line 31
    :cond_7
    new-array v0, v3, [I

    const/4 v4, 0x0

    :goto_5
    if-ge v4, v3, :cond_8

    .line 32
    aput v4, v0, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_8
    move/from16 v23, v2

    move-wide/from16 v29, v5

    const/4 v3, 0x0

    const/4 v5, 0x1

    goto :goto_6

    .line 33
    :cond_9
    array-length v4, v0

    move/from16 v23, v2

    const-string v2, "page out of bound"

    if-le v4, v3, :cond_b

    if-eqz v11, :cond_a

    .line 34
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_INDEX_OUT_OF_BOUND:I

    const/4 v4, 0x0

    invoke-interface {v11, v0, v4}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    :cond_a
    const-string v0, "pages.length > page_num"

    .line 35
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 36
    :cond_b
    array-length v4, v0

    move-wide/from16 v29, v5

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    aget v4, v0, v4

    if-ge v4, v3, :cond_63

    const/4 v3, 0x0

    aget v4, v0, v3

    if-gez v4, :cond_c

    goto/16 :goto_40

    :cond_c
    :goto_6
    move-object v6, v0

    const/4 v4, -0x1

    if-eqz p8, :cond_d

    .line 37
    array-length v0, v6

    if-ne v0, v5, :cond_d

    .line 38
    aget v0, v6, v3

    add-int/2addr v0, v5

    goto :goto_7

    :cond_d
    const/4 v0, -0x1

    :goto_7
    if-eqz v11, :cond_e

    .line 39
    array-length v2, v6

    invoke-interface {v11, v2}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onStart(I)V

    :cond_e
    if-nez v10, :cond_10

    .line 40
    invoke-static {v9, v7, v8, v1, v0}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdfPath(Landroid/content/Context;JLjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v22, v1

    move-object/from16 v38, v6

    move/from16 v25, v15

    move/from16 v36, v23

    move-wide/from16 v12, v29

    const/16 p3, 0x0

    const/4 v15, 0x3

    :cond_f
    :goto_8
    move-object v1, v0

    goto/16 :goto_9

    :cond_10
    const/4 v2, 0x5

    if-ne v10, v2, :cond_11

    .line 41
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getOutputFolder()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v22, v1

    move-object/from16 v1, p4

    move/from16 v25, v15

    move/from16 v36, v23

    const/16 p3, 0x0

    const/4 v15, 0x0

    const/16 v23, 0x5

    move-wide/from16 v2, p0

    const/4 v5, -0x1

    const/4 v15, 0x3

    move-object/from16 v4, v20

    move-wide/from16 v12, v29

    move-object/from16 v5, v22

    move-object/from16 v38, v6

    move v6, v0

    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdfPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-static {}, Lcom/intsig/utils/SDStorageUtil;->〇80〇808〇O()Z

    move-result v1

    if-nez v1, :cond_f

    .line 43
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "delete old output file"

    .line 45
    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    :cond_11
    move-object/from16 v22, v1

    move-object/from16 v38, v6

    move/from16 v25, v15

    move/from16 v36, v23

    move-wide/from16 v12, v29

    const/16 p3, 0x0

    const/4 v15, 0x3

    if-ne v10, v15, :cond_13

    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 47
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_12

    .line 49
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 50
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mkdirs = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    move-object/from16 v1, p4

    move-wide/from16 v2, p0

    move-object/from16 v5, v22

    move v6, v0

    .line 51
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdfPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    .line 52
    :cond_13
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p4

    move-wide/from16 v2, p0

    move-object/from16 v5, v22

    move v6, v0

    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdfPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    .line 53
    :goto_9
    invoke-static {}, Lcom/intsig/pdfengine/PDF_Engine;->getInstance()Lcom/intsig/pdfengine/PDF_Engine;

    move-result-object v2

    if-nez v2, :cond_15

    if-eqz v11, :cond_14

    .line 54
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_LOAD_ENGINE:I

    const/4 v1, 0x0

    invoke-interface {v11, v0, v1}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    :cond_14
    const-string v0, "engine == null"

    .line 55
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v18

    :cond_15
    const/4 v3, 0x2

    .line 56
    invoke-virtual {v2, v3}, Lcom/intsig/pdfengine/PDF_Engine;->setLogLevel(I)V

    const/4 v4, 0x0

    .line 57
    invoke-virtual {v2, v1, v4, v4}, Lcom/intsig/pdfengine/PDF_Engine;->createCanvas(Ljava/lang/String;FF)I

    if-eqz v21, :cond_16

    const/4 v6, 0x0

    goto :goto_a

    :cond_16
    const/high16 v6, 0x41a00000    # 20.0f

    .line 58
    :goto_a
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v0

    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->us_share_watermark_free:I

    const/4 v5, 0x1

    if-ne v0, v5, :cond_19

    .line 59
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o0O0O8()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 60
    invoke-static/range {p13 .. p13}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 61
    invoke-virtual/range {p13 .. p13}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, v5

    move-object/from16 v15, p13

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v23

    cmp-long v0, v23, v7

    if-nez v0, :cond_18

    .line 62
    invoke-static/range {p3 .. p3}, Lcom/intsig/camscanner/util/PreferenceHelper;->Ooo0〇(Z)V

    goto :goto_b

    .line 63
    :cond_17
    invoke-static/range {p3 .. p3}, Lcom/intsig/camscanner/util/PreferenceHelper;->Ooo0〇(Z)V

    :cond_18
    :goto_b
    const/4 v0, 0x1

    goto :goto_c

    :cond_19
    const/4 v0, 0x0

    .line 64
    :goto_c
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->isPayVersion()Z

    move-result v15

    if-nez v15, :cond_1c

    if-eq v10, v5, :cond_1c

    if-eqz p10, :cond_1a

    .line 65
    invoke-interface/range {p10 .. p10}, Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;->intecept()Z

    move-result v15

    if-nez v15, :cond_1c

    :cond_1a
    if-eqz v0, :cond_1b

    goto :goto_d

    :cond_1b
    const/high16 v0, 0x41c80000    # 25.0f

    const/high16 v15, 0x42340000    # 45.0f

    const/high16 v15, 0x41c80000    # 25.0f

    const/high16 v23, 0x42340000    # 45.0f

    goto :goto_e

    :cond_1c
    :goto_d
    const/4 v15, 0x0

    const/16 v23, 0x0

    :goto_e
    cmpl-float v0, v23, v4

    if-lez v0, :cond_27

    .line 66
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->〇〇888()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 67
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->Oo08()Lkotlin/Pair;

    move-result-object v0

    .line 68
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 69
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v27

    if-nez v27, :cond_1d

    .line 71
    :try_start_0
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 72
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object/from16 p13, v3

    const/16 v3, 0x64

    :try_start_1
    invoke-static {v5, v0, v4, v3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o8(Ljava/io/File;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_10

    :catch_0
    move-exception v0

    goto :goto_f

    :catch_1
    move-exception v0

    move-object/from16 p13, v3

    .line 73
    :goto_f
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_10

    :cond_1d
    move-object/from16 p13, v3

    .line 74
    :goto_10
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_27

    :goto_11
    move-object/from16 v3, p13

    goto/16 :goto_15

    .line 75
    :cond_1e
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇80〇808〇O()Z

    move-result v0

    if-nez v0, :cond_24

    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->OO0o〇〇〇〇0()Z

    move-result v0

    if-eqz v0, :cond_1f

    goto/16 :goto_16

    .line 76
    :cond_1f
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isOnlyIndonesiaWatermarkAddLogo()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 77
    invoke-static {}, Lcom/intsig/camscanner/share/StreamFreeShareManager;->〇080()Lkotlin/Pair;

    move-result-object v0

    .line 78
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 79
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_20

    .line 81
    :try_start_2
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v5, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 82
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-object/from16 p13, v3

    const/16 v3, 0x64

    :try_start_3
    invoke-static {v4, v0, v5, v3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o8(Ljava/io/File;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_13

    :catch_2
    move-exception v0

    goto :goto_12

    :catch_3
    move-exception v0

    move-object/from16 p13, v3

    .line 83
    :goto_12
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_13

    :cond_20
    move-object/from16 p13, v3

    .line 84
    :goto_13
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_27

    goto :goto_11

    .line 85
    :cond_21
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    move-result v0

    if-nez v0, :cond_22

    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->〇〇888()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 86
    :cond_22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getPicAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 87
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_23

    .line 89
    :try_start_4
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getPicAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/intsig/utils/FileUtil;->〇〇888(Ljava/io/InputStream;Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_14

    :catch_4
    move-exception v0

    .line 90
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    :cond_23
    :goto_14
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_27

    :goto_15
    move/from16 v15, v23

    goto :goto_1a

    .line 92
    :cond_24
    :goto_16
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->OO0o〇〇〇〇0()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 93
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->oO80()Lkotlin/Pair;

    move-result-object v0

    goto :goto_17

    .line 94
    :cond_25
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇080()Lkotlin/Pair;

    move-result-object v0

    .line 95
    :goto_17
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 96
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_26

    .line 98
    :try_start_5
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v5, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    move-object/from16 p13, v3

    const/16 v3, 0x64

    :try_start_6
    invoke-static {v4, v0, v5, v3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o8(Ljava/io/File;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_19

    :catch_5
    move-exception v0

    goto :goto_18

    :catch_6
    move-exception v0

    move-object/from16 p13, v3

    .line 100
    :goto_18
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_19

    :cond_26
    move-object/from16 p13, v3

    .line 101
    :goto_19
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_27

    goto/16 :goto_11

    :cond_27
    const/4 v3, 0x0

    :goto_1a
    const/4 v4, 0x2

    new-array v0, v4, [I

    const-wide/16 v4, 0x0

    cmp-long v23, v12, v4

    if-eqz v23, :cond_29

    .line 102
    invoke-static {v12, v13, v9}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->checkPdfSizeIdExist(JLandroid/content/Context;)[I

    move-result-object v0

    const/4 v4, 0x0

    .line 103
    aget v5, v0, v4

    const/4 v4, -0x1

    move/from16 p10, v15

    if-eq v5, v4, :cond_2a

    const/4 v5, 0x1

    aget v15, v0, v5

    if-ne v15, v4, :cond_28

    goto :goto_1b

    :cond_28
    move-object v4, v0

    const/4 v5, 0x0

    goto :goto_1c

    :cond_29
    move/from16 p10, v15

    :cond_2a
    :goto_1b
    move-object v4, v0

    const/4 v5, 0x1

    :goto_1c
    cmp-long v0, v12, v16

    if-nez v0, :cond_2b

    const/4 v12, 0x1

    goto :goto_1d

    :cond_2b
    const/4 v12, 0x0

    .line 104
    :goto_1d
    new-instance v13, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v13}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const-string v27, "_data"

    const-string v28, "raw_data"

    const-string v29, "status"

    const-string v30, "note"

    const-string v31, "ocr_border"

    const-string v32, "enhance_mode"

    const-string v33, "_id"

    .line 105
    filled-new-array/range {v27 .. v33}, [Ljava/lang/String;

    move-result-object v41

    .line 106
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v39

    invoke-static/range {p0 .. p1}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    move-result-object v40

    const/16 v42, 0x0

    const/16 v43, 0x0

    const-string v44, "page_num ASC"

    invoke-virtual/range {v39 .. v44}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    if-nez v15, :cond_2c

    const-string v0, "c == null"

    .line 107
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v18

    .line 108
    :cond_2c
    invoke-static/range {p12 .. p12}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->toMap(Ljava/util/ArrayList;)Ljava/util/Map;

    move-result-object v7

    move-object/from16 v16, v1

    const/4 v8, 0x5

    new-array v1, v8, [F

    move/from16 v18, p10

    move-object/from16 v11, v38

    const/4 v8, 0x0

    const/16 v17, 0x1

    .line 109
    :goto_1e
    array-length v0, v11

    if-ge v8, v0, :cond_55

    .line 110
    aget v0, v11, v8

    invoke-interface {v15, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_53

    move-object/from16 v38, v11

    const/4 v11, 0x2

    .line 111
    invoke-interface {v15, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2d

    const/4 v11, 0x0

    .line 112
    invoke-interface {v15, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1f

    :cond_2d
    const/4 v11, 0x1

    .line 113
    invoke-interface {v15, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1f
    const/4 v11, 0x6

    .line 114
    invoke-interface {v15, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    if-eqz v7, :cond_2e

    .line 115
    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v7, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;

    if-eqz v11, :cond_2e

    .line 116
    invoke-virtual {v11}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇o〇()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_2e

    .line 117
    invoke-virtual {v11}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇o〇()Ljava/lang/String;

    move-result-object v0

    :cond_2e
    move-object/from16 v11, p11

    if-eqz v11, :cond_2f

    .line 118
    invoke-interface {v11, v0, v8}, Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;->addBgWatermark(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 119
    :cond_2f
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->〇〇o8(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_30

    move-object/from16 p10, v7

    .line 120
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CreatePdf  miss image:"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v7, p7

    move-object/from16 v39, v1

    move/from16 v42, v6

    move-object v1, v9

    move/from16 v40, v12

    move-object/from16 v41, v13

    move/from16 v10, v25

    move/from16 v34, v36

    const/high16 v6, 0x41a00000    # 20.0f

    const/4 v9, 0x1

    const/4 v11, 0x2

    move/from16 v36, v5

    move v12, v8

    move-object v5, v15

    move-object/from16 v8, v38

    goto/16 :goto_3a

    :cond_30
    move-object/from16 p10, v7

    move-object/from16 v7, p9

    if-eqz v7, :cond_32

    .line 121
    invoke-interface/range {p9 .. p9}, Lcom/intsig/utils/ImageScaleListener;->〇o00〇〇Oo()Z

    move-result v11

    if-eqz v11, :cond_31

    goto :goto_20

    .line 122
    :cond_31
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move/from16 v26, v8

    .line 123
    new-instance v8, Ljava/io/File;

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v9, v11}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v0, v8}, Lcom/intsig/utils/ImageScaleListener;->〇o〇(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_21

    :cond_32
    :goto_20
    move/from16 v26, v8

    :goto_21
    move-object v8, v0

    const/4 v9, 0x1

    if-ne v10, v9, :cond_34

    const/4 v9, 0x5

    .line 125
    invoke-interface {v15, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v9, 0xc

    if-ne v9, v0, :cond_33

    const-string v0, "no need to procress"

    .line 126
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_22

    .line 127
    :cond_33
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 128
    new-instance v9, Ljava/io/File;

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v9, v11, v7}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :try_start_7
    invoke-static {v0, v9}, Lcom/intsig/utils/FileUtil;->Oo08(Ljava/io/File;Ljava/io/File;)V

    .line 130
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const/16 v0, 0x10

    .line 131
    invoke-static {v8, v0}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->doEnhance(Ljava/lang/String;I)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_22

    :catch_7
    move-exception v0

    const-string v7, "crate pax pdf , copyFile"

    .line 132
    invoke-static {v14, v7, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 133
    :cond_34
    :goto_22
    invoke-static {v8}, Lcom/intsig/utils/FileUtil;->O8〇o(Ljava/lang/String;)Z

    move-result v0

    const/high16 v7, 0x3f800000    # 1.0f

    if-eqz v0, :cond_35

    const-string v0, "image file is png format convertToJpgPath"

    .line 134
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    move-result-object v0

    const-string v9, ".jpg"

    invoke-static {v0, v9}, Lcom/intsig/camscanner/util/SDStorageManager;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v9, 0x50

    const/4 v11, 0x0

    .line 136
    invoke-static {v8, v11, v7, v9, v0}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    move-object v8, v0

    goto :goto_23

    :cond_35
    const/4 v11, 0x0

    :goto_23
    const/4 v9, 0x1

    .line 137
    iput-boolean v9, v13, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 138
    invoke-static {v8, v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    if-eqz v5, :cond_36

    .line 139
    iget v0, v13, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    aput v0, v4, v11

    .line 140
    iget v0, v13, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    aput v0, v4, v9

    :cond_36
    move/from16 v7, v36

    const/4 v11, 0x2

    if-ne v7, v11, :cond_37

    .line 141
    aget v0, v4, v9

    int-to-float v0, v0

    const/4 v11, 0x0

    .line 142
    aget v9, v4, v11

    int-to-float v9, v9

    goto :goto_24

    :cond_37
    const/4 v11, 0x0

    .line 143
    aget v0, v4, v11

    int-to-float v0, v0

    const/4 v9, 0x1

    .line 144
    aget v11, v4, v9

    int-to-float v9, v11

    .line 145
    :goto_24
    invoke-static {v8}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    move-result v11

    rsub-int v11, v11, 0x168

    move-object/from16 p13, v15

    const/16 v15, 0x5a

    if-eq v11, v15, :cond_39

    const/16 v15, 0x10e

    if-ne v11, v15, :cond_38

    goto :goto_25

    .line 146
    :cond_38
    iget v15, v13, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v15, v15

    move/from16 v17, v15

    .line 147
    iget v15, v13, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    goto :goto_26

    .line 148
    :cond_39
    :goto_25
    iget v15, v13, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v15, v15

    move/from16 v17, v15

    .line 149
    iget v15, v13, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    :goto_26
    int-to-float v15, v15

    move/from16 v31, v15

    move/from16 v30, v17

    if-eqz v7, :cond_3a

    if-eqz v5, :cond_3c

    :cond_3a
    cmpl-float v15, v0, v9

    if-lez v15, :cond_3b

    cmpl-float v15, v31, v30

    if-lez v15, :cond_3c

    goto :goto_27

    :cond_3b
    cmpl-float v15, v30, v31

    if-lez v15, :cond_3c

    :goto_27
    move/from16 v49, v9

    move v9, v0

    move/from16 v0, v49

    .line 150
    :cond_3c
    invoke-virtual {v2, v0, v9}, Lcom/intsig/pdfengine/PDF_Engine;->newPage(FF)V

    const/high16 v15, -0x1000000

    .line 151
    invoke-virtual {v2, v15}, Lcom/intsig/pdfengine/PDF_Engine;->setStrokeColor(I)V

    const/16 v17, 0x0

    .line 152
    aget v15, v4, v17

    int-to-float v15, v15

    move/from16 v36, v5

    const/16 v17, 0x1

    aget v5, v4, v17

    int-to-float v5, v5

    invoke-static {v15, v5}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getFontSize(FF)F

    move-result v5

    const-string v15, "freescan"

    invoke-virtual {v2, v15, v5}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    if-eqz v3, :cond_3d

    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_28

    .line 153
    :cond_3d
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->watermarkEnlargeFactor()F

    move-result v5

    mul-float v18, v18, v5

    :goto_28
    const/16 v34, 0x0

    move/from16 v27, v0

    move/from16 v28, v9

    move/from16 v29, v6

    move/from16 v32, v18

    move-object/from16 v33, v1

    .line 154
    invoke-static/range {v27 .. v34}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->parapdfImageInfo(FFFFFF[FZ)V

    const/16 v17, 0x0

    cmpl-float v24, v6, v17

    if-nez v24, :cond_3e

    cmpl-float v18, v18, v17

    if-nez v18, :cond_3e

    if-eqz v12, :cond_3e

    if-eqz v19, :cond_3e

    const/16 v17, 0x0

    aget v18, v1, v17

    const v17, 0x408fae14    # 4.49f

    add-float v18, v18, v17

    const/16 v17, 0x1

    aget v27, v1, v17

    const v17, 0x40cb3333    # 6.35f

    add-float v27, v27, v17

    const/16 v17, 0x2

    aget v28, v1, v17

    const v17, 0x410fae14    # 8.98f

    sub-float v28, v28, v17

    const/16 v17, 0x3

    aget v20, v1, v17

    const v29, 0x414b3333    # 12.7f

    sub-float v20, v20, v29

    move/from16 v34, v7

    move/from16 v45, v18

    move/from16 v48, v20

    move/from16 v46, v27

    move/from16 v47, v28

    goto :goto_29

    :cond_3e
    const/16 v17, 0x3

    move/from16 v34, v7

    const/4 v7, 0x0

    aget v18, v1, v7

    const/4 v7, 0x1

    aget v27, v1, v7

    const/16 v20, 0x2

    aget v28, v1, v20

    aget v29, v1, v17

    move/from16 v45, v18

    move/from16 v46, v27

    move/from16 v47, v28

    move/from16 v48, v29

    :goto_29
    const/4 v7, 0x4

    aget v18, v1, v7

    const/high16 v17, 0x40000000    # 2.0f

    const/high16 v37, 0x41200000    # 10.0f

    const/4 v7, 0x1

    if-eq v10, v7, :cond_4b

    const/4 v7, 0x0

    cmpl-float v24, v18, v7

    if-lez v24, :cond_4b

    const/4 v7, 0x0

    .line 155
    invoke-virtual {v2, v7}, Lcom/intsig/pdfengine/PDF_Engine;->setTextRenderingMode(I)V

    .line 156
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0〇o8o〇〇()Z

    move-result v7

    if-eqz v7, :cond_3f

    const v7, 0x7f130588

    move-object/from16 v39, v1

    move-object/from16 v1, p4

    goto :goto_2a

    :cond_3f
    move-object/from16 v39, v1

    move-object/from16 v1, p4

    const v7, 0x7f13066a

    :goto_2a
    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move/from16 v40, v12

    .line 157
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    if-nez v3, :cond_40

    .line 158
    invoke-virtual {v12, v5}, Landroid/graphics/Paint;->setTextScaleX(F)V

    :cond_40
    move-object/from16 v41, v13

    .line 159
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "pdfWaterMark="

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "  watermarkFactor = "

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v14, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->〇〇888()Z

    move-result v10

    if-eqz v10, :cond_41

    invoke-virtual {v12, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    const/high16 v12, 0x41700000    # 15.0f

    goto :goto_2b

    :cond_41
    invoke-virtual {v12, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v10

    const/high16 v12, 0x41f00000    # 30.0f

    :goto_2b
    add-float/2addr v10, v12

    .line 161
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "stringWidth="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v14, v12}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v21, :cond_42

    const/high16 v12, 0x41200000    # 10.0f

    goto :goto_2c

    :cond_42
    sub-float v12, v6, v37

    :goto_2c
    sub-float v10, v0, v10

    .line 162
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇o〇()Ljava/lang/String;

    move-result-object v13

    .line 163
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-eqz v27, :cond_43

    const/16 v27, 0x0

    .line 164
    aget v10, v4, v27

    int-to-float v10, v10

    move/from16 v42, v6

    const/16 v27, 0x1

    aget v6, v4, v27

    int-to-float v6, v6

    invoke-static {v10, v6}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getFontSize(FF)F

    move-result v6

    const-string v10, "Times-Roman"

    invoke-virtual {v2, v10, v6}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    .line 165
    invoke-static {v1, v7}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getBitmapFromString(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-static {v6}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getJPGFromBitmap(Landroid/graphics/Bitmap;)[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4c

    .line 166
    aget-object v7, v6, v27

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    int-to-float v7, v7

    mul-float v7, v7, v5

    const/4 v10, 0x2

    .line 167
    aget-object v27, v6, v10

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    int-to-float v10, v10

    mul-float v5, v5, v10

    sub-float v10, v0, v7

    const/16 v27, 0x0

    .line 168
    aget-object v28, v6, v27

    const/high16 v6, 0x41800000    # 16.0f

    sub-float v29, v10, v6

    const/16 v33, 0x0

    move-object/from16 v27, v2

    move/from16 v30, v12

    move/from16 v31, v7

    move/from16 v32, v5

    invoke-virtual/range {v27 .. v33}, Lcom/intsig/pdfengine/PDF_Engine;->drawImage(Ljava/lang/String;FFFFF)V

    const/high16 v6, 0x41f00000    # 30.0f

    sub-float v29, v10, v6

    add-float v30, v12, v5

    const/16 v33, 0x0

    move-object/from16 v28, v13

    .line 169
    invoke-virtual/range {v27 .. v33}, Lcom/intsig/pdfengine/PDF_Engine;->addLinkRegion(Ljava/lang/String;FFFFI)V

    goto/16 :goto_33

    :cond_43
    move/from16 v42, v6

    .line 170
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇80〇808〇O()Z

    move-result v5

    if-nez v5, :cond_49

    .line 171
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v5

    invoke-virtual {v5}, Lcom/intsig/tsapp/sync/AppConfigJson;->isOnlyIndonesiaWatermarkAddLogo()Z

    move-result v5

    if-nez v5, :cond_49

    .line 172
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->OO0o〇〇〇〇0()Z

    move-result v5

    if-eqz v5, :cond_44

    goto/16 :goto_31

    .line 173
    :cond_44
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0〇o8o〇〇()Z

    move-result v5

    if-eqz v5, :cond_45

    const v5, -0xe1d6c7

    .line 174
    invoke-virtual {v2, v5}, Lcom/intsig/pdfengine/PDF_Engine;->setStrokeColor(I)V

    const/16 v5, 0x17

    goto :goto_2d

    :cond_45
    const/high16 v5, -0x1000000

    .line 175
    invoke-virtual {v2, v5}, Lcom/intsig/pdfengine/PDF_Engine;->setStrokeColor(I)V

    const/16 v5, 0xf

    .line 176
    :goto_2d
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->〇〇888()Z

    move-result v6

    if-eqz v6, :cond_46

    const-string v6, "Times-Roman"

    const/high16 v12, 0x41500000    # 13.0f

    .line 177
    invoke-virtual {v2, v6, v12}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    goto :goto_2e

    :cond_46
    const/high16 v6, 0x41500000    # 13.0f

    .line 178
    invoke-virtual {v2, v15, v6}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    :goto_2e
    int-to-float v5, v5

    sub-float v6, v18, v5

    div-float v12, v6, v17

    const/high16 v27, 0x40800000    # 4.0f

    add-float v30, v12, v27

    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v27, v2

    move-object/from16 v28, v7

    move/from16 v29, v10

    .line 179
    invoke-virtual/range {v27 .. v32}, Lcom/intsig/pdfengine/PDF_Engine;->drawText(Ljava/lang/String;FFFI)V

    .line 180
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0〇〇00()I

    move-result v7

    const/4 v12, 0x1

    if-gt v7, v12, :cond_48

    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->〇〇888()Z

    move-result v7

    if-nez v7, :cond_47

    goto :goto_2f

    :cond_47
    move/from16 v29, v10

    goto :goto_30

    :cond_48
    :goto_2f
    sub-float v10, v10, v18

    add-float/2addr v10, v5

    const/high16 v5, 0x40400000    # 3.0f

    sub-float/2addr v10, v5

    const/high16 v30, 0x41100000    # 9.0f

    const/16 v33, 0x0

    move-object/from16 v27, v2

    move-object/from16 v28, v3

    move/from16 v29, v10

    move/from16 v31, v6

    move/from16 v32, v6

    .line 181
    invoke-virtual/range {v27 .. v33}, Lcom/intsig/pdfengine/PDF_Engine;->drawImage(Ljava/lang/String;FFFFF)V

    :goto_30
    sub-float v31, v0, v29

    const/16 v33, 0x0

    move-object/from16 v27, v2

    move-object/from16 v28, v13

    move/from16 v30, v18

    move/from16 v32, v18

    .line 182
    invoke-virtual/range {v27 .. v33}, Lcom/intsig/pdfengine/PDF_Engine;->addLinkRegion(Ljava/lang/String;FFFFI)V

    goto :goto_33

    .line 183
    :cond_49
    :goto_31
    invoke-static {v3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇oo〇(Ljava/lang/String;)[I

    move-result-object v5

    const/16 v6, 0x14

    int-to-float v6, v6

    sub-float v6, v18, v6

    const/4 v7, 0x0

    .line 184
    aget v10, v5, v7

    int-to-float v7, v10

    mul-float v7, v7, v6

    const/4 v10, 0x1

    aget v5, v5, v10

    int-to-float v5, v5

    div-float v5, v7, v5

    .line 185
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇o00〇〇Oo()I

    move-result v7

    const/16 v10, 0xa

    const/4 v12, 0x3

    if-ne v7, v12, :cond_4a

    int-to-float v7, v10

    sub-float v10, v9, v6

    sub-float/2addr v10, v7

    goto :goto_32

    :cond_4a
    sub-float v7, v0, v5

    int-to-float v10, v10

    sub-float/2addr v7, v10

    :goto_32
    move-object/from16 v27, v2

    move-object/from16 v28, v3

    move/from16 v29, v7

    move/from16 v30, v10

    move/from16 v31, v5

    move/from16 v32, v6

    .line 186
    invoke-virtual/range {v27 .. v32}, Lcom/intsig/pdfengine/PDF_Engine;->drawImage(Ljava/lang/String;FFFF)V

    add-float v30, v10, v6

    const/16 v33, 0x0

    move-object/from16 v28, v13

    .line 187
    invoke-virtual/range {v27 .. v33}, Lcom/intsig/pdfengine/PDF_Engine;->addLinkRegion(Ljava/lang/String;FFFFI)V

    goto :goto_33

    :cond_4b
    move-object/from16 v39, v1

    move/from16 v42, v6

    move/from16 v40, v12

    move-object/from16 v41, v13

    move-object/from16 v1, p4

    .line 188
    :cond_4c
    :goto_33
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pdfImageStartX="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v6, v45

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v7, " pdfImageStartY="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v7, v46

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v10, " pdfImageWidth="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v10, v47

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v12, " pdfImageHeight="

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v12, v48

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v13, " rotation="

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v14, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    int-to-float v5, v11

    move-object/from16 v27, v2

    move-object/from16 v28, v8

    move/from16 v29, v6

    move/from16 v30, v7

    move/from16 v31, v10

    move/from16 v32, v12

    move/from16 v33, v5

    .line 189
    invoke-virtual/range {v27 .. v33}, Lcom/intsig/pdfengine/PDF_Engine;->drawImage(Ljava/lang/String;FFFFF)V

    move-object/from16 v5, p13

    const/4 v6, 0x3

    .line 190
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 191
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    const/high16 v8, 0x42200000    # 40.0f

    if-nez v6, :cond_4e

    const-string v6, "UTF-8"

    .line 192
    invoke-static {v7, v6}, Lcom/intsig/camscanner/app/AppUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4d

    move-object/from16 v28, v6

    goto :goto_34

    :cond_4d
    move-object/from16 v28, v7

    :goto_34
    sub-float v29, v0, v8

    sub-float v30, v9, v8

    const/high16 v6, 0x41a00000    # 20.0f

    sub-float v31, v0, v6

    sub-float v32, v9, v6

    move-object/from16 v27, v2

    .line 193
    invoke-virtual/range {v27 .. v32}, Lcom/intsig/pdfengine/PDF_Engine;->addNote(Ljava/lang/String;FFFF)V

    goto :goto_35

    :cond_4e
    const/high16 v6, 0x41a00000    # 20.0f

    :goto_35
    if-lez v25, :cond_52

    const/4 v7, 0x0

    .line 194
    aget v9, v4, v7

    int-to-float v7, v9

    const/4 v9, 0x1

    aget v10, v4, v9

    int-to-float v10, v10

    invoke-static {v7, v10}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getFontSize(FF)F

    move-result v7

    invoke-static {v0, v7}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getAutoFitPaperSize(FF)F

    move-result v7

    add-int/lit8 v10, v26, 0x1

    .line 195
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v28

    move/from16 v10, v25

    if-eq v10, v9, :cond_50

    const/4 v11, 0x2

    if-eq v10, v11, :cond_4f

    .line 196
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v12

    int-to-float v12, v12

    mul-float v12, v12, v7

    sub-float/2addr v0, v12

    sub-float/2addr v0, v8

    goto :goto_36

    :cond_4f
    div-float v0, v0, v17

    goto :goto_36

    :cond_50
    const/4 v11, 0x2

    const/high16 v0, 0x42200000    # 40.0f

    :goto_36
    if-eqz v21, :cond_51

    const/high16 v8, 0x41200000    # 10.0f

    goto :goto_37

    :cond_51
    const/high16 v8, 0x41a00000    # 20.0f

    .line 197
    :goto_37
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "pageNumLocation"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v13, " pageNumX = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v13, " textStartHeight = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v14, v12}, Lcom/intsig/log/LogUtils;->o〇0(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-virtual {v2, v15, v7}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v27, v2

    move/from16 v29, v0

    move/from16 v30, v8

    .line 199
    invoke-virtual/range {v27 .. v32}, Lcom/intsig/pdfengine/PDF_Engine;->drawText(Ljava/lang/String;FFFI)V

    goto :goto_38

    :cond_52
    move/from16 v10, v25

    const/4 v9, 0x1

    const/4 v11, 0x2

    :goto_38
    move-object/from16 v7, p7

    move-object/from16 v8, v38

    const/16 v17, 0x0

    goto :goto_39

    :cond_53
    move-object/from16 v39, v1

    move/from16 v42, v6

    move-object/from16 p10, v7

    move/from16 v26, v8

    move-object v1, v9

    move-object/from16 v38, v11

    move/from16 v40, v12

    move-object/from16 v41, v13

    move/from16 v10, v25

    move/from16 v34, v36

    const/high16 v6, 0x41a00000    # 20.0f

    const/4 v9, 0x1

    const/4 v11, 0x2

    move/from16 v36, v5

    move-object v5, v15

    move-object/from16 v7, p7

    move-object/from16 v8, v38

    :goto_39
    move/from16 v12, v26

    if-eqz v7, :cond_54

    .line 200
    invoke-interface {v7, v12}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onProgress(I)V

    :cond_54
    :goto_3a
    add-int/lit8 v0, v12, 0x1

    move-object/from16 v7, p10

    move-object v9, v1

    move-object v15, v5

    move-object v11, v8

    move/from16 v25, v10

    move/from16 v5, v36

    move-object/from16 v1, v39

    move/from16 v12, v40

    move-object/from16 v13, v41

    move/from16 v6, v42

    move/from16 v10, p6

    move v8, v0

    move/from16 v36, v34

    goto/16 :goto_1e

    :cond_55
    move-object/from16 v7, p7

    move-object v1, v9

    move-object v8, v11

    move-object v5, v15

    .line 201
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 202
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    move/from16 v3, p6

    if-nez v0, :cond_57

    if-eqz v3, :cond_56

    const/4 v4, 0x3

    if-eq v3, v4, :cond_56

    const/4 v4, 0x4

    if-eq v3, v4, :cond_56

    const/4 v4, 0x5

    if-ne v3, v4, :cond_57

    :cond_56
    :try_start_8
    const-string v0, "000000000000000"
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_9

    move-object/from16 v4, p2

    .line 203
    :try_start_9
    invoke-static {v0, v4}, Lcom/intsig/crypto/CryptoUtil;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    move-object v4, v0

    goto :goto_3c

    :catch_8
    move-exception v0

    goto :goto_3b

    :catch_9
    move-exception v0

    move-object/from16 v4, p2

    .line 204
    :goto_3b
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PDF password:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v14, v5, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 205
    :goto_3c
    invoke-virtual {v2, v4, v4}, Lcom/intsig/pdfengine/PDF_Engine;->setPassword(Ljava/lang/String;Ljava/lang/String;)I

    :cond_57
    const-string v30, "intsig.com pdf producer"

    const-string v32, "CamScanner"

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v29, v2

    move-object/from16 v31, v22

    move-object/from16 v33, v22

    .line 206
    invoke-virtual/range {v29 .. v35}, Lcom/intsig/pdfengine/PDF_Engine;->setProperties(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {v2}, Lcom/intsig/pdfengine/PDF_Engine;->save2PdfFile()I

    move-result v0

    if-eqz v7, :cond_58

    .line 208
    array-length v4, v8

    invoke-interface {v7, v4}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onProgress(I)V

    .line 209
    :cond_58
    invoke-virtual {v2}, Lcom/intsig/pdfengine/PDF_Engine;->releaseCanvas()V

    if-nez v0, :cond_5f

    if-eqz v17, :cond_59

    goto/16 :goto_3e

    :cond_59
    if-nez v3, :cond_5a

    .line 210
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "_data"

    move-object/from16 v4, v16

    .line 211
    invoke-virtual {v0, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "state"

    const/4 v3, 0x0

    .line 212
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "modified_date savePdfProperty"

    .line 213
    invoke-static {v14, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    move-wide/from16 v5, p0

    invoke-static {v2, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    if-eqz v7, :cond_62

    .line 215
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->SUCCESS_CREATE:I

    invoke-interface {v7, v0, v4}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    goto/16 :goto_3f

    :cond_5a
    move-object/from16 v4, v16

    const/4 v2, 0x5

    if-ne v3, v2, :cond_5e

    .line 216
    invoke-static {}, Lcom/intsig/utils/SDStorageUtil;->oO80()Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 217
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "application/pdf"

    .line 219
    invoke-static {v1, v5, v2, v3}, Lcom/intsig/camscanner/util/SDStorageManager;->O0O8OO088(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v7, :cond_5c

    .line 220
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 221
    sget v1, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_SAVE_PDF_FILE:I

    const/4 v2, 0x0

    invoke-interface {v7, v1, v2}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    goto :goto_3d

    .line 222
    :cond_5b
    sget v1, Lcom/intsig/camscanner/pdfengine/PDF_Util;->SUCCESS_CREATE:I

    invoke-interface {v7, v1, v4}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    .line 223
    :cond_5c
    :goto_3d
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_62

    .line 224
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_3f

    :cond_5d
    if-eqz v7, :cond_62

    .line 225
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->SUCCESS_CREATE:I

    invoke-interface {v7, v0, v4}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    goto :goto_3f

    :cond_5e
    if-eqz v7, :cond_62

    .line 226
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->SUCCESS_CREATE:I

    invoke-interface {v7, v0, v4}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    goto :goto_3f

    :cond_5f
    :goto_3e
    move-object/from16 v4, v16

    .line 227
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "engine.save2PdfFile() failed , error code="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_60

    .line 230
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_60
    if-eqz v7, :cond_62

    if-eqz v17, :cond_61

    .line 231
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_ALL_PAGE_MISS:I

    const/4 v1, 0x0

    invoke-interface {v7, v0, v1}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    goto :goto_3f

    :cond_61
    const/4 v1, 0x0

    .line 232
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_SAVE_PDF_FILE:I

    invoke-interface {v7, v0, v1}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    .line 233
    :cond_62
    :goto_3f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "create PDF successfully ,pdfFile = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    :cond_63
    :goto_40
    move-object v7, v11

    if-eqz v7, :cond_64

    .line 234
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_INDEX_OUT_OF_BOUND:I

    const/4 v1, 0x0

    invoke-interface {v7, v0, v1}, Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;->onFinish(ILjava/lang/String;)V

    .line 235
    :cond_64
    invoke-static {v14, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public static createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;)Ljava/lang/String;
    .locals 9

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    .line 1
    invoke-static/range {v0 .. v8}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;Lcom/intsig/utils/ImageScaleListener;)Ljava/lang/String;
    .locals 9

    const/4 v7, 0x1

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v8, p7

    .line 2
    invoke-static/range {v0 .. v8}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;Z)Ljava/lang/String;
    .locals 9

    const/4 v8, 0x0

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move/from16 v7, p7

    .line 3
    invoke-static/range {v0 .. v8}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;)Ljava/lang/String;
    .locals 10

    const/4 v9, 0x0

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    .line 4
    invoke-static/range {v0 .. v9}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;)Ljava/lang/String;
    .locals 12

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-wide v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    .line 5
    invoke-static/range {v0 .. v11}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J[I",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Lcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;",
            "Z",
            "Lcom/intsig/utils/ImageScaleListener;",
            "Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;",
            "Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v13, 0x0

    move-wide v0, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    .line 6
    invoke-static/range {v0 .. v13}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(JLjava/lang/String;[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;Lcom/intsig/camscanner/pdfengine/PDF_Util$NoWatermarkInteceptor;Lcom/intsig/camscanner/pdfengine/PDF_Util$BgWatermarkListener;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static createPdfPath(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 3
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O08000()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdfPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createPdfPath(Landroid/content/Context;JLjava/lang/String;I)Ljava/lang/String;
    .locals 6

    .line 4
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O08000()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-wide v1, p1

    move-object v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdfPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createPdfPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const-string v5, ".pdf"

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    .line 1
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createDocPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createPdfPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 7

    const-string v6, ".pdf"

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    .line 2
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createDocPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createPdf_SinglePage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JIZZLjava/lang/String;)Z
    .locals 23

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v9, p1

    .line 4
    .line 5
    move-object/from16 v10, p2

    .line 6
    .line 7
    move-wide/from16 v2, p3

    .line 8
    .line 9
    move/from16 v11, p7

    .line 10
    .line 11
    move-object/from16 v12, p8

    .line 12
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v4, "createPdf_SinglePage()  srcpath:"

    .line 19
    .line 20
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const-string v13, "PDF_Util"

    .line 31
    .line 32
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/FileUtil;->〇00〇8(Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    const/4 v14, 0x0

    .line 40
    if-nez v0, :cond_0

    .line 41
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    const-string v1, "  is not a valid jpg file"

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    return v14

    .line 63
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 64
    .line 65
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .line 67
    .line 68
    const-string v4, "CreatePdf  outpath:"

    .line 69
    .line 70
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    const-string v4, " water="

    .line 77
    .line 78
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-static {}, Lcom/intsig/pdfengine/PDF_Engine;->getInstance()Lcom/intsig/pdfengine/PDF_Engine;

    .line 92
    .line 93
    .line 94
    move-result-object v15

    .line 95
    if-nez v15, :cond_1

    .line 96
    .line 97
    const-string v0, "engine == null"

    .line 98
    .line 99
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    return v14

    .line 103
    :cond_1
    const/4 v8, 0x2

    .line 104
    invoke-virtual {v15, v8}, Lcom/intsig/pdfengine/PDF_Engine;->setLogLevel(I)V

    .line 105
    .line 106
    .line 107
    const/4 v7, 0x0

    .line 108
    invoke-virtual {v15, v10, v7, v7}, Lcom/intsig/pdfengine/PDF_Engine;->createCanvas(Ljava/lang/String;FF)I

    .line 109
    .line 110
    .line 111
    const-wide/16 v4, 0x0

    .line 112
    .line 113
    const/16 v16, 0x1

    .line 114
    .line 115
    cmp-long v0, v2, v4

    .line 116
    .line 117
    if-eqz v0, :cond_3

    .line 118
    .line 119
    invoke-static {v2, v3, v1}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->checkPdfSizeIdExist(JLandroid/content/Context;)[I

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    aget v2, v0, v14

    .line 124
    .line 125
    const/4 v3, -0x1

    .line 126
    if-eq v2, v3, :cond_2

    .line 127
    .line 128
    aget v0, v0, v16

    .line 129
    .line 130
    if-eq v0, v3, :cond_2

    .line 131
    .line 132
    int-to-float v2, v2

    .line 133
    int-to-float v0, v0

    .line 134
    const/4 v3, 0x0

    .line 135
    goto :goto_0

    .line 136
    :cond_2
    const/4 v0, 0x0

    .line 137
    const/4 v2, 0x0

    .line 138
    const/4 v3, 0x1

    .line 139
    :goto_0
    move v5, v0

    .line 140
    move v4, v2

    .line 141
    goto :goto_1

    .line 142
    :cond_3
    const/4 v3, 0x1

    .line 143
    const/4 v4, 0x0

    .line 144
    const/4 v5, 0x0

    .line 145
    :goto_1
    if-eqz v11, :cond_6

    .line 146
    .line 147
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇〇808〇()Z

    .line 148
    .line 149
    .line 150
    move-result v0

    .line 151
    if-eqz v0, :cond_5

    .line 152
    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    .line 154
    .line 155
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 156
    .line 157
    .line 158
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v2

    .line 162
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getPicAddress()Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v2

    .line 169
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v2

    .line 176
    new-instance v6, Ljava/io/File;

    .line 177
    .line 178
    invoke-direct {v6, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    .line 182
    .line 183
    .line 184
    move-result v0

    .line 185
    if-nez v0, :cond_4

    .line 186
    .line 187
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    .line 188
    .line 189
    .line 190
    move-result-object v0

    .line 191
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getPicAddress()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v7

    .line 195
    invoke-virtual {v0, v7}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    .line 196
    .line 197
    .line 198
    move-result-object v0

    .line 199
    invoke-static {v0, v2}, Lcom/intsig/utils/FileUtil;->〇〇888(Ljava/io/InputStream;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    .line 201
    .line 202
    goto :goto_2

    .line 203
    :catch_0
    move-exception v0

    .line 204
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 205
    .line 206
    .line 207
    :cond_4
    :goto_2
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    .line 208
    .line 209
    .line 210
    move-result v0

    .line 211
    if-eqz v0, :cond_5

    .line 212
    .line 213
    const/high16 v0, 0x42340000    # 45.0f

    .line 214
    .line 215
    move-object v0, v2

    .line 216
    const/high16 v18, 0x42340000    # 45.0f

    .line 217
    .line 218
    goto :goto_3

    .line 219
    :cond_5
    const/4 v0, 0x0

    .line 220
    const/high16 v18, 0x41c80000    # 25.0f

    .line 221
    .line 222
    goto :goto_3

    .line 223
    :cond_6
    const/4 v0, 0x0

    .line 224
    const/16 v18, 0x0

    .line 225
    .line 226
    :goto_3
    move-object/from16 v2, p1

    .line 227
    .line 228
    move/from16 v6, p5

    .line 229
    .line 230
    const/16 v17, 0x0

    .line 231
    .line 232
    move/from16 v7, p6

    .line 233
    .line 234
    const/16 v19, 0x2

    .line 235
    .line 236
    move/from16 v8, v18

    .line 237
    .line 238
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->computeLayout(Ljava/lang/String;ZFFIZF)[F

    .line 239
    .line 240
    .line 241
    move-result-object v2

    .line 242
    aget v3, v2, v14

    .line 243
    .line 244
    aget v4, v2, v16

    .line 245
    .line 246
    aget v8, v2, v19

    .line 247
    .line 248
    const/4 v5, 0x3

    .line 249
    aget v18, v2, v5

    .line 250
    .line 251
    const/4 v5, 0x4

    .line 252
    aget v19, v2, v5

    .line 253
    .line 254
    const/4 v5, 0x5

    .line 255
    aget v20, v2, v5

    .line 256
    .line 257
    const/4 v5, 0x6

    .line 258
    aget v5, v2, v5

    .line 259
    .line 260
    const/4 v6, 0x7

    .line 261
    aget v6, v2, v6

    .line 262
    .line 263
    const/16 v6, 0x8

    .line 264
    .line 265
    aget v7, v2, v6

    .line 266
    .line 267
    const/16 v6, 0x9

    .line 268
    .line 269
    aget v21, v2, v6

    .line 270
    .line 271
    invoke-virtual {v15, v3, v4}, Lcom/intsig/pdfengine/PDF_Engine;->newPage(FF)V

    .line 272
    .line 273
    .line 274
    const/high16 v2, -0x1000000

    .line 275
    .line 276
    invoke-virtual {v15, v2}, Lcom/intsig/pdfengine/PDF_Engine;->setStrokeColor(I)V

    .line 277
    .line 278
    .line 279
    invoke-static {v3, v4}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getFontSize(FF)F

    .line 280
    .line 281
    .line 282
    move-result v2

    .line 283
    const-string v4, "freescan"

    .line 284
    .line 285
    invoke-virtual {v15, v4, v2}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    .line 286
    .line 287
    .line 288
    if-eqz v11, :cond_9

    .line 289
    .line 290
    cmpl-float v4, v7, v17

    .line 291
    .line 292
    if-lez v4, :cond_9

    .line 293
    .line 294
    const v4, 0x7f130197

    .line 295
    .line 296
    .line 297
    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 298
    .line 299
    .line 300
    move-result-object v4

    .line 301
    new-instance v1, Ljava/lang/StringBuilder;

    .line 302
    .line 303
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 304
    .line 305
    .line 306
    const-string v6, "pdfWaterMark="

    .line 307
    .line 308
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    .line 313
    .line 314
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 315
    .line 316
    .line 317
    move-result-object v1

    .line 318
    invoke-static {v13, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .line 320
    .line 321
    const/high16 v1, 0x41200000    # 10.0f

    .line 322
    .line 323
    if-eqz p6, :cond_7

    .line 324
    .line 325
    sub-float/2addr v5, v1

    .line 326
    goto :goto_4

    .line 327
    :cond_7
    const/high16 v5, 0x41200000    # 10.0f

    .line 328
    .line 329
    :goto_4
    sub-float/2addr v3, v1

    .line 330
    const/high16 v1, 0x432a0000    # 170.0f

    .line 331
    .line 332
    sub-float v11, v3, v1

    .line 333
    .line 334
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 335
    .line 336
    .line 337
    move-result v1

    .line 338
    if-eqz v1, :cond_8

    .line 339
    .line 340
    const-string v0, "Times-Roman"

    .line 341
    .line 342
    invoke-virtual {v15, v0, v2}, Lcom/intsig/pdfengine/PDF_Engine;->setFont(Ljava/lang/String;F)I

    .line 343
    .line 344
    .line 345
    const/4 v0, 0x0

    .line 346
    const/4 v6, 0x0

    .line 347
    move-object v1, v15

    .line 348
    move-object v2, v4

    .line 349
    move v3, v11

    .line 350
    move v4, v5

    .line 351
    move v5, v0

    .line 352
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/pdfengine/PDF_Engine;->drawText(Ljava/lang/String;FFFI)V

    .line 353
    .line 354
    .line 355
    goto :goto_5

    .line 356
    :cond_8
    const/high16 v1, 0x40a00000    # 5.0f

    .line 357
    .line 358
    sub-float v17, v7, v1

    .line 359
    .line 360
    const/high16 v1, 0x40000000    # 2.0f

    .line 361
    .line 362
    div-float v5, v17, v1

    .line 363
    .line 364
    const/4 v6, 0x0

    .line 365
    const/16 v22, 0x0

    .line 366
    .line 367
    move-object v1, v15

    .line 368
    move-object v2, v4

    .line 369
    move v3, v11

    .line 370
    move v4, v5

    .line 371
    move v5, v6

    .line 372
    move/from16 v6, v22

    .line 373
    .line 374
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/pdfengine/PDF_Engine;->drawText(Ljava/lang/String;FFFI)V

    .line 375
    .line 376
    .line 377
    sub-float v3, v11, v7

    .line 378
    .line 379
    const/high16 v4, 0x40a00000    # 5.0f

    .line 380
    .line 381
    const/4 v7, 0x0

    .line 382
    move-object v2, v0

    .line 383
    move/from16 v5, v17

    .line 384
    .line 385
    move/from16 v6, v17

    .line 386
    .line 387
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/pdfengine/PDF_Engine;->drawImage(Ljava/lang/String;FFFFF)V

    .line 388
    .line 389
    .line 390
    :cond_9
    :goto_5
    move-object v1, v15

    .line 391
    move-object/from16 v2, p1

    .line 392
    .line 393
    move/from16 v3, v19

    .line 394
    .line 395
    move/from16 v4, v20

    .line 396
    .line 397
    move v5, v8

    .line 398
    move/from16 v6, v18

    .line 399
    .line 400
    move/from16 v7, v21

    .line 401
    .line 402
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/pdfengine/PDF_Engine;->drawImage(Ljava/lang/String;FFFFF)V

    .line 403
    .line 404
    .line 405
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 406
    .line 407
    .line 408
    move-result v0

    .line 409
    if-nez v0, :cond_a

    .line 410
    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    .line 412
    .line 413
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 414
    .line 415
    .line 416
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    .line 418
    .line 419
    const-string v1, "x"

    .line 420
    .line 421
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    .line 423
    .line 424
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 425
    .line 426
    .line 427
    move-result-object v0

    .line 428
    invoke-virtual {v15, v12, v0}, Lcom/intsig/pdfengine/PDF_Engine;->setPassword(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    .line 430
    .line 431
    :cond_a
    const-string v2, "intsig.com pdf producer"

    .line 432
    .line 433
    const-string v3, ""

    .line 434
    .line 435
    const-string v4, "CamScanner"

    .line 436
    .line 437
    const-string v5, ""

    .line 438
    .line 439
    const/4 v6, 0x0

    .line 440
    const/4 v7, 0x0

    .line 441
    move-object v1, v15

    .line 442
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/pdfengine/PDF_Engine;->setProperties(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    .line 444
    .line 445
    invoke-virtual {v15}, Lcom/intsig/pdfengine/PDF_Engine;->save2PdfFile()I

    .line 446
    .line 447
    .line 448
    move-result v0

    .line 449
    new-instance v1, Ljava/lang/StringBuilder;

    .line 450
    .line 451
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 452
    .line 453
    .line 454
    const-string v2, "CreatePdf result code="

    .line 455
    .line 456
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 457
    .line 458
    .line 459
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 460
    .line 461
    .line 462
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 463
    .line 464
    .line 465
    move-result-object v1

    .line 466
    invoke-static {v13, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    .line 468
    .line 469
    invoke-virtual {v15}, Lcom/intsig/pdfengine/PDF_Engine;->releaseCanvas()V

    .line 470
    .line 471
    .line 472
    if-eqz v0, :cond_c

    .line 473
    .line 474
    new-instance v0, Ljava/io/File;

    .line 475
    .line 476
    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 477
    .line 478
    .line 479
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 480
    .line 481
    .line 482
    move-result v1

    .line 483
    if-eqz v1, :cond_b

    .line 484
    .line 485
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 486
    .line 487
    .line 488
    :cond_b
    return v14

    .line 489
    :cond_c
    return v16
.end method

.method public static createTextPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 1
    const-string v5, ".txt"

    .line 2
    .line 3
    move-object v0, p0

    .line 4
    move-wide v1, p1

    .line 5
    move-object v3, p3

    .line 6
    move-object v4, p4

    .line 7
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createDocPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static createWordPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 1
    const-string v5, ".docx"

    .line 2
    .line 3
    move-object v0, p0

    .line 4
    move-wide v1, p1

    .line 5
    move-object v3, p3

    .line 6
    move-object v4, p4

    .line 7
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createDocPath(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    return-object p0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static doEnhance(Ljava/lang/String;I)V
    .locals 3

    .line 1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "PDF_Util"

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string p0, "image is empty"

    .line 10
    .line 11
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {v2}, Lcom/intsig/tsapp/sync/AppConfigJson;->getEngineUseCupCount()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-static {v0, p0, p1, v2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->enhanceImageFile(ILjava/lang/String;II)Z

    .line 28
    .line 29
    .line 30
    move-result p0

    .line 31
    new-instance p1, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v2, "enhance result:"

    .line 37
    .line 38
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 52
    .line 53
    .line 54
    :goto_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static estimateDocListPDFSize(Landroid/content/Context;Ljava/util/List;)J
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .line 1
    const-string v0, "PDF_Util"

    .line 2
    .line 3
    if-eqz p0, :cond_0

    .line 4
    .line 5
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-lez v1, :cond_0

    .line 10
    .line 11
    invoke-static {p0, p1}, Lcom/intsig/camscanner/util/Util;->o8(Landroid/content/Context;Ljava/util/List;)J

    .line 12
    .line 13
    .line 14
    move-result-wide v1

    .line 15
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 16
    .line 17
    .line 18
    move-result p0

    .line 19
    mul-int/lit16 p0, p0, 0x400

    .line 20
    .line 21
    int-to-long v3, p0

    .line 22
    const-wide/16 v5, 0x8

    .line 23
    .line 24
    mul-long v3, v3, v5

    .line 25
    .line 26
    add-long/2addr v1, v3

    .line 27
    new-instance p0, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v3, "estimateDocListPDFSize docId length= "

    .line 33
    .line 34
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string p1, " size="

    .line 45
    .line 46
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {p0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p0

    .line 56
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    const-string p0, "estimateDocListPDFSize context or docId is <= 0"

    .line 61
    .line 62
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    const-wide/16 v1, 0x0

    .line 66
    .line 67
    :goto_0
    return-wide v1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static estimateDocsPDFSize(Landroid/content/Context;J)J
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-static {p0, p1, p2, v0}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->estimateDocsPDFSize(Landroid/content/Context;JLjava/lang/String;)J

    move-result-wide p0

    return-wide p0
.end method

.method public static estimateDocsPDFSize(Landroid/content/Context;JLjava/lang/String;)J
    .locals 5

    const-string v0, "PDF_Util"

    const-wide/16 v1, 0x0

    if-eqz p0, :cond_0

    cmp-long v3, p1, v1

    if-lez v3, :cond_0

    .line 2
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/util/Util;->o〇O(Landroid/content/Context;JLjava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x2000

    add-long/2addr v1, v3

    .line 3
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "estimateDocsPDFSize docId="

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " size="

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string p0, "estimateDocsPDFSize context or docId is <= 0"

    .line 4
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-wide v1
.end method

.method public static getAutoFitPaperSize(FF)F
    .locals 3

    .line 1
    const-wide/16 v0, 0x2

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 4
    .line 5
    .line 6
    move-result-object v2

    .line 7
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->checkPdfSizeIdExist(JLandroid/content/Context;)[I

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    aget v0, v0, v1

    .line 13
    .line 14
    if-lez v0, :cond_0

    .line 15
    .line 16
    int-to-float v0, v0

    .line 17
    cmpl-float v1, p0, v0

    .line 18
    .line 19
    if-lez v1, :cond_0

    .line 20
    .line 21
    div-float/2addr p0, v0

    .line 22
    mul-float p1, p1, p0

    .line 23
    .line 24
    :cond_0
    return p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static getBitmapFromString(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    const/high16 v0, -0x1000000

    const/4 v1, -0x1

    .line 1
    invoke-static {p0, p1, v0, v1}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->getBitmapFromString(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static getBitmapFromString(Landroid/content/Context;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 4

    .line 2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    .line 3
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 4
    new-instance p0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {p0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 5
    invoke-virtual {v1, p0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 p0, 0x41200000    # 10.0f

    mul-float v0, v0, p0

    .line 7
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    const/4 p0, 0x1

    .line 8
    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setGravity(I)V

    .line 9
    invoke-virtual {v1, p0}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 10
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 p1, 0x0

    .line 11
    invoke-static {p1, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 12
    invoke-static {p1, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 13
    invoke-virtual {v1, p2, v0}, Landroid/view/View;->measure(II)V

    .line 14
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {v1, p1, p1, p2, v0}, Landroid/view/View;->layout(IIII)V

    .line 15
    invoke-virtual {v1, p3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 16
    invoke-virtual {v1, p0}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 17
    invoke-virtual {v1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 18
    invoke-static {p0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 19
    invoke-virtual {v1, p1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getFontSize(FF)F
    .locals 2

    .line 1
    invoke-static {p0, p1}, Ljava/lang/Math;->max(FF)F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const v1, 0x44548000    # 850.0f

    .line 6
    .line 7
    .line 8
    cmpl-float v0, v0, v1

    .line 9
    .line 10
    if-lez v0, :cond_0

    .line 11
    .line 12
    const/high16 p0, 0x41700000    # 15.0f

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->max(FF)F

    .line 16
    .line 17
    .line 18
    move-result p0

    .line 19
    const p1, 0x3c905a38

    .line 20
    .line 21
    .line 22
    mul-float p0, p0, p1

    .line 23
    .line 24
    :goto_0
    return p0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static getJPGFromBitmap(Landroid/graphics/Bitmap;)[Ljava/lang/String;
    .locals 5

    .line 1
    const-string v0, "PDF_Util"

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    const-string v2, "text_image.jpg"

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    :try_start_0
    new-instance v2, Ljava/io/File;

    .line 25
    .line 26
    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 30
    .line 31
    .line 32
    new-instance v3, Ljava/io/FileOutputStream;

    .line 33
    .line 34
    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 35
    .line 36
    .line 37
    if-nez p0, :cond_0

    .line 38
    .line 39
    const-string v2, "bitmap null"

    .line 40
    .line 41
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    const/4 p0, 0x0

    .line 45
    return-object p0

    .line 46
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 47
    .line 48
    const/16 v4, 0x64

    .line 49
    .line 50
    invoke-virtual {p0, v2, v4, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 51
    .line 52
    .line 53
    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :catch_0
    move-exception v2

    .line 61
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62
    .line 63
    .line 64
    :goto_0
    const/4 v0, 0x3

    .line 65
    new-array v0, v0, [Ljava/lang/String;

    .line 66
    .line 67
    const/4 v2, 0x0

    .line 68
    aput-object v1, v0, v2

    .line 69
    .line 70
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    div-int/lit8 v1, v1, 0x7

    .line 75
    .line 76
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    const/4 v2, 0x1

    .line 81
    aput-object v1, v0, v2

    .line 82
    .line 83
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 84
    .line 85
    .line 86
    move-result p0

    .line 87
    div-int/lit8 p0, p0, 0x7

    .line 88
    .line 89
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object p0

    .line 93
    const/4 v1, 0x2

    .line 94
    aput-object p0, v0, v1

    .line 95
    .line 96
    return-object v0
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static getOutputFolder()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/utils/SDStorageUtil;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0

    .line 12
    :cond_0
    invoke-static {}, Lcom/intsig/comm/util/SDStorageLegacy;->〇080()Ljava/io/File;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    return-object v0
    .line 21
.end method

.method public static getOutputFolderShowName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/SDStorageUtil;->〇80〇808〇O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const v1, 0x7f130b28

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string p0, "/CamScanner"

    .line 23
    .line 24
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    return-object p0

    .line 32
    :cond_0
    invoke-static {}, Lcom/intsig/comm/util/SDStorageLegacy;->〇080()Ljava/io/File;

    .line 33
    .line 34
    .line 35
    move-result-object p0

    .line 36
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    return-object p0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static getPicAddress()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0〇o8o〇〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v0, "cs_logo.jpg"

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const-string v0, "qr_code.jpg"

    .line 11
    .line 12
    :goto_0
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static isNeedAddPrefix(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p0, :cond_1

    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    const-string v4, " upper(title)=? and _id >0 "

    .line 12
    .line 13
    const/4 p0, 0x1

    .line 14
    new-array v5, p0, [Ljava/lang/String;

    .line 15
    .line 16
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    aput-object p1, v5, v0

    .line 21
    .line 22
    const/4 v6, 0x0

    .line 23
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    if-eqz p1, :cond_1

    .line 28
    .line 29
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-le v1, p0, :cond_0

    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 37
    .line 38
    .line 39
    :cond_1
    return v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static isPayVersion()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o8oO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic lambda$clearPdfForHuaWeiPay$0()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "com.intsig.camscanner.huawei"

    .line 11
    .line 12
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 19
    .line 20
    const-string v1, "com.intsig.camscanner"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/utils/AppInstallerUtil;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    const-string v0, "PDF_Util"

    .line 29
    .line 30
    const-string v1, "hasAnotherCSClient, clear pdf first"

    .line 31
    .line 32
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->clearNormalPdf()V

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static parapdfImageInfo(FFFFFF[FZ)V
    .locals 12

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pdf/watermark/PdfHyperLinkWaterMark;->〇o00〇〇Oo()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x3

    .line 8
    if-ne v0, v3, :cond_0

    .line 9
    .line 10
    if-nez p7, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    const/high16 v4, 0x40000000    # 2.0f

    .line 16
    .line 17
    const/4 v5, 0x0

    .line 18
    cmpl-float v6, p2, v5

    .line 19
    .line 20
    if-lez v6, :cond_5

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    cmpl-float v0, p5, v5

    .line 25
    .line 26
    if-lez v0, :cond_2

    .line 27
    .line 28
    move v6, p2

    .line 29
    move/from16 v0, p5

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_1
    cmpl-float v0, p5, v5

    .line 33
    .line 34
    if-lez v0, :cond_2

    .line 35
    .line 36
    move v0, p2

    .line 37
    move/from16 v6, p5

    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_2
    move v0, p2

    .line 41
    move v6, v0

    .line 42
    :goto_1
    mul-float v7, p2, v4

    .line 43
    .line 44
    cmpg-float v8, v7, p0

    .line 45
    .line 46
    if-gez v8, :cond_3

    .line 47
    .line 48
    sub-float v7, p0, v7

    .line 49
    .line 50
    move v8, p2

    .line 51
    goto :goto_2

    .line 52
    :cond_3
    const/4 v8, 0x0

    .line 53
    move v7, p0

    .line 54
    :goto_2
    add-float v9, v0, v6

    .line 55
    .line 56
    cmpg-float v9, v9, p1

    .line 57
    .line 58
    if-gez v9, :cond_4

    .line 59
    .line 60
    sub-float v0, p1, v0

    .line 61
    .line 62
    sub-float/2addr v0, v6

    .line 63
    move/from16 v5, p5

    .line 64
    .line 65
    goto :goto_4

    .line 66
    :cond_4
    move v0, p1

    .line 67
    const/4 v6, 0x0

    .line 68
    goto :goto_4

    .line 69
    :cond_5
    cmpg-float v6, p5, p1

    .line 70
    .line 71
    if-gez v6, :cond_7

    .line 72
    .line 73
    if-eqz v0, :cond_6

    .line 74
    .line 75
    const/4 v0, 0x0

    .line 76
    goto :goto_3

    .line 77
    :cond_6
    move/from16 v0, p5

    .line 78
    .line 79
    :goto_3
    sub-float v6, p1, p5

    .line 80
    .line 81
    const/4 v8, 0x0

    .line 82
    move v7, p0

    .line 83
    move/from16 v5, p5

    .line 84
    .line 85
    move v11, v6

    .line 86
    move v6, v0

    .line 87
    move v0, v11

    .line 88
    goto :goto_4

    .line 89
    :cond_7
    const/4 v6, 0x0

    .line 90
    const/4 v8, 0x0

    .line 91
    move v7, p0

    .line 92
    move v0, p1

    .line 93
    :goto_4
    div-float v9, v7, p3

    .line 94
    .line 95
    div-float v10, v0, p4

    .line 96
    .line 97
    cmpg-float v9, v9, v10

    .line 98
    .line 99
    if-gez v9, :cond_8

    .line 100
    .line 101
    mul-float v9, v7, p4

    .line 102
    .line 103
    div-float/2addr v9, p3

    .line 104
    move v10, v9

    .line 105
    move v9, v7

    .line 106
    goto :goto_5

    .line 107
    :cond_8
    mul-float v9, v0, p3

    .line 108
    .line 109
    div-float v9, v9, p4

    .line 110
    .line 111
    move v10, v0

    .line 112
    :goto_5
    sub-float/2addr v7, v9

    .line 113
    div-float/2addr v7, v4

    .line 114
    add-float/2addr v7, v8

    .line 115
    sub-float/2addr v0, v10

    .line 116
    div-float/2addr v0, v4

    .line 117
    add-float/2addr v0, v6

    .line 118
    aput v7, p6, v2

    .line 119
    .line 120
    aput v0, p6, v1

    .line 121
    .line 122
    const/4 v0, 0x2

    .line 123
    aput v9, p6, v0

    .line 124
    .line 125
    aput v10, p6, v3

    .line 126
    .line 127
    const/4 v0, 0x4

    .line 128
    aput v5, p6, v0

    .line 129
    .line 130
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
.end method

.method private static toMap(Ljava/util/ArrayList;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {p0}, Lcom/intsig/utils/ListUtils;->〇080(Ljava/util/List;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-lez v0, :cond_2

    .line 6
    .line 7
    new-instance v0, Landroidx/collection/ArrayMap;

    .line 8
    .line 9
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-direct {v0, v1}, Landroidx/collection/ArrayMap;-><init>(I)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_1

    .line 25
    .line 26
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;

    .line 31
    .line 32
    if-eqz v1, :cond_0

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/signature/PdfSignatureSplice$PdfSignatureImage;->〇o00〇〇Oo()J

    .line 35
    .line 36
    .line 37
    move-result-wide v2

    .line 38
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    return-object v0

    .line 47
    :cond_2
    const/4 p0, 0x0

    .line 48
    return-object p0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static watermarkEnlargeFactor()F
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isEnlargeIndonesiaWatermarkFontSize()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/high16 v0, 0x3fc00000    # 1.5f

    .line 12
    .line 13
    return v0

    .line 14
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 15
    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇080()V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->lambda$clearPdfForHuaWeiPay$0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;
.super Landroid/widget/RelativeLayout;
.source "DefaultScrollHandle.java"

# interfaces
.implements Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$TimeOutListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_TEXT_SIZE:I = 0x10

.field private static final HANDLE_LONG:I = 0x28

.field private static final HANDLE_SHORT:I = 0x28


# instance fields
.field private bgResId:I

.field protected context:Landroid/content/Context;

.field private currentPos:F

.field private handler:Landroid/os/Handler;

.field private hidePageScrollerRunnable:Ljava/lang/Runnable;

.field private inverted:Z

.field private mTimeOutListener:Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$TimeOutListener;

.field private pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

.field private relativeHandlerMiddle:F

.field private textLeftPadding:I

.field protected textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;-><init>(Landroid/content/Context;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;-><init>(Landroid/content/Context;ZLcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$TimeOutListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$TimeOutListener;)V
    .locals 1

    .line 3
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->relativeHandlerMiddle:F

    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->bgResId:I

    .line 6
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textLeftPadding:I

    .line 7
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->handler:Landroid/os/Handler;

    .line 8
    new-instance v0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$1;

    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$1;-><init>(Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;)V

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->hidePageScrollerRunnable:Ljava/lang/Runnable;

    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 10
    iput-boolean p2, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->inverted:Z

    .line 11
    iput-object p3, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->mTimeOutListener:Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$TimeOutListener;

    .line 12
    new-instance p2, Landroid/widget/TextView;

    invoke-direct {p2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textView:Landroid/widget/TextView;

    const/4 p1, 0x4

    .line 13
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    const/high16 p1, -0x1000000

    .line 14
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->setTextColor(I)V

    const/16 p1, 0x10

    .line 15
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->setTextSize(I)V

    return-void
.end method

.method private calculateMiddle()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->isSwipeVertical()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getY()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    int-to-float v1, v1

    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 19
    .line 20
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getX()F

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    int-to-float v1, v1

    .line 34
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 35
    .line 36
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    :goto_0
    int-to-float v2, v2

    .line 41
    iget v3, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->relativeHandlerMiddle:F

    .line 42
    .line 43
    add-float/2addr v0, v3

    .line 44
    div-float/2addr v0, v2

    .line 45
    mul-float v0, v0, v1

    .line 46
    .line 47
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->relativeHandlerMiddle:F

    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private isPDFViewReady()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->getPageCount()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->documentFitsView()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private setPosition(F)V
    .locals 3

    .line 1
    invoke-static {p1}, Ljava/lang/Float;->isInfinite(F)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_5

    .line 6
    .line 7
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_3

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->isSwipeVertical()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 23
    .line 24
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 30
    .line 31
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    :goto_0
    int-to-float v0, v0

    .line 36
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->relativeHandlerMiddle:F

    .line 37
    .line 38
    sub-float/2addr p1, v1

    .line 39
    const/4 v1, 0x0

    .line 40
    cmpg-float v2, p1, v1

    .line 41
    .line 42
    if-gez v2, :cond_2

    .line 43
    .line 44
    const/4 p1, 0x0

    .line 45
    goto :goto_1

    .line 46
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 47
    .line 48
    const/16 v2, 0x28

    .line 49
    .line 50
    invoke-static {v1, v2}, Lcom/intsig/camscanner/pdfengine/utils/Util;->getDP(Landroid/content/Context;I)I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    int-to-float v1, v1

    .line 55
    sub-float v1, v0, v1

    .line 56
    .line 57
    cmpl-float v1, p1, v1

    .line 58
    .line 59
    if-lez v1, :cond_3

    .line 60
    .line 61
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 62
    .line 63
    invoke-static {p1, v2}, Lcom/intsig/camscanner/pdfengine/utils/Util;->getDP(Landroid/content/Context;I)I

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    int-to-float p1, p1

    .line 68
    sub-float p1, v0, p1

    .line 69
    .line 70
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->isSwipeVertical()Z

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-eqz v0, :cond_4

    .line 77
    .line 78
    invoke-virtual {p0, p1}, Landroid/view/View;->setY(F)V

    .line 79
    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_4
    invoke-virtual {p0, p1}, Landroid/view/View;->setX(F)V

    .line 83
    .line 84
    .line 85
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->calculateMiddle()V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 89
    .line 90
    .line 91
    :cond_5
    :goto_3
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;)Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$TimeOutListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->mTimeOutListener:Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle$TimeOutListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public destroyLayout()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hide()V
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hideDelayed()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->handler:Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->hidePageScrollerRunnable:Ljava/lang/Runnable;

    .line 4
    .line 5
    const-wide/16 v2, 0x7d0

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->isPDFViewReady()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1

    .line 12
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, 0x1

    .line 17
    if-eqz v0, :cond_2

    .line 18
    .line 19
    if-eq v0, v1, :cond_1

    .line 20
    .line 21
    const/4 v2, 0x2

    .line 22
    if-eq v0, v2, :cond_4

    .line 23
    .line 24
    const/4 v2, 0x3

    .line 25
    if-eq v0, v2, :cond_1

    .line 26
    .line 27
    const/4 v2, 0x5

    .line 28
    if-eq v0, v2, :cond_2

    .line 29
    .line 30
    const/4 v2, 0x6

    .line 31
    if-eq v0, v2, :cond_1

    .line 32
    .line 33
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    return p1

    .line 38
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->hideDelayed()V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->performPageSnap()V

    .line 44
    .line 45
    .line 46
    return v1

    .line 47
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->stopFling()V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->handler:Landroid/os/Handler;

    .line 53
    .line 54
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->hidePageScrollerRunnable:Ljava/lang/Runnable;

    .line 55
    .line 56
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->isSwipeVertical()Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-eqz v0, :cond_3

    .line 66
    .line 67
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getY()F

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    sub-float/2addr v0, v2

    .line 76
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->currentPos:F

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    invoke-virtual {p0}, Landroid/view/View;->getX()F

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    sub-float/2addr v0, v2

    .line 88
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->currentPos:F

    .line 89
    .line 90
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 91
    .line 92
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->isSwipeVertical()Z

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    const/4 v2, 0x0

    .line 97
    if-eqz v0, :cond_5

    .line 98
    .line 99
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    .line 100
    .line 101
    .line 102
    move-result p1

    .line 103
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->currentPos:F

    .line 104
    .line 105
    sub-float/2addr p1, v0

    .line 106
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->relativeHandlerMiddle:F

    .line 107
    .line 108
    add-float/2addr p1, v0

    .line 109
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->setPosition(F)V

    .line 110
    .line 111
    .line 112
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 113
    .line 114
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->relativeHandlerMiddle:F

    .line 115
    .line 116
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 117
    .line 118
    .line 119
    move-result v3

    .line 120
    int-to-float v3, v3

    .line 121
    div-float/2addr v0, v3

    .line 122
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/pdfengine/PDFView;->setPositionOffset(FZ)V

    .line 123
    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    .line 127
    .line 128
    .line 129
    move-result p1

    .line 130
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->currentPos:F

    .line 131
    .line 132
    sub-float/2addr p1, v0

    .line 133
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->relativeHandlerMiddle:F

    .line 134
    .line 135
    add-float/2addr p1, v0

    .line 136
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->setPosition(F)V

    .line 137
    .line 138
    .line 139
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 140
    .line 141
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->relativeHandlerMiddle:F

    .line 142
    .line 143
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 144
    .line 145
    .line 146
    move-result v3

    .line 147
    int-to-float v3, v3

    .line 148
    div-float/2addr v0, v3

    .line 149
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/pdfengine/PDFView;->setPositionOffset(FZ)V

    .line 150
    .line 151
    .line 152
    :goto_1
    return v1
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public setPageNum(I)V
    .locals 1

    .line 1
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textView:Landroid/widget/TextView;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textView:Landroid/widget/TextView;

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setPageNumBg(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->bgResId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setScroll(F)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->shown()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->show()V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->handler:Landroid/os/Handler;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->hidePageScrollerRunnable:Ljava/lang/Runnable;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->isSwipeVertical()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 27
    .line 28
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    goto :goto_1

    .line 33
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 34
    .line 35
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    :goto_1
    int-to-float v0, v0

    .line 40
    mul-float v0, v0, p1

    .line 41
    .line 42
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->setPosition(F)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setTextColor(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textView:Landroid/widget/TextView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setTextLeftPadding(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textLeftPadding:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setTextSize(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textView:Landroid/widget/TextView;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    int-to-float p1, p1

    .line 5
    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setupLayout(Lcom/intsig/camscanner/pdfengine/PDFView;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/PDFView;->isSwipeVertical()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->inverted:Z

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 12
    .line 13
    const v1, 0x7f08040c

    .line 14
    .line 15
    .line 16
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const/16 v1, 0x9

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->bgResId:I

    .line 24
    .line 25
    const/16 v1, 0xb

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 30
    .line 31
    invoke-static {v2, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 37
    .line 38
    const v2, 0x7f08040d

    .line 39
    .line 40
    .line 41
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    goto :goto_0

    .line 46
    :cond_2
    iget-boolean v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->inverted:Z

    .line 47
    .line 48
    if-eqz v0, :cond_3

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 51
    .line 52
    const v1, 0x7f08040e

    .line 53
    .line 54
    .line 55
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const/16 v1, 0xa

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 63
    .line 64
    const v1, 0x7f08040b

    .line 65
    .line 66
    .line 67
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    const/16 v1, 0xc

    .line 72
    .line 73
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 74
    .line 75
    .line 76
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 77
    .line 78
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 79
    .line 80
    const/16 v3, 0x28

    .line 81
    .line 82
    invoke-static {v2, v3}, Lcom/intsig/camscanner/pdfengine/utils/Util;->getDP(Landroid/content/Context;I)I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    iget-object v4, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 87
    .line 88
    invoke-static {v4, v3}, Lcom/intsig/camscanner/pdfengine/utils/Util;->getDP(Landroid/content/Context;I)I

    .line 89
    .line 90
    .line 91
    move-result v3

    .line 92
    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 93
    .line 94
    .line 95
    const/4 v2, 0x0

    .line 96
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 97
    .line 98
    .line 99
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 100
    .line 101
    const/4 v4, -0x2

    .line 102
    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 103
    .line 104
    .line 105
    const/16 v4, 0xd

    .line 106
    .line 107
    const/4 v5, -0x1

    .line 108
    invoke-virtual {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 109
    .line 110
    .line 111
    iget-object v4, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textView:Landroid/widget/TextView;

    .line 112
    .line 113
    invoke-virtual {p0, v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {p1, p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 120
    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textView:Landroid/widget/TextView;

    .line 123
    .line 124
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->context:Landroid/content/Context;

    .line 125
    .line 126
    iget v3, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->textLeftPadding:I

    .line 127
    .line 128
    invoke-static {v1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 133
    .line 134
    .line 135
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/srcoll/DefaultScrollHandle;->pdfView:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 136
    .line 137
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public show()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public shown()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

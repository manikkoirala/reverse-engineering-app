.class public interface abstract Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;
.super Ljava/lang/Object;
.source "ScrollHandle.java"


# virtual methods
.method public abstract destroyLayout()V
.end method

.method public abstract hide()V
.end method

.method public abstract hideDelayed()V
.end method

.method public abstract setPageNum(I)V
.end method

.method public abstract setScroll(F)V
.end method

.method public abstract setupLayout(Lcom/intsig/camscanner/pdfengine/PDFView;)V
.end method

.method public abstract show()V
.end method

.method public abstract shown()Z
.end method

.class public Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
.super Ljava/lang/Object;
.source "PDFView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pdfengine/PDFView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Configurator"
.end annotation


# instance fields
.field private annotationRendering:Z

.field private antialiasing:Z

.field private autoSpacing:Z

.field private defaultPage:I

.field private final documentSource:Lcom/intsig/camscanner/pdfengine/source/DocumentSource;

.field private enableDoubletap:Z

.field private enableSwipe:Z

.field private linkHandler:Lcom/intsig/camscanner/pdfengine/link/LinkHandler;

.field private nightMode:Z

.field private onDrawAllListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

.field private onDrawListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

.field private onErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;

.field private onLoadCompleteListener:Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;

.field private onLongPressListener:Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;

.field private onPageChangeListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;

.field private onPageErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;

.field private onPageScrollListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;

.field private onRenderListener:Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;

.field private onTapListener:Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;

.field private pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

.field private pageFling:Z

.field private pageNumbers:[I

.field private pageSnap:Z

.field private password:Ljava/lang/String;

.field private scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

.field private spacing:I

.field private swipeHorizontal:Z

.field final synthetic this$0:Lcom/intsig/camscanner/pdfengine/PDFView;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;)V
    .locals 3

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageNumbers:[I

    const/4 v1, 0x1

    .line 4
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->enableSwipe:Z

    .line 5
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->enableDoubletap:Z

    .line 6
    new-instance v2, Lcom/intsig/camscanner/pdfengine/link/DefaultLinkHandler;

    invoke-direct {v2, p1}, Lcom/intsig/camscanner/pdfengine/link/DefaultLinkHandler;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;)V

    iput-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->linkHandler:Lcom/intsig/camscanner/pdfengine/link/LinkHandler;

    const/4 p1, 0x0

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->defaultPage:I

    .line 8
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->swipeHorizontal:Z

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->annotationRendering:Z

    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->password:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 12
    iput-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->antialiasing:Z

    .line 13
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->spacing:I

    .line 14
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->autoSpacing:Z

    .line 15
    sget-object v0, Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;->WIDTH:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 16
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageFling:Z

    .line 17
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageSnap:Z

    .line 18
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->nightMode:Z

    .line 19
    iput-object p2, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->documentSource:Lcom/intsig/camscanner/pdfengine/source/DocumentSource;

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Lcom/intsig/camscanner/pdfengine/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;-><init>(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;)V

    return-void
.end method


# virtual methods
.method public autoSpacing(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->autoSpacing:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public defaultPage(I)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->defaultPage:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public enableAnnotationRendering(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->annotationRendering:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public enableAntialiasing(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->antialiasing:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public enableDoubletap(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->enableDoubletap:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public enableSwipe(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->enableSwipe:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public linkHandler(Lcom/intsig/camscanner/pdfengine/link/LinkHandler;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->linkHandler:Lcom/intsig/camscanner/pdfengine/link/LinkHandler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public load()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->〇080(Lcom/intsig/camscanner/pdfengine/PDFView;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 10
    .line 11
    invoke-static {v0, p0}, Lcom/intsig/camscanner/pdfengine/PDFView;->〇o00〇〇Oo(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->recycle()V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 21
    .line 22
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onLoadCompleteListener:Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnLoadComplete(Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 30
    .line 31
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnError(Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 39
    .line 40
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onDrawListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnDraw(Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 48
    .line 49
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onDrawAllListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnDrawAll(Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 57
    .line 58
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 59
    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onPageChangeListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnPageChange(Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;)V

    .line 63
    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 66
    .line 67
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 68
    .line 69
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onPageScrollListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;

    .line 70
    .line 71
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnPageScroll(Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;)V

    .line 72
    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 75
    .line 76
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 77
    .line 78
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onRenderListener:Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;

    .line 79
    .line 80
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnRender(Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 84
    .line 85
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 86
    .line 87
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onTapListener:Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnTap(Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;)V

    .line 90
    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 93
    .line 94
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 95
    .line 96
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onLongPressListener:Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnLongPress(Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;)V

    .line 99
    .line 100
    .line 101
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 102
    .line 103
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 104
    .line 105
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onPageErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;

    .line 106
    .line 107
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setOnPageError(Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;)V

    .line 108
    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 111
    .line 112
    iget-object v0, v0, Lcom/intsig/camscanner/pdfengine/PDFView;->callbacks:Lcom/intsig/camscanner/pdfengine/listener/Callbacks;

    .line 113
    .line 114
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->linkHandler:Lcom/intsig/camscanner/pdfengine/link/LinkHandler;

    .line 115
    .line 116
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/listener/Callbacks;->setLinkHandler(Lcom/intsig/camscanner/pdfengine/link/LinkHandler;)V

    .line 117
    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 120
    .line 121
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->enableSwipe:Z

    .line 122
    .line 123
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setSwipeEnabled(Z)V

    .line 124
    .line 125
    .line 126
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 127
    .line 128
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->nightMode:Z

    .line 129
    .line 130
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setNightMode(Z)V

    .line 131
    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 134
    .line 135
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->enableDoubletap:Z

    .line 136
    .line 137
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->enableDoubletap(Z)V

    .line 138
    .line 139
    .line 140
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 141
    .line 142
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->defaultPage:I

    .line 143
    .line 144
    invoke-static {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->o〇0(Lcom/intsig/camscanner/pdfengine/PDFView;I)V

    .line 145
    .line 146
    .line 147
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 148
    .line 149
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->swipeHorizontal:Z

    .line 150
    .line 151
    xor-int/lit8 v1, v1, 0x1

    .line 152
    .line 153
    invoke-static {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/pdfengine/PDFView;Z)V

    .line 154
    .line 155
    .line 156
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 157
    .line 158
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->annotationRendering:Z

    .line 159
    .line 160
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->enableAnnotationRendering(Z)V

    .line 161
    .line 162
    .line 163
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 164
    .line 165
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 166
    .line 167
    invoke-static {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->oO80(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;)V

    .line 168
    .line 169
    .line 170
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 171
    .line 172
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->antialiasing:Z

    .line 173
    .line 174
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->enableAntialiasing(Z)V

    .line 175
    .line 176
    .line 177
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 178
    .line 179
    iget v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->spacing:I

    .line 180
    .line 181
    invoke-static {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->〇80〇808〇O(Lcom/intsig/camscanner/pdfengine/PDFView;I)V

    .line 182
    .line 183
    .line 184
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 185
    .line 186
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->autoSpacing:Z

    .line 187
    .line 188
    invoke-static {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->Oo08(Lcom/intsig/camscanner/pdfengine/PDFView;Z)V

    .line 189
    .line 190
    .line 191
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 192
    .line 193
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 194
    .line 195
    invoke-static {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->〇〇888(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;)V

    .line 196
    .line 197
    .line 198
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 199
    .line 200
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageSnap:Z

    .line 201
    .line 202
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setPageSnap(Z)V

    .line 203
    .line 204
    .line 205
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 206
    .line 207
    iget-boolean v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageFling:Z

    .line 208
    .line 209
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/PDFView;->setPageFling(Z)V

    .line 210
    .line 211
    .line 212
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageNumbers:[I

    .line 213
    .line 214
    if-eqz v0, :cond_1

    .line 215
    .line 216
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 217
    .line 218
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->documentSource:Lcom/intsig/camscanner/pdfengine/source/DocumentSource;

    .line 219
    .line 220
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->password:Ljava/lang/String;

    .line 221
    .line 222
    invoke-static {v1, v2, v3, v0}, Lcom/intsig/camscanner/pdfengine/PDFView;->O8(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;[I)V

    .line 223
    .line 224
    .line 225
    goto :goto_0

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->this$0:Lcom/intsig/camscanner/pdfengine/PDFView;

    .line 227
    .line 228
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->documentSource:Lcom/intsig/camscanner/pdfengine/source/DocumentSource;

    .line 229
    .line 230
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->password:Ljava/lang/String;

    .line 231
    .line 232
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/pdfengine/PDFView;->〇o〇(Lcom/intsig/camscanner/pdfengine/PDFView;Lcom/intsig/camscanner/pdfengine/source/DocumentSource;Ljava/lang/String;)V

    .line 233
    .line 234
    .line 235
    :goto_0
    return-void
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public nightMode(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->nightMode:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onDraw(Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onDrawListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onDrawAll(Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onDrawAllListener:Lcom/intsig/camscanner/pdfengine/listener/OnDrawListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onError(Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnErrorListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onLoad(Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onLoadCompleteListener:Lcom/intsig/camscanner/pdfengine/listener/OnLoadCompleteListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onLongPress(Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onLongPressListener:Lcom/intsig/camscanner/pdfengine/listener/OnLongPressListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onPageChange(Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onPageChangeListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onPageError(Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onPageErrorListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageErrorListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onPageScroll(Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onPageScrollListener:Lcom/intsig/camscanner/pdfengine/listener/OnPageScrollListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onRender(Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onRenderListener:Lcom/intsig/camscanner/pdfengine/listener/OnRenderListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onTap(Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->onTapListener:Lcom/intsig/camscanner/pdfengine/listener/OnTapListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public pageFitPolicy(Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageFitPolicy:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public pageFling(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageFling:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public pageSnap(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageSnap:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public varargs pages([I)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->pageNumbers:[I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public password(Ljava/lang/String;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->password:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public scrollHandle(Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->scrollHandle:Lcom/intsig/camscanner/pdfengine/srcoll/ScrollHandle;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public spacing(I)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->spacing:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public swipeHorizontal(Z)Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pdfengine/PDFView$Configurator;->swipeHorizontal:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

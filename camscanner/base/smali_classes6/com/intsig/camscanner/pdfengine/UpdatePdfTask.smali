.class public Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;
.super Landroid/os/AsyncTask;
.source "UpdatePdfTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pdfengine/UpdatePdfTask$UpdateOnPdfCreateListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "UpdatePdfTask"


# instance fields
.field mContext:Landroid/content/Context;

.field private mResultCode:I

.field mUpdatePdfDlg:Lcom/intsig/app/BaseProgressDialog;

.field progress:I

.field final step_create:I

.field final step_find:I

.field final step_qury:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x5

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->step_qury:I

    .line 6
    .line 7
    const/16 v0, 0x19

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->step_find:I

    .line 10
    .line 11
    const/16 v0, 0x46

    .line 12
    .line 13
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->step_create:I

    .line 14
    .line 15
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->SUCCESS_CREATE:I

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mResultCode:I

    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private findUpdatedDocs()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    .line 7
    .line 8
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 13
    .line 14
    const-string v1, "state"

    .line 15
    .line 16
    const-string v4, "_data"

    .line 17
    .line 18
    const-string v5, "_id"

    .line 19
    .line 20
    filled-new-array {v5, v1, v4}, [Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v4

    .line 24
    const/4 v5, 0x0

    .line 25
    const/4 v6, 0x0

    .line 26
    const/4 v7, 0x0

    .line 27
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    const/4 v2, 0x5

    .line 32
    iput v2, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->progress:I

    .line 33
    .line 34
    const/4 v3, 0x1

    .line 35
    new-array v4, v3, [Ljava/lang/Integer;

    .line 36
    .line 37
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    const/4 v5, 0x0

    .line 42
    aput-object v2, v4, v5

    .line 43
    .line 44
    invoke-virtual {p0, v4}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    if-eqz v1, :cond_3

    .line 48
    .line 49
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    if-lez v2, :cond_3

    .line 54
    .line 55
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    new-instance v4, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v6, "findUpdatedDocs = "

    .line 65
    .line 66
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 70
    .line 71
    .line 72
    move-result v6

    .line 73
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    const-string v6, ", step = "

    .line 77
    .line 78
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    const/high16 v6, 0x41c80000    # 25.0f

    .line 82
    .line 83
    int-to-float v7, v2

    .line 84
    div-float/2addr v6, v7

    .line 85
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 86
    .line 87
    .line 88
    move-result v7

    .line 89
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v4

    .line 96
    const-string v7, "UpdatePdfTask"

    .line 97
    .line 98
    invoke-static {v7, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 102
    .line 103
    .line 104
    move-result v4

    .line 105
    new-array v4, v4, [Ljava/lang/String;

    .line 106
    .line 107
    const/4 v7, 0x0

    .line 108
    :goto_0
    if-ge v7, v2, :cond_2

    .line 109
    .line 110
    invoke-interface {v1, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 111
    .line 112
    .line 113
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    .line 114
    .line 115
    .line 116
    move-result-wide v8

    .line 117
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    .line 118
    .line 119
    .line 120
    move-result v10

    .line 121
    const/4 v11, 0x2

    .line 122
    invoke-interface {v1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v11

    .line 126
    aput-object v11, v4, v7

    .line 127
    .line 128
    if-eq v10, v3, :cond_0

    .line 129
    .line 130
    invoke-static {v11}, Lcom/intsig/camscanner/util/Util;->〇〇o8(Ljava/lang/String;)Z

    .line 131
    .line 132
    .line 133
    move-result v10

    .line 134
    if-nez v10, :cond_1

    .line 135
    .line 136
    :cond_0
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 137
    .line 138
    .line 139
    move-result-object v8

    .line 140
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    :cond_1
    iget v8, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->progress:I

    .line 144
    .line 145
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 146
    .line 147
    .line 148
    move-result v9

    .line 149
    add-int/2addr v8, v9

    .line 150
    iput v8, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->progress:I

    .line 151
    .line 152
    new-array v9, v3, [Ljava/lang/Integer;

    .line 153
    .line 154
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 155
    .line 156
    .line 157
    move-result-object v8

    .line 158
    aput-object v8, v9, v5

    .line 159
    .line 160
    invoke-virtual {p0, v9}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 161
    .line 162
    .line 163
    add-int/lit8 v7, v7, 0x1

    .line 164
    .line 165
    goto :goto_0

    .line 166
    :cond_2
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->getValidatePaths([Ljava/lang/String;)[Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v2

    .line 170
    if-eqz v2, :cond_4

    .line 171
    .line 172
    array-length v3, v2

    .line 173
    if-lez v3, :cond_4

    .line 174
    .line 175
    iget-object v3, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    .line 176
    .line 177
    invoke-static {v3, v2}, Lcom/intsig/camscanner/service/MediaScannerNotifier;->〇080(Landroid/content/Context;[Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    goto :goto_1

    .line 181
    :cond_3
    new-array v2, v3, [Ljava/lang/Integer;

    .line 182
    .line 183
    const/16 v3, 0x1e

    .line 184
    .line 185
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 186
    .line 187
    .line 188
    move-result-object v3

    .line 189
    aput-object v3, v2, v5

    .line 190
    .line 191
    invoke-virtual {p0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 192
    .line 193
    .line 194
    :cond_4
    :goto_1
    if-eqz v1, :cond_5

    .line 195
    .line 196
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 197
    .line 198
    .line 199
    :cond_5
    return-object v0
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private getValidatePaths([Ljava/lang/String;)[Ljava/lang/String;
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_1

    .line 3
    .line 4
    array-length v1, p1

    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_2

    .line 8
    .line 9
    aget-object v4, p1, v2

    .line 10
    .line 11
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v4

    .line 15
    if-nez v4, :cond_0

    .line 16
    .line 17
    add-int/lit8 v3, v3, 0x1

    .line 18
    .line 19
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    const/4 v3, 0x0

    .line 23
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v2, "findUpdatedDocs validateCount = "

    .line 29
    .line 30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    const-string v2, "UpdatePdfTask"

    .line 41
    .line 42
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    if-lez v3, :cond_4

    .line 46
    .line 47
    new-array v1, v3, [Ljava/lang/String;

    .line 48
    .line 49
    array-length v2, p1

    .line 50
    const/4 v3, 0x0

    .line 51
    :goto_1
    if-ge v0, v2, :cond_5

    .line 52
    .line 53
    aget-object v4, p1, v0

    .line 54
    .line 55
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 56
    .line 57
    .line 58
    move-result v5

    .line 59
    if-nez v5, :cond_3

    .line 60
    .line 61
    aput-object v4, v1, v3

    .line 62
    .line 63
    add-int/lit8 v3, v3, 0x1

    .line 64
    .line 65
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_4
    const/4 v1, 0x0

    .line 69
    :cond_5
    return-object v1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private updatePDF(Ljava/util/ArrayList;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->o〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    sget p1, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_SAVE_PDF_FILE:I

    .line 9
    .line 10
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mResultCode:I

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    const/16 v0, 0x64

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    if-eqz p1, :cond_2

    .line 17
    .line 18
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    if-lez v3, :cond_2

    .line 23
    .line 24
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    if-eqz v4, :cond_1

    .line 33
    .line 34
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v4

    .line 38
    check-cast v4, Ljava/lang/Long;

    .line 39
    .line 40
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    .line 41
    .line 42
    .line 43
    move-result-wide v5

    .line 44
    const/4 v7, 0x0

    .line 45
    iget-object v8, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    .line 46
    .line 47
    const/4 v9, 0x0

    .line 48
    const/4 v10, 0x5

    .line 49
    new-instance v11, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask$UpdateOnPdfCreateListener;

    .line 50
    .line 51
    const/4 v4, 0x0

    .line 52
    invoke-direct {v11, p0, v4}, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask$UpdateOnPdfCreateListener;-><init>(Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;Lcom/intsig/camscanner/pdfengine/〇〇888;)V

    .line 53
    .line 54
    .line 55
    invoke-static/range {v5 .. v11}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    iget v4, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->progress:I

    .line 59
    .line 60
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 61
    .line 62
    .line 63
    move-result v5

    .line 64
    int-to-float v5, v5

    .line 65
    const/high16 v6, 0x428c0000    # 70.0f

    .line 66
    .line 67
    div-float/2addr v6, v5

    .line 68
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    .line 69
    .line 70
    .line 71
    move-result v5

    .line 72
    add-int/2addr v4, v5

    .line 73
    iput v4, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->progress:I

    .line 74
    .line 75
    new-array v5, v2, [Ljava/lang/Integer;

    .line 76
    .line 77
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 78
    .line 79
    .line 80
    move-result-object v4

    .line 81
    aput-object v4, v5, v1

    .line 82
    .line 83
    invoke-virtual {p0, v5}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_1
    new-array p1, v2, [Ljava/lang/Integer;

    .line 88
    .line 89
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    aput-object v0, p1, v1

    .line 94
    .line 95
    invoke-virtual {p0, p1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 96
    .line 97
    .line 98
    return v2

    .line 99
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    .line 100
    .line 101
    invoke-static {p1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇0(Landroid/content/Context;)I

    .line 102
    .line 103
    .line 104
    move-result p1

    .line 105
    const-string v3, "UpdatePdfTask"

    .line 106
    .line 107
    if-lez p1, :cond_3

    .line 108
    .line 109
    const-string p1, "updatePDF no pdf need update"

    .line 110
    .line 111
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    goto :goto_1

    .line 115
    :cond_3
    sget p1, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_ALL_PAGE_MISS:I

    .line 116
    .line 117
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mResultCode:I

    .line 118
    .line 119
    const-string p1, "updatePDF ERROR_ALL_PAGE_MISS"

    .line 120
    .line 121
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    :goto_1
    new-array p1, v2, [Ljava/lang/Integer;

    .line 125
    .line 126
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    aput-object v0, p1, v1

    .line 131
    .line 132
    invoke-virtual {p0, p1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 133
    .line 134
    .line 135
    return v1
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mResultCode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    const-string p1, "UpdatePdfTask"

    const-string v0, "doInBackground"

    .line 2
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->findUpdatedDocs()Ljava/util/ArrayList;

    move-result-object p1

    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->updatePDF(Ljava/util/ArrayList;)Z

    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    const-string p1, "onPostExecute"

    const-string v0, "UpdatePdfTask"

    .line 2
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mUpdatePdfDlg:Lcom/intsig/app/BaseProgressDialog;

    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 4
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5
    :goto_0
    iget p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mResultCode:I

    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->SUCCESS_CREATE:I

    if-ne p1, v0, :cond_0

    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    const v0, 0x7f1303bf

    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    goto :goto_2

    .line 7
    :cond_0
    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_EMPTY_DOC:I

    if-eq p1, v0, :cond_2

    sget v0, Lcom/intsig/camscanner/pdfengine/PDF_Util;->ERROR_ALL_PAGE_MISS:I

    if-ne p1, v0, :cond_1

    goto :goto_1

    .line 8
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    const v0, 0x7f131e4e

    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    goto :goto_2

    .line 9
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    const v0, 0x7f130490

    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    :goto_2
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .line 1
    const-string v0, "onPreExecute"

    .line 2
    .line 3
    const-string v1, "UpdatePdfTask"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    invoke-static {v0, v2}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mUpdatePdfDlg:Lcom/intsig/app/BaseProgressDialog;

    .line 16
    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mContext:Landroid/content/Context;

    .line 18
    .line 19
    const v3, 0x7f1303c8

    .line 20
    .line 21
    .line 22
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-virtual {v0, v2}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mUpdatePdfDlg:Lcom/intsig/app/BaseProgressDialog;

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mUpdatePdfDlg:Lcom/intsig/app/BaseProgressDialog;

    .line 36
    .line 37
    invoke-virtual {v0, v2}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mUpdatePdfDlg:Lcom/intsig/app/BaseProgressDialog;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 47
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 48
    .line 49
    .line 50
    :goto_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->mUpdatePdfDlg:Lcom/intsig/app/BaseProgressDialog;

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pdfengine/UpdatePdfTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.class public final Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "GuideVideoViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction;,
        Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOo〇8o008:Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private volatile transient O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

.field private o0:Z

.field private volatile transient o〇00O:Ljava/util/concurrent/atomic/AtomicBoolean;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/Channel<",
            "Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;

.field private final 〇0O:Lkotlinx/coroutines/flow/Flow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private volatile transient 〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->oOo〇8o008:Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 5

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o0:Z

    .line 6
    .line 7
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 14
    .line 15
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 16
    .line 17
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇00O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 21
    .line 22
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 23
    .line 24
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 25
    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 28
    .line 29
    sget-object v0, Lkotlinx/coroutines/channels/BufferOverflow;->SUSPEND:Lkotlinx/coroutines/channels/BufferOverflow;

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    const/4 v3, 0x5

    .line 33
    invoke-static {v1, v0, v2, v3, v2}, Lkotlinx/coroutines/channels/ChannelKt;->〇o00〇〇Oo(ILkotlinx/coroutines/channels/BufferOverflow;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lkotlinx/coroutines/channels/Channel;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 38
    .line 39
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->〇oo〇(Lkotlinx/coroutines/channels/ReceiveChannel;)Lkotlinx/coroutines/flow/Flow;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇0O:Lkotlinx/coroutines/flow/Flow;

    .line 44
    .line 45
    :try_start_0
    sget-object v0, Lkotlin/Result;->Companion:Lkotlin/Result$Companion;

    .line 46
    .line 47
    sget-object v0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoUtil;->〇080:Lcom/intsig/camscanner/guide/guidevideo/GuideVideoUtil;

    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoUtil;->Oo08()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-static {v1}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoUtil;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoUtil;->〇080(Ljava/util/ArrayList;)Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-static {v0}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    .line 67
    sget-object v1, Lkotlin/Result;->Companion:Lkotlin/Result$Companion;

    .line 68
    .line 69
    invoke-static {v0}, Lkotlin/ResultKt;->〇080(Ljava/lang/Throwable;)Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-static {v0}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    :goto_0
    invoke-static {v0}, Lkotlin/Result;->isSuccess-impl(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    const-string v2, "GuideVideoViewModel"

    .line 82
    .line 83
    if-eqz v1, :cond_0

    .line 84
    .line 85
    move-object v1, v0

    .line 86
    check-cast v1, Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

    .line 87
    .line 88
    new-instance v3, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v4, "get init native data = "

    .line 94
    .line 95
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    iput-object v1, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->OO:Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

    .line 109
    .line 110
    :cond_0
    invoke-static {v0}, Lkotlin/Result;->exceptionOrNull-impl(Ljava/lang/Object;)Ljava/lang/Throwable;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    if-eqz v0, :cond_1

    .line 115
    .line 116
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 121
    .line 122
    .line 123
    invoke-static {}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoUtil;->〇o〇()Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->OO:Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

    .line 128
    .line 129
    :cond_1
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final Oooo8o0〇()V
    .locals 8

    .line 1
    const-string v0, "GuideVideoViewModel"

    .line 2
    .line 3
    const-string v1, "checkAgainAfterSlideAnimEnd"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    const/4 v4, 0x0

    .line 17
    new-instance v5, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$checkAgainAfterSlideAnimEnd$1;

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    invoke-direct {v5, p0, v0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$checkAgainAfterSlideAnimEnd$1;-><init>(Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;Lkotlin/coroutines/Continuation;)V

    .line 21
    .line 22
    .line 23
    const/4 v6, 0x2

    .line 24
    const/4 v7, 0x0

    .line 25
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇8oOo8O(Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;ZZ)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 5
    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇08O〇00〇o:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$RefreshPage2WithServerData;

    .line 12
    .line 13
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$RefreshPage2WithServerData;-><init>(Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;)V

    .line 17
    .line 18
    .line 19
    invoke-interface {v0, v1}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    if-nez p2, :cond_0

    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 25
    .line 26
    sget-object p2, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$PerformPage2BtnBreathAnim;->〇080:Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$PerformPage2BtnBreathAnim;

    .line 27
    .line 28
    invoke-interface {p1, p2}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 32
    .line 33
    new-instance p2, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$PerformPage2SlideAnim;

    .line 34
    .line 35
    invoke-direct {p2, p3}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$PerformPage2SlideAnim;-><init>(Z)V

    .line 36
    .line 37
    .line 38
    invoke-interface {p1, p2}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const-string p1, "GuideVideoViewModel"

    .line 43
    .line 44
    const-string p2, "don\'t show slide anim or breath anim"

    .line 45
    .line 46
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇0OOo〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->video_guide_premium:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇8oOO88()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇O(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 8
    .line 9
    new-instance v1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$RefreshPage2WithNativeData;

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->OO:Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

    .line 12
    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    const-string v2, "mNativeData"

    .line 16
    .line 17
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    :cond_0
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$RefreshPage2WithNativeData;-><init>(Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;)V

    .line 22
    .line 23
    .line 24
    invoke-interface {v0, v1}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 28
    .line 29
    sget-object v1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$PerformPage2BtnBreathAnim;->〇080:Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$PerformPage2BtnBreathAnim;

    .line 30
    .line 31
    invoke-interface {v0, v1}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 35
    .line 36
    new-instance v1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$PerformPage2SlideAnim;

    .line 37
    .line 38
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$PerformPage2SlideAnim;-><init>(Z)V

    .line 39
    .line 40
    .line 41
    invoke-interface {v0, v1}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8o8O〇O()V
    .locals 4

    .line 1
    const-string v0, "tryOnceOnlyRefreshData"

    .line 2
    .line 3
    const-string v1, "GuideVideoViewModel"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇0OOo〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    iget v2, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;->guide_premium_style:I

    .line 15
    .line 16
    const/4 v3, 0x1

    .line 17
    if-eq v2, v3, :cond_0

    .line 18
    .line 19
    const-string v0, "guide_premium_style != 1"

    .line 20
    .line 21
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    return-void

    .line 25
    :cond_0
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;->style_1_price_info:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    const-string v1, "priceInfo"

    .line 30
    .line 31
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0, v0, v3, v1}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->O〇8oOo8O(Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;ZZ)V

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇8o8O〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final O8ooOoo〇()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/uploadinfo/UploadDeviceInfo;->〇080:Lcom/intsig/camscanner/uploadinfo/UploadDeviceInfo;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/uploadinfo/UploadDeviceInfo;->oO80()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "1"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v0, "0"

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O8〇o()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    const/4 v2, 0x0

    .line 9
    if-ne v0, v1, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇08O〇00〇o:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;

    .line 12
    .line 13
    if-eqz v0, :cond_4

    .line 14
    .line 15
    iget-boolean v1, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o0:Z

    .line 16
    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;->switch_on_product_id:Ljava/lang/String;

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;->switch_off_product_id:Ljava/lang/String;

    .line 23
    .line 24
    :goto_0
    move-object v2, v0

    .line 25
    goto :goto_2

    .line 26
    :cond_1
    if-nez v0, :cond_5

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->OO:Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

    .line 29
    .line 30
    if-nez v0, :cond_2

    .line 31
    .line 32
    const-string v0, "mNativeData"

    .line 33
    .line 34
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_2
    move-object v2, v0

    .line 39
    :goto_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o0:Z

    .line 40
    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;->getYear()Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeYearSubItem;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeYearSubItem;->getYear_product_id()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    goto :goto_0

    .line 52
    :cond_3
    invoke-virtual {v2}, Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;->getMonth()Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeMonthSubItem;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeMonthSubItem;->getMonth_product_id()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    goto :goto_0

    .line 61
    :cond_4
    :goto_2
    return-object v2

    .line 62
    :cond_5
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 63
    .line 64
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 65
    .line 66
    .line 67
    throw v0
    .line 68
.end method

.method public final O〇O〇oO()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇0OOo〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, ""

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;->guide_premium_style:I

    .line 19
    .line 20
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const-string v0, "8887"

    .line 26
    .line 27
    :goto_0
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final oO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo88o8O()V
    .locals 2

    .line 1
    const-string v0, "checkRefreshWithinSlideAnimDuration"

    .line 2
    .line 3
    const-string v1, "GuideVideoViewModel"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 9
    .line 10
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    const-string v0, "not in purchase page, so don\'t need to refresh"

    .line 17
    .line 18
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    const-string v0, "server data is already refreshed, so don\'t need to refresh"

    .line 31
    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇00O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 37
    .line 38
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-eqz v0, :cond_2

    .line 43
    .line 44
    const-string v0, "already had purchased at least once, so don\'t need to refresh"

    .line 45
    .line 46
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇8o8O〇O()V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final oo〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇8()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "getAttributingUserData()"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇〇808〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string v0, "1"

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const-string v0, "0"

    .line 15
    .line 16
    :goto_0
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o〇〇8080()V
    .locals 4

    .line 1
    const-string v0, "revertFreeTrailSwitcher"

    .line 2
    .line 3
    const-string v1, "GuideVideoViewModel"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o0:Z

    .line 9
    .line 10
    xor-int/lit8 v0, v0, 0x1

    .line 11
    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o0:Z

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇OOo8〇0:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇08O〇00〇o:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;

    .line 23
    .line 24
    if-eqz v0, :cond_2

    .line 25
    .line 26
    const-string v2, "refresh with sever data"

    .line 27
    .line 28
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 32
    .line 33
    new-instance v3, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$RefreshPage2WithServerData;

    .line 34
    .line 35
    invoke-direct {v3, v0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$RefreshPage2WithServerData;-><init>(Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;)V

    .line 36
    .line 37
    .line 38
    invoke-interface {v2, v3}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    const-string v0, "server data is null"

    .line 42
    .line 43
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->OO:Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;

    .line 48
    .line 49
    if-nez v0, :cond_1

    .line 50
    .line 51
    const-string v0, "mNativeData"

    .line 52
    .line 53
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    const/4 v0, 0x0

    .line 57
    :cond_1
    const-string v2, "refresh with native data"

    .line 58
    .line 59
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    iget-object v1, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 63
    .line 64
    new-instance v2, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$RefreshPage2WithNativeData;

    .line 65
    .line 66
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$RefreshPage2WithNativeData;-><init>(Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoNativeItem;)V

    .line 67
    .line 68
    .line 69
    invoke-interface {v1, v2}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    :cond_2
    :goto_0
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇O00()V
    .locals 5

    .line 1
    const-string v0, "checkRefreshPurchasePage2"

    .line 2
    .line 3
    const-string v1, "GuideVideoViewModel"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇0OOo〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const/4 v3, 0x0

    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const/4 v4, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v4, 0x0

    .line 24
    :goto_0
    if-ne v4, v2, :cond_2

    .line 25
    .line 26
    const-string v0, "videoGuidePremium is null"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇8oOO88()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    const-string v0, "use old 260ms slide anim\uff0cuse native data to refresh"

    .line 38
    .line 39
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇O(Z)V

    .line 43
    .line 44
    .line 45
    return-void

    .line 46
    :cond_1
    const-string v0, "Show slide 1s anim with native data first"

    .line 47
    .line 48
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇O(Z)V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->Oooo8o0〇()V

    .line 55
    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_2
    if-nez v4, :cond_6

    .line 59
    .line 60
    const-string v4, "videoGuidePremium not null"

    .line 61
    .line 62
    invoke-static {v1, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    iget v4, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;->is_show_guide:I

    .line 66
    .line 67
    if-nez v4, :cond_3

    .line 68
    .line 69
    const-string v0, "is_show_guide is 0, so just open main page"

    .line 70
    .line 71
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇080OO8〇0:Lkotlinx/coroutines/channels/Channel;

    .line 75
    .line 76
    sget-object v1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$JustOpenMainPage;->〇080:Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction$JustOpenMainPage;

    .line 77
    .line 78
    invoke-interface {v0, v1}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    return-void

    .line 82
    :cond_3
    iget v4, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;->guide_premium_style:I

    .line 83
    .line 84
    if-eq v4, v2, :cond_4

    .line 85
    .line 86
    const-string v0, "guide_premium_style != 1"

    .line 87
    .line 88
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇O(Z)V

    .line 92
    .line 93
    .line 94
    return-void

    .line 95
    :cond_4
    iget-object v4, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;->style_1_price_info:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;

    .line 96
    .line 97
    if-nez v4, :cond_5

    .line 98
    .line 99
    const-string v0, "style_1_price_info is null"

    .line 100
    .line 101
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇O(Z)V

    .line 105
    .line 106
    .line 107
    return-void

    .line 108
    :cond_5
    const-string v4, "use style_1_price_info to refresh"

    .line 109
    .line 110
    invoke-static {v1, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremium;->style_1_price_info:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;

    .line 114
    .line 115
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 116
    .line 117
    .line 118
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇8oOO88()Z

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    xor-int/2addr v1, v2

    .line 123
    invoke-direct {p0, v0, v3, v1}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->O〇8oOo8O(Lcom/intsig/comm/purchase/entity/QueryProductsResult$VideoGuidePremiumBanner;ZZ)V

    .line 124
    .line 125
    .line 126
    :cond_6
    :goto_1
    return-void
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->o〇00O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oo〇()Lkotlinx/coroutines/flow/Flow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel$GuideVideoAction;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoViewModel;->〇0O:Lkotlinx/coroutines/flow/Flow;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇〇0〇〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$AiPromotion;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->ai_promotion:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AiPromotion;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "GuidePurchaseHandleScrollImageFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic o〇00O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇08O〇00〇o:Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private 〇OOo8〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->o〇00O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private final O0〇0()V
    .locals 9

    .line 1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const-string v1, "getDefault().country"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    const-string v2, "getDefault()"

    .line 19
    .line 20
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "this as java.lang.String).toLowerCase(locale)"

    .line 28
    .line 29
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    const/4 v2, 0x1

    .line 37
    sub-int/2addr v1, v2

    .line 38
    const/4 v3, 0x0

    .line 39
    const/4 v4, 0x0

    .line 40
    const/4 v5, 0x0

    .line 41
    :goto_0
    if-gt v4, v1, :cond_5

    .line 42
    .line 43
    if-nez v5, :cond_0

    .line 44
    .line 45
    move v6, v4

    .line 46
    goto :goto_1

    .line 47
    :cond_0
    move v6, v1

    .line 48
    :goto_1
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    .line 49
    .line 50
    .line 51
    move-result v6

    .line 52
    const/16 v7, 0x20

    .line 53
    .line 54
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->oO80(II)I

    .line 55
    .line 56
    .line 57
    move-result v6

    .line 58
    if-gtz v6, :cond_1

    .line 59
    .line 60
    const/4 v6, 0x1

    .line 61
    goto :goto_2

    .line 62
    :cond_1
    const/4 v6, 0x0

    .line 63
    :goto_2
    if-nez v5, :cond_3

    .line 64
    .line 65
    if-nez v6, :cond_2

    .line 66
    .line 67
    const/4 v5, 0x1

    .line 68
    goto :goto_0

    .line 69
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_3
    if-nez v6, :cond_4

    .line 73
    .line 74
    goto :goto_3

    .line 75
    :cond_4
    add-int/lit8 v1, v1, -0x1

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_5
    :goto_3
    add-int/2addr v1, v2

    .line 79
    invoke-interface {v0, v4, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    const/16 v4, 0xc50

    .line 92
    .line 93
    const-string v5, "id"

    .line 94
    .line 95
    if-eq v1, v4, :cond_12

    .line 96
    .line 97
    const/16 v4, 0xccc

    .line 98
    .line 99
    if-eq v1, v4, :cond_10

    .line 100
    .line 101
    const/16 v4, 0xd1b

    .line 102
    .line 103
    if-eq v1, v4, :cond_e

    .line 104
    .line 105
    const/16 v4, 0xd2b

    .line 106
    .line 107
    if-eq v1, v4, :cond_d

    .line 108
    .line 109
    const/16 v4, 0xd67

    .line 110
    .line 111
    if-eq v1, v4, :cond_b

    .line 112
    .line 113
    const/16 v4, 0xe9e

    .line 114
    .line 115
    if-eq v1, v4, :cond_a

    .line 116
    .line 117
    const/16 v4, 0xdab

    .line 118
    .line 119
    if-eq v1, v4, :cond_8

    .line 120
    .line 121
    const/16 v4, 0xdac

    .line 122
    .line 123
    if-eq v1, v4, :cond_6

    .line 124
    .line 125
    goto :goto_4

    .line 126
    :cond_6
    const-string v1, "my"

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    if-nez v1, :cond_7

    .line 133
    .line 134
    goto :goto_4

    .line 135
    :cond_7
    const-string v1, "RM117"

    .line 136
    .line 137
    goto :goto_5

    .line 138
    :cond_8
    const-string v1, "mx"

    .line 139
    .line 140
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    move-result v1

    .line 144
    if-nez v1, :cond_9

    .line 145
    .line 146
    goto :goto_4

    .line 147
    :cond_9
    const-string v1, "MXN88"

    .line 148
    .line 149
    goto :goto_5

    .line 150
    :cond_a
    const-string v1, "us"

    .line 151
    .line 152
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 153
    .line 154
    .line 155
    move-result v1

    .line 156
    if-eqz v1, :cond_13

    .line 157
    .line 158
    const-string v1, "$39.99/year"

    .line 159
    .line 160
    goto :goto_5

    .line 161
    :cond_b
    const-string v1, "kr"

    .line 162
    .line 163
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 164
    .line 165
    .line 166
    move-result v1

    .line 167
    if-nez v1, :cond_c

    .line 168
    .line 169
    goto :goto_4

    .line 170
    :cond_c
    const-string v1, "\u20a960,000"

    .line 171
    .line 172
    goto :goto_5

    .line 173
    :cond_d
    const-string v1, "it"

    .line 174
    .line 175
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    move-result v1

    .line 179
    if-nez v1, :cond_11

    .line 180
    .line 181
    goto :goto_4

    .line 182
    :cond_e
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    if-nez v1, :cond_f

    .line 187
    .line 188
    goto :goto_4

    .line 189
    :cond_f
    const-string v1, "Rp 22ribu/bulan"

    .line 190
    .line 191
    goto :goto_5

    .line 192
    :cond_10
    const-string v1, "fr"

    .line 193
    .line 194
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 195
    .line 196
    .line 197
    move-result v1

    .line 198
    if-nez v1, :cond_11

    .line 199
    .line 200
    goto :goto_4

    .line 201
    :cond_11
    const-string v1, "\u20ac 32.99"

    .line 202
    .line 203
    goto :goto_5

    .line 204
    :cond_12
    const-string v1, "br"

    .line 205
    .line 206
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 207
    .line 208
    .line 209
    move-result v1

    .line 210
    if-nez v1, :cond_14

    .line 211
    .line 212
    :cond_13
    :goto_4
    const-string v1, ""

    .line 213
    .line 214
    goto :goto_5

    .line 215
    :cond_14
    const-string v1, "R$99/ano"

    .line 216
    .line 217
    :goto_5
    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 218
    .line 219
    .line 220
    move-result v0

    .line 221
    const-string v4, "format(format, *args)"

    .line 222
    .line 223
    const/4 v5, 0x2

    .line 224
    const/4 v6, 0x0

    .line 225
    const-string v7, "3"

    .line 226
    .line 227
    if-eqz v0, :cond_17

    .line 228
    .line 229
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oO〇oo()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 230
    .line 231
    .line 232
    move-result-object v0

    .line 233
    if-eqz v0, :cond_15

    .line 234
    .line 235
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;->〇0O:Landroid/widget/TextView;

    .line 236
    .line 237
    :cond_15
    if-nez v6, :cond_16

    .line 238
    .line 239
    goto :goto_6

    .line 240
    :cond_16
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 241
    .line 242
    const v0, 0x7f1310c3

    .line 243
    .line 244
    .line 245
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 246
    .line 247
    .line 248
    move-result-object v0

    .line 249
    const-string v8, "getString(R.string.cs_619_guide__freemon)"

    .line 250
    .line 251
    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    .line 253
    .line 254
    new-array v8, v5, [Ljava/lang/Object;

    .line 255
    .line 256
    aput-object v7, v8, v3

    .line 257
    .line 258
    aput-object v1, v8, v2

    .line 259
    .line 260
    invoke-static {v8, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 261
    .line 262
    .line 263
    move-result-object v1

    .line 264
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 265
    .line 266
    .line 267
    move-result-object v0

    .line 268
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    .line 273
    .line 274
    goto :goto_6

    .line 275
    :cond_17
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oO〇oo()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 276
    .line 277
    .line 278
    move-result-object v0

    .line 279
    if-eqz v0, :cond_18

    .line 280
    .line 281
    iget-object v6, v0, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;->〇0O:Landroid/widget/TextView;

    .line 282
    .line 283
    :cond_18
    if-nez v6, :cond_19

    .line 284
    .line 285
    goto :goto_6

    .line 286
    :cond_19
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 287
    .line 288
    const v0, 0x7f1310c4

    .line 289
    .line 290
    .line 291
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 292
    .line 293
    .line 294
    move-result-object v0

    .line 295
    const-string v8, "getString(R.string.cs_619_guide__freeyear)"

    .line 296
    .line 297
    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    .line 299
    .line 300
    new-array v8, v5, [Ljava/lang/Object;

    .line 301
    .line 302
    aput-object v7, v8, v3

    .line 303
    .line 304
    aput-object v1, v8, v2

    .line 305
    .line 306
    invoke-static {v8, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 307
    .line 308
    .line 309
    move-result-object v1

    .line 310
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 311
    .line 312
    .line 313
    move-result-object v0

    .line 314
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 315
    .line 316
    .line 317
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    .line 319
    .line 320
    :goto_6
    return-void
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private static final o00〇88〇08(Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oooO888()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o880()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const-string v2, "key_is_go_to_new_purchase_page"

    .line 9
    .line 10
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->〇OOo8〇0:Z

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->〇〇o0〇8()V

    .line 19
    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    new-array v2, v0, [Landroid/view/View;

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oO〇oo()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    const/4 v4, 0x0

    .line 29
    if-eqz v3, :cond_1

    .line 30
    .line 31
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;->O8o08O8O:Landroid/widget/RelativeLayout;

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_1
    move-object v3, v4

    .line 35
    :goto_1
    aput-object v3, v2, v1

    .line 36
    .line 37
    invoke-virtual {p0, v2}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 38
    .line 39
    .line 40
    new-array v0, v0, [Landroid/view/View;

    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oO〇oo()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    if-eqz v2, :cond_2

    .line 47
    .line 48
    iget-object v4, v2, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 49
    .line 50
    :cond_2
    aput-object v4, v0, v1

    .line 51
    .line 52
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oO〇oo()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->o〇00O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oooO888()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/MainPageRoute;->o〇0(Landroid/content/Context;)Landroid/content/Intent;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 23
    .line 24
    .line 25
    :cond_1
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->o00〇88〇08(Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇〇o0〇8()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->O0〇0()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oO〇oo()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;->O8o08O8O:Landroid/widget/RelativeLayout;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    move-object v1, v0

    .line 15
    const v2, 0x3f666666    # 0.9f

    .line 16
    .line 17
    .line 18
    const-wide/16 v3, 0x7d0

    .line 19
    .line 20
    const/4 v5, -0x1

    .line 21
    const/4 v6, 0x0

    .line 22
    invoke-static/range {v1 .. v6}, Lcom/intsig/utils/AnimateUtils;->〇〇888(Landroid/view/View;FJILandroid/animation/Animator$AnimatorListener;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p1, v0

    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oO〇oo()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;->O8o08O8O:Landroid/widget/RelativeLayout;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    goto :goto_1

    .line 33
    :cond_1
    move-object v1, v0

    .line 34
    :goto_1
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    const-string v2, "GuidePurchaseHandleScrollImageFragment"

    .line 39
    .line 40
    if-eqz v1, :cond_3

    .line 41
    .line 42
    const-string p1, "purchase year"

    .line 43
    .line 44
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->o0:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 48
    .line 49
    if-nez p1, :cond_2

    .line 50
    .line 51
    const-string p1, "csPurchaseHelper"

    .line 52
    .line 53
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_2
    move-object v0, p1

    .line 58
    :goto_2
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O8O〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;)V

    .line 63
    .line 64
    .line 65
    goto :goto_3

    .line 66
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oO〇oo()Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    if-eqz v1, :cond_4

    .line 71
    .line 72
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentGuideGpPurchaseLayoutBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 73
    .line 74
    if-eqz v1, :cond_4

    .line 75
    .line 76
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    :cond_4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    if-eqz p1, :cond_6

    .line 89
    .line 90
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    instance-of p1, p1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;

    .line 95
    .line 96
    if-eqz p1, :cond_5

    .line 97
    .line 98
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->oooO888()V

    .line 99
    .line 100
    .line 101
    goto :goto_3

    .line 102
    :cond_5
    const-string p1, " activity is not GuideVideoActivity"

    .line 103
    .line 104
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    :cond_6
    :goto_3
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->o880()V

    .line 2
    .line 3
    .line 4
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 5
    .line 6
    invoke-direct {p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 7
    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSGuidePremium:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 10
    .line 11
    iput-object v0, p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 14
    .line 15
    iput-object v0, p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 18
    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-direct {v0, v1, p1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;->o0:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 27
    .line 28
    new-instance p1, L〇o〇o/〇080;

    .line 29
    .line 30
    invoke-direct {p1, p0}, L〇o〇o/〇080;-><init>(Lcom/intsig/camscanner/guide/fragment/GuidePurchaseHandleScrollImageFragment;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇O〇80o08O(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onStart()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "type"

    .line 5
    .line 6
    const-string v1, "page_one"

    .line 7
    .line 8
    const-string v2, "CSGuide"

    .line 9
    .line 10
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d02e3

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

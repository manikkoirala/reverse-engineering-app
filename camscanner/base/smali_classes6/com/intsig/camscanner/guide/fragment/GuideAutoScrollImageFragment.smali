.class public final Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "GuideAutoScrollImageFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;,
        Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$GuideCnAutoScrollAdapter;,
        Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8o08O8O:Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇080OO8〇0:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private OO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:[Landroid/widget/ImageView;

.field private final o〇00O:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:I

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇080OO8〇0:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->O8o08O8O:Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->OO:Ljava/util/List;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 12
    .line 13
    const-class v2, Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;

    .line 14
    .line 15
    const/4 v4, 0x0

    .line 16
    const/4 v5, 0x4

    .line 17
    const/4 v6, 0x0

    .line 18
    move-object v1, v0

    .line 19
    move-object v3, p0

    .line 20
    invoke-direct/range {v1 .. v6}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o〇00O:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O0〇0(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;)Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$getSkipToLastWrapper$1;

    .line 2
    .line 3
    invoke-direct {v0, p0, p2, p1}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$getSkipToLastWrapper$1;-><init>(Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o00〇88〇08(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->OO:Ljava/util/List;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/ListUtils;->〇080(Ljava/util/List;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    rem-int/2addr p1, v0

    .line 8
    return p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o880(Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/hearcnl/HearCnlFragment;->o〇00O:Lcom/intsig/camscanner/guide/hearcnl/HearCnlFragment$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/hearcnl/HearCnlFragment$Companion;->〇080()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/hearcnl/HearCnlFragment$Companion;->〇o〇()Lcom/intsig/camscanner/guide/hearcnl/HearCnlFragment;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    new-instance v1, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$checkShowHearCnlPage$1;

    .line 14
    .line 15
    invoke-direct {v1, p2}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$checkShowHearCnlPage$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/guide/hearcnl/HearCnlFragment;->〇088O(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;)Lcom/intsig/camscanner/guide/hearcnl/HearCnlFragment;

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;->〇oO88o(Landroidx/fragment/app/Fragment;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oO〇oo(Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇8〇OOoooo(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oooO888(Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o〇0〇o(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇0〇o(Ljava/lang/String;)V
    .locals 4

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v0, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v1, Landroid/util/Pair;

    .line 5
    .line 6
    const-string v2, "type"

    .line 7
    .line 8
    const-string v3, "guide_premium"

    .line 9
    .line 10
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    aput-object v1, v0, v2

    .line 15
    .line 16
    new-instance v1, Landroid/util/Pair;

    .line 17
    .line 18
    const-string v2, "scheme"

    .line 19
    .line 20
    const-string v3, "selfsearch_guide"

    .line 21
    .line 22
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    const/4 v2, 0x1

    .line 26
    aput-object v1, v0, v2

    .line 27
    .line 28
    new-instance v1, Landroid/util/Pair;

    .line 29
    .line 30
    const-string v2, "keys"

    .line 31
    .line 32
    const-string v3, "new_advertise_cn_pop"

    .line 33
    .line 34
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 35
    .line 36
    .line 37
    const/4 v2, 0x2

    .line 38
    aput-object v1, v0, v2

    .line 39
    .line 40
    new-instance v1, Landroid/util/Pair;

    .line 41
    .line 42
    const-string v2, "keys_config"

    .line 43
    .line 44
    invoke-direct {v1, v2, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    const/4 p1, 0x3

    .line 48
    aput-object v1, v0, p1

    .line 49
    .line 50
    const-string p1, "CSDevelopmentTool"

    .line 51
    .line 52
    const-string v1, "guide_config"

    .line 53
    .line 54
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇8〇OOoooo(I)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o00〇88〇08(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o0:[Landroid/widget/ImageView;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget v2, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇OOo8〇0:I

    .line 11
    .line 12
    aget-object v0, v0, v2

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    move-object v0, v1

    .line 16
    :goto_0
    if-nez v0, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    const/4 v2, 0x1

    .line 20
    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 21
    .line 22
    .line 23
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o0:[Landroid/widget/ImageView;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    aget-object v1, v0, p1

    .line 28
    .line 29
    :cond_2
    if-nez v1, :cond_3

    .line 30
    .line 31
    goto :goto_2

    .line 32
    :cond_3
    const/4 v0, 0x0

    .line 33
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 34
    .line 35
    .line 36
    :goto_2
    iput p1, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇OOo8〇0:I

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇O8oOo0(Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o880(Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o〇00O:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇080OO8〇0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p1, v0

    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;->OO:Landroid/widget/TextView;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    :cond_1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-eqz p1, :cond_4

    .line 37
    .line 38
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    instance-of p1, p1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;

    .line 43
    .line 44
    const-string v0, "GuideAutoScrollImageFragment"

    .line 45
    .line 46
    if-eqz p1, :cond_3

    .line 47
    .line 48
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.guide.guidevideo.GuideVideoActivity"

    .line 53
    .line 54
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    check-cast p1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;

    .line 58
    .line 59
    sget-object v1, Lcom/intsig/camscanner/guide/dropchannel/DropCnlShowConfiguration;->〇080:Lcom/intsig/camscanner/guide/dropchannel/DropCnlShowConfiguration;

    .line 60
    .line 61
    const/4 v2, 0x1

    .line 62
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/guide/dropchannel/DropCnlShowConfiguration;->oO80(Z)Z

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-eqz v1, :cond_2

    .line 67
    .line 68
    const-string v1, "show DropCnlConfigFragment from GuideAutoScrollImageFragment"

    .line 69
    .line 70
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-static {v2}, Lcom/intsig/camscanner/guide/tracker/CsGuideTracker$Normal;->〇o〇(Z)V

    .line 74
    .line 75
    .line 76
    sget-object v0, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/AfterScanPremiumManager;->〇080:Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/AfterScanPremiumManager;

    .line 77
    .line 78
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/AfterScanPremiumManager;->〇〇888()V

    .line 79
    .line 80
    .line 81
    sget-object v0, Lcom/intsig/camscanner/guide/dropchannel/DropCnlConfigFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/guide/dropchannel/DropCnlConfigFragment$Companion;

    .line 82
    .line 83
    const-string v1, "FROM_GUIDE"

    .line 84
    .line 85
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/guide/dropchannel/DropCnlConfigFragment$Companion;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/guide/dropchannel/DropCnlConfigFragment;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;->O〇080〇o0()Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    invoke-direct {p0, v1, p1}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->O0〇0(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;)Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/guide/dropchannel/DropCnlConfigFragment;->O〇〇O80o8(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;)Lcom/intsig/camscanner/guide/dropchannel/DropCnlConfigFragment;

    .line 98
    .line 99
    .line 100
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;->〇oO88o(Landroidx/fragment/app/Fragment;)V

    .line 101
    .line 102
    .line 103
    const-string p1, "1"

    .line 104
    .line 105
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o〇0〇o(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    goto :goto_1

    .line 109
    :cond_2
    new-instance v0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$dealClickAction$1;

    .line 110
    .line 111
    invoke-direct {v0, p1, p0}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$dealClickAction$1;-><init>(Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;)V

    .line 112
    .line 113
    .line 114
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o880(Lcom/intsig/camscanner/guide/guidevideo/GuideVideoActivity;Lkotlin/jvm/functions/Function0;)V

    .line 115
    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_3
    const-string p1, " activity is not GuideVideoActivity"

    .line 119
    .line 120
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    :cond_4
    :goto_1
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    const/4 p1, 0x1

    .line 2
    new-array v0, p1, [Landroid/view/View;

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;->OO:Landroid/widget/TextView;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v1, 0x0

    .line 14
    :goto_0
    const/4 v2, 0x0

    .line 15
    aput-object v1, v0, v2

    .line 16
    .line 17
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 18
    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    const/4 v1, 0x4

    .line 25
    new-array v1, v1, [Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;

    .line 26
    .line 27
    new-instance v3, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;

    .line 28
    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    const v4, 0x7f080eef

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    const v4, 0x7f080eee

    .line 36
    .line 37
    .line 38
    :goto_1
    const v5, 0x7f130be5

    .line 39
    .line 40
    .line 41
    const v6, 0x7f130be6

    .line 42
    .line 43
    .line 44
    invoke-direct {v3, v4, v5, v6}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;-><init>(III)V

    .line 45
    .line 46
    .line 47
    aput-object v3, v1, v2

    .line 48
    .line 49
    new-instance v3, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;

    .line 50
    .line 51
    if-eqz v0, :cond_2

    .line 52
    .line 53
    const v4, 0x7f080ef2

    .line 54
    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_2
    const v4, 0x7f080ef1

    .line 58
    .line 59
    .line 60
    :goto_2
    const v5, 0x7f130bf2

    .line 61
    .line 62
    .line 63
    const v6, 0x7f130bf4

    .line 64
    .line 65
    .line 66
    invoke-direct {v3, v4, v5, v6}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;-><init>(III)V

    .line 67
    .line 68
    .line 69
    aput-object v3, v1, p1

    .line 70
    .line 71
    new-instance v3, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;

    .line 72
    .line 73
    if-eqz v0, :cond_3

    .line 74
    .line 75
    const v4, 0x7f080ef5

    .line 76
    .line 77
    .line 78
    goto :goto_3

    .line 79
    :cond_3
    const v4, 0x7f080ef4

    .line 80
    .line 81
    .line 82
    :goto_3
    const v5, 0x7f130bee

    .line 83
    .line 84
    .line 85
    const v6, 0x7f130bef

    .line 86
    .line 87
    .line 88
    invoke-direct {v3, v4, v5, v6}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;-><init>(III)V

    .line 89
    .line 90
    .line 91
    const/4 v4, 0x2

    .line 92
    aput-object v3, v1, v4

    .line 93
    .line 94
    new-instance v3, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;

    .line 95
    .line 96
    if-eqz v0, :cond_4

    .line 97
    .line 98
    const v0, 0x7f080ef7

    .line 99
    .line 100
    .line 101
    goto :goto_4

    .line 102
    :cond_4
    const v0, 0x7f080ef6

    .line 103
    .line 104
    .line 105
    :goto_4
    const v4, 0x7f130908

    .line 106
    .line 107
    .line 108
    const v5, 0x7f13090a

    .line 109
    .line 110
    .line 111
    invoke-direct {v3, v0, v4, v5}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$PageData;-><init>(III)V

    .line 112
    .line 113
    .line 114
    const/4 v0, 0x3

    .line 115
    aput-object v3, v1, v0

    .line 116
    .line 117
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 118
    .line 119
    .line 120
    move-result-object v1

    .line 121
    iput-object v1, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->OO:Ljava/util/List;

    .line 122
    .line 123
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    const v3, 0x7f070597

    .line 128
    .line 129
    .line 130
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 131
    .line 132
    .line 133
    move-result v1

    .line 134
    iget-object v3, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->OO:Ljava/util/List;

    .line 135
    .line 136
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 137
    .line 138
    .line 139
    move-result v3

    .line 140
    new-array v3, v3, [Landroid/widget/ImageView;

    .line 141
    .line 142
    iput-object v3, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o0:[Landroid/widget/ImageView;

    .line 143
    .line 144
    iget-object v3, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->OO:Ljava/util/List;

    .line 145
    .line 146
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 147
    .line 148
    .line 149
    move-result v3

    .line 150
    const/4 v4, 0x0

    .line 151
    :goto_5
    if-ge v4, v3, :cond_8

    .line 152
    .line 153
    new-instance v5, Landroid/widget/ImageView;

    .line 154
    .line 155
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 156
    .line 157
    .line 158
    move-result-object v6

    .line 159
    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 160
    .line 161
    .line 162
    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    .line 163
    .line 164
    const/4 v7, -0x2

    .line 165
    invoke-direct {v6, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 166
    .line 167
    .line 168
    invoke-virtual {v5, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    .line 170
    .line 171
    iget-object v6, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->o0:[Landroid/widget/ImageView;

    .line 172
    .line 173
    if-eqz v6, :cond_5

    .line 174
    .line 175
    aput-object v5, v6, v4

    .line 176
    .line 177
    :cond_5
    const v6, 0x7f08043d

    .line 178
    .line 179
    .line 180
    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 181
    .line 182
    .line 183
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 184
    .line 185
    invoke-direct {v6, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 186
    .line 187
    .line 188
    iput v1, v6, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 189
    .line 190
    iput v1, v6, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 191
    .line 192
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;

    .line 193
    .line 194
    .line 195
    move-result-object v7

    .line 196
    if-eqz v7, :cond_6

    .line 197
    .line 198
    iget-object v7, v7, Lcom/intsig/camscanner/databinding/FragmentGuideAutoScrollImageBinding;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 199
    .line 200
    if-eqz v7, :cond_6

    .line 201
    .line 202
    invoke-virtual {v7, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    .line 204
    .line 205
    :cond_6
    if-nez v4, :cond_7

    .line 206
    .line 207
    invoke-virtual {v5, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 208
    .line 209
    .line 210
    :cond_7
    add-int/lit8 v4, v4, 0x1

    .line 211
    .line 212
    goto :goto_5

    .line 213
    :cond_8
    new-instance v1, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$GuideCnAutoScrollAdapter;

    .line 214
    .line 215
    iget-object v2, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->OO:Ljava/util/List;

    .line 216
    .line 217
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$GuideCnAutoScrollAdapter;-><init>(Ljava/util/List;)V

    .line 218
    .line 219
    .line 220
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 221
    .line 222
    const v3, 0x7f0a1a8c

    .line 223
    .line 224
    .line 225
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 226
    .line 227
    .line 228
    move-result-object v2

    .line 229
    check-cast v2, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;

    .line 230
    .line 231
    if-nez v2, :cond_9

    .line 232
    .line 233
    goto :goto_6

    .line 234
    :cond_9
    invoke-virtual {v2, v0}, Landroidx/viewpager/widget/ViewPager;->setOffscreenPageLimit(I)V

    .line 235
    .line 236
    .line 237
    :goto_6
    if-eqz v2, :cond_a

    .line 238
    .line 239
    const-wide/16 v3, 0x7d0

    .line 240
    .line 241
    invoke-virtual {v2, v3, v4}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setInterval(J)V

    .line 242
    .line 243
    .line 244
    :cond_a
    if-eqz v2, :cond_b

    .line 245
    .line 246
    sget-object v0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;->RIGHT:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;

    .line 247
    .line 248
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setDirection(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;)V

    .line 249
    .line 250
    .line 251
    :cond_b
    if-eqz v2, :cond_c

    .line 252
    .line 253
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setCycle(Z)V

    .line 254
    .line 255
    .line 256
    :cond_c
    if-eqz v2, :cond_d

    .line 257
    .line 258
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setStopScrollWhenTouch(Z)V

    .line 259
    .line 260
    .line 261
    :cond_d
    if-eqz v2, :cond_e

    .line 262
    .line 263
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setBorderAnimation(Z)V

    .line 264
    .line 265
    .line 266
    :cond_e
    if-nez v2, :cond_f

    .line 267
    .line 268
    goto :goto_7

    .line 269
    :cond_f
    invoke-virtual {v2, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 270
    .line 271
    .line 272
    :goto_7
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oO80()V

    .line 273
    .line 274
    .line 275
    iget p1, p0, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇08O〇00〇o:I

    .line 276
    .line 277
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;->〇8〇OOoooo(I)V

    .line 278
    .line 279
    .line 280
    new-instance p1, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$initialize$1;

    .line 281
    .line 282
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment$initialize$1;-><init>(Lcom/intsig/camscanner/guide/fragment/GuideAutoScrollImageFragment;)V

    .line 283
    .line 284
    .line 285
    invoke-virtual {v2, p1}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 286
    .line 287
    .line 288
    return-void
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/guide/tracker/CsGuideTracker$Normal;->〇080(Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d02de

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

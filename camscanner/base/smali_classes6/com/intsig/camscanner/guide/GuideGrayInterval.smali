.class public final enum Lcom/intsig/camscanner/guide/GuideGrayInterval;
.super Ljava/lang/Enum;
.source "GuideGrayInterval.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/guide/GuideGrayInterval;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final Companion:Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final FROM_PAGE_CN:Ljava/lang/String; = "CN"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final FROM_PAGE_GP:Ljava/lang/String; = "GP"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final enum GP_STYLE_ORIGIN_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final enum GP_STYLE_ORIGIN_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final enum GP_TEST_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final enum GP_TEST_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final enum GP_TEST_03:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final enum GP_TEST_04:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field private static final KEY_GRAY_INTERVAL_INT:Ljava/lang/String; = "KEY_GRAY_INTERVAL_INT"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_GRAY_INTERVAL_OPEN:Ljava/lang/String; = "KEY_GRAY_INTERVAL_OPEN"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_GRAY_INTERVAL_SERVER:Ljava/lang/String; = "KEY_GRAY_INTERVAL_SERVER"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final enum STYLE_NO_TRIAL_SPECIAL_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final enum STYLE_ORIGIN_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final enum STYLE_ORIGIN_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final enum STYLE_TRIAL_03:Lcom/intsig/camscanner/guide/GuideGrayInterval;

.field public static final TAG:Ljava/lang/String; = "GuideGrayInterval"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private serverInterval:I

.field private trackInterval:I


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/guide/GuideGrayInterval;
    .locals 3

    .line 1
    const/16 v0, 0xa

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->STYLE_ORIGIN_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->STYLE_ORIGIN_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->STYLE_TRIAL_03:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->STYLE_NO_TRIAL_SPECIAL_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_STYLE_ORIGIN_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_STYLE_ORIGIN_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_TEST_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_TEST_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_TEST_03:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_TEST_04:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    return-object v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 9

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 2
    .line 3
    const-string v1, "STYLE_ORIGIN_01"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2, v2, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->STYLE_ORIGIN_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 12
    .line 13
    const-string v1, "STYLE_ORIGIN_02"

    .line 14
    .line 15
    const/4 v3, 0x1

    .line 16
    invoke-direct {v0, v1, v3, v3, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->STYLE_ORIGIN_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 22
    .line 23
    const-string v1, "STYLE_TRIAL_03"

    .line 24
    .line 25
    const/4 v4, 0x2

    .line 26
    const/4 v5, 0x4

    .line 27
    invoke-direct {v0, v1, v4, v5, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->STYLE_TRIAL_03:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 31
    .line 32
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 33
    .line 34
    const-string v1, "STYLE_NO_TRIAL_SPECIAL_01"

    .line 35
    .line 36
    const/4 v6, 0x3

    .line 37
    const/16 v7, 0x9

    .line 38
    .line 39
    invoke-direct {v0, v1, v6, v7, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 40
    .line 41
    .line 42
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->STYLE_NO_TRIAL_SPECIAL_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 43
    .line 44
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 45
    .line 46
    const-string v1, "GP_STYLE_ORIGIN_01"

    .line 47
    .line 48
    invoke-direct {v0, v1, v5, v2, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 49
    .line 50
    .line 51
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_STYLE_ORIGIN_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 52
    .line 53
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 54
    .line 55
    const-string v1, "GP_STYLE_ORIGIN_02"

    .line 56
    .line 57
    const/4 v8, 0x5

    .line 58
    invoke-direct {v0, v1, v8, v3, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 59
    .line 60
    .line 61
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_STYLE_ORIGIN_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 62
    .line 63
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 64
    .line 65
    const-string v1, "GP_TEST_01"

    .line 66
    .line 67
    const/4 v3, 0x6

    .line 68
    invoke-direct {v0, v1, v3, v4, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 69
    .line 70
    .line 71
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_TEST_01:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 72
    .line 73
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 74
    .line 75
    const-string v1, "GP_TEST_02"

    .line 76
    .line 77
    const/4 v3, 0x7

    .line 78
    invoke-direct {v0, v1, v3, v6, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 79
    .line 80
    .line 81
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_TEST_02:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 82
    .line 83
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 84
    .line 85
    const-string v1, "GP_TEST_03"

    .line 86
    .line 87
    const/16 v3, 0x8

    .line 88
    .line 89
    invoke-direct {v0, v1, v3, v5, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 90
    .line 91
    .line 92
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_TEST_03:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 93
    .line 94
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 95
    .line 96
    const-string v1, "GP_TEST_04"

    .line 97
    .line 98
    invoke-direct {v0, v1, v7, v8, v2}, Lcom/intsig/camscanner/guide/GuideGrayInterval;-><init>(Ljava/lang/String;III)V

    .line 99
    .line 100
    .line 101
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->GP_TEST_04:Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 102
    .line 103
    invoke-static {}, Lcom/intsig/camscanner/guide/GuideGrayInterval;->$values()[Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->$VALUES:[Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 108
    .line 109
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;

    .line 110
    .line 111
    const/4 v1, 0x0

    .line 112
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 113
    .line 114
    .line 115
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->Companion:Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;

    .line 116
    .line 117
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->trackInterval:I

    .line 5
    .line 6
    iput p4, p0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->serverInterval:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final getGuideTrialInterval(Ljava/lang/String;)Lcom/intsig/camscanner/guide/GuideGrayInterval;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->Companion:Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;->Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final savedGrayInt(I)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->Companion:Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;->o〇0(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/guide/GuideGrayInterval;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/guide/GuideGrayInterval;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->$VALUES:[Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/guide/GuideGrayInterval;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getServerInterval()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->serverInterval:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTrackInterval()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->trackInterval:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setServerInterval(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->serverInterval:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setTrackInterval(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/guide/GuideGrayInterval;->trackInterval:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

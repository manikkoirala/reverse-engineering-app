.class public final Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "DropCnlProductProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;,
        Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$Companion;,
        Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlPayWayHolder;,
        Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlInnerProductHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8〇OO0〇0o:Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇oO〇〇8o:Ljava/lang/String;


# instance fields
.field private final O8o08O8O:Z

.field private OO〇00〇8oO:Ljava/lang/Boolean;

.field private oOo0:I

.field private oOo〇8o008:I

.field private final o〇00O:Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:I

.field private 〇0O:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->o8〇OO0〇0o:Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lkotlin/reflect/KClass;->O8()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 20
    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;Z)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "viewModel"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->o〇00O:Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;

    .line 10
    .line 11
    iput-boolean p2, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->O8o08O8O:Z

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    iput p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->〇080OO8〇0:I

    .line 22
    .line 23
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 24
    .line 25
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    const/16 v0, 0x30

    .line 30
    .line 31
    invoke-static {p2, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 32
    .line 33
    .line 34
    move-result p2

    .line 35
    sub-int/2addr p1, p2

    .line 36
    int-to-float p1, p1

    .line 37
    const/high16 p2, 0x40400000    # 3.0f

    .line 38
    .line 39
    div-float/2addr p1, p2

    .line 40
    float-to-int p1, p1

    .line 41
    iput p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->〇0O:I

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic O〇8O8〇008(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->oOo〇8o008:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o800o8O(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->oOo0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->oOo〇8o008:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8〇〇o(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->OO〇00〇8oO:Ljava/lang/Boolean;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇00(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->oOo0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇oo〇(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;)Ljava/lang/Boolean;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->OO〇00〇8oO:Ljava/lang/Boolean;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O8ooOoo〇(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;)V
    .locals 1
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p1, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;

    .line 12
    .line 13
    check-cast p2, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;

    .line 14
    .line 15
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;->〇〇〇0〇〇0(Ljava/util/List;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;->o〇8(Ljava/util/List;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;->o8(Ljava/util/List;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;->O8〇o()V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o〇()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;->〇00〇8(Z)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;->oO(Ljava/util/List;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;->〇o(Ljava/util/List;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 61
    .line 62
    .line 63
    move-result-object p2

    .line 64
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;->〇8(Ljava/util/List;)V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;

    .line 7
    .line 8
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string p2, "super.onCreateViewHolder\u2026arent, viewType).itemView"

    .line 15
    .line 16
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider$DropCnlProductHolder;-><init>(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO80()I
    .locals 1

    .line 1
    const v0, 0x7f0d03e1

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0000OOO()Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->o〇00O:Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->O8ooOoo〇(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇oOO8O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlProductProvider;->〇0O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

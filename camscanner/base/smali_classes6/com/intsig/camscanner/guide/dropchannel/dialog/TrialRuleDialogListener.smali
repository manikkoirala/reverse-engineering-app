.class public interface abstract Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;
.super Ljava/lang/Object;
.source "TrialRuleDialogListener.kt"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract onClose(J)V
.end method

.method public abstract onPurchase(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Ljava/lang/String;)V
    .param p1    # Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract onShow()V
.end method

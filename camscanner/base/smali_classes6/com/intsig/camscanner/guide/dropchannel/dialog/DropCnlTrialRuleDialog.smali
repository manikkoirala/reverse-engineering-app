.class public final Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;
.super Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;
.source "DropCnlTrialRuleDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8o:Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic oo8ooo8O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final o〇oO:Ljava/lang/String;


# instance fields
.field private O0O:Z

.field private final O88O:Lcom/intsig/util/CountdownTimer$OnCountdownListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o08O8O:Ljava/lang/String;

.field private OO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

.field private OO〇00〇8oO:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8oOOo:Z

.field private o8〇OO0〇0o:Z

.field private final oOO〇〇:Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$mHandler$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

.field private final oOo〇8o008:J

.field private ooo0〇〇O:Z

.field private o〇00O:I

.field private 〇080OO8〇0:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private 〇08O〇00〇o:Ljava/lang/String;

.field private 〇0O:Lcom/intsig/util/CountdownTimer;

.field private 〇8〇oO〇〇8o:Z

.field private 〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

.field private 〇O〇〇O8:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

.field private 〇o0O:Lcom/intsig/callback/DialogDismissListener;

.field private 〇〇08O:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oo8ooo8O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o8o:Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;

    .line 31
    .line 32
    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-interface {v0}, Lkotlin/reflect/KClass;->O8()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 23
    .line 24
    const/4 v0, 0x2

    .line 25
    iput v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇00O:I

    .line 26
    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 28
    .line 29
    .line 30
    move-result-wide v0

    .line 31
    iput-wide v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOo〇8o008:J

    .line 32
    .line 33
    const/4 v0, 0x1

    .line 34
    iput-boolean v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇8〇oO〇〇8o:Z

    .line 35
    .line 36
    iput-boolean v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->ooo0〇〇O:Z

    .line 37
    .line 38
    new-instance v0, L〇〇o0o/〇o00〇〇Oo;

    .line 39
    .line 40
    invoke-direct {v0, p0}, L〇〇o0o/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V

    .line 41
    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O88O:Lcom/intsig/util/CountdownTimer$OnCountdownListener;

    .line 44
    .line 45
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    new-instance v1, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$mHandler$1;

    .line 50
    .line 51
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$mHandler$1;-><init>(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/os/Looper;)V

    .line 52
    .line 53
    .line 54
    iput-object v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOO〇〇:Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$mHandler$1;

    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final Ooo8o()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f060095

    .line 6
    .line 7
    .line 8
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v1, 0x0

    .line 22
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oo(Landroidx/appcompat/widget/AppCompatTextView;I)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇8〇008(Landroidx/appcompat/widget/AppCompatTextView;)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    goto :goto_0

    .line 5
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 6
    .line 7
    .line 8
    :goto_0
    if-nez p1, :cond_1

    .line 9
    .line 10
    goto :goto_3

    .line 11
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O8o08O8O:Ljava/lang/String;

    .line 12
    .line 13
    if-eqz v1, :cond_3

    .line 14
    .line 15
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-nez v1, :cond_2

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_2
    const/4 v0, 0x0

    .line 23
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    .line 24
    .line 25
    const-string v0, "\u7acb\u5373\u5f00\u542f\u8ba2\u9605"

    .line 26
    .line 27
    goto :goto_2

    .line 28
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O8o08O8O:Ljava/lang/String;

    .line 29
    .line 30
    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    :goto_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    if-eqz v0, :cond_5

    .line 38
    .line 39
    new-instance v1, L〇〇o0o/Oo08;

    .line 40
    .line 41
    invoke-direct {v1, p1, p0}, L〇〇o0o/Oo08;-><init>(Landroidx/appcompat/widget/AppCompatTextView;Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 45
    .line 46
    .line 47
    :cond_5
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic o00〇88〇08(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇〇O80〇0o(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o88(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "click agreement"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object p0

    .line 21
    invoke-static {p0}, Lcom/intsig/camscanner/web/UrlUtil;->o0ooO(Landroid/content/Context;)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    invoke-static {p1, p0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇〇〇0(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oOoO8OO〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_4

    .line 7
    .line 8
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->trial_rule_1:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;

    .line 9
    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->trial_rule_2:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    :cond_1
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->trial_rule_3:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;

    .line 23
    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    :cond_2
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->trial_rule_4:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;

    .line 30
    .line 31
    if-eqz v1, :cond_3

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    :cond_3
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->trial_rule_5:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;

    .line 37
    .line 38
    if-eqz p1, :cond_4

    .line 39
    .line 40
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    :cond_4
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇8〇80o(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇o〇88〇8(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o88(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroidx/appcompat/widget/AppCompatTextView;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O〇8〇008(Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇O8OO()J
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-wide v2, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOo〇8o008:J

    .line 6
    .line 7
    sub-long/2addr v0, v2

    .line 8
    const/16 v2, 0x3e8

    .line 9
    .line 10
    int-to-long v2, v2

    .line 11
    div-long/2addr v0, v2

    .line 12
    return-wide v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇oo(Landroidx/appcompat/widget/AppCompatTextView;I)V
    .locals 9
    .param p2    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const v2, 0x7f130ac7

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object v0, v1

    .line 17
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v3, "\u5f00\u542f\u8ba2\u9605\u89c6\u4e3a\u540c\u610f"

    .line 23
    .line 24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v3, "\uff0c\u77e5\u6653\u4f1a\u5458\u5230\u671f\u540e\u5c06\u81ea\u52a8\u7eed\u8d39\uff0c\u53ef\u968f\u65f6\u53d6\u6d88\uff1b\u8ba2\u9605\u7ba1\u7406\u8bf7\u524d\u5f80\u4f1a\u5458\u4ed8\u8d39\u9875\u6700\u4e0b\u65b9\u67e5\u770b\u3002"

    .line 31
    .line 32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    new-instance v8, Landroid/text/SpannableString;

    .line 40
    .line 41
    invoke-direct {v8, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 42
    .line 43
    .line 44
    if-eqz v0, :cond_1

    .line 45
    .line 46
    const/4 v4, 0x0

    .line 47
    const/4 v5, 0x0

    .line 48
    const/4 v6, 0x6

    .line 49
    const/4 v7, 0x0

    .line 50
    move-object v3, v0

    .line 51
    invoke-static/range {v2 .. v7}, Lkotlin/text/StringsKt;->oO00OOO(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    :cond_1
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    .line 60
    .line 61
    invoke-direct {v2, p2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 62
    .line 63
    .line 64
    if-eqz v1, :cond_2

    .line 65
    .line 66
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 67
    .line 68
    .line 69
    move-result p2

    .line 70
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    add-int/2addr v1, v0

    .line 79
    const/16 v0, 0x21

    .line 80
    .line 81
    invoke-virtual {v8, v2, p2, v1, v0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 82
    .line 83
    .line 84
    :cond_2
    if-nez p1, :cond_3

    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_3
    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    .line 89
    .line 90
    :goto_1
    if-nez p1, :cond_4

    .line 91
    .line 92
    goto :goto_2

    .line 93
    :cond_4
    const/4 p2, 0x0

    .line 94
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHighlightColor(I)V

    .line 95
    .line 96
    .line 97
    :goto_2
    if-nez p1, :cond_5

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_5
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    .line 101
    .line 102
    .line 103
    move-result-object p2

    .line 104
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 105
    .line 106
    .line 107
    :goto_3
    if-eqz p1, :cond_6

    .line 108
    .line 109
    new-instance p2, L〇〇o0o/〇o〇;

    .line 110
    .line 111
    invoke-direct {p2, p0}, L〇〇o0o/〇o〇;-><init>(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    .line 116
    .line 117
    :cond_6
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇088O()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, L〇〇o0o/o〇0;

    .line 12
    .line 13
    invoke-direct {v1, p0}, L〇〇o0o/o〇0;-><init>(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    new-instance v1, L〇〇o0o/〇〇888;

    .line 30
    .line 31
    invoke-direct {v1, p0}, L〇〇o0o/〇〇888;-><init>(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0ooOOo()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 8
    .line 9
    if-eqz v0, :cond_3

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 13
    .line 14
    .line 15
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 16
    .line 17
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    const v3, 0x7f060234

    .line 25
    .line 26
    .line 27
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    const/high16 v2, 0x43480000    # 200.0f

    .line 36
    .line 37
    invoke-static {v2}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇080(F)F

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-virtual {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 50
    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 53
    .line 54
    if-eqz v1, :cond_0

    .line 55
    .line 56
    iget-object v1, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->reading_rule_button_title:Ljava/lang/String;

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    const/4 v1, 0x0

    .line 60
    :goto_0
    const-string v2, "\u8bf7\u5148\u9605\u8bfb\u8ba2\u9605\u4f18\u60e0\u89c4\u5219"

    .line 61
    .line 62
    invoke-static {v1, v2}, Lcom/intsig/utils/ext/StringExtKt;->o〇0(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    const-string v1, "\uff083\u79d2\uff09"

    .line 75
    .line 76
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    .line 85
    .line 86
    invoke-static {}, Lcom/intsig/util/CountdownTimer;->o〇0()Lcom/intsig/util/CountdownTimer;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0O:Lcom/intsig/util/CountdownTimer;

    .line 91
    .line 92
    if-eqz v0, :cond_1

    .line 93
    .line 94
    const/4 v1, 0x3

    .line 95
    invoke-virtual {v0, v1}, Lcom/intsig/util/CountdownTimer;->〇80〇808〇O(I)V

    .line 96
    .line 97
    .line 98
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0O:Lcom/intsig/util/CountdownTimer;

    .line 99
    .line 100
    if-eqz v0, :cond_2

    .line 101
    .line 102
    iget-object v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O88O:Lcom/intsig/util/CountdownTimer$OnCountdownListener;

    .line 103
    .line 104
    invoke-virtual {v0, v1}, Lcom/intsig/util/CountdownTimer;->OO0o〇〇〇〇0(Lcom/intsig/util/CountdownTimer$OnCountdownListener;)V

    .line 105
    .line 106
    .line 107
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0O:Lcom/intsig/util/CountdownTimer;

    .line 108
    .line 109
    if-eqz v0, :cond_3

    .line 110
    .line 111
    invoke-virtual {v0}, Lcom/intsig/util/CountdownTimer;->〇8o8o〇()V

    .line 112
    .line 113
    .line 114
    :cond_3
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oo8ooo8O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇〇〇00(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇8〇80o(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V
    .locals 5

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 16
    .line 17
    const-string v0, "free trial"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇08O〇00〇o:Ljava/lang/String;

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_1
    const/4 v0, 0x0

    .line 35
    goto :goto_1

    .line 36
    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 37
    :goto_1
    if-eqz v0, :cond_3

    .line 38
    .line 39
    const-string p0, "product id is empty"

    .line 40
    .line 41
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 46
    .line 47
    if-eqz p1, :cond_6

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO〇00〇8oO:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 50
    .line 51
    const/4 v2, 0x0

    .line 52
    const-string v3, "mPurchaseTracker"

    .line 53
    .line 54
    if-nez v0, :cond_4

    .line 55
    .line 56
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    move-object v0, v2

    .line 60
    :cond_4
    iget-object v4, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇08O〇00〇o:Ljava/lang/String;

    .line 61
    .line 62
    iput-object v4, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->productId:Ljava/lang/String;

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO〇00〇8oO:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 65
    .line 66
    if-nez v0, :cond_5

    .line 67
    .line 68
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    goto :goto_2

    .line 72
    :cond_5
    move-object v2, v0

    .line 73
    :goto_2
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->OOO(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->oO00OOO(I)V

    .line 77
    .line 78
    .line 79
    iget v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇00O:I

    .line 80
    .line 81
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇80(I)V

    .line 82
    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇08O〇00〇o:Ljava/lang/String;

    .line 85
    .line 86
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O0O8OO088(Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOo0:Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

    .line 90
    .line 91
    if-eqz p1, :cond_7

    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇08O〇00〇o:Ljava/lang/String;

    .line 94
    .line 95
    invoke-interface {p1, p0, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;->onPurchase(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    :cond_7
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇8〇OOoooo()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇O8oOo0(Landroidx/appcompat/widget/AppCompatTextView;Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇〇〇O〇(Landroidx/appcompat/widget/AppCompatTextView;Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O8〇8O0oO(Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;Landroidx/appcompat/widget/AppCompatTextView;Landroidx/appcompat/widget/AppCompatTextView;)V
    .locals 3

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    if-eqz p3, :cond_1

    .line 4
    .line 5
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;->desc:Ljava/lang/String;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    move-object v1, v0

    .line 11
    :goto_0
    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 12
    .line 13
    .line 14
    :cond_1
    iget p3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;->is_sms:I

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    const/4 v2, 0x0

    .line 18
    if-ne p3, v1, :cond_3

    .line 19
    .line 20
    if-eqz p2, :cond_5

    .line 21
    .line 22
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    if-eqz p1, :cond_5

    .line 27
    .line 28
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    const p3, 0x7f080a5d

    .line 32
    .line 33
    .line 34
    invoke-static {p1, p3}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    if-eqz p1, :cond_2

    .line 39
    .line 40
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    .line 41
    .line 42
    .line 43
    move-result p3

    .line 44
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    const/4 v1, 0x0

    .line 49
    invoke-virtual {p1, v1, v1, p3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 50
    .line 51
    .line 52
    :cond_2
    invoke-virtual {p2, p1, v2, v2, v2}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_3
    if-eqz p2, :cond_5

    .line 57
    .line 58
    iget p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;->day_interval:I

    .line 59
    .line 60
    invoke-static {p1}, Lcom/intsig/utils/DateTimeUtil;->〇080(I)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    if-eqz p1, :cond_4

    .line 65
    .line 66
    move-object v0, p1

    .line 67
    :cond_4
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p2, v2, v2, v2, v2}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 71
    .line 72
    .line 73
    :cond_5
    :goto_1
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇o08()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 8
    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO〇00〇8oO:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 14
    .line 15
    if-nez v2, :cond_0

    .line 16
    .line 17
    const-string v2, "mPurchaseTracker"

    .line 18
    .line 19
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    :cond_0
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 27
    .line 28
    iget-boolean v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o8〇OO0〇0o:Z

    .line 29
    .line 30
    if-eqz v1, :cond_1

    .line 31
    .line 32
    const/4 v1, 0x0

    .line 33
    iput-boolean v1, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->OoO8:Z

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-boolean v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->ooo0〇〇O:Z

    .line 37
    .line 38
    iput-boolean v1, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->OoO8:Z

    .line 39
    .line 40
    :goto_0
    iget-boolean v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇8〇oO〇〇8o:Z

    .line 41
    .line 42
    iput-boolean v1, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇〇8O0〇8:Z

    .line 43
    .line 44
    iget-boolean v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇〇08O:Z

    .line 45
    .line 46
    iput-boolean v1, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇oo〇:Z

    .line 47
    .line 48
    iget-boolean v1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o8oOOo:Z

    .line 49
    .line 50
    iput-boolean v1, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->o800o8O:Z

    .line 51
    .line 52
    new-instance v1, L〇〇o0o/O8;

    .line 53
    .line 54
    invoke-direct {v1, p0}, L〇〇o0o/O8;-><init>(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇O〇80o08O(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 58
    .line 59
    .line 60
    :cond_2
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇o〇88〇8(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 16
    .line 17
    const-string v0, "on click close"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->dismissAllowingStateLoss()V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOo0:Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

    .line 26
    .line 27
    if-eqz p1, :cond_1

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇O8OO()J

    .line 30
    .line 31
    .line 32
    move-result-wide v0

    .line 33
    invoke-interface {p1, v0, v1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;->onClose(J)V

    .line 34
    .line 35
    .line 36
    :cond_1
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇〇(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x1

    .line 8
    if-le v0, v3, :cond_7

    .line 9
    .line 10
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    move-object v4, v2

    .line 15
    move-object v5, v4

    .line 16
    move-object v6, v5

    .line 17
    move-object v7, v6

    .line 18
    move-object v8, v7

    .line 19
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v9

    .line 23
    if-eqz v9, :cond_9

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v9

    .line 29
    add-int/lit8 v10, v1, 0x1

    .line 30
    .line 31
    if-gez v1, :cond_0

    .line 32
    .line 33
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 34
    .line 35
    .line 36
    :cond_0
    check-cast v9, Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;

    .line 37
    .line 38
    if-nez v1, :cond_1

    .line 39
    .line 40
    move-object v4, v9

    .line 41
    goto :goto_2

    .line 42
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 43
    .line 44
    .line 45
    move-result v11

    .line 46
    sub-int/2addr v11, v3

    .line 47
    if-ne v1, v11, :cond_2

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_2
    if-ne v1, v3, :cond_3

    .line 51
    .line 52
    move-object v5, v9

    .line 53
    goto :goto_2

    .line 54
    :cond_3
    const/4 v11, 0x2

    .line 55
    if-ne v1, v11, :cond_4

    .line 56
    .line 57
    move-object v6, v9

    .line 58
    goto :goto_2

    .line 59
    :cond_4
    const/4 v11, 0x3

    .line 60
    if-ne v1, v11, :cond_5

    .line 61
    .line 62
    move-object v7, v9

    .line 63
    goto :goto_2

    .line 64
    :cond_5
    const/4 v11, 0x4

    .line 65
    if-ne v1, v11, :cond_6

    .line 66
    .line 67
    :goto_1
    move-object v8, v9

    .line 68
    :cond_6
    :goto_2
    move v1, v10

    .line 69
    goto :goto_0

    .line 70
    :cond_7
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-ne v0, v3, :cond_8

    .line 75
    .line 76
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    move-result-object v4

    .line 80
    move-object v5, v2

    .line 81
    goto :goto_3

    .line 82
    :cond_8
    move-object v4, v2

    .line 83
    move-object v5, v4

    .line 84
    :goto_3
    move-object v6, v5

    .line 85
    move-object v7, v6

    .line 86
    move-object v8, v7

    .line 87
    :cond_9
    check-cast v4, Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;

    .line 88
    .line 89
    if-eqz v4, :cond_d

    .line 90
    .line 91
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    if-eqz p1, :cond_a

    .line 96
    .line 97
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->OO:Landroidx/constraintlayout/widget/Group;

    .line 98
    .line 99
    if-eqz p1, :cond_a

    .line 100
    .line 101
    const-string v0, "groupFirst"

    .line 102
    .line 103
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 107
    .line 108
    .line 109
    :cond_a
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    if-eqz p1, :cond_b

    .line 114
    .line 115
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 116
    .line 117
    goto :goto_4

    .line 118
    :cond_b
    move-object p1, v2

    .line 119
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    if-eqz v0, :cond_c

    .line 124
    .line 125
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->oOO〇〇:Landroidx/appcompat/widget/AppCompatTextView;

    .line 126
    .line 127
    goto :goto_5

    .line 128
    :cond_c
    move-object v0, v2

    .line 129
    :goto_5
    invoke-direct {p0, v4, p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O8〇8O0oO(Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;Landroidx/appcompat/widget/AppCompatTextView;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 130
    .line 131
    .line 132
    :cond_d
    if-eqz v5, :cond_11

    .line 133
    .line 134
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    if-eqz p1, :cond_e

    .line 139
    .line 140
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->o〇00O:Landroidx/constraintlayout/widget/Group;

    .line 141
    .line 142
    if-eqz p1, :cond_e

    .line 143
    .line 144
    const-string v0, "groupSecond"

    .line 145
    .line 146
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 150
    .line 151
    .line 152
    :cond_e
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 153
    .line 154
    .line 155
    move-result-object p1

    .line 156
    if-eqz p1, :cond_f

    .line 157
    .line 158
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 159
    .line 160
    goto :goto_6

    .line 161
    :cond_f
    move-object p1, v2

    .line 162
    :goto_6
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    if-eqz v0, :cond_10

    .line 167
    .line 168
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->oo8ooo8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 169
    .line 170
    goto :goto_7

    .line 171
    :cond_10
    move-object v0, v2

    .line 172
    :goto_7
    invoke-direct {p0, v5, p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O8〇8O0oO(Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;Landroidx/appcompat/widget/AppCompatTextView;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 173
    .line 174
    .line 175
    :cond_11
    if-eqz v6, :cond_15

    .line 176
    .line 177
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 178
    .line 179
    .line 180
    move-result-object p1

    .line 181
    if-eqz p1, :cond_12

    .line 182
    .line 183
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->O8o08O8O:Landroidx/constraintlayout/widget/Group;

    .line 184
    .line 185
    if-eqz p1, :cond_12

    .line 186
    .line 187
    const-string v0, "groupThird"

    .line 188
    .line 189
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    .line 191
    .line 192
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 193
    .line 194
    .line 195
    :cond_12
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    if-eqz p1, :cond_13

    .line 200
    .line 201
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 202
    .line 203
    goto :goto_8

    .line 204
    :cond_13
    move-object p1, v2

    .line 205
    :goto_8
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 206
    .line 207
    .line 208
    move-result-object v0

    .line 209
    if-eqz v0, :cond_14

    .line 210
    .line 211
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 212
    .line 213
    goto :goto_9

    .line 214
    :cond_14
    move-object v0, v2

    .line 215
    :goto_9
    invoke-direct {p0, v6, p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O8〇8O0oO(Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;Landroidx/appcompat/widget/AppCompatTextView;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 216
    .line 217
    .line 218
    :cond_15
    if-eqz v7, :cond_19

    .line 219
    .line 220
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 221
    .line 222
    .line 223
    move-result-object p1

    .line 224
    if-eqz p1, :cond_16

    .line 225
    .line 226
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/Group;

    .line 227
    .line 228
    if-eqz p1, :cond_16

    .line 229
    .line 230
    const-string v0, "groupFourth"

    .line 231
    .line 232
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    .line 234
    .line 235
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 236
    .line 237
    .line 238
    :cond_16
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 239
    .line 240
    .line 241
    move-result-object p1

    .line 242
    if-eqz p1, :cond_17

    .line 243
    .line 244
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 245
    .line 246
    goto :goto_a

    .line 247
    :cond_17
    move-object p1, v2

    .line 248
    :goto_a
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 249
    .line 250
    .line 251
    move-result-object v0

    .line 252
    if-eqz v0, :cond_18

    .line 253
    .line 254
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->o8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 255
    .line 256
    goto :goto_b

    .line 257
    :cond_18
    move-object v0, v2

    .line 258
    :goto_b
    invoke-direct {p0, v7, p1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O8〇8O0oO(Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;Landroidx/appcompat/widget/AppCompatTextView;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 259
    .line 260
    .line 261
    :cond_19
    if-eqz v8, :cond_1d

    .line 262
    .line 263
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 264
    .line 265
    .line 266
    move-result-object p1

    .line 267
    if-eqz p1, :cond_1a

    .line 268
    .line 269
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/Group;

    .line 270
    .line 271
    if-eqz p1, :cond_1a

    .line 272
    .line 273
    const-string v0, "groupFifth"

    .line 274
    .line 275
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    .line 277
    .line 278
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 279
    .line 280
    .line 281
    :cond_1a
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 282
    .line 283
    .line 284
    move-result-object p1

    .line 285
    if-eqz p1, :cond_1b

    .line 286
    .line 287
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 288
    .line 289
    goto :goto_c

    .line 290
    :cond_1b
    move-object p1, v2

    .line 291
    :goto_c
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 292
    .line 293
    .line 294
    move-result-object v0

    .line 295
    if-eqz v0, :cond_1c

    .line 296
    .line 297
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->O88O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 298
    .line 299
    :cond_1c
    invoke-direct {p0, v8, p1, v2}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O8〇8O0oO(Lcom/intsig/comm/purchase/entity/QueryProductsResult$OneTrialRule;Landroidx/appcompat/widget/AppCompatTextView;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 300
    .line 301
    .line 302
    :cond_1d
    return-void
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private static final 〇〇O80〇0o(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    const-string v0, "null cannot be cast to non-null type com.google.android.material.bottomsheet.BottomSheetDialog"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    check-cast p0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getBehavior()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    const-string v0, "dialog.behavior"

    .line 13
    .line 14
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x3

    .line 18
    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇〇0(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;I)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOO〇〇:Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$mHandler$1;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "mHandler.obtainMessage()"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    iput v1, v0, Landroid/os/Message;->what:I

    .line 19
    .line 20
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 21
    .line 22
    iget-object p0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOO〇〇:Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$mHandler$1;

    .line 23
    .line 24
    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇〇〇00(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->dismissAllowingStateLoss()V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O〇〇O8:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 12
    .line 13
    if-eqz p0, :cond_1

    .line 14
    .line 15
    invoke-interface {p0}, Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;->O0o〇O0〇()V

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget-boolean p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O0O:Z

    .line 20
    .line 21
    if-eqz p1, :cond_1

    .line 22
    .line 23
    iget-object p0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O〇〇O8:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 24
    .line 25
    if-eqz p0, :cond_1

    .line 26
    .line 27
    invoke-interface {p0}, Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;->o80ooO()V

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇〇O〇(Landroidx/appcompat/widget/AppCompatTextView;Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p0, :cond_0

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const v2, 0x7f0601d5

    .line 18
    .line 19
    .line 20
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const v1, 0x7f0601d3

    .line 33
    .line 34
    .line 35
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    invoke-virtual {v0, p1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇oo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    sget-object v0, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 44
    .line 45
    invoke-virtual {p1, v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->o〇O8〇〇o(Landroid/graphics/drawable/GradientDrawable$Orientation;)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    const/high16 v0, 0x43480000    # 200.0f

    .line 50
    .line 51
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇080(F)F

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    invoke-virtual {p1, v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 64
    .line 65
    .line 66
    const/4 p1, 0x1

    .line 67
    const/high16 v0, 0x41a00000    # 20.0f

    .line 68
    .line 69
    invoke-virtual {p0, p1, v0}, Landroidx/appcompat/widget/AppCompatTextView;->setTextSize(IF)V

    .line 70
    .line 71
    .line 72
    :cond_0
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public final O0O0〇(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;)Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O〇〇O8:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O8〇8〇O80(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o8oOOo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public dismissAllowingStateLoss()V
    .locals 2

    .line 1
    :try_start_0
    invoke-super {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    sget-object v1, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    .line 10
    .line 11
    :goto_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTheme()I
    .locals 1

    .line 1
    const v0, 0x7f140150

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    if-eqz p1, :cond_5

    .line 9
    .line 10
    const-string v0, "KEY_TRIAL_RULES"

    .line 11
    .line 12
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    instance-of v1, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    check-cast v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    move-object v0, v2

    .line 25
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 26
    .line 27
    const-string v0, "KEY_PRODUCT_ID"

    .line 28
    .line 29
    const-string v1, ""

    .line 30
    .line 31
    invoke-virtual {p1, v0, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇08O〇00〇o:Ljava/lang/String;

    .line 36
    .line 37
    const-string v0, "KEY_PRICE_DESCRIPTION"

    .line 38
    .line 39
    invoke-virtual {p1, v0, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O8o08O8O:Ljava/lang/String;

    .line 44
    .line 45
    const-string v0, "KEY_PAY_TYPE"

    .line 46
    .line 47
    const/4 v1, 0x2

    .line 48
    invoke-virtual {p1, v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;I)I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    iput v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇00O:I

    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOo0:Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

    .line 55
    .line 56
    if-nez v0, :cond_2

    .line 57
    .line 58
    const-string v0, "KEY_TRIAL_LISTENER"

    .line 59
    .line 60
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    instance-of v1, v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

    .line 65
    .line 66
    if-eqz v1, :cond_1

    .line 67
    .line 68
    check-cast v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_1
    move-object v0, v2

    .line 72
    :goto_1
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOo0:Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

    .line 73
    .line 74
    :cond_2
    const-string v0, "KEY_PURCHASE_TRACKER"

    .line 75
    .line 76
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    instance-of v0, p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 81
    .line 82
    if-eqz v0, :cond_3

    .line 83
    .line 84
    move-object v2, p1

    .line 85
    check-cast v2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 86
    .line 87
    :cond_3
    if-nez v2, :cond_4

    .line 88
    .line 89
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 90
    .line 91
    invoke-direct {p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 92
    .line 93
    .line 94
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSGuidePremium:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 95
    .line 96
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 101
    .line 102
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->SELF_SEARCH_GUIDE:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 107
    .line 108
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    invoke-static {}, Lcom/intsig/camscanner/guide/dropchannel/DropCnlShowConfiguration;->〇o〇()I

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->times(I)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    const-string p1, "PurchaseTracker().pageId\u2026es(getDropCnlShowTimes())"

    .line 121
    .line 122
    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    :cond_4
    iput-object v2, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO〇00〇8oO:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 126
    .line 127
    :cond_5
    return-void
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d0246

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onDestroyView"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇o0O:Lcom/intsig/callback/DialogDismissListener;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/intsig/callback/DialogDismissListener;->dismiss()V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onStart"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOo0:Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;->onShow()V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    sget-object p1, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o〇oO:Ljava/lang/String;

    .line 10
    .line 11
    const-string p2, "onViewCreated"

    .line 12
    .line 13
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    new-instance p2, L〇〇o0o/〇080;

    .line 23
    .line 24
    invoke-direct {p2}, L〇〇o0o/〇080;-><init>()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0〇0()Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const/4 p2, 0x0

    .line 35
    if-eqz p1, :cond_1

    .line 36
    .line 37
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTrialRuleConfigBinding;->〇08〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    move-object p1, p2

    .line 41
    :goto_0
    if-nez p1, :cond_2

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 45
    .line 46
    if-eqz v0, :cond_3

    .line 47
    .line 48
    iget-object p2, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->rule_title:Ljava/lang/String;

    .line 49
    .line 50
    :cond_3
    const-string v0, "\u8ba2\u9605\u4f18\u60e0\u89c4\u5219\u8bf4\u660e"

    .line 51
    .line 52
    invoke-static {p2, v0}, Lcom/intsig/utils/ext/StringExtKt;->o〇0(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    .line 58
    .line 59
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->OO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 60
    .line 61
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOoO8OO〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;)Ljava/util/ArrayList;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇〇(Ljava/util/ArrayList;)V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->Ooo8o()V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇0ooOOo()V

    .line 72
    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇o08()V

    .line 75
    .line 76
    .line 77
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇088O()V

    .line 78
    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final setDialogDismissListener(Lcom/intsig/callback/DialogDismissListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇o0O:Lcom/intsig/callback/DialogDismissListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇08O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->ooo0〇〇O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇0oO〇oo00(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇8O0880(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇O0o〇〇o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇〇08O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇O8〇8000(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇Oo〇O(Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->oOo0:Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public final Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "DropCnlTopBannerProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;,
        Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$PagerAdapter;,
        Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$Companion;,
        Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$PagerHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final oOo0:Ljava/lang/String;

.field public static final oOo〇8o008:Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Z

.field private final o〇00O:Landroidx/lifecycle/LifecycleOwner;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;

.field private 〇0O:Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->oOo〇8o008:Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lkotlin/reflect/KClass;->O8()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->oOo0:Ljava/lang/String;

    .line 20
    .line 21
    return-void
.end method

.method public constructor <init>(Landroidx/lifecycle/LifecycleOwner;Z)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "lifecycleOwner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->o〇00O:Landroidx/lifecycle/LifecycleOwner;

    .line 10
    .line 11
    iput-boolean p2, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->O8o08O8O:Z

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o800o8O(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;)Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇0O:Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;)Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇oo〇(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;)V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;->〇08O〇00〇o:Landroid/view/ViewStub;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 8
    .line 9
    .line 10
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    goto :goto_0

    .line 12
    :catch_0
    move-exception v0

    .line 13
    sget-object v1, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->oOo0:Ljava/lang/String;

    .line 14
    .line 15
    const-string v2, ""

    .line 16
    .line 17
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    :goto_0
    if-eqz v0, :cond_2

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;

    .line 28
    .line 29
    invoke-static {v0}, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇0O:Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;

    .line 34
    .line 35
    if-eqz p1, :cond_0

    .line 36
    .line 37
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 38
    .line 39
    if-eqz p1, :cond_0

    .line 40
    .line 41
    const-string v0, "ivBannerDoc"

    .line 42
    .line 43
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    invoke-static {p1}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O00(Landroid/view/View;)V

    .line 47
    .line 48
    .line 49
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇0O:Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;

    .line 50
    .line 51
    if-eqz p1, :cond_1

    .line 52
    .line 53
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 54
    .line 55
    if-eqz p1, :cond_1

    .line 56
    .line 57
    const-string v0, "tvBannerTitle"

    .line 58
    .line 59
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-static {p1}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O00(Landroid/view/View;)V

    .line 63
    .line 64
    .line 65
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇0O:Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;

    .line 66
    .line 67
    if-eqz p1, :cond_2

    .line 68
    .line 69
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 70
    .line 71
    if-eqz p1, :cond_2

    .line 72
    .line 73
    const-string v0, "tvBannerSubtitle"

    .line 74
    .line 75
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    invoke-static {p1}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O00(Landroid/view/View;)V

    .line 79
    .line 80
    .line 81
    :cond_2
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public final O8ooOoo〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->o〇00O:Landroidx/lifecycle/LifecycleOwner;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$startDocAnimation$1;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$startDocAnimation$1;-><init>(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenResumed(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;

    .line 7
    .line 8
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string p2, "super.onCreateViewHolder\u2026arent, viewType).itemView"

    .line 15
    .line 16
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;-><init>(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final O〇8O8〇008()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇0O:Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    return-object v0
.end method

.method public oO80()I
    .locals 1

    .line 1
    const v0, 0x7f0d03e4

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇O8〇〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;)V
    .locals 4
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p2, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopBannerItem;

    .line 12
    .line 13
    check-cast p1, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;

    .line 14
    .line 15
    iget-boolean v0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->O8o08O8O:Z

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇oo〇(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;->OO:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;

    .line 27
    .line 28
    new-instance v1, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$PagerAdapter;

    .line 29
    .line 30
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 31
    .line 32
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    const-string v3, "holder.itemView.context"

    .line 37
    .line 38
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopBannerItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    invoke-direct {v1, v2, p2}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$PagerAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 46
    .line 47
    .line 48
    const-wide/16 v2, 0xbb8

    .line 49
    .line 50
    invoke-virtual {v0, v2, v3}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setInterval(J)V

    .line 51
    .line 52
    .line 53
    sget-object p2, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;->RIGHT:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;

    .line 54
    .line 55
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setDirection(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;)V

    .line 56
    .line 57
    .line 58
    const/4 p2, 0x1

    .line 59
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setCycle(Z)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setStopScrollWhenTouch(Z)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setBorderAnimation(Z)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider$DropCnlTopBannerHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemDropCnlTopLoopBannerBinding;->〇OOo8〇0:Lcom/intsig/camscanner/guide/IndicatorView;

    .line 76
    .line 77
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/guide/IndicatorView;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 78
    .line 79
    .line 80
    iget-boolean p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->O8o08O8O:Z

    .line 81
    .line 82
    if-nez p1, :cond_1

    .line 83
    .line 84
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oO80()V

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_1
    const/4 p1, 0x0

    .line 89
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->setEnableAutoScroll(Z)V

    .line 90
    .line 91
    .line 92
    :goto_0
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final 〇00()Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$AnimatorLocationData;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->〇0O:Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v1, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;

    .line 6
    .line 7
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 8
    .line 9
    invoke-virtual {v2}, Landroid/view/View;->getX()F

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 14
    .line 15
    invoke-virtual {v3}, Landroid/view/View;->getY()F

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;-><init>(FF)V

    .line 20
    .line 21
    .line 22
    new-instance v2, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;

    .line 23
    .line 24
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 25
    .line 26
    invoke-virtual {v3}, Landroid/view/View;->getX()F

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 31
    .line 32
    invoke-virtual {v4}, Landroid/view/View;->getY()F

    .line 33
    .line 34
    .line 35
    move-result v4

    .line 36
    invoke-direct {v2, v3, v4}, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;-><init>(FF)V

    .line 37
    .line 38
    .line 39
    new-instance v3, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;

    .line 40
    .line 41
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 42
    .line 43
    invoke-virtual {v4}, Landroid/view/View;->getX()F

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutDropCnlTopScanBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 48
    .line 49
    invoke-virtual {v0}, Landroid/view/View;->getY()F

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    invoke-direct {v3, v4, v0}, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;-><init>(FF)V

    .line 54
    .line 55
    .line 56
    new-instance v0, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$AnimatorLocationData;

    .line 57
    .line 58
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$AnimatorLocationData;-><init>(Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;Lcom/intsig/camscanner/purchase/scanfirstdoc/drop/DocAnimationUtil$Location;)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    const/4 v0, 0x0

    .line 63
    :goto_0
    return-object v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlTopBannerProvider;->o〇O8〇〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

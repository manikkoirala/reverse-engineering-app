.class public final Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider$DropCnlVipPrivilegesHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "DropCnlPrivilegesProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "DropCnlVipPrivilegesHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic OO:Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider;

.field private final o0:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/databinding/ItemDropCnlPrivilegesBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider;Landroid/view/View;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "convertView"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider$DropCnlVipPrivilegesHolder;->OO:Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider;

    .line 7
    .line 8
    invoke-direct {p0, p2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 9
    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider$DropCnlVipPrivilegesHolder;->o0:Landroid/view/View;

    .line 12
    .line 13
    invoke-static {p2}, Lcom/intsig/camscanner/databinding/ItemDropCnlPrivilegesBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemDropCnlPrivilegesBinding;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const-string p2, "bind(convertView)"

    .line 18
    .line 19
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider$DropCnlVipPrivilegesHolder;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/ItemDropCnlPrivilegesBinding;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final 〇00()V
    .locals 5

    .line 1
    const/4 v0, 0x7

    .line 2
    new-array v0, v0, [Ljava/lang/Integer;

    .line 3
    .line 4
    const v1, 0x7f08098e

    .line 5
    .line 6
    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const/4 v2, 0x0

    .line 12
    aput-object v1, v0, v2

    .line 13
    .line 14
    const v1, 0x7f080991

    .line 15
    .line 16
    .line 17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const/4 v3, 0x1

    .line 22
    aput-object v1, v0, v3

    .line 23
    .line 24
    const v1, 0x7f08098f

    .line 25
    .line 26
    .line 27
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    const/4 v3, 0x2

    .line 32
    aput-object v1, v0, v3

    .line 33
    .line 34
    const v1, 0x7f08098c

    .line 35
    .line 36
    .line 37
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    const/4 v3, 0x3

    .line 42
    aput-object v1, v0, v3

    .line 43
    .line 44
    const v1, 0x7f080990

    .line 45
    .line 46
    .line 47
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    const/4 v3, 0x4

    .line 52
    aput-object v1, v0, v3

    .line 53
    .line 54
    const v1, 0x7f080992

    .line 55
    .line 56
    .line 57
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    const/4 v3, 0x5

    .line 62
    aput-object v1, v0, v3

    .line 63
    .line 64
    const v1, 0x7f08098d

    .line 65
    .line 66
    .line 67
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    const/4 v3, 0x6

    .line 72
    aput-object v1, v0, v3

    .line 73
    .line 74
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider$DropCnlVipPrivilegesHolder;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/ItemDropCnlPrivilegesBinding;

    .line 79
    .line 80
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ItemDropCnlPrivilegesBinding;->OO:Lcom/intsig/camscanner/guide/AutoPollRecyclerView;

    .line 81
    .line 82
    new-instance v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 83
    .line 84
    iget-object v4, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider$DropCnlVipPrivilegesHolder;->o0:Landroid/view/View;

    .line 85
    .line 86
    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 87
    .line 88
    .line 89
    move-result-object v4

    .line 90
    invoke-direct {v3, v4, v2, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 94
    .line 95
    .line 96
    new-instance v2, Lcom/intsig/camscanner/guide/AutoPollAdapter;

    .line 97
    .line 98
    iget-object v3, p0, Lcom/intsig/camscanner/guide/dropchannel/adapter/provide/DropCnlPrivilegesProvider$DropCnlVipPrivilegesHolder;->o0:Landroid/view/View;

    .line 99
    .line 100
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 101
    .line 102
    .line 103
    move-result-object v3

    .line 104
    invoke-direct {v2, v3, v0}, Lcom/intsig/camscanner/guide/AutoPollAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {v1}, Lcom/intsig/camscanner/guide/AutoPollRecyclerView;->〇o〇()V

    .line 111
    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

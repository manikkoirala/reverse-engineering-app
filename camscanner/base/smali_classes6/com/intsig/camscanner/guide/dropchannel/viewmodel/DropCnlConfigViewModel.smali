.class public final Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "DropCnlConfigViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8o08O8O:Ljava/lang/String;

.field public static final o〇00O:Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private OO:Ljava/lang/Boolean;

.field private o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

.field private final 〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->o〇00O:Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lkotlin/reflect/KClass;->O8()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sput-object v0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->O8o08O8O:Ljava/lang/String;

    .line 20
    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x2

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->〇OOo8〇0:I

    .line 6
    .line 7
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 8
    .line 9
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O8ooOoo〇()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    iget-object v2, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->new_advertise_cn_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;

    .line 20
    .line 21
    const/4 v3, 0x0

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    iget v4, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;->top_banner_style:I

    .line 25
    .line 26
    const/4 v5, 0x1

    .line 27
    if-ne v4, v5, :cond_0

    .line 28
    .line 29
    const/4 v3, 0x1

    .line 30
    :cond_0
    const/4 v4, 0x0

    .line 31
    if-eqz v2, :cond_1

    .line 32
    .line 33
    iget-object v5, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;->top_region_banner_bg_url:Ljava/lang/String;

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    move-object v5, v4

    .line 37
    :goto_0
    if-eqz v2, :cond_2

    .line 38
    .line 39
    iget-object v4, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;->top_region_banner_text_url:Ljava/lang/String;

    .line 40
    .line 41
    :cond_2
    if-eqz v2, :cond_3

    .line 42
    .line 43
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->〇80〇808〇O(Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;)Ljava/util/ArrayList;

    .line 44
    .line 45
    .line 46
    move-result-object v6

    .line 47
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 48
    .line 49
    .line 50
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->O8〇o(Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;)Ljava/util/ArrayList;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 55
    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_3
    const-string v2, ""

    .line 59
    .line 60
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    new-instance v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 64
    .line 65
    invoke-direct {v2}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;-><init>()V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    new-instance v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 72
    .line 73
    invoke-direct {v2}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;-><init>()V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    new-instance v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 80
    .line 81
    invoke-direct {v2}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;-><init>()V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    .line 88
    .line 89
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .line 91
    .line 92
    if-eqz v3, :cond_4

    .line 93
    .line 94
    new-instance v6, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopRegionBannerItem;

    .line 95
    .line 96
    invoke-direct {v6}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopRegionBannerItem;-><init>()V

    .line 97
    .line 98
    .line 99
    invoke-virtual {v6, v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopRegionBannerItem;->o〇0(Ljava/util/ArrayList;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v6, v5}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopRegionBannerItem;->Oo08(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v6, v4}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopRegionBannerItem;->〇〇888(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    goto :goto_2

    .line 112
    :cond_4
    new-instance v4, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopBannerItem;

    .line 113
    .line 114
    invoke-direct {v4}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopBannerItem;-><init>()V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopBannerItem;->〇o〇(Ljava/util/ArrayList;)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    :goto_2
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlVipPrivilegesItem;

    .line 124
    .line 125
    invoke-direct {v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlVipPrivilegesItem;-><init>()V

    .line 126
    .line 127
    .line 128
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->o0ooO()Ljava/util/List;

    .line 129
    .line 130
    .line 131
    move-result-object v4

    .line 132
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlVipPrivilegesItem;->〇o00〇〇Oo(Ljava/util/List;)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    .line 137
    .line 138
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;

    .line 139
    .line 140
    invoke-direct {v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;-><init>()V

    .line 141
    .line 142
    .line 143
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 144
    .line 145
    .line 146
    move-result-object v4

    .line 147
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 148
    .line 149
    .line 150
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->O8(Z)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    .line 155
    .line 156
    return-object v2
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O8〇o(Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;->price_info_1:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    :cond_0
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;->price_info_2:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 14
    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    :cond_1
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;->price_info_3:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 21
    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    :cond_2
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o0ooO()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x7

    .line 2
    new-array v0, v0, [Ljava/lang/Integer;

    .line 3
    .line 4
    const v1, 0x7f08098e

    .line 5
    .line 6
    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const/4 v2, 0x0

    .line 12
    aput-object v1, v0, v2

    .line 13
    .line 14
    const v1, 0x7f080991

    .line 15
    .line 16
    .line 17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const/4 v2, 0x1

    .line 22
    aput-object v1, v0, v2

    .line 23
    .line 24
    const v1, 0x7f08098f

    .line 25
    .line 26
    .line 27
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    const/4 v2, 0x2

    .line 32
    aput-object v1, v0, v2

    .line 33
    .line 34
    const v1, 0x7f08098c

    .line 35
    .line 36
    .line 37
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    const/4 v2, 0x3

    .line 42
    aput-object v1, v0, v2

    .line 43
    .line 44
    const v1, 0x7f080990

    .line 45
    .line 46
    .line 47
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    const/4 v2, 0x4

    .line 52
    aput-object v1, v0, v2

    .line 53
    .line 54
    const v1, 0x7f080992

    .line 55
    .line 56
    .line 57
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    const/4 v2, 0x5

    .line 62
    aput-object v1, v0, v2

    .line 63
    .line 64
    const v1, 0x7f08098d

    .line 65
    .line 66
    .line 67
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    const/4 v2, 0x6

    .line 72
    aput-object v1, v0, v2

    .line 73
    .line 74
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    return-object v0
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final oo88o8O()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    iget-object v2, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->after_scan_premiumpage:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;

    .line 20
    .line 21
    if-eqz v2, :cond_d

    .line 22
    .line 23
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 24
    .line 25
    if-eqz v3, :cond_0

    .line 26
    .line 27
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_1:Ljava/lang/String;

    .line 28
    .line 29
    if-eqz v3, :cond_0

    .line 30
    .line 31
    const-string v4, "banner_1"

    .line 32
    .line 33
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    :cond_0
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 40
    .line 41
    if-eqz v3, :cond_1

    .line 42
    .line 43
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_2:Ljava/lang/String;

    .line 44
    .line 45
    if-eqz v3, :cond_1

    .line 46
    .line 47
    const-string v4, "banner_2"

    .line 48
    .line 49
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    :cond_1
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 56
    .line 57
    if-eqz v3, :cond_2

    .line 58
    .line 59
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_3:Ljava/lang/String;

    .line 60
    .line 61
    if-eqz v3, :cond_2

    .line 62
    .line 63
    const-string v4, "banner_3"

    .line 64
    .line 65
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    :cond_2
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 72
    .line 73
    if-eqz v3, :cond_3

    .line 74
    .line 75
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_4:Ljava/lang/String;

    .line 76
    .line 77
    if-eqz v3, :cond_3

    .line 78
    .line 79
    const-string v4, "banner_4"

    .line 80
    .line 81
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    :cond_3
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 88
    .line 89
    if-eqz v3, :cond_4

    .line 90
    .line 91
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_5:Ljava/lang/String;

    .line 92
    .line 93
    if-eqz v3, :cond_4

    .line 94
    .line 95
    const-string v4, "banner_5"

    .line 96
    .line 97
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    .line 102
    .line 103
    :cond_4
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 104
    .line 105
    if-eqz v3, :cond_5

    .line 106
    .line 107
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_6:Ljava/lang/String;

    .line 108
    .line 109
    if-eqz v3, :cond_5

    .line 110
    .line 111
    const-string v4, "banner_6"

    .line 112
    .line 113
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    :cond_5
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 120
    .line 121
    if-eqz v3, :cond_6

    .line 122
    .line 123
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_7:Ljava/lang/String;

    .line 124
    .line 125
    if-eqz v3, :cond_6

    .line 126
    .line 127
    const-string v4, "banner_7"

    .line 128
    .line 129
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    .line 134
    .line 135
    :cond_6
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 136
    .line 137
    if-eqz v3, :cond_7

    .line 138
    .line 139
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_8:Ljava/lang/String;

    .line 140
    .line 141
    if-eqz v3, :cond_7

    .line 142
    .line 143
    const-string v4, "banner_8"

    .line 144
    .line 145
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    .line 150
    .line 151
    :cond_7
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 152
    .line 153
    if-eqz v3, :cond_8

    .line 154
    .line 155
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_9:Ljava/lang/String;

    .line 156
    .line 157
    if-eqz v3, :cond_8

    .line 158
    .line 159
    const-string v4, "banner_9"

    .line 160
    .line 161
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    .line 163
    .line 164
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    .line 166
    .line 167
    :cond_8
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 168
    .line 169
    if-eqz v3, :cond_9

    .line 170
    .line 171
    iget-object v3, v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_10:Ljava/lang/String;

    .line 172
    .line 173
    if-eqz v3, :cond_9

    .line 174
    .line 175
    const-string v4, "banner_10"

    .line 176
    .line 177
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    .line 182
    .line 183
    :cond_9
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->style1_price_info_1:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 184
    .line 185
    if-eqz v3, :cond_a

    .line 186
    .line 187
    const-string v4, "style1_price_info_1"

    .line 188
    .line 189
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    .line 194
    .line 195
    :cond_a
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->style1_price_info_2:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 196
    .line 197
    if-eqz v3, :cond_b

    .line 198
    .line 199
    const-string v4, "style1_price_info_2"

    .line 200
    .line 201
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    .line 206
    .line 207
    :cond_b
    iget-object v2, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AfterScanPremiumPage;->style1_price_info_3:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 208
    .line 209
    if-eqz v2, :cond_c

    .line 210
    .line 211
    const-string v3, "style1_price_info_3"

    .line 212
    .line 213
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    .line 218
    .line 219
    move-result v2

    .line 220
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 221
    .line 222
    .line 223
    move-result-object v2

    .line 224
    goto :goto_0

    .line 225
    :cond_c
    const/4 v2, 0x0

    .line 226
    :goto_0
    if-eqz v2, :cond_d

    .line 227
    .line 228
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 229
    .line 230
    .line 231
    goto :goto_1

    .line 232
    :cond_d
    const-string v2, ""

    .line 233
    .line 234
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    .line 236
    .line 237
    new-instance v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 238
    .line 239
    invoke-direct {v2}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;-><init>()V

    .line 240
    .line 241
    .line 242
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    .line 244
    .line 245
    new-instance v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 246
    .line 247
    invoke-direct {v2}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;-><init>()V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    .line 252
    .line 253
    new-instance v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 254
    .line 255
    invoke-direct {v2}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;-><init>()V

    .line 256
    .line 257
    .line 258
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    .line 260
    .line 261
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    .line 262
    .line 263
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 264
    .line 265
    .line 266
    new-instance v3, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopBannerItem;

    .line 267
    .line 268
    invoke-direct {v3}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopBannerItem;-><init>()V

    .line 269
    .line 270
    .line 271
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlTopBannerItem;->〇o〇(Ljava/util/ArrayList;)V

    .line 272
    .line 273
    .line 274
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    .line 276
    .line 277
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlVipPrivilegesItem;

    .line 278
    .line 279
    invoke-direct {v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlVipPrivilegesItem;-><init>()V

    .line 280
    .line 281
    .line 282
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->o0ooO()Ljava/util/List;

    .line 283
    .line 284
    .line 285
    move-result-object v3

    .line 286
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlVipPrivilegesItem;->〇o00〇〇Oo(Ljava/util/List;)V

    .line 287
    .line 288
    .line 289
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    .line 291
    .line 292
    new-instance v0, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;

    .line 293
    .line 294
    invoke-direct {v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;-><init>()V

    .line 295
    .line 296
    .line 297
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/dropchannel/entity/DropCnlPriceInfoItem;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 298
    .line 299
    .line 300
    move-result-object v3

    .line 301
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 302
    .line 303
    .line 304
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    .line 306
    .line 307
    return-object v2
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final 〇80〇808〇O(Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlConfig;->banner:Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;

    .line 7
    .line 8
    if-eqz p1, :cond_1d

    .line 9
    .line 10
    const-string v1, "banner"

    .line 11
    .line 12
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_1:Ljava/lang/String;

    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    const/4 v3, 0x1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-nez v1, :cond_0

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 v1, 0x0

    .line 29
    goto :goto_1

    .line 30
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 31
    :goto_1
    if-nez v1, :cond_2

    .line 32
    .line 33
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_1:Ljava/lang/String;

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    :cond_2
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_2:Ljava/lang/String;

    .line 39
    .line 40
    if-eqz v1, :cond_4

    .line 41
    .line 42
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    if-nez v1, :cond_3

    .line 47
    .line 48
    goto :goto_2

    .line 49
    :cond_3
    const/4 v1, 0x0

    .line 50
    goto :goto_3

    .line 51
    :cond_4
    :goto_2
    const/4 v1, 0x1

    .line 52
    :goto_3
    if-nez v1, :cond_5

    .line 53
    .line 54
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_2:Ljava/lang/String;

    .line 55
    .line 56
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    :cond_5
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_3:Ljava/lang/String;

    .line 60
    .line 61
    if-eqz v1, :cond_7

    .line 62
    .line 63
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    if-nez v1, :cond_6

    .line 68
    .line 69
    goto :goto_4

    .line 70
    :cond_6
    const/4 v1, 0x0

    .line 71
    goto :goto_5

    .line 72
    :cond_7
    :goto_4
    const/4 v1, 0x1

    .line 73
    :goto_5
    if-nez v1, :cond_8

    .line 74
    .line 75
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_3:Ljava/lang/String;

    .line 76
    .line 77
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    .line 79
    .line 80
    :cond_8
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_4:Ljava/lang/String;

    .line 81
    .line 82
    if-eqz v1, :cond_a

    .line 83
    .line 84
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    if-nez v1, :cond_9

    .line 89
    .line 90
    goto :goto_6

    .line 91
    :cond_9
    const/4 v1, 0x0

    .line 92
    goto :goto_7

    .line 93
    :cond_a
    :goto_6
    const/4 v1, 0x1

    .line 94
    :goto_7
    if-nez v1, :cond_b

    .line 95
    .line 96
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_4:Ljava/lang/String;

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    :cond_b
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_5:Ljava/lang/String;

    .line 102
    .line 103
    if-eqz v1, :cond_d

    .line 104
    .line 105
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    if-nez v1, :cond_c

    .line 110
    .line 111
    goto :goto_8

    .line 112
    :cond_c
    const/4 v1, 0x0

    .line 113
    goto :goto_9

    .line 114
    :cond_d
    :goto_8
    const/4 v1, 0x1

    .line 115
    :goto_9
    if-nez v1, :cond_e

    .line 116
    .line 117
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_5:Ljava/lang/String;

    .line 118
    .line 119
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    .line 121
    .line 122
    :cond_e
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_6:Ljava/lang/String;

    .line 123
    .line 124
    if-eqz v1, :cond_10

    .line 125
    .line 126
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 127
    .line 128
    .line 129
    move-result v1

    .line 130
    if-nez v1, :cond_f

    .line 131
    .line 132
    goto :goto_a

    .line 133
    :cond_f
    const/4 v1, 0x0

    .line 134
    goto :goto_b

    .line 135
    :cond_10
    :goto_a
    const/4 v1, 0x1

    .line 136
    :goto_b
    if-nez v1, :cond_11

    .line 137
    .line 138
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_6:Ljava/lang/String;

    .line 139
    .line 140
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    :cond_11
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_7:Ljava/lang/String;

    .line 144
    .line 145
    if-eqz v1, :cond_13

    .line 146
    .line 147
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    if-nez v1, :cond_12

    .line 152
    .line 153
    goto :goto_c

    .line 154
    :cond_12
    const/4 v1, 0x0

    .line 155
    goto :goto_d

    .line 156
    :cond_13
    :goto_c
    const/4 v1, 0x1

    .line 157
    :goto_d
    if-nez v1, :cond_14

    .line 158
    .line 159
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_7:Ljava/lang/String;

    .line 160
    .line 161
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    .line 163
    .line 164
    :cond_14
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_8:Ljava/lang/String;

    .line 165
    .line 166
    if-eqz v1, :cond_16

    .line 167
    .line 168
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 169
    .line 170
    .line 171
    move-result v1

    .line 172
    if-nez v1, :cond_15

    .line 173
    .line 174
    goto :goto_e

    .line 175
    :cond_15
    const/4 v1, 0x0

    .line 176
    goto :goto_f

    .line 177
    :cond_16
    :goto_e
    const/4 v1, 0x1

    .line 178
    :goto_f
    if-nez v1, :cond_17

    .line 179
    .line 180
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_8:Ljava/lang/String;

    .line 181
    .line 182
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    .line 184
    .line 185
    :cond_17
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_9:Ljava/lang/String;

    .line 186
    .line 187
    if-eqz v1, :cond_19

    .line 188
    .line 189
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 190
    .line 191
    .line 192
    move-result v1

    .line 193
    if-nez v1, :cond_18

    .line 194
    .line 195
    goto :goto_10

    .line 196
    :cond_18
    const/4 v1, 0x0

    .line 197
    goto :goto_11

    .line 198
    :cond_19
    :goto_10
    const/4 v1, 0x1

    .line 199
    :goto_11
    if-nez v1, :cond_1a

    .line 200
    .line 201
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_9:Ljava/lang/String;

    .line 202
    .line 203
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    .line 205
    .line 206
    :cond_1a
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_10:Ljava/lang/String;

    .line 207
    .line 208
    if-eqz v1, :cond_1b

    .line 209
    .line 210
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 211
    .line 212
    .line 213
    move-result v1

    .line 214
    if-nez v1, :cond_1c

    .line 215
    .line 216
    :cond_1b
    const/4 v2, 0x1

    .line 217
    :cond_1c
    if-nez v2, :cond_1d

    .line 218
    .line 219
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$DropCnlBanner;->banner_10:Ljava/lang/String;

    .line 220
    .line 221
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    .line 223
    .line 224
    :cond_1d
    return-object v0
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method


# virtual methods
.method public final Oooo8o0〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, ""

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;->product_id:Ljava/lang/String;

    .line 12
    .line 13
    :goto_0
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO(Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;ILjava/lang/Boolean;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;->product_id:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v1, 0x0

    .line 9
    :goto_0
    if-nez v1, :cond_1

    .line 10
    .line 11
    const-string v1, ""

    .line 12
    .line 13
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v3, "mOnProductChosen\tproductId="

    .line 19
    .line 20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, "\tpayType="

    .line 27
    .line 28
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v1, "\tisComplianceChecked="

    .line 35
    .line 36
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    iput-object p1, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 50
    .line 51
    iput p2, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->〇OOo8〇0:I

    .line 52
    .line 53
    iput-object p3, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->OO:Ljava/lang/Boolean;

    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final oo〇()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;->price_description:Ljava/lang/String;

    .line 12
    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    move-object v1, v0

    .line 17
    :goto_0
    return-object v1
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0OOo〇0()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;->is_show_trial_description:I

    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    if-ne v0, v2, :cond_1

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    :cond_1
    :goto_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o8o〇()Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->Oooo8o0〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return-object v1

    .line 9
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    iget-object v2, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->product_description:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription;

    .line 18
    .line 19
    if-nez v2, :cond_1

    .line 20
    .line 21
    return-object v1

    .line 22
    :cond_1
    iget-object v3, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription;->trial_rule_list:Ljava/util/List;

    .line 23
    .line 24
    check-cast v3, Ljava/util/Collection;

    .line 25
    .line 26
    const/4 v4, 0x0

    .line 27
    const/4 v5, 0x1

    .line 28
    if-eqz v3, :cond_3

    .line 29
    .line 30
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    if-eqz v3, :cond_2

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_2
    const/4 v3, 0x0

    .line 38
    goto :goto_1

    .line 39
    :cond_3
    :goto_0
    const/4 v3, 0x1

    .line 40
    :goto_1
    if-eqz v3, :cond_4

    .line 41
    .line 42
    return-object v1

    .line 43
    :cond_4
    iget-object v2, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription;->trial_rule_list:Ljava/util/List;

    .line 44
    .line 45
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    const/4 v6, 0x0

    .line 50
    :goto_2
    if-ge v6, v3, :cond_9

    .line 51
    .line 52
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v7

    .line 56
    check-cast v7, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 57
    .line 58
    iget-object v7, v7, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->product_id:Ljava/lang/String;

    .line 59
    .line 60
    if-eqz v7, :cond_6

    .line 61
    .line 62
    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    .line 63
    .line 64
    .line 65
    move-result v7

    .line 66
    if-nez v7, :cond_5

    .line 67
    .line 68
    goto :goto_3

    .line 69
    :cond_5
    const/4 v7, 0x0

    .line 70
    goto :goto_4

    .line 71
    :cond_6
    :goto_3
    const/4 v7, 0x1

    .line 72
    :goto_4
    if-eqz v7, :cond_7

    .line 73
    .line 74
    goto :goto_5

    .line 75
    :cond_7
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 76
    .line 77
    .line 78
    move-result-object v7

    .line 79
    check-cast v7, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 80
    .line 81
    iget-object v7, v7, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->product_id:Ljava/lang/String;

    .line 82
    .line 83
    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    move-result v7

    .line 87
    if-eqz v7, :cond_8

    .line 88
    .line 89
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    check-cast v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 94
    .line 95
    return-object v0

    .line 96
    :cond_8
    :goto_5
    add-int/lit8 v6, v6, 0x1

    .line 97
    .line 98
    goto :goto_2

    .line 99
    :cond_9
    return-object v1
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇O00(Z)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/guide/dropchannel/IDropCnlType;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->oo88o8O()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->O8ooOoo〇()Ljava/util/List;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    :goto_0
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇oo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->〇OOo8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇〇0〇〇0()Ljava/lang/Boolean;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/dropchannel/viewmodel/DropCnlConfigViewModel;->OO:Ljava/lang/Boolean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

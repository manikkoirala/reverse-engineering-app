.class public final enum Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;
.super Ljava/lang/Enum;
.source "GuideGpGray4NewNormalStyleFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NewGpGuideBannerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum FRAGMENT_STYLE_1:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum FRAGMENT_STYLE_2:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum FRAGMENT_STYLE_3:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;


# instance fields
.field private final newGuideInfoItem:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;
    .locals 3

    .line 1
    const/4 v0, 0x3

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->FRAGMENT_STYLE_1:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->FRAGMENT_STYLE_2:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->FRAGMENT_STYLE_3:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;

    .line 4
    .line 5
    const v2, 0x7f130fbf

    .line 6
    .line 7
    .line 8
    const v3, 0x7f130be7

    .line 9
    .line 10
    .line 11
    const v4, 0x7f080eed

    .line 12
    .line 13
    .line 14
    const v5, 0x7f130fbe

    .line 15
    .line 16
    .line 17
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 18
    .line 19
    .line 20
    const-string v2, "FRAGMENT_STYLE_1"

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;)V

    .line 24
    .line 25
    .line 26
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->FRAGMENT_STYLE_1:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 29
    .line 30
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;

    .line 31
    .line 32
    const v2, 0x7f130fc1

    .line 33
    .line 34
    .line 35
    const v3, 0x7f130bea

    .line 36
    .line 37
    .line 38
    const v4, 0x7f080ef0

    .line 39
    .line 40
    .line 41
    const v5, 0x7f130fc0

    .line 42
    .line 43
    .line 44
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 45
    .line 46
    .line 47
    const-string v2, "FRAGMENT_STYLE_2"

    .line 48
    .line 49
    const/4 v3, 0x1

    .line 50
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;)V

    .line 51
    .line 52
    .line 53
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->FRAGMENT_STYLE_2:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 54
    .line 55
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 56
    .line 57
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;

    .line 58
    .line 59
    const v2, 0x7f130fc3

    .line 60
    .line 61
    .line 62
    const v3, 0x7f130901

    .line 63
    .line 64
    .line 65
    const v4, 0x7f080ef3

    .line 66
    .line 67
    .line 68
    const v5, 0x7f130fc2

    .line 69
    .line 70
    .line 71
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 72
    .line 73
    .line 74
    const-string v2, "FRAGMENT_STYLE_3"

    .line 75
    .line 76
    const/4 v3, 0x2

    .line 77
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;)V

    .line 78
    .line 79
    .line 80
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->FRAGMENT_STYLE_3:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 81
    .line 82
    invoke-static {}, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->$values()[Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->$VALUES:[Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 87
    .line 88
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->newGuideInfoItem:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->$VALUES:[Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getNewGuideInfoItem()Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGpGuideBannerType;->newGuideInfoItem:Lcom/intsig/camscanner/guide/GuideGpGray4NewNormalStyleFragment$NewGuideInfoItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

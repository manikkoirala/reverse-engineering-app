.class public final enum Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;
.super Ljava/lang/Enum;
.source "GuideGpNewNormalStyleFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NewGpGuideBannerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum TYPE_HANDY:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum TYPE_ID_CARD:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum TYPE_MINUTE:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum TYPE_OCR:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum TYPE_PDF:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum TYPE_PPT:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

.field public static final enum TYPE_TIDY:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;


# instance fields
.field private final newGuideInfoItem:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;
    .locals 3

    .line 1
    const/4 v0, 0x7

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_HANDY:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_TIDY:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_PDF:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_MINUTE:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    const/4 v1, 0x4

    .line 25
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_OCR:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 26
    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    const/4 v1, 0x5

    .line 30
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_ID_CARD:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 31
    .line 32
    aput-object v2, v0, v1

    .line 33
    .line 34
    const/4 v1, 0x6

    .line 35
    sget-object v2, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_PPT:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 36
    .line 37
    aput-object v2, v0, v1

    .line 38
    .line 39
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 4
    .line 5
    const v2, 0x7f130be6

    .line 6
    .line 7
    .line 8
    const v3, 0x7f130be7

    .line 9
    .line 10
    .line 11
    const v4, 0x7f080ef8

    .line 12
    .line 13
    .line 14
    const v5, 0x7f130be5

    .line 15
    .line 16
    .line 17
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 18
    .line 19
    .line 20
    const-string v2, "TYPE_HANDY"

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;)V

    .line 24
    .line 25
    .line 26
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_HANDY:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 29
    .line 30
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 31
    .line 32
    const v2, 0x7f130be9

    .line 33
    .line 34
    .line 35
    const v3, 0x7f130bea

    .line 36
    .line 37
    .line 38
    const v4, 0x7f080f4b

    .line 39
    .line 40
    .line 41
    const v5, 0x7f130be8

    .line 42
    .line 43
    .line 44
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 45
    .line 46
    .line 47
    const-string v2, "TYPE_TIDY"

    .line 48
    .line 49
    const/4 v3, 0x1

    .line 50
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;)V

    .line 51
    .line 52
    .line 53
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_TIDY:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 54
    .line 55
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 56
    .line 57
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 58
    .line 59
    const v2, 0x7f130900

    .line 60
    .line 61
    .line 62
    const v3, 0x7f130901

    .line 63
    .line 64
    .line 65
    const v4, 0x7f080eac

    .line 66
    .line 67
    .line 68
    const v5, 0x7f1308ff

    .line 69
    .line 70
    .line 71
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 72
    .line 73
    .line 74
    const-string v2, "TYPE_PDF"

    .line 75
    .line 76
    const/4 v3, 0x2

    .line 77
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;)V

    .line 78
    .line 79
    .line 80
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_PDF:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 81
    .line 82
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 83
    .line 84
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 85
    .line 86
    const v2, 0x7f13090a

    .line 87
    .line 88
    .line 89
    const v3, 0x7f13090b

    .line 90
    .line 91
    .line 92
    const v4, 0x7f080f0a

    .line 93
    .line 94
    .line 95
    const v5, 0x7f130908

    .line 96
    .line 97
    .line 98
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 99
    .line 100
    .line 101
    const-string v2, "TYPE_MINUTE"

    .line 102
    .line 103
    const/4 v3, 0x3

    .line 104
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;)V

    .line 105
    .line 106
    .line 107
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_MINUTE:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 108
    .line 109
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 110
    .line 111
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 112
    .line 113
    const v2, 0x7f130903

    .line 114
    .line 115
    .line 116
    const v3, 0x7f130904

    .line 117
    .line 118
    .line 119
    const v4, 0x7f080edc

    .line 120
    .line 121
    .line 122
    const v5, 0x7f130902

    .line 123
    .line 124
    .line 125
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 126
    .line 127
    .line 128
    const-string v2, "TYPE_OCR"

    .line 129
    .line 130
    const/4 v3, 0x4

    .line 131
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;)V

    .line 132
    .line 133
    .line 134
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_OCR:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 135
    .line 136
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 137
    .line 138
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 139
    .line 140
    const v2, 0x7f130bec

    .line 141
    .line 142
    .line 143
    const v3, 0x7f130bed

    .line 144
    .line 145
    .line 146
    const v4, 0x7f080ede

    .line 147
    .line 148
    .line 149
    const v5, 0x7f130beb

    .line 150
    .line 151
    .line 152
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 153
    .line 154
    .line 155
    const-string v2, "TYPE_ID_CARD"

    .line 156
    .line 157
    const/4 v3, 0x5

    .line 158
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;)V

    .line 159
    .line 160
    .line 161
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_ID_CARD:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 162
    .line 163
    new-instance v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 164
    .line 165
    new-instance v1, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 166
    .line 167
    const v2, 0x7f130bef

    .line 168
    .line 169
    .line 170
    const v3, 0x7f130bf0

    .line 171
    .line 172
    .line 173
    const v4, 0x7f080f48

    .line 174
    .line 175
    .line 176
    const v5, 0x7f130bee

    .line 177
    .line 178
    .line 179
    invoke-direct {v1, v4, v5, v2, v3}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;-><init>(IIII)V

    .line 180
    .line 181
    .line 182
    const-string v2, "TYPE_PPT"

    .line 183
    .line 184
    const/4 v3, 0x6

    .line 185
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;)V

    .line 186
    .line 187
    .line 188
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->TYPE_PPT:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 189
    .line 190
    invoke-static {}, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->$values()[Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    sput-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->$VALUES:[Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 195
    .line 196
    return-void
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->newGuideInfoItem:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->$VALUES:[Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getNewGuideInfoItem()Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGpGuideBannerType;->newGuideInfoItem:Lcom/intsig/camscanner/guide/GuideGpNewNormalStyleFragment$NewGuideInfoItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

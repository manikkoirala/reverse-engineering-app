.class public final Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "HearCnlAverageThreeProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$HearCnlAverageThreeHolder;,
        Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final oOo0:Landroid/graphics/drawable/GradientDrawable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOo〇8o008:Landroid/graphics/drawable/GradientDrawable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇0O:Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:I

.field private final o〇00O:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->〇0O:Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$Companion;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 12
    .line 13
    .line 14
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 15
    .line 16
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    const v3, 0x7f0601e0

    .line 21
    .line 22
    .line 23
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    const/high16 v4, 0x40800000    # 4.0f

    .line 36
    .line 37
    invoke-static {v2, v4}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    float-to-int v2, v2

    .line 42
    int-to-float v2, v2

    .line 43
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const/4 v2, 0x1

    .line 48
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->O8ooOoo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    const v2, 0x7f06024d

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 56
    .line 57
    .line 58
    move-result-object v5

    .line 59
    invoke-static {v5, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->O〇8O8〇008(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    const-string v2, "Builder()\n            .b\u2026lor)\n            .build()"

    .line 72
    .line 73
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    sput-object v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oOo〇8o008:Landroid/graphics/drawable/GradientDrawable;

    .line 77
    .line 78
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 79
    .line 80
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 84
    .line 85
    .line 86
    move-result-object v5

    .line 87
    invoke-static {v5, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 88
    .line 89
    .line 90
    move-result v3

    .line 91
    invoke-virtual {v0, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 96
    .line 97
    .line 98
    move-result-object v3

    .line 99
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 100
    .line 101
    .line 102
    move-result v3

    .line 103
    float-to-int v3, v3

    .line 104
    int-to-float v3, v3

    .line 105
    invoke-virtual {v0, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    const/4 v3, 0x2

    .line 110
    invoke-virtual {v0, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->O8ooOoo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    const v3, 0x7f06024e

    .line 115
    .line 116
    .line 117
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 118
    .line 119
    .line 120
    move-result-object v1

    .line 121
    invoke-static {v1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 122
    .line 123
    .line 124
    move-result v1

    .line 125
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->O〇8O8〇008(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    sput-object v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oOo0:Landroid/graphics/drawable/GradientDrawable;

    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .param p1    # Lkotlin/jvm/functions/Function1;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "listener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->o〇00O:Lkotlin/jvm/functions/Function1;

    .line 10
    .line 11
    const p1, 0x7f0d040f

    .line 12
    .line 13
    .line 14
    iput p1, p0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->〇080OO8〇0:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final O8ooOoo〇(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p3, "$this_run"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "$item"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object p3, p0, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oOo0:Landroid/graphics/drawable/GradientDrawable;

    .line 19
    .line 20
    invoke-virtual {p3, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 21
    .line 22
    .line 23
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 24
    .line 25
    const-string p3, "ivOneYes"

    .line 26
    .line 27
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    const/4 p3, 0x1

    .line 31
    invoke-static {p0, p3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 32
    .line 33
    .line 34
    iget-object p0, p1, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->o〇00O:Lkotlin/jvm/functions/Function1;

    .line 35
    .line 36
    check-cast p2, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;

    .line 37
    .line 38
    invoke-virtual {p2}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->getOneImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getFunctionType()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->〇oOO8O8(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->〇0000OOO(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic o〇O8〇〇o()Landroid/graphics/drawable/GradientDrawable;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oOo0:Landroid/graphics/drawable/GradientDrawable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇00()Landroid/graphics/drawable/GradientDrawable;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oOo〇8o008:Landroid/graphics/drawable/GradientDrawable;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇0000OOO(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p3, "$this_run"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$item"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "this$0"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object p3, p0, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oOo0:Landroid/graphics/drawable/GradientDrawable;

    .line 19
    .line 20
    invoke-virtual {p3, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 21
    .line 22
    .line 23
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 24
    .line 25
    const-string p3, "ivThreeYes"

    .line 26
    .line 27
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    const/4 p3, 0x1

    .line 31
    invoke-static {p0, p3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 32
    .line 33
    .line 34
    check-cast p1, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->getThreeImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    if-eqz p0, :cond_0

    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getFunctionType()I

    .line 43
    .line 44
    .line 45
    move-result p0

    .line 46
    iget-object p1, p2, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->o〇00O:Lkotlin/jvm/functions/Function1;

    .line 47
    .line 48
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    :cond_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final 〇oOO8O8(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p3, "$this_run"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$item"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "this$0"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object p3, p0, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oOo0:Landroid/graphics/drawable/GradientDrawable;

    .line 19
    .line 20
    invoke-virtual {p3, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 21
    .line 22
    .line 23
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->oOo0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 24
    .line 25
    const-string p3, "ivTwoYes"

    .line 26
    .line 27
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    const/4 p3, 0x1

    .line 31
    invoke-static {p0, p3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 32
    .line 33
    .line 34
    check-cast p1, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->getTwoImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 37
    .line 38
    .line 39
    move-result-object p0

    .line 40
    if-eqz p0, :cond_0

    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getFunctionType()I

    .line 43
    .line 44
    .line 45
    move-result p0

    .line 46
    iget-object p1, p2, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->o〇00O:Lkotlin/jvm/functions/Function1;

    .line 47
    .line 48
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    :cond_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->O8ooOoo〇(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$HearCnlAverageThreeHolder;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oO80()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {p1, v0}, Lcom/chad/library/adapter/base/util/AdapterUtilsKt;->〇080(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$HearCnlAverageThreeHolder;-><init>(Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    return-object p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇8O8〇008(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;)V
    .locals 8
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p1, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$HearCnlAverageThreeHolder;

    .line 12
    .line 13
    move-object v0, p2

    .line 14
    check-cast v0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider$HearCnlAverageThreeHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->oOo〇8o008:Landroid/graphics/drawable/GradientDrawable;

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 25
    .line 26
    .line 27
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 30
    .line 31
    .line 32
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 33
    .line 34
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->getOneImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getImageResId()Ljava/lang/Integer;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    const-string v2, "ivOneImage"

    .line 46
    .line 47
    const/4 v3, 0x1

    .line 48
    const/4 v4, 0x0

    .line 49
    if-eqz v1, :cond_0

    .line 50
    .line 51
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    iget-object v5, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 56
    .line 57
    invoke-virtual {v5, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 58
    .line 59
    .line 60
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 61
    .line 62
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-static {v1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 66
    .line 67
    .line 68
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_0
    move-object v1, v4

    .line 72
    :goto_0
    const/4 v5, 0x0

    .line 73
    if-nez v1, :cond_1

    .line 74
    .line 75
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 76
    .line 77
    invoke-virtual {v1, v4}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    .line 79
    .line 80
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 81
    .line 82
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-static {v1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 86
    .line 87
    .line 88
    :cond_1
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 89
    .line 90
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->getOneImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    invoke-virtual {v2}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getTextResId()I

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->getTwoImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    if-eqz v1, :cond_4

    .line 106
    .line 107
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 108
    .line 109
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 110
    .line 111
    .line 112
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 113
    .line 114
    invoke-virtual {v2, v3}, Landroid/view/View;->setClickable(Z)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v1}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getImageResId()Ljava/lang/Integer;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    const-string v6, "ivTwoImage"

    .line 122
    .line 123
    if-eqz v2, :cond_2

    .line 124
    .line 125
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    .line 126
    .line 127
    .line 128
    move-result v2

    .line 129
    iget-object v7, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 130
    .line 131
    invoke-virtual {v7, v2}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 132
    .line 133
    .line 134
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 135
    .line 136
    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    invoke-static {v2, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 140
    .line 141
    .line 142
    sget-object v2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 143
    .line 144
    goto :goto_1

    .line 145
    :cond_2
    move-object v2, v4

    .line 146
    :goto_1
    if-nez v2, :cond_3

    .line 147
    .line 148
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 149
    .line 150
    invoke-virtual {v2, v4}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 151
    .line 152
    .line 153
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 154
    .line 155
    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    invoke-static {v2, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 159
    .line 160
    .line 161
    :cond_3
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 162
    .line 163
    invoke-virtual {v1}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getTextResId()I

    .line 164
    .line 165
    .line 166
    move-result v1

    .line 167
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 168
    .line 169
    .line 170
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 171
    .line 172
    goto :goto_2

    .line 173
    :cond_4
    move-object v1, v4

    .line 174
    :goto_2
    const/4 v2, 0x4

    .line 175
    if-nez v1, :cond_5

    .line 176
    .line 177
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 178
    .line 179
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 180
    .line 181
    .line 182
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 183
    .line 184
    invoke-virtual {v1, v5}, Landroid/view/View;->setClickable(Z)V

    .line 185
    .line 186
    .line 187
    :cond_5
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->getThreeImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 188
    .line 189
    .line 190
    move-result-object v0

    .line 191
    if-eqz v0, :cond_8

    .line 192
    .line 193
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 194
    .line 195
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 196
    .line 197
    .line 198
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 199
    .line 200
    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getImageResId()Ljava/lang/Integer;

    .line 204
    .line 205
    .line 206
    move-result-object v1

    .line 207
    const-string v6, "ivThreeImage"

    .line 208
    .line 209
    if-eqz v1, :cond_6

    .line 210
    .line 211
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 212
    .line 213
    .line 214
    move-result v1

    .line 215
    iget-object v7, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 216
    .line 217
    invoke-virtual {v7, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 218
    .line 219
    .line 220
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 221
    .line 222
    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    .line 224
    .line 225
    invoke-static {v1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 226
    .line 227
    .line 228
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 229
    .line 230
    goto :goto_3

    .line 231
    :cond_6
    move-object v1, v4

    .line 232
    :goto_3
    if-nez v1, :cond_7

    .line 233
    .line 234
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 235
    .line 236
    invoke-virtual {v1, v4}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 237
    .line 238
    .line 239
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 240
    .line 241
    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    .line 243
    .line 244
    invoke-static {v1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 245
    .line 246
    .line 247
    :cond_7
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 248
    .line 249
    invoke-virtual {v0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;->getTextResId()I

    .line 250
    .line 251
    .line 252
    move-result v0

    .line 253
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 254
    .line 255
    .line 256
    sget-object v4, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 257
    .line 258
    :cond_8
    if-nez v4, :cond_9

    .line 259
    .line 260
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 261
    .line 262
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 263
    .line 264
    .line 265
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 266
    .line 267
    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    .line 268
    .line 269
    .line 270
    :cond_9
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 271
    .line 272
    new-instance v1, Lo88o0O/〇080;

    .line 273
    .line 274
    invoke-direct {v1, p1, p0, p2}, Lo88o0O/〇080;-><init>(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;)V

    .line 275
    .line 276
    .line 277
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    .line 279
    .line 280
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 281
    .line 282
    new-instance v1, Lo88o0O/〇o00〇〇Oo;

    .line 283
    .line 284
    invoke-direct {v1, p1, p2, p0}, Lo88o0O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;)V

    .line 285
    .line 286
    .line 287
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    .line 289
    .line 290
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 291
    .line 292
    new-instance v1, Lo88o0O/〇o〇;

    .line 293
    .line 294
    invoke-direct {v1, p1, p2, p0}, Lo88o0O/〇o〇;-><init>(Lcom/intsig/camscanner/databinding/ItemHearCnlAverageThreeBinding;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;)V

    .line 295
    .line 296
    .line 297
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    .line 299
    .line 300
    return-void
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->〇080OO8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->O〇8O8〇008(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/guide/hearcnl/provider/HearCnlAverageThreeProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

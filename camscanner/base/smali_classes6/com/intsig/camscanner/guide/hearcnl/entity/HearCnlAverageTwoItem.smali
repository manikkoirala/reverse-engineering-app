.class public final Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageTwoItem;
.super Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;
.source "HearCnlAverageTwoItem.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final oneImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final twoImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "oneImage"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "twoImage"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x3

    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;-><init>(I)V

    .line 13
    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageTwoItem;->oneImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 16
    .line 17
    iput-object p2, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageTwoItem;->twoImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final getOneImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageTwoItem;->oneImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTwoImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageTwoItem;->twoImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;
.super Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;
.source "HearCnlAverageThreeItem.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final oneImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final threeImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

.field private final twoImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "oneImage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;-><init>(I)V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->oneImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->twoImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 5
    iput-object p3, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->threeImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    move-object p3, v0

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;-><init>(Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;)V

    return-void
.end method


# virtual methods
.method public final getOneImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->oneImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getThreeImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->threeImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTwoImage()Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlAverageThreeItem;->twoImage:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlOneImageTitle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

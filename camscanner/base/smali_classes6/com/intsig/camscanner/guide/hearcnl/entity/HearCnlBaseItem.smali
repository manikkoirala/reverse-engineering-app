.class public Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;
.super Ljava/lang/Object;
.source "HearCnlBaseItem.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Companion:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final TYPE_AVERAGE_THREE:I = 0x0

.field public static final TYPE_AVERAGE_TWO:I = 0x3

.field public static final TYPE_LEFT_LARGE_RIGHT_SMALL:I = 0x1

.field public static final TYPE_LEFT_SMALL_RIGHT_LARGE:I = 0x2


# instance fields
.field private type:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;->Companion:Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;->type:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final getType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;->type:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/guide/hearcnl/entity/HearCnlBaseItem;->type:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

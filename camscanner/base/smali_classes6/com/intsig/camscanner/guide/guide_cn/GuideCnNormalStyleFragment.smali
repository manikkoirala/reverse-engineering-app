.class public final Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "GuideCnNormalStyleFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;,
        Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;,
        Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Landroid/view/View$OnClickListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

.field private o0:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Landroid/view/View$OnClickListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/utils/ClickLimit;

.field private 〇OOo8〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇080OO8〇0:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_EFFICIENT_CONVENIENT:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->o0:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇08O〇00〇o:Lcom/intsig/utils/ClickLimit;

    .line 13
    .line 14
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/〇080;

    .line 15
    .line 16
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/guide/guide_cn/〇080;-><init>(Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;)V

    .line 17
    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->o〇00O:Landroid/view/View$OnClickListener;

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/〇o00〇〇Oo;

    .line 22
    .line 23
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/guide/guide_cn/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;)V

    .line 24
    .line 25
    .line 26
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->O8o08O8O:Landroid/view/View$OnClickListener;

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O0〇0(Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇08O〇00〇o:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    sget-wide v1, Lcom/intsig/utils/ClickLimit;->〇o〇:J

    .line 9
    .line 10
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    const-string v0, "GuideCnNormalStyleFragment"

    .line 15
    .line 16
    if-nez p1, :cond_0

    .line 17
    .line 18
    const-string p0, "useNowListener click too fast"

    .line 19
    .line 20
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void

    .line 24
    :cond_0
    const/4 p1, 0x1

    .line 25
    invoke-static {p1}, Lcom/intsig/camscanner/guide/tracker/CsGuideTracker$Normal;->〇o〇(Z)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 29
    .line 30
    .line 31
    move-result-object p0

    .line 32
    const-string p1, "null cannot be cast to non-null type com.intsig.camscanner.guide.GuideGpActivity"

    .line 33
    .line 34
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    check-cast p0, Lcom/intsig/camscanner/guide/GuideGpActivity;

    .line 38
    .line 39
    iget-object p0, p0, Lcom/intsig/camscanner/guide/GuideGpActivity;->o8oOOo:Lcom/intsig/camscanner/guide/IGuideGpPresenter;

    .line 40
    .line 41
    if-eqz p0, :cond_1

    .line 42
    .line 43
    invoke-interface {p0}, Lcom/intsig/camscanner/guide/IGuideGpPresenter;->〇080()V

    .line 44
    .line 45
    .line 46
    sget-object p0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    const/4 p0, 0x0

    .line 50
    :goto_0
    if-nez p0, :cond_2

    .line 51
    .line 52
    const-string p0, "useNowListener presenter null"

    .line 53
    .line 54
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    :cond_2
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final o00〇88〇08(Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇08O〇00〇o:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    sget-wide v1, Lcom/intsig/utils/ClickLimit;->〇o〇:J

    .line 9
    .line 10
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    const-string v0, "GuideCnNormalStyleFragment"

    .line 15
    .line 16
    if-nez p1, :cond_0

    .line 17
    .line 18
    const-string p0, "registerListener click too fast"

    .line 19
    .line 20
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void

    .line 24
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 25
    .line 26
    .line 27
    move-result-object p0

    .line 28
    const-string p1, "null cannot be cast to non-null type com.intsig.camscanner.guide.GuideGpActivity"

    .line 29
    .line 30
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    check-cast p0, Lcom/intsig/camscanner/guide/GuideGpActivity;

    .line 34
    .line 35
    iget-object p0, p0, Lcom/intsig/camscanner/guide/GuideGpActivity;->o8oOOo:Lcom/intsig/camscanner/guide/IGuideGpPresenter;

    .line 36
    .line 37
    if-eqz p0, :cond_1

    .line 38
    .line 39
    invoke-interface {p0}, Lcom/intsig/camscanner/guide/IGuideGpPresenter;->oO80()V

    .line 40
    .line 41
    .line 42
    sget-object p0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    const/4 p0, 0x0

    .line 46
    :goto_0
    if-nez p0, :cond_2

    .line 47
    .line 48
    const-string p0, "registerListener presenter null"

    .line 49
    .line 50
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    :cond_2
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final o880(ZLcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;)Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;
    .locals 1
    .param p1    # Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇080OO8〇0:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Companion;->〇080(ZLcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;)Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->O0〇0(Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->OO:Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 2
    .line 3
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->o00〇88〇08(Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇o0〇8()V
    .locals 3

    .line 1
    new-instance v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, -0x2

    .line 5
    invoke-direct {v0, v1, v2}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    .line 6
    .line 7
    .line 8
    const v1, 0x7f0a06fc

    .line 9
    .line 10
    .line 11
    iput v1, v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->startToStart:I

    .line 12
    .line 13
    const v1, 0x7f0a06fe

    .line 14
    .line 15
    .line 16
    iput v1, v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->endToEnd:I

    .line 17
    .line 18
    const v1, 0x7f0a04d0

    .line 19
    .line 20
    .line 21
    iput v1, v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->bottomToBottom:I

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 28
    .line 29
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public getIntentData(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->getIntentData(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    const-string v0, "info"

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.guide.guide_cn.GuideCnNormalStyleFragment.BannerType"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    check-cast v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->o0:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 20
    .line 21
    const-string v0, "extra_is_last_page"

    .line 22
    .line 23
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    iput-boolean p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇OOo8〇0:Z

    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 6

    .line 1
    const-string p1, "GuideCnNormalStyleFragment"

    .line 2
    .line 3
    const-string v0, "initialize>>>"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 9
    .line 10
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->OO:Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 15
    .line 16
    iget-boolean p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇OOo8〇0:Z

    .line 17
    .line 18
    const/16 v0, 0x780

    .line 19
    .line 20
    const/16 v1, 0x8

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    const/4 v3, 0x1

    .line 24
    if-ne p1, v3, :cond_3

    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->oOo0:Landroid/widget/RelativeLayout;

    .line 31
    .line 32
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-gt p1, v0, :cond_0

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 50
    .line 51
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 60
    .line 61
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 69
    .line 70
    .line 71
    move-result p1

    .line 72
    const/16 v4, 0x7d0

    .line 73
    .line 74
    if-le p1, v4, :cond_1

    .line 75
    .line 76
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇〇o0〇8()V

    .line 77
    .line 78
    .line 79
    :cond_1
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    iget p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->skip_login:I

    .line 84
    .line 85
    const-string v4, "is_configuration"

    .line 86
    .line 87
    const-string v5, "CSGuide"

    .line 88
    .line 89
    if-ne p1, v3, :cond_2

    .line 90
    .line 91
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 96
    .line 97
    iget-object v3, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->o〇00O:Landroid/view/View$OnClickListener;

    .line 98
    .line 99
    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    .line 101
    .line 102
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 107
    .line 108
    const v3, 0x7f1308f4

    .line 109
    .line 110
    .line 111
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 112
    .line 113
    .line 114
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 119
    .line 120
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 121
    .line 122
    .line 123
    const-string p1, "1"

    .line 124
    .line 125
    invoke-static {v5, v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    goto :goto_1

    .line 129
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 134
    .line 135
    const v3, 0x7f130175

    .line 136
    .line 137
    .line 138
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 139
    .line 140
    .line 141
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 146
    .line 147
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 148
    .line 149
    .line 150
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 151
    .line 152
    .line 153
    move-result-object p1

    .line 154
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 155
    .line 156
    iget-object v3, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->O8o08O8O:Landroid/view/View$OnClickListener;

    .line 157
    .line 158
    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    .line 160
    .line 161
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 162
    .line 163
    .line 164
    move-result-object p1

    .line 165
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 166
    .line 167
    iget-object v3, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->o〇00O:Landroid/view/View$OnClickListener;

    .line 168
    .line 169
    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    .line 171
    .line 172
    const-string p1, "0"

    .line 173
    .line 174
    invoke-static {v5, v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    goto :goto_1

    .line 178
    :cond_3
    if-nez p1, :cond_4

    .line 179
    .line 180
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 181
    .line 182
    .line 183
    move-result-object p1

    .line 184
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->oOo0:Landroid/widget/RelativeLayout;

    .line 185
    .line 186
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 187
    .line 188
    .line 189
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 190
    .line 191
    .line 192
    move-result-object p1

    .line 193
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 194
    .line 195
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 196
    .line 197
    .line 198
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->o0:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 199
    .line 200
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->getItem()Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    invoke-static {p0}, Lcom/bumptech/glide/Glide;->〇O888o0o(Landroidx/fragment/app/Fragment;)Lcom/bumptech/glide/RequestManager;

    .line 205
    .line 206
    .line 207
    move-result-object v3

    .line 208
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;->〇o00〇〇Oo()I

    .line 209
    .line 210
    .line 211
    move-result v4

    .line 212
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 213
    .line 214
    .line 215
    move-result-object v4

    .line 216
    invoke-virtual {v3, v4}, Lcom/bumptech/glide/RequestManager;->Oooo8o0〇(Ljava/lang/Integer;)Lcom/bumptech/glide/RequestBuilder;

    .line 217
    .line 218
    .line 219
    move-result-object v3

    .line 220
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 221
    .line 222
    .line 223
    move-result-object v4

    .line 224
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 225
    .line 226
    invoke-virtual {v3, v4}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 227
    .line 228
    .line 229
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 230
    .line 231
    .line 232
    move-result-object v3

    .line 233
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 234
    .line 235
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;->Oo08()I

    .line 236
    .line 237
    .line 238
    move-result v4

    .line 239
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 240
    .line 241
    .line 242
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 243
    .line 244
    .line 245
    move-result-object v3

    .line 246
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 247
    .line 248
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;->O8()I

    .line 249
    .line 250
    .line 251
    move-result v4

    .line 252
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 253
    .line 254
    .line 255
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;->〇o〇()Z

    .line 256
    .line 257
    .line 258
    move-result v3

    .line 259
    if-eqz v3, :cond_6

    .line 260
    .line 261
    iget-boolean v3, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇OOo8〇0:Z

    .line 262
    .line 263
    if-eqz v3, :cond_5

    .line 264
    .line 265
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 266
    .line 267
    .line 268
    move-result-object v3

    .line 269
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 270
    .line 271
    .line 272
    move-result v3

    .line 273
    if-le v3, v0, :cond_6

    .line 274
    .line 275
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 276
    .line 277
    .line 278
    move-result-object v0

    .line 279
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 280
    .line 281
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 282
    .line 283
    .line 284
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 285
    .line 286
    .line 287
    move-result-object v0

    .line 288
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 289
    .line 290
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;->〇080()I

    .line 291
    .line 292
    .line 293
    move-result v1

    .line 294
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 295
    .line 296
    .line 297
    goto :goto_2

    .line 298
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 299
    .line 300
    .line 301
    move-result-object v0

    .line 302
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 303
    .line 304
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 305
    .line 306
    .line 307
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 308
    .line 309
    .line 310
    move-result-object v0

    .line 311
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 312
    .line 313
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;->o〇0()Ljava/lang/String;

    .line 314
    .line 315
    .line 316
    move-result-object v1

    .line 317
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 318
    .line 319
    .line 320
    move-result v1

    .line 321
    const/16 v2, 0xa

    .line 322
    .line 323
    if-le v1, v2, :cond_7

    .line 324
    .line 325
    const/high16 v1, 0x42200000    # 40.0f

    .line 326
    .line 327
    goto :goto_3

    .line 328
    :cond_7
    const/high16 v1, 0x42580000    # 54.0f

    .line 329
    .line 330
    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 331
    .line 332
    .line 333
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->oooO888()Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 334
    .line 335
    .line 336
    move-result-object v0

    .line 337
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 338
    .line 339
    invoke-virtual {p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;->o〇0()Ljava/lang/String;

    .line 340
    .line 341
    .line 342
    move-result-object p1

    .line 343
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 344
    .line 345
    .line 346
    return-void
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "v"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onDestroyView()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->OO:Lcom/intsig/camscanner/databinding/FragmentGuideCnNormalBinding;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;->〇OOo8〇0:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const-string v0, "is_register_show"

    .line 9
    .line 10
    const-string v1, "1"

    .line 11
    .line 12
    const-string v2, "CSGuide"

    .line 13
    .line 14
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d02df

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final enum Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;
.super Ljava/lang/Enum;
.source "GuideCnNormalStyleFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BannerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

.field public static final enum TYPE_CLEAR_SHARP:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

.field public static final enum TYPE_EFFICIENT_CONVENIENT:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

.field public static final enum TYPE_HIGH_FIDELITY:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

.field public static final enum TYPE_LOGIN:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

.field public static final enum TYPE_MOBILE_OFFICE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

.field public static final enum TYPE_OCR:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

.field public static final enum TYPE_PICTURE_QUALITY_EXQUISITE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

.field public static final enum TYPE_TIDY_STORAGE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;


# instance fields
.field private final item:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;
    .locals 3

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_EFFICIENT_CONVENIENT:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_HIGH_FIDELITY:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_TIDY_STORAGE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_CLEAR_SHARP:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_OCR:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_MOBILE_OFFICE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_LOGIN:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_PICTURE_QUALITY_EXQUISITE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 11

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 2
    .line 3
    new-instance v10, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 4
    .line 5
    const v2, 0x7f080487

    .line 6
    .line 7
    .line 8
    const v3, 0x7f1308fe

    .line 9
    .line 10
    .line 11
    const v4, 0x7f130909

    .line 12
    .line 13
    .line 14
    const v5, 0x7f13090c

    .line 15
    .line 16
    .line 17
    const-string v6, "CONVENIENT"

    .line 18
    .line 19
    const/4 v7, 0x0

    .line 20
    const/16 v8, 0x20

    .line 21
    .line 22
    const/4 v9, 0x0

    .line 23
    move-object v1, v10

    .line 24
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;-><init>(IIIILjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 25
    .line 26
    .line 27
    const-string v1, "TYPE_EFFICIENT_CONVENIENT"

    .line 28
    .line 29
    const/4 v2, 0x0

    .line 30
    invoke-direct {v0, v1, v2, v10}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V

    .line 31
    .line 32
    .line 33
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_EFFICIENT_CONVENIENT:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 34
    .line 35
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 36
    .line 37
    new-instance v10, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 38
    .line 39
    const v2, 0x7f080488

    .line 40
    .line 41
    .line 42
    const v3, 0x7f130910

    .line 43
    .line 44
    .line 45
    const v4, 0x7f130911

    .line 46
    .line 47
    .line 48
    const v5, 0x7f130912

    .line 49
    .line 50
    .line 51
    const-string v6, "PRACTICAL"

    .line 52
    .line 53
    move-object v1, v10

    .line 54
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;-><init>(IIIILjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 55
    .line 56
    .line 57
    const-string v1, "TYPE_HIGH_FIDELITY"

    .line 58
    .line 59
    const/4 v2, 0x1

    .line 60
    invoke-direct {v0, v1, v2, v10}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V

    .line 61
    .line 62
    .line 63
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_HIGH_FIDELITY:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 64
    .line 65
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 66
    .line 67
    new-instance v10, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 68
    .line 69
    const v2, 0x7f08048c

    .line 70
    .line 71
    .line 72
    const v3, 0x7f13090d

    .line 73
    .line 74
    .line 75
    const v4, 0x7f13090e

    .line 76
    .line 77
    .line 78
    const v5, 0x7f13090f

    .line 79
    .line 80
    .line 81
    const-string v6, "ORGANIZE"

    .line 82
    .line 83
    move-object v1, v10

    .line 84
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;-><init>(IIIILjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 85
    .line 86
    .line 87
    const-string v1, "TYPE_TIDY_STORAGE"

    .line 88
    .line 89
    const/4 v2, 0x2

    .line 90
    invoke-direct {v0, v1, v2, v10}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V

    .line 91
    .line 92
    .line 93
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_TIDY_STORAGE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 94
    .line 95
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 96
    .line 97
    new-instance v10, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 98
    .line 99
    const v2, 0x7f080486

    .line 100
    .line 101
    .line 102
    const v3, 0x7f130905

    .line 103
    .line 104
    .line 105
    const v4, 0x7f130906

    .line 106
    .line 107
    .line 108
    const v5, 0x7f130907

    .line 109
    .line 110
    .line 111
    const-string v6, "SHARP"

    .line 112
    .line 113
    move-object v1, v10

    .line 114
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;-><init>(IIIILjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 115
    .line 116
    .line 117
    const-string v1, "TYPE_CLEAR_SHARP"

    .line 118
    .line 119
    const/4 v2, 0x3

    .line 120
    invoke-direct {v0, v1, v2, v10}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V

    .line 121
    .line 122
    .line 123
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_CLEAR_SHARP:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 124
    .line 125
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 126
    .line 127
    new-instance v10, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 128
    .line 129
    const v2, 0x7f08048b

    .line 130
    .line 131
    .line 132
    const v3, 0x7f130902

    .line 133
    .line 134
    .line 135
    const v4, 0x7f130903

    .line 136
    .line 137
    .line 138
    const v5, 0x7f130904

    .line 139
    .line 140
    .line 141
    const-string v6, "EXACT"

    .line 142
    .line 143
    move-object v1, v10

    .line 144
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;-><init>(IIIILjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 145
    .line 146
    .line 147
    const-string v1, "TYPE_OCR"

    .line 148
    .line 149
    const/4 v2, 0x4

    .line 150
    invoke-direct {v0, v1, v2, v10}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V

    .line 151
    .line 152
    .line 153
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_OCR:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 154
    .line 155
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 156
    .line 157
    new-instance v10, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 158
    .line 159
    const v2, 0x7f08048a

    .line 160
    .line 161
    .line 162
    const v3, 0x7f1308ff

    .line 163
    .line 164
    .line 165
    const v4, 0x7f130900

    .line 166
    .line 167
    .line 168
    const v5, 0x7f130901

    .line 169
    .line 170
    .line 171
    const-string v6, "ALL-IN-ONE"

    .line 172
    .line 173
    move-object v1, v10

    .line 174
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;-><init>(IIIILjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 175
    .line 176
    .line 177
    const-string v1, "TYPE_MOBILE_OFFICE"

    .line 178
    .line 179
    const/4 v2, 0x5

    .line 180
    invoke-direct {v0, v1, v2, v10}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V

    .line 181
    .line 182
    .line 183
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_MOBILE_OFFICE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 184
    .line 185
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 186
    .line 187
    new-instance v8, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 188
    .line 189
    const v2, 0x7f080489

    .line 190
    .line 191
    .line 192
    const v3, 0x7f130c21

    .line 193
    .line 194
    .line 195
    const v4, 0x7f130c22

    .line 196
    .line 197
    .line 198
    const/4 v5, -0x1

    .line 199
    const-string v6, "LOGIN ACCOUNT"

    .line 200
    .line 201
    move-object v1, v8

    .line 202
    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;-><init>(IIIILjava/lang/String;Z)V

    .line 203
    .line 204
    .line 205
    const-string v1, "TYPE_LOGIN"

    .line 206
    .line 207
    const/4 v2, 0x6

    .line 208
    invoke-direct {v0, v1, v2, v8}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V

    .line 209
    .line 210
    .line 211
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_LOGIN:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 212
    .line 213
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 214
    .line 215
    new-instance v10, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 216
    .line 217
    const v2, 0x7f080f0a

    .line 218
    .line 219
    .line 220
    const v3, 0x7f130908

    .line 221
    .line 222
    .line 223
    const v4, 0x7f13090a

    .line 224
    .line 225
    .line 226
    const v5, 0x7f13090b

    .line 227
    .line 228
    .line 229
    const-string v6, "PERFECTION"

    .line 230
    .line 231
    const/16 v8, 0x20

    .line 232
    .line 233
    move-object v1, v10

    .line 234
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;-><init>(IIIILjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 235
    .line 236
    .line 237
    const-string v1, "TYPE_PICTURE_QUALITY_EXQUISITE"

    .line 238
    .line 239
    const/4 v2, 0x7

    .line 240
    invoke-direct {v0, v1, v2, v10}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V

    .line 241
    .line 242
    .line 243
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->TYPE_PICTURE_QUALITY_EXQUISITE:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 244
    .line 245
    invoke-static {}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->$values()[Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 246
    .line 247
    .line 248
    move-result-object v0

    .line 249
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->$VALUES:[Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 250
    .line 251
    return-void
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->item:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->$VALUES:[Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getItem()Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$BannerType;->item:Lcom/intsig/camscanner/guide/guide_cn/GuideCnNormalStyleFragment$Item;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

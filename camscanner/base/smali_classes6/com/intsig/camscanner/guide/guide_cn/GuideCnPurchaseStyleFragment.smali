.class public final Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "GuideCnPurchaseStyleFragment.kt"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;,
        Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$GuideCnPagerAdapter;,
        Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOo〇8o008:Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

.field private OO:Ljava/lang/Boolean;

.field private o0:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

.field private o〇00O:I

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:Ljava/lang/Integer;

.field private 〇0O:I

.field private 〇OOo8〇0:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$GotoMainListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->oOo〇8o008:Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇080OO8〇0:I

    .line 6
    .line 7
    const v0, 0x19bc9c

    .line 8
    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇0O:I

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O0O0〇(Lcom/intsig/camscanner/purchase/track/PurchaseAction;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSGuidePremium:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionType;->GUIDE_PREMIUM_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->type(Lcom/intsig/camscanner/purchase/track/FunctionType;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-static {v0, p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->〇080(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/camscanner/purchase/track/PurchaseAction;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O8〇8〇O80()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const v1, 0x7f130ac5

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "mActivity.getString(R.string.cs_535_guidetest_2)"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    const/4 v2, 0x1

    .line 18
    new-array v2, v2, [Ljava/lang/Object;

    .line 19
    .line 20
    const/4 v3, 0x0

    .line 21
    aput-object v0, v2, v3

    .line 22
    .line 23
    const v4, 0x7f130ac2

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    const-string v2, "mActivity.getString(R.st\u2026_guidetest_1, partString)"

    .line 31
    .line 32
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 40
    .line 41
    iget v4, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇0O:I

    .line 42
    .line 43
    new-instance v5, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$spannableStringAgreement$1;

    .line 44
    .line 45
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$spannableStringAgreement$1;-><init>(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)V

    .line 46
    .line 47
    .line 48
    invoke-static {v1, v0, v4, v3, v5}, Lcom/intsig/camscanner/util/StringUtil;->〇O〇(Ljava/lang/String;Ljava/lang/String;IZLandroid/text/style/ClickableSpan;)Landroid/text/SpannableString;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    .line 54
    .line 55
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 60
    .line 61
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHighlightColor(I)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 69
    .line 70
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final Ooo8o()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇80〇808〇O()Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideStyleNew;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, -0x1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    const-string v3, "extra_activity_from"

    .line 13
    .line 14
    invoke-virtual {v1, v3, v2}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;I)I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v1, 0x0

    .line 24
    :goto_0
    iput-object v1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇08O〇00〇o:Ljava/lang/Integer;

    .line 25
    .line 26
    if-eqz v1, :cond_1

    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    :cond_1
    const/4 v1, 0x0

    .line 33
    if-gez v2, :cond_2

    .line 34
    .line 35
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    iput-object v2, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇08O〇00〇o:Ljava/lang/Integer;

    .line 40
    .line 41
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇08O〇00〇o:Ljava/lang/Integer;

    .line 42
    .line 43
    if-nez v2, :cond_3

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    const/4 v3, 0x5

    .line 51
    if-ne v2, v3, :cond_4

    .line 52
    .line 53
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->〇080OO8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 58
    .line 59
    const/16 v3, 0x8

    .line 60
    .line 61
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->oOo〇8o008:Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;

    .line 69
    .line 70
    invoke-virtual {v2}, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->〇080()Landroid/widget/LinearLayout;

    .line 71
    .line 72
    .line 73
    move-result-object v2

    .line 74
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 75
    .line 76
    .line 77
    :cond_4
    :goto_1
    const/4 v2, 0x1

    .line 78
    if-nez v0, :cond_5

    .line 79
    .line 80
    const/4 v3, 0x1

    .line 81
    goto :goto_2

    .line 82
    :cond_5
    const/4 v3, 0x0

    .line 83
    :goto_2
    if-ne v3, v2, :cond_6

    .line 84
    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 90
    .line 91
    sget-object v3, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_GUIDE:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 92
    .line 93
    invoke-static {v3}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->Oo08(Lcom/intsig/comm/purchase/entity/ProductEnum;)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v4

    .line 97
    invoke-static {v4}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇08O8o〇0(Ljava/lang/String;)F

    .line 98
    .line 99
    .line 100
    move-result v4

    .line 101
    iget-object v5, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 102
    .line 103
    const v6, 0x7f130a5e

    .line 104
    .line 105
    .line 106
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v5

    .line 110
    const-string v6, "mActivity.getString(R.st\u2026de_first_year_price_desc)"

    .line 111
    .line 112
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    sget-object v6, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 116
    .line 117
    new-array v6, v2, [Ljava/lang/Object;

    .line 118
    .line 119
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 120
    .line 121
    .line 122
    move-result-object v4

    .line 123
    aput-object v4, v6, v1

    .line 124
    .line 125
    invoke-static {v6, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 126
    .line 127
    .line 128
    move-result-object v4

    .line 129
    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v4

    .line 133
    const-string v5, "format(format, *args)"

    .line 134
    .line 135
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    .line 140
    .line 141
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 146
    .line 147
    invoke-static {v3}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->oo88o8O(Lcom/intsig/comm/purchase/entity/ProductEnum;)F

    .line 148
    .line 149
    .line 150
    move-result v3

    .line 151
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 152
    .line 153
    const v6, 0x7f130a61

    .line 154
    .line 155
    .line 156
    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v4

    .line 160
    const-string v6, "mActivity.getString(R.st\u2026e_second_year_price_desc)"

    .line 161
    .line 162
    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    new-array v6, v2, [Ljava/lang/Object;

    .line 166
    .line 167
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 168
    .line 169
    .line 170
    move-result-object v3

    .line 171
    aput-object v3, v6, v1

    .line 172
    .line 173
    invoke-static {v6, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v1

    .line 181
    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    .line 186
    .line 187
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 188
    .line 189
    .line 190
    move-result-object v0

    .line 191
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 192
    .line 193
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 194
    .line 195
    const v2, 0x7f130a62

    .line 196
    .line 197
    .line 198
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v1

    .line 202
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    .line 204
    .line 205
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 206
    .line 207
    .line 208
    move-result-object v0

    .line 209
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 210
    .line 211
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 212
    .line 213
    const v2, 0x7f130a63

    .line 214
    .line 215
    .line 216
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object v1

    .line 220
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    .line 222
    .line 223
    goto :goto_3

    .line 224
    :cond_6
    if-nez v3, :cond_7

    .line 225
    .line 226
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 227
    .line 228
    .line 229
    move-result-object v2

    .line 230
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 231
    .line 232
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideStyleNew;->description:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;

    .line 233
    .line 234
    invoke-static {v2, v1, v3}, Lcom/intsig/camscanner/purchase/utils/PurchaseResHelper;->〇O8o08O(Landroid/widget/TextView;ILcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;)V

    .line 235
    .line 236
    .line 237
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 238
    .line 239
    .line 240
    move-result-object v2

    .line 241
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 242
    .line 243
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideStyleNew;->description2:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;

    .line 244
    .line 245
    invoke-static {v2, v1, v3}, Lcom/intsig/camscanner/purchase/utils/PurchaseResHelper;->〇O8o08O(Landroid/widget/TextView;ILcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;)V

    .line 246
    .line 247
    .line 248
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 249
    .line 250
    .line 251
    move-result-object v2

    .line 252
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 253
    .line 254
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideStyleNew;->button_title1:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;

    .line 255
    .line 256
    invoke-static {v2, v1, v3}, Lcom/intsig/camscanner/purchase/utils/PurchaseResHelper;->〇O8o08O(Landroid/widget/TextView;ILcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;)V

    .line 257
    .line 258
    .line 259
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 260
    .line 261
    .line 262
    move-result-object v2

    .line 263
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 264
    .line 265
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$GuideStyleNew;->button_title2:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;

    .line 266
    .line 267
    invoke-static {v2, v1, v0}, Lcom/intsig/camscanner/purchase/utils/PurchaseResHelper;->〇O8o08O(Landroid/widget/TextView;ILcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;)V

    .line 268
    .line 269
    .line 270
    :cond_7
    :goto_3
    return-void
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇0oO〇oo00(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇088O(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oOoO8OO〇()V
    .locals 10

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSGuidePremium:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    sget-object v2, Lcom/intsig/camscanner/purchase/dialog/LocalBottomPurchaseDialog;->oOo〇8o008:Lcom/intsig/camscanner/purchase/dialog/LocalBottomPurchaseDialog$Companion;

    .line 25
    .line 26
    sget-object v4, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_GUIDE:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 27
    .line 28
    const/4 v5, 0x0

    .line 29
    const/4 v6, 0x0

    .line 30
    const/4 v7, 0x0

    .line 31
    const/16 v8, 0x18

    .line 32
    .line 33
    const/4 v9, 0x0

    .line 34
    invoke-static/range {v2 .. v9}, Lcom/intsig/camscanner/purchase/dialog/LocalBottomPurchaseDialog$Companion;->〇o〇(Lcom/intsig/camscanner/purchase/dialog/LocalBottomPurchaseDialog$Companion;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/comm/purchase/entity/ProductEnum;Ljava/math/BigDecimal;Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/dialog/LocalBottomPurchaseDialog;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/〇〇888;

    .line 39
    .line 40
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/guide/guide_cn/〇〇888;-><init>(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/dialog/LocalBottomPurchaseDialog;->〇o08(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const-string v2, "childFragmentManager"

    .line 51
    .line 52
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/dialog/LocalBottomPurchaseDialog;->〇〇〇00(Landroidx/fragment/app/FragmentManager;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇0ooOOo(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/adapter/BaseRecyclerViewAdapter;Landroid/view/View;ILcom/intsig/comm/purchase/entity/PayItem;I)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p5}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇〇〇0(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/adapter/BaseRecyclerViewAdapter;Landroid/view/View;ILcom/intsig/comm/purchase/entity/PayItem;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final o〇0〇o()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O0O:Landroidx/viewpager/widget/ViewPager;

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/OO0o〇〇〇〇0;

    .line 8
    .line 9
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/guide/guide_cn/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->O8o08O8O:Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 2
    .line 3
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇088O(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 7
    .line 8
    if-eqz p1, :cond_4

    .line 9
    .line 10
    if-eqz p2, :cond_0

    .line 11
    .line 12
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 p1, 0x0

    .line 22
    :goto_0
    const/4 p2, 0x1

    .line 23
    if-nez p1, :cond_1

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-nez v0, :cond_2

    .line 31
    .line 32
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 33
    .line 34
    invoke-virtual {p0, p2}, Landroid/os/Handler;->removeMessages(I)V

    .line 35
    .line 36
    .line 37
    goto :goto_2

    .line 38
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-ne p1, p2, :cond_4

    .line 46
    .line 47
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 48
    .line 49
    invoke-virtual {p0, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    const-wide/16 v0, 0xbb8

    .line 54
    .line 55
    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 56
    .line 57
    .line 58
    :cond_4
    :goto_2
    const/4 p0, 0x0

    .line 59
    return p0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇0oO〇oo00(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 7
    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->OO:Ljava/lang/Boolean;

    .line 9
    .line 10
    const-string v0, "GuideCnPurchaseStyleFragment"

    .line 11
    .line 12
    const-string v1, "RenewalAgreementDialog agreeBuy"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇0o8o(Z)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->oOoO8OO〇()V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇8O0880(Z)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final 〇0ooOOo(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v0, "onPurchaseEnd "

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v0, " skip to last"

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v0, "GuideCnPurchaseStyleFragment"

    .line 29
    .line 30
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    if-eqz p2, :cond_0

    .line 34
    .line 35
    iget-object p0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o0:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 36
    .line 37
    if-eqz p0, :cond_0

    .line 38
    .line 39
    invoke-interface {p0}, Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;->o80ooO()V

    .line 40
    .line 41
    .line 42
    :cond_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇0〇0()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO〇8O8oOo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->OO:Ljava/lang/Boolean;

    .line 10
    .line 11
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 12
    .line 13
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O8o08O8O:Landroid/widget/CheckBox;

    .line 24
    .line 25
    const v1, 0x7f08106d

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O8o08O8O:Landroid/widget/CheckBox;

    .line 37
    .line 38
    const v1, 0x7f0812d4

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 42
    .line 43
    .line 44
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇8O0880(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O8o08O8O:Landroid/widget/CheckBox;

    .line 8
    .line 9
    const v0, 0x7f08106d

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O8o08O8O:Landroid/widget/CheckBox;

    .line 21
    .line 22
    const v0, 0x7f0812d4

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 26
    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇8〇80o()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;

    .line 7
    .line 8
    const v2, 0x7f130984

    .line 9
    .line 10
    .line 11
    const v3, 0x7f130a57

    .line 12
    .line 13
    .line 14
    const v4, 0x7f080176

    .line 15
    .line 16
    .line 17
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;-><init>(III)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;

    .line 24
    .line 25
    const v2, 0x7f130254

    .line 26
    .line 27
    .line 28
    const v3, 0x7f130a58

    .line 29
    .line 30
    .line 31
    const v4, 0x7f080177

    .line 32
    .line 33
    .line 34
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;-><init>(III)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;

    .line 41
    .line 42
    const v2, 0x7f130a5c

    .line 43
    .line 44
    .line 45
    const v3, 0x7f130a59

    .line 46
    .line 47
    .line 48
    const v4, 0x7f080174

    .line 49
    .line 50
    .line 51
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;-><init>(III)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;

    .line 58
    .line 59
    const v2, 0x7f130806

    .line 60
    .line 61
    .line 62
    const v3, 0x7f130a5a

    .line 63
    .line 64
    .line 65
    const v4, 0x7f08017e

    .line 66
    .line 67
    .line 68
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;-><init>(III)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;

    .line 75
    .line 76
    const v2, 0x7f130a5d

    .line 77
    .line 78
    .line 79
    const v3, 0x7f130a5b

    .line 80
    .line 81
    .line 82
    const v4, 0x7f080173

    .line 83
    .line 84
    .line 85
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;-><init>(III)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    return-object v0
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇8〇OOoooo(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O0O:Landroidx/viewpager/widget/ViewPager;

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$changeIndicatorStatusWhenImageAutoScroll$1;

    .line 8
    .line 9
    invoke-direct {v1, p1, p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$changeIndicatorStatusWhenImageAutoScroll$1;-><init>(Ljava/util/ArrayList;Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;Lcom/intsig/adapter/BaseRecyclerViewAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇〇O80〇0o(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;Lcom/intsig/adapter/BaseRecyclerViewAdapter;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇O8〇8000(I)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->oOo〇8o008:Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;

    .line 6
    .line 7
    const-string v1, "binding.llBottomPurchaseStyle2"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const v1, 0x7f0802a6

    .line 13
    .line 14
    .line 15
    const v2, 0x7f0802a4

    .line 16
    .line 17
    .line 18
    const-string v3, "#FF212121"

    .line 19
    .line 20
    const-string v4, "#FFFF7255"

    .line 21
    .line 22
    if-nez p1, :cond_0

    .line 23
    .line 24
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->OO:Landroid/widget/LinearLayout;

    .line 25
    .line 26
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 27
    .line 28
    .line 29
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 30
    .line 31
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 32
    .line 33
    .line 34
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 35
    .line 36
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 41
    .line 42
    .line 43
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->oOo0:Landroid/widget/TextView;

    .line 44
    .line 45
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 50
    .line 51
    .line 52
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 53
    .line 54
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    .line 60
    .line 61
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 62
    .line 63
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 68
    .line 69
    .line 70
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 71
    .line 72
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 77
    .line 78
    .line 79
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->〇0O:Landroid/widget/TextView;

    .line 80
    .line 81
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_0
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 90
    .line 91
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 92
    .line 93
    .line 94
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->OO:Landroid/widget/LinearLayout;

    .line 95
    .line 96
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 97
    .line 98
    .line 99
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->oOo0:Landroid/widget/TextView;

    .line 100
    .line 101
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 102
    .line 103
    .line 104
    move-result v1

    .line 105
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 106
    .line 107
    .line 108
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 109
    .line 110
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 111
    .line 112
    .line 113
    move-result v1

    .line 114
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 115
    .line 116
    .line 117
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 118
    .line 119
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 124
    .line 125
    .line 126
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 127
    .line 128
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 133
    .line 134
    .line 135
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->〇0O:Landroid/widget/TextView;

    .line 136
    .line 137
    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 138
    .line 139
    .line 140
    move-result v1

    .line 141
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 142
    .line 143
    .line 144
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 145
    .line 146
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 147
    .line 148
    .line 149
    move-result v0

    .line 150
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 151
    .line 152
    .line 153
    :goto_0
    return-void
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇o08(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->oOo0:Landroid/widget/LinearLayout;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 8
    .line 9
    .line 10
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const/4 v1, 0x0

    .line 15
    const/4 v2, 0x0

    .line 16
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    if-eqz v3, :cond_1

    .line 21
    .line 22
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    check-cast v3, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;

    .line 27
    .line 28
    new-instance v3, Landroid/widget/ImageView;

    .line 29
    .line 30
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 33
    .line 34
    .line 35
    const v4, 0x7f080482

    .line 36
    .line 37
    .line 38
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 39
    .line 40
    .line 41
    if-nez v2, :cond_0

    .line 42
    .line 43
    const/4 v4, 0x1

    .line 44
    goto :goto_1

    .line 45
    :cond_0
    const/4 v4, 0x0

    .line 46
    :goto_1
    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 47
    .line 48
    .line 49
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 50
    .line 51
    const/4 v5, -0x2

    .line 52
    invoke-direct {v4, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 53
    .line 54
    .line 55
    iget-object v5, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 56
    .line 57
    const/4 v6, 0x6

    .line 58
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 59
    .line 60
    .line 61
    move-result v5

    .line 62
    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 63
    .line 64
    iget-object v5, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 65
    .line 66
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 71
    .line 72
    invoke-virtual {v0, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    .line 74
    .line 75
    add-int/lit8 v2, v2, 0x1

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇o〇88〇8(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$BannerItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O0O:Landroidx/viewpager/widget/ViewPager;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 18
    .line 19
    const/16 v4, 0x5f

    .line 20
    .line 21
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    int-to-float v3, v3

    .line 26
    int-to-float v1, v1

    .line 27
    const v4, 0x3f4ccccd    # 0.8f

    .line 28
    .line 29
    .line 30
    mul-float v4, v4, v1

    .line 31
    .line 32
    add-float/2addr v3, v4

    .line 33
    float-to-int v3, v3

    .line 34
    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 35
    .line 36
    const v2, 0x3dd82d83

    .line 37
    .line 38
    .line 39
    mul-float v2, v2, v1

    .line 40
    .line 41
    float-to-int v3, v2

    .line 42
    const/4 v4, 0x0

    .line 43
    invoke-virtual {v0, v3, v4, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 47
    .line 48
    .line 49
    new-instance v3, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    const-string v5, "leftPadding = "

    .line 55
    .line 56
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    const-string v3, "GuideCnPurchaseStyleFragment"

    .line 67
    .line 68
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    const v2, 0x3d2aaaab

    .line 72
    .line 73
    .line 74
    mul-float v1, v1, v2

    .line 75
    .line 76
    float-to-int v1, v1

    .line 77
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setPageMargin(I)V

    .line 78
    .line 79
    .line 80
    new-instance v1, Lcom/intsig/view/viewpager/AlphaScaleTransformer;

    .line 81
    .line 82
    invoke-direct {v1}, Lcom/intsig/view/viewpager/AlphaScaleTransformer;-><init>()V

    .line 83
    .line 84
    .line 85
    const/high16 v2, 0x3f800000    # 1.0f

    .line 86
    .line 87
    invoke-virtual {v1, v2}, Lcom/intsig/view/viewpager/AlphaScaleTransformer;->〇080(F)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0, v4, v1}, Landroidx/viewpager/widget/ViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    .line 91
    .line 92
    .line 93
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$GuideCnPagerAdapter;

    .line 94
    .line 95
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 96
    .line 97
    const-string v3, "mActivity"

    .line 98
    .line 99
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    invoke-direct {v1, v2, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$GuideCnPagerAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 106
    .line 107
    .line 108
    const/16 p1, 0x14

    .line 109
    .line 110
    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 111
    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final 〇〇O80〇0o(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;Lcom/intsig/adapter/BaseRecyclerViewAdapter;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$csPurchaseHelper"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "$adapter"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget p3, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇00O:I

    .line 17
    .line 18
    const-string v0, "GuideCnPurchaseStyleFragment"

    .line 19
    .line 20
    if-nez p3, :cond_0

    .line 21
    .line 22
    iget p0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇080OO8〇0:I

    .line 23
    .line 24
    invoke-virtual {p2, p0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItem(I)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object p0

    .line 28
    check-cast p0, Lcom/intsig/comm/purchase/entity/PayItem;

    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/comm/purchase/entity/PayItem;->〇o〇()Lcom/intsig/comm/purchase/entity/PayType;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    invoke-virtual {p0}, Lcom/intsig/comm/purchase/entity/PayType;->getValue()I

    .line 35
    .line 36
    .line 37
    move-result p0

    .line 38
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇80(I)V

    .line 39
    .line 40
    .line 41
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 42
    .line 43
    .line 44
    move-result-object p0

    .line 45
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 46
    .line 47
    .line 48
    move-result-object p0

    .line 49
    iget-object p0, p0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year_guide_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 50
    .line 51
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O8O〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;)V

    .line 52
    .line 53
    .line 54
    const-string p0, "buy YEAR_GUIDE"

    .line 55
    .line 56
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    iget p0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇080OO8〇0:I

    .line 61
    .line 62
    invoke-virtual {p2, p0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItem(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object p0

    .line 66
    check-cast p0, Lcom/intsig/comm/purchase/entity/PayItem;

    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/intsig/comm/purchase/entity/PayItem;->〇o〇()Lcom/intsig/comm/purchase/entity/PayType;

    .line 69
    .line 70
    .line 71
    move-result-object p0

    .line 72
    invoke-virtual {p0}, Lcom/intsig/comm/purchase/entity/PayType;->getValue()I

    .line 73
    .line 74
    .line 75
    move-result p0

    .line 76
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇80(I)V

    .line 77
    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 80
    .line 81
    .line 82
    move-result-object p0

    .line 83
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 84
    .line 85
    .line 86
    move-result-object p0

    .line 87
    iget-object p0, p0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->extra_guide_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 88
    .line 89
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O8O〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;)V

    .line 90
    .line 91
    .line 92
    const-string p0, "buy Extra_GUIDE"

    .line 93
    .line 94
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    :goto_0
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇〇0(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/adapter/BaseRecyclerViewAdapter;Landroid/view/View;ILcom/intsig/comm/purchase/entity/PayItem;I)V
    .locals 2

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "$adapter"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p2, "item"

    .line 12
    .line 13
    invoke-static {p4, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget p2, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇080OO8〇0:I

    .line 17
    .line 18
    if-ne p2, p5, :cond_0

    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    const-string p3, "adapter.list"

    .line 26
    .line 27
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    check-cast p2, Ljava/lang/Iterable;

    .line 31
    .line 32
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    const/4 p3, 0x0

    .line 37
    const/4 p4, 0x0

    .line 38
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-eqz v0, :cond_3

    .line 43
    .line 44
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    add-int/lit8 v1, p4, 0x1

    .line 49
    .line 50
    if-gez p4, :cond_1

    .line 51
    .line 52
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 53
    .line 54
    .line 55
    :cond_1
    check-cast v0, Lcom/intsig/comm/purchase/entity/PayItem;

    .line 56
    .line 57
    if-ne p4, p5, :cond_2

    .line 58
    .line 59
    const/4 p4, 0x1

    .line 60
    goto :goto_1

    .line 61
    :cond_2
    const/4 p4, 0x0

    .line 62
    :goto_1
    invoke-virtual {v0, p4}, Lcom/intsig/comm/purchase/entity/PayItem;->OO0o〇〇〇〇0(Z)V

    .line 63
    .line 64
    .line 65
    move p4, v1

    .line 66
    goto :goto_0

    .line 67
    :cond_3
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 68
    .line 69
    .line 70
    iput p5, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇080OO8〇0:I

    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final 〇〇〇00()V
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-static {v1}, Lcom/intsig/comm/purchase/entity/PayItem;->o〇0(Landroid/content/Context;)Lcom/intsig/comm/purchase/entity/PayItem;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-virtual {v1, v2}, Lcom/intsig/comm/purchase/entity/PayItem;->OO0o〇〇〇〇0(Z)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    invoke-static {v1}, Lcom/intsig/comm/purchase/entity/PayItem;->Oo08(Landroid/content/Context;)Lcom/intsig/comm/purchase/entity/PayItem;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const/4 v3, 0x1

    .line 26
    invoke-virtual {v1, v3}, Lcom/intsig/comm/purchase/entity/PayItem;->OO0o〇〇〇〇0(Z)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 33
    .line 34
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 35
    .line 36
    invoke-direct {v1, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->oOo〇8o008:Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;

    .line 47
    .line 48
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 49
    .line 50
    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 51
    .line 52
    .line 53
    new-instance v1, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$initPurchaseTypeView$adapter$1;

    .line 54
    .line 55
    invoke-direct {v1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment$initPurchaseTypeView$adapter$1;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v1, v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->O〇8O8〇008(Ljava/util/List;)V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->oOo〇8o008:Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;

    .line 66
    .line 67
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 68
    .line 69
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 70
    .line 71
    .line 72
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/oO80;

    .line 73
    .line 74
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/guide/guide_cn/oO80;-><init>(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/adapter/BaseRecyclerViewAdapter;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v1, v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇oOO8O8(Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener;)V

    .line 78
    .line 79
    .line 80
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 81
    .line 82
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 83
    .line 84
    .line 85
    sget-object v2, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSGuidePremium:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 86
    .line 87
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 92
    .line 93
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    iget v2, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇00O:I

    .line 98
    .line 99
    if-nez v2, :cond_0

    .line 100
    .line 101
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionType;->YEAR_DOUBLE_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionType;->MONTH_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 105
    .line 106
    :goto_0
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->type(Lcom/intsig/camscanner/purchase/track/FunctionType;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_GUIDE_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 111
    .line 112
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    new-instance v2, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 117
    .line 118
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 119
    .line 120
    .line 121
    move-result-object v3

    .line 122
    invoke-direct {v2, v3, v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 123
    .line 124
    .line 125
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->o8〇OO0〇0o:Landroid/widget/RelativeLayout;

    .line 130
    .line 131
    new-instance v3, Lcom/intsig/camscanner/guide/guide_cn/〇80〇808〇O;

    .line 132
    .line 133
    invoke-direct {v3, p0, v2, v1}, Lcom/intsig/camscanner/guide/guide_cn/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;Lcom/intsig/adapter/BaseRecyclerViewAdapter;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    .line 138
    .line 139
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->handleMessage(Landroid/os/Message;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget p1, p1, Landroid/os/Message;->what:I

    .line 9
    .line 10
    if-ne p1, v1, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    :cond_0
    if-eqz v0, :cond_3

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O0O:Landroidx/viewpager/widget/ViewPager;

    .line 20
    .line 21
    if-eqz p1, :cond_2

    .line 22
    .line 23
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    sub-int/2addr v0, v1

    .line 38
    if-ne v2, v0, :cond_1

    .line 39
    .line 40
    const/16 v0, 0x14

    .line 41
    .line 42
    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    add-int/2addr v2, v1

    .line 47
    invoke-virtual {p1, v2}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 48
    .line 49
    .line 50
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 51
    .line 52
    if-eqz p1, :cond_3

    .line 53
    .line 54
    invoke-virtual {p1, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    const-wide/16 v1, 0xbb8

    .line 59
    .line 60
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 61
    .line 62
    .line 63
    :cond_3
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    const-string p1, "GuideCnPurchaseStyleFragment"

    .line 2
    .line 3
    const-string v0, "initialize>>>"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 9
    .line 10
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->O8o08O8O:Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->Ooo8o()V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 24
    .line 25
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 33
    .line 34
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->O8o08O8O:Landroid/widget/CheckBox;

    .line 42
    .line 43
    invoke-virtual {p1, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 44
    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇08O〇00〇o:Ljava/lang/Integer;

    .line 47
    .line 48
    if-nez p1, :cond_0

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    const/4 v0, 0x5

    .line 56
    if-ne p1, v0, :cond_1

    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇〇〇00()V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->〇8〇oO〇〇8o:Landroid/widget/RelativeLayout;

    .line 66
    .line 67
    const/4 v0, 0x0

    .line 68
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->OO〇00〇8oO:Landroid/widget/LinearLayout;

    .line 76
    .line 77
    const/16 v1, 0x8

    .line 78
    .line 79
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 80
    .line 81
    .line 82
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->o8〇OO0〇0o:Landroid/widget/RelativeLayout;

    .line 87
    .line 88
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->oOo〇8o008:Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;

    .line 96
    .line 97
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->OO:Landroid/widget/LinearLayout;

    .line 98
    .line 99
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    .line 101
    .line 102
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->oOo〇8o008:Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;

    .line 107
    .line 108
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnNewYearDiscountBottomLayoutBinding;->〇OOo8〇0:Landroid/widget/LinearLayout;

    .line 109
    .line 110
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    .line 112
    .line 113
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;->〇〇08O:Landroid/widget/TextView;

    .line 118
    .line 119
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    .line 121
    .line 122
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇8〇80o()Ljava/util/ArrayList;

    .line 123
    .line 124
    .line 125
    move-result-object p1

    .line 126
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇o08(Ljava/util/ArrayList;)V

    .line 127
    .line 128
    .line 129
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇o〇88〇8(Ljava/util/ArrayList;)V

    .line 130
    .line 131
    .line 132
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇8〇OOoooo(Ljava/util/ArrayList;)V

    .line 133
    .line 134
    .line 135
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇0〇o()V

    .line 136
    .line 137
    .line 138
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇0〇0()V

    .line 139
    .line 140
    .line 141
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->O8〇8〇O80()V

    .line 142
    .line 143
    .line 144
    return-void
    .line 145
    .line 146
    .line 147
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 1
    const-string p1, "GuideCnPurchaseStyleFragment"

    .line 2
    .line 3
    const-string v0, "START CHECK BOX onCheckedChanged>>>"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-eqz p2, :cond_0

    .line 9
    .line 10
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 11
    .line 12
    iput-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->OO:Ljava/lang/Boolean;

    .line 13
    .line 14
    const/4 p1, 0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇8O0880(Z)V

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->OO:Ljava/lang/Boolean;

    .line 22
    .line 23
    const/4 p1, 0x0

    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇8O0880(Z)V

    .line 25
    .line 26
    .line 27
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onClick(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    const-string v0, "SKIP>>>"

    .line 17
    .line 18
    const-string v1, "GuideCnPurchaseStyleFragment"

    .line 19
    .line 20
    if-nez p1, :cond_1

    .line 21
    .line 22
    goto :goto_1

    .line 23
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    const v3, 0x7f0a0076

    .line 28
    .line 29
    .line 30
    if-ne v2, v3, :cond_2

    .line 31
    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o0:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 36
    .line 37
    if-eqz p1, :cond_e

    .line 38
    .line 39
    invoke-interface {p1}, Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;->o80ooO()V

    .line 40
    .line 41
    .line 42
    goto/16 :goto_5

    .line 43
    .line 44
    :cond_2
    :goto_1
    const/4 v2, 0x0

    .line 45
    if-nez p1, :cond_3

    .line 46
    .line 47
    goto :goto_2

    .line 48
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    const v4, 0x7f0a0077

    .line 53
    .line 54
    .line 55
    if-ne v3, v4, :cond_6

    .line 56
    .line 57
    const-string p1, "START TRIAL>>>"

    .line 58
    .line 59
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-static {}, Lcom/intsig/utils/FastClickUtil;->〇080()Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    if-eqz p1, :cond_4

    .line 67
    .line 68
    return-void

    .line 69
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->OO:Ljava/lang/Boolean;

    .line 70
    .line 71
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 72
    .line 73
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    move-result p1

    .line 77
    if-eqz p1, :cond_5

    .line 78
    .line 79
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->oOoO8OO〇()V

    .line 80
    .line 81
    .line 82
    goto/16 :goto_5

    .line 83
    .line 84
    :cond_5
    new-instance p1, Lcom/intsig/camscanner/view/dialog/impl/agreement/RenewalAgreementDialog;

    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 87
    .line 88
    const v1, 0x7f140197

    .line 89
    .line 90
    .line 91
    invoke-direct {p1, v0, v2, v2, v1}, Lcom/intsig/camscanner/view/dialog/impl/agreement/RenewalAgreementDialog;-><init>(Landroid/content/Context;ZZI)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 95
    .line 96
    .line 97
    new-instance v0, Lcom/intsig/camscanner/guide/guide_cn/o〇0;

    .line 98
    .line 99
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/guide/guide_cn/o〇0;-><init>(Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/dialog/impl/agreement/RenewalAgreementDialog;->〇O00(Lcom/intsig/camscanner/view/dialog/impl/agreement/RenewalAgreementDialog$OnAgreementDialogListener;)Lcom/intsig/camscanner/view/dialog/impl/agreement/RenewalAgreementDialog;

    .line 103
    .line 104
    .line 105
    goto :goto_5

    .line 106
    :cond_6
    :goto_2
    if-nez p1, :cond_7

    .line 107
    .line 108
    goto :goto_3

    .line 109
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 110
    .line 111
    .line 112
    move-result v3

    .line 113
    const v4, 0x7f0a0b91

    .line 114
    .line 115
    .line 116
    if-ne v3, v4, :cond_8

    .line 117
    .line 118
    const-string p1, "Selected purchase month"

    .line 119
    .line 120
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    const/4 p1, 0x1

    .line 124
    iput p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇00O:I

    .line 125
    .line 126
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇O8〇8000(I)V

    .line 127
    .line 128
    .line 129
    sget-object p1, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 130
    .line 131
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->O0O0〇(Lcom/intsig/camscanner/purchase/track/PurchaseAction;)V

    .line 132
    .line 133
    .line 134
    goto :goto_5

    .line 135
    :cond_8
    :goto_3
    if-nez p1, :cond_9

    .line 136
    .line 137
    goto :goto_4

    .line 138
    :cond_9
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 139
    .line 140
    .line 141
    move-result v3

    .line 142
    const v4, 0x7f0a0b92

    .line 143
    .line 144
    .line 145
    if-ne v3, v4, :cond_a

    .line 146
    .line 147
    const-string p1, "Selected purchase year 99"

    .line 148
    .line 149
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    iput v2, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o〇00O:I

    .line 153
    .line 154
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇O8〇8000(I)V

    .line 155
    .line 156
    .line 157
    sget-object p1, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_DOUBLE_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 158
    .line 159
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->O0O0〇(Lcom/intsig/camscanner/purchase/track/PurchaseAction;)V

    .line 160
    .line 161
    .line 162
    goto :goto_5

    .line 163
    :cond_a
    :goto_4
    if-nez p1, :cond_b

    .line 164
    .line 165
    goto :goto_5

    .line 166
    :cond_b
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 167
    .line 168
    .line 169
    move-result p1

    .line 170
    const v2, 0x7f0a17ec

    .line 171
    .line 172
    .line 173
    if-ne p1, v2, :cond_e

    .line 174
    .line 175
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    iget-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇08O〇00〇o:Ljava/lang/Integer;

    .line 179
    .line 180
    if-nez p1, :cond_c

    .line 181
    .line 182
    goto :goto_5

    .line 183
    :cond_c
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 184
    .line 185
    .line 186
    move-result p1

    .line 187
    const/4 v0, 0x5

    .line 188
    if-ne p1, v0, :cond_e

    .line 189
    .line 190
    iget-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o0:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 191
    .line 192
    if-eqz p1, :cond_d

    .line 193
    .line 194
    invoke-interface {p1}, Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;->o80ooO()V

    .line 195
    .line 196
    .line 197
    :cond_d
    sget-object p1, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->SKIP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 198
    .line 199
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->O0O0〇(Lcom/intsig/camscanner/purchase/track/PurchaseAction;)V

    .line 200
    .line 201
    .line 202
    :cond_e
    :goto_5
    return-void
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onDestroyView()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->O8o08O8O:Lcom/intsig/camscanner/databinding/GuideCnPurchasePageBinding;

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const-wide/16 v2, 0xbb8

    .line 14
    .line 15
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :catch_0
    move-exception v0

    .line 20
    const-string v1, "GuideCnPurchaseStyleFragment"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇08O〇00〇o:Ljava/lang/Integer;

    .line 26
    .line 27
    const-string v1, "type"

    .line 28
    .line 29
    const-string v2, "CSGuide"

    .line 30
    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    const/4 v3, 0x5

    .line 39
    if-ne v0, v3, :cond_2

    .line 40
    .line 41
    const-string v0, "guide_premium_marketing"

    .line 42
    .line 43
    invoke-static {v2, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_2
    :goto_1
    const-string v0, "guide_premium"

    .line 48
    .line 49
    invoke-static {v2, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    :goto_2
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onStop()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStop()V

    .line 2
    .line 3
    .line 4
    :try_start_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :catch_0
    move-exception v0

    .line 14
    const-string v1, "GuideCnPurchaseStyleFragment"

    .line 15
    .line 16
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    :goto_0
    return-void
    .line 20
    .line 21
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0366

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇08O(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$GotoMainListener;)Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->〇OOo8〇0:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$GotoMainListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇O0o〇〇o(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;)Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/guide/guide_cn/GuideCnPurchaseStyleFragment;->o0:Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

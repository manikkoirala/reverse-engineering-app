.class Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;
.super Ljava/lang/Object;
.source "NewGuideActivity.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/guide/NewGuideActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GuideVideoCallback"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/guide/NewGuideActivity;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/guide/NewGuideActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o00〇〇Oo()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o〇0〇o(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "NewGuideActivity"

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const-string v0, "video uri is null"

    .line 12
    .line 13
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 18
    .line 19
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    const-string v0, "mMediaPlayer is already exist"

    .line 26
    .line 27
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 32
    .line 33
    new-instance v2, Landroid/media/MediaPlayer;

    .line 34
    .line 35
    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-static {v0, v2}, Lcom/intsig/camscanner/guide/NewGuideActivity;->〇8〇80o(Lcom/intsig/camscanner/guide/NewGuideActivity;Landroid/media/MediaPlayer;)V

    .line 39
    .line 40
    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 42
    .line 43
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iget-object v2, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 48
    .line 49
    invoke-static {v2}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o〇0〇o(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/net/Uri;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    invoke-virtual {v0, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 57
    .line 58
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iget-object v2, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 63
    .line 64
    invoke-static {v2}, Lcom/intsig/camscanner/guide/NewGuideActivity;->O0〇0(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/view/SurfaceView;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 76
    .line 77
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    const/4 v2, 0x2

    .line 82
    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setVideoScalingMode(I)V

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 86
    .line 87
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    const/4 v2, 0x1

    .line 92
    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 93
    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 96
    .line 97
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    new-instance v2, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback$2;

    .line 102
    .line 103
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback$2;-><init>(Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 107
    .line 108
    .line 109
    const-string v0, "video starts loading"

    .line 110
    .line 111
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 115
    .line 116
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    new-instance v2, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback$3;

    .line 121
    .line 122
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback$3;-><init>(Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 126
    .line 127
    .line 128
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 129
    .line 130
    invoke-static {v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    .line 136
    .line 137
    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 140
    .line 141
    .line 142
    :goto_0
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    .line 1
    const-string p1, "NewGuideActivity"

    .line 2
    .line 3
    const-string v0, "SurfaceHolder is created"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 9
    .line 10
    invoke-static {p1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->〇088O(Lcom/intsig/camscanner/guide/NewGuideActivity;)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 18
    .line 19
    invoke-static {p1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->〇8〇OOoooo(Lcom/intsig/camscanner/guide/NewGuideActivity;)Ljava/lang/Thread;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    return-void

    .line 26
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 27
    .line 28
    new-instance v0, Ljava/lang/Thread;

    .line 29
    .line 30
    new-instance v1, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback$1;

    .line 31
    .line 32
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback$1;-><init>(Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;)V

    .line 33
    .line 34
    .line 35
    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 36
    .line 37
    .line 38
    invoke-static {p1, v0}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o〇O8OO(Lcom/intsig/camscanner/guide/NewGuideActivity;Ljava/lang/Thread;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 42
    .line 43
    invoke-static {p1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->〇8〇OOoooo(Lcom/intsig/camscanner/guide/NewGuideActivity;)Ljava/lang/Thread;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3

    .line 1
    const-string p1, "SurfaceHolder is destroyed"

    .line 2
    .line 3
    const-string v0, "NewGuideActivity"

    .line 4
    .line 5
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 10
    .line 11
    invoke-static {v1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 18
    .line 19
    invoke-static {v1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v1, :cond_0

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 30
    .line 31
    invoke-static {v1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    invoke-static {v1, v2}, Lcom/intsig/camscanner/guide/NewGuideActivity;->〇o〇88〇8(Lcom/intsig/camscanner/guide/NewGuideActivity;I)V

    .line 40
    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 43
    .line 44
    invoke-static {v1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o00〇88〇08(Lcom/intsig/camscanner/guide/NewGuideActivity;)Landroid/media/MediaPlayer;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    .line 50
    .line 51
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 52
    .line 53
    invoke-static {v0, p1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->〇8〇80o(Lcom/intsig/camscanner/guide/NewGuideActivity;Landroid/media/MediaPlayer;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 57
    .line 58
    invoke-static {v0, p1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o〇O8OO(Lcom/intsig/camscanner/guide/NewGuideActivity;Ljava/lang/Thread;)V

    .line 59
    .line 60
    .line 61
    goto :goto_1

    .line 62
    :catchall_0
    move-exception v0

    .line 63
    goto :goto_2

    .line 64
    :catch_0
    move-exception v1

    .line 65
    :try_start_1
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :goto_1
    return-void

    .line 70
    :goto_2
    iget-object v1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 71
    .line 72
    invoke-static {v1, p1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->〇8〇80o(Lcom/intsig/camscanner/guide/NewGuideActivity;Landroid/media/MediaPlayer;)V

    .line 73
    .line 74
    .line 75
    iget-object v1, p0, Lcom/intsig/camscanner/guide/NewGuideActivity$GuideVideoCallback;->o0:Lcom/intsig/camscanner/guide/NewGuideActivity;

    .line 76
    .line 77
    invoke-static {v1, p1}, Lcom/intsig/camscanner/guide/NewGuideActivity;->o〇O8OO(Lcom/intsig/camscanner/guide/NewGuideActivity;Ljava/lang/Thread;)V

    .line 78
    .line 79
    .line 80
    throw v0
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

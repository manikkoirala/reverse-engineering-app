.class public final enum Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;
.super Ljava/lang/Enum;
.source "SchoolSeasonLotteryPrize.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

.field public static final enum SCHOOL_SEASON_PRIZE_2_TIMES_TO_WORD:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

.field public static final enum SCHOOL_SEASON_PRIZE_7_DAY_PREMIUM:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

.field public static final enum SCHOOL_SEASON_PRIZE_IQIYI:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

.field public static final enum SCHOOL_SEASON_PRIZE_LIFE_LONG_PREMIUM:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

.field public static final enum SCHOOL_SEASON_PRIZE_PRINTER:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;


# instance fields
.field private final prizePreviewResId:I

.field private final prizeType:I


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;
    .locals 3

    .line 1
    const/4 v0, 0x5

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_LIFE_LONG_PREMIUM:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_7_DAY_PREMIUM:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_PRINTER:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_2_TIMES_TO_WORD:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    const/4 v1, 0x4

    .line 25
    sget-object v2, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_IQIYI:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 26
    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 2
    .line 3
    const v1, 0x7f0808de

    .line 4
    .line 5
    .line 6
    const-string v2, "SCHOOL_SEASON_PRIZE_LIFE_LONG_PREMIUM"

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x1

    .line 10
    invoke-direct {v0, v2, v3, v4, v1}, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;-><init>(Ljava/lang/String;III)V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_LIFE_LONG_PREMIUM:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 16
    .line 17
    const-string v1, "SCHOOL_SEASON_PRIZE_7_DAY_PREMIUM"

    .line 18
    .line 19
    const v2, 0x7f0808dc

    .line 20
    .line 21
    .line 22
    invoke-direct {v0, v1, v4, v4, v2}, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;-><init>(Ljava/lang/String;III)V

    .line 23
    .line 24
    .line 25
    sput-object v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_7_DAY_PREMIUM:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 26
    .line 27
    new-instance v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 28
    .line 29
    const v1, 0x7f0808df

    .line 30
    .line 31
    .line 32
    const-string v2, "SCHOOL_SEASON_PRIZE_PRINTER"

    .line 33
    .line 34
    const/4 v3, 0x2

    .line 35
    const/4 v4, 0x3

    .line 36
    invoke-direct {v0, v2, v3, v4, v1}, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;-><init>(Ljava/lang/String;III)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_PRINTER:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 40
    .line 41
    new-instance v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 42
    .line 43
    const v1, 0x7f0808db

    .line 44
    .line 45
    .line 46
    const-string v2, "SCHOOL_SEASON_PRIZE_2_TIMES_TO_WORD"

    .line 47
    .line 48
    const/4 v5, 0x4

    .line 49
    invoke-direct {v0, v2, v4, v5, v1}, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;-><init>(Ljava/lang/String;III)V

    .line 50
    .line 51
    .line 52
    sput-object v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_2_TIMES_TO_WORD:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 53
    .line 54
    new-instance v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 55
    .line 56
    const-string v1, "SCHOOL_SEASON_PRIZE_IQIYI"

    .line 57
    .line 58
    const v2, 0x7f0808dd

    .line 59
    .line 60
    .line 61
    invoke-direct {v0, v1, v5, v3, v2}, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;-><init>(Ljava/lang/String;III)V

    .line 62
    .line 63
    .line 64
    sput-object v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->SCHOOL_SEASON_PRIZE_IQIYI:Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 65
    .line 66
    invoke-static {}, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->$values()[Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    sput-object v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->$VALUES:[Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->prizeType:I

    .line 5
    .line 6
    iput p4, p0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->prizePreviewResId:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->$VALUES:[Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getPrizePreviewResId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->prizePreviewResId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getPrizeType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/gift/lottery/SchoolSeasonLotteryPrize;->prizeType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;
.super Lcom/intsig/mvp/activity/BaseMvpActivity;
.source "PPTPreviewNewActivity.kt"

# interfaces
.implements Lcom/intsig/camscanner/ppt/preview/PPTPreviewContract$View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$PhoneImpl;,
        Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/mvp/activity/BaseMvpActivity<",
        "Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;",
        ">;",
        "Lcom/intsig/camscanner/ppt/preview/PPTPreviewContract$View;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇80O8o8O〇:Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;

.field private O88O:Z

.field private O8o〇O0:Landroid/widget/TextView;

.field private O8〇o〇88:Z

.field private OO〇OOo:Landroid/widget/TextView;

.field private Oo0O0o8:Landroid/widget/TextView;

.field private Oo0〇Ooo:Lcom/intsig/camscanner/view/MyViewPager;

.field private Oo80:Landroid/view/animation/Animation;

.field private Ooo08:Landroid/view/View;

.field private O〇08oOOO0:Landroid/view/View;

.field private O〇O:Z

.field private O〇o88o08〇:Landroid/view/animation/Animation;

.field private o0OoOOo0:Landroid/widget/EditText;

.field private final o8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8o:Ljava/lang/String;

.field private o8oOOo:I

.field private o8〇OO:Landroid/widget/TextView;

.field private oO00〇o:Landroid/widget/RelativeLayout;

.field private oOO0880O:Landroid/widget/RelativeLayout;

.field private oOO8:I

.field private oOO〇〇:Z

.field private oOoo80oO:Landroid/widget/TextView;

.field private oO〇8O8oOo:Landroid/view/View;

.field private final oo8ooo8O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooO:Ljava/lang/String;

.field private o〇oO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇o〇Oo88:Landroid/widget/TextView;

.field private 〇00O0:Landroid/view/animation/Animation;

.field private 〇08〇o0O:I

.field private 〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

.field private 〇800OO〇0O:Landroid/widget/LinearLayout;

.field private 〇8〇o88:Landroid/view/animation/Animation;

.field private 〇OO8ooO8〇:Landroidx/appcompat/app/ActionBar;

.field private 〇OO〇00〇0O:Ljava/lang/String;

.field private 〇O〇〇O8:J

.field private 〇o0O:J

.field private 〇oo〇O〇80:J

.field private final 〇〇08O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇o〇:Z

.field private 〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇80O8o8O〇:Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseMvpActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇08O:Ljava/util/List;

    .line 10
    .line 11
    const/4 v0, -0x1

    .line 12
    iput v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 13
    .line 14
    const-wide/16 v0, -0x1

    .line 15
    .line 16
    iput-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 17
    .line 18
    iput-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    iput-boolean v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO〇〇:Z

    .line 22
    .line 23
    const-string v0, ""

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8o:Ljava/lang/String;

    .line 26
    .line 27
    new-instance v1, Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .line 31
    .line 32
    iput-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 33
    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇oO:Ljava/lang/String;

    .line 35
    .line 36
    sget v0, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0:I

    .line 37
    .line 38
    iput v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇08〇o0O:I

    .line 39
    .line 40
    new-instance v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$special$$inlined$viewModels$default$1;

    .line 41
    .line 42
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$special$$inlined$viewModels$default$1;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 43
    .line 44
    .line 45
    new-instance v1, Landroidx/lifecycle/ViewModelLazy;

    .line 46
    .line 47
    const-class v2, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 48
    .line 49
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    new-instance v3, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$special$$inlined$viewModels$default$2;

    .line 54
    .line 55
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$special$$inlined$viewModels$default$2;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 56
    .line 57
    .line 58
    new-instance v4, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$special$$inlined$viewModels$default$3;

    .line 59
    .line 60
    const/4 v5, 0x0

    .line 61
    invoke-direct {v4, v5, p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$special$$inlined$viewModels$default$3;-><init>(Lkotlin/jvm/functions/Function0;Landroidx/activity/ComponentActivity;)V

    .line 62
    .line 63
    .line 64
    invoke-direct {v1, v2, v3, v0, v4}, Landroidx/lifecycle/ViewModelLazy;-><init>(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 65
    .line 66
    .line 67
    iput-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8O:Lkotlin/Lazy;

    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O00OoO〇(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O8Oo(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O08〇oO8〇(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .line 1
    iget-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 2
    .line 3
    const v2, 0x7f130009

    .line 4
    .line 5
    .line 6
    const/4 v3, 0x1

    .line 7
    new-instance v6, LO〇00O/Oo08;

    .line 8
    .line 9
    invoke-direct {v6, p0, p1}, LO〇00O/Oo08;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v7, 0x0

    .line 13
    iget-wide v8, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 14
    .line 15
    const/4 v10, 0x0

    .line 16
    move-object v0, p0

    .line 17
    move-object v4, p1

    .line 18
    move-object v5, p2

    .line 19
    invoke-static/range {v0 .. v10}, Lcom/intsig/camscanner/app/DialogUtils;->O8O〇(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;Lcom/intsig/camscanner/app/DialogUtils$OnTemplateSettingsListener;JZ)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O80OO(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O08〇oO8〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O88(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Lcom/intsig/mvp/presenter/IPresenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O880O〇(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O8O(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8o:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇00o〇O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OO0O(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OO0o(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O88O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO8〇O8(J)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇08O:Ljava/util/List;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 4
    .line 5
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 13
    .line 14
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 19
    .line 20
    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O000(JJ)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 24
    .line 25
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇0o8o8〇(J)V

    .line 26
    .line 27
    .line 28
    iput-wide p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 31
    .line 32
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->Oo8Oo00oo(J)Z

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    const/4 p2, 0x1

    .line 42
    if-ne p1, p2, :cond_1

    .line 43
    .line 44
    const/4 v1, 0x1

    .line 45
    :cond_1
    if-eqz v1, :cond_2

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 48
    .line 49
    move-object v0, p1

    .line 50
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 51
    .line 52
    if-eqz v0, :cond_3

    .line 53
    .line 54
    const-string v1, "action_retake"

    .line 55
    .line 56
    iget-wide v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 57
    .line 58
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 59
    .line 60
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O08000(Ljava/lang/String;JJ)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_2
    const-string p1, "action_retake"

    .line 65
    .line 66
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo08OO8oO(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    :cond_3
    :goto_0
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final OOo00()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f131d10

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const v1, 0x7f13029c

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    new-instance v1, LO〇00O/〇080;

    .line 26
    .line 27
    invoke-direct {v1, p0}, LO〇00O/〇080;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)V

    .line 28
    .line 29
    .line 30
    const v2, 0x7f13054e

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    new-instance v1, LO〇00O/〇o00〇〇Oo;

    .line 38
    .line 39
    invoke-direct {v1}, LO〇00O/〇o00〇〇Oo;-><init>()V

    .line 40
    .line 41
    .line 42
    const v2, 0x7f13057e

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic OO〇〇o0oO(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇oO:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OoO〇OOo8o(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇08〇o0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OooO〇(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO8(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O〇0O〇Oo〇o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o088O8800(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇0o8o8〇(J)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 8
    .line 9
    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇00(JJ)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 25
    .line 26
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O〇0o8〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mViewPager"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$setupOnTouchListeners$1;

    .line 12
    .line 13
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$setupOnTouchListeners$1;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method private final O〇o8()V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    const-string v2, "mTvEdit"

    .line 5
    .line 6
    const-string v3, "mBtnRight"

    .line 7
    .line 8
    const-string v4, "mRlEdit"

    .line 9
    .line 10
    const-string v5, "mRlUnEdit"

    .line 11
    .line 12
    const/16 v6, 0x8

    .line 13
    .line 14
    const/4 v7, 0x0

    .line 15
    const/4 v8, 0x0

    .line 16
    if-ne v0, v1, :cond_e

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 19
    .line 20
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 21
    .line 22
    const/4 v1, 0x1

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇oOO8O8()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-ne v0, v1, :cond_0

    .line 30
    .line 31
    const/4 v0, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v0, 0x0

    .line 34
    :goto_0
    if-eqz v0, :cond_5

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO00〇o:Landroid/widget/RelativeLayout;

    .line 37
    .line 38
    if-nez v0, :cond_1

    .line 39
    .line 40
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    move-object v0, v8

    .line 44
    :cond_1
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO0880O:Landroid/widget/RelativeLayout;

    .line 48
    .line 49
    if-nez v0, :cond_2

    .line 50
    .line 51
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    move-object v0, v8

    .line 55
    :cond_2
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 59
    .line 60
    if-nez v0, :cond_3

    .line 61
    .line 62
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    move-object v0, v8

    .line 66
    :cond_3
    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8o〇O0:Landroid/widget/TextView;

    .line 70
    .line 71
    if-nez v0, :cond_4

    .line 72
    .line 73
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    goto :goto_1

    .line 77
    :cond_4
    move-object v8, v0

    .line 78
    :goto_1
    invoke-virtual {v8, v7}, Landroid/view/View;->setVisibility(I)V

    .line 79
    .line 80
    .line 81
    goto/16 :goto_4

    .line 82
    .line 83
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 84
    .line 85
    if-nez v0, :cond_6

    .line 86
    .line 87
    const-string v0, "mEdtContent"

    .line 88
    .line 89
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    move-object v0, v8

    .line 93
    :cond_6
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    iget-object v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇o〇Oo88:Landroid/widget/TextView;

    .line 102
    .line 103
    if-nez v2, :cond_7

    .line 104
    .line 105
    const-string v2, "mUNEditOcrTextView"

    .line 106
    .line 107
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    move-object v2, v8

    .line 111
    :cond_7
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    .line 113
    .line 114
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO00〇o:Landroid/widget/RelativeLayout;

    .line 115
    .line 116
    if-nez v0, :cond_8

    .line 117
    .line 118
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    move-object v0, v8

    .line 122
    :cond_8
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO0880O:Landroid/widget/RelativeLayout;

    .line 126
    .line 127
    if-nez v0, :cond_9

    .line 128
    .line 129
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    move-object v0, v8

    .line 133
    :cond_9
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 134
    .line 135
    .line 136
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 137
    .line 138
    if-nez v0, :cond_a

    .line 139
    .line 140
    const-string v0, "mLlUnableEditText"

    .line 141
    .line 142
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    move-object v0, v8

    .line 146
    :cond_a
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 147
    .line 148
    .line 149
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO00〇o:Landroid/widget/RelativeLayout;

    .line 150
    .line 151
    if-nez v0, :cond_b

    .line 152
    .line 153
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    move-object v0, v8

    .line 157
    :cond_b
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 158
    .line 159
    .line 160
    move-result v0

    .line 161
    if-nez v0, :cond_c

    .line 162
    .line 163
    goto :goto_2

    .line 164
    :cond_c
    const/4 v1, 0x0

    .line 165
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 166
    .line 167
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    .line 169
    .line 170
    const-string v2, "test click ocr mRlUnEdit VISIBLE: "

    .line 171
    .line 172
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object v0

    .line 182
    const-string v1, "PPTPreviewNewActivity"

    .line 183
    .line 184
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 188
    .line 189
    if-nez v0, :cond_d

    .line 190
    .line 191
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 192
    .line 193
    .line 194
    goto :goto_3

    .line 195
    :cond_d
    move-object v8, v0

    .line 196
    :goto_3
    const v0, 0x7f080d9f

    .line 197
    .line 198
    .line 199
    invoke-virtual {v8, v0, v7, v7, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 200
    .line 201
    .line 202
    goto :goto_4

    .line 203
    :cond_e
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO00〇o:Landroid/widget/RelativeLayout;

    .line 204
    .line 205
    if-nez v0, :cond_f

    .line 206
    .line 207
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 208
    .line 209
    .line 210
    move-object v0, v8

    .line 211
    :cond_f
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 212
    .line 213
    .line 214
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO0880O:Landroid/widget/RelativeLayout;

    .line 215
    .line 216
    if-nez v0, :cond_10

    .line 217
    .line 218
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    move-object v0, v8

    .line 222
    :cond_10
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 223
    .line 224
    .line 225
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8o〇O0:Landroid/widget/TextView;

    .line 226
    .line 227
    if-nez v0, :cond_11

    .line 228
    .line 229
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 230
    .line 231
    .line 232
    move-object v0, v8

    .line 233
    :cond_11
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 234
    .line 235
    .line 236
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 237
    .line 238
    if-nez v0, :cond_12

    .line 239
    .line 240
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 241
    .line 242
    .line 243
    move-object v0, v8

    .line 244
    :cond_12
    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 245
    .line 246
    .line 247
    :goto_4
    return-void
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇8oo8888(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O〇〇o8O(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroidx/appcompat/app/ActionBar;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇OO8ooO8〇:Landroidx/appcompat/app/ActionBar;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o088O8800(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p0, "PPTPreviewNewActivity"

    .line 2
    .line 3
    const-string p1, "DIALOG_EXIT cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o0OO(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o0Oo(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8o0o8(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroid/net/Uri;Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final o0〇〇00〇o()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇O:Z

    .line 2
    .line 3
    const-string v1, "mBtnRight"

    .line 4
    .line 5
    const-string v2, "mBtnLeft"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    if-eqz v0, :cond_4

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0O0o8:Landroid/widget/TextView;

    .line 11
    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    move-object v0, v3

    .line 18
    :cond_0
    const/4 v4, 0x0

    .line 19
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 23
    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    move-object v0, v3

    .line 30
    :cond_1
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0O0o8:Landroid/widget/TextView;

    .line 34
    .line 35
    if-nez v0, :cond_2

    .line 36
    .line 37
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    move-object v0, v3

    .line 41
    :cond_2
    const v2, 0x7f13057e

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 48
    .line 49
    if-nez v0, :cond_3

    .line 50
    .line 51
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_3
    move-object v3, v0

    .line 56
    :goto_0
    const v0, 0x7f1302e9

    .line 57
    .line 58
    .line 59
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 60
    .line 61
    .line 62
    goto :goto_3

    .line 63
    :cond_4
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 64
    .line 65
    const/4 v4, 0x2

    .line 66
    if-ne v0, v4, :cond_7

    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0O0o8:Landroid/widget/TextView;

    .line 69
    .line 70
    if-nez v0, :cond_5

    .line 71
    .line 72
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    move-object v0, v3

    .line 76
    :cond_5
    const v2, 0x7f13010d

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 83
    .line 84
    if-nez v0, :cond_6

    .line 85
    .line 86
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_6
    move-object v3, v0

    .line 91
    :goto_1
    const v0, 0x7f1301c3

    .line 92
    .line 93
    .line 94
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 95
    .line 96
    .line 97
    goto :goto_3

    .line 98
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0O0o8:Landroid/widget/TextView;

    .line 99
    .line 100
    if-nez v0, :cond_8

    .line 101
    .line 102
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    move-object v0, v3

    .line 106
    :cond_8
    const/16 v2, 0x8

    .line 107
    .line 108
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 112
    .line 113
    if-nez v0, :cond_9

    .line 114
    .line 115
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    goto :goto_2

    .line 119
    :cond_9
    move-object v3, v0

    .line 120
    :goto_2
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 121
    .line 122
    .line 123
    :goto_3
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇8〇〇8o(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o88o88()V
    .locals 4

    .line 1
    const v0, 0x7f0a0ad8

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const-string v1, "findViewById(R.id.kbl_halfpack_root)"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    check-cast v0, Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 16
    .line 17
    const v0, 0x7f0a1961

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v1, "findViewById(R.id.txt_halfpack_title)"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    check-cast v0, Landroid/widget/TextView;

    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOoo80oO:Landroid/widget/TextView;

    .line 32
    .line 33
    const v0, 0x7f0a0262

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    const-string v1, "findViewById(R.id.btn_halfpack_left)"

    .line 41
    .line 42
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    check-cast v0, Landroid/widget/TextView;

    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0O0o8:Landroid/widget/TextView;

    .line 48
    .line 49
    const v0, 0x7f0a0263

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    const-string v1, "findViewById(R.id.btn_halfpack_right)"

    .line 57
    .line 58
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    check-cast v0, Landroid/widget/TextView;

    .line 62
    .line 63
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 64
    .line 65
    const v0, 0x7f0a0fc7

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    const-string v1, "findViewById(R.id.rl_ocr_result_can_edit)"

    .line 73
    .line 74
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 78
    .line 79
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO0880O:Landroid/widget/RelativeLayout;

    .line 80
    .line 81
    const v0, 0x7f0a0576

    .line 82
    .line 83
    .line 84
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    const-string v1, "findViewById(R.id.edt_halfpack_content)"

    .line 89
    .line 90
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    check-cast v0, Landroid/widget/EditText;

    .line 94
    .line 95
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 96
    .line 97
    const v0, 0x7f0a0c7d

    .line 98
    .line 99
    .line 100
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    const-string v1, "findViewById(R.id.ll_pageimage_bg_note)"

    .line 105
    .line 106
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO〇8O8oOo:Landroid/view/View;

    .line 110
    .line 111
    const v0, 0x7f0a12bd

    .line 112
    .line 113
    .line 114
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    const-string v1, "findViewById(R.id.tv_can_edit_ocr)"

    .line 119
    .line 120
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    check-cast v0, Landroid/widget/TextView;

    .line 124
    .line 125
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8o〇O0:Landroid/widget/TextView;

    .line 126
    .line 127
    const v0, 0x7f0a0fc8

    .line 128
    .line 129
    .line 130
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    const-string v1, "findViewById(R.id.rl_ocr_result_cannot_edit)"

    .line 135
    .line 136
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 140
    .line 141
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO00〇o:Landroid/widget/RelativeLayout;

    .line 142
    .line 143
    const v0, 0x7f0a0b96

    .line 144
    .line 145
    .line 146
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    const-string v1, "findViewById(R.id.ll_cannot_edit_ocr)"

    .line 151
    .line 152
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    check-cast v0, Landroid/widget/LinearLayout;

    .line 156
    .line 157
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 158
    .line 159
    const v0, 0x7f0a1621

    .line 160
    .line 161
    .line 162
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    const-string v1, "findViewById(R.id.tv_ocr)"

    .line 167
    .line 168
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    .line 170
    .line 171
    check-cast v0, Landroid/widget/TextView;

    .line 172
    .line 173
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇o〇Oo88:Landroid/widget/TextView;

    .line 174
    .line 175
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O0O:Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;

    .line 176
    .line 177
    const/4 v1, 0x0

    .line 178
    if-nez v0, :cond_0

    .line 179
    .line 180
    const-string v0, "mDeviceInterface"

    .line 181
    .line 182
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    move-object v0, v1

    .line 186
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;->〇o00〇〇Oo()Landroid/view/animation/Animation;

    .line 187
    .line 188
    .line 189
    move-result-object v0

    .line 190
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇8〇o88:Landroid/view/animation/Animation;

    .line 191
    .line 192
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 193
    .line 194
    const-string v2, "mPackRoot"

    .line 195
    .line 196
    if-nez v0, :cond_1

    .line 197
    .line 198
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 199
    .line 200
    .line 201
    move-object v0, v1

    .line 202
    :cond_1
    new-instance v3, LO〇00O/〇o〇;

    .line 203
    .line 204
    invoke-direct {v3, p0}, LO〇00O/〇o〇;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/view/KeyboardListenerLayout;->setOnkbdStateListener(Lcom/intsig/camscanner/view/KeyboardListenerLayout$onKeyboardChangeListener;)V

    .line 208
    .line 209
    .line 210
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0O0o8:Landroid/widget/TextView;

    .line 211
    .line 212
    if-nez v0, :cond_2

    .line 213
    .line 214
    const-string v0, "mBtnLeft"

    .line 215
    .line 216
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 217
    .line 218
    .line 219
    move-object v0, v1

    .line 220
    :cond_2
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    .line 222
    .line 223
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 224
    .line 225
    if-nez v0, :cond_3

    .line 226
    .line 227
    const-string v0, "mBtnRight"

    .line 228
    .line 229
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 230
    .line 231
    .line 232
    move-object v0, v1

    .line 233
    :cond_3
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    .line 235
    .line 236
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO〇8O8oOo:Landroid/view/View;

    .line 237
    .line 238
    if-nez v0, :cond_4

    .line 239
    .line 240
    const-string v0, "mNoteTouchDismissBg"

    .line 241
    .line 242
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    move-object v0, v1

    .line 246
    :cond_4
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    .line 248
    .line 249
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 250
    .line 251
    if-nez v0, :cond_5

    .line 252
    .line 253
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 254
    .line 255
    .line 256
    move-object v0, v1

    .line 257
    :cond_5
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    .line 259
    .line 260
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 261
    .line 262
    if-nez v0, :cond_6

    .line 263
    .line 264
    const-string v0, "mEdtContent"

    .line 265
    .line 266
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 267
    .line 268
    .line 269
    move-object v0, v1

    .line 270
    :cond_6
    const/4 v2, 0x0

    .line 271
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 272
    .line 273
    .line 274
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8o〇O0:Landroid/widget/TextView;

    .line 275
    .line 276
    if-nez v0, :cond_7

    .line 277
    .line 278
    const-string v0, "mTvEdit"

    .line 279
    .line 280
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 281
    .line 282
    .line 283
    move-object v0, v1

    .line 284
    :cond_7
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    .line 286
    .line 287
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇800OO〇0O:Landroid/widget/LinearLayout;

    .line 288
    .line 289
    if-nez v0, :cond_8

    .line 290
    .line 291
    const-string v0, "mLlUnableEditText"

    .line 292
    .line 293
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 294
    .line 295
    .line 296
    move-object v0, v1

    .line 297
    :cond_8
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    .line 299
    .line 300
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇o〇Oo88:Landroid/widget/TextView;

    .line 301
    .line 302
    if-nez v0, :cond_9

    .line 303
    .line 304
    const-string v0, "mUNEditOcrTextView"

    .line 305
    .line 306
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 307
    .line 308
    .line 309
    goto :goto_0

    .line 310
    :cond_9
    move-object v1, v0

    .line 311
    :goto_0
    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    .line 312
    .line 313
    .line 314
    move-result-object v0

    .line 315
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 316
    .line 317
    .line 318
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇8〇o88:Landroid/view/animation/Animation;

    .line 319
    .line 320
    if-eqz v0, :cond_a

    .line 321
    .line 322
    new-instance v1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$initNotePack$2;

    .line 323
    .line 324
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$initNotePack$2;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)V

    .line 325
    .line 326
    .line 327
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 328
    .line 329
    .line 330
    :cond_a
    return-void
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final o8O〇008()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final o8o0o8(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 7

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$uri"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const-string v0, "_data"

    .line 16
    .line 17
    const-string v2, "sync_doc_id"

    .line 18
    .line 19
    filled-new-array {v0, v2}, [Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    const/4 v4, 0x0

    .line 24
    const/4 v5, 0x0

    .line 25
    const/4 v6, 0x0

    .line 26
    move-object v2, p1

    .line 27
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    const/4 v0, 0x0

    .line 32
    if-eqz p1, :cond_1

    .line 33
    .line 34
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-eqz v1, :cond_0

    .line 39
    .line 40
    const/4 v0, 0x0

    .line 41
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 46
    .line 47
    const/4 v2, 0x1

    .line 48
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    const/4 v4, 0x3

    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 54
    .line 55
    .line 56
    move-result-wide v5

    .line 57
    move-object v2, p0

    .line 58
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->OoO8(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;IJ)V

    .line 59
    .line 60
    .line 61
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 62
    .line 63
    .line 64
    :cond_1
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 65
    .line 66
    invoke-static {v1, v2, p2, v0, p0}, Lcom/intsig/camscanner/util/Util;->o8O0(JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    sget-object p1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 70
    .line 71
    iget-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 72
    .line 73
    invoke-virtual {p1, p0, v0, v1, p3}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇00〇8(Landroid/content/Context;JI)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final oO8(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroid/content/DialogInterface;I)V
    .locals 7

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "PPTPreviewNewActivity"

    .line 7
    .line 8
    const-string p2, "KeyEvent.KEYCODE_BACK "

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 14
    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    move-object v0, p1

    .line 18
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    const-string v1, "action_cancel"

    .line 23
    .line 24
    const/4 v2, 0x1

    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇oO:Ljava/lang/String;

    .line 26
    .line 27
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 28
    .line 29
    iget-object v6, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 30
    .line 31
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O〇O〇oO(Ljava/lang/String;ZLjava/lang/String;JLjava/util/List;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    const-string p0, "CSPPTPreview"

    .line 35
    .line 36
    const-string p1, "back"

    .line 37
    .line 38
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic oOO8oo0(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oO〇O0O()V
    .locals 3

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O0O:Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string v0, "mDeviceInterface"

    .line 10
    .line 11
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    move-object v0, v1

    .line 15
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;->〇080()V

    .line 16
    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o88oo〇O(I)V

    .line 20
    .line 21
    .line 22
    const/4 v0, 0x2

    .line 23
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setDefaultKeyMode(I)V

    .line 24
    .line 25
    .line 26
    const/4 v0, 0x1

    .line 27
    iput-boolean v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇o〇:Z

    .line 28
    .line 29
    const v0, 0x7f0a022f

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    .line 38
    .line 39
    const v0, 0x7f0a02a0

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    .line 48
    .line 49
    const v0, 0x7f0a0282

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    .line 58
    .line 59
    const v0, 0x7f0a0244

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    .line 68
    .line 69
    const v0, 0x7f0a023c

    .line 70
    .line 71
    .line 72
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    .line 78
    .line 79
    const v0, 0x7f0a029b

    .line 80
    .line 81
    .line 82
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    .line 88
    .line 89
    const v0, 0x7f0a0e44

    .line 90
    .line 91
    .line 92
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    const-string v2, "findViewById(R.id.page_index)"

    .line 97
    .line 98
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    check-cast v0, Landroid/widget/TextView;

    .line 102
    .line 103
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8〇OO:Landroid/widget/TextView;

    .line 104
    .line 105
    const v0, 0x7f0a0e49

    .line 106
    .line 107
    .line 108
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    const-string v2, "findViewById(R.id.page_switch)"

    .line 113
    .line 114
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 118
    .line 119
    const v0, 0x7f0a1a3f

    .line 120
    .line 121
    .line 122
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    const-string v2, "findViewById(R.id.view_pager)"

    .line 127
    .line 128
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    check-cast v0, Lcom/intsig/camscanner/view/MyViewPager;

    .line 132
    .line 133
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/view/MyViewPager;

    .line 134
    .line 135
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 136
    .line 137
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 138
    .line 139
    if-eqz v0, :cond_1

    .line 140
    .line 141
    invoke-virtual {v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇0000OOO()Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    :cond_1
    iput-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 146
    .line 147
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇0o8〇()V

    .line 148
    .line 149
    .line 150
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o88o88()V

    .line 151
    .line 152
    .line 153
    return-void
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic o〇08oO80o(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇OoO0(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇o08〇(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇00o〇O8()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v0, v1

    .line 12
    :goto_0
    const-string v2, "mPageIndex"

    .line 13
    .line 14
    if-eqz v0, :cond_3

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    if-gtz v3, :cond_1

    .line 21
    .line 22
    goto :goto_2

    .line 23
    :cond_1
    sget-object v3, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 24
    .line 25
    const/4 v3, 0x2

    .line 26
    new-array v4, v3, [Ljava/lang/Object;

    .line 27
    .line 28
    iget v5, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 29
    .line 30
    const/4 v6, 0x1

    .line 31
    add-int/2addr v5, v6

    .line 32
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 33
    .line 34
    .line 35
    move-result-object v5

    .line 36
    const/4 v7, 0x0

    .line 37
    aput-object v5, v4, v7

    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    aput-object v0, v4, v6

    .line 48
    .line 49
    invoke-static {v4, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    const-string v3, "%s/%s"

    .line 54
    .line 55
    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const-string v3, "format(format, *args)"

    .line 60
    .line 61
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    iget-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8〇OO:Landroid/widget/TextView;

    .line 65
    .line 66
    if-nez v3, :cond_2

    .line 67
    .line 68
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_2
    move-object v1, v3

    .line 73
    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    .line 75
    .line 76
    goto :goto_4

    .line 77
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8〇OO:Landroid/widget/TextView;

    .line 78
    .line 79
    if-nez v0, :cond_4

    .line 80
    .line 81
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    goto :goto_3

    .line 85
    :cond_4
    move-object v1, v0

    .line 86
    :goto_3
    const-string v0, "0/0"

    .line 87
    .line 88
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    .line 90
    .line 91
    :goto_4
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0O8Oo(Ljava/lang/String;I)V
    .locals 7

    .line 1
    invoke-static {p1}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v3

    .line 5
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    const-string v0, "title"

    .line 12
    .line 13
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iput-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇oO:Ljava/lang/String;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 23
    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    const-string v1, "action_finish"

    .line 27
    .line 28
    const/4 v2, 0x1

    .line 29
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 30
    .line 31
    iget-object v6, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O〇O〇oO(Ljava/lang/String;ZLjava/lang/String;JLjava/util/List;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    iget-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 37
    .line 38
    const-wide/16 v2, 0x0

    .line 39
    .line 40
    cmp-long v4, v0, v2

    .line 41
    .line 42
    if-ltz v4, :cond_1

    .line 43
    .line 44
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 45
    .line 46
    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const-string v1, "withAppendedId(Documents\u2026ment.CONTENT_URI, mDocId)"

    .line 51
    .line 52
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    new-instance v1, LO〇00O/o〇0;

    .line 56
    .line 57
    invoke-direct {v1, p0, v0, p1, p2}, LO〇00O/o〇0;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Landroid/net/Uri;Ljava/lang/String;I)V

    .line 58
    .line 59
    .line 60
    invoke-static {v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 61
    .line 62
    .line 63
    :cond_1
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇08〇o0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;ILandroid/view/View$OnClickListener;)Landroid/widget/TextView;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇O0o〇〇o(ILandroid/view/View$OnClickListener;)Landroid/widget/TextView;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8o0o0()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8O〇008()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->Oo08(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    xor-int/lit8 v0, v0, 0x1

    .line 20
    .line 21
    return v0
.end method

.method private static final 〇8oo8888(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇8o0o0()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 17
    .line 18
    const/4 v5, 0x0

    .line 19
    new-instance v6, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$showRenameDlg$1$1;

    .line 20
    .line 21
    invoke-direct {v6, p0, p2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$showRenameDlg$1$1;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    new-instance v7, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$showRenameDlg$1$2;

    .line 25
    .line 26
    invoke-direct {v7, p2, p1, p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$showRenameDlg$1$2;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)V

    .line 27
    .line 28
    .line 29
    move-object v4, p2

    .line 30
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/mainmenu/SensitiveWordsChecker;->〇080(Ljava/lang/Boolean;Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇8〇〇8o(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, -0x1

    .line 7
    if-eq p1, v0, :cond_1

    .line 8
    .line 9
    const/4 v0, -0x3

    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇O:Z

    .line 16
    .line 17
    new-instance v0, Landroid/os/Handler;

    .line 18
    .line 19
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 20
    .line 21
    .line 22
    new-instance v1, LO〇00O/O8;

    .line 23
    .line 24
    invoke-direct {v1, p0, p1}, LO〇00O/O8;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 28
    .line 29
    .line 30
    :cond_1
    return-void
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇OoO0o0(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o〇88(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇oOO80o(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Lcom/intsig/camscanner/view/MyViewPager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO〇8O8oOo:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇ooO〇000(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O88O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇o〇88(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;I)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_e

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    goto/16 :goto_1

    .line 23
    .line 24
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0〇〇00〇o()V

    .line 25
    .line 26
    .line 27
    const/4 v0, -0x3

    .line 28
    const-string v1, "mTvEdit"

    .line 29
    .line 30
    const-string v2, "mEdtContent"

    .line 31
    .line 32
    const/4 v3, 0x0

    .line 33
    if-eq p1, v0, :cond_8

    .line 34
    .line 35
    const/4 v0, -0x2

    .line 36
    if-eq p1, v0, :cond_1

    .line 37
    .line 38
    goto/16 :goto_1

    .line 39
    .line 40
    :cond_1
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 41
    .line 42
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 43
    .line 44
    if-eqz p1, :cond_2

    .line 45
    .line 46
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 47
    .line 48
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇8(I)V

    .line 49
    .line 50
    .line 51
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O0O:Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;

    .line 52
    .line 53
    if-nez p1, :cond_3

    .line 54
    .line 55
    const-string p1, "mDeviceInterface"

    .line 56
    .line 57
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    move-object p1, v3

    .line 61
    :cond_3
    invoke-interface {p1}, Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;->〇o〇()Ljava/lang/Boolean;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 66
    .line 67
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    move-result p1

    .line 71
    if-eqz p1, :cond_5

    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 74
    .line 75
    if-nez p1, :cond_4

    .line 76
    .line 77
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    move-object p1, v3

    .line 81
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    .line 82
    .line 83
    .line 84
    const-string p1, "PPTPreviewNewActivity"

    .line 85
    .line 86
    const-string v0, "onKeyBoardStateChange hide to clear focus"

    .line 87
    .line 88
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 92
    .line 93
    if-nez p1, :cond_6

    .line 94
    .line 95
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    move-object p1, v3

    .line 99
    :cond_6
    const/4 v0, 0x0

    .line 100
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 101
    .line 102
    .line 103
    iget p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 104
    .line 105
    const/4 v2, 0x2

    .line 106
    if-ne p1, v2, :cond_e

    .line 107
    .line 108
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8o〇O0:Landroid/widget/TextView;

    .line 109
    .line 110
    if-nez p0, :cond_7

    .line 111
    .line 112
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_7
    move-object v3, p0

    .line 117
    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 118
    .line 119
    .line 120
    goto :goto_1

    .line 121
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 122
    .line 123
    if-nez p1, :cond_9

    .line 124
    .line 125
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    move-object p1, v3

    .line 129
    :cond_9
    const/4 v0, 0x1

    .line 130
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setCursorVisible(Z)V

    .line 131
    .line 132
    .line 133
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8o〇O0:Landroid/widget/TextView;

    .line 134
    .line 135
    if-nez p1, :cond_a

    .line 136
    .line 137
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    move-object p1, v3

    .line 141
    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 142
    .line 143
    .line 144
    move-result p1

    .line 145
    if-nez p1, :cond_c

    .line 146
    .line 147
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8o〇O0:Landroid/widget/TextView;

    .line 148
    .line 149
    if-nez p1, :cond_b

    .line 150
    .line 151
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    move-object p1, v3

    .line 155
    :cond_b
    const/16 v0, 0x8

    .line 156
    .line 157
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 158
    .line 159
    .line 160
    :cond_c
    iget-object p0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo:Landroid/widget/TextView;

    .line 161
    .line 162
    if-nez p0, :cond_d

    .line 163
    .line 164
    const-string p0, "mBtnRight"

    .line 165
    .line 166
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    move-object p0, v3

    .line 170
    :cond_d
    invoke-virtual {p0, v3, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 171
    .line 172
    .line 173
    :cond_e
    :goto_1
    return-void
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇o〇OO80oO()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 2
    .line 3
    const-string v1, "PPTPreviewNewActivity"

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    if-eqz v0, :cond_4

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 11
    .line 12
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->〇o〇(I)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    if-eqz v0, :cond_3

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    iget v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 25
    .line 26
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->〇o〇(I)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 33
    .line 34
    .line 35
    move-result-wide v3

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    const-wide/16 v3, -0x1

    .line 38
    .line 39
    :goto_1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 40
    .line 41
    invoke-static {v0, v3, v4}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇00〇8(Landroid/content/Context;J)I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    const/4 v0, 0x0

    .line 48
    const/4 v2, 0x0

    .line 49
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    const-string v5, "checkImageUnProcessing: "

    .line 55
    .line 56
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string v3, " = "

    .line 63
    .line 64
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto :goto_2

    .line 78
    :cond_3
    const-string v0, "mPagerAdapter.getPage(mCurrentPosition) == null"

    .line 79
    .line 80
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    goto :goto_2

    .line 84
    :cond_4
    const-string v0, "mPagerAdapter == null"

    .line 85
    .line 86
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    :goto_2
    return v2
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇〇〇OOO〇〇(Landroid/content/Intent;)V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    move-object v1, v0

    .line 10
    :goto_0
    iput-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8o:Ljava/lang/String;

    .line 11
    .line 12
    const-wide/16 v1, -0x1

    .line 13
    .line 14
    if-eqz p1, :cond_1

    .line 15
    .line 16
    const-string v3, "EXTRA_DOC_ID"

    .line 17
    .line 18
    invoke-virtual {p1, v3, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 19
    .line 20
    .line 21
    move-result-wide v3

    .line 22
    goto :goto_1

    .line 23
    :cond_1
    move-wide v3, v1

    .line 24
    :goto_1
    iput-wide v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 25
    .line 26
    if-eqz p1, :cond_2

    .line 27
    .line 28
    const-string v3, "EXTRA_NEW_PAGE_ID"

    .line 29
    .line 30
    invoke-virtual {p1, v3, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 31
    .line 32
    .line 33
    move-result-wide v3

    .line 34
    goto :goto_2

    .line 35
    :cond_2
    move-wide v3, v1

    .line 36
    :goto_2
    iput-wide v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 37
    .line 38
    if-eqz p1, :cond_3

    .line 39
    .line 40
    const-string v3, "parent_dir_title"

    .line 41
    .line 42
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    goto :goto_3

    .line 47
    :cond_3
    move-object v3, v0

    .line 48
    :goto_3
    iput-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->ooO:Ljava/lang/String;

    .line 49
    .line 50
    if-eqz p1, :cond_4

    .line 51
    .line 52
    const-string v3, "extra_folder_id"

    .line 53
    .line 54
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    goto :goto_4

    .line 59
    :cond_4
    move-object p1, v0

    .line 60
    :goto_4
    iput-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 61
    .line 62
    iget-wide v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 63
    .line 64
    cmp-long p1, v3, v1

    .line 65
    .line 66
    if-eqz p1, :cond_d

    .line 67
    .line 68
    iget-wide v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 69
    .line 70
    cmp-long p1, v3, v1

    .line 71
    .line 72
    if-nez p1, :cond_5

    .line 73
    .line 74
    goto/16 :goto_9

    .line 75
    .line 76
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇08O:Ljava/util/List;

    .line 77
    .line 78
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    new-instance p1, Ljava/util/ArrayList;

    .line 86
    .line 87
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .line 89
    .line 90
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 91
    .line 92
    check-cast v1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 93
    .line 94
    if-eqz v1, :cond_6

    .line 95
    .line 96
    iget-wide v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 97
    .line 98
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->oo〇(J)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    goto :goto_5

    .line 103
    :cond_6
    move-object v1, v0

    .line 104
    :goto_5
    const/4 v2, 0x0

    .line 105
    if-eqz v1, :cond_9

    .line 106
    .line 107
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    .line 109
    .line 110
    iput v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 111
    .line 112
    iget-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 113
    .line 114
    if-nez v1, :cond_7

    .line 115
    .line 116
    goto :goto_6

    .line 117
    :cond_7
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->o〇0(Ljava/util/ArrayList;)V

    .line 118
    .line 119
    .line 120
    :goto_6
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/view/MyViewPager;

    .line 121
    .line 122
    if-nez p1, :cond_8

    .line 123
    .line 124
    const-string p1, "mViewPager"

    .line 125
    .line 126
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    goto :goto_7

    .line 130
    :cond_8
    move-object v0, p1

    .line 131
    :goto_7
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 132
    .line 133
    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 134
    .line 135
    .line 136
    :cond_9
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8o:Ljava/lang/String;

    .line 137
    .line 138
    iget-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 139
    .line 140
    iget-wide v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 141
    .line 142
    new-instance v5, Ljava/lang/StringBuilder;

    .line 143
    .line 144
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    .line 146
    .line 147
    const-string v6, "handIntentFirst() mIntentAction"

    .line 148
    .line 149
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    const-string p1, ",mDocId = "

    .line 156
    .line 157
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    const-string p1, ",mCurPageId = "

    .line 164
    .line 165
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object p1

    .line 175
    const-string v0, "PPTPreviewNewActivity"

    .line 176
    .line 177
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 181
    .line 182
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 183
    .line 184
    if-eqz p1, :cond_a

    .line 185
    .line 186
    iget-wide v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 187
    .line 188
    invoke-virtual {p1, v3, v4}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->Oo8Oo00oo(J)Z

    .line 189
    .line 190
    .line 191
    move-result p1

    .line 192
    const/4 v1, 0x1

    .line 193
    if-ne p1, v1, :cond_a

    .line 194
    .line 195
    const/4 v2, 0x1

    .line 196
    :cond_a
    if-eqz v2, :cond_b

    .line 197
    .line 198
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 199
    .line 200
    new-instance p1, Ljava/lang/StringBuilder;

    .line 201
    .line 202
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 203
    .line 204
    .line 205
    const-string v3, "mCurPageId="

    .line 206
    .line 207
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    const-string v1, " is not scan finish"

    .line 214
    .line 215
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 219
    .line 220
    .line 221
    move-result-object p1

    .line 222
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    .line 224
    .line 225
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 226
    .line 227
    move-object v0, p1

    .line 228
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 229
    .line 230
    if-eqz v0, :cond_c

    .line 231
    .line 232
    const-string v1, "action_first"

    .line 233
    .line 234
    iget-wide v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 235
    .line 236
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 237
    .line 238
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O08000(Ljava/lang/String;JJ)V

    .line 239
    .line 240
    .line 241
    goto :goto_8

    .line 242
    :cond_b
    const-string p1, "handIntentFirst ImageScanIsFinish"

    .line 243
    .line 244
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    .line 246
    .line 247
    const-string p1, "action_first"

    .line 248
    .line 249
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo08OO8oO(Ljava/lang/String;)V

    .line 250
    .line 251
    .line 252
    :cond_c
    :goto_8
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇00o〇O8()V

    .line 253
    .line 254
    .line 255
    :cond_d
    :goto_9
    return-void
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method


# virtual methods
.method public O08O0〇O()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 13
    .line 14
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    move-object v0, v1

    .line 22
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 23
    .line 24
    if-eqz v2, :cond_1

    .line 25
    .line 26
    invoke-virtual {v2}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    .line 27
    .line 28
    .line 29
    :cond_1
    if-nez v0, :cond_2

    .line 30
    .line 31
    return-void

    .line 32
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 33
    .line 34
    .line 35
    move-result-wide v2

    .line 36
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇0o8o8〇(J)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 40
    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->getCount()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    :cond_3
    if-eqz v1, :cond_6

    .line 52
    .line 53
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    const/4 v2, 0x1

    .line 58
    if-ge v0, v2, :cond_4

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 61
    .line 62
    if-eqz v0, :cond_6

    .line 63
    .line 64
    move-object v2, v0

    .line 65
    check-cast v2, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 66
    .line 67
    if-eqz v2, :cond_6

    .line 68
    .line 69
    const-string v3, "action_delete_last"

    .line 70
    .line 71
    const/4 v4, 0x1

    .line 72
    iget-object v5, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇oO:Ljava/lang/String;

    .line 73
    .line 74
    iget-wide v6, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 75
    .line 76
    iget-object v8, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 77
    .line 78
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O〇O〇oO(Ljava/lang/String;ZLjava/lang/String;JLjava/util/List;)V

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_4
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 83
    .line 84
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 85
    .line 86
    .line 87
    move-result v3

    .line 88
    sub-int/2addr v3, v2

    .line 89
    if-le v0, v3, :cond_5

    .line 90
    .line 91
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 92
    .line 93
    sub-int/2addr v0, v2

    .line 94
    iput v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 95
    .line 96
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇00o〇O8()V

    .line 97
    .line 98
    .line 99
    :cond_6
    :goto_1
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 100
    .line 101
    new-instance v2, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    const-string v3, "pageCount="

    .line 107
    .line 108
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    const-string v1, " mCurrentPosition="

    .line 115
    .line 116
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    const-string v1, "PPTPreviewNewActivity"

    .line 127
    .line 128
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    const-string v0, "CSPPTPreview"

    .line 132
    .line 133
    const-string v1, "delete"

    .line 134
    .line 135
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public O0o0(I)Lcom/intsig/camscanner/view/ImageViewTouch;
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, -0x1

    .line 4
    .line 5
    const-string v2, "PPTPreviewNewActivity"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    if-lt p1, v1, :cond_1

    .line 9
    .line 10
    add-int/lit8 v0, v0, 0x1

    .line 11
    .line 12
    if-gt p1, v0, :cond_1

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/view/MyViewPager;

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    const-string v0, "mViewPager"

    .line 19
    .line 20
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    move-object v3, v0

    .line 25
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    :cond_1
    if-nez v3, :cond_2

    .line 45
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string v1, "getImageView is null, position="

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    check-cast v3, Lcom/intsig/camscanner/view/ImageViewTouch;

    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_2
    check-cast v3, Lcom/intsig/camscanner/view/ImageViewTouch;

    .line 70
    .line 71
    :goto_1
    return-object v3
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public bridge synthetic O0〇()Lcom/intsig/mvp/presenter/IPresenter;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇00O()Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O0〇oO〇o(Landroid/content/Intent;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 10
    .line 11
    iget v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 12
    .line 13
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇08O8o〇0(Landroid/content/Intent;JI)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO〇OOo(J)V
    .locals 6

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇08O:Ljava/util/List;

    .line 4
    .line 5
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 13
    .line 14
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 15
    .line 16
    const/4 p2, 0x0

    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    iget-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 20
    .line 21
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->oo〇(J)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    goto :goto_0

    .line 26
    :cond_0
    move-object p1, p2

    .line 27
    :goto_0
    const/4 v0, 0x1

    .line 28
    if-eqz p1, :cond_4

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 31
    .line 32
    if-eqz v1, :cond_1

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇08O:Ljava/util/List;

    .line 44
    .line 45
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    sub-int/2addr p1, v0

    .line 50
    iput p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 53
    .line 54
    if-eqz p1, :cond_2

    .line 55
    .line 56
    invoke-virtual {p1}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    .line 57
    .line 58
    .line 59
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/view/MyViewPager;

    .line 60
    .line 61
    if-nez p1, :cond_3

    .line 62
    .line 63
    const-string p1, "mViewPager"

    .line 64
    .line 65
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_3
    move-object p2, p1

    .line 70
    :goto_1
    iget p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 71
    .line 72
    invoke-virtual {p2, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 73
    .line 74
    .line 75
    :cond_4
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 76
    .line 77
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 78
    .line 79
    const/4 p2, 0x0

    .line 80
    if-eqz p1, :cond_5

    .line 81
    .line 82
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 83
    .line 84
    invoke-virtual {p1, v1, v2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->Oo8Oo00oo(J)Z

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    if-ne p1, v0, :cond_5

    .line 89
    .line 90
    goto :goto_2

    .line 91
    :cond_5
    const/4 v0, 0x0

    .line 92
    :goto_2
    if-eqz v0, :cond_6

    .line 93
    .line 94
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 95
    .line 96
    move-object v0, p1

    .line 97
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 98
    .line 99
    if-eqz v0, :cond_7

    .line 100
    .line 101
    const-string v1, "action_continue"

    .line 102
    .line 103
    iget-wide v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 104
    .line 105
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 106
    .line 107
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O08000(Ljava/lang/String;JJ)V

    .line 108
    .line 109
    .line 110
    goto :goto_3

    .line 111
    :cond_6
    const-string p1, "action_continue"

    .line 112
    .line 113
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo08OO8oO(Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    :cond_7
    :goto_3
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public Oo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oo08OO8oO(Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 4
    .line 5
    if-eqz v0, :cond_7

    .line 6
    .line 7
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 8
    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->oo〇(J)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_0
    if-eqz p1, :cond_6

    .line 17
    .line 18
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    const v2, 0xa727c90

    .line 23
    .line 24
    .line 25
    if-eq v1, v2, :cond_4

    .line 26
    .line 27
    const v2, 0x59442763

    .line 28
    .line 29
    .line 30
    if-eq v1, v2, :cond_2

    .line 31
    .line 32
    const v0, 0x6d94b367

    .line 33
    .line 34
    .line 35
    if-eq v1, v0, :cond_1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    const-string v0, "action_first"

    .line 39
    .line 40
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    if-nez p1, :cond_5

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    const-string v1, "action_retake"

    .line 48
    .line 49
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-eqz p1, :cond_6

    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 56
    .line 57
    if-eqz p1, :cond_3

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    if-eqz p1, :cond_3

    .line 64
    .line 65
    iget v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 66
    .line 67
    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    check-cast p1, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 72
    .line 73
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 74
    .line 75
    if-eqz p1, :cond_6

    .line 76
    .line 77
    invoke-virtual {p1}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_4
    const-string v0, "action_continue"

    .line 82
    .line 83
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    move-result p1

    .line 87
    if-nez p1, :cond_5

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 91
    .line 92
    if-eqz p1, :cond_6

    .line 93
    .line 94
    invoke-virtual {p1}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    .line 95
    .line 96
    .line 97
    :cond_6
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇00o〇O8()V

    .line 98
    .line 99
    .line 100
    :cond_7
    :goto_1
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public OoO〇(I)V
    .locals 10

    .line 1
    const-string v0, "PPTPreviewNewActivity"

    .line 2
    .line 3
    if-gez p1, :cond_0

    .line 4
    .line 5
    iget-boolean v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8〇o〇88:Z

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    const-string p1, "illegalstate: show halfpack with wrong flag"

    .line 10
    .line 11
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 16
    .line 17
    const-wide/16 v2, 0x0

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    iget v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 22
    .line 23
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->O8(I)J

    .line 24
    .line 25
    .line 26
    move-result-wide v4

    .line 27
    goto :goto_0

    .line 28
    :cond_1
    move-wide v4, v2

    .line 29
    :goto_0
    cmp-long v1, v4, v2

    .line 30
    .line 31
    if-lez v1, :cond_3

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 34
    .line 35
    if-eqz v1, :cond_2

    .line 36
    .line 37
    iget v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 38
    .line 39
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->O8(I)J

    .line 40
    .line 41
    .line 42
    move-result-wide v2

    .line 43
    :cond_2
    iput-wide v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 44
    .line 45
    :cond_3
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 46
    .line 47
    new-instance v3, Ljava/lang/StringBuilder;

    .line 48
    .line 49
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .line 51
    .line 52
    const-string v4, "togglePackVisibility mPageId = "

    .line 53
    .line 54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    iget-boolean v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8〇o〇88:Z

    .line 68
    .line 69
    const v1, 0x7f1300b9

    .line 70
    .line 71
    .line 72
    const v2, 0x7f13002f

    .line 73
    .line 74
    .line 75
    const v3, 0x7f13045f

    .line 76
    .line 77
    .line 78
    const-string v4, "mTxtTitle"

    .line 79
    .line 80
    const-string v5, "mPackRoot"

    .line 81
    .line 82
    const-string v6, "mEdtContent"

    .line 83
    .line 84
    const/4 v7, 0x0

    .line 85
    const/4 v8, 0x1

    .line 86
    const/4 v9, 0x0

    .line 87
    if-nez v0, :cond_e

    .line 88
    .line 89
    iput p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 92
    .line 93
    if-nez p1, :cond_4

    .line 94
    .line 95
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    move-object p1, v9

    .line 99
    :cond_4
    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 100
    .line 101
    .line 102
    iput-boolean v8, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8〇o〇88:Z

    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOoo80oO:Landroid/widget/TextView;

    .line 105
    .line 106
    if-nez p1, :cond_5

    .line 107
    .line 108
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    move-object p1, v9

    .line 112
    :cond_5
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 113
    .line 114
    if-ne v0, v8, :cond_6

    .line 115
    .line 116
    goto :goto_1

    .line 117
    :cond_6
    const v2, 0x7f13045f

    .line 118
    .line 119
    .line 120
    :goto_1
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 121
    .line 122
    .line 123
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 124
    .line 125
    if-nez p1, :cond_7

    .line 126
    .line 127
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    move-object p1, v9

    .line 131
    :cond_7
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 132
    .line 133
    if-ne v0, v8, :cond_8

    .line 134
    .line 135
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    goto :goto_2

    .line 140
    :cond_8
    move-object v0, v9

    .line 141
    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 142
    .line 143
    .line 144
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0〇〇00〇o()V

    .line 145
    .line 146
    .line 147
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O0O:Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;

    .line 148
    .line 149
    if-nez p1, :cond_9

    .line 150
    .line 151
    const-string p1, "mDeviceInterface"

    .line 152
    .line 153
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    move-object p1, v9

    .line 157
    :cond_9
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 158
    .line 159
    if-nez v0, :cond_a

    .line 160
    .line 161
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 162
    .line 163
    .line 164
    move-object v0, v9

    .line 165
    :cond_a
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;->Oo08(Landroid/view/View;)V

    .line 166
    .line 167
    .line 168
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO〇8O8oOo:Landroid/view/View;

    .line 169
    .line 170
    if-nez p1, :cond_b

    .line 171
    .line 172
    const-string p1, "mNoteTouchDismissBg"

    .line 173
    .line 174
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    move-object p1, v9

    .line 178
    :cond_b
    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 179
    .line 180
    .line 181
    iget-boolean p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO〇〇:Z

    .line 182
    .line 183
    if-eqz p1, :cond_c

    .line 184
    .line 185
    invoke-virtual {p0, v8, v7}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOoo80oO(ZZ)V

    .line 186
    .line 187
    .line 188
    :cond_c
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 189
    .line 190
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 191
    .line 192
    if-eqz p1, :cond_1a

    .line 193
    .line 194
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 195
    .line 196
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 197
    .line 198
    iget-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 199
    .line 200
    if-nez v3, :cond_d

    .line 201
    .line 202
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    goto :goto_3

    .line 206
    :cond_d
    move-object v9, v3

    .line 207
    :goto_3
    invoke-virtual {p1, v0, v1, v2, v9}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇80(IJLandroid/widget/EditText;)V

    .line 208
    .line 209
    .line 210
    goto/16 :goto_9

    .line 211
    .line 212
    :cond_e
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 213
    .line 214
    if-eq p1, v0, :cond_15

    .line 215
    .line 216
    if-gez p1, :cond_f

    .line 217
    .line 218
    goto :goto_7

    .line 219
    :cond_f
    iput p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 220
    .line 221
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOoo80oO:Landroid/widget/TextView;

    .line 222
    .line 223
    if-nez p1, :cond_10

    .line 224
    .line 225
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    move-object p1, v9

    .line 229
    :cond_10
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 230
    .line 231
    if-ne v0, v8, :cond_11

    .line 232
    .line 233
    goto :goto_4

    .line 234
    :cond_11
    const v2, 0x7f13045f

    .line 235
    .line 236
    .line 237
    :goto_4
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 238
    .line 239
    .line 240
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 241
    .line 242
    if-nez p1, :cond_12

    .line 243
    .line 244
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 245
    .line 246
    .line 247
    move-object p1, v9

    .line 248
    :cond_12
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 249
    .line 250
    if-ne v0, v8, :cond_13

    .line 251
    .line 252
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    goto :goto_5

    .line 257
    :cond_13
    move-object v0, v9

    .line 258
    :goto_5
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 259
    .line 260
    .line 261
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0〇〇00〇o()V

    .line 262
    .line 263
    .line 264
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 265
    .line 266
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 267
    .line 268
    if-eqz p1, :cond_1a

    .line 269
    .line 270
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 271
    .line 272
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 273
    .line 274
    iget-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 275
    .line 276
    if-nez v3, :cond_14

    .line 277
    .line 278
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 279
    .line 280
    .line 281
    goto :goto_6

    .line 282
    :cond_14
    move-object v9, v3

    .line 283
    :goto_6
    invoke-virtual {p1, v0, v1, v2, v9}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇80(IJLandroid/widget/EditText;)V

    .line 284
    .line 285
    .line 286
    goto :goto_9

    .line 287
    :cond_15
    :goto_7
    iput-boolean v7, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O8〇o〇88:Z

    .line 288
    .line 289
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 290
    .line 291
    if-nez p1, :cond_16

    .line 292
    .line 293
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 294
    .line 295
    .line 296
    move-object p1, v9

    .line 297
    :cond_16
    const/16 v0, 0x8

    .line 298
    .line 299
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 300
    .line 301
    .line 302
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 303
    .line 304
    if-nez p1, :cond_17

    .line 305
    .line 306
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 307
    .line 308
    .line 309
    move-object p1, v9

    .line 310
    :cond_17
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇8〇o88:Landroid/view/animation/Animation;

    .line 311
    .line 312
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 313
    .line 314
    .line 315
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 316
    .line 317
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 318
    .line 319
    if-nez v0, :cond_18

    .line 320
    .line 321
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 322
    .line 323
    .line 324
    goto :goto_8

    .line 325
    :cond_18
    move-object v9, v0

    .line 326
    :goto_8
    invoke-static {p1, v9}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 327
    .line 328
    .line 329
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 330
    .line 331
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 332
    .line 333
    if-eqz p1, :cond_19

    .line 334
    .line 335
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 336
    .line 337
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇8(I)V

    .line 338
    .line 339
    .line 340
    :cond_19
    invoke-virtual {p0, v8, v7}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOoo80oO(ZZ)V

    .line 341
    .line 342
    .line 343
    :cond_1a
    :goto_9
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇o8()V

    .line 344
    .line 345
    .line 346
    return-void
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public Ooo08()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "mPageSwitch"

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    const/16 v3, 0x8

    .line 13
    .line 14
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 18
    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    move-object v1, v0

    .line 26
    :goto_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 27
    .line 28
    const v2, 0x7f01003d

    .line 29
    .line 30
    .line 31
    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected O〇00O()Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewContract$View;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇O(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NonConstantResourceId"
        }
    .end annotation

    .line 1
    const-string v0, "v"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseMvpActivity;->dealClickAction(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    const-string v0, "mEdtContent"

    .line 14
    .line 15
    const-string v1, "CSPPTPreview"

    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    const-string v3, "PPTPreviewNewActivity"

    .line 19
    .line 20
    sparse-switch p1, :sswitch_data_0

    .line 21
    .line 22
    .line 23
    goto/16 :goto_3

    .line 24
    .line 25
    :sswitch_0
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 26
    .line 27
    if-nez p1, :cond_0

    .line 28
    .line 29
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    move-object p1, v2

    .line 33
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 37
    .line 38
    if-nez p1, :cond_1

    .line 39
    .line 40
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    move-object v2, p1

    .line 45
    :goto_0
    invoke-static {v2}, Lcom/intsig/utils/KeyboardUtils;->〇8o8o〇(Landroid/view/View;)V

    .line 46
    .line 47
    .line 48
    goto/16 :goto_3

    .line 49
    .line 50
    :sswitch_1
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 51
    .line 52
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 53
    .line 54
    if-eqz p1, :cond_7

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->oo88o8O()V

    .line 57
    .line 58
    .line 59
    goto/16 :goto_3

    .line 60
    .line 61
    :sswitch_2
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 62
    .line 63
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_OCR_EXPORT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 64
    .line 65
    const/16 v1, 0x44c

    .line 66
    .line 67
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇O8o08O(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/entity/Function;I)V

    .line 68
    .line 69
    .line 70
    goto/16 :goto_3

    .line 71
    .line 72
    :sswitch_3
    const-string p1, "click btn_rotate_left"

    .line 73
    .line 74
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-static {}, Lcom/intsig/camscanner/util/TimeLogger;->OO0o〇〇()V

    .line 78
    .line 79
    .line 80
    const-string p1, "turn"

    .line 81
    .line 82
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    iget-boolean p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇o〇:Z

    .line 86
    .line 87
    if-nez p1, :cond_2

    .line 88
    .line 89
    const-string p1, "Turn right is loading"

    .line 90
    .line 91
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    return-void

    .line 95
    :cond_2
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 96
    .line 97
    move-object v0, p1

    .line 98
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 99
    .line 100
    if-eqz v0, :cond_7

    .line 101
    .line 102
    iget v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 103
    .line 104
    iget-wide v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 105
    .line 106
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 107
    .line 108
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->o8oO〇(IJJ)V

    .line 109
    .line 110
    .line 111
    goto/16 :goto_3

    .line 112
    .line 113
    :sswitch_4
    const-string p1, "picture_processing"

    .line 114
    .line 115
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 119
    .line 120
    if-eqz p1, :cond_3

    .line 121
    .line 122
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 123
    .line 124
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->〇o〇(I)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 125
    .line 126
    .line 127
    move-result-object v2

    .line 128
    :cond_3
    if-eqz v2, :cond_7

    .line 129
    .line 130
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 131
    .line 132
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 133
    .line 134
    if-eqz p1, :cond_7

    .line 135
    .line 136
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 141
    .line 142
    .line 143
    move-result-wide v3

    .line 144
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v1

    .line 148
    const-string v2, "pageImage.pageSyncId"

    .line 149
    .line 150
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {p1, v0, v3, v4, v1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->o8(Ljava/lang/String;JLjava/lang/String;)V

    .line 154
    .line 155
    .line 156
    goto/16 :goto_3

    .line 157
    .line 158
    :sswitch_5
    const-string p1, "click btn_note"

    .line 159
    .line 160
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o〇OO80oO()Z

    .line 164
    .line 165
    .line 166
    move-result p1

    .line 167
    if-eqz p1, :cond_4

    .line 168
    .line 169
    const/4 p1, 0x1

    .line 170
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OoO〇(I)V

    .line 171
    .line 172
    .line 173
    goto/16 :goto_3

    .line 174
    .line 175
    :cond_4
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 176
    .line 177
    const v0, 0x7f1300a3

    .line 178
    .line 179
    .line 180
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 181
    .line 182
    .line 183
    goto/16 :goto_3

    .line 184
    .line 185
    :sswitch_6
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 186
    .line 187
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 188
    .line 189
    if-eqz p1, :cond_7

    .line 190
    .line 191
    iget-boolean v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇O:Z

    .line 192
    .line 193
    iget-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 194
    .line 195
    if-nez v3, :cond_5

    .line 196
    .line 197
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    goto :goto_1

    .line 201
    :cond_5
    move-object v2, v3

    .line 202
    :goto_1
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 203
    .line 204
    invoke-virtual {p1, v1, v2, v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->OoO8(ZLandroid/widget/EditText;I)V

    .line 205
    .line 206
    .line 207
    goto/16 :goto_3

    .line 208
    .line 209
    :sswitch_7
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 210
    .line 211
    move-object v3, p1

    .line 212
    check-cast v3, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 213
    .line 214
    if-eqz v3, :cond_7

    .line 215
    .line 216
    iget-boolean v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇O:Z

    .line 217
    .line 218
    iget v5, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO8:I

    .line 219
    .line 220
    iget-wide v6, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 221
    .line 222
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 223
    .line 224
    if-nez p1, :cond_6

    .line 225
    .line 226
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 227
    .line 228
    .line 229
    move-object v8, v2

    .line 230
    goto :goto_2

    .line 231
    :cond_6
    move-object v8, p1

    .line 232
    :goto_2
    invoke-virtual/range {v3 .. v8}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇0〇O0088o(ZIJLandroid/widget/EditText;)V

    .line 233
    .line 234
    .line 235
    goto :goto_3

    .line 236
    :sswitch_8
    const-string p1, "click btn_delete"

    .line 237
    .line 238
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 242
    .line 243
    if-eqz p1, :cond_7

    .line 244
    .line 245
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 246
    .line 247
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->O8(I)J

    .line 248
    .line 249
    .line 250
    move-result-wide v0

    .line 251
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 252
    .line 253
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 254
    .line 255
    if-eqz p1, :cond_7

    .line 256
    .line 257
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇oo〇(J)V

    .line 258
    .line 259
    .line 260
    goto :goto_3

    .line 261
    :sswitch_9
    const-string p1, "click btn_continue_photo"

    .line 262
    .line 263
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    .line 265
    .line 266
    const-string p1, "continue_take_photo"

    .line 267
    .line 268
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 272
    .line 273
    move-object v0, p1

    .line 274
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 275
    .line 276
    if-eqz v0, :cond_7

    .line 277
    .line 278
    const-string v1, "action_continue"

    .line 279
    .line 280
    const/4 v2, 0x0

    .line 281
    iget-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇oO:Ljava/lang/String;

    .line 282
    .line 283
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 284
    .line 285
    iget-object v6, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 286
    .line 287
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O〇O〇oO(Ljava/lang/String;ZLjava/lang/String;JLjava/util/List;)V

    .line 288
    .line 289
    .line 290
    goto :goto_3

    .line 291
    :sswitch_a
    const-string p1, "click btn_capture_retake"

    .line 292
    .line 293
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    .line 295
    .line 296
    const-string p1, "retake"

    .line 297
    .line 298
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    .line 300
    .line 301
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 302
    .line 303
    move-object v0, p1

    .line 304
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 305
    .line 306
    if-eqz v0, :cond_7

    .line 307
    .line 308
    const-string v1, "action_retake"

    .line 309
    .line 310
    const/4 v2, 0x0

    .line 311
    iget-object v3, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o〇oO:Ljava/lang/String;

    .line 312
    .line 313
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 314
    .line 315
    iget-object v6, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oo8ooo8O:Ljava/util/ArrayList;

    .line 316
    .line 317
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O〇O〇oO(Ljava/lang/String;ZLjava/lang/String;JLjava/util/List;)V

    .line 318
    .line 319
    .line 320
    :cond_7
    :goto_3
    return-void

    .line 321
    :sswitch_data_0
    .sparse-switch
        0x7f0a022f -> :sswitch_a
        0x7f0a023c -> :sswitch_9
        0x7f0a0244 -> :sswitch_8
        0x7f0a0262 -> :sswitch_7
        0x7f0a0263 -> :sswitch_6
        0x7f0a0282 -> :sswitch_5
        0x7f0a029b -> :sswitch_4
        0x7f0a02a0 -> :sswitch_3
        0x7f0a0b96 -> :sswitch_2
        0x7f0a0c7d -> :sswitch_1
        0x7f0a12bd -> :sswitch_0
    .end sparse-switch
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 6

    .line 1
    const-string p1, "PPTPreviewNewActivity"

    .line 2
    .line 3
    const-string v0, "onCreate"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iput-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    new-instance p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$PhoneImpl;

    .line 11
    .line 12
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity$PhoneImpl;-><init>(Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;)V

    .line 13
    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O0O:Lcom/intsig/camscanner/ppt/preview/PPTDeviceInterface;

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oO〇O0O()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇OOO〇〇(Landroid/content/Intent;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8O〇008()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 32
    .line 33
    const/4 v3, 0x0

    .line 34
    const/4 v4, 0x2

    .line 35
    const/4 v5, 0x0

    .line 36
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->〇00(Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;JZILjava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o08oOO()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o88oo〇O(I)V
    .locals 1

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-eq p1, v0, :cond_1

    .line 5
    .line 6
    const/4 v0, 0x2

    .line 7
    if-eq p1, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇OO8ooO8〇:Landroidx/appcompat/app/ActionBar;

    .line 11
    .line 12
    if-eqz p1, :cond_3

    .line 13
    .line 14
    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar;->hide()V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const/16 v0, 0x505

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇OO8ooO8〇:Landroidx/appcompat/app/ActionBar;

    .line 32
    .line 33
    if-eqz p1, :cond_3

    .line 34
    .line 35
    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar;->hide()V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    const/16 v0, 0x500

    .line 48
    .line 49
    invoke-virtual {p1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇OO8ooO8〇:Landroidx/appcompat/app/ActionBar;

    .line 53
    .line 54
    if-eqz p1, :cond_3

    .line 55
    .line 56
    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar;->show()V

    .line 57
    .line 58
    .line 59
    :cond_3
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public oOoo80oO(ZZ)V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO〇〇:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    xor-int/2addr v0, v1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO〇〇:Z

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇o88o08〇:Landroid/view/animation/Animation;

    .line 8
    .line 9
    const-string v2, "mPackAdBottomBar"

    .line 10
    .line 11
    const/4 v3, 0x0

    .line 12
    if-nez v0, :cond_4

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    move-object v0, v3

    .line 22
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_4

    .line 27
    .line 28
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 29
    .line 30
    iget-object v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 31
    .line 32
    if-nez v4, :cond_1

    .line 33
    .line 34
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    move-object v4, v3

    .line 38
    :cond_1
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    int-to-float v4, v4

    .line 43
    const/4 v5, 0x0

    .line 44
    invoke-direct {v0, v5, v5, v4, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 45
    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇o88o08〇:Landroid/view/animation/Animation;

    .line 48
    .line 49
    const-wide/16 v6, 0x1f4

    .line 50
    .line 51
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 52
    .line 53
    .line 54
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 55
    .line 56
    iget-object v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 57
    .line 58
    if-nez v4, :cond_2

    .line 59
    .line 60
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    move-object v4, v3

    .line 64
    :cond_2
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    .line 65
    .line 66
    .line 67
    move-result v4

    .line 68
    int-to-float v4, v4

    .line 69
    invoke-direct {v0, v5, v5, v5, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 70
    .line 71
    .line 72
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo80:Landroid/view/animation/Animation;

    .line 73
    .line 74
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 75
    .line 76
    .line 77
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .line 78
    .line 79
    iget-object v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 80
    .line 81
    if-nez v4, :cond_3

    .line 82
    .line 83
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    move-object v4, v3

    .line 87
    :cond_3
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    neg-int v4, v4

    .line 92
    int-to-float v4, v4

    .line 93
    invoke-direct {v0, v5, v5, v4, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 94
    .line 95
    .line 96
    iput-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇00O0:Landroid/view/animation/Animation;

    .line 97
    .line 98
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 99
    .line 100
    .line 101
    :cond_4
    iget-boolean v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->oOO〇〇:Z

    .line 102
    .line 103
    const/16 v4, 0x3ef

    .line 104
    .line 105
    const-string v5, "mPageSwitch"

    .line 106
    .line 107
    if-eqz v0, :cond_b

    .line 108
    .line 109
    const/4 p1, 0x0

    .line 110
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o88oo〇O(I)V

    .line 111
    .line 112
    .line 113
    iget-object p2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 114
    .line 115
    if-nez p2, :cond_5

    .line 116
    .line 117
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    move-object p2, v3

    .line 121
    :cond_5
    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 122
    .line 123
    .line 124
    iget-object p2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇o88o08〇:Landroid/view/animation/Animation;

    .line 125
    .line 126
    if-eqz p2, :cond_9

    .line 127
    .line 128
    iget-object p2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 129
    .line 130
    if-nez p2, :cond_6

    .line 131
    .line 132
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    move-object p2, v3

    .line 136
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇o88o08〇:Landroid/view/animation/Animation;

    .line 137
    .line 138
    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 139
    .line 140
    .line 141
    iget-object p2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 142
    .line 143
    if-nez p2, :cond_7

    .line 144
    .line 145
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    move-object p2, v3

    .line 149
    :cond_7
    invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V

    .line 150
    .line 151
    .line 152
    iget-object p2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 153
    .line 154
    if-nez p2, :cond_8

    .line 155
    .line 156
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    move-object p2, v3

    .line 160
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇o88o08〇:Landroid/view/animation/Animation;

    .line 161
    .line 162
    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 163
    .line 164
    .line 165
    :cond_9
    iget-object p2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 166
    .line 167
    if-nez p2, :cond_a

    .line 168
    .line 169
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 170
    .line 171
    .line 172
    goto :goto_0

    .line 173
    :cond_a
    move-object v3, p2

    .line 174
    :goto_0
    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    .line 175
    .line 176
    .line 177
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 178
    .line 179
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 180
    .line 181
    if-eqz p1, :cond_14

    .line 182
    .line 183
    invoke-virtual {p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->OOO〇O0()Landroid/os/Handler;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    if-eqz p1, :cond_14

    .line 188
    .line 189
    invoke-virtual {p1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 190
    .line 191
    .line 192
    goto/16 :goto_3

    .line 193
    .line 194
    :cond_b
    if-eqz p1, :cond_c

    .line 195
    .line 196
    const/4 p1, 0x2

    .line 197
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o88oo〇O(I)V

    .line 198
    .line 199
    .line 200
    goto :goto_1

    .line 201
    :cond_c
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o88oo〇O(I)V

    .line 202
    .line 203
    .line 204
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 205
    .line 206
    if-nez p1, :cond_d

    .line 207
    .line 208
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    move-object p1, v3

    .line 212
    :cond_d
    const/16 v0, 0x8

    .line 213
    .line 214
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 215
    .line 216
    .line 217
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo80:Landroid/view/animation/Animation;

    .line 218
    .line 219
    if-eqz p1, :cond_12

    .line 220
    .line 221
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇08oOOO0:Landroid/view/View;

    .line 222
    .line 223
    if-nez p1, :cond_e

    .line 224
    .line 225
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    move-object p1, v3

    .line 229
    :cond_e
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo80:Landroid/view/animation/Animation;

    .line 230
    .line 231
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 232
    .line 233
    .line 234
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 235
    .line 236
    if-nez p1, :cond_f

    .line 237
    .line 238
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    move-object p1, v3

    .line 242
    :cond_f
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 243
    .line 244
    .line 245
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Ooo08:Landroid/view/View;

    .line 246
    .line 247
    if-nez p1, :cond_10

    .line 248
    .line 249
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 250
    .line 251
    .line 252
    move-object p1, v3

    .line 253
    :cond_10
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇00O0:Landroid/view/animation/Animation;

    .line 254
    .line 255
    if-nez v0, :cond_11

    .line 256
    .line 257
    const-string v0, "mAnimSwitchBottomOut"

    .line 258
    .line 259
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 260
    .line 261
    .line 262
    goto :goto_2

    .line 263
    :cond_11
    move-object v3, v0

    .line 264
    :goto_2
    invoke-virtual {p1, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 265
    .line 266
    .line 267
    :cond_12
    if-eqz p2, :cond_13

    .line 268
    .line 269
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 270
    .line 271
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 272
    .line 273
    if-eqz p1, :cond_14

    .line 274
    .line 275
    invoke-virtual {p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->OOO〇O0()Landroid/os/Handler;

    .line 276
    .line 277
    .line 278
    move-result-object p1

    .line 279
    if-eqz p1, :cond_14

    .line 280
    .line 281
    const-wide/16 v0, 0xbb8

    .line 282
    .line 283
    invoke-virtual {p1, v4, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 284
    .line 285
    .line 286
    goto :goto_3

    .line 287
    :cond_13
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 288
    .line 289
    check-cast p1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 290
    .line 291
    if-eqz p1, :cond_14

    .line 292
    .line 293
    invoke-virtual {p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->OOO〇O0()Landroid/os/Handler;

    .line 294
    .line 295
    .line 296
    move-result-object p1

    .line 297
    if-eqz p1, :cond_14

    .line 298
    .line 299
    invoke-virtual {p1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 300
    .line 301
    .line 302
    :cond_14
    :goto_3
    return-void
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    const/16 v0, 0x65

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    const/16 v0, 0x44c

    .line 6
    .line 7
    if-eq p1, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O〇o8()V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/4 v0, -0x1

    .line 15
    if-ne p2, v0, :cond_2

    .line 16
    .line 17
    if-eqz p3, :cond_2

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 20
    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O8ooOoo〇(Landroid/content/Intent;)V

    .line 28
    .line 29
    .line 30
    :cond_2
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p2    # Landroid/view/KeyEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "event"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x4

    .line 7
    if-ne p1, v0, :cond_2

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇0O〇O00O:Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 10
    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const-string p1, "mPackRoot"

    .line 14
    .line 15
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const/4 p1, 0x0

    .line 19
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    const/4 p2, 0x1

    .line 24
    if-nez p1, :cond_1

    .line 25
    .line 26
    const/4 p1, -0x1

    .line 27
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OoO〇(I)V

    .line 28
    .line 29
    .line 30
    return p2

    .line 31
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OOo00()V

    .line 32
    .line 33
    .line 34
    return p2

    .line 35
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    return p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "intent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroidx/activity/ComponentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 7
    .line 8
    .line 9
    const-string v0, "EXTRA_ACTION"

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "EXTRA_NEW_PAGE_ID"

    .line 16
    .line 17
    const-wide/16 v2, -0x1

    .line 18
    .line 19
    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 20
    .line 21
    .line 22
    move-result-wide v4

    .line 23
    const-string v1, "parent_dir_title"

    .line 24
    .line 25
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iput-object p1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->ooO:Ljava/lang/String;

    .line 30
    .line 31
    new-instance p1, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v1, "onNewIntent() "

    .line 37
    .line 38
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string v1, ",newPageId ="

    .line 45
    .line 46
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    const-string v1, "PPTPreviewNewActivity"

    .line 57
    .line 58
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    if-nez p1, :cond_5

    .line 66
    .line 67
    if-eqz v0, :cond_5

    .line 68
    .line 69
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    const v1, 0xa727c90

    .line 74
    .line 75
    .line 76
    if-eq p1, v1, :cond_3

    .line 77
    .line 78
    const v1, 0x59442763

    .line 79
    .line 80
    .line 81
    if-eq p1, v1, :cond_1

    .line 82
    .line 83
    const v1, 0x5e5db150

    .line 84
    .line 85
    .line 86
    if-eq p1, v1, :cond_0

    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_0
    const-string p1, "action_back"

    .line 90
    .line 91
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_1
    const-string p1, "action_retake"

    .line 96
    .line 97
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    move-result p1

    .line 101
    if-nez p1, :cond_2

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_2
    cmp-long p1, v4, v2

    .line 105
    .line 106
    if-eqz p1, :cond_5

    .line 107
    .line 108
    invoke-direct {p0, v4, v5}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO8〇O8(J)V

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_3
    const-string p1, "action_continue"

    .line 113
    .line 114
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 115
    .line 116
    .line 117
    move-result p1

    .line 118
    if-nez p1, :cond_4

    .line 119
    .line 120
    goto :goto_0

    .line 121
    :cond_4
    cmp-long p1, v4, v2

    .line 122
    .line 123
    if-eqz p1, :cond_5

    .line 124
    .line 125
    invoke-virtual {p0, v4, v5}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OO〇OOo(J)V

    .line 126
    .line 127
    .line 128
    :cond_5
    :goto_0
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method protected onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSPPTPreview"

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇00O0O〇o()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 4
    .line 5
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO80(Landroid/content/Context;J)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "PPTPreviewNewActivity"

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    iget-wide v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 14
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v4, "saveNote has delete mPageId="

    .line 21
    .line 22
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 37
    .line 38
    const/4 v2, 0x0

    .line 39
    if-nez v0, :cond_1

    .line 40
    .line 41
    const-string v0, "mEdtContent"

    .line 42
    .line 43
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    move-object v0, v2

    .line 47
    :cond_1
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iget-object v3, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 56
    .line 57
    check-cast v3, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 58
    .line 59
    if-eqz v3, :cond_2

    .line 60
    .line 61
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 62
    .line 63
    invoke-virtual {v3, v4, v5}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O8〇o(J)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    goto :goto_0

    .line 68
    :cond_2
    move-object v3, v2

    .line 69
    :goto_0
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    if-nez v3, :cond_6

    .line 74
    .line 75
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 76
    .line 77
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 78
    .line 79
    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    const-string v4, "withAppendedId(Documents\u2026age.CONTENT_URI, mPageId)"

    .line 84
    .line 85
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    new-instance v4, Landroid/content/ContentValues;

    .line 89
    .line 90
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v5, "note"

    .line 94
    .line 95
    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    if-eqz v0, :cond_3

    .line 103
    .line 104
    invoke-virtual {v0, v3, v4, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 105
    .line 106
    .line 107
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;

    .line 108
    .line 109
    if-eqz v0, :cond_4

    .line 110
    .line 111
    iget v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 112
    .line 113
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/ppt/preview/CustomPagerAdapter;->〇o〇(I)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 114
    .line 115
    .line 116
    move-result-object v2

    .line 117
    :cond_4
    if-nez v2, :cond_5

    .line 118
    .line 119
    const-string v0, "saveNote pageImage == null"

    .line 120
    .line 121
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    return-void

    .line 125
    :cond_5
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 126
    .line 127
    invoke-virtual {v2}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 128
    .line 129
    .line 130
    move-result-wide v1

    .line 131
    const/4 v3, 0x3

    .line 132
    const/4 v4, 0x1

    .line 133
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO(Landroid/content/Context;JIZ)V

    .line 134
    .line 135
    .line 136
    iget-wide v6, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 137
    .line 138
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 139
    .line 140
    invoke-static {v0, v6, v7}, Lcom/intsig/camscanner/app/DBUtil;->Ooo8(Landroid/content/Context;J)V

    .line 141
    .line 142
    .line 143
    iget-object v5, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 144
    .line 145
    const/4 v8, 0x3

    .line 146
    const/4 v9, 0x1

    .line 147
    const/4 v10, 0x0

    .line 148
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 149
    .line 150
    .line 151
    goto :goto_1

    .line 152
    :cond_6
    const-string v0, "the same note, not save"

    .line 153
    .line 154
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    :goto_1
    return-void
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d00a2

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0O00oO()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo(Z)V

    .line 3
    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/loadimage/CacheKey;

    .line 6
    .line 7
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇O〇〇O8:J

    .line 8
    .line 9
    const/4 v3, 0x1

    .line 10
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/loadimage/CacheKey;-><init>(JI)V

    .line 11
    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->oO80(Lcom/intsig/camscanner/loadimage/CacheKey;)V

    .line 14
    .line 15
    .line 16
    iget v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 17
    .line 18
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O0o0(I)Lcom/intsig/camscanner/view/ImageViewTouch;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->〇〇8O0〇8()V

    .line 25
    .line 26
    .line 27
    :cond_0
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 28
    .line 29
    check-cast v1, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 30
    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    iget v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o8oOOo:I

    .line 34
    .line 35
    invoke-virtual {v1, v2, v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇〇〇0〇〇0(ILcom/intsig/camscanner/view/ImageViewTouch;)V

    .line 36
    .line 37
    .line 38
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 39
    .line 40
    check-cast v0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 41
    .line 42
    if-eqz v0, :cond_2

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->O〇8O8〇008()V

    .line 45
    .line 46
    .line 47
    :cond_2
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇80〇()V
    .locals 2

    .line 1
    const-string v0, "PPT"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->ooO:Ljava/lang/String;

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/Util;->O8〇o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x1

    .line 10
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/util/Util;->OOO(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->O08〇oO8〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public 〇Oo〇O()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->OOo00()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o8OO0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mEdtContent"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    const-string v0, "CSPPTPreview"

    .line 22
    .line 23
    const-string v1, "remark"

    .line 24
    .line 25
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    :cond_1
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇08O(Landroid/database/Cursor;J)Z
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    const/4 v1, 0x4

    .line 5
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    move-object p1, v0

    .line 11
    :goto_0
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->〇〇o8(Ljava/lang/String;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/4 v2, 0x1

    .line 16
    if-eqz v1, :cond_2

    .line 17
    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v3, "rotatenoinkimage before rotation "

    .line 24
    .line 25
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const/16 v3, 0x10e

    .line 29
    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    const-string v4, "PPTPreviewNewActivity"

    .line 38
    .line 39
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-static {p2, p3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇00〇8(J)Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    if-nez v1, :cond_1

    .line 47
    .line 48
    const/high16 v1, 0x3f800000    # 1.0f

    .line 49
    .line 50
    invoke-static {}, Lcom/intsig/camscanner/tsapp/Const;->〇080()I

    .line 51
    .line 52
    .line 53
    move-result v5

    .line 54
    invoke-static {p1, v3, v1, v5, v0}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    .line 59
    .line 60
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .line 62
    .line 63
    const-string v3, "scaleImageResult = "

    .line 64
    .line 65
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_1
    const/4 v2, 0x0

    .line 80
    :goto_1
    invoke-static {p2, p3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o〇8(J)V

    .line 81
    .line 82
    .line 83
    new-instance p2, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    const-string p3, "rotatenoinkimage noInkPath "

    .line 89
    .line 90
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    :cond_2
    return v2
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇〇o0o()Lcom/intsig/camscanner/view/MyViewPager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mViewPager"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇o8()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 4
    .line 5
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO80(Landroid/content/Context;J)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "PPTPreviewNewActivity"

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    iget-wide v2, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 14
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v4, "saveOcrUserTextToDB has delete mPageId="

    .line 21
    .line 22
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->o0OoOOo0:Landroid/widget/EditText;

    .line 37
    .line 38
    const/4 v2, 0x0

    .line 39
    if-nez v0, :cond_1

    .line 40
    .line 41
    const-string v0, "mEdtContent"

    .line 42
    .line 43
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    move-object v0, v2

    .line 47
    :cond_1
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iget-object v3, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 56
    .line 57
    check-cast v3, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 58
    .line 59
    if-eqz v3, :cond_2

    .line 60
    .line 61
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 62
    .line 63
    invoke-virtual {v3, v4, v5}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->o0ooO(J)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    goto :goto_0

    .line 68
    :cond_2
    move-object v3, v2

    .line 69
    :goto_0
    if-nez v3, :cond_5

    .line 70
    .line 71
    iget-object v4, p0, Lcom/intsig/mvp/activity/BaseMvpActivity;->ooo0〇〇O:Lcom/intsig/mvp/presenter/IPresenter;

    .line 72
    .line 73
    check-cast v4, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;

    .line 74
    .line 75
    if-eqz v4, :cond_3

    .line 76
    .line 77
    iget-wide v5, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 78
    .line 79
    invoke-virtual {v4, v5, v6}, Lcom/intsig/camscanner/ppt/preview/PPTPreviewPresenter;->〇00〇8(J)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v4

    .line 83
    move-object v5, v4

    .line 84
    goto :goto_1

    .line 85
    :cond_3
    move-object v5, v2

    .line 86
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 87
    .line 88
    .line 89
    move-result v4

    .line 90
    if-nez v4, :cond_5

    .line 91
    .line 92
    if-eqz v5, :cond_4

    .line 93
    .line 94
    const-string v6, "\r"

    .line 95
    .line 96
    const-string v7, ""

    .line 97
    .line 98
    const/4 v8, 0x0

    .line 99
    const/4 v9, 0x4

    .line 100
    const/4 v10, 0x0

    .line 101
    invoke-static/range {v5 .. v10}, Lkotlin/text/StringsKt;->〇oOO8O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    goto :goto_2

    .line 106
    :cond_4
    move-object v3, v2

    .line 107
    :cond_5
    :goto_2
    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    if-nez v4, :cond_8

    .line 112
    .line 113
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    if-eqz v4, :cond_6

    .line 118
    .line 119
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    if-eqz v3, :cond_6

    .line 124
    .line 125
    goto :goto_3

    .line 126
    :cond_6
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 127
    .line 128
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 129
    .line 130
    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 131
    .line 132
    .line 133
    move-result-object v3

    .line 134
    const-string v4, "withAppendedId(Documents\u2026age.CONTENT_URI, mPageId)"

    .line 135
    .line 136
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    new-instance v4, Landroid/content/ContentValues;

    .line 140
    .line 141
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 142
    .line 143
    .line 144
    const-string v5, "ocr_result_user"

    .line 145
    .line 146
    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    if-eqz v0, :cond_7

    .line 154
    .line 155
    invoke-virtual {v0, v3, v4, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 156
    .line 157
    .line 158
    move-result v0

    .line 159
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 160
    .line 161
    .line 162
    move-result-object v2

    .line 163
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 164
    .line 165
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .line 167
    .line 168
    const-string v3, "saveOcrUserTextToDB: "

    .line 169
    .line 170
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    .line 182
    .line 183
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 184
    .line 185
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇oo〇O〇80:J

    .line 186
    .line 187
    const/4 v3, 0x3

    .line 188
    const/4 v4, 0x1

    .line 189
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO(Landroid/content/Context;JIZ)V

    .line 190
    .line 191
    .line 192
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 193
    .line 194
    iget-wide v1, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 195
    .line 196
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/app/DBUtil;->Ooo8(Landroid/content/Context;J)V

    .line 197
    .line 198
    .line 199
    iget-object v3, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 200
    .line 201
    iget-wide v4, p0, Lcom/intsig/camscanner/ppt/preview/PPTPreviewNewActivity;->〇o0O:J

    .line 202
    .line 203
    const/4 v6, 0x3

    .line 204
    const/4 v7, 0x1

    .line 205
    const/4 v8, 0x0

    .line 206
    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 207
    .line 208
    .line 209
    goto :goto_4

    .line 210
    :cond_8
    :goto_3
    const-string v0, "saveOcrUserTextToDB the same ocr result"

    .line 211
    .line 212
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    :goto_4
    return-void
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public 〇〇〇00()I
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/base/ToolbarThemeGet;->〇080()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

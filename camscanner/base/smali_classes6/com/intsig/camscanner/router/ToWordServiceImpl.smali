.class public final Lcom/intsig/camscanner/router/ToWordServiceImpl;
.super Lcom/intsig/router/service/BaseRouterServiceImpl;
.source "ToWordServiceImpl.kt"

# interfaces
.implements Lcom/intsig/router/service/RouterToWordService;


# annotations
.annotation build Lcom/alibaba/android/arouter/facade/annotation/Route;
    name = "\u8f6cWord"
    path = "/page/to_word"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/router/ToWordServiceImpl$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Companion:Lcom/intsig/camscanner/router/ToWordServiceImpl$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final FROM_PART_CS_AI:Ljava/lang/String; = "cs_ai"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final TAG:Ljava/lang/String; = "ToWordServiceImpl"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/router/ToWordServiceImpl$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/router/ToWordServiceImpl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/router/ToWordServiceImpl;->Companion:Lcom/intsig/camscanner/router/ToWordServiceImpl$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/router/service/BaseRouterServiceImpl;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final openWordPreview(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 7
    invoke-static {p1, p2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v3

    .line 8
    invoke-static {p1, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const-string p2, "cs_ai"

    .line 9
    invoke-static {p4, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x7

    const/4 v6, 0x7

    goto :goto_0

    :cond_0
    const/4 p2, 0x6

    const/4 v6, 0x6

    .line 10
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/pagelist/WordPreviewActivity;->o8oOOo:Lcom/intsig/camscanner/pagelist/WordPreviewActivity$Companion;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x40

    const/4 v10, 0x0

    move-object v1, p1

    move-object v5, p3

    invoke-static/range {v0 .. v10}, Lcom/intsig/camscanner/pagelist/WordPreviewActivity$Companion;->〇080(Lcom/intsig/camscanner/pagelist/WordPreviewActivity$Companion;Landroid/app/Activity;Ljava/lang/String;JLjava/util/ArrayList;ILjava/lang/String;ZILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public init(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/router/service/BaseRouterServiceImpl;->mContext:Landroid/content/Context;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public openWordPreview(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    .line 1
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    return-void

    .line 2
    :cond_2
    invoke-static {p1, p2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    .line 3
    invoke-static {v0, v1}, Lcom/intsig/camscanner/image2word/Image2WordNavigator;->〇80〇808〇O(J)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4
    sget-object p2, Lcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature$CsListRecommend;->〇o00〇〇Oo:Lcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature$CsListRecommend;

    invoke-virtual {p2}, Lcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature;->〇080()Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    const-string p2, "list_bottom_word"

    goto :goto_2

    :cond_3
    const-string p2, "other_to_word"

    .line 5
    :goto_2
    invoke-static {p1, v0, v1, p2}, Lcom/intsig/camscanner/image2word/Image2WordNavigator;->Oo08(Landroidx/fragment/app/FragmentActivity;JLjava/lang/String;)V

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    .line 6
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/intsig/camscanner/router/ToWordServiceImpl;->openWordPreview(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    :goto_3
    return-void
.end method

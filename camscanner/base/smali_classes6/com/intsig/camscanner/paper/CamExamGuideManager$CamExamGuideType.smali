.class public final enum Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;
.super Ljava/lang/Enum;
.source "CamExamGuidManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/paper/CamExamGuideManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CamExamGuideType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_DOWNLOAD_FROM_DETAIL:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_DOWNLOAD_FROM_LIST:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_ENTRANCE_TYPE_CS_LIST_CHANGE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_ENTRANCE_TYPE_CS_LIST_MORE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_ENTRANCE_TYPE_DOC_LIST:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_ENTRANCE_TYPE_SCAN_DONE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_ENTRANCE_TYPE_SCAN_PAPER:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_ENTRANCE_TYPE_TOOL_BOX:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

.field public static final enum CAM_EXAM_GUIDE_ENTRANCE_TYPE_UNDEFINE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;


# instance fields
.field private final pageId:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final type:I


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;
    .locals 3

    .line 1
    const/16 v0, 0x9

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_UNDEFINE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_SCAN_DONE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_DOC_LIST:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_CS_LIST_MORE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_TOOL_BOX:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_SCAN_PAPER:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_CS_LIST_CHANGE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_DOWNLOAD_FROM_LIST:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_DOWNLOAD_FROM_DETAIL:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    return-object v0
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, ""

    .line 5
    .line 6
    const-string v3, "CAM_EXAM_GUIDE_ENTRANCE_TYPE_UNDEFINE"

    .line 7
    .line 8
    invoke-direct {v0, v3, v1, v1, v2}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_UNDEFINE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    const-string v2, "CSScanDoneOperationBubble"

    .line 17
    .line 18
    const-string v3, "CAM_EXAM_GUIDE_ENTRANCE_TYPE_SCAN_DONE"

    .line 19
    .line 20
    invoke-direct {v0, v3, v1, v1, v2}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_SCAN_DONE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 26
    .line 27
    const/4 v1, 0x2

    .line 28
    const-string v2, "CSListOperationBubble"

    .line 29
    .line 30
    const-string v3, "CAM_EXAM_GUIDE_ENTRANCE_TYPE_DOC_LIST"

    .line 31
    .line 32
    invoke-direct {v0, v3, v1, v1, v2}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_DOC_LIST:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 38
    .line 39
    const/4 v1, 0x3

    .line 40
    const-string v2, "CSMoreOperationBubble"

    .line 41
    .line 42
    const-string v3, "CAM_EXAM_GUIDE_ENTRANCE_TYPE_CS_LIST_MORE"

    .line 43
    .line 44
    invoke-direct {v0, v3, v1, v1, v2}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 45
    .line 46
    .line 47
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_CS_LIST_MORE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 48
    .line 49
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 50
    .line 51
    const/4 v1, 0x4

    .line 52
    const-string v2, "CSMainApplicationQuizgoChoosePage"

    .line 53
    .line 54
    const-string v3, "CAM_EXAM_GUIDE_ENTRANCE_TYPE_TOOL_BOX"

    .line 55
    .line 56
    invoke-direct {v0, v3, v1, v1, v2}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_TOOL_BOX:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 60
    .line 61
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 62
    .line 63
    const/4 v1, 0x5

    .line 64
    const-string v2, "CSScanDemoPop"

    .line 65
    .line 66
    const-string v3, "CAM_EXAM_GUIDE_ENTRANCE_TYPE_SCAN_PAPER"

    .line 67
    .line 68
    invoke-direct {v0, v3, v1, v1, v2}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 69
    .line 70
    .line 71
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_SCAN_PAPER:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 72
    .line 73
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 74
    .line 75
    const/4 v1, 0x6

    .line 76
    const-string v2, "CSList"

    .line 77
    .line 78
    const-string v3, "CAM_EXAM_GUIDE_ENTRANCE_TYPE_CS_LIST_CHANGE"

    .line 79
    .line 80
    invoke-direct {v0, v3, v1, v1, v2}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 81
    .line 82
    .line 83
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_CS_LIST_CHANGE:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 84
    .line 85
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 86
    .line 87
    const-string v1, "CAM_EXAM_GUIDE_DOWNLOAD_FROM_LIST"

    .line 88
    .line 89
    const/4 v2, 0x7

    .line 90
    const-string v3, "CSBeeAppGuidPop"

    .line 91
    .line 92
    invoke-direct {v0, v1, v2, v2, v3}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 93
    .line 94
    .line 95
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_DOWNLOAD_FROM_LIST:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 96
    .line 97
    new-instance v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 98
    .line 99
    const-string v1, "CAM_EXAM_GUIDE_DOWNLOAD_FROM_DETAIL"

    .line 100
    .line 101
    const/16 v2, 0x8

    .line 102
    .line 103
    invoke-direct {v0, v1, v2, v2, v3}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 104
    .line 105
    .line 106
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_DOWNLOAD_FROM_DETAIL:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 107
    .line 108
    invoke-static {}, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->$values()[Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    sput-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->$VALUES:[Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 113
    .line 114
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->type:I

    .line 5
    .line 6
    iput-object p4, p0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->pageId:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->$VALUES:[Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getPageId()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->pageId:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->type:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

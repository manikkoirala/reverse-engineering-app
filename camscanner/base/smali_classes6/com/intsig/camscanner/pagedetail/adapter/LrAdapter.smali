.class public final Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;
.super Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;
.source "LrAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO0o〇〇:Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrView;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:Z

.field private oO80:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/text/Editable;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:Z

.field private final 〇80〇808〇O:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrView;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8o8o〇:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrElement;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇O8o08O:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->OO0o〇〇:Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/pagedetail/PageDetailInterface;Ljava/lang/String;Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/pagedetail/PageDetailInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "fragment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "tag"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "pageDetailModel"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;-><init>(Lcom/intsig/camscanner/pagedetail/PageDetailInterface;Ljava/lang/String;Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;)V

    .line 17
    .line 18
    .line 19
    new-instance p2, Landroidx/lifecycle/ViewModelProvider;

    .line 20
    .line 21
    invoke-interface {p1}, Lcom/intsig/camscanner/pagedetail/PageDetailInterface;->〇oOoo〇()Landroidx/lifecycle/ViewModelStoreOwner;

    .line 22
    .line 23
    .line 24
    move-result-object p3

    .line 25
    invoke-direct {p2, p3}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 26
    .line 27
    .line 28
    const-class p3, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 29
    .line 30
    invoke-virtual {p2, p3}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 31
    .line 32
    .line 33
    move-result-object p2

    .line 34
    check-cast p2, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 35
    .line 36
    iput-object p2, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 37
    .line 38
    new-instance p3, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$mOnTableCellClickListener$1;

    .line 39
    .line 40
    invoke-direct {p3, p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$mOnTableCellClickListener$1;-><init>(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)V

    .line 41
    .line 42
    .line 43
    iput-object p3, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->oO80:Lkotlin/jvm/functions/Function1;

    .line 44
    .line 45
    new-instance p3, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$mUndoRedoUpdateListener$1;

    .line 46
    .line 47
    invoke-direct {p3, p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$mUndoRedoUpdateListener$1;-><init>(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)V

    .line 48
    .line 49
    .line 50
    iput-object p3, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇80〇808〇O:Lkotlin/jvm/functions/Function1;

    .line 51
    .line 52
    new-instance p3, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$mTextSelectionChangeListener$1;

    .line 53
    .line 54
    invoke-direct {p3, p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$mTextSelectionChangeListener$1;-><init>(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)V

    .line 55
    .line 56
    .line 57
    iput-object p3, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function1;

    .line 58
    .line 59
    new-instance p3, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$mOnChildFocusChangeListener$1;

    .line 60
    .line 61
    invoke-direct {p3, p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$mOnChildFocusChangeListener$1;-><init>(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)V

    .line 62
    .line 63
    .line 64
    iput-object p3, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇8o8o〇:Lkotlin/jvm/functions/Function1;

    .line 65
    .line 66
    new-instance p3, Ljava/util/HashMap;

    .line 67
    .line 68
    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    .line 69
    .line 70
    .line 71
    iput-object p3, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O8o08O:Ljava/util/HashMap;

    .line 72
    .line 73
    invoke-interface {p1}, Lcom/intsig/camscanner/pagedetail/PageDetailInterface;->oOO0880O()Landroidx/lifecycle/LifecycleOwner;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    invoke-virtual {p2}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o0ooO()Landroidx/lifecycle/MutableLiveData;

    .line 78
    .line 79
    .line 80
    move-result-object p3

    .line 81
    new-instance v0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$1;

    .line 82
    .line 83
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$1;-><init>(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)V

    .line 84
    .line 85
    .line 86
    new-instance v1, LO0〇/O8;

    .line 87
    .line 88
    invoke-direct {v1, v0}, LO0〇/O8;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p3, p1, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p2}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->oo〇()Landroidx/lifecycle/MutableLiveData;

    .line 95
    .line 96
    .line 97
    move-result-object p3

    .line 98
    new-instance v0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$2;

    .line 99
    .line 100
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$2;-><init>(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)V

    .line 101
    .line 102
    .line 103
    new-instance v1, LO0〇/Oo08;

    .line 104
    .line 105
    invoke-direct {v1, v0}, LO0〇/Oo08;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p3, p1, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {p2}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇00()Landroidx/lifecycle/MutableLiveData;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    new-instance p3, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$3;

    .line 116
    .line 117
    invoke-direct {p3, p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter$3;-><init>(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)V

    .line 118
    .line 119
    .line 120
    new-instance v0, LO0〇/o〇0;

    .line 121
    .line 122
    invoke-direct {v0, p3}, LO0〇/o〇0;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {p2, p1, v0}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 126
    .line 127
    .line 128
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic O8(Ljava/lang/String;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇0000OOO(Ljava/lang/String;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O8ooOoo〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O〇()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇0OOo〇0()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_2

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇080:Lcom/intsig/camscanner/pagedetail/PageDetailInterface;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/camscanner/pagedetail/PageDetailInterface;->getCurrentPosition()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇o〇:Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;

    .line 21
    .line 22
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;->O8(I)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const-string v1, "page.pageSyncId"

    .line 34
    .line 35
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇oOO8O8(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    :cond_2
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)Lcom/intsig/camscanner/pic2word/lr/LrViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final OO0o〇〇〇〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic Oo08(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->OO0o〇〇〇〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇oOO8O8(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Lcom/intsig/camscanner/loadimage/PageImage;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O888o0o(Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Lcom/intsig/camscanner/loadimage/PageImage;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final oo88o8O(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/16 v0, 0x8

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇080:Lcom/intsig/camscanner/pagedetail/PageDetailInterface;

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/pagedetail/PageDetailInterface;->o0(Z)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇8o8o〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇00(ILandroid/view/View;)V
    .locals 4

    .line 1
    instance-of v0, p2, Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    check-cast p2, Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 6
    .line 7
    invoke-virtual {p2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇0OOo〇0()Z

    .line 8
    .line 9
    .line 10
    move-result p2

    .line 11
    if-eqz p2, :cond_2

    .line 12
    .line 13
    iget-object p2, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇o〇:Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;

    .line 14
    .line 15
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;->O8(I)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const-string p2, "page.pageSyncId"

    .line 27
    .line 28
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇oOO8O8(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    instance-of v0, p2, Landroid/view/ViewGroup;

    .line 36
    .line 37
    if-eqz v0, :cond_2

    .line 38
    .line 39
    check-cast p2, Landroid/view/ViewGroup;

    .line 40
    .line 41
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-lez v0, :cond_2

    .line 46
    .line 47
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    const/4 v1, 0x0

    .line 52
    :goto_0
    if-ge v1, v0, :cond_2

    .line 53
    .line 54
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    const-string v3, "child"

    .line 59
    .line 60
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇00(ILandroid/view/View;)V

    .line 64
    .line 65
    .line 66
    add-int/lit8 v1, v1, 0x1

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :cond_2
    :goto_1
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇0000OOO(Ljava/lang/String;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)V
    .locals 2

    .line 1
    const-string v0, "$pageSyncId"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$lrImageJson"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "save Word Data To File pageSyncId: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v1, "LrAdapter"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const-string v0, "pic_2_office"

    .line 34
    .line 35
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/util/LrUtil;->〇O8o08O(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/util/LrUtil;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v0, p0, p1}, Lcom/intsig/camscanner/pic2word/util/LrUtil;->〇080(Ljava/lang/String;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)Z

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇0〇O0088o(Ljava/lang/String;)J
    .locals 3

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-wide/16 v1, 0x0

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-wide v1

    .line 10
    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/File;

    .line 11
    .line 12
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    .line 19
    .line 20
    .line 21
    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    return-wide v0

    .line 23
    :catch_0
    move-exception p1

    .line 24
    const-string v0, "LrAdapter"

    .line 25
    .line 26
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 27
    .line 28
    .line 29
    return-wide v1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final 〇80〇808〇O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇8o8o〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O00(Landroid/view/View;)Lcom/intsig/camscanner/pic2word/lr/LrView;
    .locals 3

    .line 1
    instance-of v0, p1, Landroid/view/ViewGroup;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    check-cast p1, Landroid/view/ViewGroup;

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-lt v0, v2, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    instance-of v0, p1, Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 21
    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    move-object v1, p1

    .line 25
    check-cast v1, Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 26
    .line 27
    :cond_0
    return-object v1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final 〇O888o0o(Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Lcom/intsig/camscanner/loadimage/PageImage;)V
    .locals 1

    .line 1
    const-string v0, "$vb"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "this$0"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 12
    .line 13
    const-string v0, "vb.ivIconWord"

    .line 14
    .line 15
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-static {p0, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 20
    .line 21
    .line 22
    iget-object p0, p1, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O8o08O:Ljava/util/HashMap;

    .line 23
    .line 24
    invoke-virtual {p2}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 25
    .line 26
    .line 27
    move-result-wide p1

    .line 28
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 33
    .line 34
    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)Lcom/intsig/camscanner/pic2word/lr/LrView;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O〇()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O〇()Lcom/intsig/camscanner/pic2word/lr/LrView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇080:Lcom/intsig/camscanner/pagedetail/PageDetailInterface;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagedetail/PageDetailInterface;->OO8〇()Landroid/view/View;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return-object v0

    .line 11
    :cond_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O00(Landroid/view/View;)Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇oOO8O8(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->O8ooOoo〇(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    new-instance v1, LO0〇/〇o〇;

    .line 11
    .line 12
    invoke-direct {v1, p1, v0}, LO0〇/〇o〇;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->oo88o8O(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->Oo08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇888(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇80〇808〇O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇8O0〇8(J)Lcom/bumptech/glide/request/RequestOptions;
    .locals 2

    .line 1
    new-instance v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/utils/image/GlideImageExtKey;

    .line 15
    .line 16
    invoke-direct {v1, p1, p2}, Lcom/intsig/utils/image/GlideImageExtKey;-><init>(J)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇0(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const-string p2, "RequestOptions()\n       \u2026eImageExtKey(modifyTime))"

    .line 24
    .line 25
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    check-cast p1, Lcom/bumptech/glide/request/RequestOptions;

    .line 29
    .line 30
    return-object p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final OOO〇O0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final OoO8(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/lr/LrImageJson;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "pageSyncId"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->O8ooOoo〇(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O〇()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇0OOo〇0()Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v1, 0x0

    .line 26
    :goto_0
    if-eqz v1, :cond_1

    .line 27
    .line 28
    new-instance v1, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v2, "getWordData need to save to file pageSyncId: "

    .line 34
    .line 35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    const-string v2, "LrAdapter"

    .line 46
    .line 47
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇oOO8O8(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    :cond_1
    return-object v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final O〇8O8〇008(Landroidx/viewpager/widget/ViewPager;)V
    .locals 4

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, 0x1

    .line 9
    if-lt v0, v1, :cond_1

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/4 v1, 0x0

    .line 16
    :goto_0
    if-ge v1, v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const-string v3, "parent.getChildAt(i)"

    .line 23
    .line 24
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇00(ILandroid/view/View;)V

    .line 28
    .line 29
    .line 30
    add-int/lit8 v1, v1, 0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "container"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "o"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p3, Landroid/view/View;

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O00(Landroid/view/View;)Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    if-nez p1, :cond_0

    .line 25
    .line 26
    return-void

    .line 27
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇0OOo〇0()Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-eqz p1, :cond_2

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇o〇:Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;

    .line 34
    .line 35
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;->O8(I)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    if-nez p1, :cond_1

    .line 40
    .line 41
    return-void

    .line 42
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    const-string p2, "page.pageSyncId"

    .line 47
    .line 48
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇oOO8O8(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :cond_2
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "container"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇o〇:Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;

    .line 11
    .line 12
    invoke-virtual {v1, p2}, Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;->O8(I)Lcom/intsig/camscanner/loadimage/PageImage;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-eqz v1, :cond_9

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const-string v3, "page.pageSyncId"

    .line 23
    .line 24
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->o800o8O(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-static {v0}, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->inflate(Landroid/view/LayoutInflater;)Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const-string v3, "inflate(LayoutInflater.from(context))"

    .line 40
    .line 41
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    const-string v4, "vb.root"

    .line 49
    .line 50
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->o〇00O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 54
    .line 55
    const/4 v5, 0x0

    .line 56
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setWordMarkVisible(Z)V

    .line 57
    .line 58
    .line 59
    iget-object v4, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇o〇:Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;

    .line 60
    .line 61
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagedetail/mode/PageDetailModel;->〇o00〇〇Oo()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    const/16 v6, 0x7c

    .line 66
    .line 67
    if-ne v4, v6, :cond_0

    .line 68
    .line 69
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 70
    .line 71
    const v6, 0x7f08072f

    .line 72
    .line 73
    .line 74
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_0
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 79
    .line 80
    const v6, 0x7f080731

    .line 81
    .line 82
    .line 83
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 84
    .line 85
    .line 86
    :goto_0
    invoke-static {v2}, Lcom/intsig/camscanner/pic2word/presenter/Image2jsonCallable;->O8(Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)Z

    .line 87
    .line 88
    .line 89
    move-result v4

    .line 90
    if-nez v4, :cond_1

    .line 91
    .line 92
    iget-object v6, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O8o08O:Ljava/util/HashMap;

    .line 93
    .line 94
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 95
    .line 96
    .line 97
    move-result-wide v7

    .line 98
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 99
    .line 100
    .line 101
    move-result-object v7

    .line 102
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 103
    .line 104
    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    .line 106
    .line 107
    :cond_1
    const/16 v6, 0x8

    .line 108
    .line 109
    if-eqz v4, :cond_6

    .line 110
    .line 111
    if-eqz v2, :cond_2

    .line 112
    .line 113
    invoke-virtual {v2}, Lcom/intsig/camscanner/pic2word/lr/LrImageJson;->getPages()Ljava/util/List;

    .line 114
    .line 115
    .line 116
    move-result-object v2

    .line 117
    if-eqz v2, :cond_2

    .line 118
    .line 119
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 120
    .line 121
    .line 122
    move-result-object v2

    .line 123
    check-cast v2, Lcom/intsig/camscanner/pic2word/lr/LrPageBean;

    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_2
    const/4 v2, 0x0

    .line 127
    :goto_1
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->o〇00O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 128
    .line 129
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 130
    .line 131
    .line 132
    const/4 v7, 0x1

    .line 133
    invoke-virtual {v4, v7}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setScaleEnable(Z)V

    .line 134
    .line 135
    .line 136
    iget-object v8, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇80〇808〇O:Lkotlin/jvm/functions/Function1;

    .line 137
    .line 138
    invoke-virtual {v4, v8}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇0(Lkotlin/jvm/functions/Function1;)V

    .line 139
    .line 140
    .line 141
    iget-object v8, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function1;

    .line 142
    .line 143
    invoke-virtual {v4, v8}, Lcom/intsig/camscanner/pic2word/lr/LrView;->Oo08(Lkotlin/jvm/functions/Function1;)V

    .line 144
    .line 145
    .line 146
    iget-object v8, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->oO80:Lkotlin/jvm/functions/Function1;

    .line 147
    .line 148
    invoke-virtual {v4, v8}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setOnTableCellClickListener(Lkotlin/jvm/functions/Function1;)V

    .line 149
    .line 150
    .line 151
    iget-object v8, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇8o8o〇:Lkotlin/jvm/functions/Function1;

    .line 152
    .line 153
    invoke-virtual {v4, v8}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setOnChildFocusChangeListener(Lkotlin/jvm/functions/Function1;)V

    .line 154
    .line 155
    .line 156
    iget-object v8, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇〇888:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 157
    .line 158
    invoke-virtual {v4, v8}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMImageJsonParam(Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;)V

    .line 159
    .line 160
    .line 161
    iget-boolean v8, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->o〇0:Z

    .line 162
    .line 163
    invoke-virtual {v4, v8}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMTempOnlyKeepTable(Z)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v4, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setPageData(Lcom/intsig/camscanner/pic2word/lr/LrPageBean;)V

    .line 167
    .line 168
    .line 169
    iget-object v2, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇080:Lcom/intsig/camscanner/pagedetail/PageDetailInterface;

    .line 170
    .line 171
    invoke-interface {v2}, Lcom/intsig/camscanner/pagedetail/PageDetailInterface;->getCurrentPosition()I

    .line 172
    .line 173
    .line 174
    move-result v2

    .line 175
    if-ne p2, v2, :cond_3

    .line 176
    .line 177
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->o〇00O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 178
    .line 179
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/pic2word/lr/LrView;->〇〇〇0〇〇0(Z)Z

    .line 180
    .line 181
    .line 182
    :cond_3
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 183
    .line 184
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 185
    .line 186
    .line 187
    iget-object v2, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O8o08O:Ljava/util/HashMap;

    .line 188
    .line 189
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 190
    .line 191
    .line 192
    move-result-wide v8

    .line 193
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 194
    .line 195
    .line 196
    move-result-object v4

    .line 197
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    .line 199
    .line 200
    move-result-object v2

    .line 201
    check-cast v2, Ljava/lang/Boolean;

    .line 202
    .line 203
    if-nez v2, :cond_4

    .line 204
    .line 205
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 206
    .line 207
    :cond_4
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 208
    .line 209
    .line 210
    move-result v2

    .line 211
    const-string v4, "vb.ivIconWord"

    .line 212
    .line 213
    if-eqz v2, :cond_5

    .line 214
    .line 215
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 216
    .line 217
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    .line 219
    .line 220
    invoke-static {v2, v7}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 221
    .line 222
    .line 223
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 224
    .line 225
    new-instance v4, LO0〇/〇080;

    .line 226
    .line 227
    invoke-direct {v4, v0, p0, v1}, LO0〇/〇080;-><init>(Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;Lcom/intsig/camscanner/loadimage/PageImage;)V

    .line 228
    .line 229
    .line 230
    const-wide/16 v0, 0xbb8

    .line 231
    .line 232
    invoke-virtual {v2, v4, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 233
    .line 234
    .line 235
    goto/16 :goto_3

    .line 236
    .line 237
    :cond_5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 238
    .line 239
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    .line 241
    .line 242
    invoke-static {v0, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 243
    .line 244
    .line 245
    goto :goto_3

    .line 246
    :cond_6
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->OO:Landroid/widget/ImageView;

    .line 247
    .line 248
    const-string v4, "vb.ivImage"

    .line 249
    .line 250
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    .line 252
    .line 253
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇()Ljava/lang/String;

    .line 257
    .line 258
    .line 259
    move-result-object v4

    .line 260
    invoke-static {v4}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 261
    .line 262
    .line 263
    move-result v4

    .line 264
    if-eqz v4, :cond_7

    .line 265
    .line 266
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇()Ljava/lang/String;

    .line 267
    .line 268
    .line 269
    move-result-object v4

    .line 270
    goto :goto_2

    .line 271
    :cond_7
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/PageImage;->〇80()Ljava/lang/String;

    .line 272
    .line 273
    .line 274
    move-result-object v4

    .line 275
    :goto_2
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇0〇O0088o(Ljava/lang/String;)J

    .line 276
    .line 277
    .line 278
    move-result-wide v7

    .line 279
    invoke-static {v2}, Lcom/bumptech/glide/Glide;->o800o8O(Landroid/view/View;)Lcom/bumptech/glide/RequestManager;

    .line 280
    .line 281
    .line 282
    move-result-object v9

    .line 283
    invoke-virtual {v9, v4}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 284
    .line 285
    .line 286
    move-result-object v4

    .line 287
    invoke-direct {p0, v7, v8}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇〇8O0〇8(J)Lcom/bumptech/glide/request/RequestOptions;

    .line 288
    .line 289
    .line 290
    move-result-object v7

    .line 291
    invoke-virtual {v4, v7}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 292
    .line 293
    .line 294
    move-result-object v4

    .line 295
    new-instance v7, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 296
    .line 297
    invoke-direct {v7}, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;-><init>()V

    .line 298
    .line 299
    .line 300
    invoke-virtual {v7}, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;->o〇0()Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 301
    .line 302
    .line 303
    move-result-object v7

    .line 304
    invoke-virtual {v4, v7}, Lcom/bumptech/glide/RequestBuilder;->o8O0(Lcom/bumptech/glide/TransitionOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 305
    .line 306
    .line 307
    move-result-object v4

    .line 308
    invoke-virtual {v4, v2}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 309
    .line 310
    .line 311
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/PageImage;->OO0o〇〇()I

    .line 312
    .line 313
    .line 314
    move-result v1

    .line 315
    const/4 v2, 0x2

    .line 316
    if-ne v1, v2, :cond_8

    .line 317
    .line 318
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 319
    .line 320
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 321
    .line 322
    .line 323
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 324
    .line 325
    new-instance v1, LO0〇/〇o00〇〇Oo;

    .line 326
    .line 327
    invoke-direct {v1, p0}, LO0〇/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;)V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 331
    .line 332
    .line 333
    goto :goto_3

    .line 334
    :cond_8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemPageLrWordBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 335
    .line 336
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 337
    .line 338
    .line 339
    goto :goto_3

    .line 340
    :cond_9
    const-string v1, "LrAdapter"

    .line 341
    .line 342
    const-string v2, "instantiateItem page is null !"

    .line 343
    .line 344
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    .line 346
    .line 347
    new-instance v3, Landroid/view/View;

    .line 348
    .line 349
    invoke-direct {v3, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 350
    .line 351
    .line 352
    :goto_3
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇o00〇〇Oo:Ljava/lang/String;

    .line 353
    .line 354
    new-instance v1, Ljava/lang/StringBuilder;

    .line 355
    .line 356
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 357
    .line 358
    .line 359
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    .line 361
    .line 362
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 363
    .line 364
    .line 365
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 366
    .line 367
    .line 368
    move-result-object p2

    .line 369
    invoke-virtual {v3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 370
    .line 371
    .line 372
    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    .line 373
    .line 374
    const/4 v0, -0x1

    .line 375
    invoke-direct {p2, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 376
    .line 377
    .line 378
    invoke-virtual {p1, v3, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 379
    .line 380
    .line 381
    return-object v3
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public final o800o8O(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/lr/LrImageJson;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "pageSyncId"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->O8ooOoo〇(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    const-string v0, "pic_2_office"

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/util/LrUtil;->〇O8o08O(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/util/LrUtil;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pic2word/util/LrUtil;->〇80〇808〇O(Ljava/lang/String;)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-nez v1, :cond_1

    .line 30
    .line 31
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-eqz v1, :cond_1

    .line 36
    .line 37
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/util/LrUtil;->oO80(Ljava/lang/String;)Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    if-eqz v0, :cond_1

    .line 42
    .line 43
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/presenter/Image2jsonCallable;->O8(Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)Z

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    if-eqz v1, :cond_1

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 50
    .line 51
    invoke-virtual {v1, p1, v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->O〇O〇oO(Ljava/lang/String;Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)V

    .line 52
    .line 53
    .line 54
    return-object v0

    .line 55
    :cond_1
    const/4 p1, 0x0

    .line 56
    return-object p1
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final oo〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇O〇()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v1, v0}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇O00()Landroidx/lifecycle/MutableLiveData;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v1, v0}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o〇O8〇〇o()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->Oo08:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇(Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->〇〇888:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇080()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇80〇808〇O()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/pagedetail/adapter/PageDetailBaseAdapter;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->O8ooOoo〇()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagedetail/adapter/LrAdapter;->Oo08:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

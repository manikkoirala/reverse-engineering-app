.class public final Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;
.super Ljava/lang/Object;
.source "ImageDownloadViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Request"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:J

.field private 〇o00〇〇Oo:Ljava/lang/String;

.field private 〇o〇:J


# direct methods
.method public constructor <init>()V
    .locals 8

    .line 1
    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;-><init>(JLjava/lang/String;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;J)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-wide p1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇080:J

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇o00〇〇Oo:Ljava/lang/String;

    .line 5
    iput-wide p4, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇o〇:J

    return-void
.end method

.method public synthetic constructor <init>(JLjava/lang/String;JILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 4

    and-int/lit8 p7, p6, 0x1

    const-wide/16 v0, -0x1

    if-eqz p7, :cond_0

    move-wide v2, v0

    goto :goto_0

    :cond_0
    move-wide v2, p1

    :goto_0
    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    const/4 p3, 0x0

    :cond_1
    move-object p7, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    move-wide p5, v0

    goto :goto_1

    :cond_2
    move-wide p5, p4

    :goto_1
    move-object p1, p0

    move-wide p2, v2

    move-object p4, p7

    .line 6
    invoke-direct/range {p1 .. p6}, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;-><init>(JLjava/lang/String;J)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-wide v2, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇080:J

    .line 8
    .line 9
    check-cast p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;

    .line 10
    .line 11
    iget-wide v4, p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇080:J

    .line 12
    .line 13
    cmp-long p1, v2, v4

    .line 14
    .line 15
    if-nez p1, :cond_1

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    :cond_1
    return v1
    .line 19
    .line 20
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇080:J

    .line 2
    .line 3
    invoke-static {v0, v1}, Landroidx/privacysandbox/ads/adservices/adselection/〇080;->〇080(J)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toString()Ljava/lang/String;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇080:J

    .line 2
    .line 3
    iget-object v2, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇o00〇〇Oo:Ljava/lang/String;

    .line 4
    .line 5
    new-instance v3, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v4, "Request(pageId="

    .line 11
    .line 12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v0, ", pageSyncId="

    .line 19
    .line 20
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v0, ")"

    .line 27
    .line 28
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇080()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇o〇:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇080:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$Request;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

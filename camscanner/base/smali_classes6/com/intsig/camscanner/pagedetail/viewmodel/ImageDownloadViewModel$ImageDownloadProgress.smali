.class public final Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;
.super Ljava/lang/Object;
.source "ImageDownloadViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ImageDownloadProgress"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o〇0:Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:Z

.field private Oo08:I

.field private 〇080:J

.field private 〇o00〇〇Oo:F

.field private 〇o〇:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->o〇0:Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 9

    .line 1
    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1f

    const/4 v8, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;-><init>(JFZZIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(JFZZI)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-wide p1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇080:J

    .line 4
    iput p3, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o00〇〇Oo:F

    .line 5
    iput-boolean p4, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o〇:Z

    .line 6
    iput-boolean p5, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->O8:Z

    .line 7
    iput p6, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->Oo08:I

    return-void
.end method

.method public synthetic constructor <init>(JFZZIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    const-wide/16 p1, -0x1

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p7, 0x2

    if-eqz p1, :cond_1

    const/high16 p3, -0x40800000    # -1.0f

    const/high16 v3, -0x40800000    # -1.0f

    goto :goto_0

    :cond_1
    move v3, p3

    :goto_0
    and-int/lit8 p1, p7, 0x4

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    move v4, p4

    :goto_1
    and-int/lit8 p1, p7, 0x8

    if-eqz p1, :cond_3

    const/4 v5, 0x0

    goto :goto_2

    :cond_3
    move v5, p5

    :goto_2
    and-int/lit8 p1, p7, 0x10

    if-eqz p1, :cond_4

    const/4 p6, -0x1

    const/4 v6, -0x1

    goto :goto_3

    :cond_4
    move v6, p6

    :goto_3
    move-object v0, p0

    .line 8
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;-><init>(JFZZI)V

    return-void
.end method


# virtual methods
.method public final O8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->O8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo08()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->Oo08:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    return v2

    .line 11
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;

    .line 12
    .line 13
    iget-wide v3, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇080:J

    .line 14
    .line 15
    iget-wide v5, p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇080:J

    .line 16
    .line 17
    cmp-long v1, v3, v5

    .line 18
    .line 19
    if-eqz v1, :cond_2

    .line 20
    .line 21
    return v2

    .line 22
    :cond_2
    iget v1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o00〇〇Oo:F

    .line 23
    .line 24
    iget v3, p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o00〇〇Oo:F

    .line 25
    .line 26
    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_3

    .line 31
    .line 32
    return v2

    .line 33
    :cond_3
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o〇:Z

    .line 34
    .line 35
    iget-boolean v3, p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o〇:Z

    .line 36
    .line 37
    if-eq v1, v3, :cond_4

    .line 38
    .line 39
    return v2

    .line 40
    :cond_4
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->O8:Z

    .line 41
    .line 42
    iget-boolean v3, p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->O8:Z

    .line 43
    .line 44
    if-eq v1, v3, :cond_5

    .line 45
    .line 46
    return v2

    .line 47
    :cond_5
    iget v1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->Oo08:I

    .line 48
    .line 49
    iget p1, p1, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->Oo08:I

    .line 50
    .line 51
    if-eq v1, p1, :cond_6

    .line 52
    .line 53
    return v2

    .line 54
    :cond_6
    return v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇080:J

    .line 2
    .line 3
    invoke-static {v0, v1}, Landroidx/privacysandbox/ads/adservices/adselection/〇080;->〇080(J)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    mul-int/lit8 v0, v0, 0x1f

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o00〇〇Oo:F

    .line 10
    .line 11
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    add-int/2addr v0, v1

    .line 16
    mul-int/lit8 v0, v0, 0x1f

    .line 17
    .line 18
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o〇:Z

    .line 19
    .line 20
    const/4 v2, 0x1

    .line 21
    if-eqz v1, :cond_0

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    :cond_0
    add-int/2addr v0, v1

    .line 25
    mul-int/lit8 v0, v0, 0x1f

    .line 26
    .line 27
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->O8:Z

    .line 28
    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    move v2, v1

    .line 33
    :goto_0
    add-int/2addr v0, v2

    .line 34
    mul-int/lit8 v0, v0, 0x1f

    .line 35
    .line 36
    iget v1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->Oo08:I

    .line 37
    .line 38
    add-int/2addr v0, v1

    .line 39
    return v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public toString()Ljava/lang/String;
    .locals 8
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇080:J

    .line 2
    .line 3
    iget v2, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o00〇〇Oo:F

    .line 4
    .line 5
    iget-boolean v3, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o〇:Z

    .line 6
    .line 7
    iget-boolean v4, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->O8:Z

    .line 8
    .line 9
    iget v5, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->Oo08:I

    .line 10
    .line 11
    new-instance v6, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v7, "ImageDownloadProgress(pageId="

    .line 17
    .line 18
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v0, ", progress="

    .line 25
    .line 26
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v0, ", showLoading="

    .line 33
    .line 34
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    const-string v0, ", showRetry="

    .line 41
    .line 42
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string v0, ", updateType="

    .line 49
    .line 50
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v0, ")"

    .line 57
    .line 58
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇080()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇080:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o00〇〇Oo:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/ImageDownloadViewModel$ImageDownloadProgress;->〇o〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

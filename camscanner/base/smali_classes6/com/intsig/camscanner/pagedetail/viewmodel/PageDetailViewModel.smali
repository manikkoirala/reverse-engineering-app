.class public final Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "PageDetailViewModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final OO:Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;

.field private final 〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 5
    .line 6
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;

    .line 12
    .line 13
    sget-object v1, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager$Companion$ManagerType$CsDetail;->O8:Lcom/intsig/camscanner/pagelist/reader/DocReaderManager$Companion$ManagerType$CsDetail;

    .line 14
    .line 15
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;-><init>(Lcom/intsig/camscanner/pagelist/reader/DocReaderManager$Companion$ManagerType;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->OO:Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;

    .line 19
    .line 20
    return-void
    .line 21
.end method

.method private static final oo88o8O(Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;)V
    .locals 5

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->o0:Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;->〇080:Landroid/net/Uri;

    .line 12
    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    if-nez v1, :cond_1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    const/4 v2, 0x1

    .line 23
    new-array v2, v2, [Landroid/net/Uri;

    .line 24
    .line 25
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 26
    .line 27
    const-string v4, "CONTENT_URI"

    .line 28
    .line 29
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    const/4 v4, 0x0

    .line 33
    aput-object v3, v2, v4

    .line 34
    .line 35
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/UriUtils;->〇o00〇〇Oo(Ljava/lang/String;[Landroid/net/Uri;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_2

    .line 40
    .line 41
    iget-object p0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 42
    .line 43
    iget-object v0, v0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;->〇080:Landroid/net/Uri;

    .line 44
    .line 45
    invoke-static {v0}, Lcom/intsig/camscanner/util/UriUtils;->〇o〇(Landroid/net/Uri;)J

    .line 46
    .line 47
    .line 48
    move-result-wide v0

    .line 49
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    :cond_2
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->〇oo〇(Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->oo88o8O(Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇oo〇(Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$lifecycleDataChangerManager"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->o0:Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public final Oooo8o0〇()Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->OO:Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O00(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/ViewModelStoreOwner;)Landroidx/lifecycle/LiveData;
    .locals 4
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/lifecycle/ViewModelStoreOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/lifecycle/LifecycleOwner;",
            "Landroidx/lifecycle/ViewModelStoreOwner;",
            ")",
            "Landroidx/lifecycle/LiveData<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "lifecycleOwner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "viewModelStoreOwner"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 12
    .line 13
    const-string v1, "page_detail_load_page"

    .line 14
    .line 15
    invoke-direct {v0, p1, v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const-wide/16 v1, 0x7d0

    .line 19
    .line 20
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇8o8o〇(J)V

    .line 21
    .line 22
    .line 23
    new-instance v1, LO〇〇O80o8/O8;

    .line 24
    .line 25
    invoke-direct {v1, p0}, LO〇〇O80o8/O8;-><init>(Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->OO0o〇〇(Ljava/lang/Runnable;)V

    .line 29
    .line 30
    .line 31
    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    const-string v3, "getInstance()"

    .line 38
    .line 39
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {v1, p2, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 43
    .line 44
    .line 45
    const-class p2, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 46
    .line 47
    invoke-virtual {v1, p2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    check-cast p2, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 52
    .line 53
    invoke-virtual {p2}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 54
    .line 55
    .line 56
    move-result-object p2

    .line 57
    new-instance v1, LO〇〇O80o8/Oo08;

    .line 58
    .line 59
    invoke-direct {v1, p0, v0}, LO〇〇O80o8/Oo08;-><init>(Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p2, p1, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 63
    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/camscanner/pagedetail/viewmodel/PageDetailViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 66
    .line 67
    return-object p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

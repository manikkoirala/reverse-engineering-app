.class public interface abstract Lcom/intsig/camscanner/pagedetail/PageDetailInterface;
.super Ljava/lang/Object;
.source "PageDetailInterface.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract O0〇OO8()V
.end method

.method public abstract O0〇oo()Z
.end method

.method public abstract OO()Lcom/google/android/material/tabs/TabLayout;
.end method

.method public abstract OO0o〇〇(I)V
.end method

.method public abstract OO0〇〇8(Lcom/intsig/callback/Callback0;)V
    .param p1    # Lcom/intsig/callback/Callback0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract OO88o()V
.end method

.method public abstract OO8〇()Landroid/view/View;
.end method

.method public abstract OOO〇O0()Z
.end method

.method public abstract OOo0O()Z
.end method

.method public abstract Oo80(Lcom/intsig/camscanner/view/dragexit/DragLayout;)Lcom/intsig/camscanner/view/dragexit/DragLayout$DragListener;
    .param p1    # Lcom/intsig/camscanner/view/dragexit/DragLayout;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract Oo〇O8o〇8()Z
.end method

.method public abstract O〇08oOOO0()Z
.end method

.method public abstract getCurrentPosition()I
.end method

.method public abstract o0(Z)V
.end method

.method public abstract o0O0(Lcom/intsig/callback/Callback0;)V
    .param p1    # Lcom/intsig/callback/Callback0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract o800o8O()Z
.end method

.method public abstract oO0〇〇O8o()Lcom/intsig/camscanner/pic2word/presenter/LrActPresenterImpl;
.end method

.method public abstract oOO0880O()Landroidx/lifecycle/LifecycleOwner;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract oOO〇〇(ILcom/intsig/camscanner/view/ImageViewTouch;)V
.end method

.method public abstract o〇O8〇〇o()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract 〇080OO8〇0()V
.end method

.method public abstract 〇0〇O0088o()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract 〇800OO〇0O()V
.end method

.method public abstract 〇8〇o〇8()Z
.end method

.method public abstract 〇O00()Landroid/view/View;
.end method

.method public abstract 〇O8o08O()J
.end method

.method public abstract 〇OO〇00〇0O(Lcom/intsig/callback/Callback0;)V
    .param p1    # Lcom/intsig/callback/Callback0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract 〇O〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;
.end method

.method public abstract 〇O〇80o08O()Lorg/json/JSONObject;
.end method

.method public abstract 〇oOoo〇()Landroidx/lifecycle/ViewModelStoreOwner;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract 〇〇0o()Landroidx/fragment/app/FragmentManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

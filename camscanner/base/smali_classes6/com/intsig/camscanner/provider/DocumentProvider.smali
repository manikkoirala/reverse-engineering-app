.class public Lcom/intsig/camscanner/provider/DocumentProvider;
.super Landroid/content/ContentProvider;
.source "DocumentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;
    }
.end annotation


# static fields
.field private static OO:Z = false

.field public static o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper; = null

.field public static 〇08O〇00〇o:Ljava/lang/String; = "com.intsig.provider.Documents"

.field public static 〇OOo8〇0:Landroid/content/UriMatcher;


# instance fields
.field private final o0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 6

    .line 1
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "current_share_time"

    .line 5
    .line 6
    const-string v1, "ocr_title_local"

    .line 7
    .line 8
    const-string v2, "ocr_title_cloud"

    .line 9
    .line 10
    const-string v3, "sync_ui_state"

    .line 11
    .line 12
    const-string v4, "_data"

    .line 13
    .line 14
    const-string v5, "state"

    .line 15
    .line 16
    invoke-static/range {v0 .. v5}, L〇8〇〇8o/〇080;->〇080(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/provider/DocumentProvider;->o0:Ljava/util/List;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static O8(Landroid/content/Context;JLandroid/net/Uri;Ljava/util/List;)I
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Landroid/net/Uri;",
            "Ljava/util/List<",
            "Landroid/content/ContentValues;",
            ">;)I"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p3

    .line 2
    .line 3
    const-string v9, "DocumentProvider"

    .line 4
    .line 5
    sget-object v1, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 6
    .line 7
    invoke-interface {v1}, Lcom/intsig/camscanner/db/wrap/DBHelper;->Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;

    .line 8
    .line 9
    .line 10
    move-result-object v10

    .line 11
    invoke-interface {v10}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->beginTransaction()V

    .line 12
    .line 13
    .line 14
    const/4 v11, 0x0

    .line 15
    :try_start_0
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object v12
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19
    const/4 v13, 0x0

    .line 20
    :goto_0
    :try_start_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_1

    .line 25
    .line 26
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    move-object v14, v1

    .line 31
    check-cast v14, Landroid/content/ContentValues;

    .line 32
    .line 33
    const/4 v1, 0x1

    .line 34
    new-array v15, v1, [Ljava/lang/String;

    .line 35
    .line 36
    new-array v8, v1, [Ljava/lang/String;

    .line 37
    .line 38
    new-array v7, v1, [Landroid/net/Uri;

    .line 39
    .line 40
    move-object/from16 v1, p0

    .line 41
    .line 42
    move-wide/from16 v2, p1

    .line 43
    .line 44
    move-object/from16 v4, p3

    .line 45
    .line 46
    move-object v5, v14

    .line 47
    move-object v6, v15

    .line 48
    move-object/from16 v16, v7

    .line 49
    .line 50
    move-object v7, v8

    .line 51
    move-object/from16 v17, v8

    .line 52
    .line 53
    move-object/from16 v8, v16

    .line 54
    .line 55
    invoke-static/range {v1 .. v8}, Lcom/intsig/camscanner/provider/DocumentProvider;->〇O00(Landroid/content/Context;JLandroid/net/Uri;Landroid/content/ContentValues;[Ljava/lang/String;[Ljava/lang/String;[Landroid/net/Uri;)V

    .line 56
    .line 57
    .line 58
    aget-object v1, v15, v11

    .line 59
    .line 60
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    if-eqz v1, :cond_0

    .line 65
    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    const-string v2, "applyInsert Unknown URL="

    .line 72
    .line 73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-static {v9, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_0
    aget-object v1, v15, v11

    .line 88
    .line 89
    aget-object v2, v17, v11

    .line 90
    .line 91
    invoke-interface {v10, v1, v2, v14}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->〇080(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 92
    .line 93
    .line 94
    add-int/lit8 v13, v13, 0x1

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_1
    invoke-interface {v10}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->setTransactionSuccessful()V

    .line 98
    .line 99
    .line 100
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    if-eqz v0, :cond_2

    .line 105
    .line 106
    const/4 v2, 0x0

    .line 107
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 108
    .line 109
    .line 110
    invoke-static {}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇o00〇〇Oo()Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->insert(Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    .line 116
    .line 117
    :cond_2
    invoke-interface {v10}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->endTransaction()V

    .line 118
    .line 119
    .line 120
    goto :goto_2

    .line 121
    :catch_0
    move-exception v0

    .line 122
    move v11, v13

    .line 123
    goto :goto_1

    .line 124
    :catchall_0
    move-exception v0

    .line 125
    goto :goto_3

    .line 126
    :catch_1
    move-exception v0

    .line 127
    :goto_1
    :try_start_2
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 128
    .line 129
    .line 130
    invoke-interface {v10}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->endTransaction()V

    .line 131
    .line 132
    .line 133
    move v13, v11

    .line 134
    :goto_2
    return v13

    .line 135
    :goto_3
    invoke-interface {v10}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->endTransaction()V

    .line 136
    .line 137
    .line 138
    throw v0
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private OO0o〇〇(Landroid/net/Uri;ZZZZZZZZZZ)V
    .locals 15

    .line 1
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->Oo08()Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v14, L〇8〇〇8o/O8;

    .line 6
    .line 7
    move-object v1, v14

    .line 8
    move-object v2, p0

    .line 9
    move-object/from16 v3, p1

    .line 10
    .line 11
    move/from16 v4, p2

    .line 12
    .line 13
    move/from16 v5, p3

    .line 14
    .line 15
    move/from16 v6, p4

    .line 16
    .line 17
    move/from16 v7, p5

    .line 18
    .line 19
    move/from16 v8, p6

    .line 20
    .line 21
    move/from16 v9, p7

    .line 22
    .line 23
    move/from16 v10, p8

    .line 24
    .line 25
    move/from16 v11, p9

    .line 26
    .line 27
    move/from16 v12, p10

    .line 28
    .line 29
    move/from16 v13, p11

    .line 30
    .line 31
    invoke-direct/range {v1 .. v13}, L〇8〇〇8o/O8;-><init>(Lcom/intsig/camscanner/provider/DocumentProvider;Landroid/net/Uri;ZZZZZZZZZZ)V

    .line 32
    .line 33
    .line 34
    invoke-interface {v0, v14}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
.end method

.method private synthetic OO0o〇〇〇〇0(Landroid/net/Uri;JLandroid/content/ContentValues;)V
    .locals 14

    .line 1
    move-object v0, p1

    .line 2
    move-object/from16 v1, p4

    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇o00〇〇Oo()Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;

    .line 5
    .line 6
    .line 7
    move-result-object v2

    .line 8
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->update(Landroid/net/Uri;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const/4 v3, 0x0

    .line 20
    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 21
    .line 22
    .line 23
    const-wide/16 v4, 0x1

    .line 24
    .line 25
    const-string v6, "DocumentProvider"

    .line 26
    .line 27
    const-wide/16 v7, 0x34

    .line 28
    .line 29
    const-wide/16 v9, 0x27

    .line 30
    .line 31
    const-wide/16 v11, 0x2

    .line 32
    .line 33
    cmp-long v13, p2, v4

    .line 34
    .line 35
    if-eqz v13, :cond_16

    .line 36
    .line 37
    cmp-long v4, p2, v11

    .line 38
    .line 39
    if-eqz v4, :cond_16

    .line 40
    .line 41
    const-wide/16 v4, 0x17

    .line 42
    .line 43
    cmp-long v13, p2, v4

    .line 44
    .line 45
    if-eqz v13, :cond_16

    .line 46
    .line 47
    cmp-long v4, p2, v9

    .line 48
    .line 49
    if-eqz v4, :cond_16

    .line 50
    .line 51
    const-wide/16 v4, 0x33

    .line 52
    .line 53
    cmp-long v13, p2, v4

    .line 54
    .line 55
    if-eqz v13, :cond_16

    .line 56
    .line 57
    cmp-long v4, p2, v7

    .line 58
    .line 59
    if-nez v4, :cond_0

    .line 60
    .line 61
    goto/16 :goto_9

    .line 62
    .line 63
    :cond_0
    const-wide/16 v4, 0x3

    .line 64
    .line 65
    cmp-long v7, p2, v4

    .line 66
    .line 67
    if-eqz v7, :cond_14

    .line 68
    .line 69
    const-wide/16 v4, 0x4

    .line 70
    .line 71
    cmp-long v7, p2, v4

    .line 72
    .line 73
    if-eqz v7, :cond_14

    .line 74
    .line 75
    const-wide/16 v4, 0x18

    .line 76
    .line 77
    cmp-long v7, p2, v4

    .line 78
    .line 79
    if-eqz v7, :cond_14

    .line 80
    .line 81
    const-wide/16 v4, 0x25

    .line 82
    .line 83
    cmp-long v7, p2, v4

    .line 84
    .line 85
    if-eqz v7, :cond_14

    .line 86
    .line 87
    const-wide/16 v4, 0x7

    .line 88
    .line 89
    cmp-long v7, p2, v4

    .line 90
    .line 91
    if-nez v7, :cond_1

    .line 92
    .line 93
    goto/16 :goto_8

    .line 94
    .line 95
    :cond_1
    const-wide/16 v4, 0x16

    .line 96
    .line 97
    cmp-long v7, p2, v4

    .line 98
    .line 99
    if-nez v7, :cond_2

    .line 100
    .line 101
    const-string v0, "account_state"

    .line 102
    .line 103
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 104
    .line 105
    .line 106
    move-result v0

    .line 107
    if-eqz v0, :cond_18

    .line 108
    .line 109
    const-string v0, "Sync account changed"

    .line 110
    .line 111
    invoke-static {v6, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 115
    .line 116
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 117
    .line 118
    .line 119
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 120
    .line 121
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 122
    .line 123
    .line 124
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Image;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 125
    .line 126
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 127
    .line 128
    .line 129
    goto/16 :goto_a

    .line 130
    .line 131
    :cond_2
    const-wide/16 v4, 0x6

    .line 132
    .line 133
    cmp-long v1, p2, v4

    .line 134
    .line 135
    if-eqz v1, :cond_13

    .line 136
    .line 137
    const-wide/16 v4, 0x5

    .line 138
    .line 139
    cmp-long v1, p2, v4

    .line 140
    .line 141
    if-eqz v1, :cond_13

    .line 142
    .line 143
    const-wide/16 v4, 0x19

    .line 144
    .line 145
    cmp-long v1, p2, v4

    .line 146
    .line 147
    if-eqz v1, :cond_13

    .line 148
    .line 149
    const-wide/16 v4, 0x26

    .line 150
    .line 151
    cmp-long v1, p2, v4

    .line 152
    .line 153
    if-nez v1, :cond_3

    .line 154
    .line 155
    goto/16 :goto_7

    .line 156
    .line 157
    :cond_3
    const-wide/16 v4, 0x4c

    .line 158
    .line 159
    cmp-long v1, p2, v4

    .line 160
    .line 161
    if-eqz v1, :cond_12

    .line 162
    .line 163
    const-wide/16 v4, 0x4d

    .line 164
    .line 165
    cmp-long v1, p2, v4

    .line 166
    .line 167
    if-eqz v1, :cond_12

    .line 168
    .line 169
    const-wide/16 v4, 0x50

    .line 170
    .line 171
    cmp-long v1, p2, v4

    .line 172
    .line 173
    if-eqz v1, :cond_12

    .line 174
    .line 175
    const-wide/16 v4, 0x51

    .line 176
    .line 177
    cmp-long v1, p2, v4

    .line 178
    .line 179
    if-nez v1, :cond_4

    .line 180
    .line 181
    goto/16 :goto_6

    .line 182
    .line 183
    :cond_4
    const-wide/16 v4, 0x58

    .line 184
    .line 185
    cmp-long v1, p2, v4

    .line 186
    .line 187
    if-eqz v1, :cond_11

    .line 188
    .line 189
    const-wide/16 v4, 0x59

    .line 190
    .line 191
    cmp-long v1, p2, v4

    .line 192
    .line 193
    if-nez v1, :cond_5

    .line 194
    .line 195
    goto/16 :goto_5

    .line 196
    .line 197
    :cond_5
    const-wide/16 v4, 0x5b

    .line 198
    .line 199
    cmp-long v1, p2, v4

    .line 200
    .line 201
    if-eqz v1, :cond_10

    .line 202
    .line 203
    const-wide/16 v4, 0x5a

    .line 204
    .line 205
    cmp-long v1, p2, v4

    .line 206
    .line 207
    if-eqz v1, :cond_10

    .line 208
    .line 209
    const-wide/16 v4, 0x5c

    .line 210
    .line 211
    cmp-long v1, p2, v4

    .line 212
    .line 213
    if-eqz v1, :cond_10

    .line 214
    .line 215
    const-wide/16 v4, 0x5d

    .line 216
    .line 217
    cmp-long v1, p2, v4

    .line 218
    .line 219
    if-nez v1, :cond_6

    .line 220
    .line 221
    goto/16 :goto_4

    .line 222
    .line 223
    :cond_6
    const-wide/16 v4, 0x5e

    .line 224
    .line 225
    cmp-long v1, p2, v4

    .line 226
    .line 227
    if-eqz v1, :cond_f

    .line 228
    .line 229
    const-wide/16 v4, 0x5f

    .line 230
    .line 231
    cmp-long v1, p2, v4

    .line 232
    .line 233
    if-nez v1, :cond_7

    .line 234
    .line 235
    goto :goto_3

    .line 236
    :cond_7
    const-wide/16 v4, 0x62

    .line 237
    .line 238
    cmp-long v1, p2, v4

    .line 239
    .line 240
    if-eqz v1, :cond_e

    .line 241
    .line 242
    const-wide/16 v4, 0x63

    .line 243
    .line 244
    cmp-long v1, p2, v4

    .line 245
    .line 246
    if-nez v1, :cond_8

    .line 247
    .line 248
    goto :goto_2

    .line 249
    :cond_8
    const-wide/16 v4, 0x60

    .line 250
    .line 251
    cmp-long v1, p2, v4

    .line 252
    .line 253
    if-eqz v1, :cond_d

    .line 254
    .line 255
    const-wide/16 v4, 0x61

    .line 256
    .line 257
    cmp-long v1, p2, v4

    .line 258
    .line 259
    if-nez v1, :cond_9

    .line 260
    .line 261
    goto :goto_1

    .line 262
    :cond_9
    const-wide/16 v4, 0x64

    .line 263
    .line 264
    cmp-long v1, p2, v4

    .line 265
    .line 266
    if-eqz v1, :cond_c

    .line 267
    .line 268
    const-wide/16 v4, 0x65

    .line 269
    .line 270
    cmp-long v1, p2, v4

    .line 271
    .line 272
    if-nez v1, :cond_a

    .line 273
    .line 274
    goto :goto_0

    .line 275
    :cond_a
    const-wide/16 v4, 0x66

    .line 276
    .line 277
    cmp-long v1, p2, v4

    .line 278
    .line 279
    if-eqz v1, :cond_b

    .line 280
    .line 281
    const-wide/16 v4, 0x67

    .line 282
    .line 283
    cmp-long v1, p2, v4

    .line 284
    .line 285
    if-nez v1, :cond_18

    .line 286
    .line 287
    :cond_b
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$BankcardJournalPage;->〇080:Landroid/net/Uri;

    .line 288
    .line 289
    if-eq v0, v1, :cond_18

    .line 290
    .line 291
    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 292
    .line 293
    .line 294
    goto/16 :goto_a

    .line 295
    .line 296
    :cond_c
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$InvitedESignDoc;->〇080:Landroid/net/Uri;

    .line 297
    .line 298
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 299
    .line 300
    .line 301
    goto/16 :goto_a

    .line 302
    .line 303
    :cond_d
    :goto_1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$QrCodeHistory;->〇080:Landroid/net/Uri;

    .line 304
    .line 305
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 306
    .line 307
    .line 308
    goto/16 :goto_a

    .line 309
    .line 310
    :cond_e
    :goto_2
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SignatureContact;->〇080:Landroid/net/Uri;

    .line 311
    .line 312
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 313
    .line 314
    .line 315
    goto/16 :goto_a

    .line 316
    .line 317
    :cond_f
    :goto_3
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$GalleryRadar;->〇080:Landroid/net/Uri;

    .line 318
    .line 319
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 320
    .line 321
    .line 322
    goto/16 :goto_a

    .line 323
    .line 324
    :cond_10
    :goto_4
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Dir;->〇080:Landroid/net/Uri;

    .line 325
    .line 326
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 327
    .line 328
    .line 329
    goto/16 :goto_a

    .line 330
    .line 331
    :cond_11
    :goto_5
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$MessageCenter;->〇080:Landroid/net/Uri;

    .line 332
    .line 333
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 334
    .line 335
    .line 336
    goto :goto_a

    .line 337
    :cond_12
    :goto_6
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->O8:Landroid/net/Uri;

    .line 338
    .line 339
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 340
    .line 341
    .line 342
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Dir;->〇080:Landroid/net/Uri;

    .line 343
    .line 344
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 345
    .line 346
    .line 347
    goto :goto_a

    .line 348
    :cond_13
    :goto_7
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇〇888:Landroid/net/Uri;

    .line 349
    .line 350
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 351
    .line 352
    .line 353
    goto :goto_a

    .line 354
    :cond_14
    :goto_8
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Image;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 355
    .line 356
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 357
    .line 358
    .line 359
    const-string v0, "note"

    .line 360
    .line 361
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 362
    .line 363
    .line 364
    move-result v0

    .line 365
    if-nez v0, :cond_15

    .line 366
    .line 367
    const-string v0, "image_titile"

    .line 368
    .line 369
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 370
    .line 371
    .line 372
    move-result v0

    .line 373
    if-nez v0, :cond_15

    .line 374
    .line 375
    const-string v0, "ocr_result"

    .line 376
    .line 377
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 378
    .line 379
    .line 380
    move-result v0

    .line 381
    if-nez v0, :cond_15

    .line 382
    .line 383
    const-string v0, "ocr_result_user"

    .line 384
    .line 385
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 386
    .line 387
    .line 388
    move-result v0

    .line 389
    if-eqz v0, :cond_18

    .line 390
    .line 391
    :cond_15
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 392
    .line 393
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 394
    .line 395
    .line 396
    goto :goto_a

    .line 397
    :cond_16
    :goto_9
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Document;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 398
    .line 399
    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 400
    .line 401
    .line 402
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Document;->〇〇888:Landroid/net/Uri;

    .line 403
    .line 404
    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 405
    .line 406
    .line 407
    cmp-long v1, p2, v11

    .line 408
    .line 409
    if-eqz v1, :cond_17

    .line 410
    .line 411
    cmp-long v1, p2, v9

    .line 412
    .line 413
    if-eqz v1, :cond_17

    .line 414
    .line 415
    cmp-long v1, p2, v7

    .line 416
    .line 417
    if-nez v1, :cond_18

    .line 418
    .line 419
    :cond_17
    :try_start_0
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 420
    .line 421
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 422
    .line 423
    .line 424
    move-result-wide v4

    .line 425
    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 426
    .line 427
    .line 428
    move-result-object v1

    .line 429
    invoke-static {p1, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 430
    .line 431
    .line 432
    move-result v0

    .line 433
    if-nez v0, :cond_18

    .line 434
    .line 435
    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    .line 437
    .line 438
    goto :goto_a

    .line 439
    :catch_0
    move-exception v0

    .line 440
    invoke-static {v6, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 441
    .line 442
    .line 443
    :cond_18
    :goto_a
    return-void
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/db/wrap/DBHelper;->Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private Oooo8o0〇(Landroid/net/Uri;JLandroid/content/ContentValues;)V
    .locals 8

    .line 1
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->Oo08()Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v7, L〇8〇〇8o/〇o〇;

    .line 6
    .line 7
    move-object v1, v7

    .line 8
    move-object v2, p0

    .line 9
    move-object v3, p1

    .line 10
    move-wide v4, p2

    .line 11
    move-object v6, p4

    .line 12
    invoke-direct/range {v1 .. v6}, L〇8〇〇8o/〇o〇;-><init>(Lcom/intsig/camscanner/provider/DocumentProvider;Landroid/net/Uri;JLandroid/content/ContentValues;)V

    .line 13
    .line 14
    .line 15
    invoke-interface {v0, v7}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic oO80(Landroid/net/Uri;)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇o00〇〇Oo()Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->delete(Landroid/net/Uri;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const/4 v1, 0x0

    .line 17
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 18
    .line 19
    .line 20
    sget-object v2, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 21
    .line 22
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    const/4 v3, 0x1

    .line 27
    if-eq v2, v3, :cond_4

    .line 28
    .line 29
    const/4 v3, 0x2

    .line 30
    if-eq v2, v3, :cond_4

    .line 31
    .line 32
    const/4 v3, 0x3

    .line 33
    if-eq v2, v3, :cond_3

    .line 34
    .line 35
    const/4 v3, 0x4

    .line 36
    if-eq v2, v3, :cond_3

    .line 37
    .line 38
    const/16 v3, 0xd

    .line 39
    .line 40
    if-eq v2, v3, :cond_2

    .line 41
    .line 42
    const/16 v3, 0xe

    .line 43
    .line 44
    if-eq v2, v3, :cond_2

    .line 45
    .line 46
    const/16 v3, 0x17

    .line 47
    .line 48
    if-eq v2, v3, :cond_4

    .line 49
    .line 50
    const/16 v3, 0x18

    .line 51
    .line 52
    if-eq v2, v3, :cond_3

    .line 53
    .line 54
    const/16 v3, 0x20

    .line 55
    .line 56
    if-eq v2, v3, :cond_1

    .line 57
    .line 58
    const/16 v3, 0x21

    .line 59
    .line 60
    if-eq v2, v3, :cond_1

    .line 61
    .line 62
    const/16 v3, 0x25

    .line 63
    .line 64
    if-eq v2, v3, :cond_3

    .line 65
    .line 66
    const/16 v3, 0x27

    .line 67
    .line 68
    if-eq v2, v3, :cond_4

    .line 69
    .line 70
    const/16 v3, 0x4a

    .line 71
    .line 72
    if-eq v2, v3, :cond_0

    .line 73
    .line 74
    packed-switch v2, :pswitch_data_0

    .line 75
    .line 76
    .line 77
    packed-switch v2, :pswitch_data_1

    .line 78
    .line 79
    .line 80
    packed-switch v2, :pswitch_data_2

    .line 81
    .line 82
    .line 83
    goto/16 :goto_0

    .line 84
    .line 85
    :pswitch_0
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$BankcardJournalPage;->〇080:Landroid/net/Uri;

    .line 86
    .line 87
    if-eq p1, v2, :cond_5

    .line 88
    .line 89
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 90
    .line 91
    .line 92
    goto/16 :goto_0

    .line 93
    .line 94
    :pswitch_1
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$InvitedESignDoc;->〇080:Landroid/net/Uri;

    .line 95
    .line 96
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 97
    .line 98
    .line 99
    goto :goto_0

    .line 100
    :pswitch_2
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$SignatureContact;->〇080:Landroid/net/Uri;

    .line 101
    .line 102
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 103
    .line 104
    .line 105
    goto :goto_0

    .line 106
    :pswitch_3
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$QrCodeHistory;->〇080:Landroid/net/Uri;

    .line 107
    .line 108
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :pswitch_4
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$GalleryRadar;->〇080:Landroid/net/Uri;

    .line 113
    .line 114
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :pswitch_5
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Dir;->〇o〇:Landroid/net/Uri;

    .line 119
    .line 120
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 121
    .line 122
    .line 123
    goto :goto_0

    .line 124
    :pswitch_6
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$MessageCenter;->〇080:Landroid/net/Uri;

    .line 125
    .line 126
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 127
    .line 128
    .line 129
    goto :goto_0

    .line 130
    :pswitch_7
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Dir;->〇o〇:Landroid/net/Uri;

    .line 131
    .line 132
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 133
    .line 134
    .line 135
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$TeamInfo;->〇080:Landroid/net/Uri;

    .line 136
    .line 137
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_0
    :pswitch_8
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$SystemMessage;->〇080:Landroid/net/Uri;

    .line 142
    .line 143
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 144
    .line 145
    .line 146
    goto :goto_0

    .line 147
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$PdfSize;->〇080:Landroid/net/Uri;

    .line 148
    .line 149
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 150
    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Document;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 154
    .line 155
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 156
    .line 157
    .line 158
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Document;->〇〇888:Landroid/net/Uri;

    .line 159
    .line 160
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 161
    .line 162
    .line 163
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 164
    .line 165
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 166
    .line 167
    .line 168
    goto :goto_0

    .line 169
    :cond_3
    :pswitch_9
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Image;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 170
    .line 171
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 172
    .line 173
    .line 174
    goto :goto_0

    .line 175
    :cond_4
    :pswitch_a
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 176
    .line 177
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 178
    .line 179
    .line 180
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Document;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 181
    .line 182
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 183
    .line 184
    .line 185
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Document;->〇〇888:Landroid/net/Uri;

    .line 186
    .line 187
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 188
    .line 189
    .line 190
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 191
    .line 192
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 193
    .line 194
    .line 195
    sget-object p1, Lcom/intsig/camscanner/provider/Documents$TeamInfo;->〇080:Landroid/net/Uri;

    .line 196
    .line 197
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 198
    .line 199
    .line 200
    :cond_5
    :goto_0
    return-void

    .line 201
    :pswitch_data_0
    .packed-switch 0x2f
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
    .end packed-switch

    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    :pswitch_data_1
    .packed-switch 0x42
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    :pswitch_data_2
    .packed-switch 0x58
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private o〇0(Landroid/net/Uri;ILjava/lang/String;Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;)V
    .locals 25

    move/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    .line 1
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/intsig/camscanner/sync/SyncAccountUtil;->〇080(Landroid/content/Context;)J

    move-result-wide v3

    const-string v6, "tags"

    const-string v7, "images"

    const-string v8, "documents"

    const-string v9, "sync_dir_id"

    const-string v10, "team_token IS NULL"

    const-string v11, " IS NULL"

    const-string v12, " = "

    const-string v13, "teamfileinfos"

    const-string v14, "teammembers"

    const-string v5, "_id"

    const-string v15, "belong_state"

    move-object/from16 v16, v10

    const-string v10, "sync_state"

    move-object/from16 v17, v8

    const-string v8, "team_token"

    move-object/from16 v18, v7

    const-string v7, "_id="

    move-object/from16 v19, v6

    const-string v6, " != "

    move-object/from16 v20, v15

    const-string v15, "_id = "

    move-object/from16 v21, v6

    const-string v6, " AND "

    move-object/from16 v22, v10

    const-string v10, "sync_account_id = "

    move-object/from16 v23, v11

    const-string v11, "sync_account_id"

    const/16 v24, 0x0

    packed-switch v0, :pswitch_data_0

    .line 2
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTableAndWhere Unknown URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const-string v0, "bank_card_journal_page"

    .line 3
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_2
    const-string v0, "bank_card_journal_page"

    .line 5
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_3
    const-string v0, "invited_esign_doc"

    .line 8
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_4
    const-string v0, "invited_esign_doc"

    .line 10
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 11
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 12
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_5
    const-string v0, "signature_contact"

    .line 13
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_6
    const-string v0, "signature_contact"

    .line 15
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 16
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 17
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_7
    const-string v0, "qr_code"

    .line 18
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_8
    const-string v0, "qr_code"

    .line 20
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_9
    const-string v0, "gallery_pictures"

    .line 21
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_a
    const-string v0, "gallery_pictures"

    .line 23
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_b
    const-string v0, "invitesharedir"

    .line 24
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_c
    const-string v5, "invitesharedir"

    .line 26
    iput-object v5, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    const/16 v5, 0x5c

    if-eq v0, v5, :cond_49

    if-eqz v1, :cond_3

    .line 27
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 28
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_d
    const-string v0, "message_center"

    .line 29
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_e
    const-string v0, "message_center"

    .line 31
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_f
    const-string v0, "SyncDeleteStatus"

    .line 32
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_10
    const-string v0, "SyncDeleteStatus"

    .line 34
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 35
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 36
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    .line 37
    :pswitch_11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "image_id = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v0, "signature"

    .line 38
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_13

    :pswitch_12
    const-string v0, "signature"

    .line 39
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    .line 40
    :pswitch_13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "teamfileinfos inner join  teammembers on teamfileinfos"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " and "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "user_id"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "user_id"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " and "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 41
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 42
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    .line 43
    :pswitch_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 44
    iput-object v13, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_13

    .line 45
    :pswitch_15
    iput-object v13, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 46
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 47
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    .line 48
    :pswitch_16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 49
    iput-object v14, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_13

    .line 50
    :pswitch_17
    iput-object v14, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 51
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 52
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    .line 53
    :pswitch_18
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v0, "teaminfos"

    .line 54
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_13

    :pswitch_19
    const-string v0, "teaminfos"

    .line 55
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 56
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 57
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_1a
    const-string v0, "messagecenters"

    .line 58
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_1b
    const-string v0, "messagecenters"

    .line 59
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_1c
    if-eqz v1, :cond_a

    .line 61
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    goto :goto_0

    :cond_9
    move-object/from16 v10, v24

    goto :goto_1

    .line 62
    :cond_a
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v10, v0

    :goto_1
    const-string v0, "messagecenters"

    .line 63
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_13

    :pswitch_1d
    const-string v0, "dirs"

    .line 64
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 65
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 66
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    :cond_c
    move-object/from16 v0, v24

    if-eqz v1, :cond_d

    .line 67
    invoke-virtual {v1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 68
    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 69
    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    const-string v3, "parent_sync_id"

    .line 70
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    goto/16 :goto_9

    :cond_d
    if-nez v0, :cond_e

    goto/16 :goto_8

    .line 71
    :cond_e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v7, v23

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_1e
    const-string v0, "dirs"

    .line 72
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_1f
    move-object/from16 v7, v23

    const-string v12, "dirs"

    .line 74
    iput-object v12, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    const/16 v12, 0x46

    if-ne v0, v12, :cond_10

    if-eqz v1, :cond_f

    .line 75
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    :cond_f
    const-string v10, "sync_state != 2 AND sync_state != -1 AND sync_state != 5"

    goto/16 :goto_13

    :cond_10
    if-eqz v1, :cond_12

    .line 76
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    goto :goto_2

    :cond_11
    move-object/from16 v14, v21

    goto :goto_3

    .line 77
    :cond_12
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v13, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v14, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    :goto_3
    move-object/from16 v0, v24

    move-object/from16 v12, v20

    if-eqz v1, :cond_13

    .line 78
    invoke-virtual {v1, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_15

    :cond_13
    if-nez v0, :cond_14

    const-string v0, "belong_state != 1"

    goto :goto_4

    .line 79
    :cond_14
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_15
    :goto_4
    if-eqz v1, :cond_16

    .line 80
    invoke-virtual {v1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 81
    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 82
    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    const-string v3, "parent_sync_id"

    .line 83
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_16

    goto/16 :goto_9

    :cond_16
    if-nez v0, :cond_17

    goto/16 :goto_8

    .line 84
    :cond_17
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_20
    move-object/from16 v0, v19

    .line 85
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 86
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    :cond_18
    const-string v10, "sync_state != 2"

    goto/16 :goto_13

    :pswitch_21
    move-object/from16 v14, v21

    move-object/from16 v13, v22

    const-string v0, "tags left join mtags on tags._id = tag_id"

    .line 87
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 88
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 89
    :cond_19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_22
    const-string v0, "sharedapps"

    .line 90
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_23
    const-string v0, "sharedapps"

    .line 92
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_24
    move-object/from16 v12, v20

    move-object/from16 v14, v21

    move-object/from16 v13, v22

    move-object/from16 v7, v23

    const-string v0, "documents LEFT JOIN (select mtags.document_id as cc, group_concat(tags.title,\' | \') as dd from mtags, tags where mtags.tag_id = tags._id group by mtags.document_id) on _id = cc"

    .line 93
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_1a

    .line 94
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 95
    :cond_1a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    :cond_1b
    move-object/from16 v0, v24

    if-eqz v1, :cond_1c

    .line 96
    invoke-virtual {v1, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    :cond_1c
    if-nez v0, :cond_1d

    const-string v0, "belong_state != 1"

    goto :goto_5

    .line 97
    :cond_1d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1e
    :goto_5
    if-eqz v1, :cond_1f

    .line 98
    invoke-virtual {v1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    const-string v3, "sync_doc_id"

    .line 99
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 100
    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 101
    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1f

    goto/16 :goto_9

    :cond_1f
    if-nez v0, :cond_20

    goto/16 :goto_8

    .line 102
    :cond_20
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_25
    const-string v0, "faxinfo"

    .line 103
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_26
    const-string v0, "faxinfo"

    .line 105
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_21

    .line 106
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 107
    :cond_21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_27
    const-string v0, "documentlike"

    .line 108
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_28
    const-string v0, "documentlike"

    .line 110
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_22

    .line 111
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 112
    :cond_22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_29
    const-string v0, "collaboratemsgs"

    .line 113
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_2a
    const-string v0, "collaboratemsgs"

    .line 115
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_23

    .line 116
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 117
    :cond_23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_2b
    const-string v0, "comments"

    .line 118
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_2c
    const-string v0, "comments"

    .line 120
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_24

    .line 121
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 122
    :cond_24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_2d
    const-string v0, "collaborators"

    .line 123
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_2e
    const-string v0, "collaborators"

    .line 125
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_25

    .line 126
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 127
    :cond_25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_2f
    const-string v0, "page_mark"

    .line 128
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "page_id = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_30
    const-string v0, "page_mark"

    .line 130
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_31
    const-string v0, "page_mark"

    .line 132
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_32
    const-string v0, "sync_restore"

    .line 133
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_33
    const-string v0, "sync_restore"

    .line 135
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_26

    .line 136
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 137
    :cond_26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_34
    const-string v0, "pdfsize"

    .line 138
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_35
    const-string v0, "pdfsize"

    .line 140
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_36
    const-string v0, "notepath"

    .line 141
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "graphics_id = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_37
    const-string v0, "graphics"

    .line 143
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "image_id = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_38
    const-string v0, "notepath"

    .line 145
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_39
    const-string v0, "notepath"

    .line 147
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_3a
    const-string v0, "graphics"

    .line 148
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_3b
    const-string v0, "graphics"

    .line 150
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_3c
    move-object/from16 v0, v19

    .line 151
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_27

    .line 152
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 153
    :cond_27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_3d
    if-eqz v1, :cond_29

    .line 154
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_28

    goto :goto_6

    :cond_28
    move-object/from16 v8, v18

    move-object/from16 v10, v24

    goto :goto_7

    .line 155
    :cond_29
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v10, v0

    move-object/from16 v8, v18

    .line 156
    :goto_7
    iput-object v8, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_13

    :pswitch_3e
    move-object/from16 v15, v17

    move-object/from16 v7, v23

    .line 157
    iput-object v15, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_2a

    .line 158
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 159
    :cond_2a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    :cond_2b
    move-object/from16 v0, v24

    if-eqz v1, :cond_2c

    .line 160
    invoke-virtual {v1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    const-string v3, "sync_doc_id"

    .line 161
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 162
    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 163
    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2c

    goto/16 :goto_9

    :cond_2c
    if-nez v0, :cond_2d

    :goto_8
    move-object/from16 v10, v16

    goto/16 :goto_13

    .line 164
    :cond_2d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_3f
    const-string v0, "sync_accounts"

    .line 165
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_40
    const-string v0, "sync_accounts"

    .line 167
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_41
    const-string v0, "printtask"

    .line 168
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_42
    const-string v0, "printtask"

    .line 170
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_2e

    .line 171
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 172
    :cond_2e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_43
    const-string v0, "faxtask"

    .line 173
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_44
    const-string v0, "faxtask"

    .line 175
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_2f

    .line 176
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 177
    :cond_2f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_45
    const-string v0, "mtags"

    .line 178
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_46
    const-string v0, "mtags"

    .line 180
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_11

    :pswitch_47
    const-string v0, "uploadstate"

    .line 181
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v5, "faxtask"

    .line 183
    iput-object v5, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_31

    .line 184
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_30

    goto :goto_a

    :cond_30
    :goto_9
    move-object v10, v0

    goto/16 :goto_13

    .line 185
    :cond_31
    :goto_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_48
    const-string v0, "uploadstate"

    .line 186
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_32

    .line 187
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 188
    :cond_32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_49
    const-string v0, "accounts"

    .line 189
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_4a
    const-string v0, "accounts"

    .line 191
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_33

    .line 192
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 193
    :cond_33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_4b
    move-object/from16 v15, v17

    move-object/from16 v14, v21

    move-object/from16 v13, v22

    .line 194
    iput-object v15, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_35

    .line 195
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_34

    goto :goto_b

    .line 196
    :cond_34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tag_id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    .line 197
    :cond_35
    :goto_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tag_id="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_4c
    move-object/from16 v8, v18

    move-object/from16 v14, v21

    .line 198
    iput-object v8, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_37

    const-string v0, "sync_jpage_state"

    .line 199
    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_36

    goto :goto_c

    .line 200
    :cond_36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "document_id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    .line 201
    :cond_37
    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "document_id="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "sync_jpage_state"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "sync_jpage_state"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_4d
    move-object/from16 v0, v19

    .line 202
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_4e
    move-object/from16 v0, v19

    move-object/from16 v14, v21

    move-object/from16 v13, v22

    .line 204
    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    if-eqz v1, :cond_38

    .line 205
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 206
    :cond_38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_4f
    move-object/from16 v8, v18

    .line 207
    iput-object v8, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_50
    move-object/from16 v8, v18

    move-object/from16 v12, v20

    move-object/from16 v14, v21

    move-object/from16 v13, v22

    const/16 v7, 0x31

    if-eq v0, v7, :cond_3d

    if-eqz v1, :cond_39

    .line 209
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3a

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 210
    :cond_39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    :cond_3a
    move-object/from16 v0, v24

    if-eqz v1, :cond_3b

    .line 211
    invoke-virtual {v1, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_40

    :cond_3b
    if-nez v0, :cond_3c

    const-string v0, "belong_state != 1"

    goto :goto_e

    .line 212
    :cond_3c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    :cond_3d
    if-eqz v1, :cond_3f

    .line 213
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3e

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3e

    goto :goto_d

    :cond_3e
    move-object/from16 v10, v24

    goto :goto_f

    :cond_3f
    :goto_d
    const-string v0, "sync_state != 2 AND sync_state != 5"

    :cond_40
    :goto_e
    move-object v10, v0

    .line 214
    :goto_f
    iput-object v8, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    goto/16 :goto_13

    :pswitch_51
    move-object/from16 v15, v17

    .line 215
    iput-object v15, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_13

    :pswitch_52
    move-object/from16 v15, v17

    move-object/from16 v12, v20

    move-object/from16 v14, v21

    move-object/from16 v13, v22

    move-object/from16 v7, v23

    .line 217
    iput-object v15, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    const/16 v15, 0x35

    if-eq v0, v15, :cond_48

    if-eqz v1, :cond_41

    .line 218
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_42

    .line 219
    :cond_41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    :cond_42
    move-object/from16 v0, v24

    if-eqz v1, :cond_43

    .line 220
    invoke-virtual {v1, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_45

    :cond_43
    if-nez v0, :cond_44

    const-string v0, "belong_state != 1"

    goto :goto_10

    .line 221
    :cond_44
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_45
    :goto_10
    if-eqz v1, :cond_46

    .line 222
    invoke-virtual {v1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    const-string v3, "sync_doc_id"

    .line 223
    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 224
    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 225
    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_46

    goto/16 :goto_9

    :cond_46
    if-nez v0, :cond_47

    goto/16 :goto_8

    .line 226
    :cond_47
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_13

    :cond_48
    if-eqz v1, :cond_4a

    .line 227
    invoke-virtual {v1, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    goto :goto_12

    :cond_49
    :goto_11
    move-object/from16 v10, v24

    goto :goto_13

    :cond_4a
    :goto_12
    const-string v10, "sync_state != 2 AND sync_state != -1 AND sync_state != 5"

    .line 228
    :goto_13
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4c

    .line 229
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4b

    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " AND ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇o00〇〇Oo:Ljava/lang/String;

    goto :goto_14

    .line 231
    :cond_4b
    iput-object v1, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇o00〇〇Oo:Ljava/lang/String;

    goto :goto_14

    .line 232
    :cond_4c
    iput-object v10, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇o00〇〇Oo:Ljava/lang/String;

    :goto_14
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_52
        :pswitch_51
        :pswitch_50
        :pswitch_4f
        :pswitch_4e
        :pswitch_4d
        :pswitch_4c
        :pswitch_4b
        :pswitch_4a
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_52
        :pswitch_0
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_50
        :pswitch_4f
        :pswitch_4d
        :pswitch_51
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_50
        :pswitch_4f
        :pswitch_50
        :pswitch_4f
        :pswitch_52
        :pswitch_51
        :pswitch_52
        :pswitch_51
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_1e
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_1b
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/provider/DocumentProvider;Landroid/net/Uri;ZZZZZZZZZZ)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p11}, Lcom/intsig/camscanner/provider/DocumentProvider;->〇80〇808〇O(Landroid/net/Uri;ZZZZZZZZZZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
.end method

.method private synthetic 〇80〇808〇O(Landroid/net/Uri;ZZZZZZZZZZ)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇o00〇〇Oo()Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->insert(Landroid/net/Uri;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    if-eqz p2, :cond_1

    .line 23
    .line 24
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$TeamInfo;->〇080:Landroid/net/Uri;

    .line 25
    .line 26
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 27
    .line 28
    .line 29
    :cond_1
    if-eqz p3, :cond_2

    .line 30
    .line 31
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$Dir;->〇080:Landroid/net/Uri;

    .line 32
    .line 33
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 34
    .line 35
    .line 36
    :cond_2
    if-eqz p4, :cond_3

    .line 37
    .line 38
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$Document;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 39
    .line 40
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 41
    .line 42
    .line 43
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$Document;->〇〇888:Landroid/net/Uri;

    .line 44
    .line 45
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 46
    .line 47
    .line 48
    :cond_3
    if-eqz p5, :cond_4

    .line 49
    .line 50
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$Image;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 51
    .line 52
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 53
    .line 54
    .line 55
    :cond_4
    if-eqz p6, :cond_5

    .line 56
    .line 57
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$SystemMessage;->〇080:Landroid/net/Uri;

    .line 58
    .line 59
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 60
    .line 61
    .line 62
    :cond_5
    if-eqz p7, :cond_6

    .line 63
    .line 64
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$MessageCenter;->〇080:Landroid/net/Uri;

    .line 65
    .line 66
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 67
    .line 68
    .line 69
    :cond_6
    if-eqz p8, :cond_7

    .line 70
    .line 71
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$GalleryRadar;->〇080:Landroid/net/Uri;

    .line 72
    .line 73
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 74
    .line 75
    .line 76
    :cond_7
    if-eqz p9, :cond_8

    .line 77
    .line 78
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$SignatureContact;->〇080:Landroid/net/Uri;

    .line 79
    .line 80
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 81
    .line 82
    .line 83
    :cond_8
    if-eqz p10, :cond_9

    .line 84
    .line 85
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$InvitedESignDoc;->〇080:Landroid/net/Uri;

    .line 86
    .line 87
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 88
    .line 89
    .line 90
    :cond_9
    if-eqz p11, :cond_a

    .line 91
    .line 92
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$BankcardJournalPage;->〇080:Landroid/net/Uri;

    .line 93
    .line 94
    if-eq p1, p2, :cond_a

    .line 95
    .line 96
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 97
    .line 98
    .line 99
    :cond_a
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
.end method

.method private 〇8o8o〇(Landroid/content/ContentValues;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Ljava/lang/String;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/provider/DocumentProvider;->o0:Ljava/util/List;

    .line 22
    .line 23
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-nez v1, :cond_0

    .line 28
    .line 29
    new-instance p1, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v1, "modified_date update key = "

    .line 35
    .line 36
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    const-string v0, "DocumentProvider"

    .line 47
    .line 48
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const/4 p1, 0x1

    .line 52
    return p1

    .line 53
    :cond_1
    const/4 p1, 0x0

    .line 54
    return p1
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static 〇O00(Landroid/content/Context;JLandroid/net/Uri;Landroid/content/ContentValues;[Ljava/lang/String;[Ljava/lang/String;[Landroid/net/Uri;)V
    .locals 15

    .line 1
    move-object/from16 v0, p4

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 4
    .line 5
    move-object/from16 v2, p3

    .line 6
    .line 7
    invoke-virtual {v1, v2}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const-string v2, "messagecenters"

    .line 12
    .line 13
    const-string v3, "last_modified"

    .line 14
    .line 15
    const-string v4, "created_time"

    .line 16
    .line 17
    const-string v5, "folder_type"

    .line 18
    .line 19
    const-string v6, "sync_state"

    .line 20
    .line 21
    const-string v7, "title_sort_index"

    .line 22
    .line 23
    const/4 v8, 0x1

    .line 24
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 25
    .line 26
    .line 27
    move-result-object v8

    .line 28
    const-string v9, "title"

    .line 29
    .line 30
    const-string v10, "sync_account_id"

    .line 31
    .line 32
    const/4 v11, 0x0

    .line 33
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object v12

    .line 37
    sparse-switch v1, :sswitch_data_0

    .line 38
    .line 39
    .line 40
    goto/16 :goto_0

    .line 41
    .line 42
    :sswitch_0
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    if-nez v1, :cond_0

    .line 47
    .line 48
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 53
    .line 54
    .line 55
    :cond_0
    const-string v0, "bank_card_journal_page"

    .line 56
    .line 57
    aput-object v0, p5, v11

    .line 58
    .line 59
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$BankcardJournalPage;->〇080:Landroid/net/Uri;

    .line 60
    .line 61
    aput-object v0, p7, v11

    .line 62
    .line 63
    goto/16 :goto_0

    .line 64
    .line 65
    :sswitch_1
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    if-nez v1, :cond_1

    .line 70
    .line 71
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 76
    .line 77
    .line 78
    :cond_1
    const-string v0, "invited_esign_doc"

    .line 79
    .line 80
    aput-object v0, p5, v11

    .line 81
    .line 82
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$InvitedESignDoc;->〇080:Landroid/net/Uri;

    .line 83
    .line 84
    aput-object v0, p7, v11

    .line 85
    .line 86
    goto/16 :goto_0

    .line 87
    .line 88
    :sswitch_2
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    if-nez v1, :cond_2

    .line 93
    .line 94
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 99
    .line 100
    .line 101
    :cond_2
    const-string v0, "signature_contact"

    .line 102
    .line 103
    aput-object v0, p5, v11

    .line 104
    .line 105
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SignatureContact;->〇080:Landroid/net/Uri;

    .line 106
    .line 107
    aput-object v0, p7, v11

    .line 108
    .line 109
    goto/16 :goto_0

    .line 110
    .line 111
    :sswitch_3
    const-string v0, "qr_code"

    .line 112
    .line 113
    aput-object v0, p5, v11

    .line 114
    .line 115
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$QrCodeHistory;->〇080:Landroid/net/Uri;

    .line 116
    .line 117
    aput-object v0, p7, v11

    .line 118
    .line 119
    goto/16 :goto_0

    .line 120
    .line 121
    :sswitch_4
    const-string v0, "gallery_pictures"

    .line 122
    .line 123
    aput-object v0, p5, v11

    .line 124
    .line 125
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$GalleryRadar;->〇080:Landroid/net/Uri;

    .line 126
    .line 127
    aput-object v0, p7, v11

    .line 128
    .line 129
    goto/16 :goto_0

    .line 130
    .line 131
    :sswitch_5
    const/16 v2, 0x5c

    .line 132
    .line 133
    if-eq v1, v2, :cond_3

    .line 134
    .line 135
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    if-nez v1, :cond_3

    .line 140
    .line 141
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 146
    .line 147
    .line 148
    :cond_3
    const-string v0, "invitesharedir"

    .line 149
    .line 150
    aput-object v0, p5, v11

    .line 151
    .line 152
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$InviteShareDirEntry;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 153
    .line 154
    aput-object v0, p7, v11

    .line 155
    .line 156
    const-string v0, "share_id"

    .line 157
    .line 158
    aput-object v0, p6, v11

    .line 159
    .line 160
    goto/16 :goto_0

    .line 161
    .line 162
    :sswitch_6
    const-string v0, "message_center"

    .line 163
    .line 164
    aput-object v0, p5, v11

    .line 165
    .line 166
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$MessageCenter;->〇080:Landroid/net/Uri;

    .line 167
    .line 168
    aput-object v0, p7, v11

    .line 169
    .line 170
    goto/16 :goto_0

    .line 171
    .line 172
    :sswitch_7
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 173
    .line 174
    .line 175
    move-result v1

    .line 176
    if-nez v1, :cond_4

    .line 177
    .line 178
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 179
    .line 180
    .line 181
    move-result-object v1

    .line 182
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 183
    .line 184
    .line 185
    :cond_4
    const-string v0, "SyncDeleteStatus"

    .line 186
    .line 187
    aput-object v0, p5, v11

    .line 188
    .line 189
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SyncDeleteStatus;->〇080:Landroid/net/Uri;

    .line 190
    .line 191
    aput-object v0, p7, v11

    .line 192
    .line 193
    goto/16 :goto_0

    .line 194
    .line 195
    :sswitch_8
    const-string v0, "signature"

    .line 196
    .line 197
    aput-object v0, p5, v11

    .line 198
    .line 199
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Signature;->〇080:Landroid/net/Uri;

    .line 200
    .line 201
    aput-object v0, p7, v11

    .line 202
    .line 203
    goto/16 :goto_0

    .line 204
    .line 205
    :sswitch_9
    const-string v1, "teamfileinfos"

    .line 206
    .line 207
    aput-object v1, p5, v11

    .line 208
    .line 209
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$TeamFileInfo;->〇080:Landroid/net/Uri;

    .line 210
    .line 211
    aput-object v1, p7, v11

    .line 212
    .line 213
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 214
    .line 215
    .line 216
    move-result v1

    .line 217
    if-nez v1, :cond_2c

    .line 218
    .line 219
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 220
    .line 221
    .line 222
    move-result-object v1

    .line 223
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 224
    .line 225
    .line 226
    goto/16 :goto_0

    .line 227
    .line 228
    :sswitch_a
    const-string v1, "teammembers"

    .line 229
    .line 230
    aput-object v1, p5, v11

    .line 231
    .line 232
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$TeamMember;->〇080:Landroid/net/Uri;

    .line 233
    .line 234
    aput-object v1, p7, v11

    .line 235
    .line 236
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 237
    .line 238
    .line 239
    move-result v1

    .line 240
    if-nez v1, :cond_2c

    .line 241
    .line 242
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 243
    .line 244
    .line 245
    move-result-object v1

    .line 246
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 247
    .line 248
    .line 249
    goto/16 :goto_0

    .line 250
    .line 251
    :sswitch_b
    const-string v1, "teaminfos"

    .line 252
    .line 253
    aput-object v1, p5, v11

    .line 254
    .line 255
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$TeamInfo;->〇080:Landroid/net/Uri;

    .line 256
    .line 257
    aput-object v1, p7, v11

    .line 258
    .line 259
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 260
    .line 261
    .line 262
    move-result v1

    .line 263
    if-nez v1, :cond_5

    .line 264
    .line 265
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 266
    .line 267
    .line 268
    move-result-object v1

    .line 269
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 270
    .line 271
    .line 272
    :cond_5
    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 273
    .line 274
    .line 275
    move-result v1

    .line 276
    if-nez v1, :cond_2c

    .line 277
    .line 278
    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    .line 279
    .line 280
    .line 281
    move-result-object v1

    .line 282
    invoke-static {v1}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 283
    .line 284
    .line 285
    move-result-object v1

    .line 286
    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    .line 288
    .line 289
    goto/16 :goto_0

    .line 290
    .line 291
    :sswitch_c
    aput-object v2, p5, v11

    .line 292
    .line 293
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SystemMessage;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 294
    .line 295
    aput-object v0, p7, v11

    .line 296
    .line 297
    goto/16 :goto_0

    .line 298
    .line 299
    :sswitch_d
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 300
    .line 301
    .line 302
    move-result v1

    .line 303
    if-nez v1, :cond_6

    .line 304
    .line 305
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 306
    .line 307
    .line 308
    move-result-object v1

    .line 309
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 310
    .line 311
    .line 312
    :cond_6
    const-string v1, "create_time"

    .line 313
    .line 314
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 315
    .line 316
    .line 317
    move-result v3

    .line 318
    if-nez v3, :cond_7

    .line 319
    .line 320
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 321
    .line 322
    .line 323
    move-result-wide v3

    .line 324
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 325
    .line 326
    .line 327
    move-result-object v3

    .line 328
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 329
    .line 330
    .line 331
    :cond_7
    aput-object v2, p5, v11

    .line 332
    .line 333
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SystemMessage;->〇080:Landroid/net/Uri;

    .line 334
    .line 335
    aput-object v0, p7, v11

    .line 336
    .line 337
    goto/16 :goto_0

    .line 338
    .line 339
    :sswitch_e
    const-string v2, "sync_dir_id"

    .line 340
    .line 341
    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 342
    .line 343
    .line 344
    move-result v3

    .line 345
    if-nez v3, :cond_8

    .line 346
    .line 347
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 348
    .line 349
    .line 350
    move-result-object v3

    .line 351
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    .line 353
    .line 354
    :cond_8
    const/16 v2, 0x46

    .line 355
    .line 356
    if-eq v1, v2, :cond_9

    .line 357
    .line 358
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 359
    .line 360
    .line 361
    move-result v1

    .line 362
    if-nez v1, :cond_9

    .line 363
    .line 364
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 365
    .line 366
    .line 367
    move-result-object v1

    .line 368
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 369
    .line 370
    .line 371
    :cond_9
    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 372
    .line 373
    .line 374
    move-result v1

    .line 375
    if-nez v1, :cond_a

    .line 376
    .line 377
    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    .line 378
    .line 379
    .line 380
    move-result-object v1

    .line 381
    invoke-static {v1}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 382
    .line 383
    .line 384
    move-result-object v1

    .line 385
    invoke-virtual {v0, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    .line 387
    .line 388
    :cond_a
    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 389
    .line 390
    .line 391
    move-result v1

    .line 392
    if-nez v1, :cond_b

    .line 393
    .line 394
    invoke-virtual {v0, v5, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 395
    .line 396
    .line 397
    :cond_b
    const-string v0, "dirs"

    .line 398
    .line 399
    aput-object v0, p5, v11

    .line 400
    .line 401
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Dir;->〇o〇:Landroid/net/Uri;

    .line 402
    .line 403
    aput-object v0, p7, v11

    .line 404
    .line 405
    aput-object v9, p6, v11

    .line 406
    .line 407
    goto/16 :goto_0

    .line 408
    .line 409
    :sswitch_f
    const-string v0, "sharedapps"

    .line 410
    .line 411
    aput-object v0, p5, v11

    .line 412
    .line 413
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SharedApps;->〇080:Landroid/net/Uri;

    .line 414
    .line 415
    aput-object v0, p7, v11

    .line 416
    .line 417
    goto/16 :goto_0

    .line 418
    .line 419
    :sswitch_10
    const-string v1, "faxinfo"

    .line 420
    .line 421
    aput-object v1, p5, v11

    .line 422
    .line 423
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$FaxInfo;->〇080:Landroid/net/Uri;

    .line 424
    .line 425
    aput-object v1, p7, v11

    .line 426
    .line 427
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 428
    .line 429
    .line 430
    move-result v1

    .line 431
    if-nez v1, :cond_2c

    .line 432
    .line 433
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 434
    .line 435
    .line 436
    move-result-object v1

    .line 437
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 438
    .line 439
    .line 440
    goto/16 :goto_0

    .line 441
    .line 442
    :sswitch_11
    const-string v1, "documentlike"

    .line 443
    .line 444
    aput-object v1, p5, v11

    .line 445
    .line 446
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$DocLike;->〇080:Landroid/net/Uri;

    .line 447
    .line 448
    aput-object v1, p7, v11

    .line 449
    .line 450
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 451
    .line 452
    .line 453
    move-result v1

    .line 454
    if-nez v1, :cond_2c

    .line 455
    .line 456
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 457
    .line 458
    .line 459
    move-result-object v1

    .line 460
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 461
    .line 462
    .line 463
    goto/16 :goto_0

    .line 464
    .line 465
    :sswitch_12
    const-string v1, "collaboratemsgs"

    .line 466
    .line 467
    aput-object v1, p5, v11

    .line 468
    .line 469
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$CollaborateMsg;->〇080:Landroid/net/Uri;

    .line 470
    .line 471
    aput-object v1, p7, v11

    .line 472
    .line 473
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 474
    .line 475
    .line 476
    move-result v1

    .line 477
    if-nez v1, :cond_2c

    .line 478
    .line 479
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 480
    .line 481
    .line 482
    move-result-object v1

    .line 483
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 484
    .line 485
    .line 486
    goto/16 :goto_0

    .line 487
    .line 488
    :sswitch_13
    const-string v1, "comments"

    .line 489
    .line 490
    aput-object v1, p5, v11

    .line 491
    .line 492
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Comments;->〇080:Landroid/net/Uri;

    .line 493
    .line 494
    aput-object v1, p7, v11

    .line 495
    .line 496
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 497
    .line 498
    .line 499
    move-result v1

    .line 500
    if-nez v1, :cond_2c

    .line 501
    .line 502
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 503
    .line 504
    .line 505
    move-result-object v1

    .line 506
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 507
    .line 508
    .line 509
    goto/16 :goto_0

    .line 510
    .line 511
    :sswitch_14
    const-string v1, "collaborators"

    .line 512
    .line 513
    aput-object v1, p5, v11

    .line 514
    .line 515
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Collaborators;->〇080:Landroid/net/Uri;

    .line 516
    .line 517
    aput-object v1, p7, v11

    .line 518
    .line 519
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 520
    .line 521
    .line 522
    move-result v1

    .line 523
    if-nez v1, :cond_2c

    .line 524
    .line 525
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 526
    .line 527
    .line 528
    move-result-object v1

    .line 529
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 530
    .line 531
    .line 532
    goto/16 :goto_0

    .line 533
    .line 534
    :sswitch_15
    const-string v0, "page_mark"

    .line 535
    .line 536
    aput-object v0, p5, v11

    .line 537
    .line 538
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$PageMark;->〇080:Landroid/net/Uri;

    .line 539
    .line 540
    aput-object v0, p7, v11

    .line 541
    .line 542
    goto/16 :goto_0

    .line 543
    .line 544
    :sswitch_16
    const-string v0, "sync_restore"

    .line 545
    .line 546
    aput-object v0, p5, v11

    .line 547
    .line 548
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SyncRestore;->〇080:Landroid/net/Uri;

    .line 549
    .line 550
    aput-object v0, p7, v11

    .line 551
    .line 552
    goto/16 :goto_0

    .line 553
    .line 554
    :sswitch_17
    const-string v0, "pdfsize"

    .line 555
    .line 556
    aput-object v0, p5, v11

    .line 557
    .line 558
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$PdfSize;->〇080:Landroid/net/Uri;

    .line 559
    .line 560
    aput-object v0, p7, v11

    .line 561
    .line 562
    goto/16 :goto_0

    .line 563
    .line 564
    :sswitch_18
    const-string v0, "notepath"

    .line 565
    .line 566
    aput-object v0, p5, v11

    .line 567
    .line 568
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$NotePath;->〇080:Landroid/net/Uri;

    .line 569
    .line 570
    aput-object v0, p7, v11

    .line 571
    .line 572
    goto/16 :goto_0

    .line 573
    .line 574
    :sswitch_19
    const-string v0, "graphics"

    .line 575
    .line 576
    aput-object v0, p5, v11

    .line 577
    .line 578
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Graphics;->〇080:Landroid/net/Uri;

    .line 579
    .line 580
    aput-object v0, p7, v11

    .line 581
    .line 582
    goto/16 :goto_0

    .line 583
    .line 584
    :sswitch_1a
    const-string v0, "sync_accounts"

    .line 585
    .line 586
    aput-object v0, p5, v11

    .line 587
    .line 588
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$SyncAccount;->〇080:Landroid/net/Uri;

    .line 589
    .line 590
    aput-object v0, p7, v11

    .line 591
    .line 592
    goto/16 :goto_0

    .line 593
    .line 594
    :sswitch_1b
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 595
    .line 596
    .line 597
    move-result v1

    .line 598
    if-nez v1, :cond_c

    .line 599
    .line 600
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 601
    .line 602
    .line 603
    move-result-object v1

    .line 604
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 605
    .line 606
    .line 607
    :cond_c
    const-string v0, "printtask"

    .line 608
    .line 609
    aput-object v0, p5, v11

    .line 610
    .line 611
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$PrintTask;->〇080:Landroid/net/Uri;

    .line 612
    .line 613
    aput-object v0, p7, v11

    .line 614
    .line 615
    goto/16 :goto_0

    .line 616
    .line 617
    :sswitch_1c
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 618
    .line 619
    .line 620
    move-result v1

    .line 621
    if-nez v1, :cond_d

    .line 622
    .line 623
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 624
    .line 625
    .line 626
    move-result-object v1

    .line 627
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 628
    .line 629
    .line 630
    :cond_d
    const-string v0, "faxtask"

    .line 631
    .line 632
    aput-object v0, p5, v11

    .line 633
    .line 634
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$FaxTask;->〇080:Landroid/net/Uri;

    .line 635
    .line 636
    aput-object v0, p7, v11

    .line 637
    .line 638
    goto/16 :goto_0

    .line 639
    .line 640
    :sswitch_1d
    const-string v0, "mtags"

    .line 641
    .line 642
    aput-object v0, p5, v11

    .line 643
    .line 644
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 645
    .line 646
    aput-object v0, p7, v11

    .line 647
    .line 648
    goto/16 :goto_0

    .line 649
    .line 650
    :sswitch_1e
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 651
    .line 652
    .line 653
    move-result v1

    .line 654
    if-nez v1, :cond_e

    .line 655
    .line 656
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 657
    .line 658
    .line 659
    move-result-object v1

    .line 660
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 661
    .line 662
    .line 663
    :cond_e
    const-string v0, "uploadstate"

    .line 664
    .line 665
    aput-object v0, p5, v11

    .line 666
    .line 667
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$UploadState;->〇080:Landroid/net/Uri;

    .line 668
    .line 669
    aput-object v0, p7, v11

    .line 670
    .line 671
    const-string v0, "token"

    .line 672
    .line 673
    aput-object v0, p6, v11

    .line 674
    .line 675
    goto/16 :goto_0

    .line 676
    .line 677
    :sswitch_1f
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 678
    .line 679
    .line 680
    move-result v1

    .line 681
    if-nez v1, :cond_f

    .line 682
    .line 683
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 684
    .line 685
    .line 686
    move-result-object v1

    .line 687
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 688
    .line 689
    .line 690
    :cond_f
    const-string v0, "accounts"

    .line 691
    .line 692
    aput-object v0, p5, v11

    .line 693
    .line 694
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Account;->〇080:Landroid/net/Uri;

    .line 695
    .line 696
    aput-object v0, p7, v11

    .line 697
    .line 698
    const-string v0, "username"

    .line 699
    .line 700
    aput-object v0, p6, v11

    .line 701
    .line 702
    goto/16 :goto_0

    .line 703
    .line 704
    :sswitch_20
    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 705
    .line 706
    .line 707
    move-result v1

    .line 708
    if-nez v1, :cond_10

    .line 709
    .line 710
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 711
    .line 712
    .line 713
    move-result-wide v1

    .line 714
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 715
    .line 716
    .line 717
    move-result-object v1

    .line 718
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 719
    .line 720
    .line 721
    :cond_10
    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 722
    .line 723
    .line 724
    move-result v1

    .line 725
    if-nez v1, :cond_11

    .line 726
    .line 727
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 728
    .line 729
    .line 730
    move-result-wide v1

    .line 731
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 732
    .line 733
    .line 734
    move-result-object v1

    .line 735
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 736
    .line 737
    .line 738
    :cond_11
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 739
    .line 740
    .line 741
    move-result v1

    .line 742
    if-nez v1, :cond_12

    .line 743
    .line 744
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 745
    .line 746
    .line 747
    move-result-object v1

    .line 748
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 749
    .line 750
    .line 751
    :cond_12
    const-string v1, "sync_tag_id"

    .line 752
    .line 753
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 754
    .line 755
    .line 756
    move-result v2

    .line 757
    if-nez v2, :cond_13

    .line 758
    .line 759
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 760
    .line 761
    .line 762
    move-result-object v2

    .line 763
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    .line 765
    .line 766
    :cond_13
    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 767
    .line 768
    .line 769
    move-result v1

    .line 770
    if-nez v1, :cond_14

    .line 771
    .line 772
    invoke-virtual {v0, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 773
    .line 774
    .line 775
    :cond_14
    const-string v1, "title_pinyin"

    .line 776
    .line 777
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 778
    .line 779
    .line 780
    move-result v2

    .line 781
    if-nez v2, :cond_15

    .line 782
    .line 783
    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    .line 784
    .line 785
    .line 786
    move-result-object v2

    .line 787
    invoke-static {v2}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 788
    .line 789
    .line 790
    move-result-object v2

    .line 791
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    .line 793
    .line 794
    :cond_15
    const-string v0, "tags"

    .line 795
    .line 796
    aput-object v0, p5, v11

    .line 797
    .line 798
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 799
    .line 800
    aput-object v0, p7, v11

    .line 801
    .line 802
    aput-object v9, p6, v11

    .line 803
    .line 804
    goto/16 :goto_0

    .line 805
    .line 806
    :sswitch_21
    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 807
    .line 808
    .line 809
    move-result v2

    .line 810
    if-nez v2, :cond_16

    .line 811
    .line 812
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 813
    .line 814
    .line 815
    move-result-wide v13

    .line 816
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 817
    .line 818
    .line 819
    move-result-object v2

    .line 820
    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 821
    .line 822
    .line 823
    :cond_16
    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 824
    .line 825
    .line 826
    move-result v2

    .line 827
    if-nez v2, :cond_17

    .line 828
    .line 829
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 830
    .line 831
    .line 832
    move-result-wide v13

    .line 833
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 834
    .line 835
    .line 836
    move-result-object v2

    .line 837
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 838
    .line 839
    .line 840
    :cond_17
    const/16 v2, 0x31

    .line 841
    .line 842
    if-eq v1, v2, :cond_18

    .line 843
    .line 844
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 845
    .line 846
    .line 847
    move-result v1

    .line 848
    if-nez v1, :cond_18

    .line 849
    .line 850
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 851
    .line 852
    .line 853
    move-result-object v1

    .line 854
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 855
    .line 856
    .line 857
    :cond_18
    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 858
    .line 859
    .line 860
    move-result v1

    .line 861
    if-nez v1, :cond_19

    .line 862
    .line 863
    invoke-virtual {v0, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 864
    .line 865
    .line 866
    :cond_19
    const-string v1, "sync_jpage_state"

    .line 867
    .line 868
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 869
    .line 870
    .line 871
    move-result v2

    .line 872
    if-nez v2, :cond_1a

    .line 873
    .line 874
    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 875
    .line 876
    .line 877
    :cond_1a
    const-string v1, "min_version"

    .line 878
    .line 879
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 880
    .line 881
    .line 882
    move-result v2

    .line 883
    const/high16 v3, 0x3f800000    # 1.0f

    .line 884
    .line 885
    if-nez v2, :cond_1b

    .line 886
    .line 887
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 888
    .line 889
    .line 890
    move-result-object v2

    .line 891
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 892
    .line 893
    .line 894
    :cond_1b
    const-string v1, "max_version"

    .line 895
    .line 896
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 897
    .line 898
    .line 899
    move-result v2

    .line 900
    if-nez v2, :cond_1c

    .line 901
    .line 902
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 903
    .line 904
    .line 905
    move-result-object v2

    .line 906
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 907
    .line 908
    .line 909
    :cond_1c
    const-string v1, "sync_raw_jpg_state"

    .line 910
    .line 911
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 912
    .line 913
    .line 914
    move-result v1

    .line 915
    if-nez v1, :cond_1d

    .line 916
    .line 917
    const-string v1, "sync_raw_jpg_state"

    .line 918
    .line 919
    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 920
    .line 921
    .line 922
    :cond_1d
    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 923
    .line 924
    .line 925
    move-result v1

    .line 926
    if-nez v1, :cond_1e

    .line 927
    .line 928
    invoke-virtual {v0, v5, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 929
    .line 930
    .line 931
    :cond_1e
    const-string v0, "images"

    .line 932
    .line 933
    aput-object v0, p5, v11

    .line 934
    .line 935
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 936
    .line 937
    aput-object v0, p7, v11

    .line 938
    .line 939
    const-string v0, "thumb_data"

    .line 940
    .line 941
    aput-object v0, p6, v11

    .line 942
    .line 943
    goto/16 :goto_0

    .line 944
    .line 945
    :sswitch_22
    const-string v2, "created"

    .line 946
    .line 947
    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 948
    .line 949
    .line 950
    move-result v3

    .line 951
    if-nez v3, :cond_1f

    .line 952
    .line 953
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 954
    .line 955
    .line 956
    move-result-wide v3

    .line 957
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 958
    .line 959
    .line 960
    move-result-object v3

    .line 961
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 962
    .line 963
    .line 964
    :cond_1f
    const-string v2, "modified"

    .line 965
    .line 966
    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 967
    .line 968
    .line 969
    move-result v3

    .line 970
    if-nez v3, :cond_20

    .line 971
    .line 972
    const-string v3, "DocumentProvider"

    .line 973
    .line 974
    const-string v4, "modified_date wrapInsertContentValues"

    .line 975
    .line 976
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    .line 978
    .line 979
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 980
    .line 981
    .line 982
    move-result-wide v3

    .line 983
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 984
    .line 985
    .line 986
    move-result-object v3

    .line 987
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 988
    .line 989
    .line 990
    :cond_20
    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 991
    .line 992
    .line 993
    move-result v2

    .line 994
    if-nez v2, :cond_21

    .line 995
    .line 996
    const-string v2, ""

    .line 997
    .line 998
    invoke-virtual {v0, v9, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    .line 1000
    .line 1001
    :cond_21
    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1002
    .line 1003
    .line 1004
    move-result v2

    .line 1005
    if-nez v2, :cond_22

    .line 1006
    .line 1007
    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    .line 1008
    .line 1009
    .line 1010
    move-result-object v2

    .line 1011
    invoke-static {v2}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 1012
    .line 1013
    .line 1014
    move-result-object v2

    .line 1015
    invoke-virtual {v0, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    .line 1017
    .line 1018
    :cond_22
    const/16 v2, 0x35

    .line 1019
    .line 1020
    if-eq v1, v2, :cond_23

    .line 1021
    .line 1022
    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1023
    .line 1024
    .line 1025
    move-result v1

    .line 1026
    if-nez v1, :cond_23

    .line 1027
    .line 1028
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 1029
    .line 1030
    .line 1031
    move-result-object v1

    .line 1032
    invoke-virtual {v0, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1033
    .line 1034
    .line 1035
    :cond_23
    const-string v1, "sync_doc_id"

    .line 1036
    .line 1037
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1038
    .line 1039
    .line 1040
    move-result v2

    .line 1041
    if-nez v2, :cond_24

    .line 1042
    .line 1043
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 1044
    .line 1045
    .line 1046
    move-result-object v2

    .line 1047
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    .line 1049
    .line 1050
    :cond_24
    const-string v1, "page_orientation"

    .line 1051
    .line 1052
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1053
    .line 1054
    .line 1055
    move-result v2

    .line 1056
    if-nez v2, :cond_25

    .line 1057
    .line 1058
    invoke-static {p0}, Lcom/intsig/camscanner/provider/ProviderSpHelper;->〇o00〇〇Oo(Landroid/content/Context;)I

    .line 1059
    .line 1060
    .line 1061
    move-result v2

    .line 1062
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 1063
    .line 1064
    .line 1065
    move-result-object v2

    .line 1066
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1067
    .line 1068
    .line 1069
    :cond_25
    const-string v1, "page_size"

    .line 1070
    .line 1071
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1072
    .line 1073
    .line 1074
    move-result v1

    .line 1075
    if-nez v1, :cond_26

    .line 1076
    .line 1077
    invoke-static {p0}, Lcom/intsig/camscanner/provider/ProviderSpHelper;->〇o〇(Landroid/content/Context;)J

    .line 1078
    .line 1079
    .line 1080
    move-result-wide v1

    .line 1081
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 1082
    .line 1083
    .line 1084
    move-result-object v1

    .line 1085
    const-string v2, "page_size"

    .line 1086
    .line 1087
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1088
    .line 1089
    .line 1090
    :cond_26
    const-string v1, "page_margin"

    .line 1091
    .line 1092
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1093
    .line 1094
    .line 1095
    move-result v1

    .line 1096
    if-nez v1, :cond_27

    .line 1097
    .line 1098
    invoke-static {p0}, Lcom/intsig/camscanner/provider/ProviderSpHelper;->〇080(Landroid/content/Context;)I

    .line 1099
    .line 1100
    .line 1101
    move-result v1

    .line 1102
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 1103
    .line 1104
    .line 1105
    move-result-object v1

    .line 1106
    const-string v2, "page_margin"

    .line 1107
    .line 1108
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1109
    .line 1110
    .line 1111
    :cond_27
    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1112
    .line 1113
    .line 1114
    move-result v1

    .line 1115
    if-nez v1, :cond_28

    .line 1116
    .line 1117
    invoke-virtual {v0, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1118
    .line 1119
    .line 1120
    :cond_28
    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1121
    .line 1122
    .line 1123
    move-result v1

    .line 1124
    if-nez v1, :cond_29

    .line 1125
    .line 1126
    invoke-virtual {v0, v5, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1127
    .line 1128
    .line 1129
    :cond_29
    const-string v1, "office_file_sync_state"

    .line 1130
    .line 1131
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1132
    .line 1133
    .line 1134
    move-result v1

    .line 1135
    if-nez v1, :cond_2a

    .line 1136
    .line 1137
    const-string v1, "office_file_sync_state"

    .line 1138
    .line 1139
    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1140
    .line 1141
    .line 1142
    :cond_2a
    const-string v1, "office_file_sync_state_backup"

    .line 1143
    .line 1144
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 1145
    .line 1146
    .line 1147
    move-result v1

    .line 1148
    if-nez v1, :cond_2b

    .line 1149
    .line 1150
    const-string v1, "office_file_sync_state_backup"

    .line 1151
    .line 1152
    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1153
    .line 1154
    .line 1155
    :cond_2b
    const-string v0, "documents"

    .line 1156
    .line 1157
    aput-object v0, p5, v11

    .line 1158
    .line 1159
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 1160
    .line 1161
    aput-object v0, p7, v11

    .line 1162
    .line 1163
    const-string v0, "_data"

    .line 1164
    .line 1165
    aput-object v0, p6, v11

    .line 1166
    .line 1167
    :cond_2c
    :goto_0
    return-void

    .line 1168
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_22
        0x3 -> :sswitch_21
        0x5 -> :sswitch_20
        0x9 -> :sswitch_1f
        0xb -> :sswitch_1e
        0xd -> :sswitch_1d
        0x11 -> :sswitch_1c
        0x13 -> :sswitch_1b
        0x15 -> :sswitch_1a
        0x17 -> :sswitch_22
        0x18 -> :sswitch_21
        0x19 -> :sswitch_20
        0x1a -> :sswitch_19
        0x1c -> :sswitch_18
        0x20 -> :sswitch_17
        0x22 -> :sswitch_16
        0x28 -> :sswitch_15
        0x2b -> :sswitch_14
        0x2d -> :sswitch_13
        0x31 -> :sswitch_21
        0x35 -> :sswitch_22
        0x37 -> :sswitch_12
        0x39 -> :sswitch_11
        0x3b -> :sswitch_10
        0x3e -> :sswitch_f
        0x42 -> :sswitch_e
        0x44 -> :sswitch_e
        0x46 -> :sswitch_e
        0x48 -> :sswitch_d
        0x4a -> :sswitch_c
        0x4c -> :sswitch_b
        0x4e -> :sswitch_a
        0x50 -> :sswitch_9
        0x53 -> :sswitch_8
        0x56 -> :sswitch_7
        0x58 -> :sswitch_6
        0x5a -> :sswitch_5
        0x5c -> :sswitch_5
        0x5e -> :sswitch_4
        0x60 -> :sswitch_3
        0x62 -> :sswitch_2
        0x64 -> :sswitch_1
        0x66 -> :sswitch_0
    .end sparse-switch
.end method

.method private 〇O8o08O(Landroid/net/Uri;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->Oo08()Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, L〇8〇〇8o/Oo08;

    .line 6
    .line 7
    invoke-direct {v1, p0, p1}, L〇8〇〇8o/Oo08;-><init>(Lcom/intsig/camscanner/provider/DocumentProvider;Landroid/net/Uri;)V

    .line 8
    .line 9
    .line 10
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static 〇O〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇08O〇00〇o:Ljava/lang/String;

    .line 8
    .line 9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    sget-object p0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇08O〇00〇o:Ljava/lang/String;

    .line 16
    .line 17
    invoke-static {p0}, Lcom/intsig/camscanner/provider/Documents;->〇080(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/provider/Documents;->〇080(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/provider/DocumentProvider;->〇〇888()V

    .line 25
    .line 26
    .line 27
    const/4 p0, 0x1

    .line 28
    sput-boolean p0, Lcom/intsig/camscanner/provider/DocumentProvider;->OO:Z

    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/provider/DocumentProvider;Landroid/net/Uri;JLandroid/content/ContentValues;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/provider/DocumentProvider;->OO0o〇〇〇〇0(Landroid/net/Uri;JLandroid/content/ContentValues;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/provider/DocumentProvider;Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/provider/DocumentProvider;->oO80(Landroid/net/Uri;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static 〇〇808〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/camscanner/provider/DocumentProvider;->OO:Z

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/provider/DocumentProvider;->〇O〇(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    :cond_1
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static 〇〇888()V
    .locals 4

    .line 1
    new-instance v0, Landroid/content/UriMatcher;

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 8
    .line 9
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 10
    .line 11
    const-string v2, "dirs"

    .line 12
    .line 13
    const/16 v3, 0x42

    .line 14
    .line 15
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 16
    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 19
    .line 20
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 21
    .line 22
    const-string v2, "dirs/#"

    .line 23
    .line 24
    const/16 v3, 0x43

    .line 25
    .line 26
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 30
    .line 31
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 32
    .line 33
    const-string v2, "dirs/sync"

    .line 34
    .line 35
    const/16 v3, 0x44

    .line 36
    .line 37
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 38
    .line 39
    .line 40
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 41
    .line 42
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 43
    .line 44
    const-string v2, "dirs/sync/#"

    .line 45
    .line 46
    const/16 v3, 0x45

    .line 47
    .line 48
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 49
    .line 50
    .line 51
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 52
    .line 53
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 54
    .line 55
    const-string v2, "dirs/alldir"

    .line 56
    .line 57
    const/16 v3, 0x46

    .line 58
    .line 59
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 60
    .line 61
    .line 62
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 63
    .line 64
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 65
    .line 66
    const-string v2, "dirs/alldir/#"

    .line 67
    .line 68
    const/16 v3, 0x47

    .line 69
    .line 70
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 71
    .line 72
    .line 73
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 74
    .line 75
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 76
    .line 77
    const-string v2, "documents"

    .line 78
    .line 79
    const/4 v3, 0x1

    .line 80
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 81
    .line 82
    .line 83
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 84
    .line 85
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 86
    .line 87
    const-string v2, "documents/#"

    .line 88
    .line 89
    const/4 v3, 0x2

    .line 90
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 91
    .line 92
    .line 93
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 94
    .line 95
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 96
    .line 97
    const-string v2, "documents/tag/#"

    .line 98
    .line 99
    const/16 v3, 0x8

    .line 100
    .line 101
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 102
    .line 103
    .line 104
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 105
    .line 106
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 107
    .line 108
    const-string v2, "documents/search"

    .line 109
    .line 110
    const/16 v3, 0xf

    .line 111
    .line 112
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 113
    .line 114
    .line 115
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 116
    .line 117
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 118
    .line 119
    const-string v2, "documents/searchtag"

    .line 120
    .line 121
    const/16 v3, 0x3d

    .line 122
    .line 123
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 124
    .line 125
    .line 126
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 127
    .line 128
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 129
    .line 130
    const-string v2, "documents/sync"

    .line 131
    .line 132
    const/16 v3, 0x17

    .line 133
    .line 134
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 135
    .line 136
    .line 137
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 138
    .line 139
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 140
    .line 141
    const-string v2, "documents/sync/#"

    .line 142
    .line 143
    const/16 v3, 0x27

    .line 144
    .line 145
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 146
    .line 147
    .line 148
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 149
    .line 150
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 151
    .line 152
    const-string v2, "documents/nomodify"

    .line 153
    .line 154
    const/16 v3, 0x33

    .line 155
    .line 156
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 157
    .line 158
    .line 159
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 160
    .line 161
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 162
    .line 163
    const-string v2, "documents/nomodify/#"

    .line 164
    .line 165
    const/16 v3, 0x34

    .line 166
    .line 167
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 168
    .line 169
    .line 170
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 171
    .line 172
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 173
    .line 174
    const-string v2, "documents/alldoc"

    .line 175
    .line 176
    const/16 v3, 0x35

    .line 177
    .line 178
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 179
    .line 180
    .line 181
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 182
    .line 183
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 184
    .line 185
    const-string v2, "documents/alldoc/#"

    .line 186
    .line 187
    const/16 v3, 0x36

    .line 188
    .line 189
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 190
    .line 191
    .line 192
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 193
    .line 194
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 195
    .line 196
    const-string v2, "images"

    .line 197
    .line 198
    const/4 v3, 0x3

    .line 199
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 200
    .line 201
    .line 202
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 203
    .line 204
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 205
    .line 206
    const-string v2, "images/#"

    .line 207
    .line 208
    const/4 v3, 0x4

    .line 209
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 210
    .line 211
    .line 212
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 213
    .line 214
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 215
    .line 216
    const-string v2, "images/doc/#"

    .line 217
    .line 218
    const/4 v3, 0x7

    .line 219
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 220
    .line 221
    .line 222
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 223
    .line 224
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 225
    .line 226
    const-string v2, "images/sync"

    .line 227
    .line 228
    const/16 v3, 0x18

    .line 229
    .line 230
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 231
    .line 232
    .line 233
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 234
    .line 235
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 236
    .line 237
    const-string v2, "images/sync/#"

    .line 238
    .line 239
    const/16 v3, 0x25

    .line 240
    .line 241
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 242
    .line 243
    .line 244
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 245
    .line 246
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 247
    .line 248
    const-string v2, "images/update_doc"

    .line 249
    .line 250
    const/16 v3, 0x24

    .line 251
    .line 252
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 253
    .line 254
    .line 255
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 256
    .line 257
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 258
    .line 259
    const-string v2, "images/nomodify"

    .line 260
    .line 261
    const/16 v3, 0x2f

    .line 262
    .line 263
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 264
    .line 265
    .line 266
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 267
    .line 268
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 269
    .line 270
    const-string v2, "images/nomodify/#"

    .line 271
    .line 272
    const/16 v3, 0x30

    .line 273
    .line 274
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 275
    .line 276
    .line 277
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 278
    .line 279
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 280
    .line 281
    const-string v2, "images/allpage"

    .line 282
    .line 283
    const/16 v3, 0x31

    .line 284
    .line 285
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 286
    .line 287
    .line 288
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 289
    .line 290
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 291
    .line 292
    const-string v2, "images/allpage/#"

    .line 293
    .line 294
    const/16 v3, 0x32

    .line 295
    .line 296
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 297
    .line 298
    .line 299
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 300
    .line 301
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 302
    .line 303
    const-string v2, "tags"

    .line 304
    .line 305
    const/4 v3, 0x5

    .line 306
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 307
    .line 308
    .line 309
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 310
    .line 311
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 312
    .line 313
    const-string v2, "tags/#"

    .line 314
    .line 315
    const/4 v3, 0x6

    .line 316
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 317
    .line 318
    .line 319
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 320
    .line 321
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 322
    .line 323
    const-string v2, "tags/sync"

    .line 324
    .line 325
    const/16 v3, 0x19

    .line 326
    .line 327
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 328
    .line 329
    .line 330
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 331
    .line 332
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 333
    .line 334
    const-string v2, "tags/mtag"

    .line 335
    .line 336
    const/16 v3, 0x40

    .line 337
    .line 338
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 339
    .line 340
    .line 341
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 342
    .line 343
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 344
    .line 345
    const-string v2, "tags/sync/#"

    .line 346
    .line 347
    const/16 v3, 0x26

    .line 348
    .line 349
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 350
    .line 351
    .line 352
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 353
    .line 354
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 355
    .line 356
    const-string v2, "accounts"

    .line 357
    .line 358
    const/16 v3, 0x9

    .line 359
    .line 360
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 361
    .line 362
    .line 363
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 364
    .line 365
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 366
    .line 367
    const-string v2, "accounts/#"

    .line 368
    .line 369
    const/16 v3, 0xa

    .line 370
    .line 371
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 372
    .line 373
    .line 374
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 375
    .line 376
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 377
    .line 378
    const-string v2, "uploadstate"

    .line 379
    .line 380
    const/16 v3, 0xb

    .line 381
    .line 382
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 383
    .line 384
    .line 385
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 386
    .line 387
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 388
    .line 389
    const-string v2, "uploadstate/#"

    .line 390
    .line 391
    const/16 v3, 0xc

    .line 392
    .line 393
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 394
    .line 395
    .line 396
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 397
    .line 398
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 399
    .line 400
    const-string v2, "mtags"

    .line 401
    .line 402
    const/16 v3, 0xd

    .line 403
    .line 404
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 405
    .line 406
    .line 407
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 408
    .line 409
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 410
    .line 411
    const-string v2, "mtags/#"

    .line 412
    .line 413
    const/16 v3, 0xe

    .line 414
    .line 415
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 416
    .line 417
    .line 418
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 419
    .line 420
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 421
    .line 422
    const-string v2, "faxtask"

    .line 423
    .line 424
    const/16 v3, 0x11

    .line 425
    .line 426
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 427
    .line 428
    .line 429
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 430
    .line 431
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 432
    .line 433
    const-string v2, "faxtask/#"

    .line 434
    .line 435
    const/16 v3, 0x12

    .line 436
    .line 437
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 438
    .line 439
    .line 440
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 441
    .line 442
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 443
    .line 444
    const-string v2, "printtask"

    .line 445
    .line 446
    const/16 v3, 0x13

    .line 447
    .line 448
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 449
    .line 450
    .line 451
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 452
    .line 453
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 454
    .line 455
    const-string v2, "printtask/#"

    .line 456
    .line 457
    const/16 v3, 0x14

    .line 458
    .line 459
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 460
    .line 461
    .line 462
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 463
    .line 464
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 465
    .line 466
    const-string v2, "sync_accounts"

    .line 467
    .line 468
    const/16 v3, 0x15

    .line 469
    .line 470
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 471
    .line 472
    .line 473
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 474
    .line 475
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 476
    .line 477
    const-string v2, "sync_accounts/#"

    .line 478
    .line 479
    const/16 v3, 0x16

    .line 480
    .line 481
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 482
    .line 483
    .line 484
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 485
    .line 486
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 487
    .line 488
    const-string v2, "graphics"

    .line 489
    .line 490
    const/16 v3, 0x1a

    .line 491
    .line 492
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 493
    .line 494
    .line 495
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 496
    .line 497
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 498
    .line 499
    const-string v2, "graphics/#"

    .line 500
    .line 501
    const/16 v3, 0x1b

    .line 502
    .line 503
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 504
    .line 505
    .line 506
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 507
    .line 508
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 509
    .line 510
    const-string v2, "graphics/image/#"

    .line 511
    .line 512
    const/16 v3, 0x1e

    .line 513
    .line 514
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 515
    .line 516
    .line 517
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 518
    .line 519
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 520
    .line 521
    const-string v2, "notepath"

    .line 522
    .line 523
    const/16 v3, 0x1c

    .line 524
    .line 525
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 526
    .line 527
    .line 528
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 529
    .line 530
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 531
    .line 532
    const-string v2, "notepath/#"

    .line 533
    .line 534
    const/16 v3, 0x1d

    .line 535
    .line 536
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 537
    .line 538
    .line 539
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 540
    .line 541
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 542
    .line 543
    const-string v2, "notepath/graphichs/#"

    .line 544
    .line 545
    const/16 v3, 0x1f

    .line 546
    .line 547
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 548
    .line 549
    .line 550
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 551
    .line 552
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 553
    .line 554
    const-string v2, "pdfsize"

    .line 555
    .line 556
    const/16 v3, 0x20

    .line 557
    .line 558
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 559
    .line 560
    .line 561
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 562
    .line 563
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 564
    .line 565
    const-string v2, "pdfsize/#"

    .line 566
    .line 567
    const/16 v3, 0x21

    .line 568
    .line 569
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 570
    .line 571
    .line 572
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 573
    .line 574
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 575
    .line 576
    const-string v2, "sync_restore"

    .line 577
    .line 578
    const/16 v3, 0x22

    .line 579
    .line 580
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 581
    .line 582
    .line 583
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 584
    .line 585
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 586
    .line 587
    const-string v2, "sync_restore/#"

    .line 588
    .line 589
    const/16 v3, 0x23

    .line 590
    .line 591
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 592
    .line 593
    .line 594
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 595
    .line 596
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 597
    .line 598
    const-string v2, "page_mark"

    .line 599
    .line 600
    const/16 v3, 0x28

    .line 601
    .line 602
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 603
    .line 604
    .line 605
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 606
    .line 607
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 608
    .line 609
    const-string v2, "page_mark/#"

    .line 610
    .line 611
    const/16 v3, 0x29

    .line 612
    .line 613
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 614
    .line 615
    .line 616
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 617
    .line 618
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 619
    .line 620
    const-string v2, "page_mark/page/#"

    .line 621
    .line 622
    const/16 v3, 0x2a

    .line 623
    .line 624
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 625
    .line 626
    .line 627
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 628
    .line 629
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 630
    .line 631
    const-string v2, "collaborators"

    .line 632
    .line 633
    const/16 v3, 0x2b

    .line 634
    .line 635
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 636
    .line 637
    .line 638
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 639
    .line 640
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 641
    .line 642
    const-string v2, "collaborators/#"

    .line 643
    .line 644
    const/16 v3, 0x2c

    .line 645
    .line 646
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 647
    .line 648
    .line 649
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 650
    .line 651
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 652
    .line 653
    const-string v2, "comments"

    .line 654
    .line 655
    const/16 v3, 0x2d

    .line 656
    .line 657
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 658
    .line 659
    .line 660
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 661
    .line 662
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 663
    .line 664
    const-string v2, "comments/#"

    .line 665
    .line 666
    const/16 v3, 0x2e

    .line 667
    .line 668
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 669
    .line 670
    .line 671
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 672
    .line 673
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 674
    .line 675
    const-string v2, "collaboratemsgs"

    .line 676
    .line 677
    const/16 v3, 0x37

    .line 678
    .line 679
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 680
    .line 681
    .line 682
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 683
    .line 684
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 685
    .line 686
    const-string v2, "collaboratemsgs/#"

    .line 687
    .line 688
    const/16 v3, 0x38

    .line 689
    .line 690
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 691
    .line 692
    .line 693
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 694
    .line 695
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 696
    .line 697
    const-string v2, "documentlike"

    .line 698
    .line 699
    const/16 v3, 0x39

    .line 700
    .line 701
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 702
    .line 703
    .line 704
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 705
    .line 706
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 707
    .line 708
    const-string v2, "documentlike/#"

    .line 709
    .line 710
    const/16 v3, 0x3a

    .line 711
    .line 712
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 713
    .line 714
    .line 715
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 716
    .line 717
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 718
    .line 719
    const-string v2, "faxinfo"

    .line 720
    .line 721
    const/16 v3, 0x3b

    .line 722
    .line 723
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 724
    .line 725
    .line 726
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 727
    .line 728
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 729
    .line 730
    const-string v2, "faxinfo/#"

    .line 731
    .line 732
    const/16 v3, 0x3c

    .line 733
    .line 734
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 735
    .line 736
    .line 737
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 738
    .line 739
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 740
    .line 741
    const-string v2, "sharedapps"

    .line 742
    .line 743
    const/16 v3, 0x3e

    .line 744
    .line 745
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 746
    .line 747
    .line 748
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 749
    .line 750
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 751
    .line 752
    const-string v2, "sharedapps/#"

    .line 753
    .line 754
    const/16 v3, 0x3f

    .line 755
    .line 756
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 757
    .line 758
    .line 759
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 760
    .line 761
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 762
    .line 763
    const-string v2, "tags/alltag"

    .line 764
    .line 765
    const/16 v3, 0x41

    .line 766
    .line 767
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 768
    .line 769
    .line 770
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 771
    .line 772
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 773
    .line 774
    const-string v2, "messagecenters"

    .line 775
    .line 776
    const/16 v3, 0x48

    .line 777
    .line 778
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 779
    .line 780
    .line 781
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 782
    .line 783
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 784
    .line 785
    const-string v2, "messagecenters/#"

    .line 786
    .line 787
    const/16 v3, 0x49

    .line 788
    .line 789
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 790
    .line 791
    .line 792
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 793
    .line 794
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 795
    .line 796
    const-string v2, "messagecenters/allmsg"

    .line 797
    .line 798
    const/16 v3, 0x4a

    .line 799
    .line 800
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 801
    .line 802
    .line 803
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 804
    .line 805
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 806
    .line 807
    const-string v2, "messagecenters/allsmg/#"

    .line 808
    .line 809
    const/16 v3, 0x4b

    .line 810
    .line 811
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 812
    .line 813
    .line 814
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 815
    .line 816
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 817
    .line 818
    const-string v2, "teaminfos"

    .line 819
    .line 820
    const/16 v3, 0x4c

    .line 821
    .line 822
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 823
    .line 824
    .line 825
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 826
    .line 827
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 828
    .line 829
    const-string v2, "teaminfos/#"

    .line 830
    .line 831
    const/16 v3, 0x4d

    .line 832
    .line 833
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 834
    .line 835
    .line 836
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 837
    .line 838
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 839
    .line 840
    const-string v2, "teammembers"

    .line 841
    .line 842
    const/16 v3, 0x4e

    .line 843
    .line 844
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 845
    .line 846
    .line 847
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 848
    .line 849
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 850
    .line 851
    const-string v2, "teammembers/#"

    .line 852
    .line 853
    const/16 v3, 0x4f

    .line 854
    .line 855
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 856
    .line 857
    .line 858
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 859
    .line 860
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 861
    .line 862
    const-string v2, "teamfileinfos"

    .line 863
    .line 864
    const/16 v3, 0x50

    .line 865
    .line 866
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 867
    .line 868
    .line 869
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 870
    .line 871
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 872
    .line 873
    const-string v2, "teamfileinfos/#"

    .line 874
    .line 875
    const/16 v3, 0x51

    .line 876
    .line 877
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 878
    .line 879
    .line 880
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 881
    .line 882
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 883
    .line 884
    const-string v2, "teamfileinfos/teammembers"

    .line 885
    .line 886
    const/16 v3, 0x52

    .line 887
    .line 888
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 889
    .line 890
    .line 891
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 892
    .line 893
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 894
    .line 895
    const-string v2, "signature"

    .line 896
    .line 897
    const/16 v3, 0x53

    .line 898
    .line 899
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 900
    .line 901
    .line 902
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 903
    .line 904
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 905
    .line 906
    const-string v2, "signature/#"

    .line 907
    .line 908
    const/16 v3, 0x54

    .line 909
    .line 910
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 911
    .line 912
    .line 913
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 914
    .line 915
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 916
    .line 917
    const-string v2, "signature/page/#"

    .line 918
    .line 919
    const/16 v3, 0x55

    .line 920
    .line 921
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 922
    .line 923
    .line 924
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 925
    .line 926
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 927
    .line 928
    const-string v2, "SyncDeleteStatus"

    .line 929
    .line 930
    const/16 v3, 0x56

    .line 931
    .line 932
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 933
    .line 934
    .line 935
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 936
    .line 937
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 938
    .line 939
    const-string v2, "SyncDeleteStatus/#"

    .line 940
    .line 941
    const/16 v3, 0x57

    .line 942
    .line 943
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 944
    .line 945
    .line 946
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 947
    .line 948
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 949
    .line 950
    const-string v2, "message_center"

    .line 951
    .line 952
    const/16 v3, 0x58

    .line 953
    .line 954
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 955
    .line 956
    .line 957
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 958
    .line 959
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 960
    .line 961
    const-string v2, "message_center/#"

    .line 962
    .line 963
    const/16 v3, 0x59

    .line 964
    .line 965
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 966
    .line 967
    .line 968
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 969
    .line 970
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 971
    .line 972
    const-string v2, "invitesharedir"

    .line 973
    .line 974
    const/16 v3, 0x5a

    .line 975
    .line 976
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 977
    .line 978
    .line 979
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 980
    .line 981
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 982
    .line 983
    const-string v2, "invitesharedir/#"

    .line 984
    .line 985
    const/16 v3, 0x5b

    .line 986
    .line 987
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 988
    .line 989
    .line 990
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 991
    .line 992
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 993
    .line 994
    const-string v2, "invitesharedir/allinvitedir"

    .line 995
    .line 996
    const/16 v3, 0x5c

    .line 997
    .line 998
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 999
    .line 1000
    .line 1001
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1002
    .line 1003
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1004
    .line 1005
    const-string v2, "invitesharedir/allinvitedir/#"

    .line 1006
    .line 1007
    const/16 v3, 0x5d

    .line 1008
    .line 1009
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1010
    .line 1011
    .line 1012
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1013
    .line 1014
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1015
    .line 1016
    const-string v2, "gallery_pictures"

    .line 1017
    .line 1018
    const/16 v3, 0x5e

    .line 1019
    .line 1020
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1021
    .line 1022
    .line 1023
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1024
    .line 1025
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1026
    .line 1027
    const-string v2, "gallery_pictures/#"

    .line 1028
    .line 1029
    const/16 v3, 0x5f

    .line 1030
    .line 1031
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1032
    .line 1033
    .line 1034
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1035
    .line 1036
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1037
    .line 1038
    const-string v2, "qr_code"

    .line 1039
    .line 1040
    const/16 v3, 0x60

    .line 1041
    .line 1042
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1043
    .line 1044
    .line 1045
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1046
    .line 1047
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1048
    .line 1049
    const-string v2, "qr_code/#"

    .line 1050
    .line 1051
    const/16 v3, 0x61

    .line 1052
    .line 1053
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1054
    .line 1055
    .line 1056
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1057
    .line 1058
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1059
    .line 1060
    const-string v2, "signature_contact"

    .line 1061
    .line 1062
    const/16 v3, 0x62

    .line 1063
    .line 1064
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1065
    .line 1066
    .line 1067
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1068
    .line 1069
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1070
    .line 1071
    const-string v2, "signature_contact/#"

    .line 1072
    .line 1073
    const/16 v3, 0x63

    .line 1074
    .line 1075
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1076
    .line 1077
    .line 1078
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1079
    .line 1080
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1081
    .line 1082
    const-string v2, "invited_esign_doc"

    .line 1083
    .line 1084
    const/16 v3, 0x64

    .line 1085
    .line 1086
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1087
    .line 1088
    .line 1089
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1090
    .line 1091
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1092
    .line 1093
    const-string v2, "invited_esign_doc/#"

    .line 1094
    .line 1095
    const/16 v3, 0x65

    .line 1096
    .line 1097
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1098
    .line 1099
    .line 1100
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1101
    .line 1102
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1103
    .line 1104
    const-string v2, "bank_card_journal_page"

    .line 1105
    .line 1106
    const/16 v3, 0x66

    .line 1107
    .line 1108
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1109
    .line 1110
    .line 1111
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 1112
    .line 1113
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 1114
    .line 1115
    const-string v2, "bank_card_journal_page/#"

    .line 1116
    .line 1117
    const/16 v3, 0x67

    .line 1118
    .line 1119
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1120
    .line 1121
    .line 1122
    return-void
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/db/wrap/DBHelper;->Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->beginTransaction()V

    .line 8
    .line 9
    .line 10
    :try_start_0
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-interface {v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->endTransaction()V

    .line 18
    .line 19
    .line 20
    return-object p1

    .line 21
    :catchall_0
    move-exception p1

    .line 22
    invoke-interface {v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->endTransaction()V

    .line 23
    .line 24
    .line 25
    throw p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .locals 1

    .line 1
    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/provider/DocumentProvider;->〇O〇(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .line 1
    const-string v0, "DocumentProvider"

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;-><init>(L〇8〇〇8o/o〇0;)V

    .line 7
    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    :try_start_0
    sget-object v3, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 11
    .line 12
    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 13
    .line 14
    .line 15
    move-result v3

    .line 16
    invoke-direct {p0, p1, v3, p2, v1}, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇0(Landroid/net/Uri;ILjava/lang/String;Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 17
    .line 18
    .line 19
    :try_start_1
    sget-object p2, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 20
    .line 21
    invoke-interface {p2}, Lcom/intsig/camscanner/db/wrap/DBHelper;->Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    iget-object v3, v1, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 26
    .line 27
    iget-object v1, v1, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇o00〇〇Oo:Ljava/lang/String;

    .line 28
    .line 29
    invoke-interface {p2, v3, v1, p3}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 30
    .line 31
    .line 32
    move-result v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 33
    goto :goto_0

    .line 34
    :catch_0
    move-exception p2

    .line 35
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    .line 37
    .line 38
    :goto_0
    if-lez v2, :cond_0

    .line 39
    .line 40
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/provider/DocumentProvider;->〇O8o08O(Landroid/net/Uri;)V

    .line 41
    .line 42
    .line 43
    :cond_0
    return v2

    .line 44
    :catch_1
    move-exception p1

    .line 45
    const-string p2, "delete "

    .line 46
    .line 47
    invoke-static {v0, p2, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 48
    .line 49
    .line 50
    return v2
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x20

    .line 8
    .line 9
    if-eq v0, v1, :cond_1

    .line 10
    .line 11
    const/16 v1, 0x21

    .line 12
    .line 13
    if-eq v0, v1, :cond_0

    .line 14
    .line 15
    packed-switch v0, :pswitch_data_0

    .line 16
    .line 17
    .line 18
    packed-switch v0, :pswitch_data_1

    .line 19
    .line 20
    .line 21
    packed-switch v0, :pswitch_data_2

    .line 22
    .line 23
    .line 24
    packed-switch v0, :pswitch_data_3

    .line 25
    .line 26
    .line 27
    packed-switch v0, :pswitch_data_4

    .line 28
    .line 29
    .line 30
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 31
    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v2, "getType Unknown URL "

    .line 38
    .line 39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    throw v0

    .line 53
    :pswitch_0
    const-string p1, "vnd.android.cursor.item/vnd.intsig.bank_card_journal_page"

    .line 54
    .line 55
    return-object p1

    .line 56
    :pswitch_1
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.bank_card_journal_page"

    .line 57
    .line 58
    return-object p1

    .line 59
    :pswitch_2
    const-string p1, "vnd.android.cursor.item/vnd.intsig.invited_esign_doc"

    .line 60
    .line 61
    return-object p1

    .line 62
    :pswitch_3
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.invited_esign_doc"

    .line 63
    .line 64
    return-object p1

    .line 65
    :pswitch_4
    const-string p1, "vnd.android.cursor.item/vnd.intsig.signature_contact"

    .line 66
    .line 67
    return-object p1

    .line 68
    :pswitch_5
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.signature_contact"

    .line 69
    .line 70
    return-object p1

    .line 71
    :pswitch_6
    const-string p1, "vnd.android.cursor.item/vnd.intsig.qr_code"

    .line 72
    .line 73
    return-object p1

    .line 74
    :pswitch_7
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.qr_code"

    .line 75
    .line 76
    return-object p1

    .line 77
    :pswitch_8
    const-string p1, "vnd.android.cursor.item/vnd.intsig.gallery_pictures"

    .line 78
    .line 79
    return-object p1

    .line 80
    :pswitch_9
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.gallery_pictures"

    .line 81
    .line 82
    return-object p1

    .line 83
    :pswitch_a
    const-string p1, "vnd.android.cursor.item/vnd.intsig.invitedirs"

    .line 84
    .line 85
    return-object p1

    .line 86
    :pswitch_b
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.invitedirs"

    .line 87
    .line 88
    return-object p1

    .line 89
    :pswitch_c
    const-string p1, "vnd.android.cursor.item/vnd.intsig.message_center"

    .line 90
    .line 91
    return-object p1

    .line 92
    :pswitch_d
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.message_center"

    .line 93
    .line 94
    return-object p1

    .line 95
    :pswitch_e
    const-string p1, "vnd.android.cursor.item/vnd.intsig.SyncDeleteStatus"

    .line 96
    .line 97
    return-object p1

    .line 98
    :pswitch_f
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.SyncDeleteStatus"

    .line 99
    .line 100
    return-object p1

    .line 101
    :pswitch_10
    const-string p1, "teamfileinfos/teammembers"

    .line 102
    .line 103
    return-object p1

    .line 104
    :pswitch_11
    const-string p1, "vnd.android.cursor.item/vnd.intsig.teamfileinfos"

    .line 105
    .line 106
    return-object p1

    .line 107
    :pswitch_12
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.teamfileinfos"

    .line 108
    .line 109
    return-object p1

    .line 110
    :pswitch_13
    const-string p1, "vnd.android.cursor.item/vnd.intsig.teammembers"

    .line 111
    .line 112
    return-object p1

    .line 113
    :pswitch_14
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.teammembers"

    .line 114
    .line 115
    return-object p1

    .line 116
    :pswitch_15
    const-string p1, "vnd.android.cursor.item/vnd.intsig.teaminfos"

    .line 117
    .line 118
    return-object p1

    .line 119
    :pswitch_16
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.teaminfos"

    .line 120
    .line 121
    return-object p1

    .line 122
    :pswitch_17
    const-string p1, "vnd.android.cursor.item/vnd.intsig.messagecenters"

    .line 123
    .line 124
    return-object p1

    .line 125
    :pswitch_18
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.messagecenters"

    .line 126
    .line 127
    return-object p1

    .line 128
    :pswitch_19
    const-string p1, "vnd.android.cursor.item/vnd.intsig.dirs"

    .line 129
    .line 130
    return-object p1

    .line 131
    :pswitch_1a
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.dirs"

    .line 132
    .line 133
    return-object p1

    .line 134
    :pswitch_1b
    const-string p1, "vnd.android.cursor.item/vnd.intsig.faxinfo"

    .line 135
    .line 136
    return-object p1

    .line 137
    :pswitch_1c
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.faxinfo"

    .line 138
    .line 139
    return-object p1

    .line 140
    :pswitch_1d
    const-string p1, "vnd.android.cursor.item/vnd.intsig.documentlike"

    .line 141
    .line 142
    return-object p1

    .line 143
    :pswitch_1e
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.documentlike"

    .line 144
    .line 145
    return-object p1

    .line 146
    :pswitch_1f
    const-string p1, "vnd.android.cursor.item/vnd.intsig.collaboratemsg"

    .line 147
    .line 148
    return-object p1

    .line 149
    :pswitch_20
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.collaboratemsg"

    .line 150
    .line 151
    return-object p1

    .line 152
    :pswitch_21
    const-string p1, "vnd.android.cursor.item/vnd.intsig.comments"

    .line 153
    .line 154
    return-object p1

    .line 155
    :pswitch_22
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.comments"

    .line 156
    .line 157
    return-object p1

    .line 158
    :pswitch_23
    const-string p1, "vnd.android.cursor.item/vnd.intsig.collaborators"

    .line 159
    .line 160
    return-object p1

    .line 161
    :pswitch_24
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.collaborators"

    .line 162
    .line 163
    return-object p1

    .line 164
    :pswitch_25
    const-string p1, "vnd.android.cursor.item/vnd.intsig.printtask"

    .line 165
    .line 166
    return-object p1

    .line 167
    :pswitch_26
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.printtask"

    .line 168
    .line 169
    return-object p1

    .line 170
    :pswitch_27
    const-string p1, "vnd.android.cursor.item/vnd.intsig.faxtask"

    .line 171
    .line 172
    return-object p1

    .line 173
    :pswitch_28
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.faxtask"

    .line 174
    .line 175
    return-object p1

    .line 176
    :pswitch_29
    const-string p1, "vnd.android.cursor.item/vnd.intsig.mtag"

    .line 177
    .line 178
    return-object p1

    .line 179
    :pswitch_2a
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.mtag"

    .line 180
    .line 181
    return-object p1

    .line 182
    :pswitch_2b
    const-string p1, "vnd.android.cursor.item/vnd.intsig.uploadstate"

    .line 183
    .line 184
    return-object p1

    .line 185
    :pswitch_2c
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.uploadstate"

    .line 186
    .line 187
    return-object p1

    .line 188
    :pswitch_2d
    const-string p1, "vnd.android.cursor.item/vnd.intsig.account"

    .line 189
    .line 190
    return-object p1

    .line 191
    :pswitch_2e
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.account"

    .line 192
    .line 193
    return-object p1

    .line 194
    :pswitch_2f
    const-string p1, "vnd.android.cursor.item/vnd.intsig.tag"

    .line 195
    .line 196
    return-object p1

    .line 197
    :pswitch_30
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.tag"

    .line 198
    .line 199
    return-object p1

    .line 200
    :pswitch_31
    const-string p1, "vnd.android.cursor.item/vnd.intsig.image"

    .line 201
    .line 202
    return-object p1

    .line 203
    :pswitch_32
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.image"

    .line 204
    .line 205
    return-object p1

    .line 206
    :pswitch_33
    const-string p1, "vnd.android.cursor.item/vnd.intsig.document"

    .line 207
    .line 208
    return-object p1

    .line 209
    :pswitch_34
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.document"

    .line 210
    .line 211
    return-object p1

    .line 212
    :cond_0
    const-string p1, "vnd.android.cursor.item/vnd.intsig.pdfsize"

    .line 213
    .line 214
    return-object p1

    .line 215
    :cond_1
    const-string p1, "vnd.android.cursor.dir/vnd.intsig.pdfsize"

    .line 216
    .line 217
    return-object p1

    .line 218
    nop

    .line 219
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_32
        :pswitch_34
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_34
    .end packed-switch

    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    :pswitch_data_1
    .packed-switch 0x11
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
    .end packed-switch

    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    :pswitch_data_2
    .packed-switch 0x2b
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_32
        :pswitch_31
        :pswitch_32
        :pswitch_31
        :pswitch_34
        :pswitch_33
        :pswitch_34
        :pswitch_33
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_34
    .end packed-switch

    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    :pswitch_data_3
    .packed-switch 0x42
        :pswitch_1a
        :pswitch_19
        :pswitch_1a
        :pswitch_19
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
    .end packed-switch

    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    :pswitch_data_4
    .packed-switch 0x56
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 30

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    const-string v9, "DocumentProvider"

    .line 6
    .line 7
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-static {v2}, Lcom/intsig/camscanner/sync/SyncAccountUtil;->〇080(Landroid/content/Context;)J

    .line 12
    .line 13
    .line 14
    move-result-wide v2

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    new-instance v4, Landroid/content/ContentValues;

    .line 18
    .line 19
    invoke-direct {v4, v1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    .line 24
    .line 25
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 26
    .line 27
    .line 28
    :goto_0
    move-object v10, v4

    .line 29
    const/4 v11, 0x0

    .line 30
    :try_start_0
    sget-object v1, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 31
    .line 32
    invoke-interface {v1}, Lcom/intsig/camscanner/db/wrap/DBHelper;->Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;

    .line 33
    .line 34
    .line 35
    move-result-object v12

    .line 36
    sget-object v1, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 37
    .line 38
    invoke-virtual {v1, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 39
    .line 40
    .line 41
    move-result v13

    .line 42
    const-string v1, ""

    .line 43
    .line 44
    filled-new-array {v1}, [Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v14

    .line 48
    const/4 v15, 0x1

    .line 49
    new-array v8, v15, [Ljava/lang/String;

    .line 50
    .line 51
    new-array v7, v15, [Landroid/net/Uri;

    .line 52
    .line 53
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    move-object/from16 v4, p1

    .line 58
    .line 59
    move-object v5, v10

    .line 60
    move-object v6, v14

    .line 61
    move-object/from16 v16, v7

    .line 62
    .line 63
    move-object v7, v8

    .line 64
    move-object/from16 v17, v8

    .line 65
    .line 66
    move-object/from16 v8, v16

    .line 67
    .line 68
    invoke-static/range {v1 .. v8}, Lcom/intsig/camscanner/provider/DocumentProvider;->〇O00(Landroid/content/Context;JLandroid/net/Uri;Landroid/content/ContentValues;[Ljava/lang/String;[Ljava/lang/String;[Landroid/net/Uri;)V

    .line 69
    .line 70
    .line 71
    const/4 v1, 0x0

    .line 72
    aget-object v2, v14, v1

    .line 73
    .line 74
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 75
    .line 76
    .line 77
    move-result v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 78
    if-nez v2, :cond_2

    .line 79
    .line 80
    sparse-switch v13, :sswitch_data_0

    .line 81
    .line 82
    .line 83
    const/16 v20, 0x0

    .line 84
    .line 85
    const/16 v21, 0x0

    .line 86
    .line 87
    :goto_1
    const/16 v22, 0x0

    .line 88
    .line 89
    :goto_2
    const/16 v23, 0x0

    .line 90
    .line 91
    :goto_3
    const/16 v24, 0x0

    .line 92
    .line 93
    :goto_4
    const/16 v25, 0x0

    .line 94
    .line 95
    :goto_5
    const/16 v26, 0x0

    .line 96
    .line 97
    :goto_6
    const/16 v27, 0x0

    .line 98
    .line 99
    :goto_7
    const/16 v28, 0x0

    .line 100
    .line 101
    :goto_8
    const/16 v29, 0x0

    .line 102
    .line 103
    goto/16 :goto_b

    .line 104
    .line 105
    :sswitch_0
    const/16 v20, 0x0

    .line 106
    .line 107
    const/16 v21, 0x0

    .line 108
    .line 109
    const/16 v22, 0x0

    .line 110
    .line 111
    const/16 v23, 0x0

    .line 112
    .line 113
    const/16 v24, 0x0

    .line 114
    .line 115
    const/16 v25, 0x0

    .line 116
    .line 117
    const/16 v26, 0x0

    .line 118
    .line 119
    const/16 v27, 0x0

    .line 120
    .line 121
    const/16 v28, 0x0

    .line 122
    .line 123
    const/16 v29, 0x1

    .line 124
    .line 125
    goto :goto_b

    .line 126
    :sswitch_1
    const/16 v20, 0x0

    .line 127
    .line 128
    const/16 v21, 0x0

    .line 129
    .line 130
    const/16 v22, 0x0

    .line 131
    .line 132
    const/16 v23, 0x0

    .line 133
    .line 134
    const/16 v24, 0x0

    .line 135
    .line 136
    const/16 v25, 0x0

    .line 137
    .line 138
    const/16 v26, 0x0

    .line 139
    .line 140
    const/16 v27, 0x0

    .line 141
    .line 142
    const/16 v28, 0x1

    .line 143
    .line 144
    goto :goto_8

    .line 145
    :sswitch_2
    const/16 v20, 0x0

    .line 146
    .line 147
    const/16 v21, 0x0

    .line 148
    .line 149
    const/16 v22, 0x0

    .line 150
    .line 151
    const/16 v23, 0x0

    .line 152
    .line 153
    const/16 v24, 0x0

    .line 154
    .line 155
    const/16 v25, 0x0

    .line 156
    .line 157
    const/16 v26, 0x0

    .line 158
    .line 159
    const/16 v27, 0x1

    .line 160
    .line 161
    goto :goto_7

    .line 162
    :sswitch_3
    const/16 v20, 0x0

    .line 163
    .line 164
    const/16 v21, 0x0

    .line 165
    .line 166
    const/16 v22, 0x0

    .line 167
    .line 168
    const/16 v23, 0x0

    .line 169
    .line 170
    const/16 v24, 0x0

    .line 171
    .line 172
    const/16 v25, 0x0

    .line 173
    .line 174
    const/16 v26, 0x1

    .line 175
    .line 176
    goto :goto_6

    .line 177
    :sswitch_4
    const/16 v20, 0x0

    .line 178
    .line 179
    goto :goto_9

    .line 180
    :sswitch_5
    const/16 v20, 0x0

    .line 181
    .line 182
    const/16 v21, 0x0

    .line 183
    .line 184
    const/16 v22, 0x0

    .line 185
    .line 186
    const/16 v23, 0x0

    .line 187
    .line 188
    const/16 v24, 0x0

    .line 189
    .line 190
    const/16 v25, 0x1

    .line 191
    .line 192
    goto :goto_5

    .line 193
    :sswitch_6
    const/16 v20, 0x0

    .line 194
    .line 195
    const/16 v21, 0x0

    .line 196
    .line 197
    const/16 v22, 0x0

    .line 198
    .line 199
    const/16 v23, 0x0

    .line 200
    .line 201
    const/16 v24, 0x1

    .line 202
    .line 203
    goto :goto_4

    .line 204
    :sswitch_7
    const/16 v20, 0x1

    .line 205
    .line 206
    :goto_9
    const/16 v21, 0x1

    .line 207
    .line 208
    goto :goto_1

    .line 209
    :sswitch_8
    const/16 v20, 0x0

    .line 210
    .line 211
    const/16 v21, 0x0

    .line 212
    .line 213
    goto :goto_a

    .line 214
    :sswitch_9
    const/16 v20, 0x0

    .line 215
    .line 216
    const/16 v21, 0x0

    .line 217
    .line 218
    const/16 v22, 0x0

    .line 219
    .line 220
    const/16 v23, 0x1

    .line 221
    .line 222
    goto/16 :goto_3

    .line 223
    .line 224
    :sswitch_a
    const/16 v20, 0x1

    .line 225
    .line 226
    const/16 v21, 0x1

    .line 227
    .line 228
    :goto_a
    const/16 v22, 0x1

    .line 229
    .line 230
    goto/16 :goto_2

    .line 231
    .line 232
    :goto_b
    const-wide/16 v2, 0x0

    .line 233
    .line 234
    :try_start_1
    aget-object v0, v14, v1

    .line 235
    .line 236
    aget-object v4, v17, v1

    .line 237
    .line 238
    invoke-interface {v12, v0, v4, v10}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 239
    .line 240
    .line 241
    move-result-wide v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 242
    goto :goto_c

    .line 243
    :catch_0
    move-exception v0

    .line 244
    :try_start_2
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 245
    .line 246
    .line 247
    move-wide v4, v2

    .line 248
    :goto_c
    cmp-long v0, v4, v2

    .line 249
    .line 250
    if-lez v0, :cond_1

    .line 251
    .line 252
    aget-object v0, v16, v1

    .line 253
    .line 254
    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 255
    .line 256
    .line 257
    move-result-object v11

    .line 258
    :cond_1
    move-object/from16 v18, p0

    .line 259
    .line 260
    move-object/from16 v19, v11

    .line 261
    .line 262
    invoke-direct/range {v18 .. v29}, Lcom/intsig/camscanner/provider/DocumentProvider;->OO0o〇〇(Landroid/net/Uri;ZZZZZZZZZZ)V

    .line 263
    .line 264
    .line 265
    goto :goto_d

    .line 266
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 267
    .line 268
    new-instance v2, Ljava/lang/StringBuilder;

    .line 269
    .line 270
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 271
    .line 272
    .line 273
    const-string v3, "insert Unknown URL "

    .line 274
    .line 275
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    .line 277
    .line 278
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 279
    .line 280
    .line 281
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 282
    .line 283
    .line 284
    move-result-object v0

    .line 285
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 286
    .line 287
    .line 288
    throw v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 289
    :catch_1
    move-exception v0

    .line 290
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 291
    .line 292
    .line 293
    :goto_d
    return-object v11

    .line 294
    nop

    .line 295
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_a
        0x3 -> :sswitch_9
        0xd -> :sswitch_8
        0x17 -> :sswitch_a
        0x18 -> :sswitch_9
        0x31 -> :sswitch_9
        0x35 -> :sswitch_a
        0x42 -> :sswitch_7
        0x44 -> :sswitch_7
        0x46 -> :sswitch_7
        0x48 -> :sswitch_6
        0x4a -> :sswitch_6
        0x58 -> :sswitch_5
        0x5a -> :sswitch_4
        0x5c -> :sswitch_4
        0x5e -> :sswitch_3
        0x62 -> :sswitch_2
        0x64 -> :sswitch_1
        0x66 -> :sswitch_0
    .end sparse-switch
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public onCreate()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/db/wrap/DBHelperFactory;->〇080:Lcom/intsig/camscanner/db/wrap/DBHelperFactory;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/db/wrap/DBHelperFactory;->〇080(Landroid/content/Context;)Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    sput-object v0, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14

    .line 1
    move-object v0, p1

    .line 2
    const-string v1, "DocumentProvider"

    .line 3
    .line 4
    sget-object v2, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 5
    .line 6
    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    new-instance v3, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;

    .line 11
    .line 12
    const/4 v4, 0x0

    .line 13
    invoke-direct {v3, v4}, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;-><init>(L〇8〇〇8o/o〇0;)V

    .line 14
    .line 15
    .line 16
    move-object v5, p0

    .line 17
    move-object/from16 v6, p3

    .line 18
    .line 19
    :try_start_0
    invoke-direct {p0, p1, v2, v6, v3}, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇0(Landroid/net/Uri;ILjava/lang/String;Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 20
    .line 21
    .line 22
    :try_start_1
    sget-object v2, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 23
    .line 24
    invoke-interface {v2}, Lcom/intsig/camscanner/db/wrap/DBHelper;->Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;

    .line 25
    .line 26
    .line 27
    move-result-object v6
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 28
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    const-string v7, "images/update_doc"

    .line 33
    .line 34
    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    if-eqz v2, :cond_0

    .line 39
    .line 40
    const-string v2, "document_id"

    .line 41
    .line 42
    move-object v11, v2

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    move-object v11, v4

    .line 45
    :goto_0
    :try_start_2
    iget-object v7, v3, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 46
    .line 47
    iget-object v9, v3, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇o00〇〇Oo:Ljava/lang/String;

    .line 48
    .line 49
    const/4 v12, 0x0

    .line 50
    move-object/from16 v8, p2

    .line 51
    .line 52
    move-object/from16 v10, p4

    .line 53
    .line 54
    move-object/from16 v13, p5

    .line 55
    .line 56
    invoke-interface/range {v6 .. v13}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    invoke-interface {v4, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 69
    .line 70
    .line 71
    goto :goto_1

    .line 72
    :catch_0
    move-exception v0

    .line 73
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 74
    .line 75
    .line 76
    :goto_1
    return-object v4

    .line 77
    :catch_1
    move-exception v0

    .line 78
    const-string v2, "RuntimeException"

    .line 79
    .line 80
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 81
    .line 82
    .line 83
    return-object v4

    .line 84
    :catch_2
    move-exception v0

    .line 85
    move-object v2, v0

    .line 86
    const-string v0, "query "

    .line 87
    .line 88
    invoke-static {v1, v0, v2}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 89
    .line 90
    .line 91
    return-object v4
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .line 1
    const-string v0, "DocumentProvider"

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/provider/DocumentProvider;->〇OOo8〇0:Landroid/content/UriMatcher;

    .line 4
    .line 5
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    new-instance v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;

    .line 10
    .line 11
    const/4 v3, 0x0

    .line 12
    invoke-direct {v2, v3}, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;-><init>(L〇8〇〇8o/o〇0;)V

    .line 13
    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    :try_start_0
    invoke-direct {p0, p1, v1, p3, v2}, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇0(Landroid/net/Uri;ILjava/lang/String;Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 17
    .line 18
    .line 19
    const/16 p3, 0x4c

    .line 20
    .line 21
    const-string v4, "title_sort_index"

    .line 22
    .line 23
    const-string v5, "title"

    .line 24
    .line 25
    if-eq v1, p3, :cond_2

    .line 26
    .line 27
    const/16 p3, 0x4d

    .line 28
    .line 29
    if-eq v1, p3, :cond_2

    .line 30
    .line 31
    const-string p3, "last_modified"

    .line 32
    .line 33
    packed-switch v1, :pswitch_data_0

    .line 34
    .line 35
    .line 36
    packed-switch v1, :pswitch_data_1

    .line 37
    .line 38
    .line 39
    packed-switch v1, :pswitch_data_2

    .line 40
    .line 41
    .line 42
    packed-switch v1, :pswitch_data_3

    .line 43
    .line 44
    .line 45
    packed-switch v1, :pswitch_data_4

    .line 46
    .line 47
    .line 48
    goto/16 :goto_0

    .line 49
    .line 50
    :pswitch_0
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 51
    .line 52
    .line 53
    move-result p3

    .line 54
    if-eqz p3, :cond_3

    .line 55
    .line 56
    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 57
    .line 58
    .line 59
    move-result p3

    .line 60
    if-nez p3, :cond_3

    .line 61
    .line 62
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p3

    .line 66
    invoke-static {p3}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p3

    .line 70
    invoke-virtual {p2, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    goto/16 :goto_0

    .line 74
    .line 75
    :pswitch_1
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    if-nez v4, :cond_0

    .line 80
    .line 81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 82
    .line 83
    .line 84
    move-result-wide v6

    .line 85
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 86
    .line 87
    .line 88
    move-result-object v4

    .line 89
    invoke-virtual {p2, p3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 90
    .line 91
    .line 92
    :cond_0
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 93
    .line 94
    .line 95
    move-result p3

    .line 96
    if-eqz p3, :cond_3

    .line 97
    .line 98
    const-string p3, "title_pinyin"

    .line 99
    .line 100
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 101
    .line 102
    .line 103
    move-result v4

    .line 104
    if-nez v4, :cond_3

    .line 105
    .line 106
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v4

    .line 110
    invoke-static {v4}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v4

    .line 114
    invoke-virtual {p2, p3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :pswitch_2
    const/16 v4, 0x2f

    .line 119
    .line 120
    if-eq v1, v4, :cond_3

    .line 121
    .line 122
    const/16 v4, 0x30

    .line 123
    .line 124
    if-eq v1, v4, :cond_3

    .line 125
    .line 126
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 127
    .line 128
    .line 129
    move-result v4

    .line 130
    if-nez v4, :cond_3

    .line 131
    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 133
    .line 134
    .line 135
    move-result-wide v4

    .line 136
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 137
    .line 138
    .line 139
    move-result-object v4

    .line 140
    invoke-virtual {p2, p3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 141
    .line 142
    .line 143
    goto :goto_0

    .line 144
    :pswitch_3
    const/16 p3, 0x33

    .line 145
    .line 146
    if-eq v1, p3, :cond_1

    .line 147
    .line 148
    const/16 p3, 0x34

    .line 149
    .line 150
    if-eq v1, p3, :cond_1

    .line 151
    .line 152
    const-string p3, "modified"

    .line 153
    .line 154
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 155
    .line 156
    .line 157
    move-result v6

    .line 158
    if-nez v6, :cond_1

    .line 159
    .line 160
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/provider/DocumentProvider;->〇8o8o〇(Landroid/content/ContentValues;)Z

    .line 161
    .line 162
    .line 163
    move-result v6

    .line 164
    if-eqz v6, :cond_1

    .line 165
    .line 166
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 167
    .line 168
    .line 169
    move-result-wide v6

    .line 170
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 171
    .line 172
    .line 173
    move-result-object v6

    .line 174
    invoke-virtual {p2, p3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 175
    .line 176
    .line 177
    :cond_1
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 178
    .line 179
    .line 180
    move-result p3

    .line 181
    if-eqz p3, :cond_3

    .line 182
    .line 183
    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 184
    .line 185
    .line 186
    move-result p3

    .line 187
    if-nez p3, :cond_3

    .line 188
    .line 189
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object p3

    .line 193
    invoke-static {p3}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object p3

    .line 197
    invoke-virtual {p2, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    goto :goto_0

    .line 201
    :cond_2
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 202
    .line 203
    .line 204
    move-result p3

    .line 205
    if-eqz p3, :cond_3

    .line 206
    .line 207
    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    .line 208
    .line 209
    .line 210
    move-result p3

    .line 211
    if-nez p3, :cond_3

    .line 212
    .line 213
    invoke-virtual {p2, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    .line 214
    .line 215
    .line 216
    move-result-object p3

    .line 217
    invoke-static {p3}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object p3

    .line 221
    invoke-virtual {p2, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    :cond_3
    :goto_0
    :try_start_1
    sget-object p3, Lcom/intsig/camscanner/provider/DocumentProvider;->o〇00O:Lcom/intsig/camscanner/db/wrap/DBHelper;

    .line 225
    .line 226
    invoke-interface {p3}, Lcom/intsig/camscanner/db/wrap/DBHelper;->Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;

    .line 227
    .line 228
    .line 229
    move-result-object p3

    .line 230
    iget-object v4, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇080:Ljava/lang/String;

    .line 231
    .line 232
    iget-object v2, v2, Lcom/intsig/camscanner/provider/DocumentProvider$GetTableAndWhereOutParameter;->〇o00〇〇Oo:Ljava/lang/String;

    .line 233
    .line 234
    invoke-interface {p3, v4, p2, v2, p4}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 235
    .line 236
    .line 237
    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 238
    goto :goto_1

    .line 239
    :catch_0
    move-exception p3

    .line 240
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 241
    .line 242
    .line 243
    :goto_1
    if-lez v3, :cond_4

    .line 244
    .line 245
    int-to-long p3, v1

    .line 246
    new-instance v0, Landroid/content/ContentValues;

    .line 247
    .line 248
    invoke-direct {v0, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 249
    .line 250
    .line 251
    invoke-direct {p0, p1, p3, p4, v0}, Lcom/intsig/camscanner/provider/DocumentProvider;->Oooo8o0〇(Landroid/net/Uri;JLandroid/content/ContentValues;)V

    .line 252
    .line 253
    .line 254
    :cond_4
    return v3

    .line 255
    :catch_1
    move-exception p1

    .line 256
    const-string p2, "update "

    .line 257
    .line 258
    invoke-static {v0, p2, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 259
    .line 260
    .line 261
    return v3

    .line 262
    nop

    .line 263
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    :pswitch_data_1
    .packed-switch 0x17
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    :pswitch_data_2
    .packed-switch 0x25
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    :pswitch_data_3
    .packed-switch 0x2f
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    :pswitch_data_4
    .packed-switch 0x42
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

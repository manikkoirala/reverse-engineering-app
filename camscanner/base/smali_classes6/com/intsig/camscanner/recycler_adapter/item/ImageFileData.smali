.class public Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;
.super Ljava/lang/Object;
.source "ImageFileData.java"


# instance fields
.field private O8:I

.field private Oo08:J

.field private 〇080:Ljava/lang/String;

.field private 〇o00〇〇Oo:J

.field private 〇o〇:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->Oo08()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Oo08()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    iput-wide v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o00〇〇Oo:J

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    aget v1, v0, v1

    .line 27
    .line 28
    iput v1, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇:I

    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    aget v0, v0, v1

    .line 32
    .line 33
    iput v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->O8:I

    .line 34
    .line 35
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 36
    .line 37
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O00(Ljava/lang/String;)J

    .line 38
    .line 39
    .line 40
    move-result-wide v0

    .line 41
    iput-wide v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->Oo08:J

    .line 42
    .line 43
    :cond_1
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    if-eqz p1, :cond_3

    .line 7
    .line 8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    if-eq v2, v3, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 20
    .line 21
    iget-wide v2, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o00〇〇Oo:J

    .line 22
    .line 23
    iget-wide v4, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o00〇〇Oo:J

    .line 24
    .line 25
    cmp-long v6, v2, v4

    .line 26
    .line 27
    if-nez v6, :cond_2

    .line 28
    .line 29
    iget v2, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇:I

    .line 30
    .line 31
    iget v3, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇:I

    .line 32
    .line 33
    if-ne v2, v3, :cond_2

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->O8:I

    .line 36
    .line 37
    iget v3, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->O8:I

    .line 38
    .line 39
    if-ne v2, v3, :cond_2

    .line 40
    .line 41
    iget-wide v2, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->Oo08:J

    .line 42
    .line 43
    iget-wide v4, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->Oo08:J

    .line 44
    .line 45
    cmp-long v6, v2, v4

    .line 46
    .line 47
    if-nez v6, :cond_2

    .line 48
    .line 49
    iget-object v2, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 50
    .line 51
    iget-object p1, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 52
    .line 53
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    if-eqz p1, :cond_2

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_2
    const/4 v0, 0x0

    .line 61
    :goto_0
    return v0

    .line 62
    :cond_3
    :goto_1
    return v1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public hashCode()I
    .locals 3

    .line 1
    const/4 v0, 0x5

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    iget-wide v1, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o00〇〇Oo:J

    .line 10
    .line 11
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const/4 v2, 0x1

    .line 16
    aput-object v1, v0, v2

    .line 17
    .line 18
    iget v1, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇:I

    .line 19
    .line 20
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const/4 v2, 0x2

    .line 25
    aput-object v1, v0, v2

    .line 26
    .line 27
    iget v1, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->O8:I

    .line 28
    .line 29
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    const/4 v2, 0x3

    .line 34
    aput-object v1, v0, v2

    .line 35
    .line 36
    iget-wide v1, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->Oo08:J

    .line 37
    .line 38
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    const/4 v2, 0x4

    .line 43
    aput-object v1, v0, v2

    .line 44
    .line 45
    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    return v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇0(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 4
    .line 5
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    iget-wide v2, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o00〇〇Oo:J

    .line 14
    .line 15
    iget-wide v4, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o00〇〇Oo:J

    .line 16
    .line 17
    cmp-long v0, v2, v4

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    return v1

    .line 22
    :cond_1
    iget v0, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇:I

    .line 23
    .line 24
    iget v2, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇:I

    .line 25
    .line 26
    if-eq v0, v2, :cond_2

    .line 27
    .line 28
    return v1

    .line 29
    :cond_2
    iget p1, p1, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->O8:I

    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->O8:I

    .line 32
    .line 33
    if-ne p1, v0, :cond_3

    .line 34
    .line 35
    const/4 v1, 0x1

    .line 36
    :cond_3
    return v1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇o00〇〇Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

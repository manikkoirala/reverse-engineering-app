.class public Lcom/intsig/camscanner/loadimage/PageImage;
.super Ljava/lang/Object;
.source "PageImage.java"


# static fields
.field private static OOO〇O0:Landroid/graphics/Bitmap;


# instance fields
.field O8:I

.field private volatile O8ooOoo〇:Z

.field private OO0o〇〇:Ljava/lang/String;

.field private OO0o〇〇〇〇0:Ljava/lang/String;

.field Oo08:Ljava/lang/String;

.field OoO8:Z

.field private Oooo8o0〇:I

.field private volatile O〇8O8〇008:Z

.field private transient o800o8O:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

.field oO80:Ljava/lang/String;

.field private transient oo88o8O:I

.field o〇0:Ljava/lang/String;

.field private o〇O8〇〇o:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

.field private o〇〇0〇:I

.field private volatile 〇00:F

.field private 〇0000OOO:I

.field private 〇080:I

.field private 〇0〇O0088o:J

.field private 〇80〇808〇O:Ljava/lang/String;

.field private 〇8o8o〇:Ljava/lang/String;

.field private 〇O00:I

.field private transient 〇O888o0o:Ljava/lang/String;

.field private 〇O8o08O:Ljava/lang/String;

.field private 〇O〇:Z

.field private 〇o00〇〇Oo:I

.field private volatile 〇oOO8O8:Z

.field private 〇oo〇:I

.field 〇o〇:J

.field private 〇〇808〇:Ljava/lang/String;

.field 〇〇888:Ljava/lang/String;

.field private 〇〇8O0〇8:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIZ)V
    .locals 3

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 35
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 36
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o00〇〇Oo:I

    .line 37
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇:Z

    .line 38
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O00:I

    const/4 v1, 0x0

    .line 39
    iput-object v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇8O0〇8:Ljava/lang/String;

    const-wide/16 v1, 0x0

    .line 40
    iput-wide v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0〇O0088o:J

    const/high16 v1, -0x40800000    # -1.0f

    .line 41
    iput v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇00:F

    .line 42
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O〇8O8〇008:Z

    .line 43
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇:Z

    .line 44
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8:Z

    const v0, 0x7f081093

    .line 45
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0000OOO:I

    const/high16 v0, 0x42780000    # 62.0f

    .line 46
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇〇0〇:I

    .line 47
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8:I

    .line 48
    iput-object p2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇0:Ljava/lang/String;

    .line 50
    iput-object p4, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 51
    iput-object p5, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oO80:Ljava/lang/String;

    .line 52
    iput-wide p6, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o〇:J

    .line 53
    invoke-static {p2}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 54
    iput-boolean p9, p0, Lcom/intsig/camscanner/loadimage/PageImage;->OoO8:Z

    .line 55
    iput p8, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JIZILjava/lang/String;)V
    .locals 3

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 57
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 58
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o00〇〇Oo:I

    .line 59
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇:Z

    .line 60
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O00:I

    const/4 v1, 0x0

    .line 61
    iput-object v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇8O0〇8:Ljava/lang/String;

    const-wide/16 v1, 0x0

    .line 62
    iput-wide v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0〇O0088o:J

    const/high16 v1, -0x40800000    # -1.0f

    .line 63
    iput v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇00:F

    .line 64
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O〇8O8〇008:Z

    .line 65
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇:Z

    .line 66
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8:Z

    const v0, 0x7f081093

    .line 67
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0000OOO:I

    const/high16 v0, 0x42780000    # 62.0f

    .line 68
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇〇0〇:I

    .line 69
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8:I

    .line 70
    iput-object p2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 71
    iput-object p3, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇0:Ljava/lang/String;

    .line 72
    iput-object p4, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 73
    iput-object p5, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oO80:Ljava/lang/String;

    .line 74
    iput-wide p7, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o〇:J

    .line 75
    invoke-static {p2}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 76
    iput-boolean p10, p0, Lcom/intsig/camscanner/loadimage/PageImage;->OoO8:Z

    .line 77
    iput p9, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇:I

    .line 78
    iput-object p6, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 79
    iput p11, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O00:I

    .line 80
    iput-object p12, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇8O0〇8:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 3
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o00〇〇Oo:I

    .line 4
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇:Z

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O00:I

    const/4 v1, 0x0

    .line 6
    iput-object v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇8O0〇8:Ljava/lang/String;

    const-wide/16 v1, 0x0

    .line 7
    iput-wide v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0〇O0088o:J

    const/high16 v1, -0x40800000    # -1.0f

    .line 8
    iput v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇00:F

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O〇8O8〇008:Z

    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇:Z

    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8:Z

    const v0, 0x7f081093

    .line 12
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0000OOO:I

    const/high16 v0, 0x42780000    # 62.0f

    .line 13
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇〇0〇:I

    .line 14
    iput-wide p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o〇:J

    .line 15
    iput-object p3, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O8o08O:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 18
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o00〇〇Oo:I

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇:Z

    .line 20
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O00:I

    const/4 v1, 0x0

    .line 21
    iput-object v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇8O0〇8:Ljava/lang/String;

    const-wide/16 v1, 0x0

    .line 22
    iput-wide v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0〇O0088o:J

    const/high16 v1, -0x40800000    # -1.0f

    .line 23
    iput v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇00:F

    .line 24
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O〇8O8〇008:Z

    .line 25
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇:Z

    .line 26
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8:Z

    const v0, 0x7f081093

    .line 27
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0000OOO:I

    const/high16 v0, 0x42780000    # 62.0f

    .line 28
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇〇0〇:I

    .line 29
    iput-wide p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o〇:J

    .line 30
    iput-object p3, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oO80:Ljava/lang/String;

    .line 33
    iput-object p6, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O8o08O:Ljava/lang/String;

    return-void
.end method

.method public static OOO〇O0(Landroid/content/res/Resources;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/loadimage/PageImage;->OOO〇O0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    .line 6
    .line 7
    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 8
    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 12
    .line 13
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .line 14
    .line 15
    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 16
    .line 17
    const v1, 0x7f08040a

    .line 18
    .line 19
    .line 20
    invoke-static {p0, v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 21
    .line 22
    .line 23
    move-result-object p0

    .line 24
    sput-object p0, Lcom/intsig/camscanner/loadimage/PageImage;->OOO〇O0:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception p0

    .line 28
    const-string v0, "PageImage"

    .line 29
    .line 30
    const-string v1, "OutOfMemoryError"

    .line 31
    .line 32
    invoke-static {v0, v1, p0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    .line 34
    .line 35
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 36
    .line 37
    .line 38
    const/4 p0, 0x0

    .line 39
    sput-object p0, Lcom/intsig/camscanner/loadimage/PageImage;->OOO〇O0:Landroid/graphics/Bitmap;

    .line 40
    .line 41
    :cond_0
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static 〇o00〇〇Oo()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/loadimage/PageImage;->OOO〇O0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->OOo0O(Landroid/graphics/Bitmap;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    sput-object v0, Lcom/intsig/camscanner/loadimage/PageImage;->OOO〇O0:Landroid/graphics/Bitmap;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O000(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O08000(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8(FILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    .locals 2

    .line 1
    if-eqz p4, :cond_0

    .line 2
    .line 3
    sget-object p4, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 4
    .line 5
    invoke-virtual {p4}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 6
    .line 7
    .line 8
    move-result p4

    .line 9
    if-eqz p4, :cond_0

    .line 10
    .line 11
    iget-object p4, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 12
    .line 13
    invoke-static {p4}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    move-result p4

    .line 17
    if-eqz p4, :cond_0

    .line 18
    .line 19
    iget-object p4, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object p4, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 23
    .line 24
    :goto_0
    float-to-int p1, p1

    .line 25
    invoke-static {p4, p1, p2, p3}, Lcom/intsig/camscanner/util/Util;->OOO8o〇〇(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    if-eqz p1, :cond_1

    .line 30
    .line 31
    iget p2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 32
    .line 33
    if-eqz p2, :cond_1

    .line 34
    .line 35
    const/16 p3, 0x50

    .line 36
    .line 37
    const/4 v0, 0x0

    .line 38
    const/high16 v1, 0x3f800000    # 1.0f

    .line 39
    .line 40
    invoke-static {p4, p2, v1, p3, v0}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 41
    .line 42
    .line 43
    move-result p2

    .line 44
    new-instance p3, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string v0, "image path: "

    .line 50
    .line 51
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    const-string p4, ",  rotation:"

    .line 58
    .line 59
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    iget p4, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 63
    .line 64
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    const-string p4, " result:"

    .line 68
    .line 69
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object p2

    .line 79
    const-string p3, "PageImage"

    .line 80
    .line 81
    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    const/4 p2, 0x0

    .line 85
    iput p2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 86
    .line 87
    :cond_1
    return-object p1
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public O8ooOoo〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8〇o(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇00:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO0o〇〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oo88o8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇〇〇0()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇00:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O888o0o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo8Oo00oo(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OoO8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Ooo()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->Ooo8〇〇(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/loadimage/PageImage;->OOO〇O0:Landroid/graphics/Bitmap;

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :cond_0
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇8O8〇008()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇O〇oO(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇0000OOO:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    const/4 v1, 0x0

    .line 6
    if-eqz p1, :cond_3

    .line 7
    .line 8
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    if-eq v2, v3, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 20
    .line 21
    iget v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 22
    .line 23
    iget v3, p1, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 24
    .line 25
    if-ne v2, v3, :cond_2

    .line 26
    .line 27
    iget-wide v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o〇:J

    .line 28
    .line 29
    iget-wide v4, p1, Lcom/intsig/camscanner/loadimage/PageImage;->〇o〇:J

    .line 30
    .line 31
    cmp-long v6, v2, v4

    .line 32
    .line 33
    if-nez v6, :cond_2

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8:I

    .line 36
    .line 37
    iget v3, p1, Lcom/intsig/camscanner/loadimage/PageImage;->O8:I

    .line 38
    .line 39
    if-ne v2, v3, :cond_2

    .line 40
    .line 41
    iget v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oo88o8O:I

    .line 42
    .line 43
    iget v3, p1, Lcom/intsig/camscanner/loadimage/PageImage;->oo88o8O:I

    .line 44
    .line 45
    if-ne v2, v3, :cond_2

    .line 46
    .line 47
    iget-object v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 48
    .line 49
    iget-object v3, p1, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 50
    .line 51
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    if-eqz v2, :cond_2

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 58
    .line 59
    iget-object v3, p1, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 60
    .line 61
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    if-eqz v2, :cond_2

    .line 66
    .line 67
    iget-object v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oO80:Ljava/lang/String;

    .line 68
    .line 69
    iget-object v3, p1, Lcom/intsig/camscanner/loadimage/PageImage;->oO80:Ljava/lang/String;

    .line 70
    .line 71
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    if-eqz v2, :cond_2

    .line 76
    .line 77
    invoke-virtual {p0}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 86
    .line 87
    .line 88
    move-result v2

    .line 89
    if-eqz v2, :cond_2

    .line 90
    .line 91
    iget v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oo〇:I

    .line 92
    .line 93
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    iget v3, p1, Lcom/intsig/camscanner/loadimage/PageImage;->〇oo〇:I

    .line 98
    .line 99
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 100
    .line 101
    .line 102
    move-result-object v3

    .line 103
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    if-eqz v2, :cond_2

    .line 108
    .line 109
    invoke-virtual {p0}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 110
    .line 111
    .line 112
    move-result-object v2

    .line 113
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    if-eqz p1, :cond_2

    .line 122
    .line 123
    goto :goto_0

    .line 124
    :cond_2
    const/4 v0, 0x0

    .line 125
    :goto_0
    return v0

    .line 126
    :cond_3
    :goto_1
    return v1
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public o0ooO(I)V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;-><init>(I)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇O8〇〇o:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o8(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oo88o8O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o〇:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8oO〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇〇0〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO(Ljava/lang/String;)Lcom/intsig/camscanner/loadimage/PageImage;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇80〇808〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO00OOO(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oo〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO80()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 10
    .line 11
    return-object v0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 21
    .line 22
    return-object v0

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oO80:Ljava/lang/String;

    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oo88o8O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->OoO8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oo〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->OO0o〇〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇0()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇8O0〇8:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0OOo〇0(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->OoO8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇8oOO88(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O〇8O8〇008:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇O8〇〇o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O〇8O8〇008:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇〇0〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O888o0o:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇00()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇:Z

    .line 12
    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0000OOO(Z)Lcom/intsig/camscanner/loadimage/RotateBitmap;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/loadimage/PageImage;->Ooo()Landroid/graphics/Bitmap;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 6
    .line 7
    iget v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 8
    .line 9
    invoke-direct {v1, v0, v2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;I)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 14
    .line 15
    const/16 v2, 0x5a

    .line 16
    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    rsub-int/lit8 p1, v2, 0x0

    .line 20
    .line 21
    add-int/lit16 p1, p1, 0x168

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    add-int/lit8 p1, v0, 0x5a

    .line 25
    .line 26
    :goto_0
    rem-int/lit16 p1, p1, 0x168

    .line 27
    .line 28
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 29
    .line 30
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->oO80(I)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 36
    .line 37
    const/high16 v3, 0x3f800000    # 1.0f

    .line 38
    .line 39
    const/16 v4, 0x50

    .line 40
    .line 41
    const/4 v5, 0x0

    .line 42
    invoke-static {p1, v2, v3, v4, v5}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 47
    .line 48
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    if-eqz v2, :cond_1

    .line 53
    .line 54
    iget-object v2, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 55
    .line 56
    iget v6, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 57
    .line 58
    invoke-static {v2, v6, v3, v4, v5}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    goto :goto_1

    .line 63
    :cond_1
    const/16 v2, -0x400

    .line 64
    .line 65
    :goto_1
    iput v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 66
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    .line 68
    .line 69
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    .line 71
    .line 72
    const-string v3, " after rotateimage"

    .line 73
    .line 74
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    iget-object v3, p0, Lcom/intsig/camscanner/loadimage/PageImage;->Oo08:Ljava/lang/String;

    .line 78
    .line 79
    invoke-static {v3}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string v3, " scaleImage result="

    .line 87
    .line 88
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string p1, "; resultPaper="

    .line 95
    .line 96
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    const-string v0, "PageImage"

    .line 107
    .line 108
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    return-object v1
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇00〇8(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O00:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇08O8o〇0(Ljava/lang/String;)Lcom/intsig/camscanner/loadimage/PageImage;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O8o08O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇0〇O0088o()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oo〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇80()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇888:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->OO0o〇〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O00:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8〇0〇o〇O(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oO80:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O00()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇80〇808〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O888o0o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8o08O()Lcom/intsig/camscanner/demoire/ImageQualityHelper;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇O8〇〇o:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 6
    .line 7
    const/4 v1, -0x1

    .line 8
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/demoire/ImageQualityHelper;-><init>(I)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇O8〇〇o:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o〇O8〇〇o:Lcom/intsig/camscanner/demoire/ImageQualityHelper;

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O8o08O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O〇80o08O(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇:Z

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v1, "error case, try to set mIsShowingRawTrimPaper to ["

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string p1, "] but mTrimmedPaper="

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇〇808〇:Ljava/lang/String;

    .line 31
    .line 32
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string p1, "; doesn\'t exist"

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    const-string v0, "PageImage"

    .line 45
    .line 46
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇oOO8O8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->oO80:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇oo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇(FILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/loadimage/PageImage;->O8(FILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 3
    .line 4
    .line 5
    move-result-object p1

    .line 6
    return-object p1
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public 〇〇0o(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇808〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()I
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "rotation: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "PageImage"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇080:I

    .line 26
    .line 27
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇8O0〇8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/PageImage;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇〇0〇〇0(Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)V
    .locals 0
    .param p1    # Lcom/intsig/camscanner/pic2word/lr/LrImageJson;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O:Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

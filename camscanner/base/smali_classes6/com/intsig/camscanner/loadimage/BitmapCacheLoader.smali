.class public Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;
.super Landroid/os/HandlerThread;
.source "BitmapCacheLoader.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;,
        Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$BitmapLoadOperation;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/HandlerThread;",
        "Landroid/os/Handler$Callback;"
    }
.end annotation


# static fields
.field private static 〇0O:[I


# instance fields
.field private O8o08O8O:Landroid/os/Handler;

.field private OO:Z

.field private o0:Landroidx/collection/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LruCache<",
            "TK;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private o〇00O:Landroid/os/Handler;

.field private final 〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lcom/intsig/camscanner/loadimage/BitmapCacheLoader<",
            "TK;TT;>.AsyncBitmapUtil;>;"
        }
    .end annotation
.end field

.field private 〇08O〇00〇o:Z

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇0O:[I

    .line 8
    .line 9
    return-void

    .line 10
    nop

    .line 11
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(I)V
    .locals 2

    .line 1
    const-string v0, "BitmapCacheLoader"

    .line 2
    .line 3
    const/16 v1, 0xa

    .line 4
    .line 5
    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->OO:Z

    .line 10
    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇08O〇00〇o:Z

    .line 12
    .line 13
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 19
    .line 20
    iput p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇OOo8〇0:I

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private o〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o〇00O:Landroid/os/Handler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Ljava/lang/Thread;->start()V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇08O〇00〇o:Z

    .line 10
    .line 11
    new-instance v0, Landroid/os/Handler;

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o〇00O:Landroid/os/Handler;

    .line 21
    .line 22
    new-instance v0, Landroid/os/Handler;

    .line 23
    .line 24
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->O8o08O8O:Landroid/os/Handler;

    .line 32
    .line 33
    :cond_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;)Landroidx/collection/LruCache;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8o8o〇()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->OO:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->OO:Z

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o〇0()V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o〇00O:Landroid/os/Handler;

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇o〇(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇o〇(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 5
    .line 6
    if-nez v0, :cond_1

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$1;

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇OOo8〇0:I

    .line 11
    .line 12
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$1;-><init>(Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;I)V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 16
    .line 17
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 18
    .line 19
    invoke-virtual {v0, p1, p2}, Landroidx/collection/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇〇888()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;

    .line 8
    .line 9
    :goto_0
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-boolean v1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇08O〇00〇o:Z

    .line 12
    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;->〇o〇()V

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->O8o08O8O:Landroid/os/Handler;

    .line 19
    .line 20
    const/4 v2, 0x2

    .line 21
    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    check-cast v0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/collection/LruCache;->evictAll()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 9
    .line 10
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇80〇808〇O()V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇〇〇0(Ljava/lang/Object;Landroid/widget/ImageView;Ljava/lang/Object;Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$BitmapLoadOperation;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Landroid/widget/ImageView;",
            "TT;",
            "Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$BitmapLoadOperation<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_6

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    goto :goto_1

    .line 6
    :cond_0
    const v0, 0x7f1304ed

    .line 7
    .line 8
    .line 9
    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    instance-of v2, v1, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;

    .line 16
    .line 17
    if-eqz v2, :cond_1

    .line 18
    .line 19
    check-cast v1, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;->〇o00〇〇Oo()V

    .line 22
    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 25
    .line 26
    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 30
    .line 31
    const/4 v2, 0x0

    .line 32
    if-nez v1, :cond_2

    .line 33
    .line 34
    move-object v1, v2

    .line 35
    goto :goto_0

    .line 36
    :cond_2
    invoke-virtual {v1, p1}, Landroidx/collection/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    check-cast v1, Landroid/graphics/Bitmap;

    .line 41
    .line 42
    :goto_0
    if-eqz v1, :cond_4

    .line 43
    .line 44
    instance-of p1, p2, Lcom/intsig/camscanner/view/ImageViewTouchBase;

    .line 45
    .line 46
    if-eqz p1, :cond_3

    .line 47
    .line 48
    new-instance p1, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 49
    .line 50
    const/4 p3, 0x0

    .line 51
    invoke-direct {p1, v1, p3}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;I)V

    .line 52
    .line 53
    .line 54
    move-object p3, p2

    .line 55
    check-cast p3, Lcom/intsig/camscanner/view/ImageViewTouchBase;

    .line 56
    .line 57
    const/4 p4, 0x1

    .line 58
    invoke-virtual {p3, p1, p4}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p2, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 62
    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_3
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p2, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 69
    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_4
    if-nez p4, :cond_5

    .line 73
    .line 74
    return-void

    .line 75
    :cond_5
    invoke-interface {p4, p2}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$BitmapLoadOperation;->〇o〇(Landroid/widget/ImageView;)V

    .line 76
    .line 77
    .line 78
    new-instance v1, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;

    .line 79
    .line 80
    move-object v3, v1

    .line 81
    move-object v4, p0

    .line 82
    move-object v5, p1

    .line 83
    move-object v6, p2

    .line 84
    move-object v7, p3

    .line 85
    move-object v8, p4

    .line 86
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;-><init>(Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;Ljava/lang/Object;Landroid/widget/ImageView;Ljava/lang/Object;Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$BitmapLoadOperation;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 90
    .line 91
    .line 92
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 93
    .line 94
    invoke-virtual {p1, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    invoke-direct {p0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇8o8o〇()V

    .line 98
    .line 99
    .line 100
    :cond_6
    :goto_1
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public Oo08(Ljava/util/HashSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "TK;>;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    invoke-virtual {p1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-eqz p1, :cond_1

    .line 19
    .line 20
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 31
    .line 32
    invoke-virtual {v1, v0}, Landroidx/collection/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 37
    .line 38
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 39
    .line 40
    .line 41
    :cond_2
    :goto_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5

    .line 1
    iget v0, p1, Landroid/os/Message;->what:I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x3

    .line 5
    const/4 v3, 0x1

    .line 6
    if-eq v0, v3, :cond_6

    .line 7
    .line 8
    const/4 v4, 0x2

    .line 9
    if-eq v0, v4, :cond_4

    .line 10
    .line 11
    if-eq v0, v2, :cond_2

    .line 12
    .line 13
    const/4 p1, 0x4

    .line 14
    if-eq v0, p1, :cond_0

    .line 15
    .line 16
    return v1

    .line 17
    :cond_0
    invoke-virtual {p0}, Landroid/os/HandlerThread;->quit()Z

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 21
    .line 22
    if-eqz p1, :cond_1

    .line 23
    .line 24
    invoke-virtual {p1}, Landroidx/collection/LruCache;->evictAll()V

    .line 25
    .line 26
    .line 27
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 28
    .line 29
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o〇00O:Landroid/os/Handler;

    .line 33
    .line 34
    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 35
    .line 36
    .line 37
    const/4 p1, 0x0

    .line 38
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o〇00O:Landroid/os/Handler;

    .line 39
    .line 40
    iput-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->O8o08O8O:Landroid/os/Handler;

    .line 41
    .line 42
    return v3

    .line 43
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇080OO8〇0:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 44
    .line 45
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    if-nez p1, :cond_3

    .line 50
    .line 51
    iget-boolean p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇08O〇00〇o:Z

    .line 52
    .line 53
    if-nez p1, :cond_3

    .line 54
    .line 55
    invoke-direct {p0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇8o8o〇()V

    .line 56
    .line 57
    .line 58
    :cond_3
    return v3

    .line 59
    :cond_4
    iget-boolean v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇08O〇00〇o:Z

    .line 60
    .line 61
    if-nez v0, :cond_5

    .line 62
    .line 63
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 64
    .line 65
    instance-of v0, p1, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;

    .line 66
    .line 67
    if-eqz v0, :cond_5

    .line 68
    .line 69
    check-cast p1, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;

    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$AsyncBitmapUtil;->〇080()V

    .line 72
    .line 73
    .line 74
    :cond_5
    return v3

    .line 75
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇〇888()V

    .line 76
    .line 77
    .line 78
    iput-boolean v1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->OO:Z

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o〇00O:Landroid/os/Handler;

    .line 81
    .line 82
    invoke-virtual {p1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 83
    .line 84
    .line 85
    return v3
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public oO80(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o0:Landroidx/collection/LruCache;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroidx/collection/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇80〇808〇O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->o〇00O:Landroid/os/Handler;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇0O:[I

    .line 4
    .line 5
    const-string v2, "BitmapCacheLoader"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-static {v2, v0, v1, v3}, Lcom/intsig/comm/recycle/HandlerMsglerRecycle;->〇o〇(Ljava/lang/String;Landroid/os/Handler;[I[Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->O8o08O8O:Landroid/os/Handler;

    .line 12
    .line 13
    sget-object v1, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->〇0O:[I

    .line 14
    .line 15
    invoke-static {v2, v0, v1, v3}, Lcom/intsig/comm/recycle/HandlerMsglerRecycle;->〇o〇(Ljava/lang/String;Landroid/os/Handler;[I[Ljava/lang/Runnable;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

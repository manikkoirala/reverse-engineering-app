.class public Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;
.super Ljava/lang/Object;
.source "BitmapLoaderUtil.java"


# static fields
.field private static 〇080:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/loadimage/BitmapCacheLoader<",
            "Lcom/intsig/camscanner/loadimage/CacheKey;",
            "Lcom/intsig/camscanner/loadimage/BitmapPara;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private static O8(Landroid/content/Context;)I
    .locals 1

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Landroid/app/ActivityManager;

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/app/ActivityManager;->getMemoryClass()I

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static Oo08()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇080:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 9
    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    return-void

    .line 13
    :cond_1
    new-instance v1, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇o〇(Landroid/content/Context;)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;-><init>(I)V

    .line 20
    .line 21
    .line 22
    sput-object v1, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇080:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static oO80(Lcom/intsig/camscanner/loadimage/CacheKey;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇080:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->oO80(Ljava/lang/Object;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static o〇0(Lcom/intsig/camscanner/loadimage/CacheKey;Landroid/widget/ImageView;Lcom/intsig/camscanner/loadimage/BitmapPara;Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$BitmapLoadOperation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/loadimage/CacheKey;",
            "Landroid/widget/ImageView;",
            "Lcom/intsig/camscanner/loadimage/BitmapPara;",
            "Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$BitmapLoadOperation<",
            "Lcom/intsig/camscanner/loadimage/BitmapPara;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->Oo08()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇080:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->OO0o〇〇〇〇0(Ljava/lang/Object;Landroid/widget/ImageView;Ljava/lang/Object;Lcom/intsig/camscanner/loadimage/BitmapCacheLoader$BitmapLoadOperation;)V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static 〇080()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇080:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->O8()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static 〇80〇808〇O(J)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/loadimage/CacheKey;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    invoke-direct {v0, p0, p1, v1}, Lcom/intsig/camscanner/loadimage/CacheKey;-><init>(JI)V

    .line 6
    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->oO80(Lcom/intsig/camscanner/loadimage/CacheKey;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static 〇o00〇〇Oo(Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Lcom/intsig/camscanner/loadimage/CacheKey;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇080:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->Oo08(Ljava/util/HashSet;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static 〇o〇(Landroid/content/Context;)I
    .locals 4

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    invoke-static {p0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->O8(Landroid/content/Context;)I

    .line 4
    .line 5
    .line 6
    move-result p0

    .line 7
    const/high16 v0, 0x100000

    .line 8
    .line 9
    mul-int p0, p0, v0

    .line 10
    .line 11
    div-int/lit8 p0, p0, 0x4

    .line 12
    .line 13
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    .line 18
    .line 19
    .line 20
    move-result-wide v0

    .line 21
    const-wide/16 v2, 0x4

    .line 22
    .line 23
    div-long/2addr v0, v2

    .line 24
    long-to-int v1, v0

    .line 25
    invoke-static {p0, v1}, Ljava/lang/Math;->max(II)I

    .line 26
    .line 27
    .line 28
    move-result p0

    .line 29
    return p0

    .line 30
    :cond_0
    const/high16 p0, 0x800000

    .line 31
    .line 32
    return p0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static 〇〇888(J)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/loadimage/CacheKey;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, p1, v1}, Lcom/intsig/camscanner/loadimage/CacheKey;-><init>(JI)V

    .line 5
    .line 6
    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->oO80(Lcom/intsig/camscanner/loadimage/CacheKey;)V

    .line 8
    .line 9
    .line 10
    new-instance v0, Lcom/intsig/camscanner/loadimage/CacheKey;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-direct {v0, p0, p1, v1}, Lcom/intsig/camscanner/loadimage/CacheKey;-><init>(JI)V

    .line 14
    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->oO80(Lcom/intsig/camscanner/loadimage/CacheKey;)V

    .line 17
    .line 18
    .line 19
    new-instance v0, Lcom/intsig/camscanner/loadimage/CacheKey;

    .line 20
    .line 21
    const/16 v1, 0x8

    .line 22
    .line 23
    invoke-direct {v0, p0, p1, v1}, Lcom/intsig/camscanner/loadimage/CacheKey;-><init>(JI)V

    .line 24
    .line 25
    .line 26
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->oO80(Lcom/intsig/camscanner/loadimage/CacheKey;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.class public Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;
.super Ljava/lang/Object;
.source "ForceUpdateUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil$IForceUpdateListener;
    }
.end annotation


# static fields
.field static oO80:Ljava/lang/String; = "/operating/app/get_forceupdate"

.field private static o〇0:Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil; = null

.field static 〇〇888:Ljava/lang/String; = "https://api-cs-sandbox.intsig.net/user"


# instance fields
.field private final O8:Ljava/lang/String;

.field private final Oo08:Ljava/lang/String;

.field private final 〇080:I

.field private 〇o00〇〇Oo:Landroid/os/Handler;

.field private final 〇o〇:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x1388

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->〇080:I

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil$1;

    .line 9
    .line 10
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil$1;-><init>(Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;Landroid/os/Looper;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 18
    .line 19
    const-string v0, "https://cms.camcard.com/a/v/cdbfde01da653b219f116bec6132e27f0"

    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->〇o〇:Ljava/lang/String;

    .line 22
    .line 23
    const-string v0, "https://cms.camcard.com/a/v/ca8504a02a85d62b5d023f821938e2411"

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->O8:Ljava/lang/String;

    .line 26
    .line 27
    const-string v0, "https://cms.camcard.com/a/v/cb5d4f15adbc4777bc546909569cb0d34"

    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->Oo08:Ljava/lang/String;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static Oo08()Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->o〇0:Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;-><init>()V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->o〇0:Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;

    .line 11
    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->o〇0:Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇080()V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const-string v2, "APPContinuousCrashedTimes"

    .line 7
    .line 8
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o〇(Ljava/lang/String;I)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x1

    .line 13
    add-int/2addr v0, v1

    .line 14
    const/4 v3, 0x3

    .line 15
    if-lt v0, v3, :cond_0

    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const-string v3, "IsForceUpdateLocal"

    .line 22
    .line 23
    invoke-virtual {v0, v3, v1}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->o〇0(Ljava/lang/String;Z)V

    .line 24
    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->OO0o〇〇〇〇0(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1, v2, v0}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇〇888(Ljava/lang/String;I)V

    .line 39
    .line 40
    .line 41
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇〇888()Z
    .locals 6

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    const-string v3, "AppStartTimeInMillis"

    .line 10
    .line 11
    const-wide/16 v4, 0x0

    .line 12
    .line 13
    invoke-virtual {v2, v3, v4, v5}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->O8(Ljava/lang/String;J)J

    .line 14
    .line 15
    .line 16
    move-result-wide v2

    .line 17
    sub-long/2addr v0, v2

    .line 18
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    .line 19
    .line 20
    .line 21
    move-result-wide v0

    .line 22
    const-wide/16 v2, 0x1388

    .line 23
    .line 24
    cmp-long v4, v0, v2

    .line 25
    .line 26
    if-gtz v4, :cond_0

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 v0, 0x0

    .line 31
    :goto_0
    return v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O8()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇8o8o〇()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const-string v2, "en"

    .line 22
    .line 23
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    const-string v0, "https://cms.camcard.com/a/v/ca8504a02a85d62b5d023f821938e2411"

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const-string v2, "zh"

    .line 33
    .line 34
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_1

    .line 39
    .line 40
    const-string v0, "cn"

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-nez v0, :cond_1

    .line 47
    .line 48
    const-string v0, "https://cms.camcard.com/a/v/cb5d4f15adbc4777bc546909569cb0d34"

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    const-string v0, "https://cms.camcard.com/a/v/cdbfde01da653b219f116bec6132e27f0"

    .line 52
    .line 53
    :goto_0
    return-object v0
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oO80()Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    const-string v2, "IsForceUpdateFromServe"

    .line 7
    .line 8
    invoke-virtual {v1, v2, v0}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇080(Ljava/lang/String;Z)Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    const-string v2, "IsForceUpdateLocal"

    .line 19
    .line 20
    invoke-virtual {v1, v2, v0}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇080(Ljava/lang/String;Z)Z

    .line 21
    .line 22
    .line 23
    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    if-eqz v1, :cond_1

    .line 25
    .line 26
    :cond_0
    const/4 v0, 0x1

    .line 27
    goto :goto_0

    .line 28
    :catch_0
    move-exception v1

    .line 29
    const-string v2, "ForceUpdateUtil"

    .line 30
    .line 31
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    :goto_0
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇0(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    sput-object p1, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->〇〇888:Ljava/lang/String;

    .line 4
    .line 5
    :cond_0
    if-eqz p2, :cond_1

    .line 6
    .line 7
    sput-object p2, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->oO80:Ljava/lang/String;

    .line 8
    .line 9
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    const-string p2, "6.56.0.2312240000"

    .line 14
    .line 15
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇8o8o〇(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->〇o00〇〇Oo()Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string p2, "AppStartTimeInMillis"

    .line 23
    .line 24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 25
    .line 26
    .line 27
    move-result-wide v0

    .line 28
    invoke-virtual {p1, p2, v0, v1}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateSharePreferenceSingleton;->oO80(Ljava/lang/String;J)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 32
    .line 33
    const/4 p2, 0x1

    .line 34
    const-wide/16 v0, 0x1388

    .line 35
    .line 36
    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇o00〇〇Oo()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->〇080()V

    .line 8
    .line 9
    .line 10
    const-string v0, "ForceUpdateUtil"

    .line 11
    .line 12
    const-string v1, "\u6355\u6349\u5230\u5f02\u5e38.."

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇(Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil$IForceUpdateListener;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/Verify;->O8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/forceUpdate/NetworkHttpManager;

    .line 8
    .line 9
    invoke-direct {v0}, Lcom/intsig/camscanner/forceUpdate/NetworkHttpManager;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/forceUpdate/NetworkHttpManager;->〇O8o08O(Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil$IForceUpdateListener;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/forceUpdate/ForceUpdateUtil;->oO80()Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/forceUpdate/NetworkHttpManager;->〇8o8o〇(Z)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

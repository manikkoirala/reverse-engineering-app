.class public Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;
.super Landroidx/fragment/app/DialogFragment;
.source "ProgressWithTipsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;,
        Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;
    }
.end annotation


# instance fields
.field private O8o08O8O:I

.field private OO:Landroid/widget/TextView;

.field private OO〇00〇8oO:I

.field private o0:Landroid/widget/TextView;

.field private o8〇OO0〇0o:Z

.field private oOo0:J

.field private oOo〇8o008:Z

.field private ooo0〇〇O:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;

.field private o〇00O:I

.field private 〇080OO8〇0:Ljava/lang/String;

.field private 〇08O〇00〇o:Landroid/widget/TextView;

.field private 〇0O:J

.field private 〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

.field private 〇OOo8〇0:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/DialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o〇00O:I

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->O8o08O8O:I

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    .line 11
    .line 12
    const-wide/16 v1, -0x1

    .line 13
    .line 14
    iput-wide v1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo0:J

    .line 15
    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O0〇0(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->O8o08O8O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O80〇O〇080()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/16 v1, 0x8

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic Ooo8o(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 7

    .line 1
    const/4 p1, 0x4

    .line 2
    const/4 p3, 0x0

    .line 3
    if-ne p2, p1, :cond_3

    .line 4
    .line 5
    new-instance p1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string p2, "click back tag = "

    .line 11
    .line 12
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    iget-wide v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo0:J

    .line 16
    .line 17
    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const-string p2, "ProgressWithTipsFragment"

    .line 25
    .line 26
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    sget-object p1, Lcom/intsig/camscanner/app/DBUtil;->Oo08:Ljava/util/HashMap;

    .line 30
    .line 31
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    .line 45
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    check-cast v0, Ljava/util/Map$Entry;

    .line 50
    .line 51
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    check-cast v1, Lcom/intsig/camscanner/app/DbWaitingListener;

    .line 56
    .line 57
    iget-wide v2, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo0:J

    .line 58
    .line 59
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    check-cast v4, Ljava/lang/Long;

    .line 64
    .line 65
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    .line 66
    .line 67
    .line 68
    move-result-wide v4

    .line 69
    cmp-long v6, v2, v4

    .line 70
    .line 71
    if-nez v6, :cond_0

    .line 72
    .line 73
    if-eqz v1, :cond_0

    .line 74
    .line 75
    invoke-interface {v1}, Lcom/intsig/camscanner/app/DbWaitingListener;->onBackPressed()Z

    .line 76
    .line 77
    .line 78
    move-result p3

    .line 79
    new-instance p1, Ljava/lang/StringBuilder;

    .line 80
    .line 81
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .line 83
    .line 84
    const-string v1, "back listenerDocId = "

    .line 85
    .line 86
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    const-string v0, " result = "

    .line 97
    .line 98
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    :cond_1
    if-eqz p3, :cond_2

    .line 112
    .line 113
    sget-object p1, Lcom/intsig/camscanner/app/DBUtil;->Oo08:Ljava/util/HashMap;

    .line 114
    .line 115
    iget-wide p2, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo0:J

    .line 116
    .line 117
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 118
    .line 119
    .line 120
    move-result-object p2

    .line 121
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    .line 123
    .line 124
    invoke-direct {p0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇o〇88〇8()V

    .line 125
    .line 126
    .line 127
    :cond_2
    const/4 p1, 0x1

    .line 128
    return p1

    .line 129
    :cond_3
    return p3
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method static bridge synthetic o00〇88〇08(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->O8o08O8O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇〇〇00(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic oOoO8OO〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    if-eqz p0, :cond_0

    .line 2
    .line 3
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    .line 4
    .line 5
    .line 6
    :cond_0
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇〇〇0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->Ooo8o(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private onCancel()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->ooo0〇〇O:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;->cancel()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇080OO8〇0:Ljava/lang/String;

    .line 9
    .line 10
    const-string v1, "cancel"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oooO888(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOoO8OO〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇0〇o(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->O80〇O〇080()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇O8OO(I)Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    const-string p1, "CSHDDownloadLoading"

    .line 5
    .line 6
    return-object p1

    .line 7
    :cond_0
    const/4 v0, 0x2

    .line 8
    if-ne p1, v0, :cond_1

    .line 9
    .line 10
    const-string p1, "CSEnhance"

    .line 11
    .line 12
    return-object p1

    .line 13
    :cond_1
    const/4 v0, 0x3

    .line 14
    if-ne p1, v0, :cond_2

    .line 15
    .line 16
    const-string p1, "CSPictureProcessLoading"

    .line 17
    .line 18
    return-object p1

    .line 19
    :cond_2
    const/4 p1, 0x0

    .line 20
    return-object p1
.end method

.method static bridge synthetic 〇088O(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇O0o〇〇o(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇0ooOOo(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 4
    .line 5
    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->onCancel()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇0ooOOo(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇8O0880(Landroid/view/View;)V
    .locals 16
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const v2, 0x7f0a12be

    .line 1
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    .line 2
    sget-object v2, Lcom/intsig/camscanner/app/DBUtil;->Oo08:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 3
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/intsig/camscanner/app/DbWaitingListener;

    .line 4
    iget-wide v6, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo0:J

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-nez v3, :cond_0

    if-eqz v5, :cond_0

    .line 5
    invoke-interface {v5}, Lcom/intsig/camscanner/app/DbWaitingListener;->〇o〇()Ljava/lang/String;

    move-result-object v2

    .line 6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "spec_theme_gone_cancel"

    .line 7
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 8
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    .line 9
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSpecTheme = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ProgressWithTipsFragment"

    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const v2, 0x7f0a0e9e

    .line 10
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    if-eqz v2, :cond_3

    .line 11
    new-instance v3, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    invoke-direct {v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 12
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0600ec

    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    move-result-object v3

    const/16 v5, 0xcc

    .line 13
    invoke-virtual {v3, v5}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    move-result-object v3

    .line 14
    invoke-virtual {v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v3

    .line 15
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    const v2, 0x7f0a185f    # 1.8356E38f

    .line 16
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v2, 0x7f0a16dd

    .line 17
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    const v2, 0x7f0a0d5b

    .line 18
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/airbnb/lottie/LottieAnimationView;

    .line 19
    iget v3, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    const/16 v5, 0x16

    const/16 v6, 0x12

    const/16 v7, 0x11

    const/16 v8, 0x10

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-eq v3, v9, :cond_4

    if-eq v3, v8, :cond_4

    if-eq v3, v7, :cond_4

    if-eq v3, v6, :cond_4

    if-eq v3, v5, :cond_4

    const/4 v3, 0x1

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    :goto_0
    iput-boolean v3, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 20
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 21
    iget v11, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    const v12, 0x7f1308c9

    const v13, 0x7f1308c8

    const v14, 0x7f120034

    const/4 v15, 0x4

    if-eq v11, v10, :cond_14

    if-eq v11, v8, :cond_14

    if-eq v11, v7, :cond_14

    if-eq v11, v6, :cond_14

    if-ne v11, v5, :cond_5

    goto/16 :goto_3

    :cond_5
    if-ne v11, v9, :cond_7

    .line 22
    invoke-virtual {v2, v14}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 23
    iget-object v9, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v9, v4}, Landroid/view/View;->setVisibility(I)V

    .line 24
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 25
    iget v9, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v11

    const/16 v14, 0xc8

    invoke-static {v11, v14}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    move-result v11

    iget v14, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v15, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v2, v9, v11, v14, v15}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 26
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 27
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v9, 0x7f130e22

    invoke-virtual {v0, v9}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    iget-boolean v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    if-eqz v2, :cond_6

    const v2, 0x7f1308d8

    .line 29
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308d9

    .line 30
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308da

    .line 31
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    invoke-virtual {v0, v13}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    invoke-virtual {v0, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308cc

    .line 34
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_6
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    goto/16 :goto_4

    :cond_7
    const/4 v5, 0x3

    if-ne v11, v5, :cond_8

    .line 36
    invoke-virtual {v2, v14}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 37
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/view/View;->setVisibility(I)V

    .line 38
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 39
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f1300a3

    invoke-virtual {v0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    iget-boolean v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    if-eqz v2, :cond_1a

    const v2, 0x7f1308c6

    .line 41
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308db

    .line 42
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308dc

    .line 43
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v5, v10, [Ljava/lang/Object;

    const v11, 0x7f13080f

    invoke-virtual {v0, v11}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v4

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308c7

    .line 44
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const v11, 0x7f131e81

    invoke-virtual {v0, v11}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v4

    const v12, 0x7f1303bc

    invoke-virtual {v0, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v10

    const v13, 0x7f130477

    invoke-virtual {v0, v13}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v5, v9

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308dd

    .line 45
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v11}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v4

    invoke-virtual {v0, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v10

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308de

    .line 46
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308df

    .line 47
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308e0

    .line 48
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_8
    if-ne v11, v15, :cond_9

    const v5, 0x7f120036

    .line 49
    invoke-virtual {v2, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 50
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 51
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 52
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f130ad3

    invoke-virtual {v0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iput-boolean v10, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    const v2, 0x7f1308c6

    .line 54
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308c7

    .line 55
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const v11, 0x7f131e81

    invoke-virtual {v0, v11}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v4

    const v11, 0x7f1303bc

    invoke-virtual {v0, v11}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v10

    const v11, 0x7f130477

    invoke-virtual {v0, v11}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v9

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    invoke-virtual {v0, v13}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    invoke-virtual {v0, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308ca

    .line 58
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308cb

    .line 59
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308cc

    .line 60
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    goto/16 :goto_4

    :cond_9
    const/4 v5, 0x5

    if-ne v11, v5, :cond_a

    .line 62
    invoke-virtual {v2, v14}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 63
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/view/View;->setVisibility(I)V

    .line 64
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f130cbd

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 66
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 67
    iput-boolean v10, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    goto/16 :goto_4

    :cond_a
    const/16 v5, 0x72

    const-string v6, "images"

    if-ne v11, v5, :cond_b

    const v5, 0x7f12004d

    .line 68
    invoke-virtual {v2, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 69
    invoke-virtual {v2, v6}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    .line 70
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 71
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 72
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f131b6d

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 73
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 74
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 75
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    goto/16 :goto_4

    :cond_b
    const/4 v5, 0x7

    if-ne v11, v5, :cond_c

    const v5, 0x7f12004e

    .line 76
    invoke-virtual {v2, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 77
    invoke-virtual {v2, v6}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    .line 78
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/view/View;->setVisibility(I)V

    .line 79
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 80
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f130d58

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 81
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    const v5, 0x7f130d8a

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 82
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 83
    iput-boolean v10, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    goto/16 :goto_4

    :cond_c
    const/16 v5, 0x71

    if-ne v11, v5, :cond_d

    const v5, 0x7f12004e

    .line 84
    invoke-virtual {v2, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 85
    invoke-virtual {v2, v6}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    .line 86
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/view/View;->setVisibility(I)V

    .line 87
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 88
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f130d58

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 89
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    const v5, 0x7f131a2b

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 90
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 91
    iput-boolean v10, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    goto/16 :goto_4

    :cond_d
    const/4 v5, 0x6

    if-ne v11, v5, :cond_e

    .line 92
    invoke-virtual {v2, v14}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 93
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 94
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 95
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f130d9f

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 96
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 97
    iput-boolean v10, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    goto/16 :goto_4

    :cond_e
    const/16 v5, 0x8

    if-ne v11, v5, :cond_f

    const v5, 0x7f12003d

    .line 98
    invoke-virtual {v2, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 99
    invoke-virtual {v2, v6}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    const v2, 0x7f130f3c

    .line 100
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f130f3d

    .line 101
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f130f3e

    .line 102
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    iput-boolean v10, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 104
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    goto/16 :goto_4

    :cond_f
    const/16 v5, 0x9

    if-eq v11, v5, :cond_13

    const/16 v5, 0x13

    if-ne v11, v5, :cond_10

    goto :goto_2

    :cond_10
    const/16 v5, 0x14

    if-eq v11, v5, :cond_11

    const/16 v5, 0x15

    if-ne v11, v5, :cond_1a

    .line 105
    :cond_11
    iput-boolean v4, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    const v5, 0x7f0a143f

    .line 106
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 107
    iget-object v5, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v5, v15}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget v5, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    const/16 v6, 0x14

    if-ne v5, v6, :cond_12

    const v5, 0x7f120037

    .line 109
    invoke-virtual {v2, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    goto :goto_1

    :cond_12
    const v5, 0x7f120037

    .line 110
    invoke-virtual {v2, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 111
    :goto_1
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    new-instance v5, L〇〇8/Oo08;

    invoke-direct {v5, v0}, L〇〇8/Oo08;-><init>(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    :cond_13
    :goto_2
    const v5, 0x7f120037

    .line 112
    invoke-virtual {v2, v5}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 113
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/view/View;->setVisibility(I)V

    .line 114
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 115
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f130704

    invoke-virtual {v0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\u2026\u2026"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 116
    iget-object v5, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f1308c7

    .line 117
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const v6, 0x7f131e81

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    const v6, 0x7f1303bc

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    const v6, 0x7f130477

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-virtual {v0, v13}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-virtual {v0, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308ca

    .line 120
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308cb

    .line 121
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308cc

    .line 122
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308d8

    .line 123
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308d9

    .line 124
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308da

    .line 125
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308db

    .line 126
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308dc

    .line 127
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v5, v10, [Ljava/lang/Object;

    const v6, 0x7f13080f

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    iput-boolean v10, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    .line 129
    iput-boolean v10, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    .line 130
    iget v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    const/16 v5, 0x13

    if-ne v2, v5, :cond_1a

    const v2, 0x7f0a1a38

    .line 131
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v2, v5}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_4

    .line 132
    :cond_14
    :goto_3
    invoke-virtual {v2, v14}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 133
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 135
    iget-boolean v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    if-eqz v2, :cond_15

    const v2, 0x7f1308c6

    .line 136
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308c7

    .line 137
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const v6, 0x7f131e81

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    const v6, 0x7f1303bc

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    const v6, 0x7f130477

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-virtual {v0, v13}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-virtual {v0, v12}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308ca

    .line 140
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308cb

    .line 141
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v2, 0x7f1308cc

    .line 142
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_15
    iget v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    if-ne v2, v8, :cond_16

    .line 144
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    const v5, 0x7f130f9c

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 145
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f131493

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    :cond_16
    if-ne v2, v7, :cond_17

    .line 146
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    const v5, 0x7f130f9c

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 147
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f131494

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    :cond_17
    const/16 v5, 0x12

    if-ne v2, v5, :cond_18

    .line 148
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 149
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    const v5, 0x7f131656

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 150
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f13165a

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 151
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_18
    const/16 v5, 0x16

    if-ne v2, v5, :cond_19

    .line 152
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    const v5, 0x7f13057e

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 153
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f131a69

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    .line 154
    :cond_19
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    const v5, 0x7f13030b

    invoke-virtual {v0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    :cond_1a
    :goto_4
    iget-boolean v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo〇8o008:Z

    if-eqz v2, :cond_1b

    .line 156
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 157
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    new-instance v5, L〇〇8/o〇0;

    invoke-direct {v5, v0}, L〇〇8/o〇0;-><init>(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v5, 0x8

    goto :goto_5

    .line 158
    :cond_1b
    iget-object v2, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    const v2, 0x7f0a0d01

    .line 159
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 160
    iget-boolean v6, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o8〇OO0〇0o:Z

    if-eqz v6, :cond_22

    .line 161
    iget v6, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    if-eq v6, v5, :cond_1c

    .line 162
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1c
    const v5, 0x7f0a1a1c

    .line 163
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ViewFlipper;

    iput-object v1, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    .line 164
    iget v1, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo0〇Ooo(I)I

    move-result v1

    add-int/2addr v1, v10

    .line 165
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    rem-int/2addr v1, v5

    .line 166
    iput v1, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->O8o08O8O:I

    move v5, v1

    :goto_6
    add-int/lit8 v6, v5, 0x1

    .line 167
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 168
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v6, v9, :cond_1d

    const/4 v6, 0x0

    .line 169
    :cond_1d
    new-instance v9, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v9, v11}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 170
    iget v11, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    const/16 v12, 0x8

    if-ne v11, v12, :cond_1e

    .line 171
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f060206

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    .line 172
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 173
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setGravity(I)V

    const/16 v12, 0x12

    const/16 v13, 0x16

    goto :goto_8

    :cond_1e
    const/16 v12, 0x12

    if-eq v11, v12, :cond_20

    if-eq v11, v8, :cond_20

    const/16 v13, 0x16

    if-ne v11, v13, :cond_1f

    goto :goto_7

    :cond_1f
    const-string v11, "#F1F1F1"

    .line 174
    invoke-static {v11}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_8

    :cond_20
    const/16 v13, 0x16

    .line 175
    :goto_7
    sget-object v11, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    const v14, 0x7f060208

    invoke-static {v11, v14}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_8
    const/high16 v11, 0x41600000    # 14.0f

    .line 176
    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTextSize(F)V

    .line 177
    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 178
    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    const/4 v11, -0x1

    const/4 v14, -0x2

    invoke-direct {v5, v11, v14}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 180
    iget-object v5, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    invoke-virtual {v5, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    if-ne v6, v1, :cond_21

    .line 181
    iget v1, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o〇00O:I

    add-int/2addr v1, v10

    iput v1, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o〇00O:I

    .line 182
    iget-object v1, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->startFlipping()V

    .line 183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u5f00\u59cb\u6eda\u52a8\uff0c \u5f53\u524d\u53ef\u89c1\u7684index: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->O8o08O8O:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", \u5f53\u524dType: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", \u7b2c\u4e00\u884c\u7684\u6587\u6848\u4e3a\uff1a "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "ProgressWithTipsFragment"

    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v1, L〇〇8/〇〇888;

    invoke-direct {v1, v0}, L〇〇8/〇〇888;-><init>(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v1, v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewAnimator;->getInAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    new-instance v2, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$1;

    invoke-direct {v2, v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$1;-><init>(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_9

    :cond_21
    move v5, v6

    goto/16 :goto_6

    .line 186
    :cond_22
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_9
    return-void
.end method

.method private 〇8〇80o()I
    .locals 2
    .annotation build Landroidx/annotation/LayoutRes;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    if-eq v0, v1, :cond_9

    .line 5
    .line 6
    const/4 v1, 0x6

    .line 7
    if-eq v0, v1, :cond_9

    .line 8
    .line 9
    const/4 v1, 0x7

    .line 10
    if-eq v0, v1, :cond_9

    .line 11
    .line 12
    const/16 v1, 0x71

    .line 13
    .line 14
    if-eq v0, v1, :cond_9

    .line 15
    .line 16
    const/16 v1, 0x72

    .line 17
    .line 18
    if-ne v0, v1, :cond_0

    .line 19
    .line 20
    goto :goto_3

    .line 21
    :cond_0
    const/16 v1, 0x8

    .line 22
    .line 23
    if-ne v0, v1, :cond_1

    .line 24
    .line 25
    const v0, 0x7f0d02d1

    .line 26
    .line 27
    .line 28
    return v0

    .line 29
    :cond_1
    const/16 v1, 0x9

    .line 30
    .line 31
    if-eq v0, v1, :cond_8

    .line 32
    .line 33
    const/16 v1, 0x13

    .line 34
    .line 35
    if-ne v0, v1, :cond_2

    .line 36
    .line 37
    goto :goto_2

    .line 38
    :cond_2
    const/4 v1, 0x4

    .line 39
    if-ne v0, v1, :cond_3

    .line 40
    .line 41
    const v0, 0x7f0d02bb

    .line 42
    .line 43
    .line 44
    return v0

    .line 45
    :cond_3
    const/16 v1, 0x12

    .line 46
    .line 47
    if-eq v0, v1, :cond_7

    .line 48
    .line 49
    const/16 v1, 0x10

    .line 50
    .line 51
    if-eq v0, v1, :cond_7

    .line 52
    .line 53
    const/16 v1, 0x16

    .line 54
    .line 55
    if-ne v0, v1, :cond_4

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_4
    const/16 v1, 0x14

    .line 59
    .line 60
    if-eq v0, v1, :cond_6

    .line 61
    .line 62
    const/16 v1, 0x15

    .line 63
    .line 64
    if-ne v0, v1, :cond_5

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_5
    const v0, 0x7f0d02d0

    .line 68
    .line 69
    .line 70
    return v0

    .line 71
    :cond_6
    :goto_0
    const v0, 0x7f0d0315

    .line 72
    .line 73
    .line 74
    return v0

    .line 75
    :cond_7
    :goto_1
    const v0, 0x7f0d0316

    .line 76
    .line 77
    .line 78
    return v0

    .line 79
    :cond_8
    :goto_2
    const v0, 0x7f0d02ba

    .line 80
    .line 81
    .line 82
    return v0

    .line 83
    :cond_9
    :goto_3
    const v0, 0x7f0d02d2

    .line 84
    .line 85
    .line 86
    return v0
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o〇00O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O0o〇〇o(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇o08(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇Oo〇O(Landroidx/appcompat/app/AppCompatActivity;)V
    .locals 2

    .line 1
    const-string v0, "ProgressWithTipsFragment"

    .line 2
    .line 3
    :try_start_0
    const-string v1, "useStandbyShow"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :catch_0
    move-exception p1

    .line 17
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18
    .line 19
    .line 20
    :goto_0
    return-void
.end method

.method private synthetic 〇o08(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->ooo0〇〇O:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;->〇080()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o〇88〇8()V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const-string v0, "ProgressWithTipsFragment"

    .line 7
    .line 8
    const-string v1, "user operation cancel download"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;->OO0o〇〇()Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/ImageDownloadControl;->〇O8o08O()V

    .line 18
    .line 19
    .line 20
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    .line 21
    .line 22
    const/4 v1, 0x7

    .line 23
    if-ne v0, v1, :cond_1

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 26
    .line 27
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 32
    .line 33
    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    const v3, 0x7f131a2e

    .line 44
    .line 45
    .line 46
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v2, "?"

    .line 54
    .line 55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    new-instance v1, L〇〇8/oO80;

    .line 67
    .line 68
    invoke-direct {v1}, L〇〇8/oO80;-><init>()V

    .line 69
    .line 70
    .line 71
    const v2, 0x7f131a95

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    new-instance v1, L〇〇8/〇80〇808〇O;

    .line 79
    .line 80
    invoke-direct {v1, p0}, L〇〇8/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)V

    .line 81
    .line 82
    .line 83
    const v2, 0x7f131a30

    .line 84
    .line 85
    .line 86
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->onCancel()V

    .line 99
    .line 100
    .line 101
    :goto_0
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static 〇〇O80〇0o(Landroid/os/Bundle;)Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇o0〇8(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o〇00O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇〇〇0(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    const/4 v0, 0x0

    .line 8
    :goto_0
    if-ge v0, p1, :cond_0

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    .line 11
    .line 12
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    .line 17
    .line 18
    .line 19
    add-int/lit8 v0, v0, 0x1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    .line 23
    .line 24
    invoke-virtual {p1}, Landroid/widget/ViewFlipper;->stopFlipping()V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    .line 28
    .line 29
    invoke-virtual {p1}, Landroid/widget/ViewAnimator;->showNext()V

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇oO〇〇8o:Landroid/widget/ViewFlipper;

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/widget/ViewFlipper;->startFlipping()V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇080OO8〇0:Ljava/lang/String;

    .line 38
    .line 39
    const-string v0, "next"

    .line 40
    .line 41
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic 〇〇〇00(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇o〇88〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O0O0〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8〇8〇O80(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o0:Landroid/widget/TextView;

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    new-array v2, v2, [Ljava/lang/Object;

    .line 21
    .line 22
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    aput-object p1, v2, v1

    .line 27
    .line 28
    const-string p1, "{0}%"

    .line 29
    .line 30
    invoke-static {p1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    const/4 v0, -0x1

    .line 47
    invoke-virtual {p1, v0, v0}, Landroid/view/Window;->setLayout(II)V

    .line 48
    .line 49
    .line 50
    sget-object p1, Lcom/intsig/camscanner/app/DBUtil;->Oo08:Ljava/util/HashMap;

    .line 51
    .line 52
    if-eqz p1, :cond_1

    .line 53
    .line 54
    iget-wide v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo0:J

    .line 55
    .line 56
    const-wide/16 v2, 0x0

    .line 57
    .line 58
    cmp-long p1, v0, v2

    .line 59
    .line 60
    if-ltz p1, :cond_1

    .line 61
    .line 62
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    new-instance v0, L〇〇8/O8;

    .line 67
    .line 68
    invoke-direct {v0, p0}, L〇〇8/O8;-><init>(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 72
    .line 73
    .line 74
    :cond_1
    :goto_0
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 5
    .line 6
    .line 7
    move-result-wide v0

    .line 8
    iput-wide v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇0O:J

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 2
    .line 3
    .line 4
    move-result-object p3

    .line 5
    invoke-virtual {p3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 6
    .line 7
    .line 8
    move-result-object p3

    .line 9
    const/4 v0, 0x1

    .line 10
    invoke-virtual {p3, v0}, Landroid/view/Window;->requestFeature(I)Z

    .line 11
    .line 12
    .line 13
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p3, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 20
    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    invoke-virtual {p3, v1}, Landroid/view/Window;->setDimAmount(F)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v2}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 27
    .line 28
    .line 29
    invoke-static {p3, v0}, Lcom/intsig/utils/SystemUiUtil;->o〇0(Landroid/view/Window;Z)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 33
    .line 34
    .line 35
    move-result-object p3

    .line 36
    if-eqz p3, :cond_0

    .line 37
    .line 38
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 39
    .line 40
    .line 41
    move-result-object p3

    .line 42
    const-string v0, "layerType"

    .line 43
    .line 44
    invoke-virtual {p3, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 45
    .line 46
    .line 47
    move-result p3

    .line 48
    iput p3, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    .line 49
    .line 50
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8〇80o()I

    .line 51
    .line 52
    .line 53
    move-result p3

    .line 54
    invoke-virtual {p1, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    return-object p1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onDestroy()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇080OO8〇0:Ljava/lang/String;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o〇00O:I

    .line 4
    .line 5
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const-string v2, "tips_num"

    .line 10
    .line 11
    const-string v3, "num"

    .line 12
    .line 13
    invoke-static {v0, v2, v3, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    iget-wide v4, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇0O:J

    .line 21
    .line 22
    sub-long/2addr v0, v4

    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇080OO8〇0:Ljava/lang/String;

    .line 24
    .line 25
    const-string v4, "loading_time"

    .line 26
    .line 27
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v5

    .line 31
    invoke-static {v2, v4, v3, v5}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const-wide/16 v2, 0x0

    .line 35
    .line 36
    cmp-long v4, v0, v2

    .line 37
    .line 38
    if-lez v4, :cond_0

    .line 39
    .line 40
    const/4 v2, 0x2

    .line 41
    new-array v2, v2, [Landroid/util/Pair;

    .line 42
    .line 43
    new-instance v3, Landroid/util/Pair;

    .line 44
    .line 45
    const-string v4, "from"

    .line 46
    .line 47
    const-string v5, "cs_hd_download_loading"

    .line 48
    .line 49
    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 50
    .line 51
    .line 52
    const/4 v4, 0x0

    .line 53
    aput-object v3, v2, v4

    .line 54
    .line 55
    new-instance v3, Landroid/util/Pair;

    .line 56
    .line 57
    new-instance v4, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v0, ""

    .line 66
    .line 67
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    const-string v1, "time"

    .line 75
    .line 76
    invoke-direct {v3, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 77
    .line 78
    .line 79
    const/4 v0, 0x1

    .line 80
    aput-object v3, v2, v0

    .line 81
    .line 82
    const-string v0, "CSWaiting"

    .line 83
    .line 84
    const-string v1, "show"

    .line 85
    .line 86
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 87
    .line 88
    .line 89
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    .line 90
    .line 91
    iget v1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->O8o08O8O:I

    .line 92
    .line 93
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇808〇(II)V

    .line 94
    .line 95
    .line 96
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 97
    .line 98
    .line 99
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public onResume()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "layerType"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o〇O8OO(I)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    iget p2, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->OO〇00〇8oO:I

    .line 5
    .line 6
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->o〇O8OO(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    iput-object p2, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇080OO8〇0:Ljava/lang/String;

    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇8O0880(Landroid/view/View;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇08O(Landroidx/appcompat/app/AppCompatActivity;)V
    .locals 2

    .line 1
    const-string v0, "ProgressWithTipsFragment"

    .line 2
    .line 3
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1, p0, v0}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commitNowAllowingStateLoss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :catch_0
    move-exception v1

    .line 27
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇Oo〇O(Landroidx/appcompat/app/AppCompatActivity;)V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇0oO〇oo00(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->oOo0:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇0〇0()Landroid/widget/TextView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8〇8000(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment;->ooo0〇〇O:Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

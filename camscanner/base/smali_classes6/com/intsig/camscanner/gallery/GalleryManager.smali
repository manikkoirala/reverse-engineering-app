.class public Lcom/intsig/camscanner/gallery/GalleryManager;
.super Ljava/lang/Object;
.source "GalleryManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/GalleryManager$ImageData;,
        Lcom/intsig/camscanner/gallery/GalleryManager$GroupImageData;,
        Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;
    }
.end annotation


# instance fields
.field private 〇080:Landroid/content/Context;

.field private 〇o00〇〇Oo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryManager;->〇080:Landroid/content/Context;

    .line 5
    .line 6
    new-instance p1, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryManager;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o00〇〇Oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryManager;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method 〇080()Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;
    .locals 18

    .line 1
    const-string v1, "GalleryManager"

    .line 2
    .line 3
    new-instance v2, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;

    .line 4
    .line 5
    invoke-direct {v2}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide v3

    .line 12
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/GalleryManager;->〇o00〇〇Oo()V

    .line 13
    .line 14
    .line 15
    sget-object v6, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 16
    .line 17
    move-object/from16 v11, p0

    .line 18
    .line 19
    iget-object v0, v11, Lcom/intsig/camscanner/gallery/GalleryManager;->〇080:Landroid/content/Context;

    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 22
    .line 23
    .line 24
    move-result-object v5

    .line 25
    const/4 v12, 0x0

    .line 26
    :try_start_0
    invoke-static {}, Lcom/intsig/camscanner/gallery/CustomGalleryUtil;->〇o00〇〇Oo()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v8

    .line 30
    invoke-static {}, Lcom/intsig/camscanner/gallery/CustomGalleryUtil;->〇o〇()[Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v9

    .line 34
    const-string v0, "_id"

    .line 35
    .line 36
    const-string v7, "_data"

    .line 37
    .line 38
    filled-new-array {v0, v7}, [Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v7

    .line 42
    const-string v10, "date_modified DESC"

    .line 43
    .line 44
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 45
    .line 46
    .line 47
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    .line 52
    .line 53
    move-object v0, v12

    .line 54
    :goto_0
    const/4 v5, 0x0

    .line 55
    if-eqz v0, :cond_5

    .line 56
    .line 57
    const-wide/16 v6, -0x1

    .line 58
    .line 59
    const/4 v8, 0x0

    .line 60
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 61
    .line 62
    .line 63
    move-result v9

    .line 64
    if-eqz v9, :cond_4

    .line 65
    .line 66
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    .line 67
    .line 68
    .line 69
    move-result-wide v9

    .line 70
    const/4 v13, 0x1

    .line 71
    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v13

    .line 75
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 76
    .line 77
    .line 78
    move-result v14

    .line 79
    if-eqz v14, :cond_0

    .line 80
    .line 81
    invoke-virtual {v2, v9, v10}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->〇o00〇〇Oo(J)V

    .line 82
    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_0
    new-instance v14, Ljava/io/File;

    .line 86
    .line 87
    invoke-direct {v14, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    .line 91
    .line 92
    .line 93
    move-result v15

    .line 94
    if-eqz v15, :cond_3

    .line 95
    .line 96
    if-nez v12, :cond_1

    .line 97
    .line 98
    move-object v12, v13

    .line 99
    :cond_1
    const-wide/16 v15, 0x0

    .line 100
    .line 101
    cmp-long v17, v6, v15

    .line 102
    .line 103
    if-gez v17, :cond_2

    .line 104
    .line 105
    move-wide v6, v9

    .line 106
    :cond_2
    invoke-virtual {v14}, Ljava/io/File;->getParentFile()Ljava/io/File;

    .line 107
    .line 108
    .line 109
    move-result-object v14

    .line 110
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v14

    .line 114
    invoke-virtual {v2, v14, v9, v10, v13}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->〇080(Ljava/lang/String;JLjava/lang/String;)V

    .line 115
    .line 116
    .line 117
    add-int/lit8 v8, v8, 0x1

    .line 118
    .line 119
    goto :goto_1

    .line 120
    :cond_3
    invoke-virtual {v2, v9, v10}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->〇o00〇〇Oo(J)V

    .line 121
    .line 122
    .line 123
    goto :goto_1

    .line 124
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 125
    .line 126
    .line 127
    invoke-virtual {v2, v8}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->〇80〇808〇O(I)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v2, v12}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->oO80(Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v2, v6, v7}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->〇〇888(J)V

    .line 134
    .line 135
    .line 136
    move v5, v8

    .line 137
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 138
    .line 139
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    .line 141
    .line 142
    const-string v6, "loadImages cost time = "

    .line 143
    .line 144
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 148
    .line 149
    .line 150
    move-result-wide v6

    .line 151
    sub-long/2addr v6, v3

    .line 152
    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    const-string v3, " count="

    .line 156
    .line 157
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v0

    .line 167
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    return-object v2
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.class Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "GalleryPreviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImageThumbAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private OO:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;

.field private o0:Landroid/content/Context;

.field private final o〇00O:Lcom/bumptech/glide/request/RequestOptions;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/gallery/GallerySelectedItem;

.field private 〇OOo8〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 7
    .line 8
    .line 9
    sget-object v1, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇o〇()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o〇00O:Lcom/bumptech/glide/request/RequestOptions;

    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o0:Landroid/content/Context;

    .line 26
    .line 27
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O8ooOoo〇(ILandroid/view/View;)V
    .locals 1

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->OO:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 12
    .line 13
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;->〇o00〇〇Oo(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;Lcom/intsig/camscanner/gallery/GallerySelectedItem;)I
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->oo88o8O(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O〇8O8〇008(ILandroid/view/View;)V
    .locals 1

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->OO:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 12
    .line 13
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;->〇080(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o800o8O(I)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    if-ltz p1, :cond_1

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-ge p1, v0, :cond_1

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    :cond_1
    :goto_0
    return v1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private oo88o8O(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;I)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o800o8O(I)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O00(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;ILandroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->O8ooOoo〇(ILandroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇O888o0o(Ljava/lang/String;Landroid/net/Uri;)Lcom/bumptech/glide/RequestBuilder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ")",
            "Lcom/bumptech/glide/RequestBuilder<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o0:Landroid/content/Context;

    .line 4
    .line 5
    invoke-static {p1}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestManager;->〇O8o08O(Landroid/net/Uri;)Lcom/bumptech/glide/RequestBuilder;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o〇00O:Lcom/bumptech/glide/request/RequestOptions;

    .line 14
    .line 15
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o0:Landroid/content/Context;

    .line 21
    .line 22
    invoke-static {p2}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    invoke-virtual {p2, p1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o〇00O:Lcom/bumptech/glide/request/RequestOptions;

    .line 31
    .line 32
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    :goto_0
    return-object p1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇O〇(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;ILandroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->O〇8O8〇008(ILandroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method O8〇o(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->OO:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method OOO〇O0(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p1, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇0000OOO(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o〇〇0〇(Landroid/view/ViewGroup;I)Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method oo〇(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇08O〇00〇o:Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method o〇O8〇〇o(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇〇0〇(Landroid/view/ViewGroup;I)Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    const v0, 0x7f0d068f

    .line 8
    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    new-instance p2, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;

    .line 16
    .line 17
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;-><init>(Landroid/view/View;)V

    .line 18
    .line 19
    .line 20
    return-object p2
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method 〇00()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 15
    :goto_1
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0000OOO(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;I)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getPath()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getUri()Landroid/net/Uri;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇O888o0o(Ljava/lang/String;Landroid/net/Uri;)Lcom/bumptech/glide/RequestBuilder;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇08O〇00〇o:Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 22
    .line 23
    if-ne v0, v2, :cond_0

    .line 24
    .line 25
    invoke-static {p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const/4 v2, 0x0

    .line 30
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    invoke-static {p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;)Landroid/view/View;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    const/16 v2, 0x8

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 41
    .line 42
    .line 43
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->OoO〇OOo8o()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    const-string v2, "onBindViewHolder"

    .line 48
    .line 49
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-static {p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;->O〇8O8〇008(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;)Landroid/widget/ImageView;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v1, v0}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 57
    .line 58
    .line 59
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;->〇08O〇00〇o:Landroid/view/View;

    .line 60
    .line 61
    new-instance v1, Lcom/intsig/camscanner/gallery/〇o〇;

    .line 62
    .line 63
    invoke-direct {v1, p0, p2}, Lcom/intsig/camscanner/gallery/〇o〇;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;I)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    .line 68
    .line 69
    invoke-static {p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;->〇00(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter$ImageViewHolder;)Landroid/widget/ImageView;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    new-instance v0, Lcom/intsig/camscanner/gallery/O8;

    .line 74
    .line 75
    invoke-direct {v0, p0, p2}, Lcom/intsig/camscanner/gallery/O8;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;I)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method 〇oOO8O8(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    :goto_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method 〇oo〇(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    xor-int/lit8 p1, p1, 0x1

    .line 8
    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

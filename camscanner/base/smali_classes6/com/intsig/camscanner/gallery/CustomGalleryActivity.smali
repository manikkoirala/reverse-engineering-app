.class public Lcom/intsig/camscanner/gallery/CustomGalleryActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "CustomGalleryActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;,
        Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;,
        Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;,
        Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ItemTouchCallback;,
        Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageItemSelectCallBack;,
        Lcom/intsig/camscanner/gallery/CustomGalleryActivity$EnhanceMenuManager;
    }
.end annotation


# instance fields
.field private final O0O:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private O88O:I

.field private O8o〇O0:Landroid/widget/CheckBox;

.field private O8〇o〇88:Landroid/widget/CheckBox;

.field private OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

.field private final Oo0O0o8:Lcom/intsig/camscanner/gallery/GalleryGuideManager;

.field private Oo0〇Ooo:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

.field private Oo80:I

.field private Ooo08:Z

.field private O〇08oOOO0:Ljava/lang/String;

.field private O〇O:Landroid/widget/TextView;

.field private O〇o88o08〇:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$EnhanceMenuManager;

.field private o00〇88〇08:Lcom/intsig/app/BaseProgressDialog;

.field private o0OoOOo0:I

.field private o880:Landroid/view/animation/Animation;

.field private o8O:Lcom/intsig/camscanner/view/GalleryGridView;

.field private o8o:Ljava/lang/String;

.field private o8oOOo:Z

.field private o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

.field private oO00〇o:Z

.field private oOO0880O:Z

.field private oOO8:Z

.field private oOO〇〇:Z

.field private oOoo80oO:Z

.field private final oOo〇08〇:Lcom/intsig/camscanner/gallery/GalleryFolderItemClickListener;

.field private oO〇8O8oOo:Z

.field private final oO〇oo:Lcom/intsig/camscanner/gallery/GalleryFolderManager;

.field private oo8ooo8O:Ljava/lang/String;

.field private ooO:Ljava/lang/String;

.field final ooo0〇〇O:Landroid/net/Uri;

.field private oooO888:Landroid/view/animation/Animation;

.field private o〇oO:Ljava/lang/String;

.field private o〇o〇Oo88:I

.field private 〇00O0:Ljava/lang/String;

.field private 〇08〇o0O:Z

.field private 〇0O〇O00O:Z

.field private 〇800OO〇0O:Landroid/widget/RelativeLayout;

.field private final 〇80O8o8O〇:Lcom/intsig/camscanner/EdgeClient;

.field private 〇8〇o88:Landroid/widget/TextView;

.field private 〇O8oOo0:Z

.field private final 〇OO8ooO8〇:Lcom/intsig/camscanner/view/GalleryGridView$InterceptCallBack;

.field private 〇OO〇00〇0O:Ljava/lang/String;

.field private 〇O〇〇O8:Z

.field private 〇o0O:I

.field private 〇oo〇O〇80:Landroid/widget/TextView;

.field private 〇〇08O:Ljava/lang/String;

.field private 〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

.field private 〇〇o〇:Z

.field private 〇〇〇0o〇〇0:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->ooo0〇〇O:Landroid/net/Uri;

    .line 7
    .line 8
    new-instance v0, Ljava/util/HashSet;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O0O:Ljava/util/Set;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 17
    .line 18
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O〇〇O8:Z

    .line 19
    .line 20
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO〇〇:Z

    .line 21
    .line 22
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇08〇o0O:Z

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 26
    .line 27
    const/4 v1, 0x1

    .line 28
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Ooo08:Z

    .line 29
    .line 30
    new-instance v2, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$1;

    .line 31
    .line 32
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$1;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 33
    .line 34
    .line 35
    iput-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/view/GalleryGridView$InterceptCallBack;

    .line 36
    .line 37
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇〇0o〇〇0:Z

    .line 38
    .line 39
    const/4 v2, -0x1

    .line 40
    iput v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇o〇Oo88:I

    .line 41
    .line 42
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO00〇o:Z

    .line 43
    .line 44
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO0880O:Z

    .line 45
    .line 46
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOoo80oO:Z

    .line 47
    .line 48
    new-instance v1, Lcom/intsig/camscanner/gallery/GalleryGuideManager;

    .line 49
    .line 50
    invoke-direct {v1}, Lcom/intsig/camscanner/gallery/GalleryGuideManager;-><init>()V

    .line 51
    .line 52
    .line 53
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo0O0o8:Lcom/intsig/camscanner/gallery/GalleryGuideManager;

    .line 54
    .line 55
    new-instance v1, Lcom/intsig/camscanner/EdgeClient;

    .line 56
    .line 57
    invoke-direct {v1}, Lcom/intsig/camscanner/EdgeClient;-><init>()V

    .line 58
    .line 59
    .line 60
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇80O8o8O〇:Lcom/intsig/camscanner/EdgeClient;

    .line 61
    .line 62
    new-instance v1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$2;

    .line 63
    .line 64
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$2;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 65
    .line 66
    .line 67
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOo〇08〇:Lcom/intsig/camscanner/gallery/GalleryFolderItemClickListener;

    .line 68
    .line 69
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O8oOo0:Z

    .line 70
    .line 71
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;

    .line 72
    .line 73
    invoke-direct {v0}, Lcom/intsig/camscanner/gallery/GalleryFolderManager;-><init>()V

    .line 74
    .line 75
    .line 76
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO〇oo:Lcom/intsig/camscanner/gallery/GalleryFolderManager;

    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O008oO0()V
    .locals 3

    .line 1
    const-string v0, "cancel"

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OOo00()Lorg/json/JSONObject;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "CSImport"

    .line 8
    .line 9
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O00OoO〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O088O()V
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2
    .line 3
    const/16 v1, 0x21

    .line 4
    .line 5
    if-le v0, v1, :cond_1

    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/util/PermissionUtil;->〇O888o0o(Landroid/content/Context;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 15
    .line 16
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 23
    .line 24
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 25
    .line 26
    new-instance v1, LOO〇/O8;

    .line 27
    .line 28
    invoke-direct {v1, p0}, LOO〇/O8;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 36
    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 38
    .line 39
    const/16 v1, 0x8

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O08〇oO8〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    if-eq v0, v1, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇oo〇O〇80:Landroid/widget/TextView;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8o〇O0:Landroid/widget/CheckBox;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 27
    .line 28
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oooO888:Landroid/view/animation/Animation;

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 36
    .line 37
    .line 38
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O0oO()V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O0o0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8ooo(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O0oO()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->oOO〇〇:Landroid/view/View;

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 12
    .line 13
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 18
    .line 19
    const/16 v3, 0xc

    .line 20
    .line 21
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 22
    .line 23
    .line 24
    const/4 v3, 0x2

    .line 25
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 35
    .line 36
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 40
    .line 41
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇O〇〇O8:Landroid/widget/CheckBox;

    .line 42
    .line 43
    new-instance v1, LOO〇/〇o〇;

    .line 44
    .line 45
    invoke-direct {v1, p0}, LOO〇/〇o〇;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oo88(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O80OO(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇〇0o〇〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O88(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Ooo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O880O〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oooO8〇00(Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O888Oo(Z)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇0〇O0088o()V

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇o〇()V

    .line 12
    .line 13
    .line 14
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->notifyDataSetChanged()V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇〇〇0()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇8〇(I)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO0〇O()V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0〇o8〇()V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O8O(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/camscanner/gallery/CustomGalleryActivity$EnhanceMenuManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇o88o08〇:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$EnhanceMenuManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OO0O(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇oo8O80()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Ljava/util/Set;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O0O:Ljava/util/Set;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO0〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇O〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private OO8〇O8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->Oo0oOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, LOO〇/〇〇888;

    .line 8
    .line 9
    invoke-direct {v1, p0}, LOO〇/〇〇888;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, p0, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->〇oo()Landroidx/lifecycle/MutableLiveData;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    new-instance v1, LOO〇/oO80;

    .line 22
    .line 23
    invoke-direct {v1, p0}, LOO〇/oO80;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p0, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->〇8o8O〇O()Landroidx/lifecycle/MutableLiveData;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    new-instance v1, LOO〇/〇80〇808〇O;

    .line 36
    .line 37
    invoke-direct {v1, p0}, LOO〇/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, p0, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->O8oOo80()Landroidx/lifecycle/MutableLiveData;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    new-instance v1, LOO〇/OO0o〇〇〇〇0;

    .line 50
    .line 51
    invoke-direct {v1, p0}, LOO〇/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, p0, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic OO〇〇o0oO(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇o8〇〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Oo0O〇8800()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO〇oo:Lcom/intsig/camscanner/gallery/GalleryFolderManager;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOo〇08〇:Lcom/intsig/camscanner/gallery/GalleryFolderItemClickListener;

    .line 4
    .line 5
    invoke-virtual {v0, p0, v1}, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->O8(Landroid/content/Context;Lcom/intsig/camscanner/gallery/GalleryFolderItemClickListener;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic OoO〇OOo8o(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇〇0o〇〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OooO〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇8O0O80〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇00O(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0888(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oo8(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇80O(Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇0o8o8〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇8〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇0o8〇(II)Ljava/lang/String;
    .locals 5

    .line 1
    const-string v0, "ocr_mode"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    const/4 v2, 0x1

    .line 11
    const/4 v3, 0x0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    const v0, 0x7f131bce

    .line 15
    .line 16
    .line 17
    if-gez p2, :cond_0

    .line 18
    .line 19
    new-instance p2, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v0, "(%d)"

    .line 32
    .line 33
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    new-array v0, v2, [Ljava/lang/Object;

    .line 41
    .line 42
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    aput-object p1, v0, v3

    .line 47
    .line 48
    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    goto :goto_0

    .line 53
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v0, "(%d/%d)"

    .line 66
    .line 67
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    new-array v1, v1, [Ljava/lang/Object;

    .line 75
    .line 76
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    aput-object p1, v1, v3

    .line 81
    .line 82
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    aput-object p1, v1, v2

    .line 87
    .line 88
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    goto :goto_0

    .line 93
    :cond_1
    if-gez p2, :cond_2

    .line 94
    .line 95
    new-array p2, v2, [Ljava/lang/Object;

    .line 96
    .line 97
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    aput-object p1, p2, v3

    .line 102
    .line 103
    const p1, 0x7f13013c

    .line 104
    .line 105
    .line 106
    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    goto :goto_0

    .line 111
    :cond_2
    new-array v0, v1, [Ljava/lang/Object;

    .line 112
    .line 113
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 114
    .line 115
    .line 116
    move-result-object p1

    .line 117
    aput-object p1, v0, v3

    .line 118
    .line 119
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    aput-object p1, v0, v2

    .line 124
    .line 125
    const p1, 0x7f13013b

    .line 126
    .line 127
    .line 128
    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    :goto_0
    return-object p1
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic O〇8(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇oO:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->Oo08(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇8O0O80〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇O:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x43a68000    # 333.0f

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    .line 17
    .line 18
    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 22
    .line 23
    .line 24
    const-wide/16 v1, 0x1f4

    .line 25
    .line 26
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic O〇O800oo(Landroid/view/View;)V
    .locals 3

    .line 1
    new-instance p1, Landroid/content/Intent;

    .line 2
    .line 3
    const-string v0, "android.settings.APPLICATION_DETAILS_SETTINGS"

    .line 4
    .line 5
    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    const-string v2, "package"

    .line 17
    .line 18
    invoke-static {v2, v0, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    const/16 v0, 0xcb

    .line 26
    .line 27
    invoke-virtual {p0, p1, v0}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O〇o8(Lcom/google/android/material/tabs/TabLayout;IZ)Lcom/google/android/material/tabs/TabLayout$Tab;
    .locals 4
    .param p2    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout;->newTab()Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const v2, 0x7f0d035f

    .line 10
    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    move-object v1, p1

    .line 18
    check-cast v1, Lcom/intsig/camscanner/view/TextViewWrapBadgeIcon;

    .line 19
    .line 20
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, p3}, Lcom/intsig/camscanner/view/TextViewWrapBadgeIcon;->〇080(Z)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->setCustomView(Landroid/view/View;)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 27
    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O〇oo8O80()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00〇〇〇o〇8()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 5
    .line 6
    new-instance v1, LOO〇/Oo08;

    .line 7
    .line 8
    invoke-direct {v1, p0}, LOO〇/Oo08;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v2, 0xc8

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇O800oo(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇〇o8O(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO8:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o088O8800()V
    .locals 6

    .line 1
    const/4 v1, 0x0

    .line 2
    const-string v2, "cs_import"

    .line 3
    .line 4
    const/4 v3, 0x1

    .line 5
    const/4 v4, -0x1

    .line 6
    const/4 v5, 0x0

    .line 7
    move-object v0, p0

    .line 8
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/docimport/util/DocImportHelper;->〇〇888(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;ZIZ)Landroid/content/Intent;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "intent_log_agent_from"

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 15
    .line 16
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    const v1, 0x7f1307e0

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    const-string v2, "INTENT_SHOW_TITLE"

    .line 27
    .line 28
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 29
    .line 30
    .line 31
    new-instance v1, Lcom/intsig/result/GetActivityResult;

    .line 32
    .line 33
    invoke-direct {v1, p0}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 34
    .line 35
    .line 36
    const/16 v2, 0xc9

    .line 37
    .line 38
    invoke-virtual {v1, v0, v2}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    new-instance v1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$7;

    .line 43
    .line 44
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$7;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o0OO(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o0Oo(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->ooooo0O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0〇〇00〇o()V
    .locals 5

    .line 1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/high16 v2, 0x3f800000    # 1.0f

    .line 5
    .line 6
    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o880:Landroid/view/animation/Animation;

    .line 10
    .line 11
    const-wide/16 v3, 0x12c

    .line 12
    .line 13
    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    .line 17
    .line 18
    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oooO888:Landroid/view/animation/Animation;

    .line 22
    .line 23
    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇8(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o88o88(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇8o0OOOo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o88oo〇O(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-lez v0, :cond_4

    .line 6
    .line 7
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇o〇Oo88:I

    .line 8
    .line 9
    if-ltz v0, :cond_0

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇0O(I)V

    .line 12
    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 15
    .line 16
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇8〇oO〇〇8o:Lcom/google/android/material/tabs/TabLayout;

    .line 17
    .line 18
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-nez v1, :cond_3

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/google/android/material/tabs/TabLayout;->getSelectedTabPosition()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    const-string v0, "document _picture"

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o0OoOOo0:I

    .line 34
    .line 35
    const/4 v1, 0x1

    .line 36
    if-ne v0, v1, :cond_2

    .line 37
    .line 38
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O8oOo0:Z

    .line 39
    .line 40
    if-eqz v0, :cond_2

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 43
    .line 44
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->o〇8oOO88(Ljava/util/ArrayList;)V

    .line 45
    .line 46
    .line 47
    :cond_2
    const-string v0, "all_pictures"

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_3
    const-string v0, "none"

    .line 51
    .line 52
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 53
    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v3, ""

    .line 67
    .line 68
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oo8ooo8O:Ljava/lang/String;

    .line 76
    .line 77
    const-string v4, "picture"

    .line 78
    .line 79
    invoke-static {v1, v4, v2, v3, v0}, Lcom/intsig/camscanner/util/CsImportLogAgentUtil;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 83
    .line 84
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->ooo8o〇o〇(Ljava/util/List;)V

    .line 85
    .line 86
    .line 87
    const-string p1, "CSImport"

    .line 88
    .line 89
    invoke-static {p0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getCurrentEnhanceModeIndex(Landroid/content/Context;)I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    invoke-static {p1, v0}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->oO80(Ljava/lang/String;I)V

    .line 94
    .line 95
    .line 96
    :cond_4
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic o8O〇008(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇o8〇〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o8o0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO〇oo:Lcom/intsig/camscanner/gallery/GalleryFolderManager;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇o〇(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o8o0o8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8o8o〇()Ljava/util/ArrayList;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 8
    .line 9
    new-instance v2, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 15
    .line 16
    .line 17
    move-result v3

    .line 18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string v3, ""

    .line 22
    .line 23
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oo8ooo8O:Ljava/lang/String;

    .line 31
    .line 32
    const-string v4, "picture"

    .line 33
    .line 34
    invoke-static {v1, v4, v2, v3}, Lcom/intsig/camscanner/util/CsImportLogAgentUtil;->〇080(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o88oo〇O(Ljava/util/ArrayList;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oO8()V
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOoo80oO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    const/4 v2, 0x0

    .line 10
    const-string v3, "cs_import"

    .line 11
    .line 12
    const/4 v4, 0x1

    .line 13
    const/4 v5, -0x1

    .line 14
    const/4 v6, 0x0

    .line 15
    move-object v1, p0

    .line 16
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/docimport/util/DocImportHelper;->〇〇888(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;ZIZ)Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "intent_log_agent_from"

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 23
    .line 24
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25
    .line 26
    .line 27
    const-string v1, "intent_from_custom_gallery"

    .line 28
    .line 29
    const/4 v2, 0x1

    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 31
    .line 32
    .line 33
    new-instance v1, Lcom/intsig/result/GetActivityResult;

    .line 34
    .line 35
    invoke-direct {v1, p0}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 36
    .line 37
    .line 38
    const/16 v2, 0xca

    .line 39
    .line 40
    invoke-virtual {v1, v0, v2}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    new-instance v1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$5;

    .line 45
    .line 46
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$5;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oO88〇0O8O(Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 9
    .line 10
    new-instance v1, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-direct {v1, p1, v2}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O〇8O8〇008(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V

    .line 17
    .line 18
    .line 19
    iget-boolean p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Ooo08:Z

    .line 20
    .line 21
    if-eqz p1, :cond_1

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO0〇O()V

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->notifyDataSetChanged()V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇〇〇0()I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇8〇(I)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0〇o8〇()V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇O8o08O()Ljava/util/ArrayList;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-lez v0, :cond_2

    .line 55
    .line 56
    new-instance v0, Landroid/content/Intent;

    .line 57
    .line 58
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 59
    .line 60
    .line 61
    const/4 v1, 0x0

    .line 62
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    check-cast p1, Landroid/net/Uri;

    .line 67
    .line 68
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 69
    .line 70
    .line 71
    const/4 p1, -0x1

    .line 72
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 73
    .line 74
    .line 75
    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 76
    .line 77
    .line 78
    :goto_0
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private oO8o〇08〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->oOO〇〇:Landroid/view/View;

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 12
    .line 13
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 18
    .line 19
    const/16 v3, 0xc

    .line 20
    .line 21
    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 22
    .line 23
    .line 24
    const v3, 0x7f0a0f6f

    .line 25
    .line 26
    .line 27
    const/4 v4, 0x2

    .line 28
    invoke-virtual {v1, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 29
    .line 30
    .line 31
    const v3, 0x7f0a19b6

    .line 32
    .line 33
    .line 34
    invoke-virtual {v2, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 41
    .line 42
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    .line 44
    .line 45
    const/high16 v0, 0x41000000    # 8.0f

    .line 46
    .line 47
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 52
    .line 53
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇O〇〇O8:Landroid/widget/CheckBox;

    .line 54
    .line 55
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    instance-of v2, v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 60
    .line 61
    if-eqz v2, :cond_0

    .line 62
    .line 63
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 64
    .line 65
    iget v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 66
    .line 67
    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 68
    .line 69
    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 70
    .line 71
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 72
    .line 73
    .line 74
    :cond_0
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic oOO8oo0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇o〇Oo88:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic oO〇O0O(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic oo8(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;
    .locals 4

    .line 1
    invoke-static {}, Landroidx/core/view/WindowInsetsCompat$Type;->systemBars()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    invoke-virtual {p2, p1}, Landroidx/core/view/WindowInsetsCompat;->getInsets(I)Landroidx/core/graphics/Insets;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 10
    .line 11
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->O88O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/view/View;->getPaddingStart()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 18
    .line 19
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->O88O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 20
    .line 21
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 26
    .line 27
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->O88O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 28
    .line 29
    invoke-virtual {v3}, Landroid/view/View;->getPaddingEnd()I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    iget p1, p1, Landroidx/core/graphics/Insets;->bottom:I

    .line 34
    .line 35
    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 39
    .line 40
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->O88O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 41
    .line 42
    const/4 v0, 0x0

    .line 43
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 44
    .line 45
    .line 46
    return-object p2
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic oo88(Landroid/view/View;)V
    .locals 8

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇oO:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/camscanner/docimport/util/DocImportTrackUtil;->OO0o〇〇(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog;->O8o08O8O:Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    const/4 v3, 0x0

    .line 12
    const/4 v4, 0x0

    .line 13
    const/4 v5, 0x0

    .line 14
    const-string v6, "0"

    .line 15
    .line 16
    const/4 v7, 0x0

    .line 17
    invoke-virtual/range {v0 .. v7}, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;->Oo08(Landroid/app/Activity;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method private synthetic oo8〇〇(Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$ScannedDocImages;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$ScannedDocImages;->〇080()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v1, "update gallery docType list size == "

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "CustomGalleryActivity"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇〇0o〇〇0:Z

    .line 32
    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OoO8(Ljava/util/List;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic oooO8〇00(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 1
    iput-boolean p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO8:Z

    .line 2
    .line 3
    invoke-static {p2}, Lcom/intsig/camscanner/gallery/CustomGalleryUtil;->o〇0(Z)V

    .line 4
    .line 5
    .line 6
    const-string p1, "type"

    .line 7
    .line 8
    const-string v0, "crop"

    .line 9
    .line 10
    const-string v1, "CSImport"

    .line 11
    .line 12
    if-eqz p2, :cond_0

    .line 13
    .line 14
    const-string p2, "auto"

    .line 15
    .line 16
    invoke-static {v1, v0, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const-string p2, "manual"

    .line 21
    .line 22
    invoke-static {v1, v0, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic ooooo0O()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Landroidx/core/view/ViewCompat;->getRootWindowInsets(Landroid/view/View;)Landroidx/core/view/WindowInsetsCompat;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Landroidx/core/view/WindowInsetsCompat$Type;->navigationBars()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Landroidx/core/view/WindowInsetsCompat;->getInsets(I)Landroidx/core/graphics/Insets;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iget v0, v0, Landroidx/core/graphics/Insets;->bottom:I

    .line 24
    .line 25
    const/high16 v1, 0x41000000    # 8.0f

    .line 26
    .line 27
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    add-int/2addr v0, v1

    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 33
    .line 34
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇O〇〇O8:Landroid/widget/CheckBox;

    .line 35
    .line 36
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    instance-of v2, v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 41
    .line 42
    if-eqz v2, :cond_0

    .line 43
    .line 44
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 45
    .line 46
    iget v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 47
    .line 48
    iget v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 49
    .line 50
    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 51
    .line 52
    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 56
    .line 57
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇O〇〇O8:Landroid/widget/CheckBox;

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    .line 61
    .line 62
    :cond_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o〇08oO80o(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇0(Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o〇8〇(I)V
    .locals 9

    .line 1
    const v0, 0x7f060204

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const v1, 0x7f060206

    .line 9
    .line 10
    .line 11
    invoke-static {p0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/4 v2, 0x2

    .line 16
    const v3, 0x7f13013b

    .line 17
    .line 18
    .line 19
    const v4, 0x7f13013c

    .line 20
    .line 21
    .line 22
    const/4 v5, -0x1

    .line 23
    const/4 v6, 0x1

    .line 24
    const/4 v7, 0x0

    .line 25
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 26
    .line 27
    .line 28
    move-result-object v8

    .line 29
    if-lez p1, :cond_3

    .line 30
    .line 31
    iget-boolean v8, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O〇〇O8:Z

    .line 32
    .line 33
    if-eqz v8, :cond_1

    .line 34
    .line 35
    iget v8, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O88O:I

    .line 36
    .line 37
    if-ge p1, v8, :cond_0

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 40
    .line 41
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 51
    .line 52
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 53
    .line 54
    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 56
    .line 57
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 62
    .line 63
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 64
    .line 65
    .line 66
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 67
    .line 68
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 69
    .line 70
    .line 71
    :goto_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 72
    .line 73
    if-eqz v0, :cond_2

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 76
    .line 77
    new-array v1, v2, [Ljava/lang/Object;

    .line 78
    .line 79
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    aput-object v2, v1, v7

    .line 84
    .line 85
    iget v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 86
    .line 87
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    aput-object v2, v1, v6

    .line 92
    .line 93
    invoke-virtual {p0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 101
    .line 102
    iget v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 103
    .line 104
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇0o8〇(II)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    .line 110
    .line 111
    goto :goto_1

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 113
    .line 114
    new-array v1, v6, [Ljava/lang/Object;

    .line 115
    .line 116
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    aput-object v2, v1, v7

    .line 121
    .line 122
    invoke-virtual {p0, v4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 130
    .line 131
    invoke-direct {p0, p1, v5}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇0o8〇(II)Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    .line 137
    .line 138
    :goto_1
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportPicOptExp;->〇080()Z

    .line 139
    .line 140
    .line 141
    move-result p1

    .line 142
    if-eqz p1, :cond_5

    .line 143
    .line 144
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 145
    .line 146
    const/high16 v0, 0x3f800000    # 1.0f

    .line 147
    .line 148
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 149
    .line 150
    .line 151
    goto :goto_3

    .line 152
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 153
    .line 154
    invoke-virtual {p1, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 155
    .line 156
    .line 157
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 158
    .line 159
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 160
    .line 161
    .line 162
    iget-boolean p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 163
    .line 164
    if-eqz p1, :cond_4

    .line 165
    .line 166
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 167
    .line 168
    new-array v0, v2, [Ljava/lang/Object;

    .line 169
    .line 170
    aput-object v8, v0, v7

    .line 171
    .line 172
    iget v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 173
    .line 174
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 175
    .line 176
    .line 177
    move-result-object v1

    .line 178
    aput-object v1, v0, v6

    .line 179
    .line 180
    invoke-virtual {p0, v3, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object v0

    .line 184
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    .line 186
    .line 187
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 188
    .line 189
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 190
    .line 191
    invoke-direct {p0, v7, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇0o8〇(II)Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v0

    .line 195
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    .line 197
    .line 198
    goto :goto_2

    .line 199
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 200
    .line 201
    new-array v0, v6, [Ljava/lang/Object;

    .line 202
    .line 203
    aput-object v8, v0, v7

    .line 204
    .line 205
    invoke-virtual {p0, v4, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v0

    .line 209
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    .line 211
    .line 212
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 213
    .line 214
    invoke-direct {p0, v7, v5}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇0o8〇(II)Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object v0

    .line 218
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    .line 220
    .line 221
    :goto_2
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportPicOptExp;->〇080()Z

    .line 222
    .line 223
    .line 224
    move-result p1

    .line 225
    if-eqz p1, :cond_5

    .line 226
    .line 227
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 228
    .line 229
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 230
    .line 231
    .line 232
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 233
    .line 234
    const v0, 0x3ecccccd    # 0.4f

    .line 235
    .line 236
    .line 237
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 238
    .line 239
    .line 240
    :cond_5
    :goto_3
    return-void
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method static bridge synthetic o〇OoO0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O8oOo0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇o08〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8oo0oO0(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o〇o0oOO8()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8oo8888()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇8〇(I)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o0〇〇00〇o()V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 12
    .line 13
    iget-object v2, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->OO〇00〇8oO:Landroid/widget/RelativeLayout;

    .line 14
    .line 15
    iput-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 16
    .line 17
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 18
    .line 19
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8o〇O0:Landroid/widget/CheckBox;

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/camscanner/gallery/CustomGalleryUtil;->O8()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO8:Z

    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8o〇O0:Landroid/widget/CheckBox;

    .line 28
    .line 29
    invoke-virtual {v2, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8o〇O0:Landroid/widget/CheckBox;

    .line 33
    .line 34
    new-instance v2, LOO〇/〇8o8o〇;

    .line 35
    .line 36
    invoke-direct {v2, p0}, LOO〇/〇8o8o〇;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 40
    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 43
    .line 44
    iget-object v2, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇〇08O:Landroid/widget/TextView;

    .line 45
    .line 46
    iput-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇oo〇O〇80:Landroid/widget/TextView;

    .line 47
    .line 48
    new-instance v3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$EnhanceMenuManager;

    .line 49
    .line 50
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->o8〇OO0〇0o:Landroid/widget/RelativeLayout;

    .line 51
    .line 52
    invoke-direct {v3, p0, v2, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$EnhanceMenuManager;-><init>(Landroid/content/Context;Landroid/widget/TextView;Landroid/view/View;)V

    .line 53
    .line 54
    .line 55
    iput-object v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇o88o08〇:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$EnhanceMenuManager;

    .line 56
    .line 57
    invoke-virtual {v3}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$EnhanceMenuManager;->〇o〇()V

    .line 58
    .line 59
    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 61
    .line 62
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇O〇〇O8:Landroid/widget/CheckBox;

    .line 63
    .line 64
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 65
    .line 66
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 67
    .line 68
    const v2, 0x7f0801fe

    .line 69
    .line 70
    .line 71
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    if-eqz v1, :cond_0

    .line 76
    .line 77
    const/16 v2, 0xe

    .line 78
    .line 79
    invoke-static {p0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 80
    .line 81
    .line 82
    move-result v2

    .line 83
    invoke-virtual {v1, v0, v0, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 84
    .line 85
    .line 86
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 87
    .line 88
    const/4 v3, 0x0

    .line 89
    invoke-virtual {v2, v1, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 90
    .line 91
    .line 92
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 93
    .line 94
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 98
    .line 99
    .line 100
    move-result-object v1

    .line 101
    const-string v2, "bottom_tips_res"

    .line 102
    .line 103
    const/4 v3, -0x1

    .line 104
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    if-lez v1, :cond_1

    .line 109
    .line 110
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 111
    .line 112
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 113
    .line 114
    iput-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇O:Landroid/widget/TextView;

    .line 115
    .line 116
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 117
    .line 118
    .line 119
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇O:Landroid/widget/TextView;

    .line 120
    .line 121
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 122
    .line 123
    .line 124
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇O:Landroid/widget/TextView;

    .line 125
    .line 126
    new-instance v2, LOO〇/〇O8o08O;

    .line 127
    .line 128
    invoke-direct {v2, p0}, LOO〇/〇O8o08O;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 129
    .line 130
    .line 131
    const-wide/16 v3, 0xbb8

    .line 132
    .line 133
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 134
    .line 135
    .line 136
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 137
    .line 138
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 139
    .line 140
    iget-boolean v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO〇〇:Z

    .line 141
    .line 142
    const/16 v3, 0x8

    .line 143
    .line 144
    if-eqz v2, :cond_2

    .line 145
    .line 146
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_2
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 154
    .line 155
    .line 156
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 157
    .line 158
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->O88O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 159
    .line 160
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 161
    .line 162
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇OO8ooO8〇:Lcom/intsig/camscanner/view/GalleryGridView$InterceptCallBack;

    .line 163
    .line 164
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/GalleryGridView;->setInterceptCallBack(Lcom/intsig/camscanner/view/GalleryGridView$InterceptCallBack;)V

    .line 165
    .line 166
    .line 167
    new-instance v1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 168
    .line 169
    new-instance v2, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$3;

    .line 170
    .line 171
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$3;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 172
    .line 173
    .line 174
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageItemSelectCallBack;)V

    .line 175
    .line 176
    .line 177
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 178
    .line 179
    iget-boolean v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 180
    .line 181
    if-eqz v2, :cond_3

    .line 182
    .line 183
    iget v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 184
    .line 185
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o800o8O(I)V

    .line 186
    .line 187
    .line 188
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 189
    .line 190
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 191
    .line 192
    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 193
    .line 194
    .line 195
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 196
    .line 197
    invoke-virtual {v1, v0}, Landroid/view/View;->setScrollBarStyle(I)V

    .line 198
    .line 199
    .line 200
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 201
    .line 202
    const/4 v2, 0x1

    .line 203
    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setFastScrollEnabled(Z)V

    .line 204
    .line 205
    .line 206
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00〇〇〇o〇8()V

    .line 207
    .line 208
    .line 209
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 210
    .line 211
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇8〇oO〇〇8o:Lcom/google/android/material/tabs/TabLayout;

    .line 212
    .line 213
    iget-boolean v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO〇8O8oOo:Z

    .line 214
    .line 215
    if-eqz v4, :cond_7

    .line 216
    .line 217
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 218
    .line 219
    .line 220
    new-instance v4, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$4;

    .line 221
    .line 222
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$4;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 223
    .line 224
    .line 225
    invoke-virtual {v1, v4}, Lcom/google/android/material/tabs/TabLayout;->addOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;)V

    .line 226
    .line 227
    .line 228
    iget v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o0OoOOo0:I

    .line 229
    .line 230
    if-ne v4, v2, :cond_4

    .line 231
    .line 232
    const/4 v4, 0x0

    .line 233
    goto :goto_1

    .line 234
    :cond_4
    const/4 v4, 0x1

    .line 235
    :goto_1
    invoke-static {v4}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8O〇8oo08(I)I

    .line 236
    .line 237
    .line 238
    move-result v4

    .line 239
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O80O()Z

    .line 240
    .line 241
    .line 242
    move-result v5

    .line 243
    if-eqz v5, :cond_6

    .line 244
    .line 245
    if-nez v4, :cond_5

    .line 246
    .line 247
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OoO00008o(Z)V

    .line 248
    .line 249
    .line 250
    goto :goto_2

    .line 251
    :cond_5
    const/4 v4, 0x1

    .line 252
    goto :goto_3

    .line 253
    :cond_6
    :goto_2
    const/4 v4, 0x0

    .line 254
    :goto_3
    const v5, 0x7f131121

    .line 255
    .line 256
    .line 257
    invoke-direct {p0, v1, v5, v4}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇o8(Lcom/google/android/material/tabs/TabLayout;IZ)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 258
    .line 259
    .line 260
    move-result-object v4

    .line 261
    invoke-virtual {v1, v4}, Lcom/google/android/material/tabs/TabLayout;->addTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 262
    .line 263
    .line 264
    const v4, 0x7f131122

    .line 265
    .line 266
    .line 267
    invoke-direct {p0, v1, v4, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇o8(Lcom/google/android/material/tabs/TabLayout;IZ)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 268
    .line 269
    .line 270
    move-result-object v4

    .line 271
    invoke-virtual {v1, v4}, Lcom/google/android/material/tabs/TabLayout;->addTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 272
    .line 273
    .line 274
    goto :goto_4

    .line 275
    :cond_7
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 276
    .line 277
    .line 278
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇〇0o〇〇0:Z

    .line 279
    .line 280
    :goto_4
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 281
    .line 282
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 283
    .line 284
    .line 285
    move-result v4

    .line 286
    if-nez v4, :cond_9

    .line 287
    .line 288
    iget-boolean v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO〇8O8oOo:Z

    .line 289
    .line 290
    if-eqz v4, :cond_8

    .line 291
    .line 292
    invoke-virtual {v1, v2}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 293
    .line 294
    .line 295
    move-result-object v2

    .line 296
    invoke-virtual {v1, v2}, Lcom/google/android/material/tabs/TabLayout;->selectTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 297
    .line 298
    .line 299
    :cond_8
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 300
    .line 301
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO88〇0O8O(Ljava/lang/String;)V

    .line 302
    .line 303
    .line 304
    goto :goto_6

    .line 305
    :cond_9
    iget-boolean v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO〇8O8oOo:Z

    .line 306
    .line 307
    if-eqz v4, :cond_c

    .line 308
    .line 309
    iget v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o0OoOOo0:I

    .line 310
    .line 311
    if-ne v4, v2, :cond_a

    .line 312
    .line 313
    const/4 v4, 0x0

    .line 314
    goto :goto_5

    .line 315
    :cond_a
    const/4 v4, 0x1

    .line 316
    :goto_5
    invoke-static {v4}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8O〇8oo08(I)I

    .line 317
    .line 318
    .line 319
    move-result v4

    .line 320
    if-nez v4, :cond_b

    .line 321
    .line 322
    iput-boolean v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O8oOo0:Z

    .line 323
    .line 324
    invoke-virtual {v1, v0}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 325
    .line 326
    .line 327
    move-result-object v2

    .line 328
    invoke-virtual {v1, v2}, Lcom/google/android/material/tabs/TabLayout;->selectTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 329
    .line 330
    .line 331
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo0O0o8:Lcom/intsig/camscanner/gallery/GalleryGuideManager;

    .line 332
    .line 333
    iget-object v4, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 334
    .line 335
    iget-object v2, v2, Lcom/google/android/material/tabs/TabLayout$Tab;->view:Lcom/google/android/material/tabs/TabLayout$TabView;

    .line 336
    .line 337
    invoke-virtual {v1, v4, v2}, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->O8(Landroid/app/Activity;Landroid/view/View;)V

    .line 338
    .line 339
    .line 340
    goto :goto_6

    .line 341
    :cond_b
    invoke-virtual {v1, v2}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 342
    .line 343
    .line 344
    move-result-object v2

    .line 345
    invoke-virtual {v1, v2}, Lcom/google/android/material/tabs/TabLayout;->selectTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 346
    .line 347
    .line 348
    :cond_c
    :goto_6
    iget-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO0880O:Z

    .line 349
    .line 350
    if-eqz v1, :cond_d

    .line 351
    .line 352
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 353
    .line 354
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 355
    .line 356
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 357
    .line 358
    .line 359
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 360
    .line 361
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->oOo0:Landroid/widget/LinearLayout;

    .line 362
    .line 363
    new-instance v1, LOO〇/OO0o〇〇;

    .line 364
    .line 365
    invoke-direct {v1, p0}, LOO〇/OO0o〇〇;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 366
    .line 367
    .line 368
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 369
    .line 370
    .line 371
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 372
    .line 373
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 374
    .line 375
    new-instance v1, LOO〇/〇o00〇〇Oo;

    .line 376
    .line 377
    invoke-direct {v1, p0}, LOO〇/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 378
    .line 379
    .line 380
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    .line 382
    .line 383
    goto :goto_7

    .line 384
    :cond_d
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 385
    .line 386
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 387
    .line 388
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 389
    .line 390
    .line 391
    :goto_7
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O088O()V

    .line 392
    .line 393
    .line 394
    return-void
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private o〇o8〇〇O()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "CustomGalleryActivity"

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "isFinishing"

    .line 10
    .line 11
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Ooo08:Z

    .line 16
    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    return-void

    .line 20
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 21
    .line 22
    const/4 v2, 0x6

    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 26
    .line 27
    if-ge v0, v2, :cond_2

    .line 28
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v2, "mHasMaxCountLimit=true mMaxCount="

    .line 35
    .line 36
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    iget v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 40
    .line 41
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    return-void

    .line 52
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 53
    .line 54
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇8〇oO〇〇8o:Lcom/google/android/material/tabs/TabLayout;

    .line 55
    .line 56
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    if-nez v3, :cond_3

    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/google/android/material/tabs/TabLayout;->getSelectedTabPosition()I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    if-nez v0, :cond_3

    .line 67
    .line 68
    return-void

    .line 69
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo〇0o()Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-nez v0, :cond_4

    .line 74
    .line 75
    return-void

    .line 76
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 77
    .line 78
    invoke-virtual {v0}, Landroid/widget/GridView;->getNumColumns()I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 83
    .line 84
    invoke-virtual {v3}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇0()I

    .line 85
    .line 86
    .line 87
    move-result v3

    .line 88
    new-instance v4, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v5, "numberColumns="

    .line 94
    .line 95
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    const-string v0, " imageCount="

    .line 102
    .line 103
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    if-ge v3, v2, :cond_5

    .line 117
    .line 118
    return-void

    .line 119
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo0O0o8:Lcom/intsig/camscanner/gallery/GalleryGuideManager;

    .line 120
    .line 121
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 122
    .line 123
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 124
    .line 125
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->Oo08(Landroid/app/Activity;Landroid/widget/GridView;I)V

    .line 126
    .line 127
    .line 128
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇00o〇O8(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    sget-object v7, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/gallery/CustomGalleryUtil;->〇o00〇〇Oo()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-static {}, Lcom/intsig/camscanner/gallery/CustomGalleryUtil;->〇o〇()[Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v8

    .line 15
    const-string v3, "mime_type"

    .line 16
    .line 17
    const-string v4, "date_modified"

    .line 18
    .line 19
    const-string v5, "_id"

    .line 20
    .line 21
    const-string v6, "_data"

    .line 22
    .line 23
    filled-new-array {v5, v6, v3, v4}, [Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v9

    .line 27
    new-instance v3, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v4, "ids="

    .line 33
    .line 34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    const-string v4, "CustomGalleryActivity"

    .line 45
    .line 46
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    const-string v4, ") and "

    .line 54
    .line 55
    const-string v6, "("

    .line 56
    .line 57
    if-nez v3, :cond_0

    .line 58
    .line 59
    new-instance v3, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    const-string v2, " in "

    .line 77
    .line 78
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->ooO:Ljava/lang/String;

    .line 89
    .line 90
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    if-nez v1, :cond_1

    .line 95
    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    .line 97
    .line 98
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .line 100
    .line 101
    const-string v3, "_data like \'%"

    .line 102
    .line 103
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    iget-object v3, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->ooO:Ljava/lang/String;

    .line 107
    .line 108
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v3, "%\'"

    .line 112
    .line 113
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    .line 121
    .line 122
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .line 124
    .line 125
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v1

    .line 141
    move-object v10, v1

    .line 142
    goto :goto_0

    .line 143
    :cond_1
    move-object v10, v2

    .line 144
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 145
    .line 146
    .line 147
    move-result-object v1

    .line 148
    const-string v6, "date_modified DESC, _id DESC"

    .line 149
    .line 150
    move-object v2, v7

    .line 151
    move-object v3, v9

    .line 152
    move-object v4, v10

    .line 153
    move-object v5, v8

    .line 154
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 155
    .line 156
    .line 157
    move-result-object v11

    .line 158
    new-instance v12, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 159
    .line 160
    const-string v6, "date_modified DESC, _id DESC"

    .line 161
    .line 162
    iget-boolean v13, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 163
    .line 164
    iget-boolean v14, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O〇〇O8:Z

    .line 165
    .line 166
    iget v15, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 167
    .line 168
    iget v5, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O88O:I

    .line 169
    .line 170
    move-object v1, v12

    .line 171
    move v10, v5

    .line 172
    move-object v5, v8

    .line 173
    move v7, v13

    .line 174
    move v8, v14

    .line 175
    move v9, v15

    .line 176
    invoke-direct/range {v1 .. v10}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;-><init>(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZII)V

    .line 177
    .line 178
    .line 179
    iput-object v12, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 180
    .line 181
    iget-object v1, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 182
    .line 183
    invoke-virtual {v12, v1}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇0000OOO(Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    return-object v11
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇00〇〇〇o〇8()V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oo88o8O(I)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x3

    .line 12
    if-ge v0, v1, :cond_0

    .line 13
    .line 14
    const/4 v0, 0x3

    .line 15
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method private 〇0888(Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "loadAllImage startGallery:"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O8oOo0:Z

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "CustomGalleryActivity"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00o〇O8(Ljava/lang/String;)Landroid/database/Cursor;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 30
    .line 31
    iget-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O8oOo0:Z

    .line 32
    .line 33
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->〇〇0〇0o8(Landroid/database/Cursor;Z)V

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇〇〇0()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇8〇(I)V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO0〇O()V

    .line 46
    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0〇o8〇()V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇0O8Oo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "print"

    .line 4
    .line 5
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_5

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 12
    .line 13
    const-string v1, "qr"

    .line 14
    .line 15
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    goto/16 :goto_2

    .line 22
    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 24
    .line 25
    const-string v1, "single"

    .line 26
    .line 27
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    const-string v1, ""

    .line 32
    .line 33
    if-nez v0, :cond_4

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 36
    .line 37
    const-string v2, "batch"

    .line 38
    .line 39
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_4

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 46
    .line 47
    const-string v2, "id_mode"

    .line 48
    .line 49
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-nez v0, :cond_4

    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 56
    .line 57
    const-string v2, "excel"

    .line 58
    .line 59
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-nez v0, :cond_4

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 66
    .line 67
    const-string v2, "ocr_mode"

    .line 68
    .line 69
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 70
    .line 71
    .line 72
    move-result v0

    .line 73
    if-nez v0, :cond_4

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 76
    .line 77
    const-string v2, "retake"

    .line 78
    .line 79
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    if-nez v0, :cond_4

    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 86
    .line 87
    const-string v2, "image_restore"

    .line 88
    .line 89
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    if-eqz v0, :cond_1

    .line 94
    .line 95
    goto :goto_1

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 97
    .line 98
    const-string v2, "cs_list"

    .line 99
    .line 100
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    if-nez v0, :cond_3

    .line 105
    .line 106
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 107
    .line 108
    const-string v2, "cs_main"

    .line 109
    .line 110
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 111
    .line 112
    .line 113
    move-result v0

    .line 114
    if-nez v0, :cond_3

    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 117
    .line 118
    const-string v2, "feed_back"

    .line 119
    .line 120
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 121
    .line 122
    .line 123
    move-result v0

    .line 124
    if-eqz v0, :cond_2

    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_2
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00O0:Ljava/lang/String;

    .line 128
    .line 129
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 130
    .line 131
    goto :goto_3

    .line 132
    :cond_3
    :goto_0
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00O0:Ljava/lang/String;

    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 135
    .line 136
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 137
    .line 138
    goto :goto_3

    .line 139
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 140
    .line 141
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00O0:Ljava/lang/String;

    .line 142
    .line 143
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 144
    .line 145
    goto :goto_3

    .line 146
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 147
    .line 148
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00O0:Ljava/lang/String;

    .line 149
    .line 150
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oo8ooo8O:Ljava/lang/String;

    .line 151
    .line 152
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 153
    .line 154
    :goto_3
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO00〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo0〇Ooo:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0o〇o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇0()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 8
    .line 9
    const/16 v2, 0x8

    .line 10
    .line 11
    if-nez v1, :cond_2

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇00O0O〇o()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-gt v0, v1, :cond_2

    .line 18
    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 23
    .line 24
    iget-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Ooo08:Z

    .line 25
    .line 26
    if-eqz v1, :cond_1

    .line 27
    .line 28
    const/4 v2, 0x0

    .line 29
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 30
    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 34
    .line 35
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 36
    .line 37
    .line 38
    :goto_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇0〇o8〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇〇〇0()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x2

    .line 8
    if-lt v0, v1, :cond_1

    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇08〇o0O:Z

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8O()V

    .line 16
    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O08〇oO8〇()V

    .line 20
    .line 21
    .line 22
    :goto_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇80O(Ljava/lang/Boolean;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const-string v0, "CustomGalleryActivity"

    .line 6
    .line 7
    if-eqz p1, :cond_1

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o00〇88〇08:Lcom/intsig/app/BaseProgressDialog;

    .line 10
    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    invoke-static {p0, p1}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o00〇88〇08:Lcom/intsig/app/BaseProgressDialog;

    .line 19
    .line 20
    const v1, 0x7f131ec6

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {p1, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o00〇88〇08:Lcom/intsig/app/BaseProgressDialog;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :catch_0
    move-exception p1

    .line 37
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    :try_start_1
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o00〇88〇08:Lcom/intsig/app/BaseProgressDialog;

    .line 42
    .line 43
    if-eqz p1, :cond_2

    .line 44
    .line 45
    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    if-eqz p1, :cond_2

    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o00〇88〇08:Lcom/intsig/app/BaseProgressDialog;

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :catch_1
    move-exception p1

    .line 58
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 59
    .line 60
    .line 61
    :cond_2
    :goto_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic 〇80〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "CSImport"

    .line 10
    .line 11
    const-string v1, "action_bar_show"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇oo〇O〇80:Landroid/widget/TextView;

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8o〇O0:Landroid/widget/CheckBox;

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 33
    .line 34
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇800OO〇0O:Landroid/widget/RelativeLayout;

    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o880:Landroid/view/animation/Animation;

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o880:Landroid/view/animation/Animation;

    .line 45
    .line 46
    new-instance v1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$6;

    .line 47
    .line 48
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$6;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO8o〇08〇()V

    .line 56
    .line 57
    .line 58
    :goto_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic 〇8o0o0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇8oo0oO0(Ljava/util/List;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "update gallery list size == "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "CustomGalleryActivity"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇〇0o〇〇0:Z

    .line 28
    .line 29
    if-nez v0, :cond_0

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OoO8(Ljava/util/List;)V

    .line 34
    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇o8〇〇O()V

    .line 37
    .line 38
    .line 39
    :cond_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇8oo8888()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o〇:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇8〇OOoooo(Z)V

    .line 8
    .line 9
    .line 10
    const v0, 0x7f0806a9

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇〇〇O〇(I)V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇8〇OOoooo(Z)V

    .line 18
    .line 19
    .line 20
    :goto_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 26
    .line 27
    const v3, 0x7f08070f

    .line 28
    .line 29
    .line 30
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    if-eqz v0, :cond_1

    .line 35
    .line 36
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    invoke-virtual {v0, v1, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 45
    .line 46
    .line 47
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 48
    .line 49
    invoke-virtual {v1, v2, v2, v0, v2}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 53
    .line 54
    const/4 v1, 0x5

    .line 55
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 56
    .line 57
    .line 58
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 59
    .line 60
    const v1, 0x7f130416

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 64
    .line 65
    .line 66
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO0880O:Z

    .line 67
    .line 68
    if-eqz v0, :cond_2

    .line 69
    .line 70
    const v0, 0x7f131920

    .line 71
    .line 72
    .line 73
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(I)V

    .line 74
    .line 75
    .line 76
    :cond_2
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo80:I

    .line 77
    .line 78
    if-lez v0, :cond_3

    .line 79
    .line 80
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    iget v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo80:I

    .line 85
    .line 86
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    if-nez v1, :cond_3

    .line 95
    .line 96
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 97
    .line 98
    .line 99
    :cond_3
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    const v1, 0x7f0d0684

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    check-cast v0, Landroid/widget/TextView;

    .line 111
    .line 112
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 113
    .line 114
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportPicOptExp;->〇080()Z

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    if-eqz v0, :cond_4

    .line 119
    .line 120
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 121
    .line 122
    const v1, 0x7f080471

    .line 123
    .line 124
    .line 125
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 126
    .line 127
    .line 128
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 129
    .line 130
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    .line 132
    .line 133
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 134
    .line 135
    const/4 v1, -0x2

    .line 136
    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 137
    .line 138
    .line 139
    const v1, 0x800015

    .line 140
    .line 141
    .line 142
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 143
    .line 144
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 145
    .line 146
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    .line 148
    .line 149
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇o88:Landroid/widget/TextView;

    .line 150
    .line 151
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarWrapMenu(Landroid/view/View;)V

    .line 152
    .line 153
    .line 154
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇8ooOO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->O88O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 4
    .line 5
    new-instance v1, LOO〇/o〇0;

    .line 6
    .line 7
    invoke-direct {v1, p0}, LOO〇/o〇0;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇8ooo(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇888(I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "CustomGalleryActivity"

    .line 8
    .line 9
    if-eqz v0, :cond_4

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->isLoadingItem()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8oo〇〇oO(I)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    const/4 v2, 0x0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    const-string p1, "over max count"

    .line 26
    .line 27
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const/4 v0, 0x1

    .line 35
    new-array v0, v0, [Ljava/lang/Object;

    .line 36
    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    iget v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 43
    .line 44
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string v3, ""

    .line 48
    .line 49
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    aput-object v1, v0, v2

    .line 57
    .line 58
    const v1, 0x7f130713

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    return-void

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 70
    .line 71
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇00(I)V

    .line 72
    .line 73
    .line 74
    iget-boolean p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Ooo08:Z

    .line 75
    .line 76
    if-eqz p1, :cond_2

    .line 77
    .line 78
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO0〇O()V

    .line 79
    .line 80
    .line 81
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->notifyDataSetChanged()V

    .line 84
    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 87
    .line 88
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇〇〇0()I

    .line 89
    .line 90
    .line 91
    move-result p1

    .line 92
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇8〇(I)V

    .line 93
    .line 94
    .line 95
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0〇o8〇()V

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 100
    .line 101
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇O8o08O()Ljava/util/ArrayList;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    if-lez v0, :cond_3

    .line 110
    .line 111
    new-instance v0, Landroid/content/Intent;

    .line 112
    .line 113
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 117
    .line 118
    .line 119
    move-result-object p1

    .line 120
    check-cast p1, Landroid/net/Uri;

    .line 121
    .line 122
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 123
    .line 124
    .line 125
    const/4 p1, -0x1

    .line 126
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 127
    .line 128
    .line 129
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 130
    .line 131
    .line 132
    :goto_0
    return-void

    .line 133
    :cond_4
    :goto_1
    const-string p1, "item == null or isLoadingItem"

    .line 134
    .line 135
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇8oo〇〇oO(I)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇()Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 24
    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇()Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    iget v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 38
    .line 39
    if-lt v0, v2, :cond_1

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇888(I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    if-eqz p1, :cond_1

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO0o〇〇()Ljava/util/List;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    xor-int/lit8 p1, p1, 0x1

    .line 60
    .line 61
    return p1

    .line 62
    :cond_1
    :goto_0
    return v1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇8〇0O〇()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/gallery/GalleryCacheManager;->〇8o8o〇()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/gallery/GalleryCacheManager;->〇080:Lcom/intsig/camscanner/gallery/GalleryCacheManager;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryCacheManager;->O8()Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-nez v1, :cond_0

    .line 15
    .line 16
    const-string v1, "CustomGalleryActivity"

    .line 17
    .line 18
    const-string v2, "use cache"

    .line 19
    .line 20
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryCacheManager;->O8()Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OoO8(Ljava/util/List;)V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇8〇〇8o(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0o〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇OoO0o0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o8〇8()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "EXTRA_SHOW_IMPORT_PDF"

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO〇〇:Z

    .line 13
    .line 14
    const-string v1, "extral_enable_multi"

    .line 15
    .line 16
    const/4 v3, 0x1

    .line 17
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Ooo08:Z

    .line 22
    .line 23
    const-string v1, "specific_dir"

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->ooO:Ljava/lang/String;

    .line 30
    .line 31
    const-string v1, "select_item_path"

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 38
    .line 39
    const-string v1, "show_home"

    .line 40
    .line 41
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o〇:Z

    .line 46
    .line 47
    const-string v1, "title_resource"

    .line 48
    .line 49
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    iput v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo80:I

    .line 54
    .line 55
    const-string v1, "has_max_count_limit"

    .line 56
    .line 57
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8oOOo:Z

    .line 62
    .line 63
    const-string v1, "has_min_count_limit"

    .line 64
    .line 65
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O〇〇O8:Z

    .line 70
    .line 71
    const-string v1, "max_count"

    .line 72
    .line 73
    const/16 v4, 0x9

    .line 74
    .line 75
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    iput v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 80
    .line 81
    const-string v1, "min_count"

    .line 82
    .line 83
    const/4 v4, -0x1

    .line 84
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    iput v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O88O:I

    .line 89
    .line 90
    const-string v1, "log_agent_from"

    .line 91
    .line 92
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 97
    .line 98
    const-string v1, "log_agent_from_part"

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oo8ooo8O:Ljava/lang/String;

    .line 105
    .line 106
    const-string v1, "log_agent_type"

    .line 107
    .line 108
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    const-string v4, "extra_page_view_from_part"

    .line 113
    .line 114
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    iput-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇oO:Ljava/lang/String;

    .line 119
    .line 120
    new-instance v4, Ljava/lang/StringBuilder;

    .line 121
    .line 122
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .line 124
    .line 125
    const-string v5, "onCreate  mEnableMuti:"

    .line 126
    .line 127
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    iget-boolean v5, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Ooo08:Z

    .line 131
    .line 132
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    const-string v5, " mLogAgentType="

    .line 136
    .line 137
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    .line 142
    .line 143
    const-string v5, " mLogAgentFrom="

    .line 144
    .line 145
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    iget-object v5, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 149
    .line 150
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    const-string v5, " mMaxCount "

    .line 154
    .line 155
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    iget v5, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o0O:I

    .line 159
    .line 160
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    const-string v5, " mLogAgentFromPart "

    .line 164
    .line 165
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    iget-object v5, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oo8ooo8O:Ljava/lang/String;

    .line 169
    .line 170
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 174
    .line 175
    .line 176
    move-result-object v4

    .line 177
    const-string v5, "CustomGalleryActivity"

    .line 178
    .line 179
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    .line 181
    .line 182
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 183
    .line 184
    const-string v5, "batch"

    .line 185
    .line 186
    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 187
    .line 188
    .line 189
    move-result v4

    .line 190
    if-nez v4, :cond_3

    .line 191
    .line 192
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 193
    .line 194
    const-string v5, "single"

    .line 195
    .line 196
    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 197
    .line 198
    .line 199
    move-result v4

    .line 200
    if-nez v4, :cond_3

    .line 201
    .line 202
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 203
    .line 204
    const-string v5, "doc_list"

    .line 205
    .line 206
    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 207
    .line 208
    .line 209
    move-result v4

    .line 210
    if-nez v4, :cond_3

    .line 211
    .line 212
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 213
    .line 214
    const-string v5, "cs_main"

    .line 215
    .line 216
    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 217
    .line 218
    .line 219
    move-result v4

    .line 220
    if-nez v4, :cond_3

    .line 221
    .line 222
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 223
    .line 224
    const-string v5, "local"

    .line 225
    .line 226
    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 227
    .line 228
    .line 229
    move-result v4

    .line 230
    if-nez v4, :cond_3

    .line 231
    .line 232
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 233
    .line 234
    const-string v5, "cs_list"

    .line 235
    .line 236
    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 237
    .line 238
    .line 239
    move-result v4

    .line 240
    if-eqz v4, :cond_0

    .line 241
    .line 242
    goto :goto_1

    .line 243
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 244
    .line 245
    .line 246
    move-result v1

    .line 247
    if-eqz v1, :cond_2

    .line 248
    .line 249
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 250
    .line 251
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 252
    .line 253
    .line 254
    move-result v1

    .line 255
    if-nez v1, :cond_1

    .line 256
    .line 257
    goto :goto_0

    .line 258
    :cond_1
    const-string v1, "EXTRA_BATMODE_STATE"

    .line 259
    .line 260
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 261
    .line 262
    .line 263
    move-result v1

    .line 264
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇08〇o0O:Z

    .line 265
    .line 266
    goto :goto_2

    .line 267
    :cond_2
    :goto_0
    iput-boolean v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇08〇o0O:Z

    .line 268
    .line 269
    goto :goto_2

    .line 270
    :cond_3
    :goto_1
    iput-boolean v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇08〇o0O:Z

    .line 271
    .line 272
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0O8Oo()V

    .line 273
    .line 274
    .line 275
    const-string v1, "extra_show_import_doc_entrance"

    .line 276
    .line 277
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 278
    .line 279
    .line 280
    move-result v1

    .line 281
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOO0880O:Z

    .line 282
    .line 283
    const-string v1, "extra_from_doc_import"

    .line 284
    .line 285
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 286
    .line 287
    .line 288
    move-result v0

    .line 289
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oOoo80oO:Z

    .line 290
    .line 291
    return-void
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$ScannedDocImages;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oo8〇〇(Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$ScannedDocImages;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇oOO80o(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/camscanner/view/GalleryGridView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO00〇o:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇ooO〇000(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/camscanner/gallery/GalleryGuideManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo0O0o8:Lcom/intsig/camscanner/gallery/GalleryGuideManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇o〇88(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o〇OO80oO(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇O8oOo0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇〇0(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Landroid/content/Intent;

    .line 6
    .line 7
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    if-ne v0, v2, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Landroid/net/Uri;

    .line 19
    .line 20
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const-string v0, "android.intent.extra.STREAM"

    .line 25
    .line 26
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 27
    .line 28
    .line 29
    :goto_0
    const/4 p1, -0x1

    .line 30
    invoke-virtual {p0, p1, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇〇8o0OOOo()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇〇0o〇〇0:Z

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 16
    .line 17
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇O8〇〇o(Z)V

    .line 18
    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->O8oOo80()Landroidx/lifecycle/MutableLiveData;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$ScannedDocImages;

    .line 32
    .line 33
    if-eqz v0, :cond_4

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$ScannedDocImages;->〇o00〇〇Oo()Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$RadarStatus;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    sget-object v3, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$RadarStatus$LoadingEmpty;->〇080:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$RadarStatus$LoadingEmpty;

    .line 40
    .line 41
    if-ne v0, v3, :cond_1

    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 44
    .line 45
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 46
    .line 47
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 51
    .line 52
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 53
    .line 54
    const v1, 0x7f131126

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 61
    .line 62
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇O8〇〇o(Z)V

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_1
    sget-object v3, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$RadarStatus$LoadingMore;->〇080:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$RadarStatus$LoadingMore;

    .line 67
    .line 68
    if-ne v0, v3, :cond_2

    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 71
    .line 72
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 73
    .line 74
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 78
    .line 79
    const/4 v1, 0x1

    .line 80
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇O8〇〇o(Z)V

    .line 81
    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_2
    sget-object v3, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$RadarStatus$LoadingFinish;->〇080:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel$RadarStatus$LoadingFinish;

    .line 85
    .line 86
    if-ne v0, v3, :cond_4

    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 89
    .line 90
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;->〇8o〇〇8080()Ljava/util/List;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-eqz v0, :cond_3

    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 101
    .line 102
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 103
    .line 104
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 105
    .line 106
    .line 107
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 108
    .line 109
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 110
    .line 111
    const v1, 0x7f131123

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 119
    .line 120
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 121
    .line 122
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    .line 124
    .line 125
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 126
    .line 127
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇O8〇〇o(Z)V

    .line 128
    .line 129
    .line 130
    :cond_4
    :goto_1
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇〇〇OOO〇〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO8o〇08〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public OOo00()Lorg/json/JSONObject;
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00O0:Ljava/lang/String;

    .line 7
    .line 8
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    const-string v1, "from"

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇00O0:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19
    .line 20
    .line 21
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 22
    .line 23
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    const-string v2, "from_part"

    .line 28
    .line 29
    if-nez v1, :cond_1

    .line 30
    .line 31
    :try_start_1
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇08oOOO0:Ljava/lang/String;

    .line 32
    .line 33
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 34
    .line 35
    .line 36
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇oO:Ljava/lang/String;

    .line 37
    .line 38
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-nez v1, :cond_2

    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇oO:Ljava/lang/String;

    .line 45
    .line 46
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :catch_0
    move-exception v1

    .line 51
    const-string v2, "CustomGalleryActivity"

    .line 52
    .line 53
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    .line 55
    .line 56
    :cond_2
    :goto_0
    return-object v0
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public OO〇000()Z
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/CommonDeviceUtil;->Oo08(Landroid/content/Context;)Lcom/intsig/utils/CommonDeviceUtil$SystemMemoryInfo;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/utils/CommonDeviceUtil$SystemMemoryInfo;->〇080()J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    const-wide/32 v2, 0x519999

    .line 12
    .line 13
    .line 14
    cmp-long v4, v0, v2

    .line 15
    .line 16
    if-ltz v4, :cond_0

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    return v0
.end method

.method protected Ooo8o()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 7
    .line 8
    .line 9
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇000()Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0O〇O00O:Z

    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇OO8Oo0〇()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    iput p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o0OoOOo0:I

    .line 23
    .line 24
    if-lez p1, :cond_0

    .line 25
    .line 26
    const/4 p1, 0x1

    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 p1, 0x0

    .line 29
    :goto_0
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO〇8O8oOo:Z

    .line 30
    .line 31
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->inflate(Landroid/view/LayoutInflater;)Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setContentView(Landroid/view/View;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇80O8o8O〇:Lcom/intsig/camscanner/EdgeClient;

    .line 53
    .line 54
    invoke-virtual {p1, v0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇80O8o8O〇:Lcom/intsig/camscanner/EdgeClient;

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->O8o08O8O:Landroidx/appcompat/widget/Toolbar;

    .line 60
    .line 61
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/EdgeClient;->〇80〇808〇O(Landroid/view/View;)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇80O8o8O〇:Lcom/intsig/camscanner/EdgeClient;

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 67
    .line 68
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->OO〇00〇8oO:Landroid/widget/RelativeLayout;

    .line 69
    .line 70
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/EdgeClient;->oO80(Landroid/view/View;)V

    .line 71
    .line 72
    .line 73
    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    .line 74
    .line 75
    invoke-direct {p1, p0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 76
    .line 77
    .line 78
    const-class v0, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 79
    .line 80
    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    check-cast p1, Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 85
    .line 86
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇o0〇8:Lcom/intsig/camscanner/gallery/CustomGalleryViewModel;

    .line 87
    .line 88
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇o8〇8()V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o〇o0oOO8()V

    .line 92
    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8ooOO()V

    .line 95
    .line 96
    .line 97
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o0()V

    .line 98
    .line 99
    .line 100
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇08O:Ljava/lang/String;

    .line 101
    .line 102
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0888(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇0O〇()V

    .line 106
    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO8〇O8()V

    .line 109
    .line 110
    .line 111
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x66

    .line 5
    .line 6
    if-ne v0, p1, :cond_3

    .line 7
    .line 8
    const/4 p1, -0x1

    .line 9
    if-ne p2, p1, :cond_4

    .line 10
    .line 11
    if-nez p3, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    const-string p1, "android.intent.extra.STREAM"

    .line 15
    .line 16
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_2

    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    if-eqz p2, :cond_1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o88oo〇O(Ljava/util/ArrayList;)V

    .line 30
    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_2
    :goto_0
    const-string p1, "extra_preview_selections"

    .line 34
    .line 35
    invoke-virtual {p3, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result p2

    .line 39
    if-eqz p2, :cond_4

    .line 40
    .line 41
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    check-cast p1, Ljava/util/ArrayList;

    .line 46
    .line 47
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8〇OO:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;

    .line 48
    .line 49
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇O888o0o(Ljava/util/ArrayList;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO0〇O()V

    .line 53
    .line 54
    .line 55
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0〇o8〇()V

    .line 56
    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_3
    const/16 p2, 0xcb

    .line 60
    .line 61
    if-ne p2, p1, :cond_4

    .line 62
    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o0()V

    .line 64
    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇〇08O:Ljava/lang/String;

    .line 67
    .line 68
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0888(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    invoke-static {p0}, Lcom/intsig/util/PermissionUtil;->〇O888o0o(Landroid/content/Context;)Z

    .line 72
    .line 73
    .line 74
    move-result p1

    .line 75
    if-eqz p1, :cond_4

    .line 76
    .line 77
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OO〇OOo:Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;

    .line 78
    .line 79
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityCustomGalleryBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 80
    .line 81
    const/16 p2, 0x8

    .line 82
    .line 83
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 84
    .line 85
    .line 86
    :cond_4
    :goto_1
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const v0, 0x7f0a1418

    .line 6
    .line 7
    .line 8
    const-string v1, "CustomGalleryActivity"

    .line 9
    .line 10
    if-ne p1, v0, :cond_0

    .line 11
    .line 12
    const-string p1, "export"

    .line 13
    .line 14
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o0o8()V

    .line 18
    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_0
    const v0, 0x7f0a177e

    .line 22
    .line 23
    .line 24
    if-ne p1, v0, :cond_2

    .line 25
    .line 26
    const/4 p1, 0x2

    .line 27
    new-array p1, p1, [Landroid/util/Pair;

    .line 28
    .line 29
    new-instance v0, Landroid/util/Pair;

    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 32
    .line 33
    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    if-eqz v2, :cond_1

    .line 38
    .line 39
    const-string v2, "all"

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    const-string v2, "none"

    .line 43
    .line 44
    :goto_0
    const-string v3, "type"

    .line 45
    .line 46
    invoke-direct {v0, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    const/4 v2, 0x0

    .line 50
    aput-object v0, p1, v2

    .line 51
    .line 52
    new-instance v0, Landroid/util/Pair;

    .line 53
    .line 54
    const-string v2, "from"

    .line 55
    .line 56
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8o:Ljava/lang/String;

    .line 57
    .line 58
    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    const/4 v2, 0x1

    .line 62
    aput-object v0, p1, v2

    .line 63
    .line 64
    const-string v0, "CSImport"

    .line 65
    .line 66
    const-string v2, "select_all"

    .line 67
    .line 68
    invoke-static {v0, v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 69
    .line 70
    .line 71
    new-instance p1, Ljava/lang/StringBuilder;

    .line 72
    .line 73
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .line 75
    .line 76
    const-string v0, "select isChecked="

    .line 77
    .line 78
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 82
    .line 83
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 84
    .line 85
    .line 86
    move-result v0

    .line 87
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O8〇o〇88:Landroid/widget/CheckBox;

    .line 98
    .line 99
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 100
    .line 101
    .line 102
    move-result p1

    .line 103
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O888Oo(Z)V

    .line 104
    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_2
    const v0, 0x7f0a03ca

    .line 108
    .line 109
    .line 110
    if-ne p1, v0, :cond_3

    .line 111
    .line 112
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o088O8800()V

    .line 113
    .line 114
    .line 115
    :cond_3
    :goto_1
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o8O:Lcom/intsig/camscanner/view/GalleryGridView;

    .line 5
    .line 6
    new-instance v0, LOO〇/〇080;

    .line 7
    .line 8
    invoke-direct {v0, p0}, LOO〇/〇080;-><init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v1, 0xc8

    .line 12
    .line 13
    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onDestroy()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onDestroy()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/gallery/GalleryCacheManager;->〇o〇()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O008oO0()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    return p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onStart()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CustomGalleryActivity"

    .line 5
    .line 6
    const-string v1, "onStart"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->OOo00()Lorg/json/JSONObject;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "CSImport"

    .line 16
    .line 17
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 18
    .line 19
    .line 20
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇0O〇O00O:Z

    .line 21
    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    const-string v0, "album_doc_recognize_ram_supported"

    .line 25
    .line 26
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->Oo0O〇8800()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇Oo〇O()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O008oO0()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇Oo〇O()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;
.super Ljava/lang/Object;
.source "SelectAnimationGuidePopClient.java"


# instance fields
.field private final O8:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private OO0o〇〇:Landroid/animation/ValueAnimator;

.field private OO0o〇〇〇〇0:I

.field private final Oo08:Landroid/widget/GridView;

.field private final OoO8:[I

.field private Oooo8o0〇:Landroid/graphics/PathMeasure;

.field private o800o8O:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private oO80:Z

.field private oo88o8O:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final o〇0:I

.field private 〇080:Landroid/app/Dialog;

.field private final 〇0〇O0088o:[I

.field private 〇80〇808〇O:Landroid/view/View;

.field private 〇8o8o〇:Z

.field private 〇O00:I

.field private 〇O888o0o:Landroid/animation/Animator$AnimatorListener;

.field private final 〇O8o08O:Lcom/intsig/camscanner/gallery/DialogShowDismissListener;

.field private final 〇O〇:[F

.field private 〇o00〇〇Oo:Landroid/widget/RelativeLayout;

.field private final 〇o〇:Landroid/app/Activity;

.field private final 〇〇808〇:Landroid/graphics/Path;

.field private 〇〇888:Landroid/widget/ImageView;

.field private 〇〇8O0〇8:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/GridView;Lcom/intsig/camscanner/gallery/DialogShowDismissListener;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 6
    .line 7
    new-instance v0, Landroid/util/SparseArray;

    .line 8
    .line 9
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oO80:Z

    .line 16
    .line 17
    const/4 v0, 0x6

    .line 18
    iput v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇〇〇0:I

    .line 19
    .line 20
    new-instance v0, Landroid/graphics/Path;

    .line 21
    .line 22
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇808〇:Landroid/graphics/Path;

    .line 26
    .line 27
    const/4 v0, 0x2

    .line 28
    new-array v1, v0, [F

    .line 29
    .line 30
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O〇:[F

    .line 31
    .line 32
    const/4 v1, -0x1

    .line 33
    iput v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O00:I

    .line 34
    .line 35
    new-array v1, v0, [I

    .line 36
    .line 37
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇0〇O0088o:[I

    .line 38
    .line 39
    new-array v0, v0, [I

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OoO8:[I

    .line 42
    .line 43
    new-instance v0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient$1;

    .line 44
    .line 45
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient$1;-><init>(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)V

    .line 46
    .line 47
    .line 48
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o800o8O:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 49
    .line 50
    new-instance v0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient$2;

    .line 51
    .line 52
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient$2;-><init>(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)V

    .line 53
    .line 54
    .line 55
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O888o0o:Landroid/animation/Animator$AnimatorListener;

    .line 56
    .line 57
    new-instance v0, Ljava/util/HashSet;

    .line 58
    .line 59
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 60
    .line 61
    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oo88o8O:Ljava/util/Set;

    .line 63
    .line 64
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o〇:Landroid/app/Activity;

    .line 65
    .line 66
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 67
    .line 68
    const/4 p2, 0x5

    .line 69
    invoke-static {p1, p2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    iput p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o〇0:I

    .line 74
    .line 75
    iput-object p3, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O8o08O:Lcom/intsig/camscanner/gallery/DialogShowDismissListener;

    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇oOO8O8(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O8ooOoo〇(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oO80:Z

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o()V

    .line 5
    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 9
    .line 10
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 11
    .line 12
    const/4 p1, 0x0

    .line 13
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0800〇0o(Z)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O8o08O:Lcom/intsig/camscanner/gallery/DialogShowDismissListener;

    .line 17
    .line 18
    if-eqz p1, :cond_0

    .line 19
    .line 20
    invoke-interface {p1}, Lcom/intsig/camscanner/gallery/DialogShowDismissListener;->onDismiss()V

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O8〇o()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, v0, :cond_1

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 11
    .line 12
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    check-cast v2, Landroid/widget/TextView;

    .line 17
    .line 18
    if-eqz v2, :cond_0

    .line 19
    .line 20
    const/4 v3, 0x4

    .line 21
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 25
    .line 26
    .line 27
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oo88o8O:Ljava/util/Set;

    .line 31
    .line 32
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)[F
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O〇:[F

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)[I
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OoO8:[I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OOO〇O0(Landroid/view/View;)V
    .locals 8

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance v7, Landroid/view/animation/ScaleAnimation;

    .line 4
    .line 5
    const v1, 0x3ecccccd    # 0.4f

    .line 6
    .line 7
    .line 8
    const/high16 v2, 0x3f800000    # 1.0f

    .line 9
    .line 10
    const v3, 0x3ecccccd    # 0.4f

    .line 11
    .line 12
    .line 13
    const/high16 v4, 0x3f800000    # 1.0f

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    shr-int/lit8 v0, v0, 0x1

    .line 20
    .line 21
    int-to-float v5, v0

    .line 22
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    shr-int/lit8 v0, v0, 0x1

    .line 27
    .line 28
    int-to-float v6, v0

    .line 29
    move-object v0, v7

    .line 30
    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 31
    .line 32
    .line 33
    const-wide/16 v0, 0x96

    .line 34
    .line 35
    invoke-virtual {v7, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 39
    .line 40
    .line 41
    :cond_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oO80:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O00:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/app/Dialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇8O8〇008()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/app/NoChangingStatusBarDialog;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o〇:Landroid/app/Activity;

    .line 8
    .line 9
    const v2, 0x7f1401d6

    .line 10
    .line 11
    .line 12
    invoke-direct {v0, v1, v2}, Lcom/intsig/app/NoChangingStatusBarDialog;-><init>(Landroid/content/Context;I)V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o〇:Landroid/app/Activity;

    .line 22
    .line 23
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const v1, 0x7f0d0362

    .line 28
    .line 29
    .line 30
    const/4 v2, 0x0

    .line 31
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 38
    .line 39
    const v1, 0x7f0a08cb

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    check-cast v0, Landroid/widget/ImageView;

    .line 47
    .line 48
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇888:Landroid/widget/ImageView;

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 51
    .line 52
    const v1, 0x7f0a14e7

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇80〇808〇O:Landroid/view/View;

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 62
    .line 63
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 64
    .line 65
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 69
    .line 70
    new-instance v1, LOO〇/oo〇;

    .line 71
    .line 72
    invoke-direct {v1, p0}, LOO〇/oo〇;-><init>(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 79
    .line 80
    new-instance v1, LOO〇/O8〇o;

    .line 81
    .line 82
    invoke-direct {v1, p0}, LOO〇/O8〇o;-><init>(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 86
    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 89
    .line 90
    new-instance v1, LOO〇/〇00〇8;

    .line 91
    .line 92
    invoke-direct {v1, p0}, LOO〇/〇00〇8;-><init>(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    .line 97
    .line 98
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇00〇8()V

    .line 99
    .line 100
    .line 101
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oo〇()V

    .line 102
    .line 103
    .line 104
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 105
    .line 106
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    if-nez v0, :cond_1

    .line 111
    .line 112
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 113
    .line 114
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :catch_0
    move-exception v0

    .line 119
    const-string v1, "SelectAnimationGuidePopClient"

    .line 120
    .line 121
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 122
    .line 123
    .line 124
    :cond_1
    :goto_0
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o8(I)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oo88o8O:Ljava/util/Set;

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    check-cast v0, Landroid/widget/TextView;

    .line 21
    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 26
    .line 27
    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    if-nez v1, :cond_2

    .line 32
    .line 33
    return-void

    .line 34
    :cond_2
    const/4 v2, 0x2

    .line 35
    new-array v3, v2, [I

    .line 36
    .line 37
    new-array v2, v2, [I

    .line 38
    .line 39
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 40
    .line 41
    invoke-virtual {v4, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 45
    .line 46
    .line 47
    const/4 v1, 0x0

    .line 48
    aget v4, v3, v1

    .line 49
    .line 50
    aget v5, v2, v1

    .line 51
    .line 52
    sub-int/2addr v4, v5

    .line 53
    aput v4, v3, v1

    .line 54
    .line 55
    const/4 v4, 0x1

    .line 56
    aget v5, v3, v4

    .line 57
    .line 58
    aget v2, v2, v4

    .line 59
    .line 60
    sub-int/2addr v5, v2

    .line 61
    aput v5, v3, v4

    .line 62
    .line 63
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 68
    .line 69
    aget v5, v3, v1

    .line 70
    .line 71
    iget v6, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o〇0:I

    .line 72
    .line 73
    add-int/2addr v5, v6

    .line 74
    invoke-virtual {v2, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 75
    .line 76
    .line 77
    aget v3, v3, v4

    .line 78
    .line 79
    iget v4, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o〇0:I

    .line 80
    .line 81
    add-int/2addr v3, v4

    .line 82
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 83
    .line 84
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    .line 86
    .line 87
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OOO〇O0(Landroid/view/View;)V

    .line 88
    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oo88o8O:Ljava/util/Set;

    .line 91
    .line 92
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    .line 98
    .line 99
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OOO〇O0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇8O0〇8:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo88o8O(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o〇8(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private oo〇()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-lez v0, :cond_2

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v2, 0x0

    .line 17
    :goto_0
    if-ge v2, v0, :cond_1

    .line 18
    .line 19
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 20
    .line 21
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    check-cast v3, Landroid/widget/TextView;

    .line 26
    .line 27
    if-nez v3, :cond_0

    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_0
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 31
    .line 32
    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 33
    .line 34
    .line 35
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 39
    .line 40
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 41
    .line 42
    .line 43
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 44
    .line 45
    invoke-virtual {v0}, Landroid/widget/GridView;->getNumColumns()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    const/4 v2, 0x0

    .line 50
    :goto_2
    mul-int/lit8 v3, v0, 0x2

    .line 51
    .line 52
    if-ge v2, v3, :cond_3

    .line 53
    .line 54
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o〇:Landroid/app/Activity;

    .line 55
    .line 56
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    const v4, 0x7f0d0361

    .line 61
    .line 62
    .line 63
    iget-object v5, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 64
    .line 65
    invoke-virtual {v3, v4, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v3

    .line 69
    check-cast v3, Landroid/widget/TextView;

    .line 70
    .line 71
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 72
    .line 73
    const/4 v5, 0x1

    .line 74
    new-array v5, v5, [Ljava/lang/Object;

    .line 75
    .line 76
    add-int/lit8 v6, v2, 0x1

    .line 77
    .line 78
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 79
    .line 80
    .line 81
    move-result-object v7

    .line 82
    aput-object v7, v5, v1

    .line 83
    .line 84
    const-string v7, "%d"

    .line 85
    .line 86
    invoke-static {v4, v7, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v4

    .line 90
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    .line 92
    .line 93
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8:Landroid/util/SparseArray;

    .line 94
    .line 95
    invoke-virtual {v4, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 96
    .line 97
    .line 98
    const/4 v2, 0x4

    .line 99
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 100
    .line 101
    .line 102
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 103
    .line 104
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 105
    .line 106
    .line 107
    move v2, v6

    .line 108
    goto :goto_2

    .line 109
    :cond_3
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/app/Activity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o〇:Landroid/app/Activity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇8(II)V
    .locals 1

    .line 1
    const/4 v0, -0x1

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    if-ne p2, v0, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    if-le p1, p2, :cond_1

    .line 8
    .line 9
    return-void

    .line 10
    :cond_1
    if-ne p1, v0, :cond_2

    .line 11
    .line 12
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o8(I)V

    .line 13
    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_2
    :goto_0
    if-gt p1, p2, :cond_3

    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o8(I)V

    .line 19
    .line 20
    .line 21
    add-int/lit8 p1, p1, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_3
    :goto_1
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o〇O8〇〇o()V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇oo〇()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x1

    .line 10
    if-gt v1, v2, :cond_0

    .line 11
    .line 12
    new-instance v1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v2, "initAnimationPath views.size()="

    .line 18
    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const-string v1, "SelectAnimationGuidePopClient"

    .line 34
    .line 35
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_0
    const/4 v1, 0x2

    .line 40
    new-array v3, v1, [I

    .line 41
    .line 42
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 43
    .line 44
    invoke-virtual {v4, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 45
    .line 46
    .line 47
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇808〇:Landroid/graphics/Path;

    .line 48
    .line 49
    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 50
    .line 51
    .line 52
    new-array v4, v1, [I

    .line 53
    .line 54
    const/4 v5, 0x0

    .line 55
    const/4 v6, 0x0

    .line 56
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 57
    .line 58
    .line 59
    move-result v7

    .line 60
    if-ge v6, v7, :cond_3

    .line 61
    .line 62
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v7

    .line 66
    check-cast v7, Landroid/view/View;

    .line 67
    .line 68
    if-nez v7, :cond_1

    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_1
    invoke-virtual {v7, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 72
    .line 73
    .line 74
    aget v8, v4, v2

    .line 75
    .line 76
    aget v9, v3, v2

    .line 77
    .line 78
    sub-int/2addr v8, v9

    .line 79
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    .line 80
    .line 81
    .line 82
    move-result v9

    .line 83
    iget-object v10, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇888:Landroid/widget/ImageView;

    .line 84
    .line 85
    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    .line 86
    .line 87
    .line 88
    move-result v10

    .line 89
    sub-int/2addr v9, v10

    .line 90
    div-int/2addr v9, v1

    .line 91
    add-int/2addr v8, v9

    .line 92
    aput v8, v4, v2

    .line 93
    .line 94
    if-nez v6, :cond_2

    .line 95
    .line 96
    aget v7, v4, v5

    .line 97
    .line 98
    aget v9, v3, v5

    .line 99
    .line 100
    sub-int/2addr v7, v9

    .line 101
    aput v7, v4, v5

    .line 102
    .line 103
    iget-object v9, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇808〇:Landroid/graphics/Path;

    .line 104
    .line 105
    int-to-float v7, v7

    .line 106
    int-to-float v8, v8

    .line 107
    invoke-virtual {v9, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 108
    .line 109
    .line 110
    goto :goto_1

    .line 111
    :cond_2
    aget v8, v4, v5

    .line 112
    .line 113
    aget v9, v3, v5

    .line 114
    .line 115
    sub-int/2addr v8, v9

    .line 116
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    .line 117
    .line 118
    .line 119
    move-result v7

    .line 120
    iget-object v9, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇888:Landroid/widget/ImageView;

    .line 121
    .line 122
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    .line 123
    .line 124
    .line 125
    move-result v9

    .line 126
    sub-int/2addr v7, v9

    .line 127
    div-int/2addr v7, v1

    .line 128
    add-int/2addr v8, v7

    .line 129
    aput v8, v4, v5

    .line 130
    .line 131
    iget-object v7, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇808〇:Landroid/graphics/Path;

    .line 132
    .line 133
    int-to-float v8, v8

    .line 134
    aget v9, v4, v2

    .line 135
    .line 136
    int-to-float v9, v9

    .line 137
    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 138
    .line 139
    .line 140
    :goto_1
    add-int/lit8 v6, v6, 0x1

    .line 141
    .line 142
    goto :goto_0

    .line 143
    :cond_3
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic o〇〇0〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇00()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇00()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇8O0〇8:Landroid/view/View;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string v0, "SelectAnimationGuidePopClient"

    .line 13
    .line 14
    const-string v1, "refreshView firstView == null"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o〇O8〇〇o()V

    .line 21
    .line 22
    .line 23
    new-instance v0, Landroid/graphics/PathMeasure;

    .line 24
    .line 25
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇808〇:Landroid/graphics/Path;

    .line 26
    .line 27
    invoke-direct {v0, v2, v1}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oooo8o0〇:Landroid/graphics/PathMeasure;

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o()V

    .line 33
    .line 34
    .line 35
    const/4 v0, 0x2

    .line 36
    new-array v0, v0, [F

    .line 37
    .line 38
    const/4 v2, 0x0

    .line 39
    aput v2, v0, v1

    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oooo8o0〇:Landroid/graphics/PathMeasure;

    .line 42
    .line 43
    invoke-virtual {v1}, Landroid/graphics/PathMeasure;->getLength()F

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    const/4 v2, 0x1

    .line 48
    aput v1, v0, v2

    .line 49
    .line 50
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o800o8O:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 62
    .line 63
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O888o0o:Landroid/animation/Animator$AnimatorListener;

    .line 64
    .line 65
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 69
    .line 70
    const-wide/16 v1, 0x514

    .line 71
    .line 72
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 76
    .line 77
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    .line 78
    .line 79
    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 86
    .line 87
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 88
    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic 〇0000OOO(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-boolean p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇8o8o〇:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇080:Landroid/app/Dialog;

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇00〇8()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇8o8o〇:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oo88o8O:Ljava/util/Set;

    .line 5
    .line 6
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 7
    .line 8
    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->oO80:Z

    .line 10
    .line 11
    const/4 v0, -0x1

    .line 12
    iput v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O00:I

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 15
    .line 16
    const/4 v1, 0x4

    .line 17
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇888:Landroid/widget/ImageView;

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇80〇808〇O:Landroid/view/View;

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8ooOoo〇(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇8o8o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/widget/GridView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇80〇808〇O:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)[I
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇0〇O0088o:[I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O888o0o(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O8〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O00:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/graphics/PathMeasure;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oooo8o0〇:Landroid/graphics/PathMeasure;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/animation/Animator;->removeAllListeners()V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o〇〇0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇oOO8O8(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇O8o08O:Lcom/intsig/camscanner/gallery/DialogShowDismissListener;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-interface {p1}, Lcom/intsig/camscanner/gallery/DialogShowDismissListener;->onShow()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oo〇()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/GridView;->getNumColumns()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    new-instance v2, Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    iget v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇〇〇0:I

    .line 23
    .line 24
    if-gt v1, v0, :cond_0

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 27
    .line 28
    add-int/lit8 v1, v1, -0x1

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    mul-int/lit8 v3, v0, 0x2

    .line 39
    .line 40
    if-ge v1, v3, :cond_2

    .line 41
    .line 42
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 43
    .line 44
    sub-int/2addr v1, v0

    .line 45
    add-int/lit8 v1, v1, -0x1

    .line 46
    .line 47
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 57
    .line 58
    iget v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇〇〇0:I

    .line 59
    .line 60
    add-int/lit8 v1, v1, -0x1

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    if-eqz v0, :cond_4

    .line 67
    .line 68
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 73
    .line 74
    add-int/lit8 v0, v0, -0x1

    .line 75
    .line 76
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    if-eqz v0, :cond_3

    .line 81
    .line 82
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->Oo08:Landroid/widget/GridView;

    .line 86
    .line 87
    add-int/lit8 v3, v3, -0x1

    .line 88
    .line 89
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    if-eqz v0, :cond_4

    .line 94
    .line 95
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    .line 97
    .line 98
    :cond_4
    :goto_0
    return-object v2
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇0000OOO(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇〇888:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)Landroid/animation/ValueAnimator;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇:Landroid/animation/ValueAnimator;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public o0ooO(I)V
    .locals 3

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇〇〇0:I

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o〇:Landroid/app/Activity;

    .line 4
    .line 5
    if-eqz p1, :cond_2

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->OO0o〇〇〇〇0:I

    .line 15
    .line 16
    if-gtz p1, :cond_1

    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->O〇8O8〇008()V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->〇o00〇〇Oo:Landroid/widget/RelativeLayout;

    .line 23
    .line 24
    new-instance v0, LOO〇/OOO〇O0;

    .line 25
    .line 26
    invoke-direct {v0, p0}, LOO〇/OOO〇O0;-><init>(Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;)V

    .line 27
    .line 28
    .line 29
    const-wide/16 v1, 0xc8

    .line 30
    .line 31
    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_2
    :goto_0
    const-string p1, "SelectAnimationGuidePopClient"

    .line 36
    .line 37
    const-string v0, "parentView == null || mCurActivity == null || mCurActivity.isFinishing()"

    .line 38
    .line 39
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.class public Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "GalleryPreviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PageGestureListener;,
        Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;,
        Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;,
        Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;
    }
.end annotation


# static fields
.field private static final 〇00O0:Ljava/lang/String; = "GalleryPreviewActivity"


# instance fields
.field private O0O:Landroid/widget/CheckBox;

.field private O88O:I

.field private Oo80:Landroid/view/animation/Animation;

.field private O〇o88o08〇:Lorg/json/JSONObject;

.field private o8o:Landroid/view/GestureDetector;

.field private o8oOOo:Landroid/widget/TextView;

.field private oOO〇〇:I

.field private oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

.field private ooo0〇〇O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;

.field final o〇oO:Landroid/net/Uri;

.field private 〇08〇o0O:Z

.field private 〇O〇〇O8:Landroidx/viewpager/widget/ViewPager;

.field private 〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

.field private 〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

.field private 〇〇o〇:Landroid/view/animation/Animation;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 10
    .line 11
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o〇oO:Landroid/net/Uri;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇08〇o0O:Z

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private O00OoO〇(Landroid/content/Intent;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "handleIntent start"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    const-string p1, "intent null"

    .line 11
    .line 12
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    const-string v1, "extra_preview_bean"

    .line 17
    .line 18
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    check-cast p1, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 23
    .line 24
    if-nez p1, :cond_1

    .line 25
    .line 26
    const-string p1, "bean null"

    .line 27
    .line 28
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 33
    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v2, "bean: "

    .line 40
    .line 41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇o〇()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    iput v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O88O:I

    .line 59
    .line 60
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 61
    .line 62
    new-instance v1, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$1;

    .line 63
    .line 64
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$1;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;)V

    .line 65
    .line 66
    .line 67
    const p1, 0x7f130e22

    .line 68
    .line 69
    .line 70
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 78
    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇0o0oO〇〇0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O80OO(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8o:Landroid/view/GestureDetector;

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    new-instance p1, Landroid/view/GestureDetector;

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PageGestureListener;

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PageGestureListener;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;LOO〇/o〇O8〇〇o;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p1, p0, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8o:Landroid/view/GestureDetector;

    .line 17
    .line 18
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8o:Landroid/view/GestureDetector;

    .line 19
    .line 20
    invoke-virtual {p1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 21
    .line 22
    .line 23
    const/4 p1, 0x0

    .line 24
    return p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O88()Lorg/json/JSONObject;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇o88o08〇:Lorg/json/JSONObject;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lorg/json/JSONObject;

    .line 6
    .line 7
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇o88o08〇:Lorg/json/JSONObject;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->o〇0()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_2

    .line 23
    .line 24
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇o88o08〇:Lorg/json/JSONObject;

    .line 25
    .line 26
    const-string v1, "from"

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->o〇0()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    sget-object v1, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇00O0:Ljava/lang/String;

    .line 40
    .line 41
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇o00〇〇Oo()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-nez v0, :cond_1

    .line 55
    .line 56
    :try_start_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇o88o08〇:Lorg/json/JSONObject;

    .line 57
    .line 58
    const-string v1, "type"

    .line 59
    .line 60
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 61
    .line 62
    invoke-virtual {v2}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇o00〇〇Oo()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 67
    .line 68
    .line 69
    goto :goto_1

    .line 70
    :catch_1
    move-exception v0

    .line 71
    sget-object v1, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇00O0:Ljava/lang/String;

    .line 72
    .line 73
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 74
    .line 75
    .line 76
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 77
    .line 78
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇0〇O0088o()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    if-nez v0, :cond_2

    .line 87
    .line 88
    :try_start_2
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇o88o08〇:Lorg/json/JSONObject;

    .line 89
    .line 90
    const-string v1, "scheme"

    .line 91
    .line 92
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 93
    .line 94
    invoke-virtual {v2}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇0〇O0088o()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 99
    .line 100
    .line 101
    goto :goto_2

    .line 102
    :catch_2
    move-exception v0

    .line 103
    sget-object v1, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇00O0:Ljava/lang/String;

    .line 104
    .line 105
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    .line 107
    .line 108
    :cond_2
    :goto_2
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇8〇0〇o〇O()Z

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    if-eqz v0, :cond_3

    .line 113
    .line 114
    sget-object v0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇00O0:Ljava/lang/String;

    .line 115
    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    .line 117
    .line 118
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .line 120
    .line 121
    const-string v2, "getFromJsonObject="

    .line 122
    .line 123
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇o88o08〇:Lorg/json/JSONObject;

    .line 127
    .line 128
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v1

    .line 139
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇o88o08〇:Lorg/json/JSONObject;

    .line 143
    .line 144
    return-object v0
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O880O〇(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)Lorg/json/JSONObject;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O88()Lorg/json/JSONObject;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8O(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o0OO(Ljava/util/List;Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O8〇o0〇〇8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇00()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 10
    .line 11
    const/16 v1, 0x8

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OO0O(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)Landroidx/viewpager/widget/ViewPager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇O〇〇O8:Landroidx/viewpager/widget/ViewPager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO0o()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    const v0, 0x7f0a1a88

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇O〇〇O8:Landroidx/viewpager/widget/ViewPager;

    .line 11
    .line 12
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇O〇〇O8:Landroidx/viewpager/widget/ViewPager;

    .line 16
    .line 17
    new-instance v1, LOO〇/oo88o8O;

    .line 18
    .line 19
    invoke-direct {v1, p0}, LOO〇/oo88o8O;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 23
    .line 24
    .line 25
    const v0, 0x7f0a1025

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 33
    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 35
    .line 36
    const v0, 0x7f0a0312

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    check-cast v0, Landroid/widget/CheckBox;

    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O0O:Landroid/widget/CheckBox;

    .line 46
    .line 47
    const v0, 0x7f0a0cd4

    .line 48
    .line 49
    .line 50
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 58
    .line 59
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OO〇〇o0oO(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;Landroid/database/Cursor;)Lcom/intsig/camscanner/gallery/GallerySelectedItem;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇0o88Oo〇(Landroid/database/Cursor;)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic OoO〇OOo8o()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic OooO〇(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oOO〇〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oOO8oo0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O80OO(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O〇0o8o8〇(I)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 v1, 0x1

    .line 5
    add-int/2addr p1, v1

    .line 6
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const/4 v2, 0x0

    .line 11
    aput-object p1, v0, v2

    .line 12
    .line 13
    iget p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oOO〇〇:I

    .line 14
    .line 15
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    aput-object p1, v0, v1

    .line 20
    .line 21
    const-string p1, "%d/%d"

    .line 22
    .line 23
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O〇〇O80o8(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇〇o8O()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f0d0684

    .line 6
    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Landroid/widget/TextView;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 16
    .line 17
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 18
    .line 19
    const/4 v1, -0x2

    .line 20
    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 21
    .line 22
    .line 23
    const v1, 0x800015

    .line 24
    .line 25
    .line 26
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 29
    .line 30
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 34
    .line 35
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarWrapMenu(Landroid/view/View;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 39
    .line 40
    new-instance v1, LOO〇/〇O888o0o;

    .line 41
    .line 42
    invoke-direct {v1, p0}, LOO〇/〇O888o0o;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o0OO(Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 2
    .line 3
    invoke-direct {v0, p0, p2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 7
    .line 8
    new-instance p2, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$2;

    .line 9
    .line 10
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$2;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->O8〇o(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbItemClick;)V

    .line 14
    .line 15
    .line 16
    new-instance p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 17
    .line 18
    invoke-direct {p2, p0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 19
    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 33
    .line 34
    invoke-virtual {v1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 35
    .line 36
    .line 37
    new-instance p2, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;

    .line 38
    .line 39
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-direct {p2, v1, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;-><init>(Landroidx/fragment/app/FragmentManager;Ljava/util/List;)V

    .line 44
    .line 45
    .line 46
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;

    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇O〇〇O8:Landroidx/viewpager/widget/ViewPager;

    .line 49
    .line 50
    invoke-virtual {p1, p2}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇O〇〇O8:Landroidx/viewpager/widget/ViewPager;

    .line 54
    .line 55
    new-instance p2, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$3;

    .line 56
    .line 57
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$3;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1, p2}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 61
    .line 62
    .line 63
    iget p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O88O:I

    .line 64
    .line 65
    if-nez p1, :cond_0

    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O0O:Landroid/widget/CheckBox;

    .line 68
    .line 69
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 70
    .line 71
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;

    .line 72
    .line 73
    invoke-static {v1, v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;->〇080(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->o〇O8〇〇o(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)Z

    .line 78
    .line 79
    .line 80
    move-result p2

    .line 81
    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇O〇〇O8:Landroidx/viewpager/widget/ViewPager;

    .line 86
    .line 87
    invoke-virtual {p2, p1, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    .line 88
    .line 89
    .line 90
    :goto_0
    iget p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O88O:I

    .line 91
    .line 92
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇0o8o8〇(I)V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O0O:Landroid/widget/CheckBox;

    .line 96
    .line 97
    new-instance p2, LOO〇/〇oo〇;

    .line 98
    .line 99
    invoke-direct {p2, p0}, LOO〇/〇oo〇;-><init>(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O8〇o0〇〇8()V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o〇OO80oO()V

    .line 109
    .line 110
    .line 111
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic o0Oo(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)Landroid/widget/CheckBox;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O0O:Landroid/widget/CheckBox;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o808o8o08(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic oOO8oo0(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇00()Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string p1, "import"

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O88()Lorg/json/JSONObject;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "CSAlbumPreview"

    .line 19
    .line 20
    invoke-static {v1, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 21
    .line 22
    .line 23
    new-instance p1, Landroid/content/Intent;

    .line 24
    .line 25
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 26
    .line 27
    .line 28
    new-instance v0, Ljava/util/ArrayList;

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 31
    .line 32
    invoke-static {v1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;)Ljava/util/List;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 37
    .line 38
    .line 39
    const-string v1, "android.intent.extra.STREAM"

    .line 40
    .line 41
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    const/4 v0, -0x1

    .line 45
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 49
    .line 50
    .line 51
    :cond_1
    :goto_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic o〇08oO80o(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇OoO0()V
    .locals 5

    .line 1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/high16 v2, 0x3f800000    # 1.0f

    .line 5
    .line 6
    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->Oo80:Landroid/view/animation/Animation;

    .line 10
    .line 11
    const-wide/16 v3, 0x12c

    .line 12
    .line 13
    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    .line 17
    .line 18
    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇o〇:Landroid/view/animation/Animation;

    .line 22
    .line 23
    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic o〇o08〇(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O88O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇0o0oO〇〇0(Landroid/view/View;)V
    .locals 3

    .line 1
    sget-object p1, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "click mCbChoose"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p1, "CSAlbumPreview"

    .line 9
    .line 10
    const-string v0, "select"

    .line 11
    .line 12
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;

    .line 16
    .line 17
    iget v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O88O:I

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;->〇080(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$PictureSlidePagerAdapter;I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇oo〇(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    const/4 v1, 0x1

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇OoO0o0()Z

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    if-eqz v2, :cond_0

    .line 37
    .line 38
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    new-array v0, v1, [Ljava/lang/Object;

    .line 43
    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 50
    .line 51
    invoke-virtual {v2}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇80〇808〇O()I

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    const-string v2, ""

    .line 59
    .line 60
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const/4 v2, 0x0

    .line 68
    aput-object v1, v0, v2

    .line 69
    .line 70
    const v1, 0x7f130713

    .line 71
    .line 72
    .line 73
    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O0O:Landroid/widget/CheckBox;

    .line 81
    .line 82
    invoke-virtual {p1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 83
    .line 84
    .line 85
    return-void

    .line 86
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 87
    .line 88
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇oOO8O8(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V

    .line 89
    .line 90
    .line 91
    if-eqz v0, :cond_1

    .line 92
    .line 93
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 96
    .line 97
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->getItemCount()I

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    sub-int/2addr v0, v1

    .line 102
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 103
    .line 104
    .line 105
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O8〇o0〇〇8()V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o〇OO80oO()V

    .line 109
    .line 110
    .line 111
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇0o88Oo〇(Landroid/database/Cursor;)Lcom/intsig/camscanner/gallery/GallerySelectedItem;
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 8
    .line 9
    .line 10
    move-result-wide v1

    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o〇oO:Landroid/net/Uri;

    .line 12
    .line 13
    invoke-static {p1, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    new-instance v1, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 18
    .line 19
    invoke-direct {v1, v0, p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 20
    .line 21
    .line 22
    return-object v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇OoO0o0()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇O888o0o()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->getItemCount()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇80〇808〇O()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-lt v0, v1, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇oO88o(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O88O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static 〇oOO80o(Landroid/content/Context;Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;)Landroid/content/Intent;
    .locals 2

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    const-class v1, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;

    .line 4
    .line 5
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 6
    .line 7
    .line 8
    const-string p0, "extra_preview_bean"

    .line 9
    .line 10
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇0o8o8〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇ooO〇000(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O8〇o0〇〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O〇00O()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇08〇o0O:Z

    .line 2
    .line 3
    xor-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇08〇o0O:Z

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o〇OoO0()V

    .line 8
    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇08〇o0O:Z

    .line 11
    .line 12
    const v1, 0x7f0a0cd4

    .line 13
    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {p0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/16 v2, 0x8

    .line 22
    .line 23
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->O8o08O8O:Landroidx/appcompat/widget/Toolbar;

    .line 27
    .line 28
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇o〇:Landroid/view/animation/Animation;

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->O8o08O8O:Landroidx/appcompat/widget/Toolbar;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇o〇:Landroid/view/animation/Animation;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇00()Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-nez v0, :cond_2

    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 56
    .line 57
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 61
    .line 62
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇o〇:Landroid/view/animation/Animation;

    .line 63
    .line 64
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 69
    .line 70
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇00()Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    const/4 v2, 0x0

    .line 75
    if-nez v0, :cond_1

    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 78
    .line 79
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇〇08O:Landroidx/recyclerview/widget/RecyclerView;

    .line 83
    .line 84
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->Oo80:Landroid/view/animation/Animation;

    .line 85
    .line 86
    invoke-virtual {v0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 87
    .line 88
    .line 89
    :cond_1
    invoke-virtual {p0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->O8o08O8O:Landroidx/appcompat/widget/Toolbar;

    .line 97
    .line 98
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {p0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->Oo80:Landroid/view/animation/Animation;

    .line 106
    .line 107
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 108
    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->O8o08O8O:Landroidx/appcompat/widget/Toolbar;

    .line 111
    .line 112
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->Oo80:Landroid/view/animation/Animation;

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 115
    .line 116
    .line 117
    :cond_2
    :goto_0
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o〇OoO0()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->OO0o()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O〇〇o8O()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->O00OoO〇(Landroid/content/Intent;)V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method o0O0O〇〇〇0(I)Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇O888o0o()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x1

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x2

    .line 12
    new-array v0, v0, [Ljava/lang/Object;

    .line 13
    .line 14
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    aput-object p1, v0, v1

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->〇80〇808〇O()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    aput-object p1, v0, v2

    .line 31
    .line 32
    const p1, 0x7f13013b

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    return-object p1

    .line 40
    :cond_0
    new-array v0, v2, [Ljava/lang/Object;

    .line 41
    .line 42
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    aput-object p1, v0, v1

    .line 47
    .line 48
    const p1, 0x7f13013c

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    return-object p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public oOoO8OO〇()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSAlbumPreview"

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d0034

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇Oo〇O()Z
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-static {v1}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;)Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    new-instance v1, Ljava/util/ArrayList;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 19
    .line 20
    invoke-static {v2}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;)Ljava/util/List;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 25
    .line 26
    .line 27
    const-string v2, "extra_preview_selections"

    .line 28
    .line 29
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 30
    .line 31
    .line 32
    :cond_0
    const/4 v1, -0x1

    .line 33
    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 34
    .line 35
    .line 36
    sget-object v0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇00O0:Ljava/lang/String;

    .line 37
    .line 38
    const-string v1, "onBackPressed"

    .line 39
    .line 40
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const-string v0, "CSAlbumPreview"

    .line 44
    .line 45
    const-string v1, "back"

    .line 46
    .line 47
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-super {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇Oo〇O()Z

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    return v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method 〇o〇OO80oO()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->〇o0O:Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity$ImageThumbAdapter;->getItemCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 8
    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o0O0O〇〇〇0(I)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14
    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    const v2, -0x7f636364

    .line 18
    .line 19
    .line 20
    if-lez v0, :cond_3

    .line 21
    .line 22
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 23
    .line 24
    invoke-virtual {v3}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->oo88o8O()Z

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    const/4 v4, 0x1

    .line 29
    const/4 v5, -0x1

    .line 30
    if-eqz v3, :cond_2

    .line 31
    .line 32
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 33
    .line 34
    iget-object v6, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 35
    .line 36
    invoke-virtual {v6}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->OO0o〇〇〇〇0()I

    .line 37
    .line 38
    .line 39
    move-result v6

    .line 40
    if-lt v0, v6, :cond_0

    .line 41
    .line 42
    const/4 v1, 0x1

    .line 43
    :cond_0
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 44
    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 47
    .line 48
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->oo8ooo8O:Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;

    .line 49
    .line 50
    invoke-virtual {v3}, Lcom/intsig/camscanner/gallery/GalleryPreviewParamBean;->OO0o〇〇〇〇0()I

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    if-lt v0, v3, :cond_1

    .line 55
    .line 56
    const/4 v2, -0x1

    .line 57
    :cond_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 62
    .line 63
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 67
    .line 68
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 73
    .line 74
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryPreviewActivity;->o8oOOo:Landroid/widget/TextView;

    .line 78
    .line 79
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 80
    .line 81
    .line 82
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

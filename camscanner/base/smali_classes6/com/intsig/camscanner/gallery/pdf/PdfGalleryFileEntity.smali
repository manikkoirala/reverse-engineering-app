.class public Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;
.super Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;
.source "PdfGalleryFileEntity.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ALL:I = 0x0

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXCEL:I = 0x4

.field public static final PDF:I = 0x1

.field public static final PPT:I = 0x2

.field public static final UNKNOWN:I = -0x1

.field public static final WORD:I = 0x3


# instance fields
.field private addTime:J

.field private checked:Z

.field private mime:I

.field private mimeType:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private size:J

.field private title:Ljava/lang/String;

.field private uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;-><init>()V

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;-><init>()V

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->title:Ljava/lang/String;

    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->checked:Z

    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->addTime:J

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->path:Ljava/lang/String;

    .line 20
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->uri:Landroid/net/Uri;

    .line 21
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mimeType:Ljava/lang/String;

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mime:I

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->size:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->title:Ljava/lang/String;

    .line 12
    iput-object p4, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->path:Ljava/lang/String;

    .line 13
    iput-object p5, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->uri:Landroid/net/Uri;

    .line 14
    sget-object p1, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;->FILE:Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->setType(Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JZLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->title:Ljava/lang/String;

    .line 4
    iput-boolean p4, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->checked:Z

    .line 5
    iput-wide p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->addTime:J

    .line 6
    iput-object p5, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->path:Ljava/lang/String;

    .line 7
    iput-object p6, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->uri:Landroid/net/Uri;

    .line 8
    sget-object p1, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;->FILE:Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->setType(Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;)V

    .line 9
    invoke-virtual {p0, p7}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->setMimeType(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAddTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->addTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMime()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mime:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mimeType:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->path:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSize()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->size:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->title:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->uri:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isChecked()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->checked:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setAddTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->addTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setChecked(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->checked:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setMime(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mime:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mimeType:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 6
    .line 7
    .line 8
    const-string v0, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    .line 9
    .line 10
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    const-string v0, "application/vnd.ms-powerpoint"

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    if-nez p1, :cond_0

    .line 23
    .line 24
    const/4 p1, 0x1

    .line 25
    iput p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mime:I

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    const/4 p1, 0x2

    .line 29
    iput p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mime:I

    .line 30
    .line 31
    :cond_1
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->path:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setSize(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->size:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->title:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setUri(Landroid/net/Uri;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->uri:Landroid/net/Uri;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->title:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->checked:Z

    .line 7
    .line 8
    int-to-byte v0, v0

    .line 9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 10
    .line 11
    .line 12
    iget-wide v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->addTime:J

    .line 13
    .line 14
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->path:Ljava/lang/String;

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->uri:Landroid/net/Uri;

    .line 23
    .line 24
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 25
    .line 26
    .line 27
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mimeType:Ljava/lang/String;

    .line 28
    .line 29
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    iget p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->mime:I

    .line 33
    .line 34
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 35
    .line 36
    .line 37
    iget-wide v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->size:J

    .line 38
    .line 39
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

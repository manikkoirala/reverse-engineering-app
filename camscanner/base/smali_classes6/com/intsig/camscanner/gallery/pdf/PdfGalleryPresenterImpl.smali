.class public Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;
.super Ljava/lang/Object;
.source "PdfGalleryPresenterImpl.java"

# interfaces
.implements Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenter;
.implements Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;
.implements Lcom/intsig/camscanner/gallery/pdf/DataClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$CheckerAsy;,
        Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$CheckerResultListener;
    }
.end annotation


# instance fields
.field private final O8:Landroid/view/View$OnClickListener;

.field private OO0o〇〇:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Ljava/lang/String;

.field private final Oo08:Landroid/view/View$OnClickListener;

.field private Oooo8o0〇:Z

.field private final oO80:Landroid/view/View$OnClickListener;

.field private final o〇0:Landroid/view/View$OnClickListener;

.field private final 〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

.field private 〇80〇808〇O:Ljava/lang/String;

.field private 〇8o8o〇:Z

.field private 〇O8o08O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

.field private 〇o〇:Lcom/intsig/camscanner/gallery/pdf/PdfGallerySearchAdapter;

.field private 〇〇808〇:Z

.field private final 〇〇888:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$1;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$1;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O8:Landroid/view/View$OnClickListener;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$2;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->Oo08:Landroid/view/View$OnClickListener;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$3;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$3;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->o〇0:Landroid/view/View$OnClickListener;

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$4;

    .line 26
    .line 27
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$4;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇〇888:Landroid/view/View$OnClickListener;

    .line 31
    .line 32
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$5;

    .line 33
    .line 34
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$5;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)V

    .line 35
    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->oO80:Landroid/view/View$OnClickListener;

    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇〇808〇:Z

    .line 41
    .line 42
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O8ooOoo〇(Ljava/lang/String;)Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;->NETWORK_DISK:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->Oo08:Landroid/view/View$OnClickListener;

    .line 7
    .line 8
    invoke-direct {v0, v1, p1, v2, v3}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;Ljava/lang/String;Landroid/content/Intent;Landroid/view/View$OnClickListener;)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O8〇o(Landroid/content/Context;)Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8o0〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {p1}, Lcom/intsig/util/PermissionUtil;->O8ooOoo〇(Landroid/content/Context;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OOO〇O0()[Ljava/lang/String;
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇8o8o〇:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x7

    .line 14
    new-array v0, v0, [Ljava/lang/String;

    .line 15
    .line 16
    sget-object v3, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->PDF:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 17
    .line 18
    invoke-virtual {v3}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getMimeType()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    aput-object v3, v0, v1

    .line 23
    .line 24
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->DOC:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 25
    .line 26
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getMimeType()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    aput-object v1, v0, v2

    .line 31
    .line 32
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->DOCX:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getMimeType()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const/4 v2, 0x2

    .line 39
    aput-object v1, v0, v2

    .line 40
    .line 41
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->XLS:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getMimeType()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    const/4 v2, 0x3

    .line 48
    aput-object v1, v0, v2

    .line 49
    .line 50
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->XLSX:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 51
    .line 52
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getMimeType()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const/4 v2, 0x4

    .line 57
    aput-object v1, v0, v2

    .line 58
    .line 59
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->PPT:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getMimeType()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    const/4 v2, 0x5

    .line 66
    aput-object v1, v0, v2

    .line 67
    .line 68
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->PPTX:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 69
    .line 70
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getMimeType()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    const/4 v2, 0x6

    .line 75
    aput-object v1, v0, v2

    .line 76
    .line 77
    return-object v0

    .line 78
    :cond_0
    new-array v0, v2, [Ljava/lang/String;

    .line 79
    .line 80
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->PDF:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 81
    .line 82
    invoke-virtual {v2}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getMimeType()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    aput-object v2, v0, v1

    .line 87
    .line 88
    return-object v0
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->oo〇(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇8O8〇008()Landroid/util/ArrayMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/util/ArrayMap;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 10
    .line 11
    return-object v0

    .line 12
    :cond_0
    new-instance v0, Landroid/util/ArrayMap;

    .line 13
    .line 14
    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 18
    .line 19
    invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_3

    .line 32
    .line 33
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    check-cast v2, Ljava/util/Map$Entry;

    .line 38
    .line 39
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    check-cast v3, Ljava/lang/String;

    .line 44
    .line 45
    const-string v4, "com.google.android"

    .line 46
    .line 47
    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    if-eqz v3, :cond_2

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->〇8()Z

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    if-nez v3, :cond_1

    .line 58
    .line 59
    :cond_2
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v3

    .line 63
    check-cast v3, Ljava/lang/String;

    .line 64
    .line 65
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    check-cast v2, Landroid/content/Intent;

    .line 70
    .line 71
    invoke-virtual {v0, v3, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_3
    return-object v0
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo88o8O(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)[Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OOO〇O0()[Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic oo〇(Ljava/util/List;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇O8o08O:Ljava/util/List;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇oOO8O8()Landroid/util/Pair;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇oOO8O8(Ljava/util/List;Landroid/util/Pair;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇O8〇〇o(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$7;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$7;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)V

    .line 8
    .line 9
    .line 10
    invoke-static {p1, v0, v1}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static o〇〇0〇()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "application/pdf"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇00()Landroid/util/ArrayMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/util/ArrayMap;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 10
    .line 11
    return-object v0

    .line 12
    :cond_0
    new-instance v0, Landroid/util/ArrayMap;

    .line 13
    .line 14
    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 18
    .line 19
    invoke-virtual {v1}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_2

    .line 32
    .line 33
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    check-cast v2, Ljava/util/Map$Entry;

    .line 38
    .line 39
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    check-cast v3, Ljava/lang/String;

    .line 44
    .line 45
    invoke-static {v3}, LO8O〇8oo08/〇080;->〇o〇(Ljava/lang/String;)Z

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    if-nez v3, :cond_1

    .line 50
    .line 51
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    check-cast v3, Ljava/lang/String;

    .line 56
    .line 57
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    check-cast v2, Landroid/content/Intent;

    .line 62
    .line 63
    invoke-virtual {v0, v3, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_2
    return-object v0
    .line 68
.end method

.method public static 〇0000OOO()[Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "application/pdf"

    .line 2
    .line 3
    filled-new-array {v0}, [Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)Landroid/view/View$OnClickListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O8:Landroid/view/View$OnClickListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇80〇808〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O888o0o(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->o〇O8〇〇o(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oOO8O8()Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;",
            ">;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;",
            ">;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 12
    .line 13
    sget-object v3, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;->SYSTEM_FILE_MANAGER:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 14
    .line 15
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O8:Landroid/view/View$OnClickListener;

    .line 16
    .line 17
    const v5, 0x7f1307c9

    .line 18
    .line 19
    .line 20
    const v6, 0x7f08041e

    .line 21
    .line 22
    .line 23
    invoke-direct {v2, v5, v6, v3, v4}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;-><init>(IILcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;Landroid/view/View$OnClickListener;)V

    .line 24
    .line 25
    .line 26
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    iget-boolean v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇〇808〇:Z

    .line 30
    .line 31
    if-nez v2, :cond_0

    .line 32
    .line 33
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    invoke-virtual {v2}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowingWechatMiniImport()Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-eqz v2, :cond_0

    .line 42
    .line 43
    new-instance v2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 44
    .line 45
    sget-object v3, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;->WE_CHAT:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 46
    .line 47
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->oO80:Landroid/view/View$OnClickListener;

    .line 48
    .line 49
    const v5, 0x7f1310f9

    .line 50
    .line 51
    .line 52
    const v6, 0x7f080dee

    .line 53
    .line 54
    .line 55
    invoke-direct {v2, v5, v6, v3, v4}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;-><init>(IILcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;Landroid/view/View$OnClickListener;)V

    .line 56
    .line 57
    .line 58
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 62
    .line 63
    invoke-interface {v2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    const/4 v3, 0x1

    .line 68
    invoke-static {v2, v3}, Lcom/intsig/camscanner/gallery/pdf/util/NetworkDiskUtils;->〇o00〇〇Oo(Landroid/content/Context;Z)Landroid/util/ArrayMap;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    iput-object v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O〇8O8〇008()Landroid/util/ArrayMap;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    iput-object v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 79
    .line 80
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇00()Landroid/util/ArrayMap;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    invoke-virtual {v2}, Landroid/util/ArrayMap;->entrySet()Ljava/util/Set;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 93
    .line 94
    .line 95
    move-result v4

    .line 96
    if-eqz v4, :cond_4

    .line 97
    .line 98
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 99
    .line 100
    .line 101
    move-result-object v4

    .line 102
    check-cast v4, Ljava/util/Map$Entry;

    .line 103
    .line 104
    new-instance v5, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 105
    .line 106
    sget-object v6, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;->NETWORK_DISK:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 107
    .line 108
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object v7

    .line 112
    check-cast v7, Ljava/lang/String;

    .line 113
    .line 114
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    check-cast v4, Landroid/content/Intent;

    .line 119
    .line 120
    iget-object v8, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->Oo08:Landroid/view/View$OnClickListener;

    .line 121
    .line 122
    invoke-direct {v5, v6, v7, v4, v8}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;Ljava/lang/String;Landroid/content/Intent;Landroid/view/View$OnClickListener;)V

    .line 123
    .line 124
    .line 125
    invoke-static {}, Lcom/intsig/camscanner/fit/AndroidRGreenModeHelper;->Oo08()Z

    .line 126
    .line 127
    .line 128
    move-result v4

    .line 129
    if-eqz v4, :cond_1

    .line 130
    .line 131
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    .line 133
    .line 134
    goto :goto_0

    .line 135
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 136
    .line 137
    .line 138
    move-result v4

    .line 139
    const/4 v6, 0x3

    .line 140
    if-lt v4, v6, :cond_3

    .line 141
    .line 142
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 143
    .line 144
    .line 145
    move-result v4

    .line 146
    if-ne v4, v6, :cond_2

    .line 147
    .line 148
    new-instance v4, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 149
    .line 150
    sget-object v6, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;->MORE:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 151
    .line 152
    iget-object v7, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->o〇0:Landroid/view/View$OnClickListener;

    .line 153
    .line 154
    const v8, 0x7f130b4f

    .line 155
    .line 156
    .line 157
    const v9, 0x7f08041d

    .line 158
    .line 159
    .line 160
    invoke-direct {v4, v8, v9, v6, v7}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;-><init>(IILcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;Landroid/view/View$OnClickListener;)V

    .line 161
    .line 162
    .line 163
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    .line 165
    .line 166
    :cond_2
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    .line 168
    .line 169
    goto :goto_0

    .line 170
    :cond_3
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    .line 172
    .line 173
    goto :goto_0

    .line 174
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->〇o00〇〇Oo()Z

    .line 175
    .line 176
    .line 177
    move-result v2

    .line 178
    if-eqz v2, :cond_5

    .line 179
    .line 180
    const-string v2, "com.dropbox.android"

    .line 181
    .line 182
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O8ooOoo〇(Ljava/lang/String;)Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 183
    .line 184
    .line 185
    move-result-object v2

    .line 186
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    .line 188
    .line 189
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->o〇0()Z

    .line 190
    .line 191
    .line 192
    move-result v2

    .line 193
    if-eqz v2, :cond_6

    .line 194
    .line 195
    const-string v2, "com.microsoft.skydrive"

    .line 196
    .line 197
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O8ooOoo〇(Ljava/lang/String;)Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 198
    .line 199
    .line 200
    move-result-object v2

    .line 201
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    .line 203
    .line 204
    :cond_6
    invoke-static {}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->O8()Z

    .line 205
    .line 206
    .line 207
    move-result v2

    .line 208
    if-eqz v2, :cond_7

    .line 209
    .line 210
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->〇8()Z

    .line 211
    .line 212
    .line 213
    move-result v2

    .line 214
    if-nez v2, :cond_7

    .line 215
    .line 216
    const-string v2, "com.google.android"

    .line 217
    .line 218
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O8ooOoo〇(Ljava/lang/String;)Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 219
    .line 220
    .line 221
    move-result-object v2

    .line 222
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    .line 224
    .line 225
    :cond_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 226
    .line 227
    .line 228
    move-result v2

    .line 229
    if-lez v2, :cond_8

    .line 230
    .line 231
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 232
    .line 233
    .line 234
    move-result v2

    .line 235
    sub-int/2addr v2, v3

    .line 236
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 237
    .line 238
    .line 239
    move-result-object v2

    .line 240
    check-cast v2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 241
    .line 242
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->〇0〇O0088o(Z)V

    .line 243
    .line 244
    .line 245
    :cond_8
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 246
    .line 247
    .line 248
    move-result-object v0

    .line 249
    return-object v0
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static bridge synthetic 〇oo〇(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;Landroid/content/Context;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O8〇o(Landroid/content/Context;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)Landroid/util/ArrayMap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇:Landroid/util/ArrayMap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇O888o0o()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8ooOoo〇(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO0o〇〇〇〇0(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o〇:Lcom/intsig/camscanner/gallery/pdf/PdfGallerySearchAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGallerySearchAdapter;->〇〇8O0〇8(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oo08(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->O8〇o(Landroid/content/Context;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->o〇O8〇〇o(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    new-instance p1, Landroid/content/Intent;

    .line 32
    .line 33
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v1, "intent_result_path_list"

    .line 37
    .line 38
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇80〇808〇O:Ljava/lang/String;

    .line 42
    .line 43
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_1

    .line 48
    .line 49
    const-string v0, "intent_result_log_agent_from_part"

    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇80〇808〇O:Ljava/lang/String;

    .line 52
    .line 53
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    .line 55
    .line 56
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 57
    .line 58
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->oo8ooo8O(Landroid/content/Intent;)V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OoO8(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->OoO8(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O〇O〇oO(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->O〇O〇oO(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO80()Landroid/content/Intent;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->oo88o8O()Ljava/util/ArrayList;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-lez v1, :cond_1

    .line 14
    .line 15
    new-instance v1, Landroid/content/Intent;

    .line 16
    .line 17
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "intent_result_path_list"

    .line 21
    .line 22
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇80〇808〇O:Ljava/lang/String;

    .line 26
    .line 27
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-nez v0, :cond_0

    .line 32
    .line 33
    const-string v0, "intent_result_log_agent_from_part"

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇80〇808〇O:Ljava/lang/String;

    .line 36
    .line 37
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    .line 39
    .line 40
    :cond_0
    return-object v1

    .line 41
    :cond_1
    const/4 v0, 0x0

    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o〇〇0〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGallerySearchAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇O8o08O:Ljava/util/List;

    .line 10
    .line 11
    invoke-direct {v0, v1, v2, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGallerySearchAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/gallery/pdf/DataClickListener;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o〇:Lcom/intsig/camscanner/gallery/pdf/PdfGallerySearchAdapter;

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method public 〇80〇808〇O()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$CheckerAsy;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-boolean v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇8o8o〇:Z

    .line 10
    .line 11
    new-instance v3, Lcom/intsig/camscanner/gallery/pdf/〇o00〇〇Oo;

    .line 12
    .line 13
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/gallery/pdf/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$CheckerAsy;-><init>(Landroid/content/Context;ZLcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$CheckerResultListener;)V

    .line 17
    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const/4 v2, 0x0

    .line 24
    new-array v2, v2, [Ljava/lang/Void;

    .line 25
    .line 26
    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇8o8o〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->Oooo8o0〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8o08O(Landroid/widget/LinearLayout;Landroidx/recyclerview/widget/RecyclerView;Landroid/content/Intent;)V
    .locals 8

    .line 1
    const-string v0, "intent_checked_path_list"

    .line 2
    .line 3
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 4
    .line 5
    .line 6
    move-result-object v3

    .line 7
    const-string v0, "intent_log_agent_from_part"

    .line 8
    .line 9
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇80〇808〇O:Ljava/lang/String;

    .line 14
    .line 15
    const-string v0, "intent_log_agent_from"

    .line 16
    .line 17
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 22
    .line 23
    const-string v0, "intent_only_select_pdf"

    .line 24
    .line 25
    const/4 v7, 0x0

    .line 26
    invoke-virtual {p3, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇8o8o〇:Z

    .line 31
    .line 32
    const-string v0, "intent_single_selection"

    .line 33
    .line 34
    invoke-virtual {p3, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->Oooo8o0〇:Z

    .line 39
    .line 40
    const-string v0, "INTENT_FILTER_WECHAT_DIR"

    .line 41
    .line 42
    invoke-virtual {p3, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇〇808〇:Z

    .line 47
    .line 48
    const-string v0, "intent_special_submit_res"

    .line 49
    .line 50
    const/4 v1, -0x1

    .line 51
    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 52
    .line 53
    .line 54
    move-result p3

    .line 55
    if-lez p3, :cond_0

    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 58
    .line 59
    invoke-interface {v0, p3}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->〇0O〇O00O(I)V

    .line 60
    .line 61
    .line 62
    :cond_0
    if-eqz p1, :cond_2

    .line 63
    .line 64
    iget-boolean p3, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->Oooo8o0〇:Z

    .line 65
    .line 66
    if-eqz p3, :cond_1

    .line 67
    .line 68
    const/16 p3, 0x8

    .line 69
    .line 70
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_1
    invoke-virtual {p1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 75
    .line 76
    .line 77
    :cond_2
    :goto_0
    new-instance p1, Lorg/json/JSONObject;

    .line 78
    .line 79
    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 80
    .line 81
    .line 82
    :try_start_0
    iget-object p3, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇80〇808〇O:Ljava/lang/String;

    .line 83
    .line 84
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 85
    .line 86
    .line 87
    move-result p3

    .line 88
    if-nez p3, :cond_3

    .line 89
    .line 90
    const-string p3, "from_part"

    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇80〇808〇O:Ljava/lang/String;

    .line 93
    .line 94
    invoke-virtual {p1, p3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 95
    .line 96
    .line 97
    :cond_3
    iget-object p3, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 98
    .line 99
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 100
    .line 101
    .line 102
    move-result p3

    .line 103
    if-nez p3, :cond_4

    .line 104
    .line 105
    const-string p3, "from"

    .line 106
    .line 107
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 108
    .line 109
    invoke-virtual {p1, p3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .line 111
    .line 112
    goto :goto_1

    .line 113
    :catch_0
    move-exception p3

    .line 114
    const-string v0, "PdfGalleryPresenterImpl"

    .line 115
    .line 116
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    .line 118
    .line 119
    :cond_4
    :goto_1
    const-string p3, "CSPdfImport"

    .line 120
    .line 121
    invoke-static {p3, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 122
    .line 123
    .line 124
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 125
    .line 126
    iget-object p3, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 127
    .line 128
    invoke-interface {p3}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    iget-boolean v6, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->Oooo8o0〇:Z

    .line 133
    .line 134
    move-object v1, p1

    .line 135
    move-object v4, p0

    .line 136
    move-object v5, p0

    .line 137
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;Lcom/intsig/camscanner/gallery/pdf/DataClickListener;Z)V

    .line 138
    .line 139
    .line 140
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 141
    .line 142
    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 143
    .line 144
    .line 145
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 146
    .line 147
    invoke-interface {p1, v7}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->O88O(I)V

    .line 148
    .line 149
    .line 150
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;

    .line 151
    .line 152
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇〇888:Landroid/view/View$OnClickListener;

    .line 153
    .line 154
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇0000OOO(Landroid/view/View$OnClickListener;)V

    .line 155
    .line 156
    .line 157
    return-void
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public 〇o00〇〇Oo()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "PdfGalleryPresenterImpl"

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const-string v0, "goToImportWechatFile BUT activity is NULL!"

    .line 12
    .line 13
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    invoke-static {}, Lcom/intsig/wechat/WeChatApi;->Oo08()Lcom/intsig/wechat/WeChatApi;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/intsig/wechat/WeChatApi;->〇8o8o〇()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    const-string v0, "wei xin not installed!"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    sget-object v0, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog;->O8o08O8O:Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 35
    .line 36
    invoke-interface {v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;->〇8o8o〇(Landroid/app/Activity;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 45
    .line 46
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-nez v0, :cond_2

    .line 55
    .line 56
    const-string v0, "goToImportWechatFile BUT NOT LOGIN!"

    .line 57
    .line 58
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 62
    .line 63
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    const/16 v1, 0x3eb

    .line 68
    .line 69
    invoke-static {v0, v1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇〇〇0(Landroid/app/Activity;I)V

    .line 70
    .line 71
    .line 72
    return-void

    .line 73
    :cond_2
    const/4 v4, 0x3

    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 75
    .line 76
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->〇OO0()Lcom/intsig/camscanner/newsign/CsImportUsage;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    if-eqz v0, :cond_3

    .line 81
    .line 82
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/CsImportUsage;->usageIsESign()Z

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    if-eqz v0, :cond_3

    .line 87
    .line 88
    const/4 v0, 0x1

    .line 89
    goto :goto_0

    .line 90
    :cond_3
    const/4 v0, 0x0

    .line 91
    :goto_0
    sget-object v2, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog;->O8o08O8O:Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;

    .line 92
    .line 93
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 94
    .line 95
    invoke-interface {v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 96
    .line 97
    .line 98
    move-result-object v3

    .line 99
    const/4 v5, 0x0

    .line 100
    const/4 v6, 0x0

    .line 101
    const/4 v7, 0x0

    .line 102
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v8

    .line 106
    const/4 v9, 0x0

    .line 107
    invoke-virtual/range {v2 .. v9}, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;->Oo08(Landroid/app/Activity;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 111
    .line 112
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    if-eqz v0, :cond_4

    .line 117
    .line 118
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 119
    .line 120
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    if-nez v0, :cond_4

    .line 129
    .line 130
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇080:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;

    .line 131
    .line 132
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryView;->getContext()Landroid/app/Activity;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 137
    .line 138
    .line 139
    :cond_4
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇o〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;->〇o〇:Lcom/intsig/camscanner/gallery/pdf/PdfGallerySearchAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGallerySearchAdapter;->〇O00()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888(Lcom/intsig/owlery/MessageView;)V
    .locals 11

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->o〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 12
    .line 13
    .line 14
    const v2, 0x7f08057a

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, v2}, Lcom/intsig/owlery/MessageView;->setMessageIcon(I)V

    .line 18
    .line 19
    .line 20
    const v2, 0x7f08025e

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1, v2}, Lcom/intsig/owlery/MessageView;->setRootViewBackground(I)V

    .line 24
    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->O8()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    const v3, 0x7f060208

    .line 31
    .line 32
    .line 33
    if-eqz v2, :cond_1

    .line 34
    .line 35
    const v2, 0x7f13114c

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    invoke-virtual {p1, v2, v0}, Lcom/intsig/owlery/MessageView;->〇〇888(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    const v2, 0x7f13114f

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v5

    .line 57
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 58
    .line 59
    .line 60
    move-result v6

    .line 61
    const v2, 0x7f130b28

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v7

    .line 68
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 69
    .line 70
    .line 71
    move-result v8

    .line 72
    const/4 v9, 0x0

    .line 73
    const/4 v10, 0x1

    .line 74
    move-object v4, p1

    .line 75
    invoke-virtual/range {v4 .. v10}, Lcom/intsig/owlery/MessageView;->〇80〇808〇O(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Z)V

    .line 76
    .line 77
    .line 78
    :goto_0
    invoke-virtual {p1, v1}, Lcom/intsig/owlery/MessageView;->setShowClose(Z)V

    .line 79
    .line 80
    .line 81
    const/4 v0, 0x0

    .line 82
    invoke-virtual {p1, v0}, Lcom/intsig/owlery/MessageView;->setGenElevation(F)V

    .line 83
    .line 84
    .line 85
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$6;

    .line 86
    .line 87
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl$6;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryPresenterImpl;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p1, v0}, Lcom/intsig/owlery/MessageView;->setCallBack(Lcom/intsig/owlery/MessageView$CallBack;)V

    .line 91
    .line 92
    .line 93
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

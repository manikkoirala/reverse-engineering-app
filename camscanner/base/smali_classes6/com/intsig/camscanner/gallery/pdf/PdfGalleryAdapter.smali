.class public Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "PdfGalleryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;,
        Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;,
        Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private O8o08O8O:I

.field private final OO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final OO〇00〇8oO:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private final o0:Landroid/content/Context;

.field private final o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;

.field private final oOo0:Z

.field private oOo〇8o008:Z

.field private ooo0〇〇O:Landroid/view/View$OnClickListener;

.field private final o〇00O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080OO8〇0:I

.field private final 〇08O〇00〇o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;",
            ">;"
        }
    .end annotation
.end field

.field private 〇0O:Z

.field private final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/pdf/DataClickListener;

.field private 〇OOo8〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;Lcom/intsig/camscanner/gallery/pdf/DataClickListener;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;",
            "Lcom/intsig/camscanner/gallery/pdf/DataClickListener;",
            "Z)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇0O:Z

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$1;

    .line 8
    .line 9
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$1;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO〇00〇8oO:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o0:Landroid/content/Context;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o〇00O:Ljava/util/List;

    .line 17
    .line 18
    iput-boolean p5, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->oOo0:Z

    .line 19
    .line 20
    iput-object p3, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;

    .line 21
    .line 22
    iput-object p4, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/pdf/DataClickListener;

    .line 23
    .line 24
    new-instance p1, Ljava/util/ArrayList;

    .line 25
    .line 26
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 27
    .line 28
    .line 29
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 30
    .line 31
    new-instance p1, Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .line 35
    .line 36
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO:Ljava/util/List;

    .line 37
    .line 38
    new-instance p1, Ljava/util/ArrayList;

    .line 39
    .line 40
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .line 42
    .line 43
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇08O〇00〇o:Ljava/util/List;

    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇8O8〇008(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_4

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v2, 0x1

    .line 12
    if-eq p1, v2, :cond_3

    .line 13
    .line 14
    const/4 v3, 0x2

    .line 15
    if-eq p1, v3, :cond_1

    .line 16
    .line 17
    return v1

    .line 18
    :cond_1
    if-lt v0, v2, :cond_2

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    :cond_2
    return v1

    .line 22
    :cond_3
    const/16 p1, 0x9

    .line 23
    .line 24
    if-lt v0, p1, :cond_4

    .line 25
    .line 26
    const/4 v1, 0x1

    .line 27
    :cond_4
    :goto_0
    return v1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇00(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static o〇O8〇〇o(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->o〇0(Ljava/lang/String;)Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    if-eqz p0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 p0, 0x0

    .line 21
    :goto_0
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;->〇080(Ljava/lang/String;)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    if-eqz p0, :cond_1

    .line 26
    .line 27
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 28
    .line 29
    .line 30
    move-result p0

    .line 31
    return p0

    .line 32
    :cond_1
    const p0, 0x7f080aa2

    .line 33
    .line 34
    .line 35
    return p0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇00(Z)V
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇oo〇()I

    .line 11
    .line 12
    .line 13
    move-result v3

    .line 14
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O〇8O8〇008(I)Z

    .line 15
    .line 16
    .line 17
    move-result v4

    .line 18
    iget-boolean v5, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇0O:Z

    .line 19
    .line 20
    if-ne v5, v0, :cond_1

    .line 21
    .line 22
    iget-boolean v5, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->oOo〇8o008:Z

    .line 23
    .line 24
    if-eq v5, v4, :cond_9

    .line 25
    .line 26
    :cond_1
    xor-int/lit8 v5, v4, 0x1

    .line 27
    .line 28
    const/4 v6, 0x0

    .line 29
    :goto_1
    iget-object v7, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v7}, Ljava/util/List;->size()I

    .line 32
    .line 33
    .line 34
    move-result v7

    .line 35
    if-ge v6, v7, :cond_7

    .line 36
    .line 37
    iget-object v7, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 38
    .line 39
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v7

    .line 43
    check-cast v7, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 44
    .line 45
    instance-of v8, v7, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 46
    .line 47
    if-eqz v8, :cond_2

    .line 48
    .line 49
    check-cast v7, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 50
    .line 51
    invoke-virtual {v7, v0}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->setEnable(Z)V

    .line 52
    .line 53
    .line 54
    goto :goto_3

    .line 55
    :cond_2
    instance-of v8, v7, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 56
    .line 57
    if-eqz v8, :cond_6

    .line 58
    .line 59
    check-cast v7, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 60
    .line 61
    invoke-virtual {v7}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->isChecked()Z

    .line 62
    .line 63
    .line 64
    move-result v8

    .line 65
    if-eqz v8, :cond_3

    .line 66
    .line 67
    invoke-virtual {v7, v2}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->setEnable(Z)V

    .line 68
    .line 69
    .line 70
    goto :goto_3

    .line 71
    :cond_3
    if-eqz v3, :cond_5

    .line 72
    .line 73
    invoke-virtual {v7}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getMime()I

    .line 74
    .line 75
    .line 76
    move-result v8

    .line 77
    if-ne v8, v3, :cond_4

    .line 78
    .line 79
    goto :goto_2

    .line 80
    :cond_4
    invoke-virtual {v7, v1}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->setEnable(Z)V

    .line 81
    .line 82
    .line 83
    goto :goto_3

    .line 84
    :cond_5
    :goto_2
    invoke-virtual {v7, v5}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->setEnable(Z)V

    .line 85
    .line 86
    .line 87
    :cond_6
    :goto_3
    add-int/lit8 v6, v6, 0x1

    .line 88
    .line 89
    goto :goto_1

    .line 90
    :cond_7
    if-eqz p1, :cond_8

    .line 91
    .line 92
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 93
    .line 94
    .line 95
    :cond_8
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇0O:Z

    .line 96
    .line 97
    iput-boolean v4, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->oOo〇8o008:Z

    .line 98
    .line 99
    :cond_9
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;

    .line 100
    .line 101
    if-eqz p1, :cond_a

    .line 102
    .line 103
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 104
    .line 105
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;->OoO8(I)V

    .line 106
    .line 107
    .line 108
    :cond_a
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;)Lcom/intsig/camscanner/gallery/pdf/DataClickListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/pdf/DataClickListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o0:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oo〇()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_2

    .line 5
    .line 6
    iget v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 7
    .line 8
    if-nez v2, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    if-eqz v2, :cond_2

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    check-cast v2, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 26
    .line 27
    instance-of v3, v2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 28
    .line 29
    if-eqz v3, :cond_1

    .line 30
    .line 31
    check-cast v2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 32
    .line 33
    invoke-virtual {v2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->isChecked()Z

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    if-eqz v3, :cond_1

    .line 38
    .line 39
    invoke-virtual {v2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getMime()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    return v0

    .line 44
    :cond_2
    :goto_0
    return v1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O8ooOoo〇(Z)V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇080OO8〇0:I

    .line 4
    .line 5
    if-lt v0, v1, :cond_0

    .line 6
    .line 7
    if-nez p1, :cond_4

    .line 8
    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_2

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 26
    .line 27
    instance-of v2, v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 28
    .line 29
    if-eqz v2, :cond_1

    .line 30
    .line 31
    check-cast v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 32
    .line 33
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->setChecked(Z)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_2
    const/4 v0, 0x0

    .line 38
    if-eqz p1, :cond_3

    .line 39
    .line 40
    iget p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇080OO8〇0:I

    .line 41
    .line 42
    iput p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_3
    iput v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 46
    .line 47
    :goto_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇00(Z)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 51
    .line 52
    .line 53
    :cond_4
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OOO〇O0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇08O〇00〇o:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 23
    .line 24
    instance-of v2, v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 25
    .line 26
    if-eqz v2, :cond_0

    .line 27
    .line 28
    check-cast v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 31
    .line 32
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;

    .line 37
    .line 38
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 39
    .line 40
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;->O〇O〇oO(I)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$4;->〇080:[I

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->getType()Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    aget p1, v0, p1

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    if-eq p1, v0, :cond_0

    .line 23
    .line 24
    const p1, 0x7f0d047a

    .line 25
    .line 26
    .line 27
    return p1

    .line 28
    :cond_0
    const p1, 0x7f0d047b

    .line 29
    .line 30
    .line 31
    return p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 9
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getItemViewType()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const v1, 0x7f0d047b

    .line 6
    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    const/16 v3, 0x8

    .line 10
    .line 11
    const/4 v4, 0x0

    .line 12
    const/high16 v5, 0x3f800000    # 1.0f

    .line 13
    .line 14
    const v6, 0x3df5c28f    # 0.12f

    .line 15
    .line 16
    .line 17
    if-eq v0, v1, :cond_8

    .line 18
    .line 19
    check-cast p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 22
    .line 23
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object p2

    .line 27
    check-cast p2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;

    .line 28
    .line 29
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->〇080()Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    sget-object v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;->SYSTEM_FILE_MANAGER:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 34
    .line 35
    if-ne v0, v1, :cond_0

    .line 36
    .line 37
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 38
    .line 39
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->ooo0〇〇O:Landroid/view/View$OnClickListener;

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o0:Landroid/content/Context;

    .line 50
    .line 51
    invoke-static {v0}, Lcom/intsig/camscanner/util/DarkModeUtils;->〇080(Landroid/content/Context;)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_1

    .line 56
    .line 57
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 58
    .line 59
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    if-eqz v0, :cond_1

    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o0:Landroid/content/Context;

    .line 66
    .line 67
    const v7, 0x7f060208

    .line 68
    .line 69
    .line 70
    invoke-static {v1, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_0
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 79
    .line 80
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 84
    .line 85
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    .line 87
    .line 88
    :cond_1
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->〇080()Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    sget-object v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;->NETWORK_DISK:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity$DirType;

    .line 93
    .line 94
    if-ne v0, v1, :cond_5

    .line 95
    .line 96
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->〇80〇808〇O()Z

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-eqz v0, :cond_2

    .line 101
    .line 102
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->o0:Landroid/widget/TextView;

    .line 103
    .line 104
    const-string v1, "Dropbox"

    .line 105
    .line 106
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    .line 108
    .line 109
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 110
    .line 111
    const v1, 0x7f080641

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 115
    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->OO0o〇〇〇〇0()Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-eqz v0, :cond_3

    .line 123
    .line 124
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->o0:Landroid/widget/TextView;

    .line 125
    .line 126
    const-string v1, "Google Drive"

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    .line 130
    .line 131
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 132
    .line 133
    const v1, 0x7f080645

    .line 134
    .line 135
    .line 136
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 137
    .line 138
    .line 139
    goto :goto_1

    .line 140
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->〇O00()Z

    .line 141
    .line 142
    .line 143
    move-result v0

    .line 144
    if-eqz v0, :cond_4

    .line 145
    .line 146
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->o0:Landroid/widget/TextView;

    .line 147
    .line 148
    const-string v1, "OneDrive"

    .line 149
    .line 150
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    .line 152
    .line 153
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 154
    .line 155
    const v1, 0x7f080648

    .line 156
    .line 157
    .line 158
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 159
    .line 160
    .line 161
    goto :goto_1

    .line 162
    :cond_4
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->o0:Landroid/widget/TextView;

    .line 163
    .line 164
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o0:Landroid/content/Context;

    .line 165
    .line 166
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->〇o〇()Landroid/content/Intent;

    .line 167
    .line 168
    .line 169
    move-result-object v7

    .line 170
    invoke-static {v1, v7}, Lcom/intsig/camscanner/gallery/pdf/util/NetworkDiskUtils;->O8(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/CharSequence;

    .line 171
    .line 172
    .line 173
    move-result-object v1

    .line 174
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    .line 176
    .line 177
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 178
    .line 179
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o0:Landroid/content/Context;

    .line 180
    .line 181
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->〇o〇()Landroid/content/Intent;

    .line 182
    .line 183
    .line 184
    move-result-object v7

    .line 185
    invoke-static {v1, v7}, Lcom/intsig/camscanner/gallery/pdf/util/NetworkDiskUtils;->〇o〇(Landroid/content/Context;Landroid/content/Intent;)Landroid/graphics/drawable/Drawable;

    .line 186
    .line 187
    .line 188
    move-result-object v1

    .line 189
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    .line 191
    .line 192
    :goto_1
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 193
    .line 194
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->Oo08()Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v1

    .line 198
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 199
    .line 200
    .line 201
    goto :goto_2

    .line 202
    :cond_5
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->o0:Landroid/widget/TextView;

    .line 203
    .line 204
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->o〇0()I

    .line 205
    .line 206
    .line 207
    move-result v1

    .line 208
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 209
    .line 210
    .line 211
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 212
    .line 213
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->〇o00〇〇Oo()I

    .line 214
    .line 215
    .line 216
    move-result v1

    .line 217
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 218
    .line 219
    .line 220
    :goto_2
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->isEnable()Z

    .line 221
    .line 222
    .line 223
    move-result v0

    .line 224
    if-eqz v0, :cond_6

    .line 225
    .line 226
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 227
    .line 228
    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 229
    .line 230
    .line 231
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->o0:Landroid/widget/TextView;

    .line 232
    .line 233
    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 234
    .line 235
    .line 236
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 237
    .line 238
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->O8()Landroid/view/View$OnClickListener;

    .line 239
    .line 240
    .line 241
    move-result-object v1

    .line 242
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    .line 244
    .line 245
    goto :goto_3

    .line 246
    :cond_6
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 247
    .line 248
    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    .line 249
    .line 250
    .line 251
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->o0:Landroid/widget/TextView;

    .line 252
    .line 253
    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    .line 254
    .line 255
    .line 256
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 257
    .line 258
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    .line 260
    .line 261
    :goto_3
    iget-object p1, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;->〇08O〇00〇o:Landroid/view/View;

    .line 262
    .line 263
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;->Oooo8o0〇()Z

    .line 264
    .line 265
    .line 266
    move-result p2

    .line 267
    if-eqz p2, :cond_7

    .line 268
    .line 269
    const/4 v3, 0x0

    .line 270
    :cond_7
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 271
    .line 272
    .line 273
    goto/16 :goto_6

    .line 274
    .line 275
    :cond_8
    check-cast p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;

    .line 276
    .line 277
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 278
    .line 279
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 280
    .line 281
    .line 282
    move-result-object v0

    .line 283
    check-cast v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 284
    .line 285
    iget-object v1, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 286
    .line 287
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getTitle()Ljava/lang/String;

    .line 288
    .line 289
    .line 290
    move-result-object v7

    .line 291
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    .line 293
    .line 294
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 295
    .line 296
    .line 297
    move-result-object v1

    .line 298
    const-string v7, "/storage/emulated/0"

    .line 299
    .line 300
    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 301
    .line 302
    .line 303
    move-result v8

    .line 304
    if-eqz v8, :cond_9

    .line 305
    .line 306
    const-string v8, ".."

    .line 307
    .line 308
    invoke-virtual {v1, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 309
    .line 310
    .line 311
    move-result-object v1

    .line 312
    :cond_9
    const-string v7, "/"

    .line 313
    .line 314
    invoke-virtual {v1, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    .line 315
    .line 316
    .line 317
    move-result v7

    .line 318
    if-lez v7, :cond_a

    .line 319
    .line 320
    invoke-virtual {v1, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 321
    .line 322
    .line 323
    move-result-object v1

    .line 324
    :cond_a
    iget-object v7, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->OO:Landroid/widget/TextView;

    .line 325
    .line 326
    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    .line 328
    .line 329
    iget-boolean v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->oOo0:Z

    .line 330
    .line 331
    if-eqz v1, :cond_b

    .line 332
    .line 333
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 334
    .line 335
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 336
    .line 337
    .line 338
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o0:Landroid/widget/LinearLayout;

    .line 339
    .line 340
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 341
    .line 342
    .line 343
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o0:Landroid/widget/LinearLayout;

    .line 344
    .line 345
    new-instance v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$2;

    .line 346
    .line 347
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;)V

    .line 348
    .line 349
    .line 350
    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    .line 352
    .line 353
    goto :goto_5

    .line 354
    :cond_b
    iget-object v1, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 355
    .line 356
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 357
    .line 358
    .line 359
    iget-object v1, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 360
    .line 361
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 362
    .line 363
    .line 364
    move-result-object p2

    .line 365
    invoke-virtual {v1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 366
    .line 367
    .line 368
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 369
    .line 370
    invoke-virtual {p2, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 371
    .line 372
    .line 373
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 374
    .line 375
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->isChecked()Z

    .line 376
    .line 377
    .line 378
    move-result v1

    .line 379
    invoke-virtual {p2, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 380
    .line 381
    .line 382
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->isEnable()Z

    .line 383
    .line 384
    .line 385
    move-result p2

    .line 386
    if-eqz p2, :cond_c

    .line 387
    .line 388
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->OO:Landroid/widget/TextView;

    .line 389
    .line 390
    invoke-virtual {p2, v5}, Landroid/view/View;->setAlpha(F)V

    .line 391
    .line 392
    .line 393
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 394
    .line 395
    invoke-virtual {p2, v5}, Landroid/view/View;->setAlpha(F)V

    .line 396
    .line 397
    .line 398
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 399
    .line 400
    invoke-virtual {p2, v5}, Landroid/view/View;->setAlpha(F)V

    .line 401
    .line 402
    .line 403
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 404
    .line 405
    invoke-virtual {p2, v5}, Landroid/view/View;->setAlpha(F)V

    .line 406
    .line 407
    .line 408
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 409
    .line 410
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO〇00〇8oO:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 411
    .line 412
    invoke-virtual {p2, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 413
    .line 414
    .line 415
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o0:Landroid/widget/LinearLayout;

    .line 416
    .line 417
    new-instance v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$3;

    .line 418
    .line 419
    invoke-direct {v1, p0, p1, v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$3;-><init>(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;)V

    .line 420
    .line 421
    .line 422
    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    .line 424
    .line 425
    goto :goto_4

    .line 426
    :cond_c
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->OO:Landroid/widget/TextView;

    .line 427
    .line 428
    invoke-virtual {p2, v6}, Landroid/view/View;->setAlpha(F)V

    .line 429
    .line 430
    .line 431
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 432
    .line 433
    invoke-virtual {p2, v6}, Landroid/view/View;->setAlpha(F)V

    .line 434
    .line 435
    .line 436
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 437
    .line 438
    invoke-virtual {p2, v6}, Landroid/view/View;->setAlpha(F)V

    .line 439
    .line 440
    .line 441
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 442
    .line 443
    invoke-virtual {p2, v6}, Landroid/view/View;->setAlpha(F)V

    .line 444
    .line 445
    .line 446
    :goto_4
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatCheckBox;

    .line 447
    .line 448
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->isEnable()Z

    .line 449
    .line 450
    .line 451
    move-result v1

    .line 452
    invoke-virtual {p2, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 453
    .line 454
    .line 455
    :goto_5
    invoke-static {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o〇O8〇〇o(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;)I

    .line 456
    .line 457
    .line 458
    move-result p2

    .line 459
    iget-object p1, p1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 460
    .line 461
    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 462
    .line 463
    .line 464
    :goto_6
    return-void
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const v0, 0x7f0d047b

    .line 13
    .line 14
    .line 15
    if-eq p2, v0, :cond_0

    .line 16
    .line 17
    new-instance p2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;

    .line 18
    .line 19
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfDirViewHolder;-><init>(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object p2

    .line 23
    :cond_0
    new-instance p2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;

    .line 24
    .line 25
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$PdfGalleryPdfFileViewHolder;-><init>(Landroid/view/View;)V

    .line 26
    .line 27
    .line 28
    return-object p2
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oo88o8O()Ljava/util/ArrayList;
    .locals 12
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    const/4 v3, 0x0

    .line 10
    move-object v4, v2

    .line 11
    if-eqz v1, :cond_7

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    const/4 v5, 0x0

    .line 15
    :goto_0
    iget-object v6, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 18
    .line 19
    .line 20
    move-result v6

    .line 21
    if-ge v1, v6, :cond_8

    .line 22
    .line 23
    iget-object v6, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 24
    .line 25
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v6

    .line 29
    check-cast v6, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 30
    .line 31
    instance-of v7, v6, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 32
    .line 33
    if-nez v7, :cond_0

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_0
    move-object v7, v6

    .line 37
    check-cast v7, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 38
    .line 39
    invoke-virtual {v7}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->isChecked()Z

    .line 40
    .line 41
    .line 42
    move-result v8

    .line 43
    if-nez v8, :cond_1

    .line 44
    .line 45
    goto :goto_2

    .line 46
    :cond_1
    invoke-virtual {v7}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v8

    .line 50
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 51
    .line 52
    .line 53
    move-result v8

    .line 54
    const-string v9, "PdfGalleryAdapter"

    .line 55
    .line 56
    const/4 v10, 0x1

    .line 57
    if-eqz v8, :cond_3

    .line 58
    .line 59
    const-string v5, "file path is empty"

    .line 60
    .line 61
    invoke-static {v9, v5}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    if-nez v4, :cond_2

    .line 65
    .line 66
    new-instance v4, Ljava/util/ArrayList;

    .line 67
    .line 68
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .line 70
    .line 71
    :cond_2
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    :goto_1
    const/4 v5, 0x1

    .line 75
    goto :goto_2

    .line 76
    :cond_3
    new-instance v8, Ljava/io/File;

    .line 77
    .line 78
    invoke-virtual {v7}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v11

    .line 82
    invoke-direct {v8, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    .line 86
    .line 87
    .line 88
    move-result v8

    .line 89
    if-nez v8, :cond_5

    .line 90
    .line 91
    const-string v5, "file is not exists"

    .line 92
    .line 93
    invoke-static {v9, v5}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    if-nez v4, :cond_4

    .line 97
    .line 98
    new-instance v4, Ljava/util/ArrayList;

    .line 99
    .line 100
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .line 102
    .line 103
    :cond_4
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_5
    if-nez v5, :cond_6

    .line 108
    .line 109
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    .line 111
    .line 112
    :cond_6
    :goto_2
    add-int/lit8 v1, v1, 0x1

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_7
    const/4 v5, 0x0

    .line 116
    :cond_8
    if-eqz v5, :cond_b

    .line 117
    .line 118
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-lez v0, :cond_a

    .line 123
    .line 124
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 125
    .line 126
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 127
    .line 128
    .line 129
    move-result v1

    .line 130
    if-lt v0, v1, :cond_a

    .line 131
    .line 132
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇080OO8〇0:I

    .line 133
    .line 134
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    if-lt v0, v1, :cond_a

    .line 139
    .line 140
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 141
    .line 142
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 143
    .line 144
    .line 145
    move-result v1

    .line 146
    sub-int/2addr v0, v1

    .line 147
    iput v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 148
    .line 149
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇080OO8〇0:I

    .line 150
    .line 151
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 152
    .line 153
    .line 154
    move-result v1

    .line 155
    sub-int/2addr v0, v1

    .line 156
    iput v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇080OO8〇0:I

    .line 157
    .line 158
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 163
    .line 164
    .line 165
    move-result v1

    .line 166
    if-eqz v1, :cond_9

    .line 167
    .line 168
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 169
    .line 170
    .line 171
    move-result-object v1

    .line 172
    check-cast v1, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 173
    .line 174
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 175
    .line 176
    invoke-interface {v4, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 177
    .line 178
    .line 179
    goto :goto_3

    .line 180
    :cond_9
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇00(Z)V

    .line 181
    .line 182
    .line 183
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 184
    .line 185
    .line 186
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o0:Landroid/content/Context;

    .line 187
    .line 188
    const v1, 0x7f130766

    .line 189
    .line 190
    .line 191
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 192
    .line 193
    .line 194
    :cond_a
    return-object v2

    .line 195
    :cond_b
    return-object v0
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public o〇〇0〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO:Ljava/util/List;

    .line 9
    .line 10
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;->O〇O〇oO(I)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇0000OOO(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->ooo0〇〇O:Landroid/view/View$OnClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O888o0o()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    const/4 v2, 0x0

    .line 9
    if-lez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    if-eqz v0, :cond_4

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const/4 v3, 0x0

    .line 23
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    if-eqz v4, :cond_2

    .line 28
    .line 29
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    check-cast v4, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 34
    .line 35
    invoke-virtual {v4}, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;->getType()Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    sget-object v5, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;->FILE:Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity$Type;

    .line 40
    .line 41
    if-ne v4, v5, :cond_1

    .line 42
    .line 43
    add-int/lit8 v3, v3, 0x1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_2
    const/16 v0, 0x9

    .line 47
    .line 48
    if-ge v3, v0, :cond_3

    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_3
    const/4 v1, 0x0

    .line 52
    :goto_2
    return v1

    .line 53
    :cond_4
    return v2
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇oOO8O8(Ljava/util/List;Landroid/util/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;",
            ">;",
            "Landroid/util/Pair<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;",
            ">;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/pdf/PdfGalleryDirEntity;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO:Ljava/util/List;

    .line 8
    .line 9
    check-cast v0, Ljava/util/Collection;

    .line 10
    .line 11
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 12
    .line 13
    .line 14
    :cond_0
    iget-object p2, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 15
    .line 16
    if-eqz p2, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇08O〇00〇o:Ljava/util/List;

    .line 19
    .line 20
    check-cast p2, Ljava/util/Collection;

    .line 21
    .line 22
    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 23
    .line 24
    .line 25
    :cond_1
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO:Ljava/util/List;

    .line 26
    .line 27
    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 28
    .line 29
    .line 30
    new-instance p2, Ljava/util/ArrayList;

    .line 31
    .line 32
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .line 34
    .line 35
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->OO:Ljava/util/List;

    .line 38
    .line 39
    invoke-interface {p2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 40
    .line 41
    .line 42
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    iput p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇080OO8〇0:I

    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;

    .line 49
    .line 50
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 51
    .line 52
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 53
    .line 54
    .line 55
    move-result p2

    .line 56
    invoke-interface {p1, p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter$DataChangedListener;->O〇O〇oO(I)V

    .line 57
    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o〇00O:Ljava/util/List;

    .line 60
    .line 61
    if-eqz p1, :cond_6

    .line 62
    .line 63
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    if-lez p1, :cond_6

    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇OOo8〇0:Ljava/util/List;

    .line 70
    .line 71
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 76
    .line 77
    .line 78
    move-result p2

    .line 79
    if-eqz p2, :cond_5

    .line 80
    .line 81
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    move-result-object p2

    .line 85
    check-cast p2, Lcom/intsig/camscanner/gallery/pdf/BasePdfGalleryEntity;

    .line 86
    .line 87
    instance-of v0, p2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 88
    .line 89
    if-eqz v0, :cond_4

    .line 90
    .line 91
    check-cast p2, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o〇00O:Ljava/util/List;

    .line 94
    .line 95
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    if-eqz v1, :cond_4

    .line 104
    .line 105
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    check-cast v1, Ljava/lang/String;

    .line 110
    .line 111
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object v2

    .line 115
    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 116
    .line 117
    .line 118
    move-result v2

    .line 119
    if-eqz v2, :cond_3

    .line 120
    .line 121
    iget v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 122
    .line 123
    const/4 v2, 0x1

    .line 124
    add-int/2addr v0, v2

    .line 125
    iput v0, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->O8o08O8O:I

    .line 126
    .line 127
    invoke-virtual {p2, v2}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->setChecked(Z)V

    .line 128
    .line 129
    .line 130
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o〇00O:Ljava/util/List;

    .line 131
    .line 132
    invoke-interface {p2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 133
    .line 134
    .line 135
    :cond_4
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->o〇00O:Ljava/util/List;

    .line 136
    .line 137
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 138
    .line 139
    .line 140
    move-result p2

    .line 141
    if-nez p2, :cond_2

    .line 142
    .line 143
    :cond_5
    const/4 p1, 0x0

    .line 144
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryAdapter;->〇00(Z)V

    .line 145
    .line 146
    .line 147
    :cond_6
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 148
    .line 149
    .line 150
    return-void
    .line 151
    .line 152
    .line 153
    .line 154
.end method

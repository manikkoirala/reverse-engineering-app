.class public final Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;
.super Landroid/widget/RelativeLayout;
.source "HlRippleAnimView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleListener;,
        Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleTouchListener;,
        Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇00O0:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O88O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o08O8O:I

.field private final OO:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO〇00〇8oO:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo80:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇o88o08〇:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleTouchListener;

.field private o0:Z

.field private final o8o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8oOOo:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO0〇0o:Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOO〇〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:Z

.field private final oOo〇8o008:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oo8ooo8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final ooo0〇〇O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:F

.field private final o〇oO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:F

.field private final 〇08〇o0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleListener;

.field private 〇8〇oO〇〇8o:Z

.field private 〇OOo8〇0:Landroid/graphics/Bitmap;

.field private final 〇O〇〇O8:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇08O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇o〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇00O0:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO:Landroid/graphics/Path;

    .line 3
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$valueAnimator$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$valueAnimator$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOo〇8o008:Lkotlin/Lazy;

    .line 4
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO〇00〇8oO:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;

    .line 5
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/〇080;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/〇080;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8〇OO0〇0o:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    const/4 p1, 0x1

    .line 6
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇8〇oO〇〇8o:Z

    .line 7
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textPaint$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textPaint$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->ooo0〇〇O:Lkotlin/Lazy;

    .line 8
    sget-object p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsBgPaint$2;->o0:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsBgPaint$2;

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇〇08O:Lkotlin/Lazy;

    .line 9
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressBgPaint$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressBgPaint$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O0O:Lkotlin/Lazy;

    .line 10
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressFgPaint$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressFgPaint$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8oOOo:Lkotlin/Lazy;

    .line 11
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textStr$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textStr$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇O〇〇O8:Lkotlin/Lazy;

    .line 12
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginStart$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginStart$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇o0O:Lkotlin/Lazy;

    .line 13
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginEnd$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginEnd$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O88O:Lkotlin/Lazy;

    .line 14
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginTop$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginTop$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOO〇〇:Lkotlin/Lazy;

    .line 15
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMargin$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMargin$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8o:Lkotlin/Lazy;

    .line 16
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMarginTouch$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMarginTouch$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oo8ooo8O:Lkotlin/Lazy;

    .line 17
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMinHeight$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMinHeight$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o〇oO:Lkotlin/Lazy;

    .line 18
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsRadius$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsRadius$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇08〇o0O:Lkotlin/Lazy;

    .line 19
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressRadius$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressRadius$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇〇o〇:Lkotlin/Lazy;

    .line 20
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressMargin$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressMargin$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->Oo80:Lkotlin/Lazy;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO:Landroid/graphics/Path;

    .line 23
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$valueAnimator$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$valueAnimator$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOo〇8o008:Lkotlin/Lazy;

    .line 24
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO〇00〇8oO:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;

    .line 25
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/〇080;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/〇080;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8〇OO0〇0o:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    const/4 p1, 0x1

    .line 26
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇8〇oO〇〇8o:Z

    .line 27
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textPaint$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textPaint$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->ooo0〇〇O:Lkotlin/Lazy;

    .line 28
    sget-object p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsBgPaint$2;->o0:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsBgPaint$2;

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇〇08O:Lkotlin/Lazy;

    .line 29
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressBgPaint$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressBgPaint$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O0O:Lkotlin/Lazy;

    .line 30
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressFgPaint$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressFgPaint$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8oOOo:Lkotlin/Lazy;

    .line 31
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textStr$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textStr$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇O〇〇O8:Lkotlin/Lazy;

    .line 32
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginStart$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginStart$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇o0O:Lkotlin/Lazy;

    .line 33
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginEnd$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginEnd$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O88O:Lkotlin/Lazy;

    .line 34
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginTop$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$textMarginTop$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOO〇〇:Lkotlin/Lazy;

    .line 35
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMargin$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMargin$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8o:Lkotlin/Lazy;

    .line 36
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMarginTouch$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMarginTouch$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oo8ooo8O:Lkotlin/Lazy;

    .line 37
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMinHeight$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsMinHeight$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o〇oO:Lkotlin/Lazy;

    .line 38
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsRadius$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsRadius$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇08〇o0O:Lkotlin/Lazy;

    .line 39
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressRadius$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressRadius$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇〇o〇:Lkotlin/Lazy;

    .line 40
    new-instance p1, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressMargin$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$tipsProgressMargin$2;-><init>(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->Oo80:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOo0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO0o〇〇()V
    .locals 9

    .line 1
    const-string v0, "updateContentBm start"

    .line 2
    .line 3
    const-string v1, "HlRippleAnimView"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 9
    .line 10
    .line 11
    move-result-wide v2

    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 28
    .line 29
    invoke-static {v0, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    new-instance v4, Landroid/graphics/Canvas;

    .line 34
    .line 35
    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, v4}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 39
    .line 40
    .line 41
    const/4 v4, 0x2

    .line 42
    new-array v4, v4, [I

    .line 43
    .line 44
    const/4 v5, 0x0

    .line 45
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    aput v6, v4, v5

    .line 50
    .line 51
    const/4 v5, 0x1

    .line 52
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 53
    .line 54
    .line 55
    move-result v6

    .line 56
    aput v6, v4, v5

    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getViewImageTempPath()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v5

    .line 62
    new-instance v6, Ljava/io/FileOutputStream;

    .line 63
    .line 64
    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    :try_start_0
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 68
    .line 69
    const/16 v8, 0x55

    .line 70
    .line 71
    invoke-virtual {v0, v7, v8, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 75
    .line 76
    .line 77
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    .line 79
    const/4 v0, 0x0

    .line 80
    invoke-static {v6, v0}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 81
    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getHighLightImageTempPath()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-static {v5, v0, v4}, Lcom/intsig/nativelib/HighLightLineHelper;->getLrEdgeFile(Ljava/lang/String;Ljava/lang/String;[I)I

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    if-ltz v4, :cond_1

    .line 92
    .line 93
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    if-eqz v0, :cond_1

    .line 98
    .line 99
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 100
    .line 101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 102
    .line 103
    .line 104
    move-result-wide v4

    .line 105
    sub-long/2addr v4, v2

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    .line 107
    .line 108
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .line 110
    .line 111
    const-string v2, "updateContentBm finish time = "

    .line 112
    .line 113
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    :cond_1
    return-void

    .line 127
    :catchall_0
    move-exception v0

    .line 128
    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 129
    :catchall_1
    move-exception v1

    .line 130
    invoke-static {v6, v0}, Lkotlin/io/CloseableKt;->〇080(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 131
    .line 132
    .line 133
    throw v1
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final OO0o〇〇〇〇0(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO:Landroid/graphics/Path;

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO:Landroid/graphics/Path;

    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇08O〇00〇o:F

    .line 12
    .line 13
    iget v2, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o〇00O:F

    .line 14
    .line 15
    iget v3, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O8o08O8O:I

    .line 16
    .line 17
    int-to-float v3, v3

    .line 18
    iget v4, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇080OO8〇0:F

    .line 19
    .line 20
    mul-float v3, v3, v4

    .line 21
    .line 22
    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 23
    .line 24
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO:Landroid/graphics/Path;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 33
    .line 34
    if-eqz v0, :cond_0

    .line 35
    .line 36
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-nez v1, :cond_0

    .line 41
    .line 42
    const/4 v1, 0x0

    .line 43
    const/4 v2, 0x0

    .line 44
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 45
    .line 46
    .line 47
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final Oooo8o0〇()V
    .locals 12

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇08O〇00〇o:F

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o〇00O:F

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 9
    .line 10
    .line 11
    new-instance v1, Landroid/graphics/RectF;

    .line 12
    .line 13
    iget v2, v0, Landroid/graphics/RectF;->right:F

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    .line 16
    .line 17
    .line 18
    move-result v4

    .line 19
    int-to-float v4, v4

    .line 20
    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    .line 21
    .line 22
    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 23
    .line 24
    .line 25
    new-instance v2, Landroid/graphics/RectF;

    .line 26
    .line 27
    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    .line 28
    .line 29
    iget v5, v0, Landroid/graphics/RectF;->right:F

    .line 30
    .line 31
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    .line 32
    .line 33
    .line 34
    move-result v6

    .line 35
    int-to-float v6, v6

    .line 36
    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 37
    .line 38
    .line 39
    new-instance v3, Landroid/graphics/RectF;

    .line 40
    .line 41
    iget v4, v2, Landroid/graphics/RectF;->right:F

    .line 42
    .line 43
    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    .line 44
    .line 45
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    int-to-float v6, v6

    .line 50
    iget v7, v2, Landroid/graphics/RectF;->bottom:F

    .line 51
    .line 52
    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    float-to-double v4, v4

    .line 60
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 61
    .line 62
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    .line 63
    .line 64
    .line 65
    move-result-wide v4

    .line 66
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    float-to-double v8, v0

    .line 71
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    .line 72
    .line 73
    .line 74
    move-result-wide v8

    .line 75
    add-double/2addr v4, v8

    .line 76
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    .line 77
    .line 78
    .line 79
    move-result-wide v4

    .line 80
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    float-to-double v8, v0

    .line 85
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    .line 86
    .line 87
    .line 88
    move-result-wide v8

    .line 89
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    float-to-double v0, v0

    .line 94
    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    .line 95
    .line 96
    .line 97
    move-result-wide v0

    .line 98
    add-double/2addr v8, v0

    .line 99
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    .line 100
    .line 101
    .line 102
    move-result-wide v0

    .line 103
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    .line 104
    .line 105
    .line 106
    move-result v8

    .line 107
    float-to-double v8, v8

    .line 108
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    .line 109
    .line 110
    .line 111
    move-result-wide v8

    .line 112
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    float-to-double v10, v2

    .line 117
    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->pow(DD)D

    .line 118
    .line 119
    .line 120
    move-result-wide v10

    .line 121
    add-double/2addr v8, v10

    .line 122
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    .line 123
    .line 124
    .line 125
    move-result-wide v8

    .line 126
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    .line 127
    .line 128
    .line 129
    move-result v2

    .line 130
    float-to-double v10, v2

    .line 131
    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->pow(DD)D

    .line 132
    .line 133
    .line 134
    move-result-wide v10

    .line 135
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    .line 136
    .line 137
    .line 138
    move-result v2

    .line 139
    float-to-double v2, v2

    .line 140
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    .line 141
    .line 142
    .line 143
    move-result-wide v2

    .line 144
    add-double/2addr v10, v2

    .line 145
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    .line 146
    .line 147
    .line 148
    move-result-wide v2

    .line 149
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(DD)D

    .line 150
    .line 151
    .line 152
    move-result-wide v0

    .line 153
    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(DD)D

    .line 154
    .line 155
    .line 156
    move-result-wide v2

    .line 157
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    .line 158
    .line 159
    .line 160
    move-result-wide v0

    .line 161
    double-to-int v0, v0

    .line 162
    iput v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O8o08O8O:I

    .line 163
    .line 164
    return-void
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final getCurShadowColor()I
    .locals 2

    .line 1
    const/16 v0, 0x85

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    iget v1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇080OO8〇0:F

    .line 5
    .line 6
    mul-float v0, v0, v1

    .line 7
    .line 8
    float-to-int v0, v0

    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getHighLightImageTempPath()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v0, "HlRippleTempHighLight.png"

    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final getTextMarginEnd()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O88O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTextMarginStart()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇o0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTextMarginTop()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOO〇〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTextPaint()Landroid/text/TextPaint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->ooo0〇〇O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/text/TextPaint;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTextStr()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇O〇〇O8:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsBgPaint()Landroid/graphics/Paint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇〇08O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/graphics/Paint;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsMargin()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsMarginTouch()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oo8ooo8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsMinHeight()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o〇oO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsProgressBgPaint()Landroid/graphics/Paint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/graphics/Paint;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsProgressFgPaint()Landroid/graphics/Paint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8oOOo:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/graphics/Paint;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsProgressMargin()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->Oo80:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsProgressRadius()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇〇o〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getTipsRadius()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇08〇o0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getValueAnimator()Landroid/animation/ValueAnimator;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOo〇8o008:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/animation/ValueAnimator;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getViewImageTempPath()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const-string v0, "HlRippleTempView.png"

    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic oO80(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOo0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇0O:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇80〇808〇O(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;Landroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇80〇808〇O(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getValueAnimator()Landroid/animation/ValueAnimator;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    .line 22
    .line 23
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    check-cast p1, Ljava/lang/Float;

    .line 27
    .line 28
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    const/high16 v0, 0x42c80000    # 100.0f

    .line 33
    .line 34
    div-float/2addr p1, v0

    .line 35
    iput p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇080OO8〇0:F

    .line 36
    .line 37
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇8o8o〇(Landroid/graphics/Canvas;)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v9, p1

    .line 4
    .line 5
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsMargin()I

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    const/4 v3, 0x2

    .line 14
    mul-int/lit8 v2, v2, 0x2

    .line 15
    .line 16
    sub-int/2addr v1, v2

    .line 17
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextMarginStart()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    sub-int/2addr v1, v2

    .line 22
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextMarginEnd()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    sub-int/2addr v1, v2

    .line 27
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextPaint()Landroid/text/TextPaint;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextStr()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 40
    .line 41
    const/16 v5, 0x17

    .line 42
    .line 43
    const/4 v6, 0x0

    .line 44
    if-lt v4, v5, :cond_0

    .line 45
    .line 46
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextStr()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextStr()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v5

    .line 54
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    .line 55
    .line 56
    .line 57
    move-result v5

    .line 58
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextPaint()Landroid/text/TextPaint;

    .line 59
    .line 60
    .line 61
    move-result-object v7

    .line 62
    const/4 v8, 0x0

    .line 63
    invoke-static {v4, v8, v5, v7, v1}, Landroidx/appcompat/widget/OOO〇O0;->〇080(Ljava/lang/CharSequence;IILandroid/text/TextPaint;I)Landroid/text/StaticLayout$Builder;

    .line 64
    .line 65
    .line 66
    move-result-object v4

    .line 67
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 68
    .line 69
    invoke-static {v4, v5}, Landroidx/appcompat/widget/oo〇;->〇080(Landroid/text/StaticLayout$Builder;Landroid/text/Layout$Alignment;)Landroid/text/StaticLayout$Builder;

    .line 70
    .line 71
    .line 72
    move-result-object v4

    .line 73
    const/high16 v5, 0x3f800000    # 1.0f

    .line 74
    .line 75
    invoke-static {v4, v6, v5}, Landroidx/appcompat/widget/O8〇o;->〇080(Landroid/text/StaticLayout$Builder;FF)Landroid/text/StaticLayout$Builder;

    .line 76
    .line 77
    .line 78
    move-result-object v4

    .line 79
    const/4 v5, 0x1

    .line 80
    invoke-static {v4, v5}, Landroidx/appcompat/widget/〇00〇8;->〇080(Landroid/text/StaticLayout$Builder;Z)Landroid/text/StaticLayout$Builder;

    .line 81
    .line 82
    .line 83
    move-result-object v4

    .line 84
    invoke-static {v4}, Landroidx/appcompat/widget/o〇0OOo〇0;->〇080(Landroid/text/StaticLayout$Builder;)Landroid/text/StaticLayout;

    .line 85
    .line 86
    .line 87
    move-result-object v4

    .line 88
    move-object v10, v4

    .line 89
    goto :goto_0

    .line 90
    :cond_0
    new-instance v4, Landroid/text/StaticLayout;

    .line 91
    .line 92
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextStr()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v11

    .line 96
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextPaint()Landroid/text/TextPaint;

    .line 97
    .line 98
    .line 99
    move-result-object v12

    .line 100
    sget-object v14, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 101
    .line 102
    const/high16 v15, 0x3f800000    # 1.0f

    .line 103
    .line 104
    const/16 v16, 0x0

    .line 105
    .line 106
    const/16 v17, 0x1

    .line 107
    .line 108
    move-object v10, v4

    .line 109
    move v13, v1

    .line 110
    invoke-direct/range {v10 .. v17}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 111
    .line 112
    .line 113
    :goto_0
    const-string v4, "if (Build.VERSION.SDK_IN\u2026e\n            )\n        }"

    .line 114
    .line 115
    invoke-static {v10, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    int-to-float v1, v1

    .line 119
    cmpl-float v1, v2, v1

    .line 120
    .line 121
    if-lez v1, :cond_1

    .line 122
    .line 123
    invoke-virtual {v10}, Landroid/text/Layout;->getWidth()I

    .line 124
    .line 125
    .line 126
    move-result v1

    .line 127
    int-to-float v2, v1

    .line 128
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextMarginStart()I

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    int-to-float v1, v1

    .line 133
    add-float/2addr v2, v1

    .line 134
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextMarginEnd()I

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    int-to-float v1, v1

    .line 139
    add-float/2addr v2, v1

    .line 140
    invoke-virtual {v10}, Landroid/text/Layout;->getHeight()I

    .line 141
    .line 142
    .line 143
    move-result v1

    .line 144
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextMarginTop()I

    .line 145
    .line 146
    .line 147
    move-result v4

    .line 148
    mul-int/lit8 v4, v4, 0x2

    .line 149
    .line 150
    add-int/2addr v1, v4

    .line 151
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsMinHeight()I

    .line 152
    .line 153
    .line 154
    move-result v4

    .line 155
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    .line 156
    .line 157
    .line 158
    move-result v11

    .line 159
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    .line 160
    .line 161
    .line 162
    move-result v1

    .line 163
    int-to-float v1, v1

    .line 164
    sub-float/2addr v1, v2

    .line 165
    int-to-float v12, v3

    .line 166
    div-float v13, v1, v12

    .line 167
    .line 168
    iget v1, v0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o〇00O:F

    .line 169
    .line 170
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsMarginTouch()I

    .line 171
    .line 172
    .line 173
    move-result v3

    .line 174
    int-to-float v3, v3

    .line 175
    sub-float/2addr v1, v3

    .line 176
    int-to-float v14, v11

    .line 177
    sub-float/2addr v1, v14

    .line 178
    invoke-static {v1, v6}, Ljava/lang/Math;->max(FF)F

    .line 179
    .line 180
    .line 181
    move-result v15

    .line 182
    add-float v4, v13, v2

    .line 183
    .line 184
    add-float v5, v15, v14

    .line 185
    .line 186
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsRadius()F

    .line 187
    .line 188
    .line 189
    move-result v6

    .line 190
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsRadius()F

    .line 191
    .line 192
    .line 193
    move-result v7

    .line 194
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsBgPaint()Landroid/graphics/Paint;

    .line 195
    .line 196
    .line 197
    move-result-object v8

    .line 198
    move-object/from16 v1, p1

    .line 199
    .line 200
    move v2, v13

    .line 201
    move v3, v15

    .line 202
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    .line 203
    .line 204
    .line 205
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 206
    .line 207
    .line 208
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTextMarginStart()I

    .line 209
    .line 210
    .line 211
    move-result v1

    .line 212
    int-to-float v1, v1

    .line 213
    add-float/2addr v1, v13

    .line 214
    invoke-virtual {v10}, Landroid/text/Layout;->getHeight()I

    .line 215
    .line 216
    .line 217
    move-result v2

    .line 218
    sub-int/2addr v11, v2

    .line 219
    int-to-float v2, v11

    .line 220
    div-float/2addr v2, v12

    .line 221
    add-float/2addr v2, v15

    .line 222
    invoke-virtual {v9, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 223
    .line 224
    .line 225
    invoke-virtual {v10, v9}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 226
    .line 227
    .line 228
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 229
    .line 230
    .line 231
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressMargin()I

    .line 232
    .line 233
    .line 234
    move-result v1

    .line 235
    int-to-float v1, v1

    .line 236
    add-float v2, v13, v1

    .line 237
    .line 238
    div-float/2addr v14, v12

    .line 239
    add-float/2addr v15, v14

    .line 240
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressRadius()F

    .line 241
    .line 242
    .line 243
    move-result v1

    .line 244
    sub-float v3, v15, v1

    .line 245
    .line 246
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressRadius()F

    .line 247
    .line 248
    .line 249
    move-result v1

    .line 250
    add-float/2addr v1, v2

    .line 251
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressRadius()F

    .line 252
    .line 253
    .line 254
    move-result v4

    .line 255
    add-float/2addr v4, v3

    .line 256
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressRadius()F

    .line 257
    .line 258
    .line 259
    move-result v5

    .line 260
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressBgPaint()Landroid/graphics/Paint;

    .line 261
    .line 262
    .line 263
    move-result-object v6

    .line 264
    invoke-virtual {v9, v1, v4, v5, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 265
    .line 266
    .line 267
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressRadius()F

    .line 268
    .line 269
    .line 270
    move-result v1

    .line 271
    mul-float v1, v1, v12

    .line 272
    .line 273
    add-float v4, v2, v1

    .line 274
    .line 275
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressRadius()F

    .line 276
    .line 277
    .line 278
    move-result v1

    .line 279
    mul-float v1, v1, v12

    .line 280
    .line 281
    add-float v5, v3, v1

    .line 282
    .line 283
    const/high16 v6, -0x3d4c0000    # -90.0f

    .line 284
    .line 285
    iget v1, v0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇080OO8〇0:F

    .line 286
    .line 287
    const/16 v7, 0x168

    .line 288
    .line 289
    int-to-float v7, v7

    .line 290
    mul-float v7, v7, v1

    .line 291
    .line 292
    const/4 v8, 0x0

    .line 293
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getTipsProgressFgPaint()Landroid/graphics/Paint;

    .line 294
    .line 295
    .line 296
    move-result-object v10

    .line 297
    move-object/from16 v1, p1

    .line 298
    .line 299
    move-object v9, v10

    .line 300
    invoke-virtual/range {v1 .. v9}, Landroid/graphics/Canvas;->drawArc(FFFFFFZLandroid/graphics/Paint;)V

    .line 301
    .line 302
    .line 303
    return-void
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO〇00〇8oO:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$animatorListener$1;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;)Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o8〇OO0〇0o:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o0:Z

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    if-nez p1, :cond_1

    .line 10
    .line 11
    return-void

    .line 12
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getCurShadowColor()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO0o〇〇〇〇0(Landroid/graphics/Canvas;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇8o8o〇(Landroid/graphics/Canvas;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    const/4 v1, 0x0

    .line 14
    const/4 v2, 0x1

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    if-nez v3, :cond_3

    .line 23
    .line 24
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-ne v0, v2, :cond_2

    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    :cond_2
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇8〇oO〇〇8o:Z

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    iput v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇08O〇00〇o:F

    .line 38
    .line 39
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    iput v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o〇00O:F

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O〇o88o08〇:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleTouchListener;

    .line 46
    .line 47
    if-eqz v0, :cond_10

    .line 48
    .line 49
    invoke-interface {v0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleTouchListener;->〇080()V

    .line 50
    .line 51
    .line 52
    goto :goto_a

    .line 53
    :cond_3
    :goto_1
    if-nez v0, :cond_4

    .line 54
    .line 55
    goto :goto_2

    .line 56
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    const/4 v4, 0x5

    .line 61
    if-ne v3, v4, :cond_6

    .line 62
    .line 63
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-ne v0, v2, :cond_5

    .line 68
    .line 69
    const/4 v1, 0x1

    .line 70
    :cond_5
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇8〇oO〇〇8o:Z

    .line 71
    .line 72
    goto :goto_a

    .line 73
    :cond_6
    :goto_2
    if-nez v0, :cond_7

    .line 74
    .line 75
    goto :goto_3

    .line 76
    :cond_7
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 77
    .line 78
    .line 79
    move-result v3

    .line 80
    const/4 v4, 0x2

    .line 81
    if-ne v3, v4, :cond_8

    .line 82
    .line 83
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o0:Z

    .line 84
    .line 85
    if-eqz v0, :cond_10

    .line 86
    .line 87
    return v2

    .line 88
    :cond_8
    :goto_3
    if-nez v0, :cond_9

    .line 89
    .line 90
    goto :goto_5

    .line 91
    :cond_9
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 92
    .line 93
    .line 94
    move-result v3

    .line 95
    const/4 v4, 0x3

    .line 96
    if-ne v3, v4, :cond_a

    .line 97
    .line 98
    :goto_4
    const/4 v3, 0x1

    .line 99
    goto :goto_7

    .line 100
    :cond_a
    :goto_5
    if-nez v0, :cond_b

    .line 101
    .line 102
    goto :goto_6

    .line 103
    :cond_b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 104
    .line 105
    .line 106
    move-result v3

    .line 107
    const/4 v4, 0x6

    .line 108
    if-ne v3, v4, :cond_c

    .line 109
    .line 110
    goto :goto_4

    .line 111
    :cond_c
    :goto_6
    const/4 v3, 0x0

    .line 112
    :goto_7
    if-eqz v3, :cond_d

    .line 113
    .line 114
    :goto_8
    const/4 v1, 0x1

    .line 115
    goto :goto_9

    .line 116
    :cond_d
    if-nez v0, :cond_e

    .line 117
    .line 118
    goto :goto_9

    .line 119
    :cond_e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    if-ne v0, v2, :cond_f

    .line 124
    .line 125
    goto :goto_8

    .line 126
    :cond_f
    :goto_9
    if-eqz v1, :cond_10

    .line 127
    .line 128
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o0:Z

    .line 129
    .line 130
    if-eqz v0, :cond_10

    .line 131
    .line 132
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getValueAnimator()Landroid/animation/ValueAnimator;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    if-eqz v0, :cond_10

    .line 137
    .line 138
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 139
    .line 140
    .line 141
    :cond_10
    :goto_a
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 142
    .line 143
    .line 144
    move-result p1

    .line 145
    return p1
    .line 146
    .line 147
.end method

.method public final setHlRippleTouchListener(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleTouchListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleTouchListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "hlRippleTouchListener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->O〇o88o08〇:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleTouchListener;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇O8o08O(Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "listener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇8〇oO〇〇8o:Z

    .line 7
    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇0O:Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView$HlRippleListener;

    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->o0:Z

    .line 15
    .line 16
    const/4 p1, 0x0

    .line 17
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->oOo0:Z

    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    iput p1, p0, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->〇080OO8〇0:F

    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->Oooo8o0〇()V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->OO0o〇〇()V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/pdf/HlRippleAnimView;->getValueAnimator()Landroid/animation/ValueAnimator;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    if-eqz p1, :cond_1

    .line 33
    .line 34
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    .line 35
    .line 36
    .line 37
    :cond_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

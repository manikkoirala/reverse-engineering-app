.class public final Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "CloudDiskFolderViewHolder.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private OO:Lcom/intsig/webstorage/WebStorageApi;

.field private final o0:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFolderBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Landroidx/fragment/app/FragmentActivity;

.field private 〇OOo8〇0:Lcom/intsig/webstorage/RemoteFile;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "convertView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->o0:Landroid/view/View;

    .line 10
    .line 11
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ProviderCloudDiskFolderBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ProviderCloudDiskFolderBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const-string v0, "bind(convertView)"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->o〇00O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFolderBinding;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final O8ooOoo〇(Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;Landroid/view/View;)V
    .locals 5

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->〇08O〇00〇o:Landroidx/fragment/app/FragmentActivity;

    .line 7
    .line 8
    instance-of v0, p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskActivity;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    check-cast p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskActivity;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object p1, v1

    .line 17
    :goto_0
    if-eqz p1, :cond_3

    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$Companion;

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->〇OOo8〇0:Lcom/intsig/webstorage/RemoteFile;

    .line 22
    .line 23
    const-string v3, "mFolderData"

    .line 24
    .line 25
    if-nez v2, :cond_1

    .line 26
    .line 27
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    move-object v2, v1

    .line 31
    :cond_1
    iget-object v2, v2, Lcom/intsig/webstorage/RemoteFile;->〇o00〇〇Oo:Ljava/lang/String;

    .line 32
    .line 33
    const-string v4, "mFolderData.name"

    .line 34
    .line 35
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    sget-object v4, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    .line 39
    .line 40
    invoke-virtual {v4}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->o〇0()I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    invoke-virtual {v0, v2, v4}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$Companion;->〇o〇(Ljava/lang/String;I)Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->〇OOo8〇0:Lcom/intsig/webstorage/RemoteFile;

    .line 49
    .line 50
    if-nez p0, :cond_2

    .line 51
    .line 52
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_2
    move-object v1, p0

    .line 57
    :goto_1
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->oOoO8OO〇(Lcom/intsig/webstorage/RemoteFile;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskActivity;->o0Oo(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)V

    .line 61
    .line 62
    .line 63
    :cond_3
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O〇8O8〇008()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->o〇00O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFolderBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ProviderCloudDiskFolderBinding;->OO:Landroid/widget/TextView;

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->〇OOo8〇0:Lcom/intsig/webstorage/RemoteFile;

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    const-string v1, "mFolderData"

    .line 10
    .line 11
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    :cond_0
    iget-object v1, v1, Lcom/intsig/webstorage/RemoteFile;->〇o00〇〇Oo:Ljava/lang/String;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->o〇00O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFolderBinding;

    .line 21
    .line 22
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ProviderCloudDiskFolderBinding;->〇OOo8〇0:Landroid/widget/RelativeLayout;

    .line 23
    .line 24
    new-instance v1, L〇〇O00〇8/〇o00〇〇Oo;

    .line 25
    .line 26
    invoke-direct {v1, p0}, L〇〇O00〇8/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇00(Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final 〇oOO8O8(Lcom/intsig/webstorage/RemoteFile;Lcom/intsig/webstorage/WebStorageApi;Landroidx/fragment/app/FragmentActivity;)V
    .locals 1
    .param p1    # Lcom/intsig/webstorage/RemoteFile;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "activity"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->〇OOo8〇0:Lcom/intsig/webstorage/RemoteFile;

    .line 12
    .line 13
    iput-object p3, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->〇08O〇00〇o:Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->OO:Lcom/intsig/webstorage/WebStorageApi;

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFolderViewHolder;->O〇8O8〇008()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.class public final Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "CloudDiskFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO〇00〇8oO:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8〇OO0〇0o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇oO〇〇8o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskAdapter;

.field private OO:Lcom/intsig/webstorage/RemoteFile;

.field private final o0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:Landroidx/recyclerview/widget/RecyclerView;

.field private oOo〇8o008:Z

.field private o〇00O:Lcom/intsig/webstorage/WebStorageApi;

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;

.field private 〇0O:Lcom/intsig/app/ProgressDialogClient;

.field private 〇OOo8〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/cloud_disk/ICloudDiskType;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$Companion;

    .line 8
    .line 9
    const-string v0, "current_dir"

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 12
    .line 13
    const-string v0, "cloud_disk_type"

    .line 14
    .line 15
    sput-object v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CloudDiskFragment"

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o0:Ljava/lang/String;

    .line 7
    .line 8
    new-instance v0, Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇OOo8〇0:Ljava/util/List;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)Lcom/intsig/app/ProgressDialogClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇0O:Lcom/intsig/app/ProgressDialogClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o880()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oO〇oo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇8〇80o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oooO888()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)Lcom/intsig/webstorage/WebStorageApi;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o〇00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o〇O8OO(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0〇0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o〇00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/webstorage/WebStorageApi;->〇80〇808〇O()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    if-eqz v0, :cond_3

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇0O:Lcom/intsig/app/ProgressDialogClient;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/app/ProgressDialogClient;->O8()V

    .line 23
    .line 24
    .line 25
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;

    .line 26
    .line 27
    if-nez v0, :cond_2

    .line 28
    .line 29
    const-string v0, "mViewModel"

    .line 30
    .line 31
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/4 v0, 0x0

    .line 35
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->OO:Lcom/intsig/webstorage/RemoteFile;

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o〇00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 38
    .line 39
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;->o〇0OOo〇0(Lcom/intsig/webstorage/RemoteFile;Lcom/intsig/webstorage/WebStorageApi;)V

    .line 40
    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_3
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->oOo〇8o008:Z

    .line 44
    .line 45
    if-eqz v0, :cond_4

    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 53
    .line 54
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 55
    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_4
    iput-boolean v1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->oOo〇8o008:Z

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 61
    .line 62
    iget v1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇080OO8〇0:I

    .line 63
    .line 64
    invoke-static {v0, v1}, Lcom/intsig/camscanner/service/UploadUtils;->〇〇888(Landroid/content/Context;I)V

    .line 65
    .line 66
    .line 67
    :goto_1
    return-void
    .line 68
.end method

.method private static final 〇8〇80o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8oOo0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o〇O8OO(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o〇88〇8()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "mViewModel"

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;->〇〇〇0〇〇0()Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    new-instance v3, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$initLiveData$1;

    .line 17
    .line 18
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$initLiveData$1;-><init>(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)V

    .line 19
    .line 20
    .line 21
    new-instance v4, Lo8〇/OO0o〇〇〇〇0;

    .line 22
    .line 23
    invoke-direct {v4, v3}, Lo8〇/OO0o〇〇〇〇0;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p0, v4}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;

    .line 30
    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    move-object v1, v0

    .line 38
    :goto_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;->oo〇()Landroidx/lifecycle/MutableLiveData;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    new-instance v1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$initLiveData$2;

    .line 43
    .line 44
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$initLiveData$2;-><init>(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)V

    .line 45
    .line 46
    .line 47
    new-instance v2, Lo8〇/〇8o8o〇;

    .line 48
    .line 49
    invoke-direct {v2, v1}, Lo8〇/〇8o8o〇;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->O8o08O8O:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public getIntentData(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->getIntentData(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    :goto_0
    iput p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇080OO8〇0:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    const v0, 0x7f0a1026

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 p1, 0x0

    .line 18
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->oOo0:Landroidx/recyclerview/widget/RecyclerView;

    .line 19
    .line 20
    sget-object p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;->〇080OO8〇0:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel$Companion;

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    const-string v1, "mActivity"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, v0, p0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel$Companion;->〇080(Landroid/content/Context;Landroidx/fragment/app/Fragment;)Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;

    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇o00〇〇Oo()Lcom/intsig/webstorage/WebStorageAPIFactory;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    iget v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇080OO8〇0:I

    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 42
    .line 43
    invoke-virtual {p1, v0, v2}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇080(ILandroid/content/Context;)Lcom/intsig/webstorage/WebStorageApi;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o〇00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 48
    .line 49
    new-instance p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskAdapter;

    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o〇00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇OOo8〇0:Ljava/util/List;

    .line 54
    .line 55
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 56
    .line 57
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    new-instance v1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$initialize$1;

    .line 61
    .line 62
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment$initialize$1;-><init>(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;)V

    .line 63
    .line 64
    .line 65
    invoke-direct {p1, v0, v2, v3, v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskAdapter;-><init>(Lcom/intsig/webstorage/WebStorageApi;Ljava/util/List;Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function2;)V

    .line 66
    .line 67
    .line 68
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->O8o08O8O:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskAdapter;

    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->oOo0:Landroidx/recyclerview/widget/RecyclerView;

    .line 71
    .line 72
    if-nez v0, :cond_1

    .line 73
    .line 74
    goto :goto_1

    .line 75
    :cond_1
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 76
    .line 77
    .line 78
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇o〇88〇8()V

    .line 79
    .line 80
    .line 81
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 82
    .line 83
    const v0, 0x7f131ec6

    .line 84
    .line 85
    .line 86
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-static {p1, v0}, Lcom/intsig/app/ProgressDialogClient;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)Lcom/intsig/app/ProgressDialogClient;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇0O:Lcom/intsig/app/ProgressDialogClient;

    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public interceptBackPressed()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "interceptBackPressed"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    instance-of v1, v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskActivity;

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    check-cast v0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskActivity;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    move-object v0, v2

    .line 19
    :goto_0
    if-eqz v0, :cond_1

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    const/4 v3, 0x2

    .line 23
    invoke-static {v0, v1, v2, v3, v2}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskActivity;->o808o8o08(Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskActivity;ZLjava/lang/String;ILjava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    :cond_1
    invoke-super {p0}, Lcom/intsig/fragmentBackHandler/BackHandledFragment;->interceptBackPressed()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    return v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final oOoO8OO〇(Lcom/intsig/webstorage/RemoteFile;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->OO:Lcom/intsig/webstorage/RemoteFile;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇0O:Lcom/intsig/app/ProgressDialogClient;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/app/ProgressDialogClient;->〇080()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇0〇0()V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const-string v0, "mViewModel"

    .line 12
    .line 13
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇080OO8〇0:I

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->o〇00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 20
    .line 21
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskViewModel;->o〇8oOO88(ILcom/intsig/webstorage/WebStorageApi;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d02b2

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇088O()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/cloud_disk/ICloudDiskType;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskFragment;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "CloudDiskFileViewHolder.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/webstorage/WebStorageApi;

.field private 〇080OO8〇0:Lcom/intsig/app/BaseProgressDialog;

.field private 〇08O〇00〇o:Lcom/intsig/webstorage/RemoteFile;

.field private final 〇0O:Lcom/intsig/webstorage/RemoteFile;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/intsig/webstorage/RemoteFile;",
            "Lcom/intsig/webstorage/RemoteFile;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/jvm/functions/Function2;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/intsig/webstorage/RemoteFile;",
            "-",
            "Lcom/intsig/webstorage/RemoteFile;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "convertView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "downloadFile"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->o0:Landroid/view/View;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇OOo8〇0:Lkotlin/jvm/functions/Function2;

    .line 17
    .line 18
    const-string p2, "CloudDiskFileViewHolder"

    .line 19
    .line 20
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->OO:Ljava/lang/String;

    .line 21
    .line 22
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const-string p2, "bind(convertView)"

    .line 27
    .line 28
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->O8o08O8O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;

    .line 32
    .line 33
    new-instance p1, Lcom/intsig/webstorage/RemoteFile;

    .line 34
    .line 35
    invoke-direct {p1}, Lcom/intsig/webstorage/RemoteFile;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇8O0〇8()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    iput-object p2, p1, Lcom/intsig/webstorage/RemoteFile;->Oo08:Ljava/lang/String;

    .line 43
    .line 44
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇0O:Lcom/intsig/webstorage/RemoteFile;

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final O8ooOoo〇(Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇OOo8〇0:Lkotlin/jvm/functions/Function2;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇08O〇00〇o:Lcom/intsig/webstorage/RemoteFile;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string v0, "mCloudDiskFileData"

    .line 13
    .line 14
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    :cond_0
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇0O:Lcom/intsig/webstorage/RemoteFile;

    .line 19
    .line 20
    invoke-interface {p1, v0, p0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇8O8〇008()V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n",
            "SimpleDateFormat"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->O8o08O8O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;->o〇00O:Landroid/widget/TextView;

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇08O〇00〇o:Lcom/intsig/webstorage/RemoteFile;

    .line 6
    .line 7
    const-string v2, "mCloudDiskFileData"

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    move-object v1, v3

    .line 16
    :cond_0
    iget-object v1, v1, Lcom/intsig/webstorage/RemoteFile;->〇o00〇〇Oo:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->O8o08O8O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;

    .line 22
    .line 23
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 24
    .line 25
    const-string v1, "PDF"

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->O8o08O8O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;

    .line 31
    .line 32
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 33
    .line 34
    sget-object v1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    .line 35
    .line 36
    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->o〇0()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-nez v1, :cond_2

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇08O〇00〇o:Lcom/intsig/webstorage/RemoteFile;

    .line 43
    .line 44
    if-nez v1, :cond_1

    .line 45
    .line 46
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    move-object v1, v3

    .line 50
    :cond_1
    iget-object v4, v1, Lcom/intsig/webstorage/RemoteFile;->〇8o8o〇:Ljava/lang/String;

    .line 51
    .line 52
    if-eqz v4, :cond_5

    .line 53
    .line 54
    const-string v1, "T"

    .line 55
    .line 56
    filled-new-array {v1}, [Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v5

    .line 60
    const/4 v6, 0x0

    .line 61
    const/4 v7, 0x0

    .line 62
    const/4 v8, 0x6

    .line 63
    const/4 v9, 0x0

    .line 64
    invoke-static/range {v4 .. v9}, Lkotlin/text/StringsKt;->Oo(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    if-eqz v1, :cond_5

    .line 69
    .line 70
    const/4 v2, 0x0

    .line 71
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    move-object v3, v1

    .line 76
    check-cast v3, Ljava/lang/String;

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_2
    new-instance v1, Ljava/text/SimpleDateFormat;

    .line 80
    .line 81
    const-string v4, "yyyy-MM-dd"

    .line 82
    .line 83
    invoke-direct {v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇08O〇00〇o:Lcom/intsig/webstorage/RemoteFile;

    .line 87
    .line 88
    if-nez v4, :cond_3

    .line 89
    .line 90
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    move-object v4, v3

    .line 94
    :cond_3
    iget-object v2, v4, Lcom/intsig/webstorage/RemoteFile;->〇8o8o〇:Ljava/lang/String;

    .line 95
    .line 96
    if-eqz v2, :cond_4

    .line 97
    .line 98
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 99
    .line 100
    .line 101
    move-result-wide v2

    .line 102
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 103
    .line 104
    .line 105
    move-result-object v3

    .line 106
    :cond_4
    invoke-virtual {v1, v3}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v3

    .line 110
    :cond_5
    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    .line 112
    .line 113
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->o0:Landroid/view/View;

    .line 114
    .line 115
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    const/4 v1, 0x1

    .line 120
    invoke-static {v0, v1}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    const-string v1, "getProgressDialog(conver\u2026sDialog.STYLE_HORIZONTAL)"

    .line 125
    .line 126
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇080OO8〇0:Lcom/intsig/app/BaseProgressDialog;

    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->O8o08O8O:Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;

    .line 132
    .line 133
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ProviderCloudDiskFileBinding;->OO:Landroid/widget/RelativeLayout;

    .line 134
    .line 135
    new-instance v1, L〇〇O00〇8/〇080;

    .line 136
    .line 137
    invoke-direct {v1, p0}, L〇〇O00〇8/〇080;-><init>(Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    .line 142
    .line 143
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇00(Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final 〇oOO8O8(Lcom/intsig/webstorage/RemoteFile;Lcom/intsig/webstorage/WebStorageApi;)V
    .locals 1
    .param p1    # Lcom/intsig/webstorage/RemoteFile;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->〇08O〇00〇o:Lcom/intsig/webstorage/RemoteFile;

    .line 7
    .line 8
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->o〇00O:Lcom/intsig/webstorage/WebStorageApi;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/cloud_disk/viewholder/CloudDiskFileViewHolder;->O〇8O8〇008()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public final Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;
.super Lcom/intsig/camscanner/util/AbstractPreferenceHelper;
.source "ImportWechatPreferenceHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇080:Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final O8()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final Oo08()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final oO80(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇080:Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_boolean_import_clicked"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putBoolean(Ljava/lang/String;Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final o〇0()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final 〇080()Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇080:Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_boolean_import_clicked"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final 〇80〇808〇O(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇080:Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_int_local_using_wechat_deblur"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putInt(Ljava/lang/String;I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final 〇o00〇〇Oo()I
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇080:Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_int_local_using_wechat_deblur"

    .line 4
    .line 5
    const/4 v2, -0x1

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getInt(Ljava/lang/String;I)I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇o〇()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇o00〇〇Oo()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, -0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->clarity_wechat:I

    .line 13
    .line 14
    :cond_0
    const/4 v1, 0x1

    .line 15
    if-ne v0, v1, :cond_1

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    :goto_0
    return v1
    .line 20
    .line 21
.end method

.method public static final 〇〇888()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/gallery/ImportWechatPreferenceHelper;->〇o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public getPrefixString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "key_import_wechat_"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

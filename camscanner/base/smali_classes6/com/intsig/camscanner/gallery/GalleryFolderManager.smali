.class Lcom/intsig/camscanner/gallery/GalleryFolderManager;
.super Ljava/lang/Object;
.source "GalleryFolderManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;,
        Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;,
        Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;
    }
.end annotation


# instance fields
.field private O8:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future<",
            "Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080:Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;

.field private 〇o00〇〇Oo:Ljava/util/concurrent/ExecutorService;

.field private 〇o〇:Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇o00〇〇Oo:Ljava/util/concurrent/ExecutorService;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/gallery/GalleryFolderManager;)Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇080:Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/gallery/GalleryFolderManager;)Ljava/util/concurrent/Future;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->O8:Ljava/util/concurrent/Future;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method O8(Landroid/content/Context;Lcom/intsig/camscanner/gallery/GalleryFolderItemClickListener;)V
    .locals 3

    .line 1
    const-string v0, "GalleryFolderManager"

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇o〇(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    const-string v1, "CSImport_album"

    .line 11
    .line 12
    invoke-static {v1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇080:Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;

    .line 16
    .line 17
    if-nez v1, :cond_0

    .line 18
    .line 19
    new-instance v1, Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;

    .line 20
    .line 21
    const v2, 0x7f140004

    .line 22
    .line 23
    .line 24
    invoke-direct {v1, p1, v2}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;-><init>(Landroid/content/Context;I)V

    .line 25
    .line 26
    .line 27
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇080:Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;

    .line 28
    .line 29
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇080:Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;

    .line 30
    .line 31
    invoke-virtual {v1, p2}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;->〇〇808〇(Lcom/intsig/camscanner/gallery/GalleryFolderItemClickListener;)V

    .line 32
    .line 33
    .line 34
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->O8:Ljava/util/concurrent/Future;

    .line 35
    .line 36
    if-eqz p2, :cond_3

    .line 37
    .line 38
    invoke-interface {p2}, Ljava/util/concurrent/Future;->isDone()Z

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    if-nez p2, :cond_1

    .line 43
    .line 44
    goto :goto_2

    .line 45
    :cond_1
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->O8:Ljava/util/concurrent/Future;

    .line 46
    .line 47
    invoke-interface {p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    check-cast p1, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .line 53
    goto :goto_1

    .line 54
    :catch_0
    move-exception p1

    .line 55
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    .line 57
    .line 58
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :catch_1
    move-exception p1

    .line 67
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    .line 69
    .line 70
    :goto_0
    const/4 p1, 0x0

    .line 71
    :goto_1
    if-nez p1, :cond_2

    .line 72
    .line 73
    const-string p1, "loadGalleryResult == null"

    .line 74
    .line 75
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    goto :goto_3

    .line 79
    :cond_2
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇080:Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;

    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;->〇080()Ljava/util/Map;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-virtual {p2, v0, p1}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$GalleryFolderDialog;->〇O〇(Ljava/util/List;Ljava/util/Map;)V

    .line 90
    .line 91
    .line 92
    goto :goto_3

    .line 93
    :cond_3
    :goto_2
    new-instance p2, Lcom/intsig/utils/CommonLoadingTask;

    .line 94
    .line 95
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryFolderManager$1;

    .line 96
    .line 97
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$1;-><init>(Lcom/intsig/camscanner/gallery/GalleryFolderManager;)V

    .line 98
    .line 99
    .line 100
    const v1, 0x7f130e22

    .line 101
    .line 102
    .line 103
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v1

    .line 107
    invoke-direct {p2, p1, v0, v1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {p2}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 111
    .line 112
    .line 113
    :goto_3
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method 〇o〇(Landroid/content/Context;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇o〇:Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-direct {v0, p1, v1}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;-><init>(Landroid/content/Context;LOO〇/〇0〇O0088o;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇o〇:Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;

    .line 12
    .line 13
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->O8:Ljava/util/concurrent/Future;

    .line 14
    .line 15
    if-nez p1, :cond_1

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇o00〇〇Oo:Ljava/util/concurrent/ExecutorService;

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->〇o〇:Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;

    .line 20
    .line 21
    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager;->O8:Ljava/util/concurrent/Future;

    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.class Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;
.super Landroid/widget/BaseAdapter;
.source "CustomGalleryActivity.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/gallery/CustomGalleryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageCursorAdapter"
.end annotation


# instance fields
.field private O8o08O8O:[Ljava/lang/String;

.field private OO:I

.field private OO〇00〇8oO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;"
        }
    .end annotation
.end field

.field private final o0:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageItemSelectCallBack;

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/GallerySelectedItem;

.field private oOo0:Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

.field private oOo〇8o008:Lcom/bumptech/glide/request/RequestOptions;

.field private o〇00O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:I

.field private 〇0O:Z

.field final synthetic 〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

.field private 〇OOo8〇0:I


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageItemSelectCallBack;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 p1, -0x1

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇080OO8〇0:I

    .line 8
    .line 9
    new-instance p1, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o0:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageItemSelectCallBack;

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->Oooo8o0〇()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O8()Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oOo0:Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;->o〇0()Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oOo0:Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oOo0:Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method private Oo08(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)Lcom/bumptech/glide/RequestBuilder;
    .locals 1
    .param p1    # Lcom/intsig/camscanner/gallery/GallerySelectedItem;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ")",
            "Lcom/bumptech/glide/RequestBuilder<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getUri()Landroid/net/Uri;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 8
    .line 9
    invoke-static {v0}, Lcom/bumptech/glide/Glide;->oo88o8O(Landroidx/fragment/app/FragmentActivity;)Lcom/bumptech/glide/RequestManager;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getUri()Landroid/net/Uri;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/RequestManager;->〇O8o08O(Landroid/net/Uri;)Lcom/bumptech/glide/RequestBuilder;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oO80()Lcom/bumptech/glide/request/RequestOptions;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {p1, v0}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    return-object p1

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 31
    .line 32
    invoke-static {v0}, Lcom/bumptech/glide/Glide;->oo88o8O(Landroidx/fragment/app/FragmentActivity;)Lcom/bumptech/glide/RequestManager;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getPath()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oO80()Lcom/bumptech/glide/request/RequestOptions;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {p1, v0}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    return-object p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private Oooo8o0〇()V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const v1, 0x7f070062

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    iput v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇OOo8〇0:I

    .line 22
    .line 23
    const v1, 0x7f070061

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    iput v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO:I

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oO80()Lcom/bumptech/glide/request/RequestOptions;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oOo〇8o008:Lcom/bumptech/glide/request/RequestOptions;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 8
    .line 9
    .line 10
    sget-object v1, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 17
    .line 18
    new-instance v7, Lcom/intsig/camscanner/util/GlideRoundTransform;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 21
    .line 22
    const/4 v2, 0x3

    .line 23
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    const/4 v3, 0x1

    .line 28
    const/4 v4, 0x1

    .line 29
    const/4 v5, 0x1

    .line 30
    const/4 v6, 0x1

    .line 31
    move-object v1, v7

    .line 32
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/util/GlideRoundTransform;-><init>(IZZZZ)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v7}, Lcom/bumptech/glide/request/BaseRequestOptions;->O0O8OO088(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oOo〇8o008:Lcom/bumptech/glide/request/RequestOptions;

    .line 42
    .line 43
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->oOo〇8o008:Lcom/bumptech/glide/request/RequestOptions;

    .line 44
    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇080(Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;)V
    .locals 3

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;->〇o00〇〇Oo:Landroid/view/View;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇08O〇00〇o:I

    .line 8
    .line 9
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 10
    .line 11
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 14
    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 17
    .line 18
    invoke-static {v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇80〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const/high16 v2, 0x40400000    # 3.0f

    .line 23
    .line 24
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->OO0o〇〇(Landroid/view/View;F)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;->〇080:Landroid/view/View;

    .line 32
    .line 33
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO:I

    .line 34
    .line 35
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇80〇808〇O()Lcom/intsig/camscanner/gallery/GallerySelectedItem;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-direct {v0, v1, v1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->setLoadingItem(Z)V

    .line 15
    .line 16
    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method private 〇oo〇()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇080OO8〇0:I

    .line 2
    .line 3
    if-lez v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iget v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇080OO8〇0:I

    .line 12
    .line 13
    if-lt v0, v1, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    iput-boolean v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇0O:Z

    .line 19
    .line 20
    :cond_1
    return-void
    .line 21
.end method

.method private 〇〇808〇(I)[Ljava/lang/String;
    .locals 5

    .line 1
    if-lez p1, :cond_0

    .line 2
    .line 3
    new-array v0, p1, [Ljava/lang/String;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    :goto_0
    if-ge v1, p1, :cond_1

    .line 7
    .line 8
    new-instance v2, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    add-int/lit8 v3, v1, 0x1

    .line 14
    .line 15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v4, ""

    .line 19
    .line 20
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    move v1, v3

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v1, "initePageNumIndex count="

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    const-string v0, "CustomGalleryActivity"

    .line 49
    .line 50
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    const/4 v0, 0x0

    .line 54
    :cond_1
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇〇8O0〇8(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v2, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-ltz p1, :cond_0

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v2, "removeLoadingItem cost == "

    .line 24
    .line 25
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 29
    .line 30
    .line 31
    move-result-wide v2

    .line 32
    sub-long/2addr v2, v0

    .line 33
    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    const-string v0, "CustomGalleryActivity"

    .line 41
    .line 42
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method OO0o〇〇()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇〇〇0()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OoO8(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    new-instance p1, Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 6
    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 12
    .line 13
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 14
    .line 15
    invoke-static {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->o88o88(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->notifyDataSetChanged()V

    .line 19
    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 22
    .line 23
    invoke-static {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8〇〇8o(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method O〇8O8〇008(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->isLoadingItem()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇oo〇()V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_2
    :goto_1
    const-string p1, "CustomGalleryActivity"

    .line 34
    .line 35
    const-string v0, "item == null or isLoadingItem"

    .line 36
    .line 37
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public getCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇888(I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getItemId(I)J
    .locals 2

    .line 1
    int-to-long v0, p1

    .line 2
    return-wide v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇888(I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_1

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->isLoadingItem()Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p1, 0x0

    .line 15
    return p1

    .line 16
    :cond_1
    :goto_0
    const-string p1, "CustomGalleryActivity"

    .line 17
    .line 18
    const-string v0, "item == null or isLoadingItem"

    .line 19
    .line 20
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 p1, 0x1

    .line 24
    return p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public getPositionForSection(I)I
    .locals 0

    .line 1
    return p1
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getSectionForPosition(I)I
    .locals 0

    .line 1
    return p1
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->getCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O8o08O8O:[Ljava/lang/String;

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇808〇(I)[Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O8o08O8O:[Ljava/lang/String;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    array-length v1, v1

    .line 17
    if-eq v1, v0, :cond_1

    .line 18
    .line 19
    new-instance v1, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v2, "getSections change mPageNumIndex count="

    .line 25
    .line 26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v2, " mPageNumIndex.length="

    .line 33
    .line 34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O8o08O8O:[Ljava/lang/String;

    .line 38
    .line 39
    array-length v2, v2

    .line 40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    const-string v2, "CustomGalleryActivity"

    .line 48
    .line 49
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇808〇(I)[Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O8o08O8O:[Ljava/lang/String;

    .line 57
    .line 58
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O8o08O8O:[Ljava/lang/String;

    .line 59
    .line 60
    return-object v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->getItemViewType(I)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    if-nez v0, :cond_2

    .line 8
    .line 9
    if-eqz p2, :cond_1

    .line 10
    .line 11
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    instance-of v0, v0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object p3

    .line 24
    check-cast p3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 28
    .line 29
    invoke-static {p2}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->oO〇O0O(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 30
    .line 31
    .line 32
    move-result-object p2

    .line 33
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    const v0, 0x7f0d0691

    .line 38
    .line 39
    .line 40
    invoke-virtual {p2, v0, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object p2

    .line 44
    new-instance p3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;

    .line 45
    .line 46
    invoke-direct {p3, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;-><init>(LOO〇/〇〇808〇;)V

    .line 47
    .line 48
    .line 49
    iput-object p2, p3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇080:Landroid/view/View;

    .line 50
    .line 51
    const v0, 0x7f0a0931

    .line 52
    .line 53
    .line 54
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    check-cast v0, Landroid/widget/ImageView;

    .line 59
    .line 60
    iput-object v0, p3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o00〇〇Oo:Landroid/widget/ImageView;

    .line 61
    .line 62
    const v0, 0x7f0a0fe3

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    iput-object v0, p3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o〇:Landroid/view/View;

    .line 70
    .line 71
    const v0, 0x7f0a12f1

    .line 72
    .line 73
    .line 74
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    check-cast v0, Landroid/widget/TextView;

    .line 79
    .line 80
    iput-object v0, p3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->O8:Landroid/widget/TextView;

    .line 81
    .line 82
    const v0, 0x7f0a1996

    .line 83
    .line 84
    .line 85
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    iput-object v0, p3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->Oo08:Landroid/view/View;

    .line 90
    .line 91
    const v0, 0x7f0a1a38

    .line 92
    .line 93
    .line 94
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    iput-object v0, p3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->o〇0:Landroid/view/View;

    .line 99
    .line 100
    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 101
    .line 102
    .line 103
    :goto_1
    invoke-virtual {p0, p3, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇o00〇〇Oo(Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;I)V

    .line 104
    .line 105
    .line 106
    goto :goto_4

    .line 107
    :cond_2
    const/4 p1, 0x1

    .line 108
    if-ne v0, p1, :cond_5

    .line 109
    .line 110
    if-eqz p2, :cond_4

    .line 111
    .line 112
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    instance-of p1, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;

    .line 117
    .line 118
    if-nez p1, :cond_3

    .line 119
    .line 120
    goto :goto_2

    .line 121
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    check-cast p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;

    .line 126
    .line 127
    goto :goto_3

    .line 128
    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 129
    .line 130
    invoke-static {p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->〇8o0o0(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 131
    .line 132
    .line 133
    move-result-object p1

    .line 134
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    const p2, 0x7f0d0692

    .line 139
    .line 140
    .line 141
    invoke-virtual {p1, p2, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    new-instance p2, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;

    .line 146
    .line 147
    invoke-direct {p2, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;-><init>(LOO〇/Oooo8o0〇;)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;->〇080(Landroid/view/View;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 154
    .line 155
    .line 156
    move-object v3, p2

    .line 157
    move-object p2, p1

    .line 158
    move-object p1, v3

    .line 159
    :goto_3
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇080(Lcom/intsig/camscanner/gallery/CustomGalleryActivity$LoadingViewHolder;)V

    .line 160
    .line 161
    .line 162
    :cond_5
    :goto_4
    return-object p2
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public getViewTypeCount()I
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .line 1
    :try_start_0
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const-string v1, "CustomGalleryActivity"

    .line 7
    .line 8
    const-string v2, "notifyDataSetChanged"

    .line 9
    .line 10
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 11
    .line 12
    .line 13
    :goto_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method o800o8O(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇080OO8〇0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oo88o8O(I)I
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO:I

    .line 2
    .line 3
    mul-int/lit8 v1, v0, 0x2

    .line 4
    .line 5
    sub-int/2addr p1, v1

    .line 6
    const/4 v1, 0x3

    .line 7
    if-lez p1, :cond_2

    .line 8
    .line 9
    iget v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇OOo8〇0:I

    .line 10
    .line 11
    if-lez v2, :cond_2

    .line 12
    .line 13
    mul-int/lit8 v3, v0, 0x2

    .line 14
    .line 15
    add-int/2addr v3, v2

    .line 16
    div-int v3, p1, v3

    .line 17
    .line 18
    if-le v3, v1, :cond_1

    .line 19
    .line 20
    iput v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇08O〇00〇o:I

    .line 21
    .line 22
    mul-int/lit8 v0, v0, 0x2

    .line 23
    .line 24
    add-int/2addr v0, v2

    .line 25
    mul-int v0, v0, v3

    .line 26
    .line 27
    sub-int/2addr p1, v0

    .line 28
    if-lez p1, :cond_0

    .line 29
    .line 30
    div-int/2addr p1, v3

    .line 31
    if-lez p1, :cond_0

    .line 32
    .line 33
    add-int/2addr v2, p1

    .line 34
    iput v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇08O〇00〇o:I

    .line 35
    .line 36
    :cond_0
    move v1, v3

    .line 37
    goto :goto_0

    .line 38
    :cond_1
    div-int/2addr p1, v1

    .line 39
    mul-int/lit8 v0, v0, 0x2

    .line 40
    .line 41
    sub-int/2addr p1, v0

    .line 42
    iput p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇08O〇00〇o:I

    .line 43
    .line 44
    :cond_2
    :goto_0
    return v1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o〇0()I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->getCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o8〇OO0〇0o:Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 10
    .line 11
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    add-int/lit8 v0, v0, -0x1

    .line 18
    .line 19
    :cond_0
    return v0
    .line 20
    .line 21
.end method

.method public o〇O8〇〇o(Z)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "updateRadarLoadingItem: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "CustomGalleryActivity"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇80〇808〇O()Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz p1, :cond_0

    .line 28
    .line 29
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇0()I

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-lez p1, :cond_1

    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 39
    .line 40
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V

    .line 45
    .line 46
    .line 47
    :cond_1
    :goto_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method 〇00(I)V
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇888(I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_1

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->isLoadingItem()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getPath()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getId()J

    .line 19
    .line 20
    .line 21
    move-result-wide v1

    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 23
    .line 24
    iget-object p1, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->ooo0〇〇O:Landroid/net/Uri;

    .line 25
    .line 26
    invoke-static {p1, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    new-instance v1, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 31
    .line 32
    invoke-direct {v1, v0, p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O〇8O8〇008(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_1
    :goto_0
    const-string p1, "CustomGalleryActivity"

    .line 40
    .line 41
    const-string v0, "item == null or isLoadingItem"

    .line 42
    .line 43
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇0〇O0088o()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇o〇()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 21
    .line 22
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O〇8O8〇008(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method 〇8o8o〇()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-lez v1, :cond_0

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 19
    .line 20
    .line 21
    :cond_0
    return-object v0
.end method

.method 〇O00(I)Z
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇〇888(I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getPath()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 p1, 0x0

    .line 25
    :goto_0
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method 〇O888o0o(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇oo〇()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->notifyDataSetChanged()V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/gallery/CustomGalleryActivity;

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    :goto_0
    invoke-static {p1, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity;->O〇0o8o8〇(Lcom/intsig/camscanner/gallery/CustomGalleryActivity;I)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method 〇O8o08O()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 7
    .line 8
    if-eqz v1, :cond_1

    .line 9
    .line 10
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-lez v1, :cond_1

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    const/4 v2, 0x0

    .line 23
    :goto_0
    if-ge v2, v1, :cond_1

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    if-eqz v3, :cond_0

    .line 32
    .line 33
    iget-object v3, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 34
    .line 35
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    check-cast v3, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 40
    .line 41
    invoke-virtual {v3}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getPath()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇0〇O0088o(Ljava/lang/String;)Landroid/net/Uri;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇O〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇0()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;I)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CheckResult"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, "CustomGalleryActivity"

    .line 8
    .line 9
    if-lt p2, v0, :cond_0

    .line 10
    .line 11
    new-instance p1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v0, "bindView error position "

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string p2, ", list size "

    .line 25
    .line 26
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 32
    .line 33
    .line 34
    move-result p2

    .line 35
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return-void

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 47
    .line 48
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    check-cast v0, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 53
    .line 54
    iget-object v2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o〇:Landroid/view/View;

    .line 55
    .line 56
    new-instance v3, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ItemTouchCallback;

    .line 57
    .line 58
    iget-object v4, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o0:Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageItemSelectCallBack;

    .line 59
    .line 60
    invoke-direct {v3, p2, v4}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ItemTouchCallback;-><init>(ILcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageItemSelectCallBack;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 64
    .line 65
    .line 66
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 67
    .line 68
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    move-result p2

    .line 72
    if-eqz p2, :cond_3

    .line 73
    .line 74
    iget-object p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 75
    .line 76
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    .line 77
    .line 78
    .line 79
    move-result p2

    .line 80
    if-gez p2, :cond_1

    .line 81
    .line 82
    new-instance p1, Ljava/lang/StringBuilder;

    .line 83
    .line 84
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .line 86
    .line 87
    const-string p2, "load error path: "

    .line 88
    .line 89
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    return-void

    .line 103
    :cond_1
    const/16 v2, 0x63

    .line 104
    .line 105
    if-lt p2, v2, :cond_2

    .line 106
    .line 107
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->O8:Landroid/widget/TextView;

    .line 108
    .line 109
    const v2, 0x7f131eda

    .line 110
    .line 111
    .line 112
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(I)V

    .line 113
    .line 114
    .line 115
    goto :goto_0

    .line 116
    :cond_2
    iget-object v2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->O8:Landroid/widget/TextView;

    .line 117
    .line 118
    add-int/lit8 p2, p2, 0x1

    .line 119
    .line 120
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object p2

    .line 124
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    .line 126
    .line 127
    :goto_0
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->O8:Landroid/widget/TextView;

    .line 128
    .line 129
    const v2, 0x7f0810c1

    .line 130
    .line 131
    .line 132
    invoke-virtual {p2, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 133
    .line 134
    .line 135
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->Oo08:Landroid/view/View;

    .line 136
    .line 137
    const/4 v2, 0x0

    .line 138
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 139
    .line 140
    .line 141
    goto :goto_2

    .line 142
    :cond_3
    iget p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇080OO8〇0:I

    .line 143
    .line 144
    if-lez p2, :cond_5

    .line 145
    .line 146
    iget-boolean p2, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇0O:Z

    .line 147
    .line 148
    if-eqz p2, :cond_4

    .line 149
    .line 150
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o00〇〇Oo:Landroid/widget/ImageView;

    .line 151
    .line 152
    const v2, 0x3e99999a    # 0.3f

    .line 153
    .line 154
    .line 155
    invoke-virtual {p2, v2}, Landroid/view/View;->setAlpha(F)V

    .line 156
    .line 157
    .line 158
    goto :goto_1

    .line 159
    :cond_4
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o00〇〇Oo:Landroid/widget/ImageView;

    .line 160
    .line 161
    const/high16 v2, 0x3f800000    # 1.0f

    .line 162
    .line 163
    invoke-virtual {p2, v2}, Landroid/view/View;->setAlpha(F)V

    .line 164
    .line 165
    .line 166
    :cond_5
    :goto_1
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->O8:Landroid/widget/TextView;

    .line 167
    .line 168
    const v2, 0x7f080bf2

    .line 169
    .line 170
    .line 171
    invoke-virtual {p2, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 172
    .line 173
    .line 174
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->O8:Landroid/widget/TextView;

    .line 175
    .line 176
    const-string v2, ""

    .line 177
    .line 178
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    .line 180
    .line 181
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->Oo08:Landroid/view/View;

    .line 182
    .line 183
    const/16 v2, 0x8

    .line 184
    .line 185
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 186
    .line 187
    .line 188
    :goto_2
    sget-object p2, Lcom/intsig/camscanner/gallery/GalleryCacheManager;->〇080:Lcom/intsig/camscanner/gallery/GalleryCacheManager;

    .line 189
    .line 190
    invoke-virtual {p2}, Lcom/intsig/camscanner/gallery/GalleryCacheManager;->〇〇888()Landroid/util/LruCache;

    .line 191
    .line 192
    .line 193
    move-result-object p2

    .line 194
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getId()J

    .line 195
    .line 196
    .line 197
    move-result-wide v2

    .line 198
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 199
    .line 200
    .line 201
    move-result-object v2

    .line 202
    invoke-virtual {p2, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    .line 204
    .line 205
    move-result-object p2

    .line 206
    check-cast p2, Landroid/graphics/Bitmap;

    .line 207
    .line 208
    if-eqz p2, :cond_6

    .line 209
    .line 210
    invoke-static {}, Lcom/intsig/camscanner/experiment/GalleryCacheExp;->〇080()Z

    .line 211
    .line 212
    .line 213
    move-result v2

    .line 214
    if-eqz v2, :cond_6

    .line 215
    .line 216
    const-string v2, "bindView use cache"

    .line 217
    .line 218
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    iget-object v1, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o00〇〇Oo:Landroid/widget/ImageView;

    .line 222
    .line 223
    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 224
    .line 225
    .line 226
    :cond_6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->Oo08(Lcom/intsig/camscanner/gallery/GallerySelectedItem;)Lcom/bumptech/glide/RequestBuilder;

    .line 227
    .line 228
    .line 229
    move-result-object v0

    .line 230
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 231
    .line 232
    const/16 v2, 0x18

    .line 233
    .line 234
    if-lt v1, v2, :cond_7

    .line 235
    .line 236
    if-nez p2, :cond_7

    .line 237
    .line 238
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->O8()Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 239
    .line 240
    .line 241
    move-result-object p2

    .line 242
    invoke-virtual {v0, p2}, Lcom/bumptech/glide/RequestBuilder;->o8O0(Lcom/bumptech/glide/TransitionOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 243
    .line 244
    .line 245
    move-result-object p2

    .line 246
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o00〇〇Oo:Landroid/widget/ImageView;

    .line 247
    .line 248
    invoke-virtual {p2, v0}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 249
    .line 250
    .line 251
    goto :goto_3

    .line 252
    :cond_7
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o00〇〇Oo:Landroid/widget/ImageView;

    .line 253
    .line 254
    invoke-virtual {v0, p2}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 255
    .line 256
    .line 257
    :goto_3
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o〇:Landroid/view/View;

    .line 258
    .line 259
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 260
    .line 261
    .line 262
    move-result-object p2

    .line 263
    check-cast p2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 264
    .line 265
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇08O〇00〇o:I

    .line 266
    .line 267
    iput v0, p2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 268
    .line 269
    iput v0, p2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 270
    .line 271
    iget-object v0, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇o〇:Landroid/view/View;

    .line 272
    .line 273
    invoke-virtual {v0, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 274
    .line 275
    .line 276
    iget-object p2, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇080:Landroid/view/View;

    .line 277
    .line 278
    iget v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO:I

    .line 279
    .line 280
    invoke-virtual {p2, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 281
    .line 282
    .line 283
    iget-object p1, p1, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ViewHolder;->〇080:Landroid/view/View;

    .line 284
    .line 285
    const p2, 0x7f080fac

    .line 286
    .line 287
    .line 288
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 289
    .line 290
    .line 291
    return-void
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method 〇o〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->〇oo〇()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888(I)Lcom/intsig/camscanner/gallery/GallerySelectedItem;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lt p1, v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "getItem error position "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string p1, ", list size is "

    .line 23
    .line 24
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 28
    .line 29
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    const-string v0, "CustomGalleryActivity"

    .line 41
    .line 42
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    const/4 p1, 0x0

    .line 46
    return-object p1

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/CustomGalleryActivity$ImageCursorAdapter;->OO〇00〇8oO:Ljava/util/List;

    .line 48
    .line 49
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    check-cast p1, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 54
    .line 55
    return-object p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

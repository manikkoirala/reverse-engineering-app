.class public final Lcom/intsig/camscanner/gallery/GalleryGuideManager;
.super Ljava/lang/Object;
.source "GalleryGuideManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/gallery/GalleryGuideManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/gallery/GalleryGuideManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private 〇080:Lcom/app/hubert/guide/core/Controller;

.field private 〇o00〇〇Oo:Z

.field private 〇o〇:Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryGuideManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/gallery/GalleryGuideManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->O8:Lcom/intsig/camscanner/gallery/GalleryGuideManager$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/gallery/GalleryGuideManager;)Lcom/app/hubert/guide/core/Controller;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇080:Lcom/app/hubert/guide/core/Controller;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/gallery/GalleryGuideManager;Lcom/app/hubert/guide/core/Controller;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇080:Lcom/app/hubert/guide/core/Controller;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/gallery/GalleryGuideManager;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final O8(Landroid/app/Activity;Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "anchorView"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇o00〇〇Oo:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const-string p1, "GalleryGuideManager"

    .line 16
    .line 17
    const-string p2, "is showing other dialog"

    .line 18
    .line 19
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_0
    invoke-static {p2}, Landroidx/core/view/ViewCompat;->isLaidOut(Landroid/view/View;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {p2}, Landroid/view/View;->isLayoutRequested()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    new-instance v0, Landroid/graphics/Rect;

    .line 36
    .line 37
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p2, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 41
    .line 42
    .line 43
    iget v1, v0, Landroid/graphics/Rect;->left:I

    .line 44
    .line 45
    const/high16 v2, 0x40800000    # 4.0f

    .line 46
    .line 47
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    add-int/2addr v1, v3

    .line 52
    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 53
    .line 54
    iget v1, v0, Landroid/graphics/Rect;->right:I

    .line 55
    .line 56
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    sub-int/2addr v1, v3

    .line 61
    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 62
    .line 63
    invoke-static {p1}, Lcom/app/hubert/guide/NewbieGuide;->〇080(Landroid/app/Activity;)Lcom/app/hubert/guide/core/Builder;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const-string v3, "radarguidewindow"

    .line 68
    .line 69
    invoke-virtual {v1, v3}, Lcom/app/hubert/guide/core/Builder;->Oo08(Ljava/lang/String;)Lcom/app/hubert/guide/core/Builder;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    invoke-virtual {v1, p1}, Lcom/app/hubert/guide/core/Builder;->〇o〇(Landroid/view/View;)Lcom/app/hubert/guide/core/Builder;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    new-instance v1, Lcom/intsig/camscanner/gallery/GalleryGuideManager$showRadarGuideWindow$1$1;

    .line 86
    .line 87
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/gallery/GalleryGuideManager$showRadarGuideWindow$1$1;-><init>(Lcom/intsig/camscanner/gallery/GalleryGuideManager;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p1, v1}, Lcom/app/hubert/guide/core/Builder;->o〇0(Lcom/app/hubert/guide/listener/OnGuideChangedListener;)Lcom/app/hubert/guide/core/Builder;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    invoke-static {}, Lcom/app/hubert/guide/model/GuidePage;->〇〇808〇()Lcom/app/hubert/guide/model/GuidePage;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    const/4 v3, 0x0

    .line 99
    invoke-virtual {v1, v3}, Lcom/app/hubert/guide/model/GuidePage;->〇O00(Z)Lcom/app/hubert/guide/model/GuidePage;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    new-instance v3, Landroid/graphics/RectF;

    .line 104
    .line 105
    invoke-direct {v3, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 106
    .line 107
    .line 108
    sget-object v0, Lcom/app/hubert/guide/model/HighLight$Shape;->ROUND_RECTANGLE:Lcom/app/hubert/guide/model/HighLight$Shape;

    .line 109
    .line 110
    const/high16 v4, 0x41400000    # 12.0f

    .line 111
    .line 112
    invoke-static {v4}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 117
    .line 118
    .line 119
    move-result v2

    .line 120
    new-instance v5, Lcom/intsig/camscanner/gallery/GalleryGuideManager$showRadarGuideWindow$1$2;

    .line 121
    .line 122
    invoke-direct {v5, p0, p2, v2}, Lcom/intsig/camscanner/gallery/GalleryGuideManager$showRadarGuideWindow$1$2;-><init>(Lcom/intsig/camscanner/gallery/GalleryGuideManager;Landroid/view/View;I)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {v1, v3, v0, v4, v5}, Lcom/app/hubert/guide/model/GuidePage;->〇080(Landroid/graphics/RectF;Lcom/app/hubert/guide/model/HighLight$Shape;ILcom/app/hubert/guide/model/RelativeGuide;)Lcom/app/hubert/guide/model/GuidePage;

    .line 126
    .line 127
    .line 128
    move-result-object p2

    .line 129
    invoke-virtual {p1, p2}, Lcom/app/hubert/guide/core/Builder;->〇080(Lcom/app/hubert/guide/model/GuidePage;)Lcom/app/hubert/guide/core/Builder;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/app/hubert/guide/core/Builder;->〇〇888()Lcom/app/hubert/guide/core/Controller;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    invoke-static {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇o00〇〇Oo(Lcom/intsig/camscanner/gallery/GalleryGuideManager;Lcom/app/hubert/guide/core/Controller;)V

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryGuideManager$showRadarGuideWindow$$inlined$doOnLayout$1;

    .line 142
    .line 143
    invoke-direct {v0, p2, p0, p1}, Lcom/intsig/camscanner/gallery/GalleryGuideManager$showRadarGuideWindow$$inlined$doOnLayout$1;-><init>(Landroid/view/View;Lcom/intsig/camscanner/gallery/GalleryGuideManager;Landroid/app/Activity;)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {p2, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 147
    .line 148
    .line 149
    :goto_0
    return-void
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final Oo08(Landroid/app/Activity;Landroid/widget/GridView;I)V
    .locals 2
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/widget/GridView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "gridView"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇o00〇〇Oo:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const-string p1, "GalleryGuideManager"

    .line 16
    .line 17
    const-string p2, "is showing other dialog"

    .line 18
    .line 19
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇o〇:Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;

    .line 24
    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    new-instance v0, Lcom/intsig/camscanner/gallery/GalleryGuideManager$showSelectAnimationGuide$listener$1;

    .line 28
    .line 29
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/gallery/GalleryGuideManager$showSelectAnimationGuide$listener$1;-><init>(Lcom/intsig/camscanner/gallery/GalleryGuideManager;)V

    .line 30
    .line 31
    .line 32
    new-instance v1, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;

    .line 33
    .line 34
    invoke-direct {v1, p1, p2, v0}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;-><init>(Landroid/app/Activity;Landroid/widget/GridView;Lcom/intsig/camscanner/gallery/DialogShowDismissListener;)V

    .line 35
    .line 36
    .line 37
    iput-object v1, p0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇o〇:Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;

    .line 38
    .line 39
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryGuideManager;->〇o〇:Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;

    .line 40
    .line 41
    if-eqz p1, :cond_2

    .line 42
    .line 43
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/gallery/SelectAnimationGuidePopClient;->o0ooO(I)V

    .line 44
    .line 45
    .line 46
    :cond_2
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

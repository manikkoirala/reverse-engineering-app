.class Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;
.super Ljava/lang/Object;
.source "GalleryFolderManager.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/gallery/GalleryFolderManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoadGalleryFolderCallable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;",
        ">;"
    }
.end annotation


# instance fields
.field private o0:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;->o0:Landroid/content/Context;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;LOO〇/〇0〇O0088o;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v0, p0

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 4
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 5
    new-instance v5, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;-><init>(LOO〇/OoO8;)V

    .line 6
    invoke-virtual {v5, v4}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;->〇o〇(Ljava/util/Map;)V

    .line 7
    invoke-virtual {v5, v3}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;->O8(Ljava/util/ArrayList;)V

    .line 8
    new-instance v6, Lcom/intsig/camscanner/gallery/GalleryManager;

    iget-object v7, v0, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;->o0:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/intsig/camscanner/gallery/GalleryManager;-><init>(Landroid/content/Context;)V

    .line 9
    invoke-virtual {v6}, Lcom/intsig/camscanner/gallery/GalleryManager;->〇080()Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;

    move-result-object v6

    const-string v7, "GalleryFolderManager"

    if-eqz v6, :cond_2

    .line 10
    new-instance v15, Lcom/intsig/camscanner/gallery/ImageFolder;

    invoke-virtual {v6}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->o〇0()I

    move-result v9

    iget-object v8, v0, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;->o0:Landroid/content/Context;

    const v10, 0x7f130416

    .line 11
    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    .line 12
    invoke-virtual {v6}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->Oo08()Ljava/lang/String;

    move-result-object v12

    .line 13
    invoke-virtual {v6}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->O8()J

    move-result-wide v13

    move-object v8, v15

    invoke-direct/range {v8 .. v14}, Lcom/intsig/camscanner/gallery/ImageFolder;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 14
    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 15
    invoke-virtual {v6}, Lcom/intsig/camscanner/gallery/GalleryManager$LoadImageResults;->〇o〇()Ljava/util/Map;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 16
    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 17
    new-instance v8, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-direct {v8, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 18
    new-instance v6, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable$1;

    invoke-direct {v6, v0}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable$1;-><init>(Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;)V

    invoke-static {v8, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 19
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 20
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/intsig/camscanner/gallery/GalleryManager$GroupImageData;

    invoke-virtual {v9}, Lcom/intsig/camscanner/gallery/GalleryManager$GroupImageData;->〇o00〇〇Oo()Ljava/util/ArrayList;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 21
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_0

    goto :goto_1

    .line 22
    :cond_0
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 23
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v10, 0x0

    .line 24
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/intsig/camscanner/gallery/GalleryManager$ImageData;

    .line 25
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 26
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    .line 27
    new-instance v15, Lcom/intsig/camscanner/gallery/ImageFolder;

    invoke-virtual {v10}, Lcom/intsig/camscanner/gallery/GalleryManager$ImageData;->〇080()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10}, Lcom/intsig/camscanner/gallery/GalleryManager$ImageData;->getId()J

    move-result-wide v16

    move-object v10, v15

    move-object v13, v8

    move-object v0, v15

    move-wide/from16 v15, v16

    invoke-direct/range {v10 .. v16}, Lcom/intsig/camscanner/gallery/ImageFolder;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 28
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    invoke-static {v9}, Lcom/intsig/utils/ext/ImageIdExtKt;->〇080(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_1
    :goto_1
    const-string v0, "empty data"

    .line 30
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move-object/from16 v0, p0

    goto :goto_0

    .line 31
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LoadGalleryFolderCallable cost time="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v1

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryFolderCallable;->call()Lcom/intsig/camscanner/gallery/GalleryFolderManager$LoadGalleryResult;

    move-result-object v0

    return-object v0
.end method

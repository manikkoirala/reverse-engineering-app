.class public final Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion;
.super Ljava/lang/Object;
.source "AccountInfoChange.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion;-><init>()V

    return-void
.end method

.method private final 〇080(Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "handleEduAuthMsg"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "AccountInfoChange"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v0, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion$handleEduAuthMsg$1;

    .line 24
    .line 25
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion$handleEduAuthMsg$1;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-static {p1, v0}, Lcom/intsig/tsapp/account/api/AccountApi;->Oo08(Ljava/lang/String;Lcom/intsig/okgo/callback/JsonCallback;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇o〇()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/eventbus/AccountInfoUpdatedEvent;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/eventbus/AccountInfoUpdatedEvent;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/eventbus/AccountInfoUpdatedEvent;->〇o00〇〇Oo(Z)V

    .line 11
    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇o00〇〇Oo()V
    .locals 13

    .line 1
    const-string v0, "refreshAccountInfo start"

    .line 2
    .line 3
    const-string v1, "AccountInfoChange"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 9
    .line 10
    .line 11
    move-result-wide v2

    .line 12
    const-string v0, ""

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇〇8(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    sget-object v4, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 22
    .line 23
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 24
    .line 25
    .line 26
    move-result-object v5

    .line 27
    const/4 v6, 0x1

    .line 28
    invoke-virtual {v0, v5, v6}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->Oooo8o0〇(Landroid/content/Context;Z)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇O〇〇O8(Z)Z

    .line 40
    .line 41
    .line 42
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 47
    .line 48
    .line 49
    move-result-object v5

    .line 50
    invoke-static {v5}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 51
    .line 52
    .line 53
    move-result v5

    .line 54
    invoke-static {v0, v5}, Lcom/intsig/camscanner/app/AppUtil;->ooo〇8oO(Landroid/content/Context;Z)V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion;->〇o〇()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    const-wide/16 v7, 0x0

    .line 65
    .line 66
    invoke-static {v0, v7, v8}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo0〇o〇(Landroid/content/Context;J)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->OO0o〇〇〇〇0(Landroid/content/Context;)Z

    .line 74
    .line 75
    .line 76
    new-instance v0, Lcom/intsig/tsapp/account/login_task/WXLoginControl;

    .line 77
    .line 78
    invoke-virtual {v4}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 79
    .line 80
    .line 81
    move-result-object v4

    .line 82
    const/4 v5, 0x0

    .line 83
    invoke-direct {v0, v4, v5}, Lcom/intsig/tsapp/account/login_task/WXLoginControl;-><init>(Landroid/content/Context;Lcom/intsig/login/WXNetCallBack;)V

    .line 84
    .line 85
    .line 86
    sget-object v4, Lcom/intsig/tsapp/account/login_task/WXLoginControl$WXType;->QUERY_BIND:Lcom/intsig/tsapp/account/login_task/WXLoginControl$WXType;

    .line 87
    .line 88
    invoke-virtual {v0, v4}, Lcom/intsig/tsapp/account/login_task/WXLoginControl;->〇〇8O0〇8(Lcom/intsig/tsapp/account/login_task/WXLoginControl$WXType;)V

    .line 89
    .line 90
    .line 91
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication;->oo〇()Lkotlinx/coroutines/CoroutineScope;

    .line 96
    .line 97
    .line 98
    move-result-object v7

    .line 99
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 100
    .line 101
    .line 102
    move-result-object v8

    .line 103
    const/4 v9, 0x0

    .line 104
    new-instance v10, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion$refreshAccountInfo$1;

    .line 105
    .line 106
    invoke-direct {v10, v5}, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion$refreshAccountInfo$1;-><init>(Lkotlin/coroutines/Continuation;)V

    .line 107
    .line 108
    .line 109
    const/4 v11, 0x2

    .line 110
    const/4 v12, 0x0

    .line 111
    invoke-static/range {v7 .. v12}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 112
    .line 113
    .line 114
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->Oooo8o0〇()Z

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    .line 120
    const-string v0, "edu_af"

    .line 121
    .line 122
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion;->〇080(Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    goto :goto_0

    .line 126
    :cond_0
    const-string v0, "edu_auth"

    .line 127
    .line 128
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange$Companion;->〇080(Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/gpfirstpremium/GpFirstPremiumGiftConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/gpfirstpremium/GpFirstPremiumGiftConfig;

    .line 132
    .line 133
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/gpfirstpremium/GpFirstPremiumGiftConfig;->Oo08(Z)V

    .line 134
    .line 135
    .line 136
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 137
    .line 138
    .line 139
    move-result-wide v4

    .line 140
    sub-long/2addr v4, v2

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    .line 142
    .line 143
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .line 145
    .line 146
    const-string v2, "refreshAccountInfo costTime:"

    .line 147
    .line 148
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    return-void
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

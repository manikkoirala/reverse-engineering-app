.class public final Lcom/intsig/camscanner/message/messages/MessageHandler;
.super Ljava/lang/Object;
.source "MessageHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/message/messages/MessageHandler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static 〇o00〇〇Oo:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/intsig/camscanner/message/messages/WxMsgDataListener;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/message/messages/MessageHandler;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/message/messages/MessageHandler;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/message/messages/MessageHandler;->〇080:Lcom/intsig/camscanner/message/messages/MessageHandler;

    .line 7
    .line 8
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v0, Lcom/intsig/camscanner/message/messages/MessageHandler;->〇o00〇〇Oo:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇080()Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Lcom/intsig/camscanner/message/messages/WxMsgDataListener;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/message/messages/MessageHandler;->〇o00〇〇Oo:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo(Lcom/intsig/camscanner/message/entity/CsSocketMsg;)V
    .locals 5
    .param p1    # Lcom/intsig/camscanner/message/entity/CsSocketMsg;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "csSocketMsg"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/entity/CsSocketMsg;->Oo08()Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;->getMtype()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    new-instance v2, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v3, "message.mtype="

    .line 23
    .line 24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const-string v2, "MessageHandler"

    .line 35
    .line 36
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v3, "message == "

    .line 45
    .line 46
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0}, Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;->getMtype()I

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    if-eqz v1, :cond_c

    .line 64
    .line 65
    const/16 v2, 0x3e9

    .line 66
    .line 67
    if-eq v1, v2, :cond_b

    .line 68
    .line 69
    const/16 v2, 0x3f1

    .line 70
    .line 71
    if-eq v1, v2, :cond_a

    .line 72
    .line 73
    const/16 v2, 0x1b63

    .line 74
    .line 75
    if-eq v1, v2, :cond_9

    .line 76
    .line 77
    const/16 v2, 0x1b68

    .line 78
    .line 79
    if-eq v1, v2, :cond_8

    .line 80
    .line 81
    const/16 v2, 0xbb9

    .line 82
    .line 83
    if-eq v1, v2, :cond_7

    .line 84
    .line 85
    const/16 v2, 0xbba

    .line 86
    .line 87
    if-eq v1, v2, :cond_6

    .line 88
    .line 89
    const/16 v2, 0x1b5b

    .line 90
    .line 91
    if-eq v1, v2, :cond_5

    .line 92
    .line 93
    const/16 v2, 0x1b5c

    .line 94
    .line 95
    if-eq v1, v2, :cond_4

    .line 96
    .line 97
    packed-switch v1, :pswitch_data_0

    .line 98
    .line 99
    .line 100
    packed-switch v1, :pswitch_data_1

    .line 101
    .line 102
    .line 103
    const/4 v2, 0x0

    .line 104
    packed-switch v1, :pswitch_data_2

    .line 105
    .line 106
    .line 107
    packed-switch v1, :pswitch_data_3

    .line 108
    .line 109
    .line 110
    goto/16 :goto_2

    .line 111
    .line 112
    :pswitch_0
    new-instance v2, Lcom/intsig/camscanner/message/messages/account/SmartRoutineUploadFileMsg;

    .line 113
    .line 114
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/account/SmartRoutineUploadFileMsg;-><init>()V

    .line 115
    .line 116
    .line 117
    goto/16 :goto_2

    .line 118
    .line 119
    :pswitch_1
    new-instance v2, Lcom/intsig/camscanner/message/messages/esigndoc/ESignSyncMsg;

    .line 120
    .line 121
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/esigndoc/ESignSyncMsg;-><init>()V

    .line 122
    .line 123
    .line 124
    goto/16 :goto_2

    .line 125
    .line 126
    :pswitch_2
    new-instance v2, Lcom/intsig/camscanner/message/messages/esigndoc/ESignInviteMsg;

    .line 127
    .line 128
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/esigndoc/ESignInviteMsg;-><init>()V

    .line 129
    .line 130
    .line 131
    goto/16 :goto_2

    .line 132
    .line 133
    :pswitch_3
    new-instance v2, Lcom/intsig/camscanner/message/messages/sharedir/ShareDirCloudStorageMsg;

    .line 134
    .line 135
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/sharedir/ShareDirCloudStorageMsg;-><init>()V

    .line 136
    .line 137
    .line 138
    goto/16 :goto_2

    .line 139
    .line 140
    :pswitch_4
    new-instance v2, Lcom/intsig/camscanner/message/messages/sharedir/ShareDirPermissionChangeMsg;

    .line 141
    .line 142
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/sharedir/ShareDirPermissionChangeMsg;-><init>()V

    .line 143
    .line 144
    .line 145
    goto/16 :goto_2

    .line 146
    .line 147
    :pswitch_5
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/entity/CsSocketMsg;->o〇0()Ljava/lang/Long;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/entity/CsSocketMsg;->oO80()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    if-eqz v1, :cond_3

    .line 156
    .line 157
    if-eqz p1, :cond_2

    .line 158
    .line 159
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 160
    .line 161
    .line 162
    move-result v3

    .line 163
    if-nez v3, :cond_1

    .line 164
    .line 165
    goto :goto_0

    .line 166
    :cond_1
    const/4 v3, 0x0

    .line 167
    goto :goto_1

    .line 168
    :cond_2
    :goto_0
    const/4 v3, 0x1

    .line 169
    :goto_1
    if-nez v3, :cond_3

    .line 170
    .line 171
    invoke-static {}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair;->newBuilder()Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;

    .line 172
    .line 173
    .line 174
    move-result-object v2

    .line 175
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 176
    .line 177
    .line 178
    move-result-wide v3

    .line 179
    invoke-virtual {v2, v3, v4}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;->〇080(J)Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;

    .line 180
    .line 181
    .line 182
    move-result-object v1

    .line 183
    invoke-virtual {v1, p1}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->build()Lcom/google/protobuf/GeneratedMessageLite;

    .line 188
    .line 189
    .line 190
    move-result-object p1

    .line 191
    move-object v2, p1

    .line 192
    check-cast v2, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair;

    .line 193
    .line 194
    :cond_3
    new-instance p1, Lcom/intsig/camscanner/message/messages/sync/SyncWxmpImgMsg;

    .line 195
    .line 196
    sget-object v1, Lcom/intsig/camscanner/message/messages/MessageHandler;->〇o00〇〇Oo:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 197
    .line 198
    invoke-direct {p1, v1, v2}, Lcom/intsig/camscanner/message/messages/sync/SyncWxmpImgMsg;-><init>(Ljava/util/concurrent/CopyOnWriteArrayList;Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair;)V

    .line 199
    .line 200
    .line 201
    move-object v2, p1

    .line 202
    goto/16 :goto_2

    .line 203
    .line 204
    :pswitch_6
    new-instance v2, Lcom/intsig/camscanner/message/messages/sync/TeamUpdateMsg;

    .line 205
    .line 206
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/sync/TeamUpdateMsg;-><init>()V

    .line 207
    .line 208
    .line 209
    goto/16 :goto_2

    .line 210
    .line 211
    :pswitch_7
    new-instance v2, Lcom/intsig/camscanner/message/messages/sync/TeamSyncMsg;

    .line 212
    .line 213
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/sync/TeamSyncMsg;-><init>()V

    .line 214
    .line 215
    .line 216
    goto/16 :goto_2

    .line 217
    .line 218
    :pswitch_8
    new-instance v2, Lcom/intsig/camscanner/message/messages/sync/SyncDirMsg;

    .line 219
    .line 220
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/sync/SyncDirMsg;-><init>()V

    .line 221
    .line 222
    .line 223
    goto :goto_2

    .line 224
    :pswitch_9
    new-instance v2, Lcom/intsig/camscanner/message/messages/pay/VipMonthPromotionMsg;

    .line 225
    .line 226
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/pay/VipMonthPromotionMsg;-><init>()V

    .line 227
    .line 228
    .line 229
    goto :goto_2

    .line 230
    :pswitch_a
    new-instance v2, Lcom/intsig/camscanner/message/messages/pay/VipLevelUpgradeMsg;

    .line 231
    .line 232
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/pay/VipLevelUpgradeMsg;-><init>()V

    .line 233
    .line 234
    .line 235
    goto :goto_2

    .line 236
    :pswitch_b
    new-instance v2, Lcom/intsig/camscanner/message/messages/account/ApiCenterChangeMsg;

    .line 237
    .line 238
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/account/ApiCenterChangeMsg;-><init>()V

    .line 239
    .line 240
    .line 241
    goto :goto_2

    .line 242
    :pswitch_c
    new-instance v2, Lcom/intsig/camscanner/message/messages/account/UnBindWeChat;

    .line 243
    .line 244
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/account/UnBindWeChat;-><init>()V

    .line 245
    .line 246
    .line 247
    goto :goto_2

    .line 248
    :pswitch_d
    new-instance v2, Lcom/intsig/camscanner/message/messages/account/BindWeChat;

    .line 249
    .line 250
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/account/BindWeChat;-><init>()V

    .line 251
    .line 252
    .line 253
    goto :goto_2

    .line 254
    :pswitch_e
    new-instance v2, Lcom/intsig/camscanner/message/messages/account/AccountFrozenMsg;

    .line 255
    .line 256
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/account/AccountFrozenMsg;-><init>()V

    .line 257
    .line 258
    .line 259
    goto :goto_2

    .line 260
    :cond_4
    new-instance v2, Lcom/intsig/camscanner/message/messages/pay/UnSubscribeRedeemMsg;

    .line 261
    .line 262
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/pay/UnSubscribeRedeemMsg;-><init>()V

    .line 263
    .line 264
    .line 265
    goto :goto_2

    .line 266
    :cond_5
    new-instance v2, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange;

    .line 267
    .line 268
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/pay/AccountInfoChange;-><init>()V

    .line 269
    .line 270
    .line 271
    goto :goto_2

    .line 272
    :cond_6
    new-instance v2, Lcom/intsig/camscanner/message/messages/sync/SyncPageMsg;

    .line 273
    .line 274
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/sync/SyncPageMsg;-><init>()V

    .line 275
    .line 276
    .line 277
    goto :goto_2

    .line 278
    :cond_7
    new-instance v2, Lcom/intsig/camscanner/message/messages/sync/SyncDocMsg;

    .line 279
    .line 280
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/sync/SyncDocMsg;-><init>()V

    .line 281
    .line 282
    .line 283
    goto :goto_2

    .line 284
    :cond_8
    new-instance v2, Lcom/intsig/camscanner/message/messages/account/AppFlyerAttributeInfoMsg;

    .line 285
    .line 286
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/account/AppFlyerAttributeInfoMsg;-><init>()V

    .line 287
    .line 288
    .line 289
    goto :goto_2

    .line 290
    :cond_9
    new-instance v2, Lcom/intsig/camscanner/message/messages/account/PayAndTrialMsg;

    .line 291
    .line 292
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/account/PayAndTrialMsg;-><init>()V

    .line 293
    .line 294
    .line 295
    goto :goto_2

    .line 296
    :cond_a
    new-instance v2, Lcom/intsig/camscanner/message/messages/pay/IdShareActivityMsg;

    .line 297
    .line 298
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/pay/IdShareActivityMsg;-><init>()V

    .line 299
    .line 300
    .line 301
    goto :goto_2

    .line 302
    :cond_b
    new-instance v2, Lcom/intsig/camscanner/message/messages/account/EduActivitiesMsg;

    .line 303
    .line 304
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/account/EduActivitiesMsg;-><init>()V

    .line 305
    .line 306
    .line 307
    goto :goto_2

    .line 308
    :cond_c
    new-instance v2, Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg;

    .line 309
    .line 310
    invoke-direct {v2}, Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg;-><init>()V

    .line 311
    .line 312
    .line 313
    :goto_2
    if-eqz v2, :cond_d

    .line 314
    .line 315
    invoke-interface {v2, v0}, Lcom/intsig/camscanner/message/messages/IMessage;->〇080(Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;)V

    .line 316
    .line 317
    .line 318
    :cond_d
    return-void

    .line 319
    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_e
        :pswitch_d
        :pswitch_c
    .end packed-switch

    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    :pswitch_data_1
    .packed-switch 0x457
        :pswitch_b
        :pswitch_a
        :pswitch_9
    .end packed-switch

    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    :pswitch_data_2
    .packed-switch 0xbbc
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    :pswitch_data_3
    .packed-switch 0xbcd
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

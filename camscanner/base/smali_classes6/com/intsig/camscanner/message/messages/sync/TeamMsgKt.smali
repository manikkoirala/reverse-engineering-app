.class public final Lcom/intsig/camscanner/message/messages/sync/TeamMsgKt;
.super Ljava/lang/Object;
.source "TeamMsg.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public static final synthetic 〇080(Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/message/messages/sync/TeamMsgKt;->〇o00〇〇Oo(Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇o00〇〇Oo(Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;)V
    .locals 8

    .line 1
    const-string v0, "TeamMsg"

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->Ooo()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-nez v1, :cond_2

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;->getParams()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;->getParams()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;->getParams()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    const-class v1, Lcom/intsig/camscanner/message/entity/Params;

    .line 31
    .line 32
    invoke-static {p0, v1}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object p0

    .line 36
    check-cast p0, Lcom/intsig/camscanner/message/entity/Params;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :catch_0
    move-exception p0

    .line 40
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 41
    .line 42
    .line 43
    const/4 p0, 0x0

    .line 44
    :goto_0
    if-nez p0, :cond_1

    .line 45
    .line 46
    const-string p0, "syncByMsg params == null"

    .line 47
    .line 48
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    return-void

    .line 52
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/entity/Params;->getRevision()I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/entity/Params;->getUpdated_folder()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/entity/Params;->getUpload_time()J

    .line 65
    .line 66
    .line 67
    move-result-wide v4

    .line 68
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/entity/Params;->getUpdate_type()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v6

    .line 72
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/entity/Params;->getUpdated_team()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v7

    .line 76
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->OO8oO0o〇(ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    :cond_2
    :goto_1
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

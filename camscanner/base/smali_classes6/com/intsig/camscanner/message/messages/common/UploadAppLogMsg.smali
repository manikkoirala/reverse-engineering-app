.class public final Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg;
.super Ljava/lang/Object;
.source "UploadAppLogMsg.kt"

# interfaces
.implements Lcom/intsig/camscanner/message/messages/IMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg;->〇080:Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇o00〇〇Oo()V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/message/messages/common/UploadAppLogMsg;->〇o〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇o〇()V
    .locals 17

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {v1}, Lcom/intsig/camscanner/log/AutoReportLogUtil;->〇o00〇〇Oo(Landroid/content/Context;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    new-instance v1, Lcom/intsig/camscanner/log/FeedbackParams;

    .line 12
    .line 13
    const-string v4, "[Auto Log] Android_CamScanner_Feedback"

    .line 14
    .line 15
    const-string v5, "UploadAppLogMsg"

    .line 16
    .line 17
    const-string v6, " \n"

    .line 18
    .line 19
    const/4 v7, 0x0

    .line 20
    const/4 v8, 0x0

    .line 21
    const/4 v9, 0x0

    .line 22
    const/4 v10, 0x0

    .line 23
    const/4 v11, 0x0

    .line 24
    const/4 v12, 0x0

    .line 25
    const/4 v13, 0x0

    .line 26
    const/4 v14, 0x0

    .line 27
    const/16 v15, 0x920

    .line 28
    .line 29
    const/16 v16, 0x0

    .line 30
    .line 31
    move-object v2, v1

    .line 32
    invoke-direct/range {v2 .. v16}, Lcom/intsig/camscanner/log/FeedbackParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/util/ArrayList;ZLjava/util/ArrayList;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 33
    .line 34
    .line 35
    const-string v2, "error"

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/log/FeedbackParams;->〇〇808〇(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    new-instance v2, Lcom/intsig/camscanner/log/GetLogTask$SendLogTask;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const/4 v3, 0x0

    .line 47
    invoke-direct {v2, v0, v1, v3}, Lcom/intsig/camscanner/log/GetLogTask$SendLogTask;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/log/FeedbackParams;Lcom/intsig/camscanner/log/GetLogTask$IUploadCallback;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2}, Lcom/intsig/camscanner/log/GetLogTask$SendLogTask;->OO0o〇〇()Landroid/net/Uri;

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;)V
    .locals 5
    .param p1    # Lcom/intsig/camscanner/message/entity/CsSocketMsgContent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "message"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8()J

    .line 11
    .line 12
    .line 13
    move-result-wide v2

    .line 14
    sub-long/2addr v0, v2

    .line 15
    const-wide/32 v2, 0xea60

    .line 16
    .line 17
    .line 18
    const-string p1, "UploadAppLogMsg"

    .line 19
    .line 20
    cmp-long v4, v0, v2

    .line 21
    .line 22
    if-lez v4, :cond_0

    .line 23
    .line 24
    const-string v0, "received a auto report log msg"

    .line 25
    .line 26
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->ooo〇880()V

    .line 30
    .line 31
    .line 32
    new-instance p1, LO〇08oOOO0/〇080;

    .line 33
    .line 34
    invoke-direct {p1}, LO〇08oOOO0/〇080;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-static {p1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    const-string v0, "auto report log density too high"

    .line 42
    .line 43
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method
